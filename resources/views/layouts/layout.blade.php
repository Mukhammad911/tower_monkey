@include('js-localization::head')
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <meta name="csrf-token" content="{{csrf_token()}}"/>

        <title>Roulette | CSGOTower - CS:GO Gambling Is Back! 2019</title>

        <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
        <link rel="manifest" href="/images/icons/site.webmanifest">
        <link rel="mask-icon" href="/images/icons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="msapplication-TileImage" content="/images/icons/mstile-144x144.png">
        <meta name="theme-color" content="#0b1320">
        <meta name="description" content="CS:GO Gambling is back! Deposit and withdraw CS:GO skins instantly with our new skin trading system on the oldest and most trusted CS:GO skin gambling site.">

        @yield('title')
        @yield('description')

        <link rel="stylesheet" href="/fonts/fontawesome/css/all.min.css">
        <link rel="stylesheet" href="/assets/css/icons/simple-icons/simple-line-icons.css">
        <link rel="stylesheet" href="{{ elixir('assets/css/all.css') }}">

        @yield('css')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137863802-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-137863802-1');
        </script>
    </head>

    <body>
        <div class="wrapper-page">
            @include('layouts.layout_inc')
        </div>

        @include('site.partials.modal-login')
        @include('site.partials.modal-sign-in')
        @include('site.partials.modal-remind-password')
        @include('site.partials.modal-balance')
        @include('site.partials.modal-cashout')
        @include('site.partials.modal-provably-fair')
        @include('site.partials.modal-win-numbers')
        @include('site.partials.modal-once-free')

        <a href="#?w=430" class="update-tradeurl" data-rel="popup-trade"></a>

        @yield('js-localization.head')

        @include('site.partials.js')

        <script src="{{ elixir('assets/js/all.js') }}"></script>

        @yield('js')
    </body>
</html>
