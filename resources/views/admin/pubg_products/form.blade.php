<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Name: <span class="text-danger">*</span></label>
        <div class="col-lg-5">
            @if(isset($weapons))
                <select name="name" class="form-control existing-weapon" required="required">
                    <option></option>
                    @foreach($weapons as $weapon)
                        <option @if($product->name == $weapon->name) selected @endif value="{{$weapon->name}}">{{ (Lang::has('weapon.'.$weapon->name)) ? trans('weapon.'.$weapon->name) : $weapon->name }}</option>
                    @endforeach
                </select>
            @else
                <input type="text" name="name" class="form-control" required="required">
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Price: <span class="text-danger">*</span></label>
        <div class="col-lg-5">
            <input name="price" type="text" class="form-control price number" max="100000000" value="{{ ($product->price/100) }}" required="required">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Image Source: </label>
        <div class="col-lg-10">
            <label class="radio-inline">URL:</label>
            <input value="url" {{ (!$product->id || ($product->image_source=='url')) ? 'checked' : '' }} type="radio" class="styled image_source" name="image_source">
            <label class="radio-inline">File:</label>
            <input value="file" {{ ($product->image_source=='file') ? 'checked' : '' }} type="radio" class="styled image_source" name="image_source">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Image <span class="text-danger">*</span></label>
        <div class="col-lg-10" id="image-source-block">
            <div class="form-group url-source {{ ($product->id && $product->image_source=='file') ? 'hidden' : '' }}">
                <label class="text-semibold">Image URL:</label>
                <input name="image" type="text" class="form-control url" value="{{ ($product->image_source == 'url') ? $product->image : '' }}">
                @if($product->image_source == 'url')
                    <img src="/images/pubg_images/100/{{ $product->image }}" alt="Image" style="width: 58px; height: 58px; border-radius: 2px;">
                @endif
            </div>
            <div class="form-group image {{ (($product->id && $product->image_source=='url') || !$product->id) ? 'hidden' : '' }}">
                <label class="text-semibold">Image:</label>
                <div class="media no-margin-top">
                    <div class="media-left">
                        @if($product->image_source == 'file')
                            <img src="{{ asset('/images/pubg_images/100') }}/{{ $product->image }}" alt="Image" style="width: 58px; height: 58px; border-radius: 2px;">
                        @else
                            <img src="{{ asset('images/placeholder.jpg') }}" alt="Image" style="width: 58px; height: 58px; border-radius: 2px;">
                        @endif
                    </div>

                    <div class="media-body">
                        <input name="image" type="file" class="file-styled">
                        <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($product->id)
        <div class="form-group">
            <label class="control-label col-lg-2 text-nowrap">Locked: </label>
            <div class="col-lg-10">
                <div class="checkbox checkbox-switch">
                    <input name="locked"  type="checkbox" class="switch" data-on-text="Yes" data-off-text="No" data-on-color="danger" data-off-color="default" @if($product->locked) checked @endif>
                </div>
            </div>
        </div>
    @endif
</fieldset>
