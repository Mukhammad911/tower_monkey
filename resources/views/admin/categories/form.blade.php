<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Name: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="name" type="text" class="form-control" value="{{ $category->name or '' }}" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Description: </label>
        <div class="col-lg-10">
            <textarea name="description" class="form-control">{{ $category->description or '' }}</textarea>
        </div>
    </div>
</fieldset>