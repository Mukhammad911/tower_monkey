@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Categories') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Categories</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <button type="button" id="add_category" class="btn bg-success-400 btn-labeled">
                    <b><i class="fa fa-plus"></i></b>Add Category
                </button>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="categories" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Sort</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        function deleteCategory(id){
            swal({
                title: "Are you sure?",
                text: "Selected Category will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/category') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#categories').DataTable().ajax.reload();
                        swal(
                            "Deleted!",
                            "Category has been deleted.",
                            "success"
                        );
                    }
                });
            });
        }

        function sortUp(id){
            $.ajax({
                type: "GET",
                dataType: "json",
                url: '{{ url(Config::get('app.admin_prefix').'/category/sortup') }}/'+id
            })
            .success(function(success){
                if(success){
                    $('#categories').DataTable().ajax.reload();
                }
            });
        }

        function sortDown(id){
            $.ajax({
                type: "GET",
                dataType: "json",
                url: '{{ url(Config::get('app.admin_prefix').'/category/sortdown') }}/'+id
            })
            .success(function(success){
                if(success){
                    $('#categories').DataTable().ajax.reload();
                }
            });
        }

        $(function(){
            $('#add_category').click(function(){
                location = '{{ url(Config::get('app.admin_prefix').'/category/create') }}';
            });

            $('#categories').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/category') }}',
                sorting:false,
                searching:false,
                ordering:false,
                info:false,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "name"},
                    { data: "sort"},
                    { data: 'action', name: 'action', orderable: false, searchable: false, width:500}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection