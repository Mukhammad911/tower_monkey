<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
            <label class="control-label col-lg-2 text-nowrap">Status: <span class="text-danger">*</span></label>
            <div class="col-lg-10">
                <select name="status" class="form-control" required="required">
                        <?php
                        $statuses = [
                            'pending',
                            'rejected',
                            'completed',
                        ];
                        ?>
                        @foreach($statuses as $status)
                         <option value="{{ $status }}" @if(@$withdrawal->status == $status) selected @endif>{{ $status }}</option>
                        @endforeach
                </select>
            </div>
        </div>

</fieldset>