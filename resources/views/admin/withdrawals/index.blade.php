@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Withdrawals</span></h4>
        </div>

    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="withdrawals" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th></th>
                    <th>User</th>
                    <th>Amount</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#withdrawals').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/withdrawal') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id", name: 'w.id'},
                    { data: "user_id", name: 'user.id', visible: false},
                    { data: "username", name: 'user.username'},
                    { data: "amount", name: 'w.amount'},
                    { data: "type", name: 'w.type'},
                    { data: "status", name: 'w.status'},
                    { data: "details", name: 'w.details'},
                    { data: "created_at", name: 'w.created_at'},
                    { data: 'actions', name: 'actions', orderable: false, searchable: false}

                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection