@extends('admin.template')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit Battle setting</h5>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/battle-setting').'/'.$setting->id }}" method="post" enctype='multipart/form-data'>
                        <input name="_method" type="hidden" value="PUT">
                        @include('admin.battle_settings.form')
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>

    </script>
@endsection