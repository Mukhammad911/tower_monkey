@include('js-localization::head')
@extends('admin.template')
@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i> <span class="text-semibold">Pubg Purchases</span> - Add new purchase</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/csgo-purchase') }}"
               class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-exit3"></i></b> Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="beastCreatePurchaseContainer" data-ng-app="beastCreatePurchaseCsgo">
        <div data-ng-controller="Ctrl6">
            <input class="custom_search_input" id="search_form" data-ng-model="searchString"
                   data-ng-change="searchStringClick()"
                   type="text" placeholder="Enter item name">
            <div class="container_product_select csgo_container_product_select">
                <div class="dota_product_not_selected">
                    <div class="purchase_product csgo_purchase_product"
                         data-ng-repeat="product in csgo_products | startFrom:csgo_currentPage*csgo_pageSize | limitTo:csgo_pageSize"
                         data-ng-click='csgo_select_item_purchase("<%product.id%>","<%product.name%>","<%product.price%>","<%product.image%>")'>
                        <p class="name_product_purchase"><%product.name%></p>
                        <img class="image_product_purchase_csgo" src="<%product.image%>">
                        <p class="price_product_purchase csgo_price_product_purchase"><%product.price%> ₽</p>
                    </div>
                    <div class="pagination_button_purchase">
                    <button class="btn btn-info" data-ng-click="csgo_currentPage=csgo_currentPage-1">
                        Previous
                    </button>

                    <button class="btn btn-info"  data-ng-click="csgo_currentPage=csgo_currentPage+1">
                        Next
                    </button>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="dota_product_selected">
                <div class="purchase_product_selected csgo_purchase_product_selected"
                     data-ng-repeat="product in csgo_selected_product_purchase">
                    <p class="name_product_purchase_selected"><%product.name%></p>
                    <img class="image_product_purchase_csgo" src="<%product.image%>">
                    <p class="price_product_purchase_selected pubg_product_purchase_selected"><%product.price%></p>
                    <input id="value_id_of_<%product.id%>" class="input_purchase_count" type="number" placeholder="count" value="1">
                    <div data-ng-click='csgo_delete_purchase_selected_item("<%product.id%>")' class="delete_selected_purchase">X</div>
                    <img class="rubl_symbol" src="/images/csgotower/rubl_symbol.png">

                </div>
            </div>
            <hr/>
            <div class="container_for_margin pubg_container">
                <div class="container_for_label">
                    <p class="input_price_label">BOT</p>
                    <p class="input_price_label">MAX PRICE</p>
                    <p class="input_price_label">MIN PRICE</p>
                    <p class="input_price_label">Select All</p>
                </div>
                <div class="container_for_input">
                <select class="input_price"  data-ng-model="csgo_selected_bot_purchase">
                    <option selected></option>
                    <option data-ng-repeat="bot in csgo_bots" id="<%bot.id%>" value="<%bot.id%>"> <%bot.name%></option>
                </select></br>
                    <input class="input_price" data-ng-model="csgo_max_price" type="text" placeholder="max price"></br>
                    <input class="input_price" data-ng-model="csgo_min_price" type="text" placeholder="min price"></br>
                    <input type="button" class="btn btn-info" value="Selec All" data-ng-click="csgo_select_all_item()"></br>
                </div>
            </div>
           {{-- <div class="container_quality_purchase">
                <div class="select_item_quality" data-ng-click="csgo_selectItem()">QUALITY</div>
                <div id="listOfItems" class="all_item_quality">
                    <div data-ng-click="csgo_selectItems('all')" class="item_quality">All</div>
                    <div data-ng-click="csgo_selectItems('Немного поношенное')" class="item_quality">Немного поношенное</div>
                    <div data-ng-click="csgo_selectItems('Закаленное в боях')" class="item_quality">Закаленное в боях</div>
                    <div data-ng-click="csgo_selectItems('Прямо с завода')" class="item_quality">Прямо с завода</div>
                    <div data-ng-click="csgo_selectItems('Поношенное')" class="item_quality">Поношенное</div>
                    <div data-ng-click="csgo_selectItems('После полевых испытаний')" class="item_quality">После полевых испытаний</div>
                    <div data-ng-click="csgo_selectItems('Не покрашено')" class="item_quality">Не покрашено</div>
                </div>
                <div id="selectd_items_quality" class="selected_items_quality">
                    <div data-ng-repeat="items in csgo_selected_items" class="quality_selected_items csgo_quality_selected_items"><%items%>
                        <div data-ng-click="csgo_deleteItems('<%items%>')" class="delete_quality_items">x
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_quality_purchase">
                <div class="select_item_quality" data-ng-click="csgo_selectItemRare()">Rarity</div>
                <div id="listOfItemsRare" class="all_item_quality">
                    <div data-ng-click="csgo_selectItemsRare('all')" class="item_quality">All</div>
                    <div data-ng-click="csgo_selectItemsRare('Базового класса')" class="item_quality">Базового класса</div>
                    <div data-ng-click="csgo_selectItemsRare('Ширпотреб')" class="item_quality">Ширпотреб</div>
                    <div data-ng-click="csgo_selectItemsRare('Промышленное качество')" class="item_quality">Промышленное качество</div>
                    <div data-ng-click="csgo_selectItemsRare('Армейское качество')" class="item_quality">Армейское качество</div>
                    <div data-ng-click="csgo_selectItemsRare('Запрещенное')" class="item_quality">Запрещенное</div>
                    <div data-ng-click="csgo_selectItemsRare('Засекреченное')" class="item_quality">Засекреченное</div>
                    <div data-ng-click="csgo_selectItemsRare('Тайное')" class="item_quality">Тайное</div>
                    <div data-ng-click="csgo_selectItemsRare('Высшего класса')" class="item_quality">Высшего класса</div>
                    <div data-ng-click="csgo_selectItemsRare('Примечательного типа')" class="item_quality">Примечательного типа</div>
                    <div data-ng-click="csgo_selectItemsRare('Экзотичного вида')" class="item_quality">Экзотичного вида</div>
                    <div data-ng-click="csgo_selectItemsRare('Контрабандное')" class="item_quality">Контрабандное</div>
                    <div data-ng-click="csgo_selectItemsRare('Контрабандное')" class="item_quality">Экстраординарного типа</div>
                </div>
                <div id="selectd_items_quality" class="selected_items_quality">
                    <div data-ng-repeat="items in csgo_selected_items_rare" class="quality_selected_items csgo_quality_selected_items"><%items%>
                        <div data-ng-click="csgo_deleteItemsRare('<%items%>')" class="delete_quality_items">x
                        </div>
                    </div>
                </div>
            </div>--}}

            <input class="btn btn-danger custom_btn custom_btn_pubg" data-ng-click="csgo_savePurchase()" type="button" value="SAVE">
            <div id="loader_purchase" class="loading_purchase"><img src="/images/csgotower/squares.gif">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/angular.js"></script>
    <script type="text/javascript" src="/assets/js/underscore.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pubg_create_purchase.js') }}"></script>
@endsection
