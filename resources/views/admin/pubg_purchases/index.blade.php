@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Purchase') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i><span class="text-semibold">PUBG Purchases</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/pubg-purchase/create_form') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-plus-circle2"></i></b> Add new purchase</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="purchases" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Bot Steam Login</th>
                <th>MIN</th>
                <th>MAX</th>
                <th>QUALITY</th>
                <th>RARITY</th>
                <th>CREATED_AT</th>
                <th>UPDATED_AT</th>
                <th>EDIT</th>
                <th>DELETE</th>
            </tr>
            @foreach($data as $d)
                <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->bot_id}}</td>
                    <td>{{$d->min_price}}</td>
                    <td>{{$d->max_price}}</td>
                    <td>{{$d->quality}}</td>
                    <td>{{$d->rarity}}</td>
                    <td>{{$d->created_at}}</td>
                    <td>{{$d->updated_at}}</td>
                    <form action="pubg_edit/{{$d->id}}" method="post">
                        {{ csrf_field() }}
                        <td><input class="btn btn-info" type="submit" value="edit"></td>
                    </form>
                    <form action="pubg_delete/{{$d->id}}" method="post">
                        {{ csrf_field() }}
                    <td><input class="btn btn-danger" type="submit" value="delete"></td>
                    </form>
                </tr>
                @endforeach
            </thead>
        </table>
    </div>
@endsection


