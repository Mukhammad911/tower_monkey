@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('DotaCase-create') !!}
    </div>
@endsection

@section('css')
    <link href="{{ asset('assets/css/case-create-edit.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Create Case</h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" id="case_form" action="{{ url(Config::get('app.admin_prefix').'/pubg_case') }}" method="post" enctype='multipart/form-data'>
                        @include('admin.pubg_cases.form')
                        <div class="text-right">
                            <button id="btn-save" type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $(function(){
            $("[name='active']").bootstrapSwitch({});

            $('.price').numberMask({type:'float',afterPoint:2,decimalMark:'.'});
        });
    </script>
@endsection