<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Name: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="name" type="text" class="form-control" value="{{ $case->name }}" required="required">
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-10">
            <fieldset>
                <legend class="label label-danger">Price</legend>
                @if(count($currency))
                    @foreach($currency as $c)
                        <div class="form-group">
                            <label class="control-label col-md-4 text-nowrap">{{ $c->title }}({{ $c->symbol_left }}) {{--<span class="text-danger">*</span>--}}</label>
                            <div class="col-md-8">
                                <input name="price[{{ $c->code }}]" type="text" class="form-control price" value="{{ (isset($prices[$c->code])) ? $prices[$c->code] : '' }}">
                            </div>
                        </div>
                    @endforeach
                @else
                    <label id="price-error" class="validation-error-label" for="currency">
                        At least 1 currency must be active in Settings->Localization!
                    </label>
                @endif
            </fieldset>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Image: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="media no-margin-top">
                <div class="media-left">
                    @if($case->image)
                        <img style="max-width:200px" src="{{ asset('images/cases').'/'.$case->image }}" style="width: 200px; height: 120px; border-radius: 2px;" alt="Image">
                    @else
                        <img style="max-width:200px" src="{{ asset('images/placeholder.jpg') }}" alt="image" style="width: 58px; height: 58px; border-radius: 2px;">
                    @endif
                </div>
                <div class="media-body">
                    <input name="image" type="file" class="file-styled">
                    <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Mark: </label>
        <div class="col-lg-10">
            <select name="mark" id="mark" class="form-control">
                <option value=""></option>
                @foreach(\App\Models\PubgCases\PubgCase::$marks as $mark)
                    <option @if($case->mark == $mark) selected @endif value="{{ $mark }}">{{ trans('messages.'.$mark) }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Category: </label>
        <div class="col-lg-10">
            <select name="category_id" id="category_id" class="form-control" required="required">
                <option value=""></option>
                @foreach($categories as $item)
                    <option @if($item->id == $case->category_id) {{ 'selected' }} @endif value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Active: </label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input name="active"  type="checkbox" class="switch" data-on-text="Yes" data-off-text="No" data-on-color="danger" data-off-color="default" @if($case->id && $case->active) checked @endif>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Title: </label>
        <div class="col-lg-10">
            <input maxlength="255" type="text" name="title" class="form-control" value="{{ $case->title }}">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Description: </label>
        <div class="col-lg-10">
            <textarea name="description" class="form-control">{{ $case->description }}</textarea>
        </div>
    </div>
    <div class="product-box">
    </div>
</fieldset>