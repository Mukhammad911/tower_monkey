@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Case-edit', $case->id) !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit Case
                        <label class="label label-info">
                            ID - {{ $case->id }}
                        </label>
                    </h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" id="case_form" action="{{ url(Config::get('app.admin_prefix').'/pubg_case').'/'.$case->id }}" method="post" enctype='multipart/form-data'>
                        <input type="hidden" value="{{ $case->id }}" id="case_id">
                        <input name="_method" type="hidden" value="PUT">
                        @include('admin.pubg_cases.form')
                        <div class="text-right">
                            <button id="btn-save" type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-flat panel-products">
                <div class="panel-body">
                    <form method="POST" id="search-form" role="form">
                        <div class="form-group">
                            <div class="col-sm-1">
                                <label for="case">Name</label>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="product_name">
                                    <option></option>
                                    @foreach($weapons as $weapon)
                                        <option value="{{$weapon->name}}">{{ (Lang::has('weapon.'.$weapon->name)) ? trans('weapon.'.$weapon->name) : $weapon->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <label for="case">Descr</label>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" name="product_description">
                                    <option></option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
                <div class="panel-body" id="products_container">
                    <table class="table case-products" style="width:100%">
                        <thead>
                        <tr class="bg-primary-700">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Check</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $(function(){
            var case_active = '{{ (bool)$case->active }}';

            $("[name='active']").bootstrapSwitch({
                state:case_active
            });

            var table = $('.case-products').DataTable({
                ajax:{
                    url: '{{ url(Config::get('app.admin_prefix').'/pubg/case/'.$case->id.'/product-list') }}',
                    data: function (d) {
                        d.product_name = $('[name=product_name]').find('option:selected').val(),
                        d.product_description = $('[name=product_description]').find('option:selected').val()
                    }
                },
                autoWidth: true,
                sorting:false,
                searching:false,
                processing: true,
                serverSide: true,
                ordering:false,
                info:false,
                columns: [
                    { data: "id"},
                    { data: "name"},
                    { data: "image"},
                    { data: "check"}
                ],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                },
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>'
            });

            $('.case-products').on( 'draw.dt', function () {
                $('.styled').uniform();

                $('.weapon-item').change(function(e){
                    var product_id = $(this).val(),
                        checked = ($(this).prop('checked')) ? 1 : 0;

                    $.ajax({
                        url:'{{ url(Config::get('app.admin_prefix').'/pubg/case/attach-product') }}',
                        method:'post',
                        dataType:'json',
                        data:{
                            case_id:'{{ $case->id }}',
                            product_id:product_id,
                            attach:checked
                        }
                    })
                });
                $('[data-popup="tooltip"]').tooltip();
            });

            $('[name="product_name"]').select2({
                placeholder: "Select Weapon",
                allowClear: true
            });

            $('[name="product_description"]').select2({
                placeholder: "Select Description",
                allowClear: true
            });

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

            $('.price').numberMask({type:'float',afterPoint:2,decimalMark:'.'});
        });
    </script>
@endsection