@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Products') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Products for Case <span>{{ $case->name }}</span></h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/pubg/cases') }}" type="button" class="btn bg-primary btn-labeled heading-btn btn-sm">
                    <b><i class="icon-arrow-left13"></i></b>Back
                </a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="products" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Image</th>
                <th>Class</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var caseId         = '{{ $case->id }}';
        var productsData   = '{{ url(Config::get('app.admin_prefix').'/pubg/case-products/') }}';
        var removeProduct  = '{{ url(Config::get('app.admin_prefix').'/pubg/delete/product-case') }}';
        var up             = '{{ url(Config::get('app.admin_prefix').'/pubg-product/sortup') }}/id/'+'{{ $case->id }}';
        var down           = '{{ url(Config::get('app.admin_prefix').'/pubg-product/sortdown') }}/id/'+'{{ $case->id }}';
    </script>
    <script src="/assets/js/case-products.js"></script>
@endsection
