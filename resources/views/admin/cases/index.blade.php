@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Cases') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Cases</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <button id="change_position_btn" data-toggle="modal" data-target="#change_cases" class="btn bg-orange-800">Change Position</button>
                <a href="{{ url(Config::get('app.admin_prefix').'/case/create') }}" type="button" id="add_case" class="btn bg-success-400 btn-labeled">
                    <b><i class="fa fa-plus"></i></b>Add Case
                </a>
            </div>
        </div>
    </div>

    <div class="modal fade" id="change_cases" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="change-position-form">
                    <div class="modal-header">
                        <h4 class="modal-title">Change Cases Position</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="case" class="control-label">Case1:</label>
                                <select name="case1" type="text" class="form-control" id="case1"></select>
                            </div>
                            <div class="form-group">
                                <label for="case2" class="control-label">Case2:</label>
                                <select name="case2" type="text" class="form-control" id="case2"></select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="confirm_change_position" type="button" class="btn btn-primary">Change</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="cases" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Active</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var casesData           = '{{ url(Config::get('app.admin_prefix').'/case') }}';
        var casesAutocomplete   = '{{ url(Config::get('app.admin_prefix').'/case/autocomplete') }}';
        var deleteCase          = '{{ url(Config::get('app.admin_prefix').'/case') }}';
        var up                  = '{{ url(Config::get('app.admin_prefix').'/case/sortup') }}/';
        var down                = '{{ url(Config::get('app.admin_prefix').'/case/sortdown') }}/';
        var changePositions     = '{{ url(Config::get('app.admin_prefix').'/cases/change-positions') }}';
    </script>
    <script src="/assets/js/cases.js"></script>
@endsection