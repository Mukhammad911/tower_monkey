@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Products') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i> <span class="text-semibold">Purchases for Case <span>{{ $case->name }}</span></h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/cases') }}" type="button" class="btn bg-primary btn-labeled heading-btn btn-sm">
                    <b><i class="icon-arrow-left13"></i></b>Back
                </a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <div class="panel-heading">
            <h5 class="panel-title">Low price products. <mark class="bg-success">{{ $totalLow }}</mark></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <table id="products" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-success-700">
                <th>Count</th>
                <th>Image</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Class</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                @if($product->price <= $caseLowPrice)
                    @if(count($product->availableProducts) > 0)
                    <tr>
                        <td><span class="badge badge-primary">{{ count($product->availableProducts) }}</span></td>
                        <td><img style="max-height:40px" src="{{ ($product->image_source == 'url') ? $product->image : asset("images/products/$product->image") }}"></td>
                        <td><strong>{{ (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name }}</strong></td>
                        <td>{{ (!empty($product->short_description)) ? trans('weapon.'.$product->short_description) : '' }}</td>
                        <td><span class="badge badge-success">{{ $product->price/100 }} руб.</span></td>
                        <td><label class="label {{ $product->class }}">{{ $product->class }}</label></td>
                    </tr>
                    @endif
                @endif
            @endforeach
            </tbody>
        </table>


    </div>

    <div class="panel panel-white">
        <div class="panel-heading">
            <h5 class="panel-title">Mid price products. <mark class="bg-orange">{{ $totalMid }}</mark></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <table id="products" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-orange">
                <th>Count</th>
                <th>Image</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Class</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                @if($product->price > $caseLowPrice && $product->price <= $caseMidPrice)
                    @if(count($product->availableProducts) > 0)
                        <tr>
                            <td><span class="badge badge-primary">{{ count($product->availableProducts) }}</span></td>
                            <td><img style="max-height:40px" src="{{ ($product->image_source == 'url') ? $product->image : asset("images/products/$product->image") }}"></td>
                            <td><strong>{{ (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name }}</strong></td>
                            <td>{{ (!empty($product->short_description)) ? trans('weapon.'.$product->short_description) : '' }}</td>
                            <td><span class="badge badge-success">{{ $product->price/100 }} руб.</span></td>
                            <td><label class="label {{ $product->class }}">{{ $product->class }}</label></td>
                        </tr>
                    @endif
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="panel panel-white">
        <div class="panel-heading">
            <h5 class="panel-title">High price products. <mark class="bg-danger">{{ $totalHigh }}</mark></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <table id="products" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-danger">
                <th>Count</th>
                <th>Image</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Class</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                @if($product->price > $caseMidPrice)
                    @if(count($product->availableProducts) > 0)
                        <tr>
                            <td><span class="badge badge-primary">{{ count($product->availableProducts) }}</span></td>
                            <td><img style="max-height:40px" src="{{ ($product->image_source == 'url') ? $product->image : asset("images/products/$product->image") }}"></td>
                            <td><strong>{{ (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name }}</strong></td>
                            <td>{{ (!empty($product->short_description)) ? trans('weapon.'.$product->short_description) : '' }}</td>
                            <td><span class="badge badge-success">{{ $product->price/100 }} руб.</span></td>
                            <td><label class="label {{ $product->class }}">{{ $product->class }}</label></td>
                        </tr>
                    @endif
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('js')
    <script>


    </script>
@endsection
