<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
            <label class="control-label col-lg-2 text-nowrap">User: <span class="text-danger">*</span></label>
            <div class="col-lg-10">
                <select id="user_id" name="user_id" class="form-control" required>
                    <option></option>
                </select>
            </div>
        </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Amount: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input type="number" class="form-control" name="amount">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Currency: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <select class="form-control" name="currency_id">
                    @foreach($currencies as $currency)
                        <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Description: </label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input type="text" name="description" value="created by moderator" disabled>
            </div>
        </div>
    </div>

</fieldset>