@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Tower settings</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/tower-setting/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Create</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="tower-settings" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Setting</th>
                    <th>Value</th>
                    <th>Description</th>
                    <th>Updated</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        function deleteTowerSetting(id){
            swal({
                title: "Are you sure?",
                text: "Selected setting will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/tower-setting') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#tower-settings').DataTable().ajax.reload();
                        swal(
                            "Deleted!",
                            "Setting has been deleted.",
                            "success"
                        );
                    }
                });
            });
        }

        $(function(){
            tableObj = $('#tower-settings').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/tower-setting') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "key"},
                    { data: "value"},
                    { data: "description"},
                    { data: "updated_at"},
                    { data: "action", orderable: false, searchable: false}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection