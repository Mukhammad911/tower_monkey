<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Parameter<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="text" class="course form-control" name="key" value="{{ $setting->key or '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Value<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="text" class="course form-control" name="value" value="{{ $setting->value or '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Symbol<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input type="text" class="course form-control" name="symbol" value="{{ $setting->symbol or '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Description<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <textarea rows="5" class="course form-control" name="description">{{ $setting->description or '' }}</textarea>
            </div>
        </div>
    </div>
</fieldset>