@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Operations') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Operations</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/operation/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Create operation</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="users" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>Operation ID</th>
                    <th></th>
                    <th>User</th>
                    <th>Balance</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th>Date</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#users').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/operation') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id", name: 'operation.id'},
                    { data: "user_id", name: 'user.id', visible: false},
                    { data: "username", name: 'user.username'},
                    { data: "balance", name: 'operation.balance'},
                    { data: "amount", name: 'operation.amount'},
                    { data: "description", name: 'operation.description'},
                    { data: "created_at", name: 'operation.created_at'}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection