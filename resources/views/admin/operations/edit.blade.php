@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('User-edit', $user->id) !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit User
                        <label class="label label-info">
                            ID - {{ $user->id }}
                        </label>
                        <label class="label label-danger">
                            Youtuber - {{ $user->youtuber }}
                        </label>
                    </h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/user').'/'.$user->id }}" method="post" enctype='multipart/form-data'>
                        <input name="_method" type="hidden" value="PUT">
                        <input name="id" type="hidden" value="{{ $user->id }}">
                        @include('admin.users.form')
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        var active = '{{ (bool)$user->active }}';
        console.log(active);
        $(function(){
            $("[name='active']").bootstrapSwitch({
                state: active
            });

            var top_banned_state = '{{ (bool)$user->top_banned }}';
            var drop_banned_state = '{{ (bool)$user->drop_banned }}';
            $("[name='top_banned']").bootstrapSwitch({
                state:top_banned_state
            });
            $("[name='drop_banned']").bootstrapSwitch({
                state:drop_banned_state
            });
        });

    </script>
@endsection