@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">

    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Edit Price Dota Available Products</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <div data-ng-app="editPriceProductDota" data-ng-controller="editDotaCtrl" data-ng-init="init()">
            <input class="custom_search_input" id="search_form" data-ng-model="searchString"
                   data-ng-change="searchStringClick()"
                   type="text" placeholder="Enter item name">

            <div class="container_product_select csgo_container_product_select">


                <div class="dota_product_not_selected">
                    <div class="purchase_product csgo_purchase_product_edit"
                         data-ng-repeat="product in csgo_products | startFrom:csgo_currentPage*csgo_pageSize | limitTo:csgo_pageSize"
                         data-ng-click='csgo_select_item_purchase("<%product.id%>","<%product.name%>","<%product.price%>","<%product.short_description%>","<%product.image%>","<%product.quality%>")'>

                        <p class="name_product_purchase_edit"><%product.stattrak%> <%product.name%></p>
                        <p class="quality_product_purchase_dota">(<%product.quality%>)</p>
                        <img class="image_product_purchase_csgo" src="https://steamcommunity-a.akamaihd.net/economy/image/<%product.image%>/100fx100f/image.png">
                        <p class="price_product_purchase csgo_price_product_purchase"><%product.price%> ₽</p>
                        <p class="short_product_purchase"><%product.short_description%></p>
                    </div>
                    <div class="pagination_button_purchase_edit">
                        <button class="btn btn-info" data-ng-click="csgo_currentPage=csgo_currentPage-1">
                            Previous
                        </button>

                        <button class="btn btn-info"  data-ng-click="csgo_currentPage=csgo_currentPage+1">
                            Next
                        </button>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="dota_product_selected">
                <div class="purchase_product_selected csgo_purchase_product_selected_edit"
                     data-ng-repeat="product in csgo_selected_product_purchase">
                    <p class="name_product_purchase_selected_dota"><%product.name%></p>
                    <p class="quality_product_purchase_selected_edit_dota">(<%product.quality%>)</p>
                    <img class="image_product_purchase_dota_edit" src="https://steamcommunity-a.akamaihd.net/economy/image/<%product.image%>/100fx100f/image.png">
                    <p class="description_product_purchase_selected_edit"><%product.short_description%></p>
                    <input id="value_id_of_<%product.id%>" class="input_purchase_count_edit_dota" type="number" placeholder="<%product.price%>" value="<%product.price%>">
                    <div data-ng-click='csgo_delete_purchase_selected_item("<%product.id%>")' class="delete_selected_purchase_edit_dota">X</div>
                </div>
            </div>
            <hr/>
            <input class="btn btn-danger custom_btn_edit_price" data-ng-click="csgo_savePurchase()" type="button" value="SAVE">
            <div id="loader_purchase" class="loading_purchase"><img src="/images/beastskins/squares.gif">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/angular.js"></script>
    <script type="text/javascript" src="/assets/js/underscore.js"></script>
    <script type="text/javascript" src="/assets/js/editPriceProductsDota.js"></script>
@endsection

