@extends('admin.template')

@section('breadcrumbs')
    <h1>Roulette CSGO CASE Games statistics</h1>
    <div class="page-header-content">
        <div class="row">
            <div class="col-md-4 text-center bg-success">
                <br>
                <i class="icon-plus-circle2" style="font-size: 50px;"></i>
                <p>Income</p>
                <p style="font-size: 30px;">${{ number_format($total_income, 2) }}</p>
                <br><br>
            </div>

            <div class="col-md-4 text-center bg-danger">
                <br>
                <i class="icon-minus-circle2" style="font-size: 50px;"></i>
                <p>Expense</p>
                <p style="font-size: 30px;">${{ number_format($total_expense, 2) }}</p>
                <br><br>
            </div>

            <div class="col-md-4 text-center bg-primary">
                <br>
                <i class="icon-wallet" style="font-size: 50px;"></i>
                <p>Balance</p>
                <p style="font-size: 30px;">${{ number_format($total_income - $total_expense, 2) }}</p>
                <br><br>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <div class="row">
            <div class="col-md-4">
                <table class="table table-striped">
                    <tr>
                        <td>Target profit:</td>
                        <td>{{ $target_company_percent }}% <a href="/{{ Config::get('app.admin_prefix') }}/csgo-case-roulette-settings"><button class="btn btn-primary">edit...</button></a></td>
                    </tr>
                    <tr>
                        <td>Current profit:</td>
                        <td>{{ number_format($current_company_percent, 2) }}%</td>
                    </tr>
                    <tr>
                        <td>Profit deviation percent:</td>
                        <td>{{ number_format($profit_deviation_percent, 2) }}%</td>
                    </tr>
                    <tr>
                        <td>Reset Statistics:</td>
                        <td><a href="/{{ Config::get('app.admin_prefix') }}/csgo-case-roulette-reset"><button class="btn btn-danger">reset</button></a></td>
                    </tr>
                    <tr>
                        <td>Reset Statistics YouTube:</td>
                        <td><a href="/{{ Config::get('app.admin_prefix') }}/csgo-case-roulette-reset-youtubers"><button class="btn btn-danger">reset youtube</button></a></td>
                    </tr>
                </table>
            </div>

            <div class="col-md-4">

            </div>

            <div class="col-md-4">

            </div>
        </div>
    </div>



    <div class="panel panel-white col-md-6">
        <div id="chart_current_profit_percent" style="width:100%; height:300px;"></div>
    </div>

    <div class="panel panel-white col-md-6">
        <div id="chart_profit_deviation" style="width:100%; height:300px;"></div>
    </div>

    <div class="panel panel-white col-md-6">
        <div id="chart_income" style="width:100%; height:300px;"></div>
    </div>

    <div class="panel panel-white col-md-6">
        <div id="chart_expense" style="width:100%; height:300px;"></div>
    </div>
@endsection

@section('js')
<script>
    $(function () {
        Highcharts.chart('chart_current_profit_percent', {
            chart: {
                type: 'line',
                zoomType: 'x'
            },
            title: {
                text: 'Current profit percent'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '%'
                }
            },
            series: [{
                name: 'current profit',
                data: <?= json_encode($current_profit_percent_data) ?>,
                color: '#2196F3',
                marker: {
                    enabled: true
                }
            }]
        });

        Highcharts.chart('chart_profit_deviation', {
            chart: {
                type: 'coloredline',
                zoomType: 'x'
            },
            title: {
                text: 'Profit deviation percent'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '%'
                }
            },
            series: [{
                name: 'Profit deviation percent',
                data: <?= json_encode($profit_deviation_percent_data) ?>,
                color: '#A143F3',
                marker: {
                    enabled: true
                }
            }]
        });

        Highcharts.chart('chart_income', {
            chart: {
                type: 'coloredline',
                zoomType: 'x'
            },
            title: {
                text: 'Income'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '$'
                }
            },
            series: [{
                name: 'income',
                data: <?= json_encode($income_data) ?>,
                color: '#4CAF50',
                marker: {
                    enabled: true
                }
            }]
        });

        Highcharts.chart('chart_expense', {
            chart: {
                type: 'coloredline',
                zoomType: 'x'
            },
            title: {
                text: 'Expense'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '$'
                }
            },
            series: [{
                name: 'expense',
                data: <?= json_encode($expense_data) ?>,
                color: '#F44336',
                marker: {
                    enabled: true
                }
            }]
        });
    });
</script>
@endsection