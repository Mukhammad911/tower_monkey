@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">CSGO CASE Roulette games (drops)</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="roulettes" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>User</th>
                    <th>Status</th>
                    <th>Price</th>
                    <th>Case</th>
                    <th>Product</th>
                    <th>Class</th>
                    <th>Updated</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#roulettes').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/csgo-case-roulette') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "user_id"},
                    { data: "status"},
                    { data: "price"},
                    { data: "case_id"},
                    { data: "product_id"},
                    { data: "class"},
                    { data: "updated_at"}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection