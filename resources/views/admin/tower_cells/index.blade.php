@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Tower turns</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="tower-cells" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Game ID</th>
                    <th>Step</th>
                    <th>Cell</th>
                    <th>Win</th>
                    <th>User seed</th>
                    <th>Server seed</th>
                    <th>Win number</th>
                    <th>Updated</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#tower-cells').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/tower-cell') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "tower_id"},
                    { data: "step"},
                    { data: "cell"},
                    { data: "win"},
                    { data: "user_seed"},
                    { data: "server_seed"},
                    { data: "win_number"},
                    { data: "updated_at"}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection