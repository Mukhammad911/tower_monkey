<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">User: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="user_id" name="user_id" class="form-control" required>
                <option></option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Case: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="case_id" name="case_id" class="form-control" required>
                <option></option>
                @foreach($cases as $case)
                    <option value="{{ $case->id }}"
                            data-name="{{ $case->name }}"
                            data-image="{{ $case->image }}"
                            @if($scenario->case_id == $case->id) selected @endif>
                        {{ $case->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Attempt: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="attempt" type="number" max="100" class="form-control" value="{{ $scenario->attempt or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Product: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="product_id" name="product_id"  class="form-control" required>
                <option></option>
                @foreach($products as $product)
                    <option value="{{ $product->id }}"
                            data-name="{{ $product->name }}"
                            data-desc="{{ $product->short_description }}"
                            data-image_source="{{ $product->image_source }}"
                            data-image="{{ $product->image }}"
                            @if($scenario->product_id == $product->id) selected @endif>
                        {{ $product->name }} {{ $product->short_description }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Status: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input name="status" type="checkbox" class="switch" data-on-text="Enabled" data-off-text="Disabled" data-on-color="danger" data-off-color="default" checked>
            </div>
        </div>
    </div>



</fieldset>
