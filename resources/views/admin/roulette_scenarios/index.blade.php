@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Scenarios') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Roulette scenarios</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/roulette-scenarios/drop-counters') }}" title="Drop all current attempts to 0. After that, all enabled scenarios might be played again">><button type="button" class="delete btn bg-warning-400 btn-labeled"><b><i class="fa fa-refresh"></i></b>Drop counters</button></a>
                <a href="{{ url(Config::get('app.admin_prefix').'/roulette-scenario/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Add Scenario</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="scenarios" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>User</th>
                    <th>Case</th>
                    <th>Attempt (current)</th>
                    <th>Product</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        function deleteScenario(id){
            swal({
                title: "Are you sure?",
                text: "Selected scenario will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/roulette-scenario') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#scenarios').DataTable().ajax.reload();

                        swal(
                            "Deleted!",
                            "Scenario has been deleted.",
                            "success"
                        );
                    }
                })
            });
        }

        $(function(){
            tableObj = $('#scenarios').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/roulette-scenario') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id", name: 'scenario.id'},
                    { data: "username", name: 'user.username'},
                    { data: "case_name", name: 'case.name'},
                    { data: "attempt",  searchable: false},
                    { data: "product_name", name: 'product.name'},
                    { data: "status", name: 'scenario.status'},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection