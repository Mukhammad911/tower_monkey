@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Scenario-edit', $scenario->id) !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit Scenario</h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/roulette-scenario').'/'.$scenario->id }}" method="post" enctype='multipart/form-data'>
                        <input name="_method" type="hidden" value="PUT">
                        @include('admin.roulette_scenarios.form')
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $(function(){
            var stattrak_state = '{{ (bool)$scenario->stattrak }}';
            var status_state = '{{ (bool)$scenario->status }}';
            var select_user_id = $('#user_id');

            $("[name='stattrak']").bootstrapSwitch({
                state:stattrak_state
            });
            $("[name='status']").bootstrapSwitch({
                state:status_state
            });

            $('#product_id').select2({
                            allowClear: true,
                            placeholder: "Select product...",
                            escapeMarkup: function (markup) {
                                return markup;
                            },
                            templateResult: function (state) {
                                if (!state.id) { return state.text; }
                                var option = $(state.element);

                                var src = (option.data('image_source') == 'file') ? "{{ asset('images/products') }}/"+option.data('image') : option.data('image');
                                var image = '<img src="' + src + '" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                                var $state = $(
                                    image + '<option style="line-height:40px;" value="' + option.value + '">'+ option.data('name') + ' '+ option.data('desc') + '</option>'
                                );

                                return $state;
                            },
                            templateSelection: function (state) {
                                if (!state.id) { return state.text; }
                                var option = $(state.element);

                                var src = (option.data('image_source') == 'file') ? "{{ asset('images/products') }}/"+option.data('image') : option.data('image');
                                var image = '<img src="' + src + '" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                                var $state = $(
                                    image + '<option value="' + option.value + '">'+ option.data('name') + ' '+ option.data('desc') + '</option>'
                                );

                                return $state;
                            }
                        });

            $('#case_id').select2({
                allowClear: true,
                placeholder: "Select case...",
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);
                    var image =  '<img src="{{ asset('images/cases') }}/'+option.data('image')+'" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                    var $state = $(
                        image + '<option style="line-height:40px;" value="' + option.value + '">'+ option.data('name') + '</option>'
                    );
                    return $state;
                },
                templateSelection: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);
                    var image =  '<img src="{{ asset('images/cases') }}/'+option.data('image')+'" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                    var $state = $(
                        image + '<option value="' + option.value + '">'+ option.data('name') + '</option>'
                    );
                    return $state;
                }
            });

            select_user_id.select2({
                ajax:{
                    type: "GET",
                    url: '{{ url(Config::get('app.admin_prefix').'/user/autocomplete') }}/',
                    delay: 250,
                    dataType: 'json',
                    data:function(params){
                        return {term: params.term}
                    },
                    processResults: function (data) {
                        var results = [];

                        $.each(data, function (index, value) {
                            var o = {};
                            o.id = value.id;
                            o.name = value.name;
                            o.username = value.username;
                            o.image = value.avatar;
                            o.steamid = value.steamid;
                            results.push(o);
                        });

                        return {
                            results: results
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                allowClear: true,
                placeholder: "Select user...",
                templateResult: function  (state) {
                    if (!state.id) { return state.text; }

                    var image =  '<img src="'+ state.image +'" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                    var $state = $(
                        image + '<option style="line-height:40px;" value="'+state.id+'">'+state.steamid+' | '+state.username+' '+state.name+'</option>'
                    );
                    return $state;
                },
                templateSelection: function (state) {
                    console.log(state);
                    if (!state.id) { return state.text; }

                    if (typeof(state.steamid) == 'undefined') { return state.text; }

                    var image =  '<img src="'+ state.image +'" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                    var $state = $(
                        image + '<option value="'+state.id+'">'+state.steamid+' | '+state.username+' '+state.name+'</option>'
                    );
                    return $state;
                }
            });

            select_user_id.append(function(){
                var image =  '<img src="{{ $user->avatar }}" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                var qwe = image + '<option value="{{ $user->id }}" selected>{{ $user->steamid }} | {{ $user->username }} {{ $user->name }}</option>';
                return qwe;
            });
            select_user_id.trigger('change');


        });
    </script>
@endsection