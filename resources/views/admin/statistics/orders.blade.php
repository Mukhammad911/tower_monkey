@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Statistics-Orders') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Orders</h4>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="POST" id="search-form" role="form">
                <div class="form-group col-md-9">
                    <div class="col-sm-1">
                        <label class="text-bold" for="case">Status</label>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="status">
                            <option value="">All</option>
                            @foreach(\App\Models\Order::$statuses as $status)
                                <option value="{{ $status }}">{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <label for="case" class="text-bold">Created At</label>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group input-daterange">
                            <input readonly name="date_from" type="text" class="form-control cursor-pointer" value="">
                            <span class="input-group-btn">
                                <button data-popup="tooltip" data-original-title="Clear" class="btn btn-default clear date-from" type="button">
                                    <span class="icon-x cursor-pointer text-danger"></span>
                                </button>
                            </span>
                            <span class="input-group-addon">to</span>
                            <input readonly name="date_to" type="text" class="form-control cursor-pointer" value="">
                            <span class="input-group-btn">
                                <button data-popup="tooltip" data-original-title="Clear" class="btn btn-default clear date-to" type="button">
                                    <span class="icon-x cursor-pointer text-danger"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="text-bold" for="case">Currency</label>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="currency_id">
                            <option value="">All</option>
                            @foreach($currency as $c)
                                <option value="{{$c->id}}">{{ $c->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="purchases" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Payment System</th>
                <th>Currency</th>
                <th>Amount</th>
                <th>Status</th>
                <th>User</th>
                <th>Created At</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $('.input-daterange input').each(function() {
                $(this).datepicker({
                    format:'dd-M-yyyy',
                    autoclose:true,
                    todayHighlight:true
                });
            });

            $('.clear.date-from').click(function(){
                $("input[name='date_from']").datepicker('clearDates');
            });

            $('.clear.date-to').click(function(){
                $("input[name='date_to']").datepicker('clearDates');
            });
            var table = $('#purchases').DataTable({
                ajax:{
                    url: '{{ url(Config::get('app.admin_prefix').'/statistics/orders-list') }}',
                    data: function (d) {
                        d.date_from = $('[name="date_from"]').val();
                        d.date_to = $('[name="date_to"]').val();
                        d.status = $('[name="status"]').val();
                        d.currency_id = $('[name="currency_id"] option:selected').val();
                    }
                },
                sorting:false,
                searching:false,
                ordering:false,
                info:false,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "payment_system"},
                    { data: "currency"},
                    { data: "amount"},
                    { data: "status"},
                    { data: "user"},
                    { data: "created_at"}
                ],
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: '<"datatable-header"l><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });

            $('#search-form').on('submit', function(e) {
                e.preventDefault();
                table.draw();
            });
        });
    </script>
@endsection