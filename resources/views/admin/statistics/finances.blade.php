@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Statistics') !!}
    </div>
    <div class="page-header-content">
        <div class="row">
            <div class="col-md-4 text-center bg-success">
                <br>
                <i class="icon-plus-circle2" style="font-size: 50px;"></i>
                <p>Games Income</p>
                <p style="font-size: 30px;">${{ number_format($total_income, 2) }}</p>
                <br><br>
            </div>

            <div class="col-md-4 text-center bg-danger">
                <br>
                <i class="icon-minus-circle2" style="font-size: 50px;"></i>
                <p>Games Expense</p>
                <p style="font-size: 30px;">${{ number_format($total_expense, 2) }}</p>
                <br><br>
            </div>

            <div class="col-md-4 text-center bg-primary">
                <br>
                <i class="icon-wallet" style="font-size: 50px;"></i>
                <p>Games Balance</p>
                <p style="font-size: 30px;">${{ number_format($total_income - $total_expense, 2) }}</p>
                <br><br>
            </div>
        </div>
    </div>


    <div class="panel panel-white col-md-12">
        <div id="chart_current_profit_percent" style="width:100%; height:300px;"></div>
    </div>

    <div class="panel panel-white col-md-6">
        <div id="chart_income" style="width:100%; height:300px;"></div>
    </div>

    <div class="panel panel-white col-md-6">
        <div id="chart_expense" style="width:100%; height:300px;"></div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">

    </div>
@endsection




@section('js')
<script>
    $(function () {
        Highcharts.chart('chart_current_profit_percent', {
            chart: {
                type: 'line',
                zoomType: 'x'
            },
            title: {
                text: 'Games current profit'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '%'
                }
            },
            series: [{
                name: 'games current profit',
                data: <?= json_encode($current_profit_percent_data) ?>,
                color: '#2196F3',
                marker: {
                    enabled: true
                }
            }]
        });

        Highcharts.chart('chart_income', {
            chart: {
                type: 'coloredline',
                zoomType: 'x'
            },
            title: {
                text: 'Games income'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '$'
                }
            },
            series: [{
                name: 'games income',
                data: <?= json_encode($income_data) ?>,
                color: '#4CAF50',
                marker: {
                    enabled: true
                }
            }]
        });

        Highcharts.chart('chart_expense', {
            chart: {
                type: 'coloredline',
                zoomType: 'x'
            },
            title: {
                text: 'Games expense'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: '$'
                }
            },
            series: [{
                name: 'games expense',
                data: <?= json_encode($expense_data) ?>,
                color: '#F44336',
                marker: {
                    enabled: true
                }
            }]
        });
    });
</script>
@endsection