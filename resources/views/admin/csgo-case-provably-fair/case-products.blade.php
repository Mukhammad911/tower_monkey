@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Products') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Chances for Case <span>{{ $case->name }}</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/fair-csgo-case') }}" type="button" class="btn bg-primary btn-labeled heading-btn btn-sm">
                    <b><i class="icon-arrow-left13"></i></b>Back
                </a>
            </div>
        </div>
    </div>

    <div class="fix-menu-panel">
        <div class="fix-panel-div">
            <p>Rates Sum: </p>
            <span class="rates-sum"></span>
        </div>

        <div class="fix-panel-div">
            <p>Case Recommended Price: </p>
            <span class="case-recom-price"></span>
        </div>

        <a href="" type="button" class="btn bg-success save-fair-all" disabled>
            <b><i class="icon-arrow-left13"></i></b>Submit
        </a>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="products" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Image</th>
                <th>Chance</th>
                <th>Start</th>
                <th>End</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var caseId         = '{{ $case->id }}';
        var saveProductChance  = '{{ url(Config::get('app.admin_prefix').'/fair-csgo-case/save-chance') }}';
        var productsData   = '{{ url(Config::get('app.admin_prefix').'/fair-csgo-case/case-products/'.$case->id) }}';
    </script>

    <script type="text/javascript">

        function saveChance(id, case_id){

            var chance = $('.chance-' + id).val();
            var start = $('.start-' + id).val();
            var end = $('.end-' + id).val();

            console.log(chance + ' ' + start + ' ' + end);

            $.ajax({
                type: "GET",
                dataType: "json",
                url: saveProductChance,
                data:{
                    id:id,
                    case_id:case_id,
                    chance:chance,
                    start:start,
                    end:end
                }
            })
                .success(function(success){
                    if(success){
                        $('#products').DataTable().ajax.reload();
                        swal(
                            "Chance Changed!",
                            "Product has been changed.",
                            "success"
                        );
                    }
                })
                .error(function(error){
                    var err = JSON.parse(error.responseText);

                    if(err.chance){
                        $.jGrowl(err.chance[0], {
                            header: '<img src="/images/beastskins/icon-error.png"><span>Error<span>',
                            theme: 'bg-danger',
                            life: 7000,
                            position: 'bottom-right'
                        });
                    }

                    if(err.start){
                        $.jGrowl(err.start[0], {
                            header: '<img src="/images/beastskins/icon-error.png"><span>Error<span>',
                            theme: 'bg-danger',
                            life: 7000,
                            position: 'bottom-right'
                        });
                    }

                    if(err.end){
                        $.jGrowl(err.end[0], {
                            header: '<img src="/images/beastskins/icon-error.png"><span>Error<span>',
                            theme: 'bg-danger',
                            life: 7000,
                            position: 'bottom-right'
                        });
                    }
                });
        }

        function toFixFloat(x) {
            return parseFloat((x).toFixed(2));
        }

        function initFixPanel() {
            var rate_sum = 0;
            var after_case_price = 0;
            var min_range = 0;
            var max_range = 0;
            var range_position = 0;

            $('.rates-class').each(function() {
                var id = parseInt($(this).data('id'));
                var price = parseFloat($(this).data('price'));
                var chance = parseFloat($(this).val());

                min_range = range_position + 1;
                max_range = range_position + (chance * 100000) / 100;

                $('.start-' + id).val(min_range);
                $('.end-' + id).val(max_range);

                after_case_price += (price * chance) / 100;

                rate_sum += chance;

                range_position += max_range - range_position;
            });

            $('.rates-sum').text(toFixFloat(rate_sum) + '%');

            $('.case-recom-price').text('');
            $('.case-recom-price').append(toFixFloat(after_case_price) + '$');

            if (saveAvailable(false) == false)
            {
                $('.save-fair-all').attr('disabled','disabled');
            }
            else
            {
                $('.save-fair-all').removeAttr('disabled');
            }
        }

        function saveAvailable(data) {
            var rate_sum = 0;
            var count = 0;
            var data_array = [];

            $('.rates-class').each(function() {
                var id = parseInt($(this).data('id'));
                var case_id = $(this).data('case-id');
                var chance = parseFloat($(this).val());
                var start = $('.start-' + id).val();
                var end = $('.end-' + id).val();

                data_array.push({
                    id: id,
                    case_id: case_id,
                    chance: chance,
                    start: start,
                    end: end
                });

                rate_sum += chance;

                if (chance == 0){
                    count++;
                }
            });

            if (toFixFloat(rate_sum) != 100 || count > 0)
            {
                return false;
            }

            if (data == true)
            {
                return data_array;
            }
            else
            {
                return true;
            }
        }

        $(function(){

            $(document).on('change', 'input.rates-class', function () {
                var rate_sum = 0;
                var after_case_price = 0;
                var min_range = 0;
                var max_range = 0;
                var range_position = 0;

                $('.rates-class').each(function() {
                    var id = parseInt($(this).data('id'));
                    var price = parseFloat($(this).data('price'));
                    var chance = parseFloat($(this).val());

                    min_range = range_position + 1;
                    max_range = range_position + (chance * 100000) / 100;

                    $('.start-' + id).val(min_range);
                    $('.end-' + id).val(max_range);

                    after_case_price += (price * chance) / 100;

                    rate_sum += chance;

                    range_position += max_range - range_position;
                });

                $('.rates-sum').text(toFixFloat(rate_sum) + '%');

                $('.case-recom-price').text('');
                $('.case-recom-price').append(toFixFloat(after_case_price) + '$');

                if (saveAvailable(false) == false)
                {
                    $('.save-fair-all').attr('disabled','disabled');
                }
                else
                {
                    $('.save-fair-all').removeAttr('disabled');
                }

            });

            $(document).on('click', '.save-fair-all', function (e) {
                e.preventDefault();

                var data_send = saveAvailable(true);

                if (data_send == false)
                {
                    $.jGrowl('Can not save check values for rate sum 100% and all items chances must be more than 0', {
                        header: '<img src="/images/beastskins/icon-error.png"><span>Error<span>',
                        theme: 'bg-danger',
                        life: 7000,
                        position: 'bottom-right'
                    });

                    return;
                }

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: saveProductChance,
                    data:{
                        data: data_send
                    }
                })
                .success(function(success){
                    if(success){
                        $('#products').DataTable().ajax.reload();
                        swal(
                            "Chance Changed!",
                            "Product has been changed.",
                            "success"
                        );
                    }
                })
                .error(function(error){
                    console.log(error);
                });

            });

            var table = $('#products').DataTable({
                ajax:{
                    url: productsData,
                    data:{
                        case_id:caseId
                    }
                },
                initComplete: function( settings, json){
                    // call your function here
                    initFixPanel();
                },
                autoWidth: true,
                sorting:false,
                searching:false,
                processing: true,
                serverSide: true,
                ordering:false,
                lengthMenu: [ [-1], ["All"] ],
                columns: [
                    { data: "id"},
                    { data: "name"},
                    { data: "short_description"},
                    { data: "price"},
                    { data: "image"},
                    { data: "chance"},
                    { data: "start"},
                    { data: "end"}
                ]
            });

            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });
        });
    </script>
@endsection
