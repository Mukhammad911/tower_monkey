<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Parameter<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="text" class="course form-control" name="key" value="{{ $setting->key or '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">Value<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="text" class="course form-control" name="value" value="{{ $setting->value or '' }}">
            </div>
        </div>
    </div>
</fieldset>