<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Partner: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="refer_id" name="refer_id" class="form-control" required>
                <option></option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Affiliate user: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="user_id" name="user_id" class="form-control" required>
                <option></option>
            </select>
        </div>
    </div>

</fieldset>
