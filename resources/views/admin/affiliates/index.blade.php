@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Affiliates') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Affiliates</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/affiliate/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Add Affiliate</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="affiliates" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Partner</th>
                    <th>Affiliated user</th>

                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        function deleteAffiliate(id){
            swal({
                title: "Are you sure?",
                text: "Selected affiliate will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/affiliate') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#affiliates').DataTable().ajax.reload();

                        swal(
                            "Deleted!",
                            "Affiliate has been deleted.",
                            "success"
                        );
                    }
                })
            });
        }

        $(function(){
            tableObj = $('#affiliates').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/affiliate') }}',
                searching:true,
                ordering: true,
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: 'affiliate_id', name: 'affiliate.id'},
                    { data: 'refer_username', name: 'refer.username'},
                    { data: 'user_username', name: 'user.username'},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection