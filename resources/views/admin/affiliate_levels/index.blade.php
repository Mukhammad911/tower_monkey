@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Affiliate-levels') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Affiliate Levels</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/affiliate-level/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Add Affiliate level</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="affiliate_levels" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>Level</th>
                    <th>Border amount</th>
                    <th>Affiliate percent</th>
                    <th>Refer Percent</th>
                    <th>Total users</th>

                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        function deleteScenario(id){
            swal({
                title: "Are you sure?",
                text: "Selected affiliate level will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/affiliate-levels') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#affiliate_levels').DataTable().ajax.reload();

                        swal(
                            "Deleted!",
                            "Affiliate level has been deleted.",
                            "success"
                        );
                    }
                })
            });
        }

        $(function(){
            tableObj = $('#affiliate_levels').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/affiliate-level') }}',
                searching:true,
                ordering: true,
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "level", name: 'level.id'},
                    { data: "border_amount", name: 'level.border_amount'},
                    { data: "bonus_amount", name: 'level.bonus_amount'},
                    { data: "percent", name: 'level.percent'},
                    { data: "users", orderable: false, searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection