<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Level: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input type="number" name="level" class="form-control" value="{{ $affiliate_level->level or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Border amount: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="input-group">
                 <span class="input-group-addon">$</span>
                <input type="number" name="border_amount" class="form-control" value="{{ $affiliate_level->border_amount or '' }}" placeholder="0.00" required>
             </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Bonus amount: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="number" name="bonus_amount" class="form-control" value="{{ $affiliate_level->bonus_amount or '' }}" placeholder="0.00" required>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Percent: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="input-group">
                <input type="number" name="percent" class="form-control" value="{{ $affiliate_level->percent or '' }}" placeholder="0" required>
                <span class="input-group-addon">%</span>
            </div>
        </div>
    </div>



</fieldset>
