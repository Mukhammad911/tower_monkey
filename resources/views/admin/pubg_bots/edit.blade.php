@extends('admin/template')

@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-brain position-left"></i> <span class="text-semibold">Pubg Bots</span> - Edit bot</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'pubg_bots') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-exit3"></i></b> Back</a>
        </div>
    </div>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('bots', 'Edit') !!}
    </div>
@endsection

@section('css')
@endsection

@section('content')
    @if(isset($bot->secret))
        <div class="alert alert-danger alert-styled-left alert-bordered">
            <span class="text-semibold">Danger!</span> Changing activated bot will make it unusable up to 7 days.
        </div>
    @endif

    <h5 class="content-group text-semibold">
        <mark class="bg-primary">{{ $bot->steam_login }}</mark> trade bot
    </h5>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-body">
                <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/pubg_bots/'.$bot->id) }}" method="post">
                    <input type="hidden" name="_method" value="put">
                    @include('admin.pubg_bots.form', $bot)

                    <div class="text-right">
                        <button type="submit" class="btn btn-success">Update <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title">Steam trade bot status</h6>
                </div>
                <div class="panel-body">
                    <p>
                        <strong>Guard status:</strong>
                        @if(isset($bot->secret))
                            <span class="label label-success position-right">Activated</span>
                        @else
                            <span class="label label-danger position-right">Inactive</span>
                        @endif
                    </p>
                </div>
            </div>

            @include('admin.pubg_bots.delete', $bot)
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
@endsection
