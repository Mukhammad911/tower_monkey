@extends('admin/template')

@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-brain position-left"></i> <span class="text-semibold">Pubg Bots</span> - Add new bot</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/pubg_bots') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-exit3"></i></b> Back</a>
        </div>
    </div>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('bots', 'Create') !!}
    </div>
@endsection

@section('css')
@endsection

@section('content')
    <div class="panel panel-body">

        <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/pubg_bots') }}" method="post">
            @include('admin.bots.form')
            <div class="text-right">
                <button type="submit" class="btn btn-success">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </form>

    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
@endsection
