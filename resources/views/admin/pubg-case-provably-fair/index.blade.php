@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Cases') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Provably Fair PUBG Cases</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>

@endsection

@section('content')
    <div class="panel panel-white">
        <table id="cases" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var casesData = '{{ url(Config::get('app.admin_prefix').'/fair-pubg') }}';
    </script>
    <script src="/assets/js/case-fair.js"></script>
@endsection