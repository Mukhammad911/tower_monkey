@extends('admin/template')

@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-brain position-left"></i> <span class="text-semibold">Prices</span> - Index
                <small>Purchase price manager</small>
            </h4>
        </div>
        <!--div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/bots/create') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-user-plus"></i></b> Add new price</a>
        </div-->
    </div>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('generic', ['Prices'], 'admin/prices') !!}
    </div>
@endsection

@section('content')
    <h5 class="content-group text-semibold">
        All weapon purchase prices according to quality
        <small class="display-block">Inline price update for any weapon.</small>
    </h5>

    <div class="panel panel-white">
        <div class="panel-heading">
            <h5 class="panel-title">Basic datatable</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            The <code>DataTables</code> is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function. Searching, ordering, paging etc goodness will be immediately added to the table, as shown in this example. <strong>Datatables support all available table styling.</strong>
        </div>

        <table class="table datatable-basic">
            <thead>
            <tr class="bg-primary-700">
                <th>Product</th>
                <th>Quality</th>
                <th>Price</th>
            </tr>
            </thead>
        </table>
    </div>

    <div id="codeModal" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Bot Authentication Guard Code</h6>
                </div>

                <div class="modal-body">
                    <h2 class="text-semibold" id="botCode">KG42A</h2>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var ADMIN_PREFIX = '{{ Config::get('app.admin_prefix') }}';
    </script>
    <script src="/assets/js/plugins/notifications/bootbox.min.js"></script>
    <script src="/assets/js/plugins/forms/inputs/switchery.min.js"></script>
    <script src="/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/assets/js/bot.js"></script>
    <script src="/assets/js/datatable-defaults.js"></script>

    <script>
        $(function() {
            $('.datatable-basic').DataTable({
                pageLength: 50,
                processing: true,
                serverSide: true,
                ajax: {{ route('prices.data') }}',
                columns: [
                    { data: 'product_id', name: 'product_id' },
                    { data: 'product.name', name: 'product.name' },
                    { data: 'product.class', name: 'product.class' },
                    { data: 'quality', name: 'quality'},
                    { data: 'price', name: 'price'}
                ]
            });

            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });
        });
    </script>
@endsection