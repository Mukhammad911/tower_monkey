<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">User: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <select id="user_id" name="user_id" class="form-control" required>
                <option></option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Attempt: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="attempt" type="number" max="100" class="form-control" value="{{ $scenario->attempt or '' }}" required>
        </div>
    </div>

    <div class="form-group">
            <label class="control-label col-lg-2 text-nowrap">Level: <span class="text-danger">*</span></label>
            <div class="col-lg-10">
                <select name="level" class="form-control" required="required">
                        <?php
                        $levels = [
                            'anyone',
                            'easy',
                            'medium',
                            'hard',
                        ];
                        ?>
                        @foreach($levels as $level)
                         <option value="{{ $level }}" @if($scenario->level == $level) selected @endif>{{ $level }}</option>
                        @endforeach
                </select>
            </div>
        </div>

    <div class="form-group">
        <label class="control-label col-lg-4 text-nowrap">Losing step (which step should be losing): <span class="text-danger">*</span></label>
        <div class="col-lg-8">
            <input name="step" type="number" max="100" class="form-control" value="{{ $scenario->step or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Status: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input name="status" type="checkbox" class="switch" data-on-text="Enabled" data-off-text="Disabled" data-on-color="danger" data-off-color="default" checked>
            </div>
        </div>
    </div>



</fieldset>
