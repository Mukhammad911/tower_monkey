<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign In</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/admin-design.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->


    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/core/app.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <!-- /theme JS files -->

</head>

<body class="login-container">

<!-- Main navbar -->

<!-- /main navbar -->


<!-- Page container -->
<div class="page-container page-admin">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Simple login form -->
                <form method="post" action="{{ url(Config::get('app.admin_prefix').'/login') }}">
                    <div id="login-admin" class="panel panel-body login-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="text-center">
                            <div class="icon-admin"></div>
                            <h5 class="content-group">Login to your account
                                <small class="display-block">Enter your credentials below</small>
                            </h5>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            @if($errors->get('email'))
                                <label id="password-error" class="validation-error-label" for="password">
                                    {{ $errors->get('email')[0] }}
                                </label>
                            @endif
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                            @if($errors->get('password'))
                                <label id="password-error" class="validation-error-label" for="password">
                                    {{ $errors->get('password')[0] }}
                                </label>
                            @endif
                            @if($errors->get('incorrect'))
                                <label id="incorrect-error" class="validation-error-label" for="password">
                                    {{ $errors->get('incorrect')[0] }}
                                </label>
                            @endif
                        </div>
                        <div class="form-group login-options">
                            <label class="checkbox-inline">
                                <input name="remember_token" type="checkbox" class="styled">
                                Remember
                            </label>
                        </div>
                        <div class="frm-button">
                            <button type="submit" class="custom_btn_save_bal">Sign in </button>
                        </div>

                        <!--<div class="text-center">
                            <a href="login_password_recover.html">Forgot password?</a>
                        </div>-->
                    </div>
                </form>
                <!-- /simple login form -->


                <!-- Footer -->
                <div class="footer text-muted text-center">
                    &copy; 2019 Csgotower.
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
<script>
    $(function(){
        $('.styled').uniform();
    })
</script>
</body>
</html>
