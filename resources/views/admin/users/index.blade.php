@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Users') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Users</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url('admin/user/create') }}"><button type="button" class="delete btn bg-success-400 btn-labeled"><b><i class="fa fa-plus"></i></b>Add User</button></a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="users" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Ava</th>
                    <!--<th>Role</th>-->
                    <th>Username</th>
                    <th>Balance</th>
                    <th>Profit</th>
                    <th>Drops</th>
                    <th>Active</th>
                    <th>Youtuber</th>
                    <th>Ban top</th>
                    <th>Ban drop</th>
                    <th>Ban withdraw</th>

                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(document).on('click', '.login_like_user', function(e){
            location = '{{ url(Config::get('app.admin_prefix').'/login-like-user') }}'+'/'+$(this).data('user_id');
        });

        function deleteUser(id){
            swal({
                title: "Are you sure?",
                text: "Selected user will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: '{{ url(Config::get('app.admin_prefix').'/user') }}'+'/'+id
                })
                .success(function(success){
                    if(success){
                        $('#users').DataTable().ajax.reload();
                        swal(
                            "Deleted!",
                            "User has been deleted.",
                            "success"
                        );
                    }
                })
            });
        }

        $(function(){
            tableObj = $('#users').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/user') }}',
                searching: true,
                ordering: true,
                order: [[0, 'desc']],
                info: true,
                paging: true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id", name: 'user.id'},
                    { data: "avatar", name: 'user.avatar', render: function (data) {
                        return '<img style="max-height:40px; border-radius: 40px;" src="'+data+'">';
                    }},
                    { data: "username", name: 'user.username'},
                    { data: "balance", searchable: false},
                    { data: "profit", name: 'user.profit'},
                    { data: "count_drops", name: 'user.count_drops'},
                    { data: "active", name: 'user.active'},
                    { data: "youtuber", name: 'user.youtuber'},
                    { data: "top_banned", name: 'user.top_banned'},
                    { data: "drop_banned", name: 'user.drop_banned'},
                    { data: "withdraw_banned", name: 'user.withdraw_banned'},
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection