@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Profile') !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">My Profile</h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/profile/update') }}" method="post" enctype='multipart/form-data'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="active" value="on">
                        @include('admin.users.form')

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
@endsection