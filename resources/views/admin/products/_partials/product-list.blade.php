@foreach($products as $item)
    <div class="col-md-3 col-sm-6 col-xs-6 table-bordered product-item">
        <div class="text-center img-wrapper">
            @if($item->image_source == 'url')
                <img src="{{ $item->image }}">
            @else
                <img src="{{ asset('images/products') }}/{{ $item->image }}" alt="Image" style="width: 110px;">
            @endif
        </div>
        <div class="text-center product-checker">
            <input  @if(in_array($item->id, $case_product)) {{ 'checked' }} @endif  value="{{ $item->id }}" class="styled weapon-item" type="checkbox">
        </div>
        <div class="clearfix"></div>
        <div class="product-desc {{ $item->class }}">
            <span class="text-bold">
                {{ (Lang::has('weapon.'.$item->name)) ? trans('weapon.'.$item->name) : $item->name}}
            </span>
            <span class="descr" style="display:block; text-transform: capitalize">{{ trans('weapon.'.$item->short_description) }}</span>
        </div>
    </div>
@endforeach