@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Products') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Products</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/product/create') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-plus-circle2"></i></b> Add new product</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <div class="panel-body">
            <form method="POST" id="search-form" role="form">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="case">Case:</label>
                        <select class="form-control" id="case" name="case"></select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="case">Class:</label>
                        <select class="form-control" id="class" name="class">
                            <option></option>
                            @foreach($classList as $class)
                                <option value="{{ $class }}">{{ $class }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="case">Name/Description:</label>
                        <select class="form-control" id="id" name="id">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label>Price:</label>
                        <input id="min" type="text" class="form-control" placeholder="min">
                    </div>
                    <div class="form-group col-md-1">
                        <label>&nbsp;</label>
                        <input id="max" type="text" class="form-control" placeholder="max">
                    </div>
                    <div class="form-group col-md-1">
                        <button type="submit" class="btn btn-info" style="margin-top: 27px;">Search</button>
                    </div>
                </div>
            </form>
        </div>
        <table id="products" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Class</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var ADMIN_PREFIX = '{{ Config::get('app.admin_prefix') }}';
        var productsData = '{{ url(Config::get('app.admin_prefix').'/product') }}';
        var productDelete = '{{ url(Config::get('app.admin_prefix').'/product') }}';
        var caseSearch = '{{ url(Config::get('app.admin_prefix').'/product-case/autocomplete') }}';
        var productSearch = '{{ url(Config::get('app.admin_prefix').'/product/autocomplete') }}';
        var assetPath = '{{ asset('images/products') }}';
    </script>
    <script src="/assets/js/products.js"></script>
@endsection
