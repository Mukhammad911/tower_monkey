@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Product-create') !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Create Product</h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/product') }}" method="post" enctype='multipart/form-data'>
                        @include('admin.products.form')
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $(function(){
            var classList = JSON.parse('{!! json_encode($classList) !!}'),
                classStr = classList.toString().replace(/,/g, ' ');

            $('.image_source').change(function(){
                $('#image-source-block .url-source, #image-source-block .image').toggleClass('hidden');
            });

            $('select[name="class"]').change(function(){
                $('.product-class-temp').removeClass(classStr);
                $('.product-class-temp').addClass($(this).val())
            });

            $('.existing-weapon').select2({
                placeholder: "Select Weapon",
            });

            $('.descriptions').select2({
                placeholder: "Select Description",
                allowClear: true
            });

            $("select").on("select2:close", function (e) {
                $(this).valid();
            });

            //$('.price').numberMask();
        });
    </script>
@endsection