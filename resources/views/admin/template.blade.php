
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Dashboard</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/jquery-ui/jquery-ui-1.10.0.custom.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/core/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/admin.style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/product-class.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/charts/highcharts.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/charts/multicolor_series.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/validation/additional_methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/inputs/jquery.numberMask.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/components_modals.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/sliders/ion_rangeslider.min.js') }}"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a href="{{ url('/') }}"><img style="width: 113px;height: auto;margin: auto;margin-left: 10px;padding-bottom: 6px;" src="{{ asset('images/version2/') }}" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::user()->avatar)
                            <img src="{{ asset('images/avatars').'/'.Auth::user()->avatar }}" alt="Avatar">
                        @else
                            <img src="{{ asset('images/placeholder.jpg') }}" alt="Avatar">
                        @endif
                        <span>
                            {{ Auth::user()->name }}
                        </span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ url(Config::get('app.admin_prefix').'/profile') }}"><i class="icon-user-plus"></i> My profile</a></li>
                        <li><a href="{{ url('logout') }}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="{{ url(Config::get('app.admin_prefix').'/profile') }}" class="media-left">
                                @if(Auth::user()->avatar)
                                    <img src="{{ asset('images/avatars').'/'.Auth::user()->avatar }}" alt="Avatar" class="img-circle img-sm">
                                @else
                                    <img src="{{ asset('images/placeholder.jpg') }}" alt="Avatar" class="img-circle img-sm">
                                @endif
                            </a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">
                                    {{ Auth::user()->name }} ({{ Auth::user()->email }})
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main">

                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="home">
                                <a href="{{ url('/'.Config::get('app.admin_prefix')) }}">
                                <i class="icon-home4"></i>
                                <span>Dashboard</span></a>
                            </li>
                            @role('admin')
                            <li>
                                <a href="{{ url('/'.Config::get('app.admin_prefix').'/users') }}">
                                    <i class="icon-users4"></i><span>Users</span></a>
                            </li>
                            {{--<li>
                                <a href="{{ url('/'.Config::get('app.admin_prefix').'/youtube') }}">
                                    <i class="icon-users4"></i><span>Youtube</span></a>
                            </li> --}}

                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-cash"></i><span>Finances</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/operations') }}">
                                            <i class="icon-coins"></i><span>Operations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/orders') }}">
                                            <i class="icon-file-plus"></i><span>Orders</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/withdrawals') }}">
                                            <i class="icon-file-minus"></i><span>Withdrawals</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/imbalances') }}">
                                            <i class="icon-calculator3"></i><span>Imbalances</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/categories') }}">
                                            <i class="icon-cabinet"></i><span>Categories</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/markets') }}">
                                            <i class="icon-store"></i><span>Market</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-rating3"></i><span>Affiliate program</span></a>
                                <ul>
                                    <li>
                                        <a href="{{ url(Config::get('app.admin_prefix').'/affiliates') }}">
                                            <i class="icon-tree6"></i><span>Affiliates</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url(Config::get('app.admin_prefix').'/affiliate-levels') }}">
                                            <i class="icon-podium"></i><span>Affiliate levels</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-film"></i><span>CSGO  Cases</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo_case_bots') }}">
                                            <i class="icon-brain"></i><span>Bots</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/cases') }}">
                                            <i class="icon-drawer"></i><span>Cases</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/products') }}">
                                            <i class="icon-stack3"></i><span>Products</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-case-purchases') }}">
                                            <i class="icon-basket"></i><span>Purchases</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-case-purchases-history') }}">
                                            <i class="icon-basket"></i><span>Purchases History</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/fair-csgo-case') }}">
                                            <i class="icon-drawer"></i><span>Provably Fair</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-case-available-products') }}">
                                            <i class="icon-qrcode"></i><span>Available products</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-case-trades') }}">
                                            <i class="icon-stack3"></i><span>TRADES</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-film"></i><span>PUBG  Cases</span>
                                </a>
                                <ul>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg_case_bots') }}">
                                            <i class="icon-brain"></i><span>Bots</span>
                                        </a>
                                    </li>


                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg/cases') }}">
                                            <i class="icon-drawer"></i><span>Cases</span>
                                        </a>
                                    </li>


                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-products') }}">
                                            <i class="icon-stack3"></i><span>Products</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-case-purchases') }}">
                                            <i class="icon-basket"></i><span>Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-case-purchases-history') }}">
                                            <i class="icon-basket"></i><span>Purchases History</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/fair-pubg-case') }}">
                                            <i class="icon-drawer"></i><span>Provably Fair</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-case-available-products') }}">
                                            <i class="icon-qrcode"></i><span>Available products</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-case-trades') }}">
                                            <i class="icon-stack3"></i><span>TRADES</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-film"></i><span>CSGO MARKET</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/bots') }}">
                                            <i class="icon-brain"></i><span>Bots</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/edit-csgo-available-products') }}">
                                            <i class="icon-stack3"></i><span>Prices</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-trades') }}">
                                            <i class="icon-stack3"></i><span>TRADES</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/csgo-purchase') }}">
                                            <i class="icon-basket"></i><span>Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/available-products') }}">
                                            <i class="icon-qrcode"></i><span>Available products</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-film"></i><span>DOTA2 MARKET</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/dota_bots') }}">
                                            <i class="icon-brain"></i><span>Bots</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/edit-dota-available-products') }}">
                                            <i class="icon-stack3"></i><span>Prices</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/dota-trades') }}">
                                            <i class="icon-stack3"></i><span>TRADES</span>
                                        </a>
                                    </li>


                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/dota-purchase') }}">
                                            <i class="icon-basket"></i><span>Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/dota-available-products') }}">
                                            <i class="icon-qrcode"></i><span>Available products</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-film"></i><span>PUBG MARKET</span>
                                </a>
                                <ul>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg_bots') }}">
                                            <i class="icon-brain"></i><span>Bots</span>
                                        </a>
                                    </li>


                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/edit-pubg-available-products') }}">
                                            <i class="icon-stack3"></i><span>Prices</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-trades') }}">
                                            <i class="icon-stack3"></i><span>TRADES</span>
                                        </a>
                                    </li>


                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-purchase') }}">
                                            <i class="icon-basket"></i><span>Purchases</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/'.Config::get('app.admin_prefix').'/pubg-available-products') }}">
                                            <i class="icon-qrcode"></i><span>Available products</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>


                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-dice"></i><span>Games</span>
                                </a>
                                <ul>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-film"></i><span>"Roulette"</span>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/roulettes') }}"><i class="icon-finish"></i>Games</a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/roulette-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/'.Config::get('app.admin_prefix').'/roulette-scenarios') }}">
                                                    <i class="icon-equalizer2"></i><span>Scenarios</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/roulette-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-arrow-right16"></i><span>"Upgrade"</span>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/upgrades') }}"><i class="icon-finish"></i>Games</a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/upgrade-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/'.Config::get('app.admin_prefix').'/upgrade-scenarios') }}">
                                                    <i class="icon-equalizer2"></i><span>Scenarios</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/upgrade-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-traffic-cone"></i><span>"Tower"</span>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/towers') }}"><i class="icon-finish"></i>Games</a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/tower-cells') }}"><i class="icon-list-numbered"></i>Turns</a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/tower-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/'.Config::get('app.admin_prefix').'/tower-scenarios') }}">
                                                    <i class="icon-equalizer2"></i><span>Scenarios</span></a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/tower-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-film"></i><span>"Roulette CSGO CASE"</span>
                                        </a>
                                        <ul>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/csgo-case-roulette-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/csgo-case-roulette-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-film"></i><span>"Roulette DOTA2 CASE"</span>
                                        </a>
                                        <ul>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/dota-case-roulette-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/dota-case-roulette-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-film"></i><span>"Roulette PUBG CASE"</span>
                                        </a>
                                        <ul>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/pubg-case-roulette-statistics') }}"><i class="icon-chart"></i>Statistics</a>
                                            </li>

                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/pubg-case-roulette-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="nav-parent">
                                        <a href="#">
                                            <i class="icon-eyedropper2"></i><span>Battles</span>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/battles') }}"><i class="icon-finish"></i>Games</a>
                                            </li>
                                            <li>
                                                <a href="{{ url(Config::get('app.admin_prefix').'/battle-settings') }}"><i class="icon-cogs"></i>Settings</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-parent">
                                <a href="#">
                                    <i class="icon-stats-dots"></i><span>Statistics</span>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url(Config::get('app.admin_prefix').'/statistics/orders') }}">Orders</a>
                                    </li>
                                    <li>
                                        <a href="{{ url(Config::get('app.admin_prefix').'/statistics/finances') }}">Finances</a>
                                    </li>
                                    <li>
                                        <a href="{{ url(Config::get('app.admin_prefix').'/statistics/games') }}">Games</a>
                                    </li>
                                </ul>
                            </li>

                                <li class="nav-parent">
                                    <a href="#">
                                        <i class="icon-wrench"></i><span>Settings</span>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="{{ url(Config::get('app.admin_prefix').'/settings/general') }}">General</a>
                                        </li>
                                        <li>
                                            <a href="{{ url(Config::get('app.admin_prefix').'/settings/localization') }}">Localization</a>
                                        </li>
                                        <li>
                                            <a href="{{ url(Config::get('app.admin_prefix').'/settings/clear-project') }}"><i class="icon-alert"></i>Clear project</a>
                                        </li>
                                    </ul>
                                </li>
                            @endrole
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default page-header-xs">
                @yield('breadcrumbs')
                @yield('header')
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                @foreach (['danger', 'warning', 'success', 'info'] as $flash_status)
                    @if( Session::has($flash_status) )
                    <div class="alert alert-{{ $flash_status }} alert-styled-left alert-arrow-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">{{ ucfirst($flash_status ) }}!</span> {{ session($flash_status) }}
                    </div>
                    @endif
                @endforeach

                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-styled-left alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                            <span class="text-semibold">Error!</span> {{ $error }}.
                        </div>
                    @endforeach
                @endif
                @yield('content')
            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    $(function(){
        $('.styled').uniform();

        if(location.pathname == '/{{ Config::get('app.admin_prefix') }}'){
            $('ul.navigation li.home').addClass('active');
        }
        else {
            $('ul.navigation li a').each(function(){
                if($(this).attr('href').indexOf(location.pathname)>0) {
                    $(this).parents('li,ul').addClass('active').show();
                }
            });
        }
    })
</script>
@yield('js-localization.head')
@yield('js')
</body>
</html>
