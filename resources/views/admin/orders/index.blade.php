@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Orders') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Orders</span></h4>
        </div>

    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="users" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>Inside ID</th>
                    <th></th>
                    <th>User</th>
                    <th>Transaction ID</th>
                    <th>Amount</th>
                    <th>status</th>
                    <th>Updated</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#users').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/order') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id", name: 'order.id'},
                    { data: "steamid", name: 'user.steamid', visible: false},
                    { data: "username", name: 'user.username'},
                    { data: "transaction_id", name: 'order.transaction_id'},
                    { data: "amount", name: 'order.amount'},
                    { data: "status", name: 'order.status'},
                    { data: "updated_at", name: 'order.updated_at'}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection