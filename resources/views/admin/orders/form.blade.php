<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Avatar: </label>
        <div class="media no-margin-top col-lg-10">
            <div class="media-left">
                @if($user->avatar)
                    <img src="{{ $user->avatar }}" style="width: 58px; height: 58px; border-radius: 2px;" alt="Avatar">
                @else
                    <img src="{{ asset('images/placeholder.jpg') }}" alt="Avatar" style="width: 58px; height: 58px; border-radius: 2px;">
                @endif
            </div>

            <div class="media-body">
                <input name="avatar" type="file" class="file-styled">
                <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">User name: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="username" type="text" class="form-control" value="{{ $user->username or '' }}" required="required">
        </div>
    </div>

    <!--<div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Password: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="password" type="password" class="form-control" minlength="8" @if(!$user->id) required="required" @endif>
        </div>
    </div>-->

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Email: <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input name="email" value="{{ $user->email }}" type="text" class="form-control email">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Balance: <span class="text-danger">*</span></label>
        <div class="col-lg-3">
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input name="balance_usd" value="<?php foreach($user->balance as $balance){ if($balance->currency_id == 1 && $balance->type == 0) echo $balance->summ; } ?>" type="text" class="form-control number" required="required" placeholder="0">
            </div>
        </div>

        <div class="col-lg-3">
            <div class="input-group">
                <span class="input-group-addon">P.</span>
                <input name="balance_rur" value="<?php foreach($user->balance as $balance){ if($balance->currency_id == 7 && $balance->type == 0) echo $balance->summ; } ?>" type="text" class="form-control number" required="required" placeholder="0">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Top ban: </label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input name="top_banned" type="checkbox" class="switch" data-on-text="Yes" data-off-text="No" data-on-color="danger" data-off-color="default" @if($user->top_banned) checked @endif>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Drop ban: </label>
        <div class="col-lg-10">
            <div class="checkbox checkbox-switch">
                <input name="drop_banned" type="checkbox" class="switch" data-on-text="Yes" data-off-text="No" data-on-color="danger" data-off-color="default" @if($user->drop_banned) checked @endif>
            </div>
        </div>
    </div>

</fieldset>