@include('js-localization::head')
@extends('admin.template')
@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i> <span class="text-semibold">Purchases</span> - Add new purchase</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/dota-purchase') }}"
               class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-exit3"></i></b> Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="beastCreatePurchaseContainer" data-ng-app="beastCreatePurchase">
        <div data-ng-controller="Ctrl4">
            <input class="custom_search_input" id="search_form" data-ng-model="searchString"
                   data-ng-change="searchStringClick()"
                   type="text" placeholder="Enter item name">
            <div class="container_product_select">
                <div class="dota_product_not_selected">
                    <div class="purchase_product"
                         data-ng-repeat="product in products | startFrom:currentPage*pageSize | limitTo:pageSize"
                         data-ng-click='select_item_purchase("<%product.id%>","<%product.name%>","<%product.price%>","<%product.image%>")'>
                        <p class="name_product_purchase"><%product.name%></p>
                        <img class="image_product_purchase" src="/images/dota2_images/100/<%product.image%>">
                        <p class="price_product_purchase"><%product.price%></p>
                    </div>
                    <div class="pagination_button_purchase">
                    <button class="btn btn-info" data-ng-click="currentPage=currentPage-1">
                        Previous
                    </button>

                    <button class="btn btn-info"  data-ng-click="currentPage=currentPage+1">
                        Next
                    </button>
                    </div>
                </div>
            </div>
            <div class="dota_product_selected">
                <div class="purchase_product_selected"
                     data-ng-repeat="product in selected_product_purchase">
                    <p class="name_product_purchase_selected"><%product.name%></p>
                    <img class="image_product_purchase" src="/images/dota2_images/100/<%product.image%>">
                    <p class="price_product_purchase_selected"><%product.price%></p>
                    <input id="value_id_of_<%product.id%>" class="input_purchase_count" type="number" placeholder="count" value="1">
                    <div data-ng-click='delete_purchase_selected_item("<%product.id%>")' class="delete_selected_purchase">X</div>
                </div>
            </div>
            <hr/>
            <div class="container_for_margin">
                <div class="container_for_label">
                    <p class="input_price_label">BOT</p>
                    <p class="input_price_label">MAX PRICE</p>
                    <p class="input_price_label">MIN PRICE</p>
                    <p class="input_price_label">Select All Items</p>
                </div>
                <div class="container_for_input">
                <select class="input_price"  data-ng-model="selected_bot_purchase">
                    <option selected></option>
                    <option data-ng-repeat="bot in bots" id="<%bot.id%>" value="<%bot.id%>"> <%bot.name%></option>
                </select></br>
                    <input class="input_price" data-ng-model="max_price" type="text" placeholder="max price"></br>
                    <input class="input_price" data-ng-model="min_price" type="text" placeholder="min price"></br>
                    <input type="button" class="btn btn-info" value="Select All" data-ng-click="select_all_item()"></br>
                </div>
            </div>
            <div class="container_quality_purchase">
                <div class="select_item_quality" data-ng-click="selectItem()">QUALITY</div>
                <div id="listOfItems" class="all_item_quality">
                    <div data-ng-click="selectItems('all')" class="item_quality">All</div>
                    <div data-ng-click="selectItems('standard')" class="item_quality">Standard</div>
                    <div data-ng-click="selectItems('unusual')" class="item_quality">Unusual</div>
                    <div data-ng-click="selectItems('inscribed')" class="item_quality">Inscribed</div>
                    <div data-ng-click="selectItems('elder')" class="item_quality">Elder</div>
                    <div data-ng-click="selectItems('heroic')" class="item_quality">Heroic</div>
                    <div data-ng-click="selectItems('genuine')" class="item_quality">Genuine</div>
                    <div data-ng-click="selectItems('inscribed')" class="item_quality">Inscribed</div>
                    <div data-ng-click="selectItems('autographed')" class="item_quality">Autographed</div>
                    <div data-ng-click="selectItems('cursed')" class="item_quality">Cursed</div>
                    <div data-ng-click="selectItems('exalted')" class="item_quality">Exalted</div>
                    <div data-ng-click="selectItems('frozen')" class="item_quality">Frozen</div>
                    <div data-ng-click="selectItems('corrupted')" class="item_quality">Corrupted</div>
                    <div data-ng-click="selectItems('auspicious')" class="item_quality">Auspicious</div>
                    <div data-ng-click="selectItems('infused')" class="item_quality">Infused</div>
                    <div data-ng-click="selectItems('удачливый')" class="item_quality">Удачливый</div>
                </div>
                <div id="selectd_items_quality" class="selected_items_quality">
                    <div data-ng-repeat="items in selected_items" class="quality_selected_items"><%items%>
                        <div data-ng-click="deleteItems('<%items%>')" class="delete_quality_items">x
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_quality_purchase">
                <div class="select_item_quality" data-ng-click="selectItemRare()">Rarity</div>
                <div id="listOfItemsRare" class="all_item_quality">
                    <div data-ng-click="selectItemsRare('all')" class="item_quality">All</div>
                    <div data-ng-click="selectItemsRare('common')" class="item_quality">Common</div>
                    <div data-ng-click="selectItemsRare('uncommon')" class="item_quality">Uncommon</div>
                    <div data-ng-click="selectItemsRare('rare')" class="item_quality">Rare</div>
                    <div data-ng-click="selectItemsRare('mythical')" class="item_quality">Mythical</div>
                    <div data-ng-click="selectItemsRare('legendary')" class="item_quality">Legendary</div>
                    <div data-ng-click="selectItemsRare('immortal')" class="item_quality">Immortal</div>
                    <div data-ng-click="selectItemsRare('ancient')" class="item_quality">Ancient</div>
                    <div data-ng-click="selectItemsRare('arcana')" class="item_quality">Arcana</div>
                </div>
                <div id="selectd_items_quality" class="selected_items_quality">
                    <div data-ng-repeat="items in selected_items_rare" class="quality_selected_items"><%items%>
                        <div data-ng-click="deleteItemsRare('<%items%>')" class="delete_quality_items">x
                        </div>
                    </div>
                </div>
            </div>

            <input class="btn btn-danger custom_btn" data-ng-click="savePurchase()" type="button" value="SAVE">
            <div id="loader_purchase" class="loading_purchase"><img src="/images/csgotower/squares.gif">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/angular.js"></script>
    <script type="text/javascript" src="/assets/js/underscore.js"></script>
    <script type="text/javascript" src="/assets/js/create_purchase.js"></script>
@endsection
