@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Settings-General') !!}
    </div>

    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Clear project</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-body text-center">
        <div class="col-md-offset-3 col-md-6">
            <h2>Внимание!!!</h2>
            <p>Запуск очистки проекта обнулит все рабочие данные проекта, такие как операции, игры, балансы и т.д.
            <br><b>Запрещено</b> использовать, когда проект в режиме Production.</p>
        </div>

        <div  class="col-md-offset-3 col-md-6">
            <h2>Attention!!!</h2>
            <p>This process will trancate all working data of project, such as operations, games, balances etc.
            <br><b>Forbidden</b> to use when project in production mode.</p>
        </div>

        <div class="clearfix"></div>

        <form class="form-horizontal validate " method="post" action="{{ url(Config::get('app.admin_prefix').'/settings/clear-project') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div>
                <button type="submit" class="btn btn-danger"><i class="icon-alert"></i> CLEAR PROJECT</button>
            </div>
        </form>
    </div>
@endsection