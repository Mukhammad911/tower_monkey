@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Settings-Localization') !!}
    </div>

    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Localization</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <div class="panel-heading"><h3>Languages</h3></div>
        <table id="languages" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-teal-800">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Locale</th>
                    <th>Code</th>
                    <th>Active</th>
                    <th>Default Currency</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="panel panel-white">
        <div class="panel-heading"><h3>Currency</h3></div>
        <table id="currency" class="table table-hover" style="width:100%">
            <thead>
                <tr class="bg-teal-400">
                    <th>ID</th>
                    <th>Title</th>
                    <th>Symbol</th>
                    <th>Code</th>
                    <th>Active</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        var defaultCurrency = $.parseJSON('{!!  $defaultCurrency !!}'),
            defaultLocales    = $.parseJSON('{!!  $defaultLocales !!}');

        $(function () {
            var table = $('#languages').DataTable({
                ajax: {
                    "type": "GET",
                    "url": '{{ url(Config::get('app.admin_prefix').'/settings/languages') }}'
                },
                autoWidth: true,
                sorting: false,
                searching: false,
                ordering: false,
                info: false,
                columns: [
                    {data: "id"},
                    {data: "name"},
                    {data: "locale"},
                    {data: "code"},
                    {data: "active", width:150,
                        render:function(data, type, row){
                            var selected = (data==0) ? 'selected' : '',
                                disabled = ($.inArray(row.locale, defaultLocales) !=-1) ? 'disabled' : '';
                            return '<select '+ disabled +' class="lang_active form-control">' +
                                        '<option value="1">Yes</option>'+
                                        '<option '+ selected +' value="0">No</option>'+
                                    '</select>';
                        }
                    },
                    {data: "currency",
                        render:function(data, type, row){
                            var disabled = ($.inArray(row.locale, defaultLocales) !=-1) ? 'disabled' : '';

                            var select = '<select ' + disabled + ' class="curr_default">';
                                select += '<option value=""></option>';

                                $.each(row.currency_list,function(i,v){
                                    if(v.id == row.currency_id){
                                        select += '<option selected value="'+v.id+'">'+v.title+'</option>';
                                    }
                                    else{
                                        select += '<option value="'+v.id+'">'+v.title+'</option>';
                                    }
                                });

                                select += '</select>';

                            return select;
                        },
                        width: 200}
                ],
                "columnDefs": [
                    {
                        targets: -1,
                        data: 'currency',
                        defaultContent: ''
                    }
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });

            $('#languages tbody').on( 'change', '.lang_active', function () {
                var tableData = table.row( $(this).parents('tr') ).data(),
                    active = $(this).find('option:selected').val();

                $.ajax({
                    url:'{{ url(Config::get('app.admin_prefix').'/lang-active') }}',
                    method:'post',
                    dataType:'json',
                    data:{
                        id:tableData.id,
                        active:active
                    }
                })
                .success(function(ret){
                    if(ret){
                        swal('Language status changed!')
                    }
                })
            } );

            var currency = $('#currency').DataTable({
                ajax: {
                    "type": "GET",
                    "url": '{{ url(Config::get('app.admin_prefix').'/settings/currency') }}'
                },
                autoWidth: true,
                sorting: false,
                searching: false,
                ordering: false,
                info: false,
                columns: [
                    {data: "id"},
                    {data: "title"},
                    {data: "symbol_left"},
                    {data: "code"},
                    {data: "status",width:150,
                        render:function(data, type, row){
                            var selected = (data==0) ? 'selected' : '',
                                disabled = 'disabled';
                                //disabled = ($.inArray(row.code, defaultCurrency) !=-1) ? 'disabled' : '';

                            return '<select ' + disabled + ' class="curr_active form-control">' +
                                    '<option value="1">Yes</option>'+
                                    '<option '+ selected +' value="0">No</option>'+
                                    '</select>';
                        }
                    }
                ],
                "columnDefs": [
                    {
                        targets: -1,
                        data: 'currency',
                        defaultContent: ''
                    }
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←'}
                }
            });

            $('#currency tbody').on( 'change', '.curr_active', function () {
                var tableData = currency.row( $(this).parents('tr') ).data(),
                        active = $(this).find('option:selected').val();

                $.ajax({
                    url:'{{ url(Config::get('app.admin_prefix').'/currency-active') }}',
                    method:'post',
                    dataType:'json',
                    data:{
                        id:tableData.id,
                        active:active
                    }
                })
                .success(function(ret){
                    if(ret){
                        swal('Currency status changed!')
                        table.ajax.reload();
                    }
                })
            } );

            table.on('draw.dt',function(){
                $('.curr_default').select2({
                    placeholder: "Select a currency",
                    minimumResultsForSearch: -1
                });
            });

            $('#languages tbody ').on( 'change', '.curr_default', function () {
                var tableData = table.row( $(this).parents('tr') ).data(),
                    currency_id = $(this).find('option:selected').val();

                $.ajax({
                    url:'{{ url(Config::get('app.admin_prefix').'/language/set-currency') }}',
                    method:'post',
                    dataType:'json',
                    data:{
                        id:tableData.id,
                        currency_id:currency_id
                    }
                })
                .success(function(ret){
                    if(ret){
                        swal('Default currency changed!')
                        table.ajax.reload();
                    }
                });
            })
        });
    </script>
@endsection