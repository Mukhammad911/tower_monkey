@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Settings-General') !!}
    </div>

    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">General Settings</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-body">
        <form class="form-horizontal validate" method="post" action="{{ url(Config::get('app.admin_prefix').'/settings') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <fieldset class="content-group">

                @foreach($settings as $setting)
                    <?php if($setting->key == 'profit') continue; ?>
                    <div class="form-group">
                        <label class="control-label col-md-2 text-nowrap">{{ $setting->key }}<span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input required="required" class="course form-control" name="{{ $setting->key }}" value="{{ $setting->value }}">
                        </div>
                    </div>
                @endforeach
            </fieldset>

            <div>
                Last updated: <span class="label label-default">{{ $settings[0]->updated_at }}</span>
                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $('.course').numberMask({type:'float',afterPoint:2,decimalMark:'.'});
    </script>
@endsection