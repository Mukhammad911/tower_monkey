@extends('admin/template')

@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="icon-brain position-left"></i> <span class="text-semibold">Bots</span> - Index
                <small>Steam accounts manager</small>
            </h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/bots/create') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-user-plus"></i></b> Add new bot</a>
        </div>
    </div>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('bots') !!}
    </div>
@endsection

@section('content')
    <h5 class="content-group text-semibold">
        Steam trading bots list
        <small class="display-block">View/edit bots. To remove bot go to the view page</small>
    </h5>


    <div class="panel panel-white">
        <!--div class="panel-heading">
            <h5 class="panel-title">Steam trading bots</h5>
        </div-->

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr class="bg-primary-700">
                    <th>#</th>
                    <th>Guard</th>
                    <th>Login</th>
                    <th>Password</th>
                    <th>Enable</th>
                    <th>Status</th>
                    <th style="width: 210px"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($bots as $bot)
                    <tr>
                        <td>{{ $bot->id }}</td>
                        <td>@if(empty($bot->secret))
                                <i class="icon-shield-notice text-danger"></i>
                            @else
                                <i class="icon-shield-check text-success"></i>
                            @endif</td>
                        <td>
                            {{ $bot->steam_login }}
                        </td>
                        <td>{{ $bot->steam_password }}</td>
                        <td>
                            <div class="switchery-sm">
                                <input type="checkbox" data-id="{{ $bot->id }}" class="switchery"
                                       @if($bot->enabled) checked="checked" @endif
                                       @if(empty($bot->secret)) disabled="disabled" @endif >
                            </div>
                        </td>
                        <td>
                            @if ($bot->status_code == 1)
                                <span class="label label-success">OK</span>
                            @elseif ($bot->status_code == 2)
                                <span class="label label-danger" title="{{$bot->status_msg}}">Guard Error</span>
                            @elseif ($bot->status_code == 3)
                                <span class="label label-danger" title="{{$bot->status_msg}}">Account Locked</span>
                            @elseif ($bot->status_code == 4)
                                <span class="label label-danger" title="{{$bot->status_msg}}">API Error</span>
                            @endif
                        </td>
                        <td style="min-width: 300px">
                            <ul class="icons-list">
                                <li><a title="" data-popup="tooltip" href="{{ url(Config::get('app.admin_prefix').'/bots/' . $bot->id . '/getCode') }}" data-original-title="Get 5-digit Code" class="getCodeButton"><i class="icon-calculator4"></i> Code</a></li>
                                <li><a title="" data-popup="tooltip" href="{{ url(Config::get('app.admin_prefix').'/bots/' . $bot->id) }}" data-original-title="View"><i class="icon-eye4 position-right"></i> View</a></li>
                                <li><a title="" data-popup="tooltip" href="{{ url(Config::get('app.admin_prefix').'/bots/' . $bot->id . '/edit') }}" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
                                <!--li><a title="" data-popup="tooltip" href="{{ url(Config::get('app.admin_prefix').'/bots/' . $bot->id . '/purchases') }}" data-original-title="Purchases"><i class="icon-basket position-right"></i> Purchases</a></li-->
                            </ul>
                        </td>
                    </tr>
                @endforeach

                @if(!count($bots))
                    <tr><td colspan="6">No data available in table</td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div id="codeModal" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h6 class="modal-title">Bot Authentication Guard Code</h6>
                </div>

                <div class="modal-body">
                    <h2 class="text-semibold" id="botCode">KG42A</h2>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var ADMIN_PREFIX = '{{ Config::get('app.admin_prefix') }}';
    </script>
    <script src="/assets/js/plugins/notifications/bootbox.min.js"></script>
    <script src="/assets/js/plugins/forms/inputs/switchery.min.js"></script>
    <script src="/assets/js/bot.js"></script>
@endsection