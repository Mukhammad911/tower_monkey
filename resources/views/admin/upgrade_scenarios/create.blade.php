@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Scenario-create') !!}
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Create Scenario</h5>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal validate" action="{{ url(Config::get('app.admin_prefix').'/upgrade-scenario') }}" method="post" enctype='multipart/form-data'>
                        @include('admin.upgrade_scenarios.form')
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script src="{{ asset('assets/js/pages/form_validation.js') }}"></script>
    <script>
        $(function(){
            $("[name='stattrak']").bootstrapSwitch({});
            $("[name='status']").bootstrapSwitch({});

            $('#product_id').select2({
                allowClear: true,
                placeholder: "Select product...",
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);

                    var src = (option.data('image_source') == 'file') ? "{{ asset('images/products') }}/"+option.data('image') : option.data('image');
                    var image = '<img src="' + src + '" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                    var $state = $(
                        image + '<option style="line-height:40px;" value="' + option.value + '">'+ option.data('name') + ' '+ option.data('desc') + '</option>'
                    );

                    return $state;
                },
                templateSelection: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);

                    var src = (option.data('image_source') == 'file') ? "{{ asset('images/products') }}/"+option.data('image') : option.data('image');
                    var image = '<img src="' + src + '" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                    var $state = $(
                        image + '<option value="' + option.value + '">'+ option.data('name') + ' '+ option.data('desc') + '</option>'
                    );

                    return $state;
                }
            });

            $('#case_id').select2({
                allowClear: true,
                placeholder: "Select case...",
                escapeMarkup: function (markup) {
                    return markup;
                },
                templateResult: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);
                    var image =  '<img src="{{ asset('images/cases') }}/'+option.data('image')+'" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                    var $state = $(
                        image + '<option style="line-height:40px;" value="' + option.value + '">'+ option.data('name') + '</option>'
                    );
                    return $state;
                },
                templateSelection: function (state) {
                    if (!state.id) { return state.text; }
                    var option = $(state.element);
                    var image =  '<img src="{{ asset('images/cases') }}/'+option.data('image')+'" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                    var $state = $(
                        image + '<option value="' + option.value + '">'+ option.data('name') + '</option>'
                    );
                    return $state;
                }
            });

            $('#user_id').select2({
                ajax:{
                    type: "GET",
                    url: '{{ url(Config::get('app.admin_prefix').'/user/autocomplete') }}/',
                    delay: 250,
                    dataType: 'json',
                    data:function(params){
                        return {term: params.term}
                    },
                    processResults: function (data) {
                        var results = [];

                        $.each(data, function (index, value) {
                            var o = {};
                            o.id = value.id;
                            o.name = value.name;
                            o.username = value.username;
                            o.image = value.avatar;
                            o.steamid = value.steamid;
                            results.push(o);
                        });

                        return {
                            results: results
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                allowClear: true,
                placeholder: "Select user...",
                templateResult: function  (state) {
                    if (!state.id) { return state.text; }
                    //var option = $(state.element);

var image =  '<img src="'+ state.image +'" style="width:40px; border-radius:40px; display:inline-block; margin-right:20px; float:left;">';
                    var $state = $(
                        image + '<option style="line-height:40px;" value="'+state.id+'">'+state.steamid+' | '+state.username+' '+state.name+'</option>'
                    );
                    return $state;
                },
                templateSelection: function (state) {
                    if (!state.id) { return state.text; }
                    //var option = $(state.element);

var image =  '<img src="'+ state.image +'" style="width:24px; border-radius:24px; display:inline-block; margin-right:10px; float:left;">';
                    var $state = $(
                        image + '<option value="'+state.id+'">'+state.steamid+' | '+state.username+' '+state.name+'</option>'
                    );
                    return $state;
                }
            });

        });
    </script>
@endsection