<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="row">
    <div class="col-md-6">
        <fieldset>
            <legend class="text-semibold"><i class="icon-gear position-left"></i> Basic purchase settings</legend>
            <div class="form-group">
                <label class="control-label col-lg-4 text-nowrap">Bot <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <select  id="bot_id" name="bot_id" class="form-control" required="required">
                        <option value=""></option>
                        @foreach($bots as $bot)
                            <option @if($bot->id == $purchase->bot_id) selected @endif value="{{ $bot->id }}">{{ $bot->steam_login }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4 text-nowrap">Cases <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <select id="cases" name="cases[]"  class="form-control" required="required" multiple>
                        @foreach($cases as $case)
                            <option @if(in_array($case->id, $caseIds)) selected @endif value="{{ $case->id }}">{{ $case->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4">Inventory size (max 900) <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <input value="{{ $purchase->target }}" name="target" type="text" class="form-control number" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-4 text-nowrap">Max item price (₽) <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <input value="{{ $purchase->max_price/100 }}" name="max_price" type="text" class="form-control number" required="required">
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-6">
        <fieldset>
            <legend class="text-semibold"><i class="icon-truck position-left"></i> Product Quotas</legend>
            <div class="form-group">
                <label class="control-label col-lg-3 text-nowrap text-success text-semibold">Low quota <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group has-feedback has-success has-feedback-left">
                        <input value="{{ (isset($purchase->low_quota)) ? $purchase->low_quota : 75 }}" name="low_quota" type="text" class="form-control number" required="required">
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 text-nowrap text-orange text-semibold">Mid quota <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group has-feedback has-warning has-feedback-left">
                        <input value="{{ (isset($purchase->mid_quota)) ? $purchase->mid_quota : 23 }}" name="mid_quota" type="text" class="form-control number" required="required">
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 text-nowrap text-danger text-semibold">High quota <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group has-feedback has-error has-feedback-left">
                        <input value="{{ (isset($purchase->high_quota)) ? $purchase->high_quota : 2 }}" name="high_quota" type="text" class="form-control number" required="required">
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend class="text-semibold"><i class="icon-coins position-left"></i> Price Limits</legend>
            <div class="form-group">
                <label class="control-label col-lg-3 text-nowrap text-success text-semibold">Low price limit <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group has-feedback has-success has-feedback-left">
                        <input value="{{ (isset($purchase->low_price_limit)) ? $purchase->low_price_limit : 80 }}" name="low_price_limit" type="text" class="form-control number" required="required">
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 text-nowrap text-orange text-semibold">Mid price limit <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <div class="form-group has-feedback has-warning has-feedback-left">
                        <input value="{{ (isset($purchase->mid_price_limit)) ? $purchase->mid_price_limit : 200 }}" name="mid_price_limit" type="text" class="form-control number" required="required">
                        <div class="form-control-feedback">
                            <i class="icon-percent"></i>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>




