@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Purchase') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i><span class="text-semibold">Csgo Case Purchases</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/csgo-case-purchases/create') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-plus-circle2"></i></b> Add new purchase</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="purchases" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Bot Steam Login</th>
                <th>Available</th>
                <th>In drops</th>
                <th>Target</th>
                <th>Max Price</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var ADMIN_PREFIX = '{{ Config::get('app.admin_prefix') }}';
        var deleteURL = '{{ url(Config::get('app.admin_prefix').'/csgo-case-purchases') }}';
        var dataURL = '{{ url(Config::get('app.admin_prefix').'/csgo-case-purchases') }}';
    </script>
    <script>
        function deletePurchase(id){
            swal({
                title: "Are you sure?",
                text: "Selected purchase will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                html: false
            }, function() {
                $.ajax({
                    type: "DELETE",
                    dataType: "json",
                    url: deleteURL+ '/'+id,
                })
                    .success(function(success){
                        if(success){
                            $('#purchases').DataTable().ajax.reload();
                            swal(
                                "Deleted!",
                                "Purchase has been deleted.",
                                "success"
                            );
                        }
                    });
            });
        }
        $(function(){
            tableObj = $('#purchases').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/csgo-case-purchases/get/data') }}',
                searching:true,
                ordering: true,
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "bot.steam_login"},
                    {
                        data: "available",
                        render: function(data) {
                            return '<span class="badge badge-success">'+data+'</span>';
                        }
                    },
                    {
                        data: "drops",
                        render: function(data) {
                            return '<span class="badge badge-warning">'+data+'</span>';
                        }
                    },
                    {
                        data: "target",
                        render: function(data) {
                            return '<span class="badge badge-flat border-primary text-primary">'+data+'</span>';
                        }
                    },
                    {
                        data: "max_price",
                        render: function (data) {
                            return data/100;
                        }
                    },
                    {
                        data: 'id',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        width:200,
                        render: function (data) {
                            return '<ul class="icons-list">' +
                                '<li><a title="" data-popup="tooltip" href="/'+ ADMIN_PREFIX +'/csgo-case-purchases/'+data+'/edit" data-original-title="Edit"><i class="icon-pencil7"></i> Edit</a></li>'+
                                '<li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deletePurchase('+data+')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a></li>'+
                                '</ul>';
                        }
                    }
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection