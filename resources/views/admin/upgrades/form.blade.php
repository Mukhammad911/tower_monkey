<fieldset class="content-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">{{trans('messages.multiplier')}}<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="number" class="course form-control" name="multiplier" value="{{ $setting->multiplier or '' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-2 text-nowrap">{{trans('messages.win_chance')}}<span class="text-danger">*</span>:</label>
            <div class="col-md-4">
                <input required="required" type="number" class="course form-control" name="chance" value="{{ $setting->chance or '' }}">
            </div>
        </div>
    </div>
</fieldset>