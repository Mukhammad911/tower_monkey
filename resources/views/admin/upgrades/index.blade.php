@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Upgrade Games</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="upgrades" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>User</th>
                    <th>Bet</th>
                    <th>Profit</th>
                    <th>Multiplier</th>
                    <th>Win</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#upgrades').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/upgrade') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "user_id"},
                    { data: "bet"},
                    { data: "profit"},
                    { data: "multiplier"},
                    { data: "win"}

                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection