@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">

    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Available products</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="available" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Weapon</th>
                    <th>StatTrak™</th>
                    <th>Class</th>
                    <th>Price</th>
                    <th>Bot</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        $(function(){
            tableObj = $('#available').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/dota-available-product') }}',
                searching:true,
                ordering: true,
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "market_name"},
                    { data: "stattrak"},
                    { data: "class"},
                    { data: "price"},
                    { data: "bot_id"}
                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection