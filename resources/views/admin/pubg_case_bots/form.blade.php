<input type="hidden" name="_token" value="{{ csrf_token() }}">

<fieldset class="content-group">
    <legend class="text-bold">Steam details</legend>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Steam login <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input @if(isset($steam_login)) value="{{ $steam_login }}" readonly="readonly" @endif name="steam_login" type="text" class="form-control" required="required">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Steam Password <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input value="{{ $steam_password or '' }}" name="steam_password" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Steam ID <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input value="{{ $steamid or '' }}" name="steamid" type="text" class="form-control" required="required">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Phone number <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input value="{{ $phone or '' }}" name="phone" type="text" class="form-control">
        </div>
    </div>
</fieldset>
<fieldset class="content-group">
    <legend class="text-bold">Email details</legend>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Email login <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input value="{{ $email or '' }}" name="email" type="text" class="form-control email" required="required">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">Email Password <span class="text-danger">*</span></label>
        <div class="col-lg-10">
            <input value="{{ $email_password or '' }}" name="email_password" type="text" class="form-control" required="required">
        </div>
    </div>
</fieldset>
<fieldset class="content-group">
    <legend class="text-bold">csgo.tm settings</legend>

    <div class="form-group">
        <label class="control-label col-lg-2 text-nowrap">API key</label>
        <div class="col-lg-10">
            <input value="{{ $api_key or '' }}" name="api_key" type="text" class="form-control">
        </div>
    </div>
</fieldset>