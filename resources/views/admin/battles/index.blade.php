@extends('admin.template')

@section('breadcrumbs')
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Battles</span></h4>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <br>
        <table id="upgrades" class="table table-hover">
            <thead>
                <tr class="bg-primary-700">
                    <th>ID</th>
                    <th>Host</th>
                    <th>Winner</th>
                    <th>Players</th>
                    <th>Rounds/current</th>
                    <th>Price</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            tableObj = $('#upgrades').DataTable({
                ajax: '{{ url(Config::get('app.admin_prefix').'/battle') }}',
                searching:true,
                ordering: true,
                order: [[0, 'desc']],
                info:true,
                paging:true,
                processing: true,
                serverSide: true,
                columns: [
                    { data: "id"},
                    { data: "host_id"},
                    { data: "winner_id"},
                    { data: "players"},
                    { data: "rounds"},
                    { data: "price"},
                    { data: "status"}

                ],
                dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
                }
            });
        });
    </script>
@endsection