@extends('admin/template')

@section('header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-brain position-left"></i> <span class="text-semibold">Bots</span> - View Bot Details</h4>
        </div>
        <div class="heading-elements">
            <a href="{{ url(Config::get('app.admin_prefix').'/dota_bots') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-exit3"></i></b> Back</a>
            <a href="{{ url(Config::get('app.admin_prefix').'/dota_bots/'.$bot->id.'/edit') }}" class="btn bg-success btn-labeled heading-btn btn-sm"><b><i class="icon-pencil3"></i></b> Edit</a>
        </div>
    </div>
@endsection

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('bots', 'View') !!}
    </div>
@endsection

@section('css')
@endsection

@section('content')
    <h5 class="content-group text-semibold">
        <mark class="bg-primary">{{ $bot->steam_login }}</mark> trade bot
        <small class="display-block">Details and statistic</small>
    </h5>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-flat border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Steam account details</h6>
                </div>
                <div class="panel-body">
                    <div class="well">
                        <dl class="dl-horizontal">
                            <dt>Steam login:</dt>
                            <dd>{{ $bot->steam_login }}</dd>
                            <dt>Steam password:</dt>
                            <dd>{{ $bot->steam_password }}</dd>
                            <dt>Phone number:</dt>
                            <dd>{{ $bot->phone }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="panel panel-flat border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Steam Guard details</h6>
                </div>
                <div class="panel-body">
                    <div class="well">
                        <dl class="dl-horizontal">
                            <dt>Steam secret:</dt>
                            <dd>{{ $bot->secret }}</dd>
                            <dt>Steam Revocation Code:</dt>
                            <dd>{{ $bot->revocation_code }}</dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="panel panel-flat border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Email account details</h6>
                </div>
                <div class="panel-body">
                    <div class="well">
                        <dl class="dl-horizontal">
                            <dt>Email:</dt>
                            <dd>{{ $bot->email }}</dd>
                            <dt>Password:</dt>
                            <dd>{{ $bot->email_password }}</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="panel panel-flat border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">csgo.tm details</h6>
                </div>
                <div class="panel-body">
                    <div class="well">
                        <dl class="dl-horizontal">
                            <dt>API key:</dt>
                            <dd>{{ $bot->api_key }}</dd>
                        </dl>
                    </div>
                </div>
            </div>

            @include('admin.dota_bots.delete', $bot)
        </div>
        <div class="col-md-6">
            <div class="panel panel-flat border-top-success">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Statistics and Status</h6>
                </div>
                <div class="panel-body">
                    <strong>Guard status:</strong>
                    @if(isset($bot->secret))
                        <span class="label label-success position-right">Activated</span>
                    @else
                        <span class="label label-danger position-right">Inactive</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

@endsection