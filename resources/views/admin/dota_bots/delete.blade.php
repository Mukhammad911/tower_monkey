<div class="panel panel-danger panel-bordered">
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-warning position-left"></i> Delete bot</h6>
    </div>
    <div class="panel-body">
        All bot related data will be erased, including HardwareId files!
        <form action="{{ url(Config::get('app.admin_prefix').'/dota_bots/'.$id) }}" method="post">
            <input type="hidden" name="_method" value="delete" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="text-right">
                <button type="submit" class="btn btn-danger"> Delete </button>
            </div>
        </form>
    </div>
</div>