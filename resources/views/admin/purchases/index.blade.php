@include('js-localization::head')
@extends('admin.template')

@section('breadcrumbs')
    <div class="breadcrumb-line">
        {!! Breadcrumbs::render('Purchase') !!}
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-basket position-left"></i><span class="text-semibold">Purchases</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{ url(Config::get('app.admin_prefix').'/purchase/create') }}" class="btn bg-primary btn-labeled heading-btn btn-sm"><b><i class="icon-plus-circle2"></i></b> Add new purchase</a>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-white">
        <table id="purchases" class="table table-hover" style="width:100%">
            <thead>
            <tr class="bg-primary-700">
                <th>ID</th>
                <th>Bot Steam Login</th>
                <th>Available</th>
                <th>In drops</th>
                <th>Target</th>
                <th>Max Price</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@section('js')

    <script src="/assets/js/datatable-defaults.js"></script>
    <script>
        var ADMIN_PREFIX = '{{ Config::get('app.admin_prefix') }}';
        var deleteURL = '{{ url(Config::get('app.admin_prefix').'/purchase') }}';
        var dataURL = '{{ url(Config::get('app.admin_prefix').'/purchases') }}';
    </script>
    <script src="/assets/js/purchases.js"></script>
@endsection