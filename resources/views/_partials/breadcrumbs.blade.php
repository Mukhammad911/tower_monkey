@if ($breadcrumbs)
    <ol class="breadcrumb position-right">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$breadcrumb->last)
                <li>
                    @if($breadcrumb->first)
                        <i class="icon-home2 position-left"></i>
                    @endif
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="active">{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
    </ol>
@endif