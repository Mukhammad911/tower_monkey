    @include('site.partials.site-stats')
    <div id="content" style="">
        <div class="roulette-tower-container container" style="padding-left: 0;padding-right: 0;">
            <div class="row" style="width: 100%;margin: 0;">

                <div class="ladder-middle-roulette col-md-4 col-md-push-4 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0" style="">
                    {{--<div class="roulette-timer-container">
                        <span class="tower-roulette-timer" id="timer"></span>

                    </div>--}}

                    <img class="bg-roulette-monkey" src="/images/csgotower/gorilla.png">

                    <div class="roulette-container">

                        <div class="timer-wrapper tower-roulette-winner">
                            <div class="winner">
                                <div class="digits" id="winner" style="font-size: 55px; font-weight: 600; position: relative; z-index:10;"></div>
                            </div>
                        </div>

                        <div class="timer-wrapper tower-roulette-timer">
                            <h6>ROLLING</h6>
                            <div class="timer">
                                <div class="digits" id="timer"></div>
                            </div>
                        </div>

                        <div class="roulette-wrapper run-up">
                            <img id="roulette-dynamic" class="roulette-dynamic" src="/images/csgotower/r-dynamic.png">
                            <img id="roulette-static" class="roulette-static" src="/images/csgotower/r-static.png" style="margin-top: -365px;position: relative;">
                        </div>

                        <div class="previous-rolls" style="margin: 0 0 30px 0;">
                            <div class="text-center" style="font-size:12px;padding-top: 5px;">PREVIOUS ROLLS</div>
                            <div class="items text-center previous-rolls-items">

                            </div>
                        </div>

                        <div class="ladder-left-foor">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 25%;"><button class="roulette-play play-2x" data-demo="no">2x</button></td>
                                    <td style="width: 25%;"><button class="roulette-play play-3x" data-demo="no">3x</button></td>
                                    <td style="width: 25%;"><button class="roulette-play play-5x" data-demo="no">5x</button></td>
                                    <td style="width: 25%;"><button class="roulette-play play-50x" data-demo="no">50x</button></td>
                                </tr>
                            </table>
                        </div>

                        <div class="ladder-left-foor2">
                            <span><b class="text-center tower_min">min</b></span>
                            <span><b class="ladder-left-active text-center tower_min_plus">-</b></span>
                            <span><input type="number" class="tower-bet" value="10" min="1" max="5000" step="any"><!--CRD--></span>
                            <span><b class="text-center tower_max_plus">+</b></span>
                            <span><b class="text-center tower_max">max</b></span>
                        </div>

                        {{--}<div class="tower-provably">
                            <div class="tower-provably"><span>Check winning numbers</span></div>
                        </div> --}}
                        <div class="online_users text-center">
                            PLAYERS ONLINE
                            <span class="tower-bets-counter-t tower-bets-counter-top users-online">2</span>
                        </div>
                    </div>

                    {{--<canvas id="canvas" width="258" height="261" style="
        background: url(/images/csgotower/roulette-bg.png);
        top: -55px;
        position: relative;
        right: -4px;    display: inherit;
        margin: auto;"></canvas>--}}
                </div>

                {{--<div class="ladder-left-roulette col-md-4 col-md-pull-4 col-sm-12 col-sm-pull-0 col-xs-12 col-xs-pull-0" style="padding-right: 25px;">

                    <div class="ladder-right-stats ladder-right-stats-top">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active" style="border-top-left-radius: 12px;">
                                <a class="heroes-stats-link" data-toggle="tab" href="#x2"
                                   style="border-top-left-radius: 12px;color:#f5cc9b;">
                                    2X
                                    --}}{{--<img src="/images/csgotower/heroes.png">--}}{{--
                                </a>
                            </li>
                            <li style="">
                                <a class="heroes-chat-link" data-toggle="tab"
                                   style="color:#f5cc9b;" href="#x3">
                                    3X
                                </a>
                            </li>
                            <li style="">
                                <a class="heroes-chat-link" data-toggle="tab"
                                   style="color:#f5cc9b;" href="#x5">
                                    5X
                                </a>
                            </li>
                            <li style="border-top-right-radius: 12px;">
                                <a class="heroes-chat-link" data-toggle="tab"
                                   style="border-top-right-radius: 12px;color:#f5cc9b;" href="#x50">
                                    50X
                                </a>
                            </li>
                        </ul>
                        <div class="bets-stats-wrapper">
                            <div id="bets-stats-2x" class="bets-stats">Bets: <span class="count">154</span></div>
                            <div id="bets-stats-3x" class="bets-stats">Bets: <span class="count">22</span></div>
                            <div id="bets-stats-5x" class="bets-stats">Bets: <span class="count">13</span></div>
                            <div id="bets-stats-50x" class="bets-stats">Bets: <span class="count">6</span></div>
                        </div>
                        <div class="bets-coins-wrapper">
                            <div id="bets-coins-2x" class="bets-coins"><i class="fas fa-coins"></i> <span class="amount">1540.00</span></div>
                            <div id="bets-coins-3x" class="bets-coins"><i class="fas fa-coins"></i> <span class="amount">220.10</span></div>
                            <div id="bets-coins-5x" class="bets-coins"><i class="fas fa-coins"></i> <span class="amount">130.25</span></div>
                            <div id="bets-coins-50x" class="bets-coins"><i class="fas fa-coins"></i> <span class="amount">60.90</span></div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="x2" class="tab-pane fade in active">
                            <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_bet')}}</span>
                                <span>{{trans('messages.tower_table_profit')}}</span>
                                <span>{{trans('messages.tower_table_step')}}</span>
                                <span>{{trans('messages.tower_table_level')}}</span>
                            </div>
                            <div class="ladder-right-usrs">

                                @foreach($live_games as $live_game)
                                    @if(@$live_game->tower->profit > 0 )
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava">
                                                    <img src="{{ @$live_game->tower->user->avatar }}" alt="">
                                                </span>
                                                <span class="ladder-right-usr-login">
                                                    {{ @$live_game->tower->user->username }}
                                                </span>
                                            </a>
                                            <span class="ladder-right-usr-bet">
                                                {{ @$live_game->tower->bet }}
                                            </span>
                                            <span class="ladder-right-usr-profit win">
                                                {{ @$live_game->tower->profit }}
                                                <span class="coin_total_profit">
                                                    coins
                                                </span>
                                            </span>
                                            <span class="ladder-right-usr-step">
                                                {{ @$live_game->tower->step-1 }}
                                            </span>
                                            <span class="ladder-ico">
                                                <i class="ladder-{{ @$live_game->tower->level }}"></i>
                                            </span>
                                        </div>
                                    @else
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava">
                                                    <img src="{{ @$live_game->tower->user->avatar }}" alt="">
                                                </span>
                                                <span class="ladder-right-usr-login">
                                                    {{ @$live_game->tower->user->username }}
                                                </span>
                                            </a>
                                            <span class="ladder-right-usr-bet">
                                                {{ @$live_game->tower->bet }}
                                            </span>
                                            <span class="ladder-right-usr-profit neon">
                                                {{ @$live_game->tower->profit }}
                                                <span class="coin_total_profit">coins</span>
                                            </span>
                                            <span class="ladder-right-usr-step">
                                                {{ @$live_game->tower->step-1 }}
                                            </span>
                                            <span class="ladder-ico">
                                                <i class="ladder-{{ @$live_game->tower->level }}"></i>
                                            </span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div id="x3" class="tab-pane fade">
                            <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_bet')}}</span>
                                <span>{{trans('messages.tower_table_profit')}}</span>
                                <span>{{trans('messages.tower_table_step')}}</span>
                                <span>{{trans('messages.tower_table_level')}}</span>
                            </div>
                            <div class="ladder-right-usrs">

                                @foreach($live_games as $live_game)
                                    @if(@$live_game->tower->profit > 0 )
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit win">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @else
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit neon">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div id="x5" class="tab-pane fade">
                            <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_bet')}}</span>
                                <span>{{trans('messages.tower_table_profit')}}</span>
                                <span>{{trans('messages.tower_table_step')}}</span>
                                <span>{{trans('messages.tower_table_level')}}</span>
                            </div>
                            <div class="ladder-right-usrs">

                                @foreach($live_games as $live_game)
                                    @if(@$live_game->tower->profit > 0 )
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit win">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @else
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit neon">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div id="x50" class="tab-pane fade">
                            <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_bet')}}</span>
                                <span>{{trans('messages.tower_table_profit')}}</span>
                                <span>{{trans('messages.tower_table_step')}}</span>
                                <span>{{trans('messages.tower_table_level')}}</span>
                            </div>
                            <div class="ladder-right-usrs">

                                @foreach($live_games as $live_game)
                                    @if(@$live_game->tower->profit > 0 )
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit win">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @else
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit neon">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>


                </div>--}}

                <div class="ladder-left-roulette col-md-4 col-md-pull-4 col-sm-12 col-sm-pull-0 col-xs-12 col-xs-pull-0" style="padding-right: 25px;">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <div class="ladder-right-stats ladder-right-stats-top " id="collapse-stats-2x">
                                            <div class="bet text-center" style="color: #fdfe00;">2X</div>
                                            <div class="count text-center">Total Bets: <span class="total total-count-user-2x">0</span></div>
                                            <div class="amount text-right">
                                                <i class="fas fa-coins"></i>
                                                <span class="total total-bet-2x">0.00</span>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="ladder-right-usrs" id="collapse-2x">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <div class="ladder-right-stats ladder-right-stats-top " id="collapse-stats-3x">
                                            <div class="bet text-center" style="color: #f59809;">3X</div>
                                            <div class="count text-center">Total Bets: <span class="total total-count-user-3x">0</span></div>
                                            <div class="amount text-right">
                                                <i class="fas fa-coins"></i>
                                                <span class="total total-bet-3x">0.00</span>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="ladder-right-usrs" id="collapse-3x">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <div class="ladder-right-stats ladder-right-stats-top " id="collapse-stats-5x">
                                            <div class="bet text-center" style="color: #32b02c;">5X</div>
                                            <div class="count text-center">Total Bets: <span class="total total-count-user-5x">0</span></div>
                                            <div class="amount text-right">
                                                <i class="fas fa-coins"></i>
                                                <span class="total total-bet-5x">0.00</span>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div class="ladder-right-usrs" id="collapse-5x">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <div class="ladder-right-stats ladder-right-stats-top " id="collapse-stats-50x">
                                            <div class="bet text-center" style="color: #f20107;">50X</div>
                                            <div class="count text-center">Total Bets: <span class="total total-count-user-50x">0</span></div>
                                            <div class="amount text-right">
                                                <i class="fas fa-coins"></i>
                                                <span class="total total-bet-50x">0.00</span>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <div class="ladder-right-usrs" id="collapse-50x">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="ladder-right-roulette col-md-4 col-sm-12" style="padding-left: 25px;">

                    <div class="ladder-right-stats ladder-right-stats-top">
                        <ul class="nav nav-pills nav-justified">
                            <li class="active" style="border-top-left-radius: 12px;">
                                <a class="heroes-stats-link" data-toggle="tab" href="#stats"
                                   style="border-top-left-radius: 12px;">
                                    {{trans('messages.HALL_OF_HEROES')}}
                                    {{--<img src="/images/csgotower/heroes.png">--}}
                                </a>
                            </li>
                            <li style="border-top-right-radius: 12px;">
                                <a class="heroes-chat-link" data-toggle="tab" href="#chat"
                                   style="border-top-right-radius: 12px;">
                                    {{trans('messages.chat')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div id="stats" class="tab-pane fade in active">
                            <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                <span>{{trans('messages.tower_table_rank')}}</span>
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_wagered')}}</span>
                            </div>
                            <div class="ladder-right-usrs-t ladder-right-usrs-top heroes-rank">
                                @foreach($top as  $key=>$top_user)
                                    <div class="ladder-right-usr">
                                        <span class="ladder-right-usr-rank">
                                            <span class="text_top_count">{{$key+1}}</span>
                                        </span>
                                        <a href="/profile/{{$top_user->id}}">
                                            <span class="ladder-right-usr-login ladder-right-usr-login-top">
                                                {{$top_user->username }}
                                            </span>
                                        </a>
                                        <span class="ladder-right-usr-bet ladder-right-usr-bet-top">
                                            {{ $top_user->profit }} <span class="coins_header_top">coins</span>
                                        </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div id="chat" class="tab-pane fade">
                                <div class="chat-container">
                                    <div class="chat-header">
                                        <div class="chat-selector">
                                            <ul class="selector-lang">
                                                <li class="selector-lang-li en">EN</li>
                                                <li class="selector-lang-li ru">RU</li>
                                                <li class="selector-lang-li tr">TR</li>
                                                <li class="selector-lang-li es">ES</li>
                                                <li class="selector-lang-li de">DE</li>
                                                <li class="selector-lang-li pl">PL</li>
                                            </ul>
                                        </div>
                                         {{--<div class="online-user-show-chat">
                                            <span class="span-user-online">Online:</span>
                                            <span class="span-user-online users-online">0</span>
                                        </div> --}}
                                    </div>

                                    <div class="chat-body">

                                    </div>

                                    <div class="chat-footer">
                                        <div class="chat-footer-container">
                                            <input type="text" class="chat-message-box">
                                            <button type="button" class="chat-message-btn">SEND</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@section('js')
@endsection