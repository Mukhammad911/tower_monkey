<div id="content">
    <div class="partnership-page">
    @role('user')
        <div class="partnership-page-title"><h3>{{ trans('messages.partner_head') }}</h3></div>

        <div class="partnership-page-fields">
            <div class="partnership-page-fields-link">
                <form  id="form_promo_code">
                    <label>{{trans('messages.your_partner_link')}}:<input type="text" value="{{ url('') }}/p/<?= empty($user->promo_code) ? $user->id : $user->promo_code ?>" disabled></label>
                    <label>{{trans('messages.promocodes')}}:<input type="text" id="promo_code" name="promo_code"  value="<?= empty($user->promo_code) ? $user->id : $user->promo_code ?>"></label>
                    <button>{{trans('messages.save')}}</button>
                </form>
            </div>
        </div>

        <div class="partnership-page-stats">
            <div class="partnership-page-stats-title">
                <span>{{trans('messages.level')}}</span>
                <span>{{trans('messages.referrals')}}</span>
                <span>{{trans('messages.to_next_level')}}</span>
                <span>{{trans('messages.percentage')}}</span>
                <span>{{trans('messages.referral_receive')}}</span>
                <span>{{trans('messages.referral_deposited')}}</span>
                <span>{{trans('messages.earnings')}}</span>
            </div>
            <div class="partnership-page-stats-item">
                <span>{{ $user->affiliate_level }}</span>
                <span>{{ $referrals_amount }}</span>
                <span>${{ Partnership::affiliatedAmount($user->id) }} / <b>${{ $next_border_amount }}</b></span>
                <span>{{ $user->partnerLevel->percent }}%</span>
                <span>+{{ number_format($user->partnerLevel->bonus_amount, 2) }}% {{trans('messages.to_deposit')}}</span>
                <span>${{ Partnership::affiliatedAmount($user->id) }}</span>
                <span>${{ $bonus_balance->summ or '0.00' }}</span>
            </div>
        </div>

        <div class="partnership-page-info">
            <div class="partnership-page-info-item">
                <b>{{trans('messages.best_affiliates')}}</b>
                {{trans('messages.on_csgotower')}}
            </div>
            <div class="partnership-page-info-item">
                <b>{{trans('messages.on_start')}}</b>
                {{trans('messages.percent_on_market')}}
            </div>
            <div class="partnership-page-info-item">
                <b>{{trans('messages.sponsorship')}}</b>
                {{trans('messages.youtube_or_twitch')}}
            </div>
        </div>

        <div class="partnership-page-lvls-faq">

            {{--<div class="partnership-page-lvls1">
                <div class="partnership-page-lvls-title2">{{trans('messages.faq')}}</div>
                <div class="partnership-page-faq">
                    <div class="partnership-page-faq-item">
                        <div class="partnership-page-faq-item-title">Before opening Cases</div>
                        Be sure to check your account exchanges on Steam, otherwise our bots will not be able to send you your items. Open your inventory, make sure that there is no ban 7 days after the password had been changed.
                    </div>

                    <div class="partnership-page-faq-item">
                        <div class="partnership-page-faq-item-title">Just filled my balance but money does not appear</div>
                        From our side we process payments instantly. Some delays may occur in paygate G2A which verifies every payment from 5 seconds to 10 minutes. Also, payment may be withheld by your bank. Wait an hour - if money still don't arrive - write to us at support@csgotower.net, we'll investigate why this might happen.
                    </div>

                    <div class="partnership-page-faq-item">
                        <div class="partnership-page-faq-item-title">I can not login to Steam or send my skin</div>
                        As you know Steam may be worldwide down sometimes. As our system fully relies on Steam services, only option we have - just wait with all others when it's up again.
                    </div>
                </div>
            </div>--}}

            <div class="partnership-page-lvls-title1">{{trans('messages.partner_levels')}}</div>
            <div class="partnership-page-lvls">
                <div class="partnership-page-lvls-title">
                    <span>{{trans('messages.level')}}</span>
                    <span>{{trans('messages.to_next_level')}}</span>
                    <span>{{trans('messages.percentage')}}</span>
                    <span>{{trans('messages.referral_receive')}}</span>
                </div>

                @foreach($levels as $level)
                    <div class="partnership-page-lvls-item">
                        <span>{{ $level->level }}</span>
                        <span>${{ number_format($level->border_amount, 0 , '', ' ') }}</span>
                        <span>{{ number_format($level->percent, 2) }}%</span>
                        <span><b>+{{ number_format($level->bonus_amount, 2) }}%</b>  <i>{{trans('messages.to_deposit')}}</i></span>
                    </div>
                @endforeach
            </div>


        </div>
    </div>

    @else

    @endrole
</div>



<script>
    var ACTIVE_MENU = '{{ $active_menu }}';
</script>