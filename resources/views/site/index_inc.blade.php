@role('user')

@else
<div class="no-login-info">
    <h1>THIS IS YOUR WAY TO MAKE PROFIT!</h1>
    <span>
        Unbox and withdraw the skins you actually want. And win up to 1 million dollars!
        <br><br>
        All skins are always available
    </span>
    <a href="" class="btn-login">Start</a>
</div>
@endrole

<?php $i = 0; ?>
@foreach($categories as $category)
    <div class="original-cases">
        <div class="cases-title"><i class="original-cases-title-ico"></i><h3>{{ (Lang::has('messages.'.$category->name)) ? trans("messages.$category->name") : $category->name }}</h3></div>
        @foreach($category->cases as $case)
            <div class="original-cases-item @if($category->name == 'SPECIAL CASES') special-cases @endif">
                <img src="{{ asset('images/cases') }}/{{ $case->image }}" alt="">
                <div class="original-cases-item-info">
                    <span><b>{{ $case->priceObj->price }}</b> {{trans('messages.credits')}}</span>
                    <!--{{ (Lang::has('case.'.$case->name)) ? trans("case.$case->name") : $case->name }} -->
                    {{trans('messages.you_may_win')}} <i>{{ $case->highestPrice }} SCOR</i>
                </div>
                <div class="original-cases-item-link"><a href="/case/{{ $case->id }}" class="link_case">Open</a></div>
            </div>
        @endforeach

        @if($i == 0)
            <a href="/upgrade" class="link_upgrade upgrade-btn"><span>Try now</span></a>
        @elseif($i == 1)
        <a href="/tower" class="link_tower upgrade-btn upgrade-btn2"><span>Try now</span></a>
        @endif
    </div>
    <?php $i++; ?>
@endforeach

@include('site.partials.top-winners')
