@extends('layouts.layout')

@section('content')
    @include('site.dota2_inc')
    <script src="/assets/js/angular.js"></script>
    <script src="/assets/js/underscore.js"></script>
    <script src="/assets/js/market.js"></script>
@endsection
