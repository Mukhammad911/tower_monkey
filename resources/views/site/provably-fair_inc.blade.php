@if(!Auth::check())
    @include('site.must-be-logged_inc')
@else

<div id="content">
    <div class="fair-page">
        <div class="fair-page-title"><h3>{{trans('messages.provably_fair')}}</h3></div>

        <div class="fair-page-info" style="text-align: justify;">
            <p>{{trans('messages.text_provably_fair_1')}}<a href="http://phptester.net/"> phptester </a> {{trans('messages.text_provably_fair_2')}}</p>
            <p>{{trans('messages.text_provably_fair_3')}}</p>
            <ul>
                <li>{{trans('messages.text_provably_fair_4')}}</li>
                <li>{{trans('messages.text_provably_fair_5')}}</li>
                <li>{{trans('messages.text_provably_fair_6')}}</li>
            </ul>
        </div>

        <div class="example-calcul">
            <div class="fair-page-example">
                <div class="fair-page-example-title">{{trans('messages.example')}}:</div>
                <div class="fair-page-example-code" style="font-size: 0.8em">
                $user_seed = 'a1f8ac3cbd8855d93ea56c409ad5a9d50e5d409294781d47459ae27f1dbe9621';<br>
                $server_seed = '026693ff6f7ca683d39ba7fbd1182c78ba5ceacb781c49b2ec6ca8589bbaa8c1';<br>
                $win_number = 78290;<br>
                <br>
                $hash = hash('sha256', $user_seed + $server_seed + $win_number);<br>
                <br>
                echo "Hash number = " . $hash;<br>
                </div>
            </div>

            <div class="fair-page-calcul">
                <div class="fair-page-example-title">{{trans('messages.calculator')}}:</div>
                <div class="fair-page-example-form">
                    <form id="calculate_hash">
                        <input type="text" name="user_seed" placeholder="{{trans('messages.user_seed')}}...">
                        <input type="text" name="server_seed" placeholder="{{trans('messages.server_seed')}}...">
                        <input type="number" name="win_number" placeholder="{{trans('messages.win_number')}}...">
                        <button>Calculate</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="fair-page-table">
            <div class="fair-page-table-item fair-table-green">
                <span style="width: 18.6%">{{trans('messages.hash_number')}}:</span><span id="calculated_hash" style="width: 81%;"></span>
            </div>

            {{--<div class="fair-page-table-title"><span>Server seed</span> <span>date</span></div>
            <div class="fair-page-table-item fair-table-red">
                <span>Текущий server seed сейчас используется... </span><span>2016-11-04 </span>
            </div>
            <div class="fair-page-table-item fair-table-green">
                <span>A215CBDFA74C5829FDEF42A9361994D2D14FAB210F2ACD2ED2D2FC20D5940EA7</span><span>2016-11-04 </span>
            </div>
            <div class="fair-page-table-item fair-table-green">
                <span>A215CBDFA74C5829FDEF42A9361994D2D14FAB210F2ACD2ED2D2FC20D5940EA7</span><span>2016-11-04 </span>
            </div>
            <div class="fair-page-table-item fair-table-green">
                <span>A215CBDFA74C5829FDEF42A9361994D2D14FAB210F2ACD2ED2D2FC20D5940EA7</span><span>2016-11-04 </span>
            </div>--}}
        </div>
    </div>
</div>

@endif