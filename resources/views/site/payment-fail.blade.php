@extends('layouts.layout')

@section('title')
    <title>csgotower - Payment error</title>
@endsection

@section('drops')
    @include('site.partials.drops')
@endsection

@section('content')
    <div class="content">
        <p class="text-center payment-status payment-failed">{{ trans('messages.payment_error').', '.trans('messages.contact').' support@csgotower.net' }}</p>
    </div>
@endsection