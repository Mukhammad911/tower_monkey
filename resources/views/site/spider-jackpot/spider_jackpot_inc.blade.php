@section('content')
    <div id="content" class="content-coin-flip">
        <div class="container-spider-jackpot">
            <div class="left-container-spider-jackpot">
                <div class="jackpot-header">
                    <div class="jackpot-header-number_game">
                        <p class="jackpot-header-text-game">Game #</p>
                        <p class="jackpot-header-text-game">5</p>
                    </div>
                    <button class="spider-jackpot-history-game">History</button>
                </div>

                <div class="jackpot-content-roulette">

                    <div class="jackpot-content-roulette-header">
                        <div class="jackpot-progress-bar">
                            <div class="bar " id="bar">
                                <div class="dot">
                                    <p id="gameitems">0</p>
                                </div>
                                <div class="progress-bar">
                                    <div class="progress-bar-bar" id="progress" style="width: 0%;">
                                        <div class="icon-runner no-opacity" style=" opacity: 1;  " id="progress-bar-runner"></div>
                                    </div>
                                </div>
                                <div class="dot">
                                    <p id="max-items-count">100</p>
                                </div>
                            </div>
                        </div>
                        <div class="jackpot-header-test-or">
                            <p class="text-or">OR</p>
                        </div>
                        <div class="jackpot-timer">

                        </div>
                    </div>
                </div>

                <div class="second-information-jackpot-container">
                    <div class="jackpot-count-enter-item">
                        <p class="text-counter-of-item-entered">You entered </p>
                        <p class="text-counter-of-item-value">25</p>
                        <p class="text-counter-of-items">items</p>
                        <p class="minimum-set-item-info">Min 0.1$ max 20 items you can add. More items more chanse for win</p>
                    </div>

                    <div class="jackpot-count-enter-item-chance">
                        <p class="text-counter-of-item-entered-second">You entered </p>
                        <p class="text-counter-of-item-value-second">25</p>

                    </div>
                    <div class="jackpot-count-enter-item-start">
                     <button class="jackpot-btn-add-items"></button>

                    </div>

                </div>
                <div class="jackpot-selected-users">
                    <div class="jackpot-container-users">
                        <img class="jackpot-user-avatar" src="">
                        <p class="jackpot-user-profit">25</p>
                        <p class="jackpot-user-profit_percent">%</p>
                        <div class="jackpot-user-profit-color"></div>
                    </div>
                </div>

                <div class="jackpot-selected-users-items">
                  <div class="jackpot-header-users">
                      <div class="jackpot-content-items-user-left">
                          <img class="jackpot-user-avatar" src=""/>
                          <p class="jackpot-username-users">Muhammad Yakubov</p>
                      </div>
                      <div class="jackpot-content-items-user-right">
                          <p class="jackpot-enter-summ-users-dollar">100</p>
                          <p class="jackpot-enter-summ-users-symbol-dollar">$</p>
                          <p class="jackpot-enter-summ-users-percent-chance">10</p>
                          <p class="jackpot-enter-summ-users-percent-symbol">%</p>
                          <div class="jackpot-color-right"></div>
                      </div>
                      <div class="jackpot-container-entered-items">
                          <div class="jackpot-div-items">
                              <img src="" class="jackpot-item-image">
                              <p class="jackpot-price-of-item-entered">0.50</p>
                              <p class="jackpot-price-of-item-entered-symbol">$</p>

                          </div>
                      </div>
                  </div>
                </div>

            </div>
            <div class="right-container-spider-jackpot">

            </div>
        </div>
    </div>
@endsection

