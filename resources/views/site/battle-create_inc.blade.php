<style>
    .create-battle-wrapper{
        text-align: center;
        width: 90%;
        margin: auto;
        color: #cedff5;
    }

    .create-battle-case{
        width: 280px;
        min-height: 280px;
        display: inline-block;
        vertical-align: top;
        margin: 10px;
        text-align: center;
        border-radius: 15px;
    }

    .create-battle-case img{
        display: block;
        min-height: 150px;
        max-height: 200px;
        max-width: 280px;
        margin: auto;
    }

    .create-battle-case button{
        font-size: 16px;
        border-radius: 5px;
        padding: 0 30px;
        margin: 15px auto;
        height: 30px;
    }
</style>



<div class="create-battle-wrapper">
@foreach($cases as $case)
    <div class="create-battle-case">
        <img src="{{ asset('images/cases') }}/{{ $case->image }}">
        <span><b>{{ $case->priceObj->price }}</b> {{trans('messages.credits')}}</span>
        <br>
        <button class="add_battle_case">Add case +</button>
    </div>
@endforeach

    <hr>

    <div>
        <span>Battle price:</span>
        <span class="create_battle_price">0</span> SCOR
        <br><br>
    </div>

    <button class="btn_create_battle">CREATE BATTLE</button>

    <br><br>
</div>