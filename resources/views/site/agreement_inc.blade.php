<div class="guarantee-page">
    <div class="guarantee-page-title"><h3>Terms & conditions</h3></div>

    <div style="color: #d6d6d6; margin: 20px 20%; text-align: justify; font-size: 14px;">
        <h2 style="font-family:lithos-pro">1.INTRODUCTION:</h2>

        <p>1.1. Opening the website, using and/or visiting any section http://csgotower.net ( the "website"), the user automatically accepts agreement to: terms and conditions, privacy Policy, the rules of any lottery, terms and conditions of any promotional activities, bonuses and special offers from time to time can be accessed on the Website. All of the above terms and conditions are referred to below as the "Terms". Before accepting the Terms, the user agrees to read it carefully. If you do not agree with the terms of this agreement, we urge you stop using the services of the website. Continuation of using the website is confirmation of Your agreement to the Terms.</p>

        <br>
        <h1 style="font-family:lithos-pro">GENERAL TERMS AND CONDITIONS</h1>

        <br>
        <h2 style="font-family:lithos-pro">2. PARTIES TO THE AGREEMENT</h2>

        <p>2.1. A website owned and operated by csgotower. Excerpts from the Terms containing the pronouns "us", "our", "we" or the "Company" refer to the company with which You contract in accordance with the previous paragraph.</p>

        <br>
        <h2 style="font-family:lithos-pro">3. ALTERATION OF CONTRACT</h2>

        <p>3.1. The company has the right to add, edit and remove any of the Terms due to number of reasons, including legal (in accordance with new laws or regulations), commercial, as well as reasons connected with customer service. Current Conditions in their current form, as well as the date of coming into legal force are available on the Website. The company will inform the player about all the changes, additions or amendments by posting a new version of the Terms on the website. The user assumes the responsibility to review the current Terms. We urge You to check for updates of Conditions regularly. The company reserves the right to make any changes to the operating procedure of the website (the software, the order of service) and modify requirements according to current legislation at any time and without prior notice to the users.</p>

        <p>3.2. In case of refusal of acceptance of the changes, You must stop using the website. Further your use of any part of the website after the date of become effective of the revised terms and Conditions will automatically be considered as acceptance of the revised Terms, including (for the avoidance of doubt) any additions, removals, substitutions or other changes to identifying information relating to the Company referred to in paragraph 2.1 of these Terms, regardless of whether You receive a notification or read the revised Terms.</p>

        <br>
        <h2 style="font-family:lithos-pro">4. LEGAL REQUIREMENTS</h2>

        <p>4.1. The use of the services of the website is strictly prohibited for persons under 18 or persons not reached the age of majority, which is permitted for gambling or gaming activities under the law of a particular jurisdiction (“Valid age”). The use of the services of the website by a person younger than the legal age is considered a violation of the Terms. To ensure that persons who have not attained the legal age, do not use the services of the website, the company reserves the right at any stage to require documentary proof of Your age. The company reserves the right to terminate your use of the services of the website, as well as in the issuance of Your winning item with no refund of the amount spent on a lottery ticket (case), if not provided proof of age of majority or the Company suspects that the service uses a person under the legal age.</p>

        <p>4.2. Online lotteries may be prohibited in some jurisdictions. You understand and agree that the Company is unable to provide You any legal advice or warranty regarding the legality of the use of the services of the website. The company makes no representation that the services of the website are not contrary to the laws of Your jurisdiction. Services provided by the website, are used by the player of their choice and discretion. In deciding whether the use of the services of the website is lawful in accordance with applicable legal acts in Your jurisdiction, You have the risk of liability solely on yourself.</p>

        <p>4.3. The Company has no intentions to provide You with services that would be contrary to applicable laws of Your jurisdiction. You acknowledge, agree and warrant that your use of the services on the website complies with all applicable laws, rules and regulations of Your jurisdiction. The company assumes no responsibility for any illegal or unauthorized use of the services of the website.</p>

        <p>4.4. You assume full responsibility for all taxes and fees that may be imposed in connection with any winnings resulting from use of the website. If the gain is subject to tax by Your local (legislative, fiscal, etc.) authorities, You are responsible for reporting your winnings and/or losses to the appropriate authorities.</p>

        <br>
        <h2 style="font-family:lithos-pro">5. RULES OF RECEIPT OF GOODS</h2>

        <p>5.1. The user selects a category of a lottery ticket (case) available on the Web site.</p>

        <p>5.2. The user agrees that he pays only for a lottery ticket, which allows you to win the particular item different price categories determined in a random selection of our lottery.</p>

        <p>5.3. The user agrees and takes into account that the price of a lottery ticket may exceed the value of the winning item, as well as to be lower than the cost of the purchased ticket.</p>

        <p>5.4. In case of any questions about purchasing or receiving gained goods or any other problems, You can contact customer support by e-mail: support@csgotower.net</p>

        <p>5.5. None of the employees of the website, as well as users don't know and can't know the value of a winning product in advance. All risks on the cost of a lottery ticket bears the user.</p>

        <p>5.5.1. To invalidate all the operations that were carried out by a person who has not attained the age of majority and to refuse to issue a winning product and refund the money spent on a lottery ticket (case);</p>

        <p>5.5.2. The web site under no circumstances does not return the money spent on a lottery ticket (case), and do not exchange the product for another, is not liable, if the lottery ticket (case) was significantly higher than the cost of the won item.</p>

        <p>5.5.3. The website reserves the right to refuse to issue the product won the users, if the user has not picked up the item within 1 hour from the time of the winning this product without refund of the cost of a lottery ticket (case), and to refuse to issue the won item upon request if a website user has not provided proof of confirmation of age.</p>

        <br>
        <h2 style="font-family:lithos-pro">6. OTHER PROHIBITED ACTIONS</h2>

        <p>6.1. Prohibited any form of offensive communication with employees and other players (profanity, belittling, threats).</p>

        <p>6.2. You are not allowed to tamper with, modify, remove or in any way alter the information posted on the website. Also, it is prohibited to make any action that may cause disruption or in any way affect the performance of the website (use and dissemination of viruses, malware and scripts). Any mass mailing ("spam") is strictly prohibited.</p>

        <p>6.3. Prohibited in any way reproduce or copy the contents of the website without our prior permission. You agree to use the website only for entertainment purposes.</p>

        <p>6.4. Prohibited any attempt to gain unauthorized access to the Web site, or any servers and computers associated with the Web site. If we suspect You of hacking attempts or bypass the security system, we will be forced to immediately suspend Your account, report this incident to authorities, as well as to provide them with Your personal information.</p>

        <p>6.5. We are not responsible for any damages or losses incurred by You as a result of technical failures, infect Your computer with viruses or other malware when using the Web site or posted links.</p>

        <br>
        <h2 style="font-family:lithos-pro">7. CHANGES ON THE WEB SITE</h2>

        <p>7.1 We reserve the right at any time change or revise any service hosted on the Web site.</p>

        <br>
        <h2 style="font-family:lithos-pro">8. SYSTEM FAILURE</h2>

        <p>8.1. We will try as soon as possible to correct failures in the lottery, in case of their occurrence. The company is not liable for faults and malfunctions in equipment used by You, and also for any interruptions to Your Internet service provider.</p>

        <br>
        <h2 style="font-family:lithos-pro">9. DRAWBACKS AND DEFECTS</h2>

        <p>9.1. During using the services of the website You may encounter circumstances in which Your won items will not be received. In this case, the Web site will make every effort to resolve this situation and try as soon as possible to resolve Your problem. The maximum term of consideration of the defect/error is 45 working days.</p>

        <br>
        <h2 style="font-family:lithos-pro">10. EXCLUSION OF OUR LIABILITY</h2>

        <p>10.1. You agree that use of this website is at your own risk.</p>

        <p>10.2. Services on the site are provided in accordance with these Conditions and they are fully adjustable. The company does not provide additional guarantees or promises regarding the Web site or hosted it services.</p>

        <p>10.3. We are not responsible for any damages or losses, including loss of data, reputation or income. The company assumes no responsibility for the content of websites for which links may be posted on our Web site.</p>

        <br>
        <h2 style="font-family:lithos-pro">11. INFRINGEMENT OF CONDITIONS</h2>

        <p>11.1. You assume the responsibility to compensate us for any costs, claims and expenses (including legal) the occurrence of which is possible with the breach by You of these Terms.</p>

        <p>11.2. You agree to defend the interests of the Company (including its employees and management) and reimburse any losses incurred by the Company in consequence of: 11.2.1. breach by You of these Terms;</p>

        <p>11.2.2. violation by You of law or the rights of third parties;</p>

        <p>11.2.3. use Your identity by a third party to access the Web site without Your permission; 11.2.4. the adoption of the winnings received by such violation; 11.3. If You breach the Terms, we have the right to:</p>

        <p>11.3.1. send You a warning of violation, with a request to cease such activity;</p>

        <p>11.3.2. to restrict access to the Web site.</p>

        <p>11.3.3. to deduct the amount spent on a lottery ticket (case), and also to keep the gained goods due to any breach by You of the Terms of the website;</p>

        <br>
        <h2 style="font-family:lithos-pro">12. INTELLECTUAL PROPERTY RIGHT</h2>

        <p>12.1. The contents of this Web site are governed by the copyright law and other intellectual property rights. All material available for download on the Web site may only be downloaded to a single personal computer and/or printed solely for personal, non-commercial use.</p>

        <p>12.2. All Intellectual Property Rights in the Site shall be owned by us absolutely and shall not be reproduced without our prior written consent. Subject to the licence granted to you to use the Site, we reserve all rights, title and interest in our Intellectual Property Rights in the Site.</p>

        <p>12.3 In these Terms, 'Intellectual Property Rights' means patents, database rights, copyright, domain names, design rights (whether registered or unregistered) and/or other similar rights, together with the right to apply for the protection of any such rights.</p>

        <p>12.4. You are responsible for damages and expenses incurred by the Company in connection perfect You in prohibited activities. If You become aware of the Commission of prohibited activities by anyone in the Company, You must promptly inform us, and cooperate fully with the Company in the conduct of investigations.</p>

        <br>
        <h2 style="font-family:lithos-pro">13. YOUR PERSONAL DATA</h2>

        <p>13.1. The Company will take all the necessary measures for the proper and safe storage of the User’s collected Personal Information in accordance with the conditions of this Policy. All information provided by User is stored on the servers of the Company . We are very serious in respect to obtained Your personal information. We will process personal information provided by You strictly in accordance with the privacy policy.</p>

        <p>13.2. You are giving us the right to process Your personal information for the purposes described in the Terms of use of the website.</p>

        <p>13.3. Personal Information received by the Company can only be accessed by the Company’s employees and representatives as well as the affiliates of the Company in possession of the required clearance.</p>

        <p>13.4. All your correspondence for accurate recording of all information received from You.</p>

        <br>
        <h2 style="font-family:lithos-pro">14. PAYMENTS PROCESSING</h2>

        14.1 When you use G2A Pay services provided by G2A.COM Limited (hereinafter referred to as the "G2A Pay services provider") to make a purchase on our website, responsibility over your purchase will first be transferred to G2A.COM Limited before it is delivered to you. G2A Pay services provider assumes primary responsibility, with our assistance, for payment and payment related customer support. The terms between G2A Pay services provider and customers who utilize services of G2A Pay are governed by separate agreements and are not subject to the Terms on this website. </p>

        <p>14.2 With respect to customers making purchases through G2A Pay services provider checkout, (i) the Privacy Policy of G2A Pay services provider shall apply to all payments and should be reviewed before making any purchase, and (ii) the G2A Pay services provider Refund Policy shall apply to all payments unless notice is expressly provided by the relevant supplier to buyers in advance. In addition the purchase of certain products may also require shoppers to agree to one or more EndUser License Agreements (or "EULAs") that may include additional terms set by the product supplier rather than by Us or G2A Pay services provider. You will be bound by any EULA that you agree to. </p>

        <p>14.3 We and/or entities that sell products on our website by using G2A Pay services are primarily responsible for warranty, maintenance, technical or product support services for those Products. We and/or entities that sell products on our website are primarily responsible to users for any liabilities related to fulfillment of orders, and EULAs entered into by the End-User Customer. G2A Pay services provider is primarily responsible for facilitating your payment. </p>

        <p>14.4 You are responsible for any fees, taxes or other costs associated with the purchase and delivery of your items resulting from charges imposed by your relationship with payment services providers or the duties and taxes imposed by your local customs officials or other regulatory body. For customer service inquiries or disputes, You may contact us by email at support@csgotower.net. Questions related to payments made through G2A Pay services provider payment should be addressed to support@g2a.com. Where possible, we will work with You and/or any user selling on our website, to resolve any disputes arising from your purchase.</p>

        <br>
        <h2 style="font-family:lithos-pro">15. INTERNET COOKIES</h2>

        <p>15.1. Cookies are necessary to ensure the health of the website. The use of these files makes a visit to the Web site more convenient and simple. For more information about managing cookies can be found at www.aboutcookies.org. Ban or deleting cookies may limit your access to some features and sections of the website.</p>

        <br>
        <h2 style="font-family:lithos-pro">16 COMPLAINTS AND NOTIFICATIONS</h2>

        <p>16.1. In case of having any complaint or grievance concerning the operation of the website, you must promptly contact customer support in any convenient way for You.</p>

        <p>16.2. You agree that information on the server is final and decisive evidence in determining the outcome of any disputes.</p>

        <p>16.3. You accept the fact that the outcome of the bets are determined by our random number generator, and accept the outcomes of all stakes and games. If any discrepancies in the display of the balance in Your account or outcomes in Your browser and on the server of the Company, the records on the server are true and final.</p>

        <br>
        <h2 style="font-family:lithos-pro">17. INTERPRETATION</h2>

        <p>17.1. The original text of these terms and Conditions are written in Russian. Any interpretation and the interpretation should be based on the Russian text. In all cases and under all circumstances, the Russian version of terms and Conditions takes precedence over versions in other languages.</p>

        <br>
        <h2 style="font-family:lithos-pro">18. TRANSACTION OF RIGHTS</h2>

        <p>18.1. The company reserves the right to assign, transfer or impact Conditions in the Deposit is partly or fully to any person. Such transfer may be made without Your consent and will be held on no less favorable terms for You.</p>

        <br>
        <h2 style="font-family:lithos-pro">19. FORCE MAJEURE CIRCUMSTANCES</h2>

        <p>19.1. In case of circumstances of force majeure we are not liable for any failure or delay in performing any of our obligations. Circumstances of force majeure include war, civil unrest, natural disasters, failures in public communications, DDOS-attacks and industrial disputes.</p>

        <p>19.2. During Force majeure the company shall be considered as suspended. On all obligations set out in these Terms, is deferred until the end of the period of Force majeure. The company will do everything possible and use all available resources to find ways in which we could fulfil the obligations despite the Force majeure.</p>

        <br>
        <h2 style="font-family:lithos-pro">20. REPUDIATION</h2>

        <p>20.1. If the Company is unable to secure the performance by You of any obligations, or if the Company is unable to exercise any legal remedy, it shall not constitute a waiver of such rights and shall not relieve You from compliance with the obligations provided in these Terms.</p>

        <p>20.2. Our disclaimer is legally binding only if it is formalized and forwarded to You in writing.</p>

        <br>
        <h2 style="font-family:lithos-pro">21. SEVERABILITY</h2>

        <p>21.1. In case of losing legal force by provision of the Terms otherwise, such provision will be separated from the other Conditions. The remaining terms and conditions will remain valid and will not lose legal force in this division. If necessary, we can modify the unenforceable part hereof in accordance with applicable legal and legitimate demands.</p>

        <br>
        <h2 style="font-family:lithos-pro">22. JURISDICTION</h2>

        <p>22.1. These terms are interpreted and are subject to the jurisdiction and the laws of Curasao . As a user, you implicitly agree to the exclusivity rights of the courts of Curacao regulate claims and disputes arising in connection with any legal requirements of all parties in any way relating to these Terms or use</p>
    </div>
</div>