<div class="faq-page">
    <div class="faq-page-title"><h3>Faq</h3></div>
    <div class="faq-page-items">

        <div class="column">
            <div class="faq-page-item">
                <div class="faq-page-item-title">INTRODUCTION</div>
                CSGO Tower is one of the oldest and largest CSGO Gambling sites. Since our launch in early 2018, we've hosted well over 2 million registered users and we've since seen the largest gambling CSGO community evolve!

                The way to bet your skins is simple. Head to the deposit page to exchange your skins for coins, and then you can start playing on roulette. If you win, you can exchange the coins back to skins from the withdraw page. You can try out the games by using the referral code "CSGOTOWER" for free 0.50 coins to get started!
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">HOW CAN I DEPOSIT?</div>
                Simply, head over to the deposit page and select the deposit option of your choice. To select one of the Steam games, press its tab and select the skin(s) you'd like to deposit.

                We are one of the only sites who do not apply any fees when depositing and withdrawing. We accept nearly all CSGO, Dota 2 items that are worth more than 1.00 coin.

                If you don't want to deposit using skins, fear not! You are also able to deposit with cryptocurrencies and G2A Pay. If you have any issues or questions regarding depositing, our live support team is always happy to help!
            </div>

                    <div class="faq-page-item">
                <div class="faq-page-item-title">I'M GETTING AN ERROR WHEN TRYING TO DEPOSIT/WITHDRAW</div>
                        Sometimes Steam can experience issues which may prevent our bots from sending trade offers, and similar problems. Usually when this happens, you just need to wait a few minutes for Steam to resolve these issues automatically.

                        If the above doesn't help and you are still facing issues, feel free to contact support. Our live support team is open 7 days a week, 365 days a year and will be more than happy to help!
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">MY SKIN IS NOT ACCEPTED BY YOUR SITE</div>
                Sometimes when the price of an item is too volatile, we may prevent it from being deposited. If your item should be worth more than 1.00 coin but still isn't accepted, you can leave a message with our support team, which will fix it as soon as possible.

                Please note, your skin may not show up in the deposit page if you have recently unboxed it from a case, or if you bought it from the Steam marketplace. Furthermore, if the skin has recently been traded, the skin can be affected by a 7 day trade hold.

                You must wait 7 days before those items can be accepted.
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">COIN RAINS</div>
                At random times, COIN RAIN will appear in the chat! Simply click on it and you will receive free coins!

                Just like with the daily free case, the amount you receive from rains depends on your recent bet amounts. Some of the biggest bettors have received more than 2,000.00 coins from a single rain!
            </div>
            <div class="faq-page-item">
                <div class="faq-page-item-title">CAN I GET FREE COINS/SKINS?</div>
                There are a few ways to get coins for free. First of all, you can get free 0.50 coins by using the referral code "CSGOTOWER". Second, you can get free coins every single day by claiming your daily free coins. Third, stay active in the chat and keep your eyes open for the COIN RAINS! And last but not the least, we're known for daily giveaways on our Twitter page!
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">CAN I GET PAID FOR PROMOTING THE SITE?</div>
                Yes! Anyone can earn coins by inviting new users to CSGOTower . New users will receive 0.50 coins for using your referral code and you will get a percentage of all the coins they wager. You can find your referral code from the "REFERRALS" tab.

                If you have over 10000 followers on any social media platform, you can use the advertisement contact form on the website to get in touch with us about being sponsored.
            </div>

        </div>



        <div class="column">
            <div class="faq-page-item">
                <div class="faq-page-item-title">MY SKINS ARE UNDERPRICED!</div>
                As we accept well over 10,000 different kinds of skins, it can be tricky to have perfectly accurate prices for every single item.

                However, if you believe that your item is undervalued in the deposit page, please contact our live support team and we will balance the price as soon as possible.
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">WHY CAN'T I WITHDRAW?</div>
                Before withdrawing, you must wager at least 50% of your total deposit value. This rule is in place to prevent malicious users from using our marketplace as a trading platform.
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">WHAT IS THE MAXIMUM BET AMOUNT?</div>
                Currently the highest possible bet amount is 100,000 coins on roulette and 3,000 coins. If you're interested in placing bigger bets, please contact our support team and we'll increase the limits temporarily.

                At all times, we have a massive surplus of skins and if needed, we're able to acquire plenty upon demand. You will never have to worry about us running out of skins! Within the past 2 years of our operations, paying out the winnings has never been a problem – and never will be!
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">HOW CAN I TRUST THAT THE GAME RESULTS ARE LEGITIMATE?</div>
                We utilize the strongest possible provable fairness which means you don't have to trust us. Each roll is mathematically proven and verifiable to be provably and manipulation free.

                By using simple cryptography, we can 100% prove, not just promise, that each roll is random and honest.

                Because at the end of the day, why trust people when you can trust numbers?

                The technical specifications of our provably fair system are listed here.
            </div>

            <div class="faq-page-item">
                <div class="faq-page-item-title">HOW DOES THE BONUS POT WORK?</div>
                In roulette, 0.4% of all bets will be combined into one huge bonus pot. In each round, there is a small chance for the roulette wheel to land on the special bonus pot icon! When that happens, the bonus pot is split proportionally between everyone who placed a bet on that round!

                You must have "csgotower.net" in your Steam username in order to participate in bonus pots.

                The chance of hitting the bonus pot is 1 million divided by the total amount of coins wagered on that round, which means that the higher amount of coins there is in that round, the higher is the chance for the bonus pot to appear!
            </div>
            <div class="faq-page-item">
                <div class="faq-page-item-title">HOW DOES THE DAILY FREE WORK?</div>
                You can open a daily free case, which can contain a Sand Dune, coins or even a Dragon Lore! The size of the coin prizes are based on your recent bet amounts and in some cases it can reach very high amounts. Some highrollers have received as high as 1,500.00+ coins from the daily gift at once!

                CSGOTower offers by far the most generous player reward systems and we return a very significant amount back to the users. We're giving away well over 15,000.00 coins every single day in different bonuses, such as the daily free cases and coin rains.
            </div>


            <div class="faq-page-item">
                <div class="faq-page-item-title">DIDN'T FIND AN ANSWER TO YOUR QUESTION?</div>
                If you ever have any questions, or just want to say hi, feel free to contact our live support by clicking the "Support" tab in the navbar. Our support team will respond to you in a matter of minutes. Our support team is available 7 days a week, 365 days a year, to ensure you the best possible gambling experience.
            </div>
        </div>
    </div>
</div>