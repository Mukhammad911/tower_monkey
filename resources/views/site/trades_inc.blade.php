<div class="content live-trade">
    <h2 class="h2">{{ trans('messages.bots_trades') }}</h2>
    <div class="container-fluid">
        <div class="trade-table">
            <div class="trade-title">
                <div class="trade-id td">id {{ trans('messages.trade\'s') }}</div>
                <div class="trade-bot td">{{ trans('messages.bot') }}</div>
                <div class="trade-user td">{{ trans('messages.user') }}</div>
                <div class="trade-item td">{{ trans('messages.item') }}</div>
                <div class="trade-time td">{{ trans('messages.time') }}</div>
            </div>
            <div class="trade-body">
                @foreach($trades as $trade)
                    <div class="tr" id="{{ $trade->offer_id }}">
                        <div class="trade-id td">{{ $trade->offer_id }}</div>
                        <a target="_blank" href="https://steamcommunity.com/profiles/{{ $trade->bot->steamid }}/inventory" class="trade-bot td">{{ $trade->bot->steamid }}</a>
                        <a href="{{ url('profile') }}/{{ $trade->user->steamid }}" class="trade-user td"><img src="{{ $trade->user->avatar }}" alt="" class="user-pic"><span>{{ $trade->user->username }}</span></a>
                        <div class="trade-item td"><img src="https://steamcommunity-a.akamaihd.net/economy/image/{{$trade->image_hash}}/100fx75f/image.png" alt=""><span>{{ Helper::marketName($trade) }}</span></div>
                        <div class="trade-time td">{{ $trade->updated_at }}</div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


@section('js')
    <script>
        String.prototype.replaceObj = function(obj) {
            var replaceString = this;

            $.each(obj, function( key, value ) {
                replaceString = replaceString.replace(key, value);
            });

            return replaceString;
        };

        String.prototype.ucwords = function(){
            var str = this;

            return str.replace(/^(.)|\s(.)/g, function ( $1 ) { return $1.toUpperCase ( ); } );
        };

        function addN(n){
            return (n<10) ? '0'+n : n;
        }

        function getNow(){
            var today   = new Date();
            var dd      = addN(today.getDate());
            var mm      = addN(today.getMonth()+1);
            var yyyy    = addN(today.getFullYear());
            var H       = addN(today.getHours());
            var i       = addN(today.getMinutes());
            var s       = addN(today.getSeconds());

            return yyyy+'-'+mm+'-'+dd+' '+H+':'+i+':'+s;
        }

        function parseMarketName(nameString) {
            var productNameDetails = String(nameString).split("|");
                productNameDetails[0] = productNameDetails[0]
                    .replace('StatTrak\u2122 ', '') //Remove StatTrak™ from name
                    .replace('Souvenir ', ''); //Remove Souvenir from name
            var productName = productNameDetails[0].trim().toLowerCase();
            var productDescription = (productNameDetails[1]) ? productNameDetails[1].trim().toLowerCase() : "";

            return {
                name: productName,
                short_description: productDescription
            };
        };

        function marketName(item){
            var parsedMarketName = parseMarketName(item.marketName);
                item.name =  parsedMarketName.name;
                item.descr =  parsedMarketName.short_description;

            var name = (Lang.has('weapon.'+item.name)) ? Lang.get('weapon.'+item.name) : item.name,
                descr = Lang.get('weapon.'+item.descr),
                quality = Lang.get('weapon.'+item.quality);

            var obj = {};
                obj[parsedMarketName.name] = name;
                obj[parsedMarketName.short_description.ucwords()] = descr;

            return item.marketName.replaceObj(obj)+' ('+quality+')';
        }

        function itemTemplate(obj){
            var now = getNow();
            var html = '<div class="tr">';
                html += '<div class="trade-id td">'+obj.offerId+'</div>';
                html += '<a target="_blank" href="https://steamcommunity.com/profiles/'+obj.botSteamId+'/inventory" class="trade-bot td">'+obj.botSteamId+'</a>';
                html += '<a href="'+BASE_URL+'/profile/'+obj.user.steamid+'" class="trade-user td"><img src="'+obj.user.avatar+'" alt="" class="user-pic"><span>'+obj.user.username+'</span></a>';
                html += '<div class="trade-item td"><img src="https://steamcommunity-a.akamaihd.net/economy/image/'+obj.item.imageUrl+'/100fx75f/image.png" alt=""><span>'+marketName(obj.item)+'</span></div>';
                html += '<div class="trade-time td">'+now+'</div>';
                html += '</div>';

            return html;
        }

        $(function () {
            var socket = io(window.location.hostname+':'+LIVE_DROP_PORT);

            socket.on("live-trades",function(message){
                if($('.trade-body .tr').length == 20){
                    $('.trade-body .tr:last').remove();
                }

                var html = itemTemplate(message);

                if(!$('.trade-body .tr#'+message.offerId).length){
                    $('.trade-body').prepend(html);
                }
            });
        })
    </script>
@endsection