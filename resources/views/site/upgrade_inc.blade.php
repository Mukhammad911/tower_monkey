@if(!Auth::check())
    @include('site.must-be-logged_inc')
@else
<div id="content">
    <div class="upgrade-page">
        <div class="upgrade-page-title" id="anchor-upgrade-title"></div>

        <div class="upgrade-page-chance">
            <div class="upgrade-page-timer-btn"><span id="info_span"></span></div>
            <div class="upgrade-page-chance-arrow-block">
                <span class="upgrade-page-chance-arrow"></span>
                <span class="upgrade-page-chance-arrow"></span>
                <span class="upgrade-page-chance-arrow"></span>
            </div>
            <div class="upgrade-page-chance-in">
                <span id="upgrade_crd_in">ENTER THE AMOUNT</span>
            </div>
            <div class="upgrade-page-chance-out">
                <span id="upgrade_crd_out">???</span>
            </div>
        </div>

        <div class="upgrade-page-check">
            <form action="">
                <b><input type="number" id="upgrade_amount" placeholder="ENTER THE AMOUNT"><span>SCOR</span></b>
                <ul class="reset upgrade-page-check1">
                <?php $i = 0; ?>
                    @foreach($settings as $setting)
                        <li class="multiplier @if($i == 0) upgrade-page-checked @endif" data-val="{{ $setting['multiplier'] }}" data-chance="{{ $setting['chance'] }}" data-active="true">{{ floatval($setting['multiplier']) }}X</li>
                        <?php $i++; ?>
                    @endforeach
                    <li class="multiplier custom_multiplier">Custom item</li>
                </ul>
                <button class="upgrade_btn">Upgrade money</button>
            </form>
            <div class="upgrade-page-check2">
                <div class="upgrade-page-multi">
                    <span>WIN CHANCE <b class="current_chance">{{ $settings[0]['chance'] }}%</b></span>
                    <span>MULTIPLIER <b class="current_multiplier">{{ $settings[0]['multiplier'] }}x</b></span>
                </div>
                <a href="#?w=630" class="credits-cases-check-numbers get-modal" data-rel="popup-provably-fair">Provably fair</a>
            </div>
        </div>
    </div>

    <div class="case-page-contains upgrade-page-nominal custom-nominal-width">
        <div class="case-page-contains-title">
            <h4>Choose nominal</h4>
            <a href="#?w=430" data-rel="popup-balance" class="add-balance">Deposit</a>
            <a href="/csgo">WITHDRAW</a>
        </div>

        <div class="case-page-contains-items">
        @foreach($products as $product)
            <a href="" class="case-page-contains-item upgrade_nominal" data-price="{{ ($product->price / 100) }}">
                <img src="/images/products/{{ $product->image }}" alt="">
            </a>
            @endforeach
        </div>
    </div>
</div>
<style type="text/css">
    #wrapper.wrapper-login:after {
        background-color: rgba(17,17,38,0.5)
    }
</style>
@endif

