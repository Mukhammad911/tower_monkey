<div class="guarantee-page">
    <div class="guarantee-page-title"><h3>Guarantee</h3></div>
    <div class="guarantee-page-items">
        <div class="guarantee-page-item">
            <div class="guarantee-page-item-title">RELIABLE</div>
            <span>All your skins are securely stored in your inventory. At any time during the hour after opening case you can go to your profile and send skins to your Steam account. If within an hour skin is not sent - it will be auto-sold by market price and fill your balance</span>
        </div>
        <div class="guarantee-page-item">
            <div class="guarantee-page-item-title">Qualitatively</div>
            <span>You can always check our Bot's Live Trades page to see what skins we are sending to our users. We always try to fill our cases with factory new and minimal wear exteriors.</span>
        </div>
        <div class="guarantee-page-item">
            <div class="guarantee-page-item-title">Fast</div>
            <span>You will be pleasantly surprised by the trade speed to Steam by our bots. Transfer is fully automated and executed by high-speed bots which have been optimized specifically for use with Steam.</span>
        </div>
    </div>
    <a href="" class="guarantee-page-faqlink">FAQ</a>
</div>