<div id="content" class="pubg-case-bg">
    <div class="profile-page">
        @if($own)
           {{-- <div class="profile-page-title"><h3>{{trans('messages.personal_area')}}</h3></div> --}}
            <div class="profile-page-area">
                    <div class="profile-page-area-left">
                        <div class="profile-page-area-left-ava"><span><img src="{{ Auth::user()->avatar }}" alt=""></span></div>
                        <div class="profile-page-area-left-info">
                            <a href="#?w=430" data-rel="popup-balance" class="spider-btn-success add-balance"><span>{{trans('messages.add_balance')}}</span></a>
                            <a href="https://steamcommunity.com/profiles//{{ Auth::user()->steamid }}" class="spider-btn-success-steam add-balance" target="_blank">
                                <span>Steam Profile</span>
                            </a>
                        </div>
                        <div class=".profile-page-area-left-info1">
                        <b class="profile_game">{{ $count }} {{trans('messages.cases_opened')}}</b>
                        <span class="case_contains_price_product_csgo_ava"><strong>{{ Helper::get('balance')->summ }} <i><span class="coins_profil1">coins</span></i></strong></span>
                        </div>
                    </div>


                    <div class="profile-page-area-right">
                        <div class="profile-page-area-right-promocode">
                            <div class="profile-page-area-right-promocode-title"></div>
                            <div class="profile-page-area-left-login">Welcome, {{ $user->username }}!</div>
                        </div>


                        <div class="profile-page-area-right-promocode">
                            <div class="profile-page-area-right-promocode-title">
                                {{ trans('messages.your_link_exchange') }}
                                <a target="_blank" href="http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url" class="link">({{ trans('messages.copy_steam_link') }})</a>
                            </div>
                            <form name="trade" class="profile-trade-url">
                               <div class="trade_url"> <input type="url" name="trade_url_from_profile" placeholder="https://steamcommunity.com/tradeoffer/new/?partner=XXXXXXXX&amp;token=XXXXXXXX" @if($user->trade_url!='') value='{{$user->trade_url}}' @endif></div>
                                <button class="btn-save-profile-url-trade"><span>{{ trans('messages.save_profile_link') }}</span></button>
                            </form>

                            {{--<div class="tradelink-block">
                                <form name="trade" class="input-button profile-trade-url">
                                    <input type="url" name="trade_url_from_profile" style="color: #000000;"
                                           placeholder="https://steamcommunity.com/tradeoffer/new/?partner=393102322&amp;token=XXXXXXXX"
                                        @if($trade_url_user!='')
                                        value='{{$trade_url_user}}'
                                        @endif
                                    >
                                    <button class="buttonlink button-primary btn-save-profile-url-trade">{{ trans('messages.save_profile_link') }}</button>
                                </form>
                            </div>--}}
                        </div>


                    </div>
                </div>
        @else
          {{--  <div class="profile-page-title page-title-other"><h3>{{trans('messages.profile')}}</h3></div> --}}
            <div class="profile-page-area">
                <div class="profile-page-area-left-other profile-page-other">
                    <div class="profile-page-area-left-ava"><span><img src="{{ $user->avatar }}" alt=""></span></div>
                    <div class="profile-page-area-left-login-other">{{ $user->username }}</div>
                    <div class="profile-page-area-left-info">

               <b>{{ $count }}</b> {{trans('messages.cases_opened')}}<br/><br/>
                        <a href="https://steamcommunity.com/profiles//{{$user->steamid }}" class="spider-btn-success-steam add-balance" target="_blank"><span>Steam Profile</span></a>
                    </div>
                </div>
            </div>
        @endif
    </div>

    @if(sizeof($profile_drops) > 0)
        <div class="case-pending-items-block">
            <div class="case-page-contains-title case-win-page-can">
                <div class="container_for_get">
                    <h4 class="text_waiting">CASE STORIES</h4>
                </div>
            </div>
            @if ($own)
                <div class="profile-actions-block">
                    <a href="javascript:;" class="sell-all-csgo {{($pending_csgo_count == 0) ? "disabled" : ""}}" data-text="sell all items"><span>sell all cs:go items</span></a>
                    <a href="javascript:;" class="sell-all-pubg {{($pending_pubg_count == 0) ? "disabled" : ""}}" data-text="sell all items"><span>sell all pubg items</span></a>
                </div>
            @endif
            <div class="case-page-contains-items case-games-drops-profile">
                @foreach($profile_drops as $item)
                    @if(!empty($item->id))
                        @if($item->profile_drop_type == 'csgo')
                            <div class="case-winned-item csgo-rarity-{{$item->class}} {{($item->stattrak) ? "stattrak" : ""}}" data-id="{{$item->id}}" data-drop-type="csgo">
                                @if ($own)
                                    @if ($item->status == 'pending')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <div class="send-item">
                                                <a target="_blank" data-id="{{$item->id}}" data-drop-type="csgo" rel="noreferrer noopener" href="javascript:;">Send</a>
                                            </div>
                                            <div class="sold-item">
                                                <a target="_blank" data-id="{{$item->id}}" data-drop-type="csgo" rel="noreferrer noopener" href="javascript:;">Sell for <span class="case-win-item-price-coins"></span> {{Helper::currentPrice($item->price)}}</a>
                                            </div>
                                        </div>
                                    @elseif ($item->status == 'requested')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-clock-o"></i></span>
                                                Requested
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @elseif ($item->status == 'offered')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="fa fa-paper-plane-o"></i></span>
                                                Offered
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @else
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @endif
                                @else
                                    @if ($item->status == 'pending' || $item->status == 'requested' || $item->status == 'offered')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-arrow-down"></i></span>
                                                Waiting
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @else
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @endif
                                @endif

                                @if ($own)
                                    <div class="get" style="display: {{($item->status == 'offered' ? "block" : "none")}};">
                                        <a target="_blank" rel="noreferrer noopener" href="{{($item->offer_id) ? "https://steamcommunity.com/tradeoffer/".$item->offer_id : ""}}">Get</a>
                                    </div>
                                @endif

                                <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image" style="width: 120px;">
                                <img src="/{{ $item->case_image }}" class="top1-image" alt="case">
                                <p class="case-winned-item-name" style="text-align: left;">{{$item->nameParsed}}<br> {{$item->shortDescriptionParsed}}</p>
                            </div>
                        @elseif($item->profile_drop_type == 'pubg')
                            <div class="case-winned-item pubg-rarity-{{$item->product->class}}" data-id="{{$item->id}}" data-drop-type="pubg">
                                @if ($own)
                                    @if ($item->status == 'pending')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <div class="send-item">
                                                <a target="_blank" data-id="{{$item->id}}" data-drop-type="pubg" rel="noreferrer noopener" href="javascript:;">Send</a>
                                            </div>
                                            <div class="sold-item">
                                                <a target="_blank" data-id="{{$item->id}}" data-drop-type="pubg" rel="noreferrer noopener" href="javascript:;">Sell for <span class="case-win-item-price-coins"></span> {{Helper::currentPrice($item->price)}}</a>
                                            </div>
                                        </div>
                                    @elseif ($item->status == 'requested')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-clock-o"></i></span>
                                                Requested
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @elseif ($item->status == 'offered')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="fa fa-paper-plane-o"></i></span>
                                                Offered
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @else
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @endif
                                @else
                                    @if ($item->status == 'pending' || $item->status == 'requested' || $item->status == 'offered')
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-arrow-down"></i></span>
                                                Waiting
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @else
                                        <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                                            <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                                                {{Helper::currentPrice($item->price)}}
                                            </span>
                                        </div>
                                    @endif
                                @endif

                                @if ($own)
                                    <div class="get" style="display: {{($item->status == 'offered' ? "block" : "none")}};">
                                        <a target="_blank" rel="noreferrer noopener" href="{{($item->offer_id) ? "https://steamcommunity.com/tradeoffer/".$item->offer_id : ""}}">Get</a>
                                    </div>
                                @endif

                                <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image">
                                <img src="/{{ $item->case_image }}" class="top1-image" alt="case">
                                <p class="case-winned-item-name">{{$item->market_name}}</p>
                            </div>
                        @endif
                    @endif
                @endforeach
            </div>
        </div>

        @if($drops_count > 50)
            <div style="text-align: center;">
                <a id="next50caseDrops" class="preload load-more-drops" data-user-id="{{ $user->id }}">
                    <span id="n50CaseDrops">{{ trans('messages.next50_drops') }}</span>
                </a>
                <div id="preload50" class="loading_case_drops_profile" style="display: none;"><img class="loader_image" src="/images/csgotower/s_l.gif"></div>
            </div>
        @endif
    @endif

    <div class="case-page-contains withdrawals-stories">
        <div class="case-page-contains-title case-win-page-can">
            <h4>{{trans('messages.market_stories')}}</h4>
        </div>

        <div class="case-page-contains-items market-drops-profile">
            @foreach($profile_drops_market as $item)
                @if(!empty($item->id))
                    @if($item->profile_drop_type == 'csgo')
                        <div class="market-withdrawed-item csgo-rarity-{{$item->class}} {{($item->stattrak) ? "stattrak" : ""}}" data-id="{{$item->id}}">
                            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                <span class="status">
                                    <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                    {{ "Accepted" }}
                                </span>
                                <span class="price">
                                    <span class="case-win-item-price-coins"></span>
                                    {{Helper::currentPrice($item->price)}}
                                </span>
                            </div>

                            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image" style="width: 120px;">

                            <p class="case-winned-item-name" style="text-align: left;">{{$item->nameParsed}}<br> {{$item->shortDescriptionParsed}}</p>
                        </div>
                    @elseif($item->profile_drop_type == 'dota')
                        <div class="market-withdrawed-item dota-rarity-{{$item->class}}" data-id="{{$item->id}}">
                            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                <span class="status">
                                    <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                    {{ "Accepted" }}
                                </span>
                                        <span class="price">
                                    <span class="case-win-item-price-coins"></span>
                                            {{Helper::currentPrice($item->price)}}
                                </span>
                            </div>

                            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360x360" alt="drop" class="drop-image" style="width: 137px; top: 31px;">

                            <p class="case-winned-item-name">{{$item->market_name}}</p>
                        </div>
                    @elseif($item->profile_drop_type == 'pubg')
                        <div class="market-withdrawed-item pubg-rarity-{{$item->product->class}}" data-id="{{$item->id}}">
                            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                            <span class="status">
                                <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                {{ "Accepted" }}
                            </span>
                                <span class="price">
                                <span class="case-win-item-price-coins"></span>
                                    {{Helper::currentPrice($item->price)}}
                            </span>
                            </div>

                            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image">

                            <p class="market-withdrawed-item-name">{{$item->market_name}}</p>
                        </div>
                    @endif
                @endif
            @endforeach
        </div>

        @if($drops_count_market > 50)
            <div style="text-align: center;">
                <a id="next50marketDrops" class="preload load-more-drops" data-user-id="{{ $user->id }}">
                    <span id="n50MarketDrops">{{ trans('messages.next50_drops') }}</span>
                </a>
                <div id="preload50market" class="loading_market_drops_profile" style="display: none;"><img class="loader_image" src="/images/csgotower/s_l.gif"></div>
            </div>
        @endif

    </div>
</div>