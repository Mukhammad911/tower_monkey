@extends('layouts.layout')

@section('drops')
    @include('site.partials.drops')
@endsection

@section('breadcrumbs')
    <div class="container-fluid">
        {!! Breadcrumbs::render('site-reviews') !!}
    </div>
@endsection

@section('content')
@endsection