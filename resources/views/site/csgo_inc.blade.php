@section('css')
    <style type="text/css">
       
    </style>
@stop
    @if(Auth::check())
            <div id="content" data-ng-app="spiderCsgo" data-ng-controller="MyCtrl2" data-ng-init="init()" style="
    background-size: cover;
    min-height: 840px;
        ">
            <div  class="loading_market market"><img class="loader_image" src="/images/csgotower/s_l.gif"></div>
            <div class="shop-page">
                <div class="shop-page-top">
                        <div class="app-selector">
                                <a href="/csgo" class="csgotower-btn-market active_market"><span>CS:GO</span></a>
                                <a href="/dota2" class="csgotower-btn-market"><span>DOTA2</span></a>
                               {{-- <a href="/pubg" class="csgotower-btn-market"><span>PUBG</span></a> --}}
                            </div>

                    <div class="shop-page-leftbar-search">    
                        <input id="search_form" data-ng-model="searchString"
                         data-ng-change="searchStringClick()"
                         type="text" placeholder="{{trans('messages.enter_item_name')}}"><button>search</button>
                    </div>
                    <div class="shop-page-leftbar-form">

                        <div class="custom_select">
                            <div data-ng-click="select_box()" class="all_container_sort sort_by_price">{{trans('messages.sort_item_by_price')}}
                            </div>
                            <span
                                    id="arrow_image" class="span_image_arrow"></span>
                            <div id="select_box" style="display: none;" class="container_list_select">
                                <div data-ng-click="asc()" class="list_of_select">{{trans('messages.price_low')}}</div>
                                <div data-ng-click="desc()" class="list_of_select">{{trans('messages.price_high')}}</div>
                            </div>

                        </div>

                        <div class="custom_select_qyality">
                            <div data-ng-click="csgo_select_box_quality()" class="all_container_sort sort_by_quality">QUALITY
                            </div>
                            <span
                                    id="arrow_image" class="span_image_arrow"></span>

                            <div id="select_box_quality" style="display: none;" class="container_list_select_hero">
                                <div class="scrollbar" id="style-2">
                                    <div  data-ng-click="select_qualities('All')" class="list_of_select_hero  "  >All</div>
                                    <div  data-ng-repeat="q in quality_array" data-ng-click='select_qualities("<%q.quality%>")' class="list_of_select_hero "><%q.quality%></div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="shop-page-open"><i></i></div>
                <div class="shop-page-leftbar">
                    <form>
                        <div class="selected-skins-header">
                            <div class="column"><a>{{trans('messages.selected_skins')}}</a></div>
                            <div class="column"><span class="delete_icon"></span><a data-ng-click="refresh_click()" class="column" href="">{{trans('messages.remove_all')}}</a></div>
                        </div>

                        <!--                        <div class="shop-page-leftbar-btns-refresh">-->
                    <!--                            <a data-ng-click="refresh_click()" href="" class="shop-leftbar-btn">{{trans('messages.refresh')}} </a>-->
                        <!--                        </div>-->

                        <div class="show_select_item">
                            <div ng-cloak
                                 data-ng-repeat="a in clear_items"
                                 id="item_resut_<%a.id%>"
                                 class="shop-page-item shop-page-item-right">
                                <div class="shop-page-item-lot shop-page-item-lot-right"><span data-ng-click='delete_from_icon(<%a.id%>,<%"a.img"%>,<%"a.name"%>,<%"a.short_description"%>,<%"a.price"%>)' class="delete_icon_csgo"></span>
                                    <b><%a.stattrak%><%a.name%></b><span><strong><%a.short_description%></strong></span>
                                    <img class="img_result_right" ng-src="https://steamcommunity-a.akamaihd.net/economy/image/<%a.img%>/60fx60f/image.png" alt="">
                                </div>
                                <div class="shop-page-item-price shop-page-item-price-right"><span class="coins coins-right"></span> <%a.price%></div>
                            </div>

                        </div>


                        <div class="shop-page-leftbar-info">
                            <span></span><br>
                            {{trans('messages.withdrawable')}}: <span data-ng-cloak><%balance%></span><span class="coins_info"></span><br>
                            <span></span><br>
                        </div>
                        <div class="shop-page-leftbar-btns">
                            <button data-ng-click="open_modal()"><span>{{trans('messages.withdraw')}}</span></button>
                        </div>
                    </form>
                </div>
                <!-- or dota-bg -->
                <div class="shop-page-items cs-bg">
                    <div ng-cloak
                         data-ng-repeat="a in arms"
                         id="item_<%a.id%>"
                         data-ng-click="getItemId(<%a.id%>,<%'a.img_url'%>,<%'a.name'%>,<%'a.short_description'%>,<%'a.price'%>)" class="shop-page-item">
                        <div class="shop-page-item-lot">

                            <b><%a.stattrak%><%a.name%></b><span><strong><%a.short_description%></strong>(<%a.quality%>)</span>

                            <img ng-src="https://steamcommunity-a.akamaihd.net/economy/image/<%a.img_url%>/90fx90f/image.png" alt="">
                        </div>
                        <div class="shop-page-item-price"><span class="coins"></span> <%a.price%></div>
                    </div>
                    <div class="pagination-div">
                        <ul ng-cloak data-ng-if="pager.pages.length" class="pagination-div">

                            <li data-ng-click="setPage(pager.currentPage - 1)"  ng-cloak class="li_for_pagination prev_market_page" data-ng-class="{disabled:pager.currentPage === 1}">
                                <a data-ng-click="setPage(pager.currentPage - 1)"><span class="text_pagination"><img src="/images/csgotower/icon_prewu.png" /></span></a>
                            </li>
                            <li data-ng-click="setPage(page)" ng-cloak class="li_for_pagination" data-ng-repeat="page in pager.pages"
                                data-ng-class="{active:pager.currentPage === page}">
                                <a ng-cloak data-ng-click="setPage(page)"><span class="text_pagination"><%page%></span></a>
                            </li>
                            <li data-ng-click="setPage(pager.currentPage + 1)" ng-cloak class="li_for_pagination next_market_page"
                                data-ng-class="{disabled:pager.currentPage === pager.totalPages}">
                                <a data-ng-click="setPage(pager.currentPage + 1)"><span class="text_pagination"><img src="/images/csgotower/icon_next.png" /></span></a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>


            @include('site.partials.modal-withdraw')

            @include('site.partials.modal-trade')

        </div>
    @else
        @include('site.must-be-logged_inc')
    @endif
