<div class="landing-logo">
    <img src="/images/version2/">
</div>
<div class="landing-h2">{{ trans('messages.trade_restrict_head') }}</div>

</br>

<div class="container-fluid restrict-wrap">
    <h3>{{ trans('messages.trade_restrict_b0') }}</h3>
    <p>{{ trans('messages.trade_restrict_p1') }}
        <a href="https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932" target="_blank">
            {{ trans('messages.trade_restrict_p1_a') }}</a>{{ trans('messages.trade_restrict_p1_end') }}</p>
    <p><b>{{ trans('messages.trade_restrict_b1') }}</b></p>
    <p>{{ trans('messages.trade_restrict_p2') }}</p>
    <p><b>{{ trans('messages.trade_restrict_b2') }}</b></p>
    <p>{{ trans('messages.trade_restrict_p3') }}</p>
</div>