@if(!Auth::check())
    @include('site.must-be-logged_inc')
@else
    @include('site.partials.site-stats')
    <div id="content" style="">
        <div class="ladder-page">
            <div class="ladder-page-towers">
                <div class="container" style="padding-left: 0;padding-right: 0;">
                    <div class="row" style="margin: 0;">

                        <div class="ladder-left col-md-4 col-md-push-4 col-sm-12">
                            {{--<img src="/images/csgotower/monkey_head.png" class="monkey_head"/>--}}
                            <img src="/images/csgotower/gorilla.png" class="monkey_head"/>
                            <div class="section">
                                <div class="ladder-left-title"><span class="current-prize">0</span> <span
                                            class="coin_total_title"></span></div>
                                <div class="box visible">
                                    <div class="ladder-left-content">
                                        @for($i = $steps; $i > 0; $i--)
                                            <div class="tower-row " data-row="{{ $i }}">
                                                <span class="tower-cell disabled" data-cell="1">10</span>
                                                <span class="tower-cell disabled" data-cell="2">10</span>
                                                <span class="tower-cell disabled" data-cell="3">10</span>
                                            </div>
                                        @endfor
                                    </div>
                                </div>


                                <div class="ladder-left-foor">
                                    <button class="tower-take-prize" style="display: none;
            width: 86.1%;
            background: #689e90;">{{trans('messages.tower_take1')}} <span
                                                class="tower-current-prize">0</span> {{trans('messages.tower_take2')}}
                                    </button>
                                    <button class="tower-play" data-demo="no">{{trans('messages.start_game')}}</button>
                                    <button class="tower-play" data-demo="yes">{{trans('messages.play_demo')}}</button>
                                </div>

                                <div class="ladder-left-foor2">
                                    <span><b class=" tower_min">{{trans('messages.min')}}</b></span>
                                    <span><b class="ladder-left-active tower_min_plus">-</b></span>
                                    <span><input type="number" class="tower-bet" value="10" min="1" max="5000"
                                                 step="any"><!--CRD--></span>
                                    <span><b class="tower_max_plus">+</b></span>
                                    <span><b class="tower_max">{{trans('messages.max')}}</b></span>
                                </div>
                                {{-- <div class="ladder-left-foor3">
                                     <span><b class=" tower_min_1">+0.01</b></span>
                                     <span><b class=" tower_min_2">+0.10</b></span>
                                     <span><b class=" tower_min_3">+1</b></span>
                                     <span><b class=" tower_min_4">+10</b></span>
                                     <span><b class=" tower_min_5">+100</b></span>
                                     <span><b class=" tower_min_6">+1000</b></span>
                                 </div> --}}

                                <ul class="reset tabs">
                                    <li class="tower-level current" data-level="easy"
                                        data-active="1">{{trans('messages.easy')}}</li>
                                    <li class="tower-level" data-level="medium"
                                        data-active="0">{{trans('messages.medium')}}</li>
                                    <li class="tower-level" data-level="hard"
                                        data-active="0">{{trans('messages.hard')}}</li>
                                </ul>
                                <div class="tower-provably">
                                    <div class="tower-provably"><span>Check winning numbers</span></div>
                                </div>
                                <div class="online_users">
                                    PLAYERS ONLINE
                                    <span class="tower-bets-counter-t tower-bets-counter-top users-online">0</span>
                                </div>


                            </div>
                        </div>

                        <div class="ladder-right col-md-4 col-md-pull-4 col-sm-12" style="padding-right: 25px;">
                            {{--<div class="ladder-right-title"></div>--}}
                            <div class="ladder-right-stats">
                                <span><b class="tower-games-counter">{{ $total_games }}</b> {{trans('messages.games')}}</span>
                                <span><b><span class="tower-bets-counter">{{ $total_bets }}</span></b> {{trans('messages.wagered')}}</span>
                            </div>
                            <div class="ladder-right-usrstitle">
                                <span>{{trans('messages.tower_table_player')}}</span>
                                <span>{{trans('messages.tower_table_bet')}}</span>
                                <span>{{trans('messages.tower_table_profit')}}</span>
                                <span>{{trans('messages.tower_table_step')}}</span>
                                <span>{{trans('messages.tower_table_level')}}</span>
                            </div>

                            <div class="ladder-right-usrs">

                                @foreach($live_games as $live_game)
                                    @if(@$live_game->tower->profit > 0 )
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit win">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @else
                                        <div class="ladder-right-usr">
                                            <a href="/profile/{{@$live_game->tower->user->id}}">
                                                <span class="ladder-right-usr-ava"><img
                                                            src="{{ @$live_game->tower->user->avatar }}" alt=""></span>
                                                <span class="ladder-right-usr-login"> {{ @$live_game->tower->user->username }}</span></a>
                                            <span class="ladder-right-usr-bet">{{ @$live_game->tower->bet }}</span>
                                            <span class="ladder-right-usr-profit neon">{{ @$live_game->tower->profit }}
                                                <span
                                                        class="coin_total_profit">coins</span></span>
                                            <span class="ladder-right-usr-step">{{ @$live_game->tower->step-1 }}</span>
                                            <span class="ladder-ico"><i
                                                        class="ladder-{{ @$live_game->tower->level }}"></i></span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                        </div>

                        <div class="ladder-right ladder-right-top col-md-4 col-sm-12" style="padding-left: 25px;">

                            <div class="ladder-right-stats ladder-right-stats-top">
                                <ul class="nav nav-pills nav-justified">
                                    <li class="active" style="border-top-left-radius: 12px;">
                                        <a class="heroes-stats-link" data-toggle="tab" href="#stats"
                                           style="border-top-left-radius: 12px;">
                                            {{trans('messages.HALL_OF_HEROES')}}
                                            {{--<img src="/images/csgotower/heroes.png">--}}
                                        </a>
                                    </li>
                                    <li style="border-top-right-radius: 12px;">
                                        <a class="heroes-chat-link" data-toggle="tab"
                                           style="border-top-right-radius: 12px;" href="#chat">
                                            {{trans('messages.chat')}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div id="stats" class="tab-pane fade in active">
                                    <div class="ladder-right-usrstitle ladder-right-usrstitle-top">
                                        <span>{{trans('messages.tower_table_rank')}}</span>
                                        <span>{{trans('messages.tower_table_player')}}</span>
                                        <span>{{trans('messages.tower_table_wagered')}}</span>
                                    </div>
                                    <div class="ladder-right-usrs-t ladder-right-usrs-top heroes-rank">
                                        @foreach($top as  $key=>$top_user)
                                            <div class="ladder-right-usr">
                                                <span class="ladder-right-usr-rank">
                                                    <span class="text_top_count">{{$key+1}}</span>
                                                </span>
                                                <a href="/profile/{{$top_user->id}}">
                                                    <span class="ladder-right-usr-login ladder-right-usr-login-top">
                                                        {{$top_user->username }}
                                                    </span>
                                                </a>
                                                <span class="ladder-right-usr-bet ladder-right-usr-bet-top">
                                                    {{ $top_user->profit }} <span class="coins_header_top">coins</span>
                                                </span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="chat" class="tab-pane fade">
                                    <div class="ladder-right-usrs-t ladder-right-usrs-top heroes-chat-container">
                                        <div class="chat-container">
                                            <div class="chat-header">
                                                <div class="chat-selector">
                                                    <ul class="selector-lang">
                                                        <li class="selector-lang-li en">EN</li>
                                                        <li class="selector-lang-li ru">RU</li>
                                                        <li class="selector-lang-li tr">TR</li>
                                                        <li class="selector-lang-li es">ES</li>
                                                        <li class="selector-lang-li de">DE</li>
                                                        <li class="selector-lang-li pl">PL</li>
                                                    </ul>
                                                </div>
                                                <div class="online-user-show-chat">
                                                    <span class="span-user-online">Online:</span>
                                                    <span class="span-user-online users-online">0</span>
                                                </div>
                                            </div>

                                            <div class="chat-body">

                                            </div>

                                            <div class="chat-footer">
                                                <div class="chat-footer-container">
                                                    <input type="text" class="chat-message-box">
                                                    <button type="button" class="chat-message-btn">SEND</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            @include('site.partials.modal-provably-fair-tower')
        </div>
    </div>
@endif