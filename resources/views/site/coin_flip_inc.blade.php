@include('site.partials.site-stats')
<div id="content" class="content-coin-flip">
    <div class="flip_content">
        <div class="flip_header">
            <h2 class="coin_flip_header">COIN FLIP</h2>
            <h4 class="coin_flip_header_history">History</h4>
            <div class="coin_flip_right_content">
                <div class="icon_roll flip_icon_active">

                </div>
                <div class="icon_csgo">

                </div>
                <div class="coin_flip_input">
                    <input type="number" class="coin_flip_input_amount" value="10" min="0.1" max="5000">
                </div>
                <div class="coin_flip_create_game">
                    <button class="coin_flip_button_create_game">CREATE GAME</button>
                </div>
                <div class="coin_flip_get_five_percent">
                    <p class="coin_flip_text_five_percent">Coin flips take 1% commission from the pot</p>
                </div>
            </div>
        </div>
        <div class="flip_title">
            <h2 class="coin_flip_top_games">TOP GAMES</h2>
            <span class="icon_top_games"></span>
            {{--  <div class="flip_right_title">
                  <p class="coin_flip_txt">Games:</p>
                  <p class="coin_flip_txt_value">2</p>
                  <span class="flip_coin_icon_coin"></span>
                  <p class="coin_flip_count_amount">2.01</p>
              </div>--}}
        </div>
        <div  class="loading_market coin_flip"><img class="loader_image" src="/images/csgotower/s_l.gif"></div>
        <div class="flip_content_full"></div>

    </div>
</div>


