<section>
    <div class="swidth">
       <div class="staticpage">
          <div class="profile-common otheruser">
             <!-- Чужой профиль -->
             <div class="o-info">
                <div class="left">
                   <img src="{{ $user->avatar }}">
                </div>
                <div class="right">
                   <nav class="breadcrumbs">
                      <h1>{{ $user->username }}</h1>
                   </nav>
                   <div class="ref-stat">
                      <div class="w25">
                         <a class="iconlink steamacc" href="{{ url('https://steamcommunity.com/profiles/') }}/{{ $user->steamid }}" target="_blank">{{ trans('messages.steam_profile') }}</a>
                      </div>
                      <div class="w25 center">
                         <span>{{ $drops_count }}</span>
                         <i>{{ trans('messages.cases_opened') }}</i>
                      </div>
                   </div>
                </div>
             </div>
                <div id="drops">
                    <div class="items-incase">
                @include('site.partials.profile-guest-drops',$drops)
                    </div>
                </div>

                @if($drops_count > 100)
                   <div style="text-align: center;">
                      <button id="next100"  class="preload load-more-drops" data-user-id="{{ $user->id }}">
                         <span id="n100">↓ {{ trans('messages.next100_drops') }} ↓</span>
                         <img id="preload100" src="/images/preloader_button.gif" style="width: 180px; height: 40px; display: none;" >
                      </button>
                   </div>
                @endif
          </div>
          <!-- profile-common -->
       </div>
       <!-- staticpage -->
    </div>
    <!-- swidth -->
</section>