    @if(Auth::check())
        <div id="content" data-ng-app="spiderPubg" data-ng-controller="MyCtrl5" data-ng-init="init()" style="
            background: url(/images/csgotower/pubg-bg.jpg) no-repeat;
            background-size: cover;
            min-height: 1000px;">
            <div  class="loading_market market"><img class="loader_image"  src="/images/csgotower/s_l.gif"></div>
            <div class="shop-page">

                <div class="app-selector">
                    <a href="/csgo" class="csgotower-btn-market"><span>CS:GO</span></a>
                    <a href="/dota2" class="csgotower-btn-market"><span>DOTA2</span></a>
                    <a href="/pubg" class="csgotower-btn-market active_market"><span>PUBG</span></a>
                </div>

                <div class="shop-page-top-pubg">
                    <div class="shop-page-leftbar-search">    <input id="search_form" data-ng-model="pubg_searchString"
                                                                     data-ng-change="pubg_searchStringClick()"
                                                                     type="text" placeholder="{{trans('messages.enter_item_name')}}..."><button>search</button>
                    </div>
                    <div class="shop-page-leftbar-form">

                        <div class="custom_select_pubg">
                            <div data-ng-click="pubg_select_box()" class="all_container_sort">{{trans('messages.sort_item_by_price')}}
                            </div>
                            <span
                                    id="arrow_image" class="span_image_arrow"></span>
                            <div id="select_box" style="display: none;" class="container_list_select">
                                <div data-ng-click="pubg_asc()" class="list_of_select">{{trans('messages.price_low')}}</div>
                                <div data-ng-click="pubg_desc()" class="list_of_select">{{trans('messages.price_high')}}</div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="shop-page-open"><i></i></div>

                <div class="shop-page-leftbar">
                    <div class="selected-skins-header">
                        <div class="column"><a>{{trans('messages.selected_skins')}}</a></div>
                        <div class="column"><span class="delete_icon"></span><a data-ng-click="pubg_refresh_click()"  class="column" href="">{{trans('messages.remove_all')}}</a></div>
                    </div>
                    <!--                    <div class="shop-page-leftbar-btns-refresh">-->
                <!--                        <a data-ng-click="pubg_refresh_click()" href="" class="shop-leftbar-btn">{{trans('messages.refresh')}} </a>-->
                    <!--                    </div>-->
                    <form>
                        <div class="show_select_item">
                            <div ng-cloak
                                 data-ng-repeat="a in pubg_clear_items"
                                 id="item_resut_<%a.id%>"
                                 class="shop-page-item shop-page-item-right">
                                <div class="shop-page-item-lot shop-page-item-lot-right"><span data-ng-click='pubg_delete_from_icon(<%a.id%>,<%"a.img"%>,<%"a.name"%>,<%"a.price"%>)' class="delete_icon_csgo"></span>
                                    <b><%a.stattrak%><%a.name%></b><span><strong><%a.short_description%></strong></span>
                                    <img class="img_result_right" ng-src="https://steamcommunity-a.akamaihd.net/economy/image/<%a.img%>/60fx60f/image.png" alt="">
                                </div>
                                <div class="shop-page-item-price shop-page-item-price-right"><span class="coins coins-right"></span> <%a.price%></div>
                            </div>


                        </div>

                        <div class="shop-page-leftbar-info">
                            {{trans('messages.withdrawable')}}: <span data-ng-cloak><%pubg_balance%> <span class="coins_info"></span></span><br>
                        </div>
                        <div class="shop-page-leftbar-btns">

                            <button data-ng-click="pubg_open_modal()"><span>{{trans('messages.withdraw')}}</span></button>
                        </div>
                    </form>
                </div>
                <!-- or dota-bg -->
                <div class="shop-page-items pubg-bg">

                    <div ng-cloak
                         data-ng-repeat="a in pubg_arms "
                         id="pubg_item_<%a.id%>"
                         data-ng-click="pubg_getItemId(<%a.id%>,<%'a.img_url'%>,<%'a.name'%>,<%'a.price'%>)" class="shop-page-item">
                        <div class="shop-page-item-lot dota_img_padding shop-page-item-lot-top">
                            <b><%a.name%></b><span><strong></strong></span>
                            <img ng-src="https://steamcommunity-a.akamaihd.net/economy/image/<%a.img_url%>/100fx100f/image.png" alt="">
                        </div>
                        <div class="shop-page-item-price"><span class="coins"></span> <%a.price%></div>
                    </div>
                    <div class="pagination-div">
                        <ul ng-cloak data-ng-if="pubg_pager.pages.length" class="pagination-div">

                            <li data-ng-click="pubg_setPage(pubg_pager.currentPage - 1)" ng-cloak class="li_for_pagination prev_market_page" data-ng-class="{disabled:pubg_pager.currentPage === 1}">
                                <a data-ng-click="pubg_setPage(pubg_pager.currentPage - 1)"><span class="text_pagination"><img src="/images/csgotower/icon_prewu.png" /></span></a>
                            </li>
                            <li data-ng-click="pubg_setPage(page)" ng-cloak class="li_for_pagination" data-ng-repeat="page in pubg_pager.pages"
                                data-ng-class="{active:pubg_pager.currentPage === page}">
                                <a ng-cloak data-ng-click="pubg_setPage(page)"><span class="text_pagination"><%page%></span></a>
                            </li>
                            <li data-ng-click="pubg_setPage(pubg_pager.currentPage + 1)" ng-cloak class="li_for_pagination next_market_page"
                                data-ng-class="{disabled:pubg_pager.currentPage === pubg_pager.totalPages}">
                                <a data-ng-click="pubg_setPage(pubg_pager.currentPage + 1)"><span class="text_pagination"><img src="/images/csgotower/icon_next.png" /></span></a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

            @include('site.partials.modal-withdraw-pubg')

            @include('site.partials.modal-trade-pubg')

        </div>
    @else
        @include('site.must-be-logged_inc')
    @endif
