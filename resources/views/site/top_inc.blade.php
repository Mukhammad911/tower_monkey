<div id="content">
    <div class="top-users-page">
        <div class="top-users-page-title"><h3>{{trans('messages.top_winners')}}</h3></div>
        <div class="top-users-page-items">
            <div class="top-users-page-item">
                <div class="top-users-page-item-ava"><a href="/profile/{{ @$top_general[1]->id }}"><img src="{{ @$top_general[1]->avatar }}" alt=""></a></div>
                <div class="top-users-page-item-info">
                    <b>{{ @$top_general[1]->username }}</b>
                    <strong>{{ number_format(@$top_general[1]->profit, 2) }}</strong> SCOR profit
                    <span><strong>{{ @$top_general[1]->count_drops }}</strong> games</span>
                </div>
                <div class="top-users-page-item-placce">2</div>
                <div class="top_users_page_line"></div>
            </div>

            <div class="top-users-page-item">
                <div class="top-users-page-item-ava"><a href="/profile/{{ @$top_general[0]->id }}"><img src="{{ @$top_general[0]->avatar }}" alt=""></a></div>
                <div class="top-users-page-item-info">
                    <b>{{ @$top_general[0]->username }}</b>
                    <strong>{{ number_format(@$top_general[0]->profit, 2) }}</strong> SCOR profit
                    <span><strong>{{ @$top_general[0]->count_drops }}</strong> games</span>
                </div>
                <div class="top-users-page-item-placce">1</div>
                <div class="top_users_page_line"></div>
            </div>

            <div class="top-users-page-item">
                <div class="top-users-page-item-ava"><a href="/profile/{{ @$top_general[2]->id }}"><img src="{{ @$top_general[2]->avatar }}" alt=""></a></div>
                <div class="top-users-page-item-info">
                    <b>{{ @$top_general[2]->username }}</b>
                    <strong>{{ number_format(@$top_general[2]->profit, 2) }}</strong> SCOR profit
                    <span><strong>{{ @$top_general[2]->count_drops }}</strong> games</span>
                </div>
                <div class="top-users-page-item-placce">3</div>
                <div class="top_users_page_line"></div>
            </div>
        </div>
    </div>

    <div class="top-users-page-winners">
        <div class="top-users-page-title"><h3>Last day top wins</h3></div>
        @foreach(@$top_day as $game)
            @if(!empty($game->roulette_id))
            <a href="/profile/{{ $game->roulette->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item">
                        <img src="{{ Helper::caseImageUrl($game->roulette->product) }}" alt="">
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-case"><img src="{{ asset('images/cases/thumbnails').'/'.$game->roulette->steamCase->image }}" alt=""></div>
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->roulette->user->avatar }}" alt=""></div>
                            <span><b>{{ $game->roulette->user->username }}</b>
                            {{ $game->roulette->steamCase->priceDefault->price }} credits case</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->upgrade_id))
            <a href="/profile/{{ $game->upgrade->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item upgrade-chips-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ $game->upgrade->profit }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->upgrade->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->upgrade->bet }} <em>SCOR</em> <b>{{ $game->upgrade->profit }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->upgrade->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->tower_id))
            <a href="/profile/{{ $game->tower->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item ladder-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ ($game->tower->bet + $game->tower->profit) }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->tower->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->tower->bet }} <em>SCOR</em> <b>{{ ($game->tower->bet + $game->tower->profit) }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->tower->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif
        @endforeach
    </div>

    <div class="top-users-page-winners">
        <div class="top-users-page-title"><h3>Last week top wins</h3></div>
        @foreach(@$top_week as $game)
            @if(!empty($game->roulette_id))
            <a href="/profile/{{ $game->roulette->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item">
                        <img src="{{ Helper::caseImageUrl($game->roulette->product) }}" alt="">
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-case"><img src="{{ asset('images/cases/thumbnails').'/'.$game->roulette->steamCase->image }}" alt=""></div>
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->roulette->user->avatar }}" alt=""></div>
                            <span><b>{{ $game->roulette->user->username }}</b>
                            {{ $game->roulette->steamCase->priceDefault->price }} credits case</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->upgrade_id))
            <a href="/profile/{{ $game->upgrade->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item upgrade-chips-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ $game->upgrade->profit }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->upgrade->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->upgrade->bet }} <em>SCOR</em> <b>{{ $game->upgrade->profit }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->upgrade->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->tower_id))
            <a href="/profile/{{ $game->tower->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item ladder-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ ($game->tower->bet + $game->tower->profit) }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->tower->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->tower->bet }} <em>SCOR</em> <b>{{ ($game->tower->bet + $game->tower->profit) }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->tower->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif
        @endforeach
    </div>

    <div class="top-users-page-winners">
        <div class="top-users-page-title"><h3>Last month top wins</h3></div>
        @foreach(@$top_month as $game)
            @if(!empty($game->roulette_id))
            <a href="/profile/{{ $game->roulette->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item">
                        <img src="{{ Helper::caseImageUrl($game->roulette->product) }}" alt="">
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-case"><img src="{{ asset('images/cases/thumbnails').'/'.$game->roulette->steamCase->image }}" alt=""></div>
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->roulette->user->avatar }}" alt=""></div>
                            <span><b>{{ $game->roulette->user->username }}</b>
                            {{ $game->roulette->steamCase->priceDefault->price }} credits case</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->upgrade_id))
            <a href="/profile/{{ $game->upgrade->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item upgrade-chips-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ $game->upgrade->profit }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->upgrade->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->upgrade->bet }} <em>SCOR</em> <b>{{ $game->upgrade->profit }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->upgrade->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif

            @if(!empty($game->tower_id))
            <a href="/profile/{{ $game->tower->user_id }}" class="link_profile">
                <div class="top-chip">
                    <div class="left-bar-chips-item ladder-item">
                        <img src="/images/csgotower/upgrade-1.png" alt="">
                        <div class="upgrade-chips-item-price">{{ ($game->tower->bet + $game->tower->profit) }}</div>
                        <div class="left-bar-chips-item-info">
                            <div class="left-bar-chips-item-info-ava"><img src="{{ $game->tower->user->avatar }}" alt=""></div>
                            <span class="upgrade-chips-item-cash">{{ $game->tower->bet }} <em>SCOR</em> <b>{{ ($game->tower->bet + $game->tower->profit) }} <em>SCOR</em></b></span>
                            <span class="upgrade-chips-item-name">{{ $game->tower->user->username }}</span>
                        </div>
                    </div>
                </div>
            </a>
            @endif
        @endforeach
    </div>
</div>


