<div id="content">
    <div class="must-logged-page">
        <div class="must-logged-title"><h3>You must be logged in</h3></div><br>
        <a href="javascript:void(0)" class="must-logged-login-btn btn-login"><span>Login</span></a>
    </div>
</div>