<div id="content">
    <!-- Win -->
    <div class="case-win-page" style="display: none">
        <div class="case-win-page-title">
            <h2>Congratulations! <b>Your win:</b></h2>
        </div>
        <div class="win-items-wrapper">
            <!-- ITEMS -->
        </div>
        <div class="case-win-page-try"><a href="" class="try-again">Try again</a></div>
        <a href="/" class="case-win-home-page link_index">Home page</a>
    </div>

    <!-- Roulette -->
    <div class="case-page">
        <div class="case-page-credits">
            <h1>{{ $case->priceObj->price }} {{trans('messages.credits_case')}}</h1>
            <a href="/" class="case-page-credits1 link_index"><b>{{trans('messages.try_other_cases')}}</b><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20px" height="22px" viewBox="1417.66 1516.144 854.03 938.878"	 enable-background="new 1417.66 1516.144 854.03 938.878" xml:space="preserve"><path d="M1609.072,1843.412c39.393-90.908,130.301-155.049,235.351-155.049c141.412,0,256.057,114.645,256.057,256.058	s-114.645,256.058-256.057,256.058h-131.817v-85.858l-294.946,170.2l294.946,170.2v-84.343h131.817	c235.351,0,427.268-191.917,427.268-427.268s-191.412-427.268-427.268-427.268c-168.18,0-312.623,98.989-382.319,240.906L1609.072,1843.412z"/></svg></a>
            <div class="case-page-credits-win-op">
                {{trans('messages.win_up_to')}} <span>{{ $highest_prize }} SCOR</span>
            </div>
            <div class="spins-wrapper">
                <div class="case-page-credits-rulette">
                    <span class="blip"></span>
                    <div class="win-line">
                        <span class="movable_block">
                            <!-- ITEMS -->
                        </span>
                    </div>
                </div>
            </div>
            <div class="credits-rulette-open-btn">
                @role('user')
                    @if(Helper::get('balance')->summ >= $case->priceObj->price)
                        <div class="rulette-open-btn "><a href="" class="btn-open-case" data-case_id="{{ $case->id }}">Open for {{ $case->priceObj->price }} SCOR</a></div>
                    @else
                        <div class="rulette-open-btn-die"><span>not enough SCOR</span><br><button href="#?w=430" data-rel="popup-balance" class="add-balance balance_case">{{trans('messages.add_balance')}}</button></div>
                    @endif
                    <div class="credits-cases-to-open">
                        {{trans('messages.cases_to_open')}}
                        <select class="spins_count">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <a href="#?w=600" class="credits-cases-check-numbers modal-win-number" data-rel="popup-win-number"  data-case_id="{{ @$case->id }}">{{trans('messages.check_winning_number')}}</a>
                @else
                    <div class="rulette-open-btn-login">
                        <a class="rulette-login-btn btn-login" href="#?w=296" data-rel="popup-login">Login</a><br>
                    </div>
                @endrole
            </div>
        </div>
    </div>

    <!-- Case contains -->
    <div class="case-page-contains">
        <div class="case-page-contains-title">
            <h4>Case contains</h4>
        </div>
        <div class="case-page-contains-items">
            @foreach($case->products()->orderBy('sort')->get() as $item)
                <a href="" class="case-page-contains-item">
                    <img src="{{ Helper::caseImageUrl($item) }}" alt="">
                </a>
            @endforeach
        </div>
    </div>
</div>