@extends('layouts.layout')

@section('title')
    <title>Csgotower - Payment success</title>
@endsection

@section('drops')
    @include('site.partials.drops')
@endsection

@section('content')
    <div class="content">
        <p class="text-center payment-status payment-success">{{ trans('messages.payment_success') }}</p>
    </div>
@endsection