<div class="top-winners">
	<div class="cases-title"><i class="top-winners-title-ico"></i><h3>{{trans('messages.top_winners')}}</h3><a href="{{ url('/top') }}" class="top-winners-show-all">{{trans('messages.show_all')}}<svg width="14px" height="16px" viewBox="1644.926 1275.669 962.976 1099.245" enable-background="new 1644.926 1275.669 962.976 1099.245" xml:space="preserve"><path d="M1741.394,1275.933c-54.545,3.535-96.969,48.484-96.464,103.029v893.424c0.505,57.07,46.969,103.029,103.534,102.524c18.182,0,36.363-5.051,52.02-14.646l756.052-445.449c48.99-28.787,66.161-91.918,37.374-140.907	c-9.091-15.656-21.717-28.282-37.373-37.373l-756.052-446.46C1782.302,1279.469,1761.595,1274.418,1741.394,1275.933L1741.394,1275.933z M1850.483,1559.264l449.995,266.158l-449.995,265.148V1559.264L1850.483,1559.264z"/></svg></a></div>
	<div class="top-winners-items">
	    @foreach(Helper::getTopWinners() as $top_user)
		<a href="/profile/{{ $top_user->id }}" class="top-winners-item link_profile">
			<div class="top-winners-item-ava"><span><img src="{{ $top_user->avatar }}" alt=""></span></div>
			<div class="top-winners-item-info">
				<span>{{ $top_user->username }}</span>
				<b>{{ $top_user->profit }}</b> SCOR
			</div>
		</a>
		@endforeach
	</div>
</div>