@if(session()->has('error'))
<script>
    $(function(){
        notifyError('{{ session('error') }}', true);
    })
</script>
@endif
@if(session()->has('success'))
<script>
    $(function(){
        notifySuccess('{{ session('success') }}', true);
    })
</script>
@endif