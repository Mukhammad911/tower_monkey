<div id="popup-login" class="popup">
    <div class="popup-login-logo"></div>
    <div class="popup-content">
        <div>
            <ul class="reset popup-login-soc">
                <li class="fb" data-uloginbutton="facebook"><a href="javascript:void(0)"></a></li>
                <li class="tw" data-uloginbutton="twitter"><a href="javascript:void(0)"></a></li>
                <li class="st" data-uloginbutton="steam"><a href="javascript:void(0)"></a></li>
                <li class="gl" data-uloginbutton="google"><a href="javascript:void(0)"></a></li>
                <li class="more-icons" data-uloginbutton="window"><a href=""><i></i></a></li>
            </ul>
        </div>

        <form id="form_login">
            <input type="email" name="email" placeholder="e-mail...">
            <input type="password" name="password" placeholder="password...">
            <button>Login</button>
        </form>

        <div class="popup-login-links">
            <a href="#?w=296" class="btn-remind-password" data-rel="popup-remind-password">Forgot password?</a><br>
            <a href="#?w=296" class="btn-sign-in" data-rel="popup-sign-in">Create account</a>
        </div>
    </div>
</div>