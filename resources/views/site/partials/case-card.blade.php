<div class="card_holder">
<!-- УБРАТЬ КЛАСС tutor В ОБЫЧНЫХ БИЛЕТАХ -->
    <?php
    $card_pic = '';
    switch($case->id) {
        case 2: $card_pic = 'playcard-milspec';
            break;
        case 3: $card_pic = 'playcard-restricted';
            break;
        case 4: $card_pic = 'playcard-classified';
            break;
        case 5: $card_pic = 'playcard-covert';
            break;
        case 6: $card_pic = 'playcard-rare';
            break;
    }
    ?>
<div class="playcard playcard-4 {{ $card_pic }} nomoney">
<!-- БЛОК ПОКУПКИ БЫЛЕТА -->
<div class="shutupandtakemymoney">
    <img class="shut-light" src="/images/version2/buycard_light.png">
    <button class="gold" id="gogame" data-case_id="{{ $case->id }}"
    @if ( ! $case->isAvailable()) disabled="disabled" @endif>
    {{ trans('messages.open_ticket') }}
    <em class="price">{{ $case->priceObj->symbol_left }} {{ $case->priceObj->price }}</em>
    </button>
    <!-- Если недостаточно средств или нужно авторизоваться -->
    @if ( ! Auth::check())
    <div class="putmoney-block">
        <div class="putmoney-inner mini">
            <p style="padding: 0; margin: 0;">{{ trans('messages.card.auth.1') }}, <a href="javascript:void(0)" class="link login btn-login">{{ trans('messages.card.auth.2') }}!</a>
            </p>
        </div>
    </div>
    @elseif ( ! $case->isAvailable())
    <div class="putmoney-block">
        <div class="putmoney-inner mini">
            <p style="padding: 0; margin: 0;"> {{ trans('messages.ticket_unavailable') }}</p>
        </div>
    </div>
    @endif
</div>
<!-- БЛОК С ПОЗДРАВЛЕНИЕМ И ПРЕДМЕТОМ -->
<div class="winner-popup" style="display: none;">
    <div class="wp-head1">
        {{ trans('messages.card.congratulation') }}
    </div>
    <div class="wp-head2">
        {{ trans('messages.card.thisisyour') }}
        <span></span>
    </div>
    <div class="wp-item">
    </div>
    <div class="wp-buttons">
        <div>
            <button class="gold go-next">
                {{ trans('messages.card.tryanother') }}
            </button>
        </div>
        <div class="wp-buttons-bottom" style="padding: 15px 0 0;">
            <button class="green mini btn-drop-sell">
                {{ trans('messages.card.sellfor') }} <span class="price"></span>
            </button>
            <a href="{{ Auth::check() ? '/profile/' . Auth::user()->steamid : 'javascript:;' }}"
               class="buttonlink green mini btn-takeit link_profile">
                {{ trans('messages.card.take') }}
            </a>
            <a href="/" class="buttonlink green mini btn-tohome link_index">
                {{ trans('messages.card.tohome') }}
            </a>
        </div>
    </div>
    <div class="wp-left dno">
        <p>
            {{ trans('messages.card.share.1') }}
            <nobr>{{ trans('messages.card.share.2') }} <b class="price">50</b> {{ trans('messages.card.share.3') }}!</nobr>
        </p>
        <p style="padding-top: 10px;"><a href="#" class="bold">{{ trans('messages.card.share.4') }}</a></p>
    </div>
    <div class="wp-right">
        <p>
            {{ trans('messages.card.take.1') }} <a href="{{ Auth::check() ? '/profile/' . Auth::user()->steamid : 'javascript:;' }}" class="link_profile">{{ trans('messages.card.take.2') }}</a>
            <nobr>{{ trans('messages.card.take.3') }}!</nobr>
        </p>
    </div>
    <img class="wp-light" src="/images/version2/winner_light.png">

    <div class="wp-light-colored"></div>
    <img class="wp-light-anim" src="/images/version2/winner_light_anim.png">
</div>
<!-- БЛОК СОДЕРЖИМОГО БИЛЕТА -->
<div class="playcard-inner">
    <div class="scratchcases">
        <div class="scratchcase" id="case-0" data-id="0"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-1" data-id="1"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-2" data-id="2"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-3" data-id="3"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-4" data-id="4"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-5" data-id="5"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-6" data-id="6"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-7" data-id="7"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
        <div class="scratchcase" id="case-8" data-id="8"
             style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
            <div class="item-scratch">
                <div class="picture"></div>
                <div class="descr"></div>
            </div>
            <img src="/images/version2/empty.png" crossorigin=""
                 style="position: absolute; width: 100%; height: 100%; display: inline;">
            <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120">
        </div>
    </div>
    <!-- scratchcases -->
    <div class="card-right">
        <img src="/images/version2/arrow_extra_right.png" class="card-extraarrow-right">

        <div class="card-icon"><img src="/images/cases/thumbnails/{{ $case->image }}"></div>
        <div class="card-name">{{ $case->name }}</div>
        <div class="card-extra">
            <div class="card-extracase">
                <div class="scratchcase off" id="case-10" data-id="10"
                     style="cursor: url(&quot;//images/version2/coin.png&quot;) 5 5, default;">
                    <div class="item-scratch">
                        <div class="picture">
                        </div>
                        <div class="descr">
                        </div>
                    </div>
                    <img src="/images/version2/empty.png" crossorigin=""
                         style="position: absolute; width: 100%; height: 100%; display: inline;">
                    @if ($custom_lang=='ru')
                        <img src="/images/version2/extra_case_ru.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='en')
                        <img src="/images/version2/extra_case_en.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='fr')
                        <img src="/images/version2/extra_case_fr.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='es')
                        <img src="/images/version2/extra_case_es.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='tr')
                        <img src="/images/version2/extra_case_tr.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='de')
                        <img src="/images/version2/extra_case_de.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='cs')
                        <img src="/images/version2/extra_case_cs.png" class="free-card-canvas" width="160" height="120" />
                    @elseif ($custom_lang=='sv')
                        <img src="/images/version2/extra_case_sv.png" class="free-card-canvas" width="160" height="120" />
                    @else
                        <img src="/images/version2/extra_case_en.png" class="free-card-canvas" width="160" height="120" />
                    @endif
                </div>
                <!-- scratchcase -->
            </div>
        </div>
        <div class="card-rules">
            <b>{{ trans('messages.rules_first') }}</b>
            <ul>
                <li>{{ trans('messages.rules_second') }}</li>
                <li>{{ trans('messages.rules_third') }}</li>
                <li>{{ trans('messages.rules_fourth') }}</li>
            </ul>
        </div>
    </div>
    <div class="card-extraerase">
        <img src="/images/version2/arrow_extra_left.png" class="card-extraarrow-left">
        <button class="button pic large" id="extraerase">
            {{ trans('messages.choose_try') }}
            <span class="extraPrice">{{ trans('messages.choose_free') }}</span>
        </button>
        <button class="button-line refill" style="display: none">
            {{ trans('messages.choose_notenough') }}
        </button>
        <div class="ce-text">
            <div class="ce-item">{{ trans('messages.choose_get') }} <span id="possible_item"></span></div>
            <div>{{ trans('messages.choose_info') }}</div>
        </div>
    </div>
    <div class="card-id">XXXXXXXX</div>
    <div class="card-game csgo"></div>
</div>
<!-- playcard-inner -->
<!-- Мелкие фаны -->
<div class="card-funny card-funny-2">
    <img src="/images/version2/funny_tear.png">
</div>
<div class="card-funny card-funny-3">
    <img src="/images/version2/funny_nichosi.png">
</div>
<div class="card-funny card-funny-4">
    <img src="/images/version2/funny_satana.png">
</div>
<div class="card-funny card-funny-5">
    <img src="/images/version2/funny_wish.png">
</div>
<div class="card-funny card-funny-6">
    <img src="/images/version2/funny_cat.png">
</div>
<!-- Большие фаны на 4 слот -->
<div class="card-funny card-funny-1">
    <img src="/images/version2/funny_gabe.png">
</div>
<div class="card-funny card-funny-7">
    <img src="/images/version2/funny_sahar.png">
</div>
<div class="card-funny card-funny-8">
    <img src="/images/version2/funny_santa.png">
</div>
</div>
<!-- playcard -->
</div>