<header class="new-header">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                @include('site.partials.header.nav_inc')


                @if(Auth::check())

                    @include('site.partials.header.header-balance', [
                        'class' => 'desktop'
                    ])

                    <div class="header-1" style="">
                        <a href="{{ url('/profile/'.Auth::user()->id) }}" class="user-profile-setting">
                            <i class="setting_icon">
                                Welcome, <span class="username">{{ Auth::user()->username }}</span>
                                <svg aria-hidden="true" focusable="false"
                                     data-prefix="fas" data-icon="cog"
                                     class="svg-inline--fa fa-cog fa-w-16"
                                     role="img" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 512 512">
                                    <path fill="currentColor"
                                          d="M487.4 315.7l-42.6-24.6c4.3-23.2 4.3-47 0-70.2l42.6-24.6c4.9-2.8 7.1-8.6 5.5-14-11.1-35.6-30-67.8-54.7-94.6-3.8-4.1-10-5.1-14.8-2.3L380.8 110c-17.9-15.4-38.5-27.3-60.8-35.1V25.8c0-5.6-3.9-10.5-9.4-11.7-36.7-8.2-74.3-7.8-109.2 0-5.5 1.2-9.4 6.1-9.4 11.7V75c-22.2 7.9-42.8 19.8-60.8 35.1L88.7 85.5c-4.9-2.8-11-1.9-14.8 2.3-24.7 26.7-43.6 58.9-54.7 94.6-1.7 5.4.6 11.2 5.5 14L67.3 221c-4.3 23.2-4.3 47 0 70.2l-42.6 24.6c-4.9 2.8-7.1 8.6-5.5 14 11.1 35.6 30 67.8 54.7 94.6 3.8 4.1 10 5.1 14.8 2.3l42.6-24.6c17.9 15.4 38.5 27.3 60.8 35.1v49.2c0 5.6 3.9 10.5 9.4 11.7 36.7 8.2 74.3 7.8 109.2 0 5.5-1.2 9.4-6.1 9.4-11.7v-49.2c22.2-7.9 42.8-19.8 60.8-35.1l42.6 24.6c4.9 2.8 11 1.9 14.8-2.3 24.7-26.7 43.6-58.9 54.7-94.6 1.5-5.5-.7-11.3-5.6-14.1zM256 336c-44.1 0-80-35.9-80-80s35.9-80 80-80 80 35.9 80 80-35.9 80-80 80z"></path>
                                </svg>
                            </i>
                        </a>
                    </div>

                @endif

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>

<nav id="header" class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

            @if(Auth::check())
                @include('site.partials.header.header-balance', [
                    'class' => 'mobile'
                ])
            @else
                @include('site.partials.header.steam-login-btn', [
                    'class' => 'mobile'
                ])
            @endif

            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-2"
                    aria-expanded="false">{{ asset('') }}
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}" style="padding: 10px 15px 10px 15px;">
                {{--<img src="/images/csgotower/logo-full.png" alt="CS:GO Tower" style="max-width: 135px;">--}}
                <img src="/images/csgotower/logo2-full.png" alt="CS:GO Tower" style="max-width: 150px;">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

            <ul class="nav {{--navbar-nav--}} header-nav">
                <li class="@if (isset($active_menu) && ($active_menu == 'tower')) active active-menu-crate @endif ">
                    <a href="{{ url('/tower') }}">
                        {{ trans('messages.header_tower') }}
                    </a>
                </li>
                <li class="@if (isset($active_menu) && ($active_menu == '/')) active active-menu-crate @endif ">
                    <a href="{{ url('/') }}">
                        ROULETTE
                    </a>
                </li>
            </ul>


            @include('site.partials.header.nav_inc')


            @if(Auth::check())

                <ul class="nav header-nav navbar-right">
                    <li class="other-li withdraw-only">
                        <a class="get-modal add-balance" data-rel="popup-balance" href="{{ url('/#?w=430') }}">
                            DEPOSIT
                        </a>
                    </li>
                    <li class="other-li">
                        <a href="{{ url('/csgo') }}">
                            WITHDRAW
                        </a>
                    </li>
                </ul>

            @else

                <ul class="nav navbar-right login-btn">
                    <li>
                        @include('site.partials.header.steam-login-btn', [
                            'class' => 'desktop'
                        ])
                    </li>
                </ul>

            @endif

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>