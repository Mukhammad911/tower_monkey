<div id="popup-win-number" class="popup popup1">

	<div class="popup-login-title" style="width: 537px">Provably fair</div>
	<div class="popup-content"  style="overflow-y: auto; max-height: 600px; color: #555; width: 537px;">
	    <div style="color: #fff; padding: 6px;">
	        <h4 style="text-transform: uppercase;">Next game</h4>

	        <p><button  class="roulette_refresh_seeds" data-case_id="{{ @$case->id }}"><span>new seeds</span></button></p>

            <div class="roulette-next-game">

            <div class="ladder-page-verify-slide-next">
                   <input type="text" name="" placeholder="Roulette 1">
                    <input type="text" name="" placeholder="{{ @$server_seed }}">
                    <input type="text" name="" placeholder="{{ @$user_seed }}">
            </div>
                </div>

	        <br/>
            <h4 style="text-transform: uppercase;">Previous game</h4>
            <div class="roulette-prev-game" style="    position: relative;    top: 51px;">
	        @if(!empty($previous_game))

                <div class="ladder-page-verify-slide-previous">

                        <label>Server seed</label><br/>
                        <input type="text" name="server_seed" placeholder="{{ $previous_game->server_seed }}"><br/>
                        <label>User seed</label><br/>
                        <input type="text" name="user_seed" placeholder="{{ $previous_game->user_seed }}"><br/>
                        <label>Win number</label><br/>
                        <input type="text" name="win_number" placeholder="{{ $previous_game->win_number }}"><br/>
                        <label>Hash</label><br/>
                        <input type="text" name="hash" placeholder="{{ $previous_game->hash }}">
                </div>
	        @endif
            </div>
	    </div>

        <hr>
        {{--<p>Items in the game are distributed in portions, each item has winning numbers range. If you roll on that item’s range - you get the item.</p>--}}

        <table class="table" style="width: 100%; text-align: center; padding-left: 13px;">
            <thead>
                <tr>
                    <th style=" color: #3eca58; height: 40px">Item</th>
                    <th style=" color: #3eca58;height: 40px">Name</th>
                    <th style="color: #3eca58;height: 40px">Winning number range</th><br/><br/>
                </tr>
            </thead>

            <tbody class="win_ranges">
                @if(!empty($win_numbers))
                    @foreach($win_numbers as $win_number)
                    <tr style="color: #fff;font-size: 12px;">
                        <td><img style=" height: 130px;
    background: url(/images/csgotower/case_contains_item_bg.png);" src="@if($win_number->image_source != 'url'){{ url('/images/products') }}/@endif{{ $win_number->image }}"></td>
                        <td><strong>{{ $win_number->win_range_min }} - {{ $win_number->win_range_max }}</strong></td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
	</div>
</div>