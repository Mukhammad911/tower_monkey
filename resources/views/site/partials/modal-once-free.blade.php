<!-- Modal -->
<div class="modal fade modal-once-free" id="once-free" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-once-free" role="document">
        <div class="modal-content modal-content-once-free">
            <div class="modal-header modal-header-once-free">
                <button type="button" class="close close-once-free" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title modal-title-once-free" id="myModalLabel">REFERRAL</h4>
            </div>
            <div class="modal-body modal-body-once-free">
                <p class="modal-once-free-p">Please enter a referral code to gain your <span class="modal-once-free-coin-item"></span> 0.50 coins.</p>
                <input  type="text" class="once-free-value modal-input-once-free">
            </div>
            <div class="modal-footer modal-footer-once-free">
                <button type="button" class="btn btn-default modal-once-free-btn-default" data-dismiss="modal">CANCEL</button>
                <button type="button" class="btn btn-primary once-free-confirm modal-once-free-btn-primary">CONFIRM</button>
            </div>
        </div>
    </div>
</div>