@if(!empty($item->id))
    @if($item->profile_drop_type == 'csgo')
        <div class="case-winned-item csgo-rarity-{{$item->class}} {{($item->stattrak) ? "stattrak" : ""}}" data-id="{{$item->id}}" data-drop-type="csgo">
            @if ($own)
                @if ($item->status == 'pending')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                        <div class="send-item">
                            <a target="_blank" data-id="{{$item->id}}" data-drop-type="csgo" rel="noreferrer noopener" href="javascript:;">Send</a>
                        </div>
                        <div class="sold-item">
                            <a target="_blank" data-id="{{$item->id}}" data-drop-type="csgo" rel="noreferrer noopener" href="javascript:;">Sell for <span class="case-win-item-price-coins"></span> {{Helper::currentPrice($item->price)}}</a>
                        </div>
                    </div>
                @elseif ($item->status == 'requested')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-clock-o"></i></span>
                                                Requested
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @elseif ($item->status == 'offered')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="fa fa-paper-plane-o"></i></span>
                                                Offered
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @else
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @endif
            @else
                @if ($item->status == 'pending' || $item->status == 'requested' || $item->status == 'offered')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-arrow-down"></i></span>
                                                Waiting
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @else
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @endif
            @endif

            @if ($own)
                <div class="get" style="display: {{($item->status == 'offered' ? "block" : "none")}};">
                    <a target="_blank" rel="noreferrer noopener" href="{{($item->offer_id) ? "https://steamcommunity.com/tradeoffer/".$item->offer_id : ""}}">Get</a>
                </div>
            @endif

            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image" style="width: 120px;">
            <img src="/{{ $item->case_image }}" class="top1-image" alt="case">
            <p class="case-winned-item-name" style="text-align: left;">{{$item->nameParsed}}<br> {{$item->shortDescriptionParsed}}</p>
        </div>
    @elseif($item->profile_drop_type == 'pubg')
        <div class="case-winned-item pubg-rarity-{{$item->product->class}}" data-id="{{$item->id}}" data-drop-type="pubg">
            @if ($own)
                @if ($item->status == 'pending')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                        <div class="send-item">
                            <a target="_blank" data-id="{{$item->id}}" data-drop-type="pubg" rel="noreferrer noopener" href="javascript:;">Send</a>
                        </div>
                        <div class="sold-item">
                            <a target="_blank" data-id="{{$item->id}}" data-drop-type="pubg" rel="noreferrer noopener" href="javascript:;">Sell for <span class="case-win-item-price-coins"></span> {{Helper::currentPrice($item->price)}}</a>
                        </div>
                    </div>
                @elseif ($item->status == 'requested')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-clock-o"></i></span>
                                                Requested
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @elseif ($item->status == 'offered')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="fa fa-paper-plane-o"></i></span>
                                                Offered
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @else
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @endif
            @else
                @if ($item->status == 'pending' || $item->status == 'requested' || $item->status == 'offered')
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-yellow"><i class="fa fa-arrow-down"></i></span>
                                                Waiting
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @else
                    <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                            <span class="status">
                                                <span class="ico-green"><i class="{{($item->status == 'sold') ? "fa fa-usd" : (($item->status == 'accepted') ? "fa fa-check" : "")}}"></i></span>
                                                {{($item->status == 'sold') ? "Sold" : (($item->status == 'accepted') ? "Accepted" : "")}}
                                            </span>
                        <span class="price">
                                                <span class="case-win-item-price-coins"></span>
                            {{Helper::currentPrice($item->price)}}
                                            </span>
                    </div>
                @endif
            @endif

            @if ($own)
                <div class="get" style="display: {{($item->status == 'offered' ? "block" : "none")}};">
                    <a target="_blank" rel="noreferrer noopener" href="{{($item->offer_id) ? "https://steamcommunity.com/tradeoffer/".$item->offer_id : ""}}">Get</a>
                </div>
            @endif

            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image">
            <img src="/{{ $item->case_image }}" class="top1-image" alt="case">
            <p class="case-winned-item-name">{{$item->market_name}}</p>
        </div>
    @endif
@endif