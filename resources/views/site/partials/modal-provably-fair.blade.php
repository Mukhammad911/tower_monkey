<div id="popup-provably-fair" class="popup popup1">
    <div class="popup-login-title">PROVABLY FAIR</div>
    <div class="popup-content" style="color: #ffffff; padding-left: 25px;">
        <br>
        <h3 style="color: #D82956;">NEXT GAME</h3>

        <p>
            <button style="width: 90%;" class="upgrade_refresh_hash">new seeds</button>
        </p>

        <p>
            <label>user seed: <br>
                <input type="text" id="upgrade_next_user_seed" value="{{ @$next_game['user_seed'] }}" style="width: 90%;" disabled>
            </label>
        </p>
        <p>
            <label>server seed: <br>
                <input type="text" id="upgrade_next_server_seed" value="{{ @$next_game['server_seed'] }}" style="width: 90%;" disabled>
            </label>
        </p>
        <br>

        <h3 style="color: #D82956;">PREVIOUS GAME</h3>
        <p>
            <label>user seed: <br>
                <input autocomplete="off" type="text" id="upgrade_prev_user_seed" value="{{ @$prev_game->user_seed }}" style="width: 90%;" disabled>
            </label>
        </p>
        <p>
            <label>server seed: <br>
                <input autocomplete="off" type="text" id="upgrade_prev_server_seed" value="{{ @$prev_game->server_seed }}" style="width: 90%;" disabled>
            </label>
        </p>
        <p>
            <label>win number: <br>
                <input autocomplete="off" type="text" id="upgrade_win_number" value="{{ @$prev_game->win_number }}" style="width: 90%;" disabled>
            </label>
        </p>
        <p>
            <label>hash: <br><input autocomplete="off" type="text" id="upgrade_hash" value="{{ @$prev_game->hash }}" style="width: 90%;" disabled>
            </label>
        </p>
    </div>
</div>