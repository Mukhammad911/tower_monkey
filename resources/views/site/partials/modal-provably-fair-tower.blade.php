<div id="popup-provably-fair-tower" class="provable-tower popup1">
        <div class="ladder-page-verify-slide" style="display: block;">
            <div class="ladder-page-verify-slide-title close_provably_fair">Verification<span class="close_provably_fair"></span></div>
            <div class="ladder-page-verify-slide-previous">
                <h6>Previous ticket</h6>
                <form class="form-previous-ticket">
                    <label>Server seed</label><br/>
                    <input type="text" name="server_seed" placeholder="Server seed"><br/>
                    <label>User seed</label><br/>
                    <input type="text" name="user_seed" placeholder="User seed"><br/>
                    <label>Win number</label><br/>
                    <input type="text" name="win_number" placeholder="Win number"><br/>
                    <label>Hash</label><br/>
                    <input type="text" name="hash" placeholder="Hash">
                </form>
            </div>
            <div class="ladder-page-verify-slide-next">
                <h6>Next ticket
                    <button style="" class="tower_refresh_seeds"><span>new seeds</span></button></h6>
                <form class="form-next-ticket">
                    <input type="text" name="server_seed">
                    <input type="text" name="user_seed">
                </form>
            </div>
        </div>

</div>