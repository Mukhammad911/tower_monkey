<div class="spiner-pregame open pregame">
    <div class="spin-back">
        <div class="spin-block spin-block-pregame">
            <div class="current-case">
                <img alt="" src="/images/version2/case/icons/{{ $case->class }}.png">
            </div>
            <div class="loading text-center hidden">
                <div style="display: table-cell;vertical-align: middle">{{trans('messages.loading')}}...</div>
            </div>
        </div>
    </div>

    <div class="spin-buttons">
        @if($case->active && $case->isAvailable())
            @role('user')
                <h1>{{ $case->name }}</h1>
                <button class="gold login disable btn-open-case">
                     <em class="price">@if($case->priceObj)
                          <sup><i class="fa {{ $case->priceObj->class }}"></i></sup>
                          <span>{{ $case->priceObj->price }}</span>
                      @endif</em>
                </button>
            @else
                <div class="putmoney-block">
                    <div class="putmoney-inner mini">
                        <p style="padding: 0; margin: 0;">Пожалуйста, <a href="javascript:void(0)" class="btn-login">авторизуйтесь через Steam</a></p>
                    </div>
                </div>
            @endrole

        @else
            <h1 class="title-grey"><span>{{ trans('messages.unavailable') }}</span></h1>
            <br><br><br>
        @endif

    </div>
</div>