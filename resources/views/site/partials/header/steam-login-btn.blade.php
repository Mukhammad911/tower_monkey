<a href="javascript:void(0)" class="header-login-btn btn-login {{ $class }}">
    <span> {{ trans('messages.login') }}</span>
    <span class="mobile"> {{ trans('messages.login_mobile') }}</span>
    <img src="/images/csgotower/steam_ico.png">
</a>