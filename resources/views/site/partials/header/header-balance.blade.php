<div class="header-balance {{ $class }}">
    <span class="coins_header"></span>
    <span>
        <bal class="user-balance">
            {{ number_format(Helper::get('balance')->summ, 2, '.', '') }}
        </bal>
    </span>
    <a href="/logout" class="get-modal header-add-balance-btn-exit add-balance logout_header">
        <i class="icon-logout icon-logout-my icons"></i>
    </a>
</div>