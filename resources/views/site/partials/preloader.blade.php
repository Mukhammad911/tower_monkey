@extends('layouts.layout')

@section('content')
    <div class="landing-logo">
        <img src="/images/version2/">
    </div>
    <div class="pre-loader">
        <div class="image"></div>
        <p class="landing-h2">Loading...</p>
    </div>
@endsection