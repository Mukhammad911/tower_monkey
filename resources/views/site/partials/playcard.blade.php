<!-- УБРАТЬ КЛАСС tutor В ОБЫЧНЫХ БИЛЕТАХ -->
<div class="playcard playcard-4 playcard-tutorial nomoney">
    <!-- БЛОК ПОКУПКИ БЫЛЕТА -->
    <div class="shutupandtakemymoney">
        <img class="shut-light" src="/images/version2/buycard_light.png">
        @if ( ! Auth::check())
        <button class="gold tutor_auth" onclick="ifNotAuth()">
            {{ trans('messages.tutor_auth') }}
            <span>{{ trans('messages.tutor_auth_2') }}</span>
        </button>
        @else
        <button class="gold tutor_auth" id="gogame">
            {{ trans('messages.tutor_auth') }}
            <span>{{ trans('messages.tutor_auth_2') }}</span>
        </button>
        @endif
        @if ( ! Auth::check())
        <div class="putmoney-block">
            <div class="putmoney-inner mini">
                <p style="padding: 0; margin: 0;">{{ trans('messages.notice_first') }}<br>{{ trans('messages.notice_second') }} <a href="javascript:;" class="link login btn-login">{{ trans('messages.notice_third') }}</a></p>
            </div>
        </div>
        @endif
    </div>
    <div class="playcard-teacher1">
        {{ trans('messages.tutor_teacher1_info') }}
        <button id="teacher1" class="gold">{{ trans('messages.tutor_teacher1_okay') }}</button>
    </div>
    <div class="playcard-teacher2">
        {{ trans('messages.tutor_teacher2_info') }}
        <button id="teacher2" class="gold">{{ trans('messages.tutor_teacher2_okay') }}</button>
    </div>
    <div class="playcard-teacher3">
        {{ trans('messages.tutor_teacher3_info') }}
        <button id="teacher3" class="gold">{{ trans('messages.tutor_teacher3_okay') }}</button>
    </div>
    <!-- БЛОК С ПОЗДРАВЛЕНИЕМ И ПРЕДМЕТОМ -->
    <div class="winner-popup" style="display: none;">
        <img src="/images/version2/demonstration{{ (\App::getLocale() == 'ru') ? '-ru' : '' }}.png" class="tutor-demonstration">
        <div class="wp-head1">
            {{ trans('messages.tutor_over') }}
        </div>
        <div class="wp-head2">
            {{ trans('messages.tutor_example') }}
            <span></span>
        </div>
        <div class="wp-item">
        </div>
        <div class="wp-buttons">
            <div>
                <a href="#" class="buttonlink gold finish_tutor">
                    {{ trans('messages.tutor_complete') }}
                    <span>{{ trans('messages.tutor_complete_info') }}</span>
                </a>
            </div>
            <div class="wp-buttons-bottom" style="padding: 15px 0 0;">
                <div class="note-attention" style="text-align: left; width: 360px; margin: 0 auto;">
                    <div class="icon"></div>
                    <div class="descr">
                        <div class="head">
                            {{ trans('messages.tutor_demo_info1') }}
                        </div>
                        <div class="text">
                            {{ trans('messages.tutor_demo_info2') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wp-left dno">
            <p>
                Поделитесь с друзьями и
                <nobr>получите <b class="price">50</b> на аккаунт!</nobr>
            </p>
            <p style="padding-top: 10px;"><a href="#" class="bold">Рассказать друзьям</a></p>
        </div>
{{--
        <div class="wp-right">
            <p>
                Заберите предмет в <a href="/user/">профиле</a>
                <nobr>в течение часа!</nobr>
            </p>
            <p style="padding-top: 10px;"><a href="#" class="bold viewURL">Осмотреть в игре</a></p>
        </div>
--}}
        <img class="wp-light" src="/images/version2/winner_light.png">
        <div class="wp-light-colored"></div>
        <img class="wp-light-anim" src="/images/version2/winner_light_anim.png">
    </div>
    <!-- БЛОК СОДЕРЖИМОГО БИЛЕТА -->
    <div class="playcard-inner">
        <div class="scratchcases">
            <div class="scratchcase" id="case-0" data-id="0" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;" />
                <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120" />
            </div>
            <div class="scratchcase" id="case-1" data-id="1" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-2" data-id="2" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-3" data-id="3" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-4" data-id="4" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-5" data-id="5" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-6" data-id="6" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-7" data-id="7" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
            <div class="scratchcase" id="case-8" data-id="8" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                <div class="item-scratch">
                    <div class="picture"></div>
                    <div class="descr"></div>
                </div>
                <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
               <img src="/images/version2/car-item-ps.png" class="free-card-canvas" width="160" height="120"></img>
            </div>
        </div>
        <!-- scratchcases -->
        <div class="card-right">
            <img src="/images/version2/arrow_extra_right.png" class="card-extraarrow-right">
            <div class="card-icon"><img src="/images/version2/case/icons/tutorial.png"></div>
            <div class="card-name"> {{ trans('messages.card_demo') }}</div>
            <div class="card-extra">
                <div class="card-extracase">
                    <div class="scratchcase off" id="case-10" data-id="10" style="cursor: url(&quot;/images/version2/coin.png&quot;) 5 5, default;">
                        <div class="item-scratch">
                            <div class="picture">
                            </div>
                            <div class="descr">
                            </div>
                        </div>
                        <img src="/images/version2/empty.png" crossorigin="" style="position: absolute; width: 100%; height: 100%; display: inline;">
                        @if ($custom_lang=='ru')
                            <img src="/images/version2/extra_case_ru.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='en')
                            <img src="/images/version2/extra_case_en.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='fr')
                            <img src="/images/version2/extra_case_fr.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='es')
                            <img src="/images/version2/extra_case_es.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='tr')
                            <img src="/images/version2/extra_case_tr.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='de')
                            <img src="/images/version2/extra_case_de.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='cs')
                            <img src="/images/version2/extra_case_cs.png" class="free-card-canvas" width="160" height="120" />
                        @elseif ($custom_lang=='sv')
                            <img src="/images/version2/extra_case_sv.png" class="free-card-canvas" width="160" height="120" />
                        @else
                            <img src="/images/version2/extra_case_en.png" class="free-card-canvas" width="160" height="120" />
                        @endif
                    </div>
                    <!-- scratchcase -->
                </div>
            </div>
            <div class="card-rules">
                <b>{{ trans('messages.rules_first') }}</b>
                <ul>
                    <li>{{ trans('messages.rules_second') }}</li>
                    <li>{{ trans('messages.rules_third') }}</li>
                    <li>{{ trans('messages.rules_fourth') }}</li>
                </ul>
            </div>
        </div>
        <div class="card-extraerase">
            <img src="/images/version2/arrow_extra_left.png" class="card-extraarrow-left">
            <button class="button pic large" id="extraerase">
                {{ trans('messages.choose_try') }}
            <span class="extraPrice">{{ trans('messages.choose_free') }}</span>
            </button>
            <button class="button-line refill" style="display: none">
                {{ trans('messages.choose_notenough') }}
            </button>
            <div class="ce-text">
                <div class="ce-item">{{ trans('messages.choose_get') }} <span id="possible_item"></span></div>
                <div>{{ trans('messages.choose_info') }}</div>
            </div>
        </div>
        <div class="card-id">XXXXXXXX</div>
        <div class="card-game csgo"></div>
    </div>
    <!-- playcard-inner -->
    <!-- Мелкие фаны -->
    <div class="card-funny card-funny-2">
        <img src="/images/version2/funny_tear.png">
    </div>
    <div class="card-funny card-funny-3">
        <img src="/images/version2/funny_nichosi.png">
    </div>
    <div class="card-funny card-funny-4">
        <img src="/images/version2/funny_satana.png">
    </div>
    <div class="card-funny card-funny-5">
        <img src="/images/version2/funny_wish.png">
    </div>
    <div class="card-funny card-funny-6">
        <img src="/images/version2/funny_cat.png">
    </div>
    <!-- Большие фаны на 4 слот -->
    <div class="card-funny card-funny-1">
        <img src="/images/version2/funny_gabe.png">
    </div>
    <div class="card-funny card-funny-7">
        <img src="/images/version2/funny_sahar.png">
    </div>
    <div class="card-funny card-funny-8">
        <img src="/images/version2/funny_santa.png">
    </div>
</div>
<script type="text/javascript">
    function ifNotAuth(){
        $.jGrowl(Lang.get('messages.c_error'), {
            theme:'bg-danger', life:2000,position:'bottom-right'
        });
    }
</script>