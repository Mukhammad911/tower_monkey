@if(!empty($item->id))
    @if($item->profile_drop_type == 'csgo')
        <div class="market-withdrawed-item csgo-rarity-{{$item->class}} {{($item->stattrak) ? "stattrak" : ""}}" data-id="{{$item->id}}">
            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                <span class="status">
                                    <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                    {{ "Accepted" }}
                                </span>
                <span class="price">
                                    <span class="case-win-item-price-coins"></span>
                    {{Helper::currentPrice($item->price)}}
                                </span>
            </div>

            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image" style="width: 120px;">

            <p class="case-winned-item-name" style="text-align: left;">{{$item->nameParsed}}<br> {{$item->shortDescriptionParsed}}</p>
        </div>
    @elseif($item->profile_drop_type == 'dota')
        <div class="market-withdrawed-item dota-rarity-{{$item->class}}" data-id="{{$item->id}}">
            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                                <span class="status">
                                    <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                    {{ "Accepted" }}
                                </span>
                <span class="price">
                                    <span class="case-win-item-price-coins"></span>
                    {{Helper::currentPrice($item->price)}}
                                </span>
            </div>

            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360x360" alt="drop" class="drop-image" style="width: 137px; top: 31px;">

            <p class="case-winned-item-name">{{$item->market_name}}</p>
        </div>
    @elseif($item->profile_drop_type == 'pubg')
        <div class="market-withdrawed-item pubg-rarity-{{$item->product->class}}" data-id="{{$item->id}}">
            <div class="info" data-price="{{Helper::currentPrice($item->price)}}">
                            <span class="status">
                                <span class="ico-green"><i class="{{ "fa fa-check" }}"></i></span>
                                {{ "Accepted" }}
                            </span>
                <span class="price">
                                <span class="case-win-item-price-coins"></span>
                    {{Helper::currentPrice($item->price)}}
                            </span>
            </div>

            <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $item->image_hash }}/360fx360f" alt="drop" class="drop-image">

            <p class="market-withdrawed-item-name">{{$item->market_name}}</p>
        </div>
    @endif
@endif