{{-- <div id="live_drop">
    <div class="title">
        <a href="http://csgotower.com" class="logo link_index"></a>
    </div>

    <div class="switch_toggle"><i class="fa fa-angle-double-left" aria-hidden="true"></i></div>

    <div class="drop_cont">
        @foreach(Helper::liveDrops() as $last_win)
            @if($last_win->game == 'csgo-case-roulette')
                <div class="item category_csgo csgo-rarity-{{$last_win->drop->class}} {{($last_win->drop->stattrak) ? "stattrak" : ""}}" style="display: block;" data-csgo-case-drop-id="{{ $last_win->drop->id }}">
                    <a class="picture" href="/profile/{{ $last_win->drop->user_id }}">
                        <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $last_win->drop->image_hash }}/180fx180f" class="drop-image" alt="Drop Image">
                        <img src="/{{ $last_win->drop->case_image }}" class="case-image" alt="Case Image">
                        <div class="title">{{ $last_win->nameParsed }}<br> {{ $last_win->shortDescriptionParsed }}</div>
                        <div class="hover">
                            <div class="user">
                                <div class="userpic" style="background-image: url({{ $last_win->drop->avatar }});"></div>
                                <div class="text">
                                    <span>{{ $last_win->drop->username }}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($last_win->game == 'pubg-case-roulette')
                <div class="item category_pubg pubg-rarity-{{$last_win->drop->product->class}}" style="display: block;" data-pubg-case-drop-id="{{ $last_win->drop->id }}">
                    <a class="picture" href="/profile/{{ $last_win->drop->user_id }}">
                        <img src="https://steamcommunity-a.akamaihd.net/economy/image/{{ $last_win->drop->image_hash }}/180fx180f" class="drop-image" alt="Drop Image">
                        <img src="/{{ $last_win->drop->case_image }}" class="case-image" alt="Case Image">
                        <div class="title title-pubg-livedrop">{{ $last_win->drop->market_name }}</div>
                        <div class="hover">
                            <div class="user">
                                <div class="userpic" style="background-image: url({{ $last_win->drop->avatar }});"></div>
                                <div class="text">
                                    <span>{{ $last_win->drop->username }}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($last_win->game == 'coin-flip')
                <div class="item category_coin_flip coin-flip-live" style="display: block;" data-coin-flip-drop-id="{{ $last_win->drop->id }}">
                    <a class="picture" href="/profile/{{ $last_win->drop->user_id }}">
                        <img src="/images/csgotower/coin_flip_live.png" class="drop-image" alt="Drop Image">
                        <div class="title title-coin-flip-livedrop">{{ $last_win->drop->bet }}</div>
                        <div class="hover">
                            <div class="user">
                                <div class="userpic" style="background-image: url({{ $last_win->drop->avatar }});"></div>
                                <div class="text">
                                    <span>{{ $last_win->drop->username }}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @elseif($last_win->game == 'tower')
                <div class="item category_tower tower-live" style="display: block;" data-tower-drop-id="{{ $last_win->id }}">
                    <a class="picture" href="/profile/{{ $last_win->tower->user_id  }}">
                        <img src="/images/csgotower/coin_tower.png" class="drop-image" alt="Drop Image">
                        <div class="title title-tower-livedrop">{{ $last_win->tower->profit }}</div>
                        <div class="hover">
                            <div class="user">
                                <div class="userpic" style="background-image: url({{ $last_win->tower->user->avatar }});"></div>
                                <div class="text">
                                    <span>{{ $last_win->tower->user->username }}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endif
        @endforeach

        <div id="drop_cont_pre">
            <div class="item">
                <a class="picture">
                    <img src="" class="drop-image" alt="Drop Image">
                    <img src="" class="case-image" alt="Case Image">
                    <div class="title"></div>
                    <div class="hover">
                        <div class="user">
                            <div class="userpic"></div>
                            <div class="text">
                                <span></span>
                                <span class="ago"></span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>

    <div class="live_drop_online"><div><i class="icon-feed icons"></i> <span>0</span></div></div>
</div>
--}}