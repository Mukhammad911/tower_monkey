<div id="popup-sign-in" class="popup">
    <div class="popup-login-logo"></div>
    <div class="popup-content">
        <div class="uLogin" data-ulogin="display=buttons;theme=classic;fields=first_name,nickname,photo_big,network,identity;hidden=other;redirect_uri={{ urlencode(url('/login')) }};mobilebuttons=0;">
            <ul class="reset popup-login-soc">
                <li class="fb" data-uloginbutton="facebook"><a href="javascript:void(0)"></a></li>
                <li class="tw" data-uloginbutton="twitter"><a href="javascript:void(0)"></a></li>
                <li class="st" data-uloginbutton="steam"><a href="javascript:void(0)"></a></li>
                <li class="gl" data-uloginbutton="google"><a href="javascript:void(0)"></a></li>
                <li class="more-icons" data-uloginbutton="window"><a href=""><i></i></a></li>
            </ul>
        </div>

        <form id="form_sign_in">
            <input type="email" name="email" placeholder="e-mail...">
            <input type="password" name="password" placeholder="enter password...">
            <input type="password" name="password2" placeholder="repeat password...">
            <button>Sign in</button>
        </form>
    </div>
</div>