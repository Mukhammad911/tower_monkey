<div id="popup-balance" class="popup popup1">
    <div class="popup-login-title">{{ trans('messages.balance_fill') }}</div>
    <div class="popup-content">
        <form target="_blank" action="" method="post" class="payment-form" data-trade_url="@if(Auth::check()){{ Auth::user()->trade_url }} @endif">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="payment_system" value="">
            <div class="popup-add-balance">
            </div>
            <input autocomplete="off" type="number" name="amount" placeholder="{{ trans('messages.enter_amount') }}">
            {{--<div class="promo_addition" style="width: 286px; margin-top: 20px; color: #7676c1; text-align: center; display: none;" data-affiliate-percent="{{ Partnership::getBonusPercent() }}">
                Bonus: + <span class="promo_add_sum">0</span> (<span class="prom_add_percent">0</span>%) = <span class="promo_add_total">0</span> SCOR
            </div>--}}
            <button  class="btn-pay bg-btn-my"><span>{{ trans('messages.pay') }}</span></button>
            {{--<span class="or">Or</span>
            <div class="skinpay-block">
                <a href="" class="csgotower-btn-pay" id="skinpay-btn-pay"><span>Pay by Steam skins!</span></a>
            </div> --}}
        </form>

    </div>
</div>
