<div id="popup-withdraw" class="popup popup1">
    <div class="popup-login-title">Withdraw items</div>
    <div class="popup-content">
        <div class="popup-content-withdraw">
            <div data-ng-show="loading_selected_item" class="loading"><img src="/images/csgotower/squares.gif">
            </div>
            <form action="">
                <b>Please wait while your trade offers are being sent</b>

                <div data-ng-repeat="item in pubg_selected_all_items">
                    <div class="popup-content-withdraw-item">
                        Storage #<%item.id%>

                        <a href="" id="pubg_trade_withdraw_<%item.id%>" data-ng-if="item.status=='pending'"
                           style="pointer-events: none; cursor: default;">open trade</a>

                        <a href="" id="pubg_trade_withdraw_<%item.id%>" data-ng-if="item.status=='requested'"
                           style="pointer-events: none; cursor: default;">open trade</a>

                        <a href="https://steamcommunity.com/tradeoffer/<%item.offer_id%>" target="_blank" id="pubg_trade_withdraw_<%item.id%>" data-ng-if="item.status=='offered'">open trade</a>


                        <span id="pubg_status_withdraw_<%item.id%>" data-ng-if="item.status=='pending'" class="withdraw-send">Sending</span>

                        <span id="pubg_status_withdraw_<%item.id%>" data-ng-if="item.status=='requested'" class="withdraw-send">Sending</span>

                        <span id="pubg_status_withdraw_<%item.id%>" data-ng-if="item.status=='offered'" class="withdraw-success">Success</span>

                    </div>
                </div>

                {{--
                <div class="popup-content-withdraw-item">
                    Storage #0351
                    <a href="">open trade</a>
                    <span class="withdraw-error">Error</span>
                </div>--}}

            </form>
        </div>
    </div>

    <div class="popup-footer-cont">

    </div>

</div>