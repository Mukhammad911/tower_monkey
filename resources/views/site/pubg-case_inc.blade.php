@include('site.partials.site-stats')
<div id="content" class="pubg-case-bg">
    @role('user')

    @else
        <!-- Not logged -->
    @endrole

    <?php $i = 0; ?>

    @foreach($categories as $category)
        <div class="original-cases">
            @foreach($category->pubgCases as $key => $case)
                @if($case->isAvailablePubgCase())
                    <a href="/pubg-crate/{{ $case->id }}" class="original-cases-item pubg-original-cases-item avail link_case @if($category->name == 'PUBG') csgo cases @endif">
                        <img class="csgo_cases_image" src="{{ asset('images/cases') }}/{{ $case->image }}" alt="">
                        <div class="original-cases-item-info">
                            <span class="span_available">{{ (Lang::has('case.'.$case->name)) ? trans("case.$case->name") : $case->name }}</span>
                            <span class="span_available price"><i><span class="coins-case"></span> {{ $case->priceObj->price }}</i></span>
                        </div>
                    </a>
                @else
                    <a class="original-cases-item pubg-original-cases-item pubg-original-cases-item-unavailbale @if($category->name == 'PUBG') csgo cases @endif">
                        <img class="csgo_cases_image" src="{{ asset('images/cases') }}/{{ $case->image }}" alt="">
                        <div class="original-cases-item-info">
                            <span class="text_unavailable">{{ (Lang::has('case.'.$case->name)) ? trans("case.$case->name") : $case->name }}</span>
                            <span class="span_available price"><i><span class="coins-case"></span> {{ $case->priceObj->price }}</i></span>
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
        <?php $i++; ?>
    @endforeach
</div>
