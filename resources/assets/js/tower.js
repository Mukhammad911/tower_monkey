/*** EDITABLE SETTINGS ***/

var btn_level = '.tower-level';
var btn_play = '.tower-play';
var btn_take_profit = '.tower-take-prize';

var obj_row = '.tower-row';
var obj_cell = '.tower-cell';
var obj_bet = 'input.tower-bet';
var obj_current_profit = '.tower-current-prize';
var obj_current_prize = '.current-prize';

var form_prev_game = '.form-previous-ticket';
var form_next_game = '.form-next-ticket';

var live_list = '.ladder-right-usrs';
var live_games_counter = '.tower-games-counter';
var live_bets_counter = '.tower-bets-counter';

var AUDIO_TOWER_WIN= new Audio('/assets/snd/win_tower.mp3');
var AUDIO_TOWER_FAIL = new Audio('/assets/snd/fail_tower.mp3');

function tower_new_live_game(data) {
    if(parseFloat(data.profit)>0){
        var pl =parseInt(data.step);
        var lp = pl-1;
        return '<div class="ladder-right-usr"> ' +
            '<a href="/profile/' + data.user_id + '">' +
            '<span class="ladder-right-usr-ava">' +
            '<img src="' + data.avatar + '" alt="">' +
            '</span> ' +
            '<span class="ladder-right-usr-login"> ' + data.username + '</span> ' +
            '</a>'+
            '<span class="ladder-right-usr-bet">' + data.bet + '</span> ' +
            '<span class="ladder-right-usr-profit win">' + data.profit + '<span class="coin_total_profit">coins</span></span> ' +
            '<span class="ladder-right-usr-step">' + lp + '</span> ' +
            '<span class="ladder-ico"><i class="ladder-' + data.level + '"></i></span> ' +
            '</div>';
    }
    else{
        var p =parseInt(data.step);
        var l = p-1;
        return '<div class="ladder-right-usr"> ' +
            '<a href="/profile/' + data.user_id + '">' +
            '<span class="ladder-right-usr-ava">' +
            '<img src="' + data.avatar + '" alt="">' +
            '</span> ' +
            '<span class="ladder-right-usr-login"> ' + data.username + '</span> ' +
            '</a>'+
            '<span class="ladder-right-usr-bet">' + data.bet + '</span> ' +
            '<span class="ladder-right-usr-profit neon">' + data.profit + '<span class="coin_total_profit">coins</span></span> ' +
            '<span class="ladder-right-usr-step">' + l + '</span> ' +
            '<span class="ladder-ico"><i class="ladder-' + data.level + '"></i></span> ' +
            '</div>';
    }

}
/*** EDITABLE SETTINGS ***/



function Tower() {
    this.mode = ''; // play or pause
    this.demo = 'no';
    this.cells = [];
    this.step = 0;
    this.cell_number = 0;
    this.multiplier = 0;
    this.bet = 0;
    this.level = $(btn_level + '[data-active=1]').data('level');

    $('.heroes-chat-container').hide();
    $('.heroes-rank').show();
}
var tower = new Tower();

Tower.prototype.load = function(){
    this.checkOpenedGame();
    this.getSeeds();
};

Tower.prototype.buttonMode = function(obj, mode){
    if(mode == 'disabled'){
        obj.attr('disabled', 'disabled');
        obj.addClass('disabled');
    }else if(mode =='current'){
        obj.attr('current', 'current');
        obj.addClass('current');
    } else{
        obj.removeAttr('disabled');
        obj.removeClass('disabled');
        obj.removeAttr('current', 'current');
        obj.removeClass('current');
    }
};

Tower.prototype.buttonModeLevel = function(obj, mode){
    if(mode == 'disabled'){
        obj.attr('disabled', 'disabled');
        obj.addClass('disabled');
        obj.removeAttr('current', 'current');
        obj.removeClass('current');
    } else{
        obj.removeAttr('disabled');
        obj.removeClass('disabled');
        obj.attr('current', 'current');
        obj.addClass('current');
    }
};

Tower.prototype.start = function(){
    var self = this;
    this.request('/tower/start', {
        bet : self.bet,
        level : self.level,
        demo: self.demo
    }, function(res){
        if (res.success) {
            self.mode = 'play';
            self.step = 1;
            self.modifyBtnGame();
            /*if(self.demo == 'no'){
             changeBalance( - self.bet);
             }*/
            $(obj_current_profit).text(self.bet);

            self.prepare();
        } else if (res.message) {
            notifyError(res.message);
            self.restart();
        }
    });
};

Tower.prototype.restart = function(prepare){
    this.mode = 'pause';
    this.modifyBtnGame();
    if(prepare != 'off'){
        this.prepare();
    }
    this.buttonModeLevel($(btn_level), 'enabled');
    this.buttonModeLevel($(btn_level + '[data-level=' + this.level + ']'), 'disabled');
    $(obj_current_prize).text(0);
};

Tower.prototype.cellClick = function(){
    var self = this;
    var row = $(obj_row + '[data-row=' + this.step + ']');
    var opened_cell = row.find(obj_cell + '[data-cell=' + this.cell_number + ']');
    var row_cells = row.find(obj_cell);
    opened_cell.addClass('ladder-waiting');

    this.request('/tower/open-cell', {
        bet : self.bet,
        level : self.level,
        cell_number : self.cell_number,
        demo: self.demo
    }, function(res){
        if(res.success){
            self.openCell(opened_cell, row_cells, res.status);
            self.setSeeds();
            if(self.step == 10){
                if(self.demo == 'no'){
                    self.takeProfit();
                }else{
                    self.restart();
                }
            }
        }else{
            notifyError(res.message);
        }
    });
};

Tower.prototype.takeProfit = function(){
    this.buttonMode($('.tower-take-prize'), 'disabled');

    var self = this;
    var exist_rows = $(obj_row + '[data-row=' + (self.step + 1) + ']');
    exist_rows.removeClass('ladder-active-line');
    this.request('/tower/take-profit', {
    }, function(res){
        if(res.success){
            notifySuccess(res.message);
            self.restart();
        }else{
            notifyError(res.message);
        }
    });
};

Tower.prototype.levelClick = function(new_btn_level){
    var new_level = new_btn_level.data('level');
    if(new_level != this.level && this.mode == 'pause'){
        if(new_level == 'medium'){
            $(obj_cell).css('width', '46.9%');
            $(obj_cell + '[data-cell=3]').hide();
        }else{
            $(obj_cell).css('width', '31%');
            $(obj_cell + '[data-cell=3]').show();
        }

        $(btn_level).attr('data-active', 0);
        new_btn_level.attr('data-active', 1);
        this.prepare();
    }
};

Tower.prototype.changeBet = function(new_bet){
    if(this.mode == 'pause'){
        this.bet = new_bet;
        //$('.tower-bet').val(this.bet);
        this.setCells();
    }
};

Tower.prototype.liveGame = function(data){
    $(live_list).prepend(tower_new_live_game(data));

    $(live_games_counter).text(data.games);
    //$(live_bets_counter).text(data.bets);
    liveCounter($(live_bets_counter), parseInt($(live_bets_counter).text()), data.bets);

    var live_rows = $(live_list + ' tr');
    if( live_rows.length >= 15 ){
        live_rows.last().remove();
    }
};

Tower.prototype.checkOpenedGame = function(){
    var self = this;
    self.mode = 'pause';
    $(obj_cell).addClass('ladder-waiting');
    this.request('/tower/get-opened-game', {

    },function(res){
        if(res.success){
            self.mode = 'play';
            self.cells = res.game.cells;
            self.bet = res.game.bet;
            self.level = res.game.level;
            self.step = res.game.step;
            self.multiplier = res.game.multiplier;
            self.setCells();
            self.modifyBtnGame();
            self.buttonModeLevel($(btn_level), 'disabled');
        } else {
            self.prepare();
        }
    });
};

Tower.prototype.prepare = function(){
    this.bet = $(obj_bet).val();
    this.level = $(btn_level + '[data-active=1]').data('level');

    var level_status = $(btn_level + '[data-active=1]');
    level_status.addClass('current');

    if(this.mode == 'play'){
        $(obj_row + '[data-row=1]').find(obj_cell).removeClass('disabled');
        $(obj_row + '[data-row=1]').addClass('ladder-active-line');
    }else{
        $(obj_row).removeClass('ladder-active-line');
    }


    var self = this;
    this.request('/tower/multiplier', {
        level: self.level
    }, function(res){
        self.multiplier = res.multiplier;
        self.setCells();
    });
};

Tower.prototype.setCells = function(){
    var self = this;
    $(obj_row).each(function(row){
        var index = $(this).data('row');
        var cur_cells = $(obj_row + '[data-row=' + index + ']').find(obj_cell);
        var prize = self.calculatePrize(index);

        // reset
        cur_cells
            .removeClass('ladder-close')
            .removeClass('ladder-close-medium')
            .removeClass('ladder-win')
            .removeClass('ladder-waiting')
            .removeClass('disabled')
            .html(prize);

        // set
        if(self.mode == 'play'){
            var next_rows = $(obj_row + '[data-row=' + (this.step) + ']');
            next_rows.addClass('ladder-active-line');

            if(index > self.step){
                cur_cells.addClass('disabled');
            }
            if(index < self.step){
                cur_cells.addClass('ladder-close');
            }
        }else if(self.mode == 'pause'){
            cur_cells.addClass('disabled');
        }
    });

    $.each(this.cells, function(i, cell){
        var row = $(obj_row + '[data-row=' + cell.step + ']');
        var opened_cell = row.find(obj_cell + ':eq(' + (cell.cell - 1) + ')');

        opened_cell.addClass('ladder-win');
    });
};

Tower.prototype.calculatePrize = function(calc_step){
    return parseFloat((this.bet * Math.pow(parseFloat(this.multiplier), calc_step)).toFixed(2));
};

Tower.prototype.openCell = function(opened_cell, row_cells, status){
    opened_cell.removeClass('ladder-waiting');
    row_cells.addClass('disabled');
    opened_cell.addClass('ladder-win');
    this.getSeeds();

    if(status == 'victory'){
        var next_cells = $(obj_row + '[data-row=' + (this.step + 1) + ']').find(obj_cell);
        var next_rows = $(obj_row + '[data-row=' + (this.step + 1) + ']');
        var old_rows = $(obj_row + '[data-row=' + (this.step) + ']');
        next_cells.removeClass('disabled');
        next_rows.addClass('ladder-active-line');
        old_rows.removeClass('ladder-active-line');
        $(obj_current_profit).text(this.calculatePrize(this.step));
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_TOWER_WIN.volume = 0;
        }else{
            AUDIO_TOWER_WIN.volume = 1;
        }
        AUDIO_TOWER_WIN.currentTime = 0;
        AUDIO_TOWER_WIN.play();
        old_rows.removeClass('ladder-active-line');

        $(obj_current_prize).text( this.calculatePrize(this.step) );
    }else if(status == 'loss'){
        this.restart('off');
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_TOWER_FAIL.volume = 0;
        }else{
            AUDIO_TOWER_FAIL.volume = 1;
        }
        AUDIO_TOWER_FAIL.currentTime = 0;
        AUDIO_TOWER_FAIL.play();

        var old1_rows = $(obj_row + '[data-row=' + (this.step) + ']');
        old1_rows.removeClass('ladder-active-line');
        opened_cell.removeClass('ladder-win');

        if (this.level == 'medium')
        {
            opened_cell.addClass('ladder-close').addClass('ladder-close-medium');
        }
        else
        {
            opened_cell.addClass('ladder-close');
        }
        this.showAllCells();
    }
};

Tower.prototype.showAllCells = function(){
    var self = this;
    $(obj_row).each(function(ir, row){
        var row_status = 'disabled'; // loss, victory
        var opened_cell_number = 0;
        var cells = $(row).find(obj_cell);

        cells.each(function(ic, cell){
            if($(cell).hasClass('ladder-win')){
                row_status = 'victory';
                opened_cell_number = $(cell).data('cell');
            } else if($(cell).hasClass('ladder-close')){
                row_status = 'loss';
                opened_cell_number = $(cell).data('cell');
            }
        });

        var win_cell_number = getRandomInt(0, 2);
        var numbers = [1, 2, 3];
        numbers.splice(numbers.indexOf(opened_cell_number), 1);

        switch(row_status){
            case 'victory':
                switch(self.level){
                    case 'hard':
                    case 'medium':
                        break;

                    case 'easy':
                    default:
                        $(row).find(obj_cell + '[data-cell=' + numbers[win_cell_number] + ']').addClass('ladder-win');
                        break;
                }
                break;

            case 'loss':
                switch(self.level){
                    case 'hard':
                        $(row).find(obj_cell + '[data-cell=' + numbers[win_cell_number] + ']').addClass('ladder-win');
                        break;

                    case 'medium':
                    case 'easy':
                    default:
                        $(row).find(obj_cell + '[data-cell!=' + opened_cell_number + ']').addClass('ladder-win');
                        break;
                }
                break;

            case 'disabled':
            default:
                switch(self.level){
                    case 'hard':
                        win_cell_number = getRandomInt(1, 4);
                        $(row).find(obj_cell + '[data-cell=' + win_cell_number + ']').addClass('ladder-win');
                        break;

                    case 'medium':
                        win_cell_number = getRandomInt(1, 3);
                        $(row).find(obj_cell + '[data-cell=' + win_cell_number + ']').addClass('ladder-win');
                        break;

                    case 'easy':
                    default:
                        var loss_cell_number = getRandomInt(1, 4);
                        $(row).find(obj_cell + '[data-cell!=' + loss_cell_number + ']').addClass('ladder-win');
                        break;
                }
                break;
        }
    });
};

Tower.prototype.modifyBtnGame = function(){
    switch (this.mode){
        case 'play':
            $(btn_play).hide();
            if(this.demo == 'no'){
                this.buttonMode($(btn_take_profit), 'enabled');
                $(btn_take_profit).show();
            }
            $(obj_current_profit).text(this.calculatePrize(this.step - 1));
            break;

        case 'pause':
        default:
            this.buttonMode($(btn_play), 'enabled');
            $(btn_play).show();
            $(btn_take_profit).hide();
            break;
    }
};

Tower.prototype.getSeeds = function(){
    this.request('/tower/get-seeds', {
    }, function(res){
        if(typeof res.next_game == 'undefined'){
            return false;
        }
        $(form_prev_game + ' input[name=user_seed]').val(res.prev_game.user_seed);
        $(form_prev_game + ' input[name=server_seed]').val(res.prev_game.server_seed);
        $(form_prev_game + ' input[name=hash]').val(res.prev_game.hash);
        $(form_prev_game + ' input[name=win_number]').val(res.prev_game.win_number);

        $(form_next_game + ' input[name=user_seed]').val(res.next_game.user_seed);
        $(form_next_game + ' input[name=server_seed]').val(res.next_game.server_seed);
    });
};

Tower.prototype.setSeeds = function(){
    var self = this;
    this.request('/tower/set-seeds', {
    }, function(res){
        self.getSeeds();
    });
};

Tower.prototype.request = function(url, data, callback_success ){
    var self = this;
    return $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        cache: false,
        data: data,
        timeout:30000,
        beforeSend:function () {

        },
        success:  function (response) {
            callback_success(response);
        },
        error: function (xhr) {
            self.restart();
            $.each(xhr.responseJSON, function(key, val){
                notifyError(val, true);
            });

        }
    });
};



$(function () {
    tower.load();

    /*** ACTIONS ***/
    $(document).on('click', btn_play, function(e){
        e.preventDefault();

        tower.demo = $(this).data('demo');
        tower.buttonMode($(this), 'disabled');
        tower.buttonModeLevel($(btn_level), 'disabled');
        tower.start();
    });

    $(document).on('click', btn_take_profit, function(e){
        e.preventDefault();

        tower.takeProfit();
    });

    $(document).on('click', btn_level, function(e){
        e.preventDefault();

        tower.buttonModeLevel($(btn_level), 'disabled');
        tower.buttonModeLevel($(this), 'enabled');
        tower.levelClick($(this));
    });

    $(document).on('change keyup', obj_bet, function(e){
        e.preventDefault();

        var bet = $(this).val();
        if(bet > 5000){
            bet = 5000;
            $(this).val(bet);
        }
        tower.changeBet(bet);
    });

    $(document).on('click', obj_cell, function(e){
        e.preventDefault();
        if($(this).hasClass('disabled') || $(this).hasClass('ladder-close')){
            return false;
        }
        $(obj_cell).addClass('disabled');

        tower.step = $(this).parent(obj_row).data('row');
        tower.cell_number = $(this).data('cell');
        tower.cellClick();
    });

    $(document).on('click', '.ladder-page-verify-btn span, .ladder-page-verify-slide-title span', function() {
        $('.ladder-page-verify-slide').slideToggle(400);
        return false;
    });

    $(document).on('click', '.tower_refresh_seeds', function(){
        tower.getSeeds();
    });
    $(document).on('click', '.tower_max_plus', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat(bet_get) + 1;
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_plus', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat((parseFloat(bet_get) - 1).toFixed(2));
        if(bet <= 1){
            bet = 1;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });

    $(document).on('click', '.tower_min_1', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat((parseFloat(bet_get) + 0.01).toFixed(2));
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_2', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet =parseFloat((parseFloat(bet_get) + 0.10).toFixed(2));
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_3', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat((parseFloat(bet_get) + 1).toFixed(2));
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_4', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat(bet_get) + 10;
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_5', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat(bet_get) + 100;
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min_6', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = parseFloat(bet_get) + 1000;
        if(bet > 5000){
            bet = 5000;
            $('.tower-bet').val(bet);
        }
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_min', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = 1;
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });
    $(document).on('click', '.tower_max', function(e){
        e.preventDefault();
        var bet_get = $('.tower-bet').val();
        bet = 5000;
        tower.changeBet(bet);
        $('.tower-bet').val(bet);
    });

    $(document).on('click', '.heroes-chat', function(e){
        e.preventDefault();
        $('.heroes-rank').hide();
        $('.heroes-chat-container').show();
    });

    $(document).on('click', '.heroes', function(e){
        e.preventDefault();
        $('.heroes-chat-container').hide();
        $('.heroes-rank').show();
    });


    socket.on("tower:tower",function(message){
        var data = $.parseJSON(message.data);

        var count_tower_stat = parseInt($('#all-tower-stat').text());

        $('#all-tower-stat').text(++count_tower_stat);

        tower.liveGame(data);

        if (parseFloat(data.profit) > 0)
        {
            var drop;

            drop =
                '<div class="item category_tower tower-live" style="display: none;" data-tower-drop-id="' + data.game_id + '">' +
                '<a class="picture link_profile" href="/profile/' + data.user_id + '">' +
                '<img src="/images/csgotower/coin_tower.png" class="drop-image" alt="Drop Image">' +
                '<div class="title title-tower-livedrop">' + data.profit + '</div>' +
                '<div class="hover">' +
                '<div class="user">' +
                '<div class="userpic" style="background-image: url(' + data.avatar + ');"></div>' +
                '<div class="text">' +
                '<span>' + data.username + '</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</a>' +
                '</div>';

            $("#live_drop .drop_cont").prepend(drop);

            var prepend_el = $('#live_drop .drop_cont div[data-tower-drop-id=' + data.game_id + ']');

            if (prepend_el.length) {
                setTimeout(function () {
                    prepend_el.fadeIn(1000);
                    $itemsList = $("#live_drop .drop_cont div");

                    if ($itemsList.length > 15) {
                        $itemsList.last().remove();
                    }
                }.bind(this), 100);
            }
        }
    });
});