/********* DESIGN ***********/
/*global jQuery */
/*!
 * FitText.js 1.2
 *
 * Copyright 2011, Dave Rupert http://daverupert.com
 * Released under the WTFPL license
 * http://sam.zoy.org/wtfpl/
 *
 * Date: Thu May 05 14:23:00 2011 -0600
 */
(function ($) {
    $.fn.fitText = function (kompressor, options) {
        // Setup options
        var compressor = kompressor || 1,
            settings = $.extend({
                'minFontSize': Number.NEGATIVE_INFINITY,
                'maxFontSize': Number.POSITIVE_INFINITY
            }, options);

        return this.each(function () {
            // Store the object
            var $this = $(this);
            // Resizer() resizes items based on the object width divided by the compressor * 10
            var resizer = function () {
                $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
            };
            // Call once to set.
            resizer();
            // Call on resize. Opera debounces their resize by default.
            $(window).on('resize.fittext orientationchange.fittext', resizer);
        });
    };

    // EXPLOSION PLUGIN
    $.fn.explosion = function (options) {
        var settings = $.extend({
            particles: 50,
            radius: 300
        }, options);

        return this.each(function () {
            for (var i = 0; i < settings.particles; i++) {
                var particle = $("<div />").addClass(particleColor);
                $(particle).css("position", "absolute");
                $(this).append($(particle));
                $(particle).animate({
                        "margin-top": (Math.floor(Math.random() * settings.radius) - settings.radius / 2) + "px",
                        "margin-left": (Math.floor(Math.random() * settings.radius) - settings.radius / 2) + "px",
                        "opacity": 0
                    }, {
                        "duration": Math.floor(Math.random() * 500) + 500,
                        "complete": function () {
                            $(this).remove();
                        }
                    }
                );
            }
        });
    };
})(jQuery);

$(".upgrade-page-chance-in span").fitText(1.2, {minFontSize: '12px', maxFontSize: '26px'});
$(".upgrade-page-chance-out span").fitText(1.0, {minFontSize: '14px', maxFontSize: '35px'});

//var particleColor = "particle";
var particleColor = "particle2";

var timer_checked = false;

var AUDIO_UPGRADE_START = new Audio('/assets/snd/upgrade_start.mp3');
var AUDIO_UPGRADE_START_SLOW = new Audio('/assets/snd/upgrade_start_slow.mp3');
var AUDIO_UPGRADE_LOSE = new Audio('/assets/snd/upgrade_fail.mp3');
var AUDIO_UPGRADE_LOSE_SLOW = new Audio('/assets/snd/upgrade_fail_slow.mp3');
var AUDIO_UPGRADE_WIN = new Audio('/assets/snd/upgrade_win.mp3');
var AUDIO_UPGRADE_WIN_SLOW = new Audio('/assets/snd/upgrade_win_slow.mp3');



/**
 * Upgrade
 * @constructor
 */
function Upgrade(){
    this._arrow_obj = '.upgrade-page-chance';
    this._bet_crd_obj = '#upgrade_crd_in';
    this._target_crd_obj = '#upgrade_crd_out';
    this._current_multiplier_obj = '.current_multiplier';

    this._bubble = '';
    this._chance = 0;
    this._bet = 0;
    this._profit = 0;
    this._multiplier = 0;
    this._multiplier_custom = false;
}
var upgrade = new Upgrade();

Upgrade.prototype.prepare = function () {
    this.getBet();
    this.getMultiplier();
    this.getChance();

    if (this._bet < 0.5) {
        return false;
    }
    if (!this.validation()) return false;

    $('.current_multiplier').text(this._multiplier + 'X');
    this.setChance();
    $(this._bet_crd_obj).text(this._bet);
    $(this._target_crd_obj).text( (this._bet * this._multiplier).toFixed(2) );

    if (this._multiplier_custom == true) {
        this.customNominal(this._profit);
    }
};

Upgrade.prototype.play = function () {
    this.getBet();
    this.getMultiplier();
    if(this._multiplier_custom != true){
        this.getProfit();
    }
    this.btn_mode('disabled');

    if (this._bet.length != 0) {
        //changeBalance(-this._bet);
        if(timer_checked){
            this.pre_animation_slow();
        }else{
            this.pre_animation_fast();
        }
    }

    var self = this;
    this.request('/upgrade', {
        bet: this._bet,
        profit: this._profit
    }, function (res) {
        var timeout = timer_checked ? 5000 : 3000;
        if (res.win == true) {
            setTimeout((function () {
                //changeBalance(self._bet * self._multiplier)
            }), timeout);
        }
        if(timer_checked){
            self.after_animation_slow(res.win);
        }else{
            self.after_animation_fast(res.win);
        }
        self.getSeeds();
        $('#upgrade_hash').val(res.hash);
        $('#upgrade_win_number').val(res.win_number);
    });
};

Upgrade.prototype.pre_animation_fast = function () {
    if($('.header-sound').hasClass('header-sound-off')){
        AUDIO_UPGRADE_START.volume = 0;
        AUDIO_UPGRADE_START.play();
    }else{
        AUDIO_UPGRADE_START.volume = 1;
        AUDIO_UPGRADE_START.play();
    }
    var self = this;
    setTimeout(function () {
        $(self._arrow_obj).addClass("partplugon");
    }, 800);
};

Upgrade.prototype.after_animation_fast = function (win) {
    var self = this;
    if (typeof win == 'undefined' && typeof $(this._arrow_obj) != 'undefined') {
        setTimeout(function () {
            $(self._arrow_obj).removeClass("partplugon");
            self.btn_mode('enabled');
        }, 800);
        return true;
    }

    if (win == true) {
        this._bubble = "chance-buble-win";
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_UPGRADE_WIN.volume = 0;
            AUDIO_UPGRADE_WIN.play();
        }else{
            AUDIO_UPGRADE_WIN.volume = 1;
            AUDIO_UPGRADE_WIN.play();
        }
    } else {
        this._bubble = "chance-buble-lose";
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_UPGRADE_LOSE.volume = 0;
            AUDIO_UPGRADE_LOSE.play();
        }else{
            AUDIO_UPGRADE_LOSE.volume = 1;
            AUDIO_UPGRADE_LOSE.play();
        }
    }

    setTimeout(function () {
        $(".upgrade-page-chance").explosion({origin});
        var arrow_obj = $(self._arrow_obj);
        arrow_obj.append($("<div />").addClass(self._bubble));

        $("." + self._bubble).addClass("chance-buble-win-viz");
        arrow_obj.removeClass("partplugon");
        arrow_obj.addClass("partplugon2");

        setTimeout(function () {
            $("." + self._bubble).removeClass("chance-buble-win-viz");
            $("." + self._bubble).addClass("chance-buble-win-viz2");
            setTimeout(function () {
                $("." + self._bubble).remove();
                arrow_obj.removeClass("partplugon2");
                arrow_obj.addClass("partplugon3");
                setTimeout(function () {
                    $(".upgrade-page-chance").removeClass("partplugon3");
                    self.btn_mode('enabled');
                }, 600);
            }, 400);
        }, 2000);
    }, 1500);
};

Upgrade.prototype.pre_animation_slow = function () {
    if($('.header-sound').hasClass('header-sound-off')){
        AUDIO_UPGRADE_START_SLOW.volume = 0;
        AUDIO_UPGRADE_START_SLOW.play();
    }else{
        AUDIO_UPGRADE_START_SLOW.volume = 1;
        AUDIO_UPGRADE_START_SLOW.play();
    }

    var self = this;
    var arrow_obj = $(self._arrow_obj);
    setTimeout(function () {
        arrow_obj.addClass("partplugon");
        setTimeout(function () {
            arrow_obj.removeClass("partplugon");
            arrow_obj.addClass("partplugon2");
            setTimeout(function () {
                arrow_obj.removeClass("partplugon2");
                arrow_obj.addClass("partplugon3");
                setTimeout(function () {
                    arrow_obj.removeClass("partplugon3");
                    arrow_obj.addClass("partplugon");
                    setTimeout(function () {
                        arrow_obj.removeClass("partplugon");
                        arrow_obj.addClass("partplugon2");

                    },1500)
                }, 1800);
            },700)
        },700)
    }, 800);

};

Upgrade.prototype.after_animation_slow = function (win) {
    var self = this;
    if (typeof win == 'undefined') {
        setTimeout(function () {
            $(self._arrow_obj).removeClass("partplugon");
            self.btn_mode('enabled');
        }, 1800);
        return true;
    }

    if (win == true) {
        this._bubble = "chance-buble-win";
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_UPGRADE_WIN_SLOW.volume = 0;
            AUDIO_UPGRADE_WIN_SLOW.play();
        }else{
            AUDIO_UPGRADE_WIN_SLOW.volume = 1;
            AUDIO_UPGRADE_WIN_SLOW.play();
        }
    } else {
        this._bubble = "chance-buble-lose";
        if($('.header-sound').hasClass('header-sound-off')){
            AUDIO_UPGRADE_LOSE_SLOW.volume = 0;
            AUDIO_UPGRADE_LOSE_SLOW.play();
        }else{
            AUDIO_UPGRADE_LOSE_SLOW.volume = 1;
            AUDIO_UPGRADE_LOSE_SLOW.play();
        }
    }

    setTimeout(function () {
        $(".upgrade-page-chance").explosion({origin});
        var arrow_obj = $(self._arrow_obj);
        arrow_obj.append($("<div />").addClass(self._bubble));

        $("." + self._bubble).addClass("chance-buble-win-viz");
        setTimeout(function () {
            $("." + self._bubble).removeClass("chance-buble-win-viz");
            $("." + self._bubble).addClass("chance-buble-win-viz2");
            setTimeout(function () {
                $("." + self._bubble).remove();
                arrow_obj.removeClass("partplugon2");
                arrow_obj.addClass("partplugon3");
                setTimeout(function () {
                    $(".upgrade-page-chance").removeClass("partplugon3");
                    self.btn_mode('enabled');
                }, 600);
            }, 400);
        }, 2000);
    }, 4800);
};

Upgrade.prototype.btn_mode = function (mode) {
    if (mode == 'disabled') {
        $('.upgrade_btn').attr('disabled', 'disabled');
    } else {
        $('.upgrade_btn').removeAttr('disabled');
    }
};

Upgrade.prototype.customNominal = function (target_price) {
    $(this._target_crd_obj).text((target_price).toFixed(2));
    this._profit = target_price;
    this._multiplier_custom = true;
    if (!this.validation()) return false;

    this.setMultiplier(target_price);
    //$(this._target_crd_obj).text(target_price);
    $(this._current_multiplier_obj).text(this._multiplier + 'X');

    var self = this;
    this.request('/upgrade/get-custom-chance', {
        bet: this._bet,
        profit: this._profit
    }, function (res) {
        if (res.success == true) {
            self._chance = res.chance;
            self.setChance();
        }
    });
};

Upgrade.prototype.getSeeds = function () {
    this.request('/upgrade/get-seeds', {}, function(res){
        if(res.success){
            $('#upgrade_next_user_seed').val(res.next_game.user_seed);
            $('#upgrade_next_server_seed').val(res.next_game.server_seed);

            if(res.prev_game){
                $('#upgrade_prev_user_seed').val(res.prev_game.user_seed);
                $('#upgrade_prev_server_seed').val(res.prev_game.server_seed);
                $('#upgrade_win_number').val(res.prev_game.win_number);
                $('#upgrade_hash').val(res.prev_game.hash);
            }
        }
    });
};

Upgrade.prototype.is_page = function(){
    console.log('type: ' + $('*').is('.upgrade-page'));
    if(!$('*').is('.upgrade-page')){
        return false;
    }
};

Upgrade.prototype.getBet = function() {
    var bet_obj = $('#upgrade_amount');
    this._bet = bet_obj.val() ? bet_obj.val() : 0;
};

Upgrade.prototype.getMultiplier = function() {
    if (this._multiplier_custom != true) {
        var multiplier_obj = $('.multiplier[data-active=true]');
        this._multiplier = multiplier_obj.data('val');
    }
};

Upgrade.prototype.setMultiplier = function() {
    this._multiplier = (this._profit / this._bet).toFixed(2);
};

Upgrade.prototype.getChance = function() {
    if (this._multiplier_custom != true) {
        var multiplier_obj = $('.multiplier[data-active=true]');
        this._chance = multiplier_obj.data('chance');
    }
};

Upgrade.prototype.setChance = function() {
    $('.current_chance').text(this._chance + '%');
};

Upgrade.prototype.getProfit = function(){
    this._profit = this._bet * this._multiplier;
};

Upgrade.prototype.request = function(url, data, callback) {
    var self = this;
    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        cache: false,
        data: data,
        success: function (res) {
            callback(res);
        },
        error: function (res) {
            self.after_animation_fast();
            //changeBalance(self._bet);
            switch (res.status) {
                case 422:
                    var answer = JSON.parse(res.responseText);
                    for (var val in answer) {
                        notifyError(answer[val], true);
                    }
                    break;
                default:
                    notifyError('status: ' + res.status);
                    break;
            }

            self.btn_mode('enabled');
        }
    });
};

Upgrade.prototype.validation = function() {
    if (typeof this._bet == 'undefined' || this._bet < 0.5) {
        //notifyError('Bet should be more than 1 SCOR!');
        return false;
    }
    if (this._multiplier_custom == true && this._bet >= this._profit) {
        notifyError('Profit should be more than your bet!');
        return false;
    }

    return true;
};



$(function () {

    /********* ACTIONS ***********/

    $(document).on('click', '.upgrade_nominal', function (e) {
        e.preventDefault();

        $('.multiplier').attr('data-active', '');
        $('.custom_multiplier').attr('data-active', 'true');
        window.scrollTo(0, 0);

        upgrade.customNominal($(this).data('price'));
    });

    $(document).on('keyup', '#upgrade_amount', function () {
        console.log('keyup');
        upgrade.prepare();
    });

    $(document).on('click', '.multiplier', function () {
        if ($(this).hasClass("upgrade-page-checked")) {
        } else {
            $(".upgrade-page-check1 li").removeClass("upgrade-page-checked");
            $(this).addClass("upgrade-page-checked");
        }

        if ($(this).hasClass('custom_multiplier')) {
            window.scrollTo(0, window.innerHeight);
            return true;
        }

        $('.multiplier').attr('data-active', '');
        $(this).attr('data-active', 'true');

        upgrade._multiplier_custom = false;
        upgrade.prepare();
    });

    $(document).on('click', '.upgrade_btn', function (e) {
        e.preventDefault();
        upgrade.play();
    });

    $(document).on('click', '.upgrade-page-timer-btn', function () {
        if (!timer_checked) {
            $(this).addClass("upgrade-page-timer-btn-on");
            timer_checked = true;
        } else {
            $(this).removeClass("upgrade-page-timer-btn-on");
            timer_checked = false;
        }
    });

    $(document).on('click', '.credits-cases-check-numbers', function(){
        if($('#upgrade_next_user_seed').val() == ''){
            upgrade.getSeeds();
        }
    });

    $(document).on('click', '.upgrade_refresh_hash', function(){
        upgrade.getSeeds();
    });

});
