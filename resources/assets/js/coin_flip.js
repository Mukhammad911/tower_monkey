function CoinFlip() {
    this.mode = 'roll';
    this.bet = 10;
    this.balance = 0;
    this.join_game_id = '';
}
CoinFlip.prototype.start = function () {
    this.before_start(function () {

    });
};
CoinFlip.prototype.before_start = function (callback) {
    this.request('/coin-flip/check-auth', {}, function (response) {
        if (response.success) {
            this.check_balance(function () {
                this.check_input_value(function () {
                    this.request('/coin-flip/start-game', {
                        'mode': this.mode,
                        'bet': this.bet,
                        'balance': this.balance
                    }, function (response_start_game) {
                        if (response_start_game.success) {
                            console.log(response_start_game.success);
                        }
                        else {
                            notifyError(response.message, true);
                        }
                    })
                }.bind(this))
            }.bind(this))
        }
        else {
            notifyError(response.message, true);
        }
    }.bind(this));
};
CoinFlip.prototype.load = function () {
    $('.flip_content_full').hide();
    $('.coin_flip').show();
    this.request('/coin-flip/load-games', {}, function (response) {
        $('.flip_content_full').show();
        $('.coin_flip').hide();
        for (var i = 0; i < response.length; i++) {
            if (response[i].mode == "roll") {
                $('.flip_content_full').append(coin_flip_new_live_game(response[i]));
            }
            else {
                $('.flip_content_full').append(coin_flip_new_live_game(response[i]));
            }
        }
    })

};
CoinFlip.prototype.check_input_value = function (callback) {
    this.bet = $('.coin_flip_input_amount').val();
    if (this.bet <= 0) {
        notifyError("Error");
        return
    }
    else {
        callback();
    }

};
CoinFlip.prototype.check_balance = function (callback) {
    this.request('/coin-flip/check-balance', {}, function (response) {
        if (response.success) {
            this.balance = response.data;
            callback();
        }
        else {
            notifyError(response.message, true);
        }
    }.bind(this));

};

CoinFlip.prototype.join = function () {
    this.request('/coin-flip/check-auth', {}, function (response) {
        if (response.success) {
            this.check_balance(function () {
                this.startJoin();
            }.bind(this))
        }
        else {
            notifyError(response.message, true);
        }
    }.bind(this));
};
CoinFlip.prototype.startJoin = function () {
    this.request('/coin-flip/start-join', {
        'game_id': this.join_game_id
    }, function () {

    })
};

CoinFlip.prototype.startTimer = function (data) {

    $('#id_' + data.game_id).find('.join_flip_roll_button').hide();

    var arr = [4,3,2,1];
    var numberAnimate = function (data) {
        if (arr.length) {
            var result_array = arr[0];
            setTimeout(function () {
                $('#id_' + data.game_id).find('.number-animate').show().text(result_array-1);
                arr.splice(0, 1);
                numberAnimate(data);
            }.bind(this), 1000);
        }
        else {
            $('#id_' + data.game_id).find('.number-animate').hide();
            if(data.win=="csgo"){
                var sum_price = (parseFloat(data.bet) *2).toFixed(2);
                $('#id_' + data.game_id).find('.flip_roll_change').append(winAnimate(data));
                setTimeout(function () {
                    $('#id_' + data.game_id).find('#f1_card').addClass("win_csgo");
                }.bind(this),100);
                setTimeout(function () {
                    $('#id_' + data.game_id).find('.second_child_state').addClass("opacity_state_active");
                    $('#id_' + data.game_id).find('.first_child_state').addClass("opacity_state_inactive");

                    $('#id_' + data.game_id).find('.amount_of_second_child').text(sum_price);
                    $('#id_' + data.game_id).find('.amount_of_first_child').text('0.00');
                }.bind(this),3900);
                setTimeout(function () {
                    $('#id_' + data.game_id).addClass('shadow_coin_flip');
                }.bind(this),5100);
                setTimeout(function () {
                    $('#id_' + data.game_id).remove();
                }.bind(this),11800);
            }
            else{
                var sum_price = (parseFloat(data.bet) *2).toFixed(2);
                $('#id_' + data.game_id).find('.flip_roll_change').append(winAnimate(data));
                setTimeout(function () {
                    $('#id_' + data.game_id).find('#f1_card').addClass("win_roll");
                }.bind(this),100);
                setTimeout(function () {
                    $('#id_' + data.game_id).find('.second_child_state').addClass("opacity_state_inactive");
                    $('#id_' + data.game_id).find('.first_child_state').addClass("opacity_state_active");
                    $('#id_' + data.game_id).find('.amount_of_second_child').text('0.00');
                    $('#id_' + data.game_id).find('.amount_of_first_child').text(sum_price);
                }.bind(this),3900);
                setTimeout(function () {
                    $('#id_' + data.game_id).addClass('shadow_coin_flip');
                }.bind(this),5100);
                setTimeout(function () {
                    $('#id_' + data.game_id).remove();
                }.bind(this),11800);
            }
        }
    }.bind(this);

    numberAnimate(data);



};

CoinFlip.prototype.request = function (url, data, callback_success) {
    var self = this;
    return $.ajax({
        url: url,
        method: 'POST',
        dataType: 'json',
        cache: false,
        data: data,
        timeout: 30000,
        beforeSend: function () {

        },
        success: function (response) {
            callback_success(response);
        },
        error: function (xhr) {
            /*    self.restart();
             $.each(xhr.responseJSON, function(key, val){
             notifyError(val, true);
             });*/

        }
    });
};

function winAnimate(data) {
return '<div id="f1_container" data-id_animate="'+data.game_id+'" >'+
       '<div id="f1_card" class="shadow">'+
       '<div class="front face">'+
        '<img class="coin_rotate " src="/images/csgotower/tails.png"/>'+
        '</div>'+
        '<div class="back face center">'+
        '<img  class="coin_rotate" src="/images/csgotower/heads.png"/>'+
        '</div>'+
        '</div>'+
        '</div>';
}

function coin_flip_new_live_game(data) {
    if (data.mode == 'roll') {
        return '<div id="id_' + data.game_id + '" data-game-mode="roll"  class="one_roll">' +
            '<div class="first_child_state">' +
            '<img  src="' + data.avatar + '" class="first_child_img">' +
            '<div class="flip_coin_coin_next_second">' +
            '<p class="name_of_first_child">' + data.user_name + '</p>' +
            '<img src="/images/csgotower/coins.png" class="img_flip_coin_icon"/>' +
            '<p class="amount_of_first_child">' + (parseFloat(data.bet)).toFixed(2) + '</p>' +
            '</div>' +
            '</div>' +
            '<div class="second_child_state">' +
            '</div>' +
            '<div class="flip_roll_change">' +
            '<p class="number-animate"></p>' +
            '<button class="join_flip_roll_button" data-game_id="' + data.game_id + '">JOIN</button>' +
            '</div>' +
            '</div>';
    }
    else {

        return '<div id="id_' + data.game_id + '" data-game-mode="csgo"  class="one_roll">' +
            '<div class="first_child_state">' +
            '</div>' +
            '<div class="second_child_state">' +
            '<img src="' + data.avatar + '" class="second_child_img">' +
            '<div class="flip_coin_coin_next_second">' +
            '<p class="name_of_second_child">' + data.user_name + '</p>' +
            '<img src="/images/csgotower/coins.png" class="img_flip_coin_icon"/>' +
            '<p class="amount_of_second_child">' + (parseFloat(data.bet)).toFixed(2) + '</p>' +
            '</div>' +
            '</div>' +
            '<div class="flip_roll_change">' +
            '<p class="number-animate"></p>' +
            '<button class="join_flip_roll_button"  data-game_id="' + data.game_id + '">JOIN</button>' +
            '</div>' +
            '</div>';
    }

}
function game_append(data) {
    var game_mode = $('#id_' + data.game_id).data('game-mode');
    if (game_mode == "roll") {
        $('#id_' + data.game_id).find('.second_child_state').append('<img src="' + data.avatar + '" class="second_child_img">' +
            '<div class="flip_coin_coin_next_second">' +
            '<p class="name_of_second_child">' + data.user_name + '</p>' +
            '<img src="/images/csgotower/coins.png" class="img_flip_coin_icon"/>' +
            '<p class="amount_of_second_child">' + (parseFloat(data.bet)).toFixed(2) + '</p>' +
            '</div>');
    }
    else {
        $('#id_' + data.game_id).find('.first_child_state').append('<img src="' + data.avatar + '" class="first_child_img">' +
            '<div class="flip_coin_coin_next_first">' +
            '<p class="name_of_first_child">' + data.user_name + '</p>' +
            '<img src="/images/csgotower/coins.png" class="img_flip_coin_icon"/>' +
            '<p class="amount_of_first_child">' + (parseFloat(data.bet)).toFixed(2) + '</p>' +
            '</div>');
    }
}

var coin_flip = new CoinFlip();

$(function () {

    coin_flip.load();

    $(document).on('click', '.coin_flip_button_create_game', function (e) {
        e.preventDefault();
        coin_flip.start();

    });
    $(document).on('click', '.join_flip_roll_button', function (e) {
        e.preventDefault();
        coin_flip.join_game_id = $(this).data('game_id');
        coin_flip.join();

    });

    $(document).on('click', '.icon_roll', function (e) {
        e.preventDefault();
        $('.icon_csgo').removeClass('flip_icon_active');
        $('.icon_roll').removeClass('flip_icon_active').addClass('flip_icon_active');
        coin_flip.mode = 'roll';

    });

    $(document).on('click', '.icon_csgo', function (e) {
        e.preventDefault();
        $('.icon_csgo').removeClass('flip_icon_active').addClass('flip_icon_active');
        $('.icon_roll').removeClass('flip_icon_active');
        coin_flip.mode = 'csgo';
    });

    $(document).on('change keyup', '.coin_flip_input_amount', function(e){
        e.preventDefault();
        var bet = $(this).val();
        if(bet > 5000){
            bet = 5000;
            $(this).val(bet);
        }
        else if(bet < 0.1){
            bet = 0.1;
            $(this).val(bet);
        }
    });



    socket.on("coin-flip:coin-flip", function (message) {
        var data = $.parseJSON(message.data);
        $('.flip_content_full').append(coin_flip_new_live_game(data));

        console.log(data);
    });

    socket.on("coin-flip-join:coin-flip-join", function (message) {
        var data = $.parseJSON(message.data);
        console.log('join');
        console.log(data);
        game_append(data);
        coin_flip.startTimer(data);
    });

});
