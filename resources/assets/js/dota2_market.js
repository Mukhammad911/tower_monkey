var beastDota = angular.module('beastDota', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
beastDota.service('sharedPropertiesDota', function () {
    var cases = [];
    var items = [];

    return {
        set_case: function (new_case) {
            cases.push(new_case);
        },
        get_case: function () {
            return cases
        },
        set_item: function (new_item) {
            items.push(new_item);
        },
        get_item: function () {
            return items;
        }
    }
});
beastDota.factory('PagerServiceDota', function () {
    var service = {};
    service.GetPager = GetPager;
    return service;
    function GetPager(totalItems, currentPage, pageSize) {
        currentPage = currentPage || 1;
        pageSize = pageSize || 33;
        var totalPages = Math.ceil(totalItems / pageSize);
        var startPage, endPage;
        if (totalPages <= 15) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        var pages = _.range(startPage, endPage + 1);
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

});
beastDota.controller('MyCtrl3', function ($scope, sharedPropertiesDota, $http, $rootScope, $q, $filter, PagerServiceDota) {
    var all_arms = [];
    var count_profile_product = [];
    var origin_balance = '';
    var trade_url_bool = false;
    var timeout_clear = false;
    var rub_usd_sell = 0;
    var setting_checked = false;
    $scope.dota_searchString = '';
    $scope.dota_startNumber = 0;
    $scope.dota_endNumber = 3000;
    $scope.dota_sortAsc = true;
    $scope.dota_sortDesc = false;
    $scope.dota_curPage = 0;
    $scope.dota_pageSize = 33;
    $scope.dota_itemsPerPage = 5;
    $scope.dota_weapon_types = all_arms;
    $scope.dota_arms = '';
    $scope.dota_propertyName = 'price';
    $scope.dota_reverse = false;
    $scope.dota_available_crd = 0;
    $scope.dota_get_at_summ_price = 0;
    $scope.dota_clear_items = [];
    $scope.dota_arms_item = [];
    $scope.dota_cases_id = [];
    $scope.dota_case_price = '';
    $scope.dota_value_of_profit = [];
    $scope.dota_result_item = [];
    $scope.dota_balance = 0;
    $scope.dota_pager = {};
    $scope.heroes = 'All';
    $scope.heroes_array = [];

    $scope.init = function () {
        if (getDotaSettings()) {
        }
    };

    function clearAllDota() {
        if (timeout_clear) {
            all_arms = [];
            count_profile_product = [];
            origin_balance = '';
            trade_url_bool = false;
            timeout_clear = false;
            rub_usd_sell = 0;
            $scope.dota_searchString = '';
            $scope.dota_startNumber = 0;
            $scope.dota_endNumber = 3000;
            $scope.dota_sortAsc = true;
            $scope.dota_sortDesc = false;
            $scope.dota_curPage = 0;
            $scope.dota_pageSize = 33;
            $scope.dota_itemsPerPage = 5;
            $scope.dota_weapon_types = all_arms;
            $scope.dota_arms = '';
            $scope.dota_propertyName = 'price';
            $scope.dota_reverse = false;
            $scope.dota_available_crd = 0;
            $scope.dota_get_at_summ_price = 0;
            $scope.dota_clear_items = [];
            $scope.dota_arms_item = [];
            $scope.dota_cases_id = [];
            $scope.dota_case_price = '';
            $scope.dota_value_of_profit = [];
            $scope.dota_result_item = [];
            $scope.dota_balance = 0;
            $scope.dota_pager = {};
            $scope.heroes = 'All';
            $scope.heroes_array = [];
            $scope.init();
        }
        else {
        }
    }

    function dota_setPage(page) {
        if (page < 1 || page > $scope.dota_pager.totalPages) {
            return;
        }
        $scope.dota_pager = PagerServiceDota.GetPager($scope.dota_dummyItems.length, page);
        $scope.dota_arms = $scope.dota_dummyItems.slice($scope.dota_pager.startIndex, $scope.dota_pager.endIndex + 1);
        setTimeout(function () {
            displaySelectedDotaItems()
        }, 100);
    }

    function getDotaBalance() {
        $scope.dota_loading_balance = true;
        $http.get('/get/csgo/balance')
            .success(function (data, status, headers, config) {
                $scope.dota_balance = parseFloat(data).toFixed(2);
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                $scope.dota_loading_balance = false;
                origin_balance = $scope.dota_balance;
            });
    }

    function getDotaSettings() {
        $('.market').show();
        $http.get('/get/settings')
            .success(function (data, status, headers, config) {
                rub_usd_sell = parseFloat(((parseFloat(data)) * 100).toFixed(2));
                setting_checked = true;
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                if (setting_checked) {
                    getDotaBalance();
                    getDotaProfileProduct();
                    getDotaSkins();
                    getDotaHeroes();
                    $scope.dota_desc();
                }
            });
    }

    function getDotaSkins() {
        all_arms = [];
        $scope.dota_loading = true;
        $http.get('/get/dota2/skins')
            .success(function (data, status, headers, config) {
                $('.market').hide();
                $scope.dota_loading = false;
                for (var i = 0; i < data.length; i++) {
                    all_arms.push({
                        img_url: data[i].image_hash,
                        id: data[i].id,
                        name: data[i].name,
                        hero: data[i].hero,
                        short_description: data[i].short_description,
                        quality: data[i].quality,
                        price: parseFloat((parseInt(data[i].price) / rub_usd_sell).toFixed(2))
                    });
                    sharedPropertiesDota.set_item(data[i]);
                }
            })
            .error(function (data, status, headers, config) {
                $scope.dota_loading = false;
                $('.market').hide();
            })
            .then(function (res) {
                $('.market').hide();
                timeout_clear = true;
                setAllDotaSkins($scope.dota_searchString, $scope.dota_startNumber, $scope.dota_endNumber, $scope.dota_sortAsc, $scope.dota_sortDesc, $scope.heroes);
            });
    }

    function getDotaHeroes() {
        $http.get('/get/dota2/heroes')
            .success(function (data, status, headers, config) {
                for (var i = 0; i < data.length; i++) {
                    $scope.heroes_array.push(data[i]);
                }
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
            });
    }

    function setAllDotaSkins(searchString, startNumber, endNumber, sortAsc, sortDesc, heroes) {
        $scope.dota_arms = '';
        var result_asc = [];
        var sort_value_asc = [];
        var resultItems = '';
        $scope.dota_dummyItems = '';
        $scope.dota_pager = {};
        for (i = 0; i < all_arms.length; i++) {
            if ((all_arms[i].name.indexOf(searchString) > -1) && (all_arms[i].price >= startNumber) && (all_arms[i].price <= endNumber)) {
                if (heroes == 'All') {
                    result_asc.push(all_arms[i]);
                }
                else {
                    if (all_arms[i].hero == heroes) {
                        result_asc.push(all_arms[i]);
                    }
                }
            }
        }
        if (result_asc.length == 0) {

        }
        else {
            if (sortAsc) {
                sort_value_asc = _.sortBy(result_asc, function (num) {
                    return num.price;
                });
                resultItems = sort_value_asc;
            }
            else {
                sort_value_asc = _.sortBy(result_asc, function (num) {
                    return num.price;
                });
                resultItems = sort_value_asc.reverse();
            }
            $scope.dota_dummyItems = resultItems;
            $scope.dota_setPage = dota_setPage;
            $scope.dota_setPage(1);
        }
        $('.pagination-div').show();
    };
    $scope.dota_searchStringClick = function () {
        setAllDotaSkins($scope.dota_searchString, $scope.dota_startNumber, $scope.dota_endNumber, $scope.dota_sortAsc, $scope.dota_sortDesc, $scope.heroes);
    };
    $scope.dota_select_box = function () {
        if (t == 0) {
            $('#select_box').show();
            t++;
        }
        else if (t == 1) {
            $('#select_box').hide();
            t--;
        }
    };
    $scope.dota_select_box_hero = function () {
        if (h == 0) {
            $('#select_box_hero').show();
            h++;
        }
        else if (h == 1) {
            $('#select_box_hero').hide();
            h--;
        }
    };
    $scope.select_heroes = function (name) {
        h = 0;
        $('#select_box_hero').hide();
        $scope.heroes = name;
        setAllDotaSkins($scope.dota_searchString, $scope.dota_startNumber, $scope.dota_endNumber, $scope.dota_sortAsc, $scope.dota_sortDesc, $scope.heroes);
    };
    $scope.dota_asc = function () {
        $scope.dota_sortAsc = true;
        $scope.dota_sortDesc = false;
        $('#select_box').attr('style', 'display: none');
        $scope.dota_text_sort = 'Price: Low -> High';
        t = 0;
        setAllDotaSkins($scope.dota_searchString, $scope.dota_startNumber, $scope.dota_endNumber, $scope.dota_sortAsc, $scope.dota_sortDesc, $scope.heroes);
        setTimeout(function () {
            displaySelectedDotaItems()
        }, 100);
    };
    $scope.dota_desc = function () {
        $('#select_box').attr('style', 'display: none');
        $scope.dota_sortAsc = false;
        $scope.dota_sortDesc = true;
        $scope.dota_text_sort = 'Price: High -> Low';
        t = 0;
        setAllDotaSkins($scope.dota_searchString, $scope.dota_startNumber, $scope.dota_endNumber, $scope.dota_sortAsc, $scope.dota_sortDesc, $scope.heroes);
        setTimeout(function () {
            displaySelectedDotaItems()
        }, 100);
    };
    function displaySelectedDotaItems() {
        for (k = 0; k < $scope.dota_clear_items.length; k++) {
            $('#dota_item_price_' + $scope.dota_clear_items[k].id).addClass('check');
            $('#dota_item_' + $scope.dota_clear_items[k].id).addClass('check');
        }
    }

    $scope.$watch('dota_selected', function () {
        if ($scope.dota_selected == '1') {
            $scope.dota_reverse = false;
        }
        else if ($scope.dota_selected == '2') {
            $scope.dota_reverse = true;
        }
    });
    $scope.dota_select_all_view = function () {
        for (o = 0; o < $scope.dota_arms.length; o++) {
            var int_id = parseInt($scope.dota_arms[o].id);
            if (_.where($scope.dota_clear_items, {
                    'id': int_id,
                    'img': $scope.dota_arms[o].img,
                    'name': $scope.dota_arms[o].name,
                    'short_description': $scope.dota_arms[o].short_description,
                    'item_value': 0,
                }).length) {
            }
            else {
                if ($scope.dota_clear_items.length >= 8) {
                    notifyError('You can not get more 8 items!', false, true);
                    return
                }
                else {
                    $scope.dota_dif = 0;
                    $scope.dota_dif = parseFloat(($scope.dota_balance - parseFloat($scope.dota_arms[o].price)).toFixed(2));
                    $('#dota_item_price_' + $scope.dota_arms[o].id).addClass('check');
                    $('#dota_item_' + $scope.dota_arms[o].id).addClass('check');
                    $scope.dota_available_crd = parseInt($scope.dota_available_crd) + 1;
                    $scope.dota_get_at_summ_price = parseFloat((parseFloat($scope.dota_get_at_summ_price) + parseFloat($scope.dota_arms[o].price)).toFixed(2));
                    $scope.dota_balance = parseFloat((parseFloat($scope.dota_balance) - parseFloat($scope.dota_arms[o].price)).toFixed(2));
                    $scope.dota_clear_items.push({
                        'id': int_id,
                        'img': $scope.dota_arms[o].image,
                        'name': $scope.dota_arms[o].name,
                        'short_description': $scope.dota_arms[o].short_description,
                        'item_value': 0
                    });
                }
            }
        }
    };
    function getDotaProfileProduct() {
        $scope.dota_loading_count_item = true;
        $http.get('/get/dota/skins/profile')
            .success(function (data, status, headers, config) {
                for (n = 0; n < data.length; n++) {
                    count_profile_product.push(data[n]);
                }
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                $scope.dota_loading_count_item = false;
                $scope.dota_count_profile = count_profile_product.length;
            });
    }

    $scope.dota_sell_drops_market = function (id) {
        $.ajax({
            method: 'get',
            dataType: 'json',
            url: BASE_URL + '/dotasell/' + id,
            beforeSend: function () {
                $('#text_selling_' + id).show();
            },
            success: function (ret) {
                if (ret.success) {
                    $('#text_selling_' + id).remove();
                    $('#csgo_drop_modal_' + id).css('display', 'none');
                }
            }
        })
    };

    $scope.dota_delete_from_icon = function(item_id, item_image, item_name, short_description, price){
        var id_item = parseInt(item_id);
        if (_.where($scope.dota_clear_items, {
            'id': id_item,
            'img': item_image,
            'name': item_name,
            'short_description': short_description,
            'item_value': 0,
            'price': price,
        }).length) {
            $("#dota_item_price_" + item_id).removeClass('check');
            $("#dota_item_" + item_id).removeClass('check');
            $scope.dota_available_crd = parseInt($scope.dota_available_crd) - 1;
            $scope.dota_get_at_summ_price = parseFloat((parseFloat($scope.dota_get_at_summ_price) - parseFloat(price)).toFixed(2));
            $scope.dota_balance = parseFloat((parseFloat($scope.dota_balance) + parseFloat(price)).toFixed(2));
            $scope.dota_clear_items = _.without($scope.dota_clear_items, _.findWhere($scope.dota_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'short_description': short_description,
                'item_value': 0,
                'price': price,
            }));
        }

    }
    $scope.dota_getItemId = function (item_id, item_image, item_name, short_description, price) {
        var id_item = parseInt(item_id);
        if (_.where($scope.dota_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'short_description': short_description,
                'item_value': 0,
                'price': price
            }).length) {
            $("#dota_item_price_" + item_id).removeClass('check');
            $("#dota_item_" + item_id).removeClass('check');
            $scope.dota_available_crd = parseInt($scope.dota_available_crd) - 1;
            $scope.dota_get_at_summ_price = parseFloat((parseFloat($scope.dota_get_at_summ_price) - parseFloat(price)).toFixed(2));
            $scope.dota_balance = parseFloat((parseFloat($scope.dota_balance) + parseFloat(price)).toFixed(2));
            $scope.dota_clear_items = _.without($scope.dota_clear_items, _.findWhere($scope.dota_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'short_description': short_description,
                'item_value': 0,
                'price': price
            }));
        }
        else {
            if ($scope.dota_clear_items.length >= 8) {
                notifyError('You can not get more 8 items!', false, true);
                return
            }
            else {
                $scope.dota_dif = 0;
                $scope.dota_dif = parseFloat(($scope.dota_balance - parseFloat(price)).toFixed(2));
                $('#dota_item_price_' + item_id).addClass('check');
                $('#dota_item_' + item_id).addClass('check');
                $scope.dota_available_crd = parseInt($scope.dota_available_crd) + 1;
                $scope.dota_get_at_summ_price = parseFloat((parseFloat($scope.dota_get_at_summ_price) + parseFloat(price)).toFixed(2));
                $scope.dota_balance = parseFloat((parseFloat($scope.dota_balance) - parseFloat(price)).toFixed(2));
                $scope.dota_clear_items.push({
                    'id': id_item,
                    'img': item_image,
                    'name': item_name,
                    'short_description': short_description,
                    'item_value': 0,
                    'price': price
                });
            }
        }
    };
    $scope.dota_refresh_click = function () {
        clearAllDota();
    };

    $scope.dota_open_modal = function () {

        $scope.dota_loading_count_item = true;
        var get_all_profile_product = [];

        first_process();


        function first_process() {
            var trade_checked = false;
            var items_send = [];

            for (i = 0; i < $scope.dota_clear_items.length; i++) {
                items_send.push($scope.dota_clear_items[i].id);
            }

            var checkBalance = function () {
                if ($scope.dota_balance < 0) {
                    $scope.dota_loading_count_item = false;
                    notifyError('You do not have enough SCOR!', false, true);
                    return false;
                } else
                    return true;
            };

            var check_trade = function () {
                $http.get('/trade/check')
                    .success(function (data, status, headers, config) {
                        if (data.error == 'trade_url_required') {
                            open_modal();
                            notifyError('Error. Please Enter your trade URL!', false, true);
                            $scope.dota_loading_count_item = false;
                            trade_checked = false;
                        }
                        else if (data.check == 'success_checked') {
                            $scope.dota_loading_count_item = false;
                            trade_checked = true;
                        }
                    })
                    .error(function (data, status, headers, config) {
                    })
                    .then(function () {
                        if (trade_checked) {
                            save_items();
                        }
                    });
            };

            var areItemExist = function () {
                if (!$scope.dota_clear_items.length && $scope.dota_count_profile == 0) {
                    $scope.dota_loading_count_item = false;
                    notifyError('You must select at least 1 product!', false, true);
                    return false;
                } else
                    return true;
            };

            var save_items = function () {

                var save_item = function () {
                    var item = items_send[0];
                    if (!items_send.length) {
                        $scope.dota_loading_count_item = false;
                        saved();
                    }
                    else {
                        items_send.splice(0, 1);
                        $http.get('/save-dota-drop/' + item)
                            .success(function (data, status, headers, config) {
                                if (data.error == 'money_required_market') {
                                    notifyError('You do not have enough SCOR', false, true);
                                    return;
                                }
                                if (data.error == 'you_not_have_g2a_transaction') {
                                    notifyError('You haven\'t pay transaction least 2$. Please fill your balance!', false, true);
                                    return;
                                }
                                if (data.error == 'trade_url_required') {
                                    open_modal();
                                    return;
                                }
                                save_item();
                            })
                            .error(function (data, status, headers, config) {
                                notifyError(data.message, false, true);
                            })
                            .then(function (res) {
                            })
                    }
                };
                save_item();
            };

            var saved = function () {
                $scope.dota_loading_selected_item = true;

                open_withdraw_dota();

                $http.get('/get/dota/skins/profile')
                    .success(function (data, status, headers, config) {
                        for (n = 0; n < data.length; n++) {
                            get_all_profile_product.push({
                                id: data[n].id,
                                status: data[n].status,
                                offer_id: data[n].offer_id
                            });

                            if (data[n].status == 'pending') {
                                $scope.dota_result_item.push(data[n]);
                            }
                        }
                    })
                    .error(function (data, status, headers, config) {
                    })
                    .then(function (res) {
                        $scope.dota_selected_all_items = get_all_profile_product;
                        $scope.dota_count_profile = $scope.dota_selected_all_items.length;
                        $scope.dota_loading_selected_item = false;

                        $scope.dota_send_csgo_drops();
                    })
            };

            if (checkBalance()) {
                if (areItemExist()) {
                    check_trade();
                }
            }
        }

    };

    function open_withdraw_dota() {
        $('.popup').fadeOut();

        $('#popup-withdraw').fadeIn().css({'width': Number('531')}).prepend('<a href="#" class="close">close</a>');

        var popMargTop = ($('#popup-withdraw').height()) / 2;
        var popMargLeft = ($('#popup-withdraw').width()) / 2;

        $('#popup-withdraw').css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        $('body').append('<div id="bg-darker"></div>');
        $('#bg-darker, #popup-withdraw').fadeIn();

        $('#bg-darker, a.close').click(function () {
            $('#bg-darker, #popup-withdraw').fadeOut(function () {
                clearAllDota();
                $('#bg-darker, a.close').remove();
            });
            return false;
        });
    }

    $scope.dota_update_trade_url = function () {
        open_modal();
    };

    function open_modal() {
        trade_url_bool = true;

        $('.popup').fadeOut();

        $('#popup-trade-dota').fadeIn().css({'width': Number('531')}).prepend('<a href="#" class="close">close</a>');

        var popMargTop = ($('#popup-trade-dota').height()) / 2;
        var popMargLeft = ($('#popup-trade-dota').width()) / 2;

        $('#popup-trade-dota').css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        $('body').append('<div id="bg-darker"></div>');
        $('#bg-darker, #popup-trade-dota').fadeIn();

        $('#bg-darker, a.close').click(function () {
            $('#bg-darker, #popup-trade-dota').fadeOut(function () {
                $('#bg-darker, a.close').remove();
            });
            return false;
        });

        return false;
    }

    $scope.dota_text_sort = '';
    var t = 0;
    var h = 0;

    $scope.dota_save_trade_url = function () {
        $.ajax({
            method: 'post',
            dataType: 'json',
            url: BASE_URL + '/profile/settings',
            data: {
                trade_url: $scope.dota_trade_url
            }
        }).success(function (ret) {
            if (ret.success) {
                $('#bg-darker, #popup-trade-dota').fadeOut(function () {
                    $('#bg-darker, a.close').remove();
                });
                $scope.dota_open_modal()
            } else {
            }
        })
    };


    $scope.dota_send_csgo_drops = function () {

        var accept_dota_drop = function () {
            var drop = $scope.dota_result_item[0];

            if (!$scope.dota_result_item.length) {

            }
            else {
                $scope.dota_result_item.splice(0, 1);

                $.ajax({
                    url: BASE_URL + '/accept-dota-drop/' + drop.id,
                    method: 'get',
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {

                    }, success: function (response) {

                        if (response.success) {
                            accept_dota_drop();
                        }

                        if (response.error == 'trade_url_required') {
                            open_modal();
                        }
                    }, error: function (xhr) {

                    }
                });
            }
        };

        accept_dota_drop();

    };

    var socket_dota = io(window.location.hostname + ':' + LIVE_DROP_PORT);

    socket_dota.on("angular-bot-" + token + ":offered", function (message) {

        for (var i = 0; i < $scope.dota_selected_all_items.length; i++) {
            if ($scope.dota_selected_all_items[i].id == message.dropId) {
                $('#dota_status_withdraw_' + message.dropId).text("Success").addClass('withdraw-success').removeClass('withdraw-send');

                $("#dota_trade_withdraw_" + message.dropId).attr("href", "https://steamcommunity.com/tradeoffer/" + message.offerId).attr("target", "_blank").removeAttr("style");
            }
        }
    });

});