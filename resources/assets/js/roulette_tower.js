/**
 * Constants
 * @constructor
 */

var all_summ_text = '.all-summ-text';

var btn_clear = ".btn-clear";

var btn_plus_point_zero_one = ".btn-plus-point-zero-one";

var btn_plus_point_ten = '.btn-plus-point-ten';

var btn_plus_one = '.btn-plus-one';

var btn_plus_ten = '.btn-plus-ten';

var btn_plus_hundred = '.btn-plus-hundred';

var btn_plus_devided_one = '.btn-plus-devided-one';

var btn_plus_multy_two = '.btn-plus-multy-two';

var btn_plus_max = '.btn-plus-max';

var roullete_input = '.tower-bet';

var btn_bet_two_x = '.play-2x';

var btn_bet_three_x = '.play-3x';

var btn_bet_five_x = '.play-5x';

var btn_bet_fifty_x = '.play-50x';

var initial = 0;
var count = initial;
var counter;

function timer() {
    if (count <= 0) {
        clearInterval(counter);
        return;
    }
    count--;
    displayCount(count);
}

function displayCount(count) {
    var res = count / 100;
    document.getElementById("timer").innerHTML = res.toPrecision(count.toString().length) + "";
}

$('#start').on('click', function () {
    counter = setInterval(timer, 10);
});

$('#stop').on('click', function () {
    clearInterval(counter);
});

$('#reset').on('click', function () {
    clearInterval(counter);
    count = initial;
    displayCount(count);
});

displayCount(initial);

function getColor(item, maxitem) {

    if (options[item] == '2x') {
        return '#f5cba1';
    }
    if (options[item] == '3x') {
        return '#f38f5d';
    }
    if (options[item] == '5x') {
        return '#67dba0';
    }
    if (options[item] == '50x') {
        return '#d25e9b';
    }
}


function spin(data) {
    $('.roulette-dynamic').css('-webkit-transform', 'rotate(' + data + 'deg)');
    $('.roulette-dynamic').css('-webkit-transition', '-webkit-transform 6s cubic-bezier(0, 0.42, 0.58, 1)');

    var game_name = ['2x', '3x', '5x', '50x'];

    for (var i = 0; i < game_name.length; i++) {
        $('#collapse-stats-' + game_name[i] + '').addClass('collapse-opacity-down');
    }

    if ($('.ladder-right-usr').length > 0) {
        for (var l = 0; l < $('.ladder-right-usr').length; l++) {
            $('.ladder-right-usr').each(function (index) {
                $(this).addClass('collapse-opacity-down');
            })
        }
    }
}


//drawRouletteWheel();

function RouletteTower() {

    this.balance = 0;

    this.clearRouletteInput(0);
    //this.setJackpot();
    this.setHistory();

    this.checkAuthUser(function (checked) {
        if (checked) {
            this.getBalance();
        }

        this.loadUsers(function (data) {
            if (data.length) {
                for (var i = 0; i < data.length; i++) {
                    this.addUsersDiv(data[i]);
                }
            }
        }.bind(this))
    }.bind(this));


}

/*RouletteTower.prototype.setJackpot = function () {
 this.requestPOST('/tower-roulette-get-jackpot', 'GET', 'Getting Jackpot', {}, function (response) {
 if (response.success) {
 this.setJackpotValue(response.data);
 }
 else {
 }
 }.bind(this));

 }*/

RouletteTower.prototype.setHistory = function () {
    this.requestPOST('/tower-roulette-get-history', 'GET', 'Getting history', {}, function (response) {
        if (response) {
            for (var i = 0; i < response.length; i++) {
                $('.previous-rolls-items').append('<span style="margin: 3px; padding: 5px 5px; width: 30px; border-radius: 4px;" class="badge badge-' + response[i].win + '">' + response[i].win + '</span>');
            }
        }
    }.bind(this));
}

RouletteTower.prototype.rouletteUpdateHistory = function (winner) {
    var count = $(".previous-rolls-items span").length;
    if (count >= 10) {
        $('.previous-rolls-items span').first().remove();
    }

    $('.previous-rolls-items').append('<span style="margin: 3px; padding: 5px 5px; width: 30px; border-radius: 4px;" class="badge badge-' + winner + '">' + winner + '</span>');

}

/*RouletteTower.prototype.setJackpotValue = function (value) {
 var old_value = $(all_summ_text).text();
 if (old_value == " " || old_value == "" || old_value == undefined) {
 old_value = 0.00
 }
 var new_value = parseFloat(value).toFixed(2);
 $(all_summ_text).text(new_value);

 $(all_summ_text).prop('Counter', parseFloat(old_value).toFixed(2)).animate({
 Counter: $(all_summ_text).text()
 }, {
 duration: 400,
 easing: 'linear',
 step: function ($now) {
 $(all_summ_text).text(parseFloat($now).toFixed(2));
 }
 });

 }*/

RouletteTower.prototype.loadUsers = function (callback) {
    this.requestPOST('/tower-roulette-load-users', 'GET', 'Loading Users', {}, function (response) {
        if (response.success) {
            callback(response.data);
        }
        else {
            callback(false)
        }
    }.bind(this));
};

RouletteTower.prototype.preClosedAll = function () {
    $('.tower-roulette-timer').hide();

    $('.roulette-dynamic').css('opacity', 1);
    $('.roulette-static').css('opacity', 1);

    $(btn_bet_two_x).prop("disabled", true);
    $(btn_bet_two_x).css({
        'background': '#565655',
        'box-shadow': 'none'
    });

    $(btn_bet_three_x).prop("disabled", true);
    $(btn_bet_three_x).css({
        'background': '#565655',
        'box-shadow': 'none'
    });

    $(btn_bet_five_x).prop("disabled", true);
    $(btn_bet_five_x).css({
        'background': '#565655',
        'box-shadow': 'none'
    });
    $(btn_bet_fifty_x).prop("disabled", true);
    $(btn_bet_fifty_x).css({
        'background': '#565655',
        'box-shadow': 'none'
    });
};

RouletteTower.prototype.activeForm = function (winner) {
    /**
     * Display Winner
     */
    switch (winner) {
        case '2x':
            $('#winner').css('color', '#fdfe01').text(winner).show();
            break;
        case '3x':
            $('#winner').css('color', '#f59807').text(winner).show();
            break;
        case '5x':
            $('#winner').css('color', '#32b02c').text(winner).show();
            break;
        case '50x':
            $('#winner').css('color', '#f20806').text(winner).show();
            break;
    }

    var game_name = ['2x', '3x', '5x', '50x'];

    for (var i = 0; i < game_name.length; i++) {
        if (winner == game_name[i]) {
            $('#collapse-stats-' + game_name[i] + '').removeClass('collapse-opacity-down');

            if ($('#collapse-' + winner + '').find('.ladder-right-usr').length > 0) {
                for (var l = 0; l < $('#collapse-' + winner + '').find('.ladder-right-usr').length; l++) {
                    $('#collapse-' + winner + '').find('.ladder-right-usr').each(function (index) {
                        $(this).removeClass('collapse-opacity-down')
                    });
                }
            }
        }
    }

};


RouletteTower.prototype.setRouletteInput = function (value) {

    var old_value = $(roullete_input).val();
    var old_value_parse = parseFloat(old_value);

    var new_value = value;
    var new_value_parse = parseFloat(new_value);

    var sum_value = old_value_parse + new_value_parse;

    $(roullete_input).val(parseFloat(sum_value).toFixed(2));
};

RouletteTower.prototype.clearRouletteInput = function (value) {

    $(roullete_input).val(parseFloat(value).toFixed(2));

};

RouletteTower.prototype.checkAuthUser = function (callback) {
    this.requestPOST('/tower-roulette-check-auth', 'GET', 'checking auth', {}, function (response) {
        if (response.success) {
            callback(true)
        }
        else {
            callback(false)
        }
    }.bind(this));
};

RouletteTower.prototype.getBalance = function () {

    this.requestPOST('/tower-roulette-get-balance', 'GET', 'getting balance', {}, function (response) {
        if (response.success) {
            this.balance = parseFloat(response.balance);
            $('.user-balance').text(parseFloat(this.balance).toFixed(2));

        }
    }.bind(this));

};

RouletteTower.prototype.setRouletteInputMax = function () {
    $(roullete_input).val(parseFloat(this.balance).toFixed(2));
};

RouletteTower.prototype.setRouletteInputMulty = function () {
    var old_value = $(roullete_input).val();
    var old_value_parse = parseFloat(old_value);

    if (!old_value_parse == 0) {

        var sum_value = old_value_parse * 2;

        $(roullete_input).val(parseFloat(sum_value).toFixed(2));
    }
}

RouletteTower.prototype.setRouletteInputDivided = function () {
    var old_value = $(roullete_input).val();
    var old_value_parse = parseFloat(old_value);

    if (!old_value_parse == 0) {

        var sum_value = old_value_parse / 2;

        $(roullete_input).val(parseFloat(sum_value).toFixed(2));
    }
};

RouletteTower.prototype.betTwoX = function () {
    var bet = $(roullete_input).val();

    if (bet == 0 || bet == undefined || bet == '') {
        notifyError('Please enter a bet amount.');
        return;
    }

    this.requestPOST('/tower-roulette-bet', 'POST', 'bet', {
        'bet': bet,
        'type': '2x'
    }, function (response) {
        if (!response.success) {
            notifyError(response.message, true);
        }
    });

};
RouletteTower.prototype.betThreeX = function () {
    var bet = $(roullete_input).val();

    if (bet == 0 || bet == undefined || bet == '') {
        notifyError('Please enter a bet amount.');
        return;
    }

    this.requestPOST('/tower-roulette-bet', 'POST', 'bet', {
        'bet': bet,
        'type': '3x'
    }, function (response) {
        if (!response.success) {
            notifyError(response.message, true);
        }
    });

};
RouletteTower.prototype.betfiveX = function () {
    var bet = $(roullete_input).val();

    if (bet == 0 || bet == undefined || bet == '') {
        notifyError('Please enter a bet amount.');
        return;
    }

    this.requestPOST('/tower-roulette-bet', 'POST', 'bet', {
        'bet': bet,
        'type': '5x'
    }, function (response) {
        if (!response.success) {
            notifyError(response.message, true);
        }
    });

};

RouletteTower.prototype.betFiftyX = function () {
    var bet = $(roullete_input).val();

    if (bet == 0 || bet == undefined || bet == '') {
        notifyError('Please enter a bet amount.');
        return;
    }

    this.requestPOST('/tower-roulette-bet', 'POST', 'bet', {
        'bet': bet,
        'type': '50x'
    }, function (response) {
        if (!response.success) {
            notifyError(response.message, true);
        }
    });

};

RouletteTower.prototype.addUsersDivCommon = function (user, type_game) {

    $('#collapse-' + type_game + '').append(this.userDiv({
        user_id: user.user_id,
        avatar: user.avatar,
        username: user.username,
        bet: user.bet,
    }));

    $('#collapse-stats-' + type_game + '').addClass('collapse-stats-' + type_game + '');

    setTimeout(function () {
        $('#collapse-stats-' + type_game + '').removeClass('collapse-stats-' + type_game + '');
    }, 1500);


    /**
     * Set count of user
     */

    var count_of_user_div = '.total-count-user-' + type_game + '';
    var count_user = parseInt($(count_of_user_div).text());
    count_user++;
    $(count_of_user_div).text(count_user);

    /**
     * Set total bet
     */

    var total_of_bet_div = '.total-bet-' + type_game + '';

    var total_of_bet = $(total_of_bet_div).text();
    var result_total_of_bet = parseFloat(total_of_bet) + parseFloat(user.bet);
    $(total_of_bet_div).text(parseFloat(result_total_of_bet).toFixed(2));

}

RouletteTower.prototype.addUsersDiv = function (user) {
    //this.setJackpot();
    this.addUsersDivCommon(user, user.type);

};
RouletteTower.prototype.userDiv = function (item) {
    return '<div class="ladder-right-usr">' +
        '<a href="/profile/"' + item.user_id + '>' +
        '<span class="ladder-right-usr-ava">' +
        '<img src="' + item.avatar + '">' +
        '</span>' +
        '<span class="ladder-right-usr-login">' + item.username + '</span></a>' +
        '<span class="ladder-right-usr-profit win">' + parseFloat(item.bet).toFixed(2) + '</span></div>';
};

RouletteTower.prototype.clearAllView = function () {

    $('.roulette-dynamic').css('opacity', 0.1);
    $('.roulette-static').css('opacity', 0.1);
    $('.roulette-dynamic').css('-webkit-transform', 'rotate(' + 0 + 'deg)');
    $('.roulette-dynamic').css('-webkit-transition', '-webkit-transform 0.2s cubic-bezier(0, 0.42, 0.58, 1)');

    $('#winner').text("").hide();
    $('.tower-roulette-timer').show();

    var game_name = ['2x', '3x', '5x', '50x'];

    for (var i = 0; i < game_name.length; i++) {

        var total_bet_div = '.total-bet-' + game_name[i] + '';

        $("#collapse-" + game_name[i] + "").empty();
        $(".total-count-user-" + game_name[i] + "").text('0');
        $(total_bet_div).text('0.00');
        $(total_bet_div).css('color', 'silver');
    }

    for (var j = 0; j < game_name.length; j++) {
        $('#collapse-stats-' + game_name[j] + '').removeClass('collapse-opacity-down');
    }

    if ($('.ladder-right-usr').length > 0) {
        for (var l = 0; l < $('.ladder-right-usr').length; l++) {
            $('.ladder-right-usr').each(function (index) {
                $(this).removeClass('collapse-opacity-down');
            })
        }
    }

    $(btn_bet_two_x).prop("disabled", false);
    $(btn_bet_two_x).css({
        'background': '#fdfe00',
        'box-shadow': 'none'
    });

    $(btn_bet_three_x).prop("disabled", false);
    $(btn_bet_three_x).css({
        'background': '#f59809',
        'box-shadow': 'none'
    });

    $(btn_bet_five_x).prop("disabled", false);
    $(btn_bet_five_x).css({
        'background': '#32b02c',
        'box-shadow': 'none'
    });
    $(btn_bet_fifty_x).prop("disabled", false);
    $(btn_bet_fifty_x).css({
        'background': '#f20107',
        'box-shadow': 'none'
    });
}

RouletteTower.prototype.matchData = function (winner) {

    var getValueByGameName = function (i) {
        var multi_value = 0;
        switch (i) {
            case 0:
                multi_value = 2;
                break;
            case 1:
                multi_value = 3;
                break;
            case 2:
                multi_value = 5;
                break;
            case 3:
                multi_value = 50;
                break;
        }
        return multi_value
    }

    var game_name = ['2x', '3x', '5x', '50x'];


    for (var i = 0; i < game_name.length; i++) {

        if ($('#collapse-' + game_name[i] + '').find('.ladder-right-usr').length == 0) {
            continue;
        }

        var total_bet_div = '.total-bet-' + game_name[i] + '';

        $(total_bet_div).text("0.00");

        $('#collapse-' + game_name[i] + '').find('.ladder-right-usr').each(function (index) {
            if (game_name[i] == winner) {
                var profit = $(this).find('.ladder-right-usr-profit').text();

                var result_profit = parseFloat(profit) * getValueByGameName(i);

                $(this).find('.ladder-right-usr-profit').text(parseFloat(result_profit).toFixed(2));
                $(this).find('.ladder-right-usr-profit').css('color', '#50da2f');

                var result_total_bet = $(total_bet_div).text();
                var sum_total_bet = parseFloat(result_total_bet) + parseFloat(result_profit);

                $(total_bet_div).text(parseFloat(sum_total_bet).toFixed(2));

                $(total_bet_div).css('color', '#50da2f');

            } else {
                $(this).find('.ladder-right-usr-profit').text("0.00");
                $(this).find('.ladder-right-usr-profit').css('color', '#dc110f');
                $(total_bet_div).css('color', '#dc110f');
            }
        });
    }

}

RouletteTower.prototype.updateBalanceView = function () {

    this.checkAuthUser(function (checked) {
        if (checked) {
            this.getBalance();
        }
    }.bind(this));
}

RouletteTower.prototype.requestPOST = function (url, method, loader_info, data, callback_success) {
    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: method,
        dataType: 'json',
        cache: false,
        data: data,
        timeout: 30000,
        beforeSend: function () {

        }.bind(this),

        success: function (response) {
            callback_success(response);
        }.bind(this),

        error: function (xhr) {
            $.each(xhr.responseJSON, function (key, val) {
                notifyError(val, true);
            });
        }.bind(this)
    });
};


$(function () {

    var rouletteTower = new RouletteTower();

    $('#winner').text(" ").hide();

    $(document).on('click', btn_clear, function (e) {
        e.preventDefault();

        rouletteTower.clearRouletteInput(0);
    });

    $(document).on('click', btn_plus_point_zero_one, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInput(0.01);
    });

    $(document).on('click', btn_plus_point_ten, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInput(0.10);
    });

    $(document).on('click', btn_plus_one, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInput(1);
    });

    $(document).on('click', btn_plus_ten, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInput(10);
    });

    $(document).on('click', btn_plus_hundred, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInput(100);
    });

    $(document).on('click', btn_plus_multy_two, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInputMulty();
    });

    $(document).on('click', btn_plus_devided_one, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInputDivided();
    });

    $(document).on('click', btn_plus_max, function (e) {
        e.preventDefault();

        rouletteTower.setRouletteInputMax();
    });

    $(document).on('click', btn_bet_two_x, function (e) {
        e.preventDefault();
        rouletteTower.betTwoX();
    });

    $(document).on('click', btn_bet_three_x, function (e) {
        e.preventDefault();
        rouletteTower.betThreeX();
    });

    $(document).on('click', btn_bet_five_x, function (e) {
        e.preventDefault();
        rouletteTower.betfiveX();
    });

    $(document).on('click', btn_bet_fifty_x, function (e) {
        e.preventDefault();
        rouletteTower.betFiftyX();
    });

    socket.on("roulette-tower-timer", function (message) {
        if (message.timer) {
            clearInterval(counter);
            count = parseInt(message.timer);
            counter = setInterval(timer, 10);
        }
    });

    socket.on("roulette-tower-game-pre-closed", function (message) {
        if (message.data) {
            rouletteTower.preClosedAll();
        }
    });

    socket.on("roulette-tower-start-roulette", function (message) {
        if (message.data) {
            spin(message.data);
        }
    });

    socket.on("roulette-tower-game-active", function (message) {
        if (message.data) {

            rouletteTower.activeForm(message.winner);
            rouletteTower.matchData(message.winner);
            rouletteTower.updateBalanceView();
            rouletteTower.rouletteUpdateHistory(message.winner);
        }
    });

    socket.on("roulette-tower-bet-add", function (data) {
        if (data) {
            rouletteTower.addUsersDiv(data);
        }
    });

    socket.on("roulette-tower-clear-all", function (data) {
        rouletteTower.clearAllView();
    });

});



