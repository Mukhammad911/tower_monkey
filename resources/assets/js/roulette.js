/*** EDITABLE SETTINGS ***/
var roulette_time           = 5000;
var motion_curve            = 'easeOutCubic';

var win_content             = '.case-win-page';
var win_wrapper             = '.win-items-wrapper';
var roulette_content        = '.case-page';
var btns_block              = '.credits-rulette-open-btn';
var btn_open                = '.btn-open-case';
var btn_try_again           = '.try-again';
var roulettes_number        = '.spins_count';
var roulettes_wrapper       = '.spins-wrapper';
var roulette_wrapper        = '.case-page-credits-rulette';
var roulette                = '.win-line';
var drop_roulette           = '.drop-win-line';
var movable_wrapper         = 'span';
var movable_block           = '.movable_block';
var items                   = 'img';
var items_drop              = 'a';
var source_items_wrapper    = '.case-page-contains-items';
var source_items_csgo       = '.case-contain-item-csgo';
var source_items_pubg       = '.case-contain-item-pubg';
var provably_fair_wrapper   = '.provably-fair-wrapper';
var hash_selector           = '.pf-hash';

var SOUND_ROULETTE_SPINNING= '/assets/snd/spinning.mp3';
var AUDIO_ROULETTE_SPINNING = new Audio(SOUND_ROULETTE_SPINNING);

var SOUND_ROULETTE_WIN= '/assets/snd/win.mp3';
var AUDIO_ROULETTE_WIN = new Audio(SOUND_ROULETTE_WIN);

function rouletteItemDesign(item){
    return '<img  src="' + item.image + '" alt="">';
}

function rouletteItemDesignCsgo(item){
    return '<a class="csgo-drop-roulette-item csgo-rarity-' + item.class + '"><img src="' + item.image + '" alt="">' +
                '<div class="csgo-drop-roulette-item-name">' + item.name +
                    '<br>' +
                    '<div class="csgo-drop-roulette-item-short-description">' + item.short_description + '</div>' +
                '</div>' +
            '</a>';
}

function rouletteItemDesignPubg(item){
    return '<a class="pubg-drop-roulette-item pubg-rarity-' + item.class + '"><img src="' + item.image + '" alt=""><p class="pubg-drop-roulette-item-name">' + item.name + '</p></a>';
}

function winItemDesign(item){
    return '<div class="case-win-monet">' +
        '<img src="' + item.image + '" alt="">' +
        '</div>';
}

function parseMarketName(market_name) {
    var split1 = market_name.split('(');

    return {
        market_name_without_quality:	split1[0].trim(),
        quality:           				split1 && (split1.length > 1) ? split1[1].replace(')', '').trim() : ''
    }
}

function winItemDesignCsgo(item){
    var market_name = parseMarketName(item.market_name);

    return '<div class="case-win-item">' +
                '<div class="win-item-image no-win-class csgo-rarity-' + item.class + '">' +
                    '<div class="win-item-img">' +
                        '<img src="https://steamcommunity-a.akamaihd.net/economy/image/'  + item.image_hash + '/110fx110f" alt="">' +
                    '</div>'+
                '<div class="win-image-bg"></div>' +
            '</div>'+
                '<div class="win-item-details">' +
                    '<div class="win-item-name-block">' +
                        '<span class="win-item-csgo-name">'+ market_name.market_name_without_quality + '</span>'+
                        '<span class="win-item-csgo-quality">(' + market_name.quality + ')</span>' +
                    '</div>'+
                    '<div class="win-item-actions">' +
                        '<a href="javascript:;" class="button_of_product_csgo set_sold_item_'+item.id+'" data-case_product_id="'+item.id+'">' +
                            '<span><i class="icon-basket" aria-hidden="true"></i> SELL FOR <span class="coins-sell-one-btn"></span> '+ item.price +'</span>' +
                        '</a>'+
                    '</div>'+
                '</div>'+
            '</div>';
}

function winItemDesignPubg(item){
    return '<div class="case-win-item">' +
                '<div class="win-item-image no-win-class pubg-rarity-' + item.class + '">' +
                    '<div class="win-item-img">' +
                        '<img src="https://steamcommunity-a.akamaihd.net/economy/image/'  + item.image_hash + '/110fx110f" alt="">' +
                    '</div>'+
                    '<div class="win-image-bg"></div>' +
                '</div>'+
                '<div class="win-item-details">' +
                    '<div class="win-item-name-block">' +
                        '<span class="win-item-name">'+ item.market_name + '</span>'+
                    '</div>'+
                    '<div class="win-item-actions">' +
                        '<a href="javascript:;" class="button_of_product_pubg set_sold_item_'+item.id+'" data-case_product_id="'+item.id+'">' +
                            '<span><i class="icon-basket" aria-hidden="true"></i> SELL FOR <span class="coins-sell-one-btn"></span> '+ item.price +'</span>' +
                        '</a>'+
                    '</div>'+
                '</div>'+
            '</div>';
}

function rouletteWrapper(){
    return '<div class="case-page-credits-rulette">' +
        '<span class="blip"></span>' +
        '<div class="win-line">' +
        '<span class="movable_block"><!-- ITEMS --></span>' +
        '</div>' +
        '</div>';
}

function rouletteWrapperCsgo(){
    return '<div class="case-page-credits-rulette csgo-rulette">' +
        '<div class="win-line drop-win-line">' +
        '<span class="movable_block"><!-- ITEMS --></span>' +
        '</div>' +
        '</div>';
}

function provablyFairDesignNext(data, index){
//    return '<table border="1" style="font-size: 12px; height: 100px; position: relative;left: 27px;">' +
//                '<tr>' +
//                    '<th colspan="2"> Roulette ' + index + '</th>' +
//                '</tr>' +
//                '<tr>' +
//                    '<td>User seed:</td>' +
//                    '<td>'+ data.user_seed + '</td>' +
//                '</tr>' +
//                '<tr>' +
//                    '<td>Server seed:</td>' +
//                    '<td>'+ data.server_seed + '</td>' +
//                '</tr>' +
//            '</table>';

  return  '<div class="ladder-page-verify-slide-next">'+
        '  <input type="text" name="" placeholder="Roulette' + index + '">'+
        '<input type="text" name="" placeholder="'+ data.server_seed +'">'+
            '<input type="text" name="" placeholder="'+ data.user_seed +'">'+
            '</div>';


}

function provablyFairDesignPrev(data){
//    return '<table border="1" style="font-size: 12px; height: 100px; position: relative;left: 27px;">' +
//                '<tr>' +
//                    '<td>User seed:</td>' +
//                    '<td>'+ data.user_seed + '</td>' +
//                '</tr>' +
//                '<tr>' +
//                    '<td>Server seed:</td>' +
//                    '<td>'+ data.server_seed + '</td>' +
//                '</tr>' +
//                '<tr> ' +
//                    '<td>Win number:</td> ' +
//                    '<td>'+ data.win_number + '</td> ' +
//                '</tr> ' +
//                '<tr> ' +
//                    '<td>Hash:</td> ' +
//                    '<td>' + data.hash + '</td> ' +
//                '</tr> ' +
//            '</table>';

   return '<div class="ladder-page-verify-slide-previous">'+
            '<label>Server seed</label><br/>'+
            '<input type="text" name="server_seed" placeholder="'+ data.server_seed + '"><br/>'+
                '<label>User seed</label><br/>'+
                '<input type="text" name="user_seed" placeholder="'+ data.user_seed + '"><br/>'+
                    '<label>Win number</label><br/>'+
                    '<input type="text" name="win_number" placeholder="'+ data.win_number + '"><br/>'+
                        '<label>Hash</label><br/>'+
                        '<input type="text" name="hash" placeholder="' + data.hash + '">'+
                   '</div>';


}
/*** EDITABLE SETTINGS ***/

function prepareItems(){
    $(roulette).each(function(index, r){
        $(r).find(movable_wrapper).empty();
        for(var i = 0; i < 100; i++){
            var source_item_image = $(source_items + ':eq(' + getRandomInt(0, $(source_items).size()) + ') img');
            $(r).find(movable_wrapper).append(rouletteItemDesign({
                image: source_item_image.attr('src')
            }));
        }
    });
}

function prepareItemsDrops(){
    $(drop_roulette).each(function(index, r){
        $(r).find(movable_wrapper).empty();
        for(var i = 0; i < 100; i++){
            var source_item_image = $(source_items_csgo + ':eq(' + getRandomInt(0, $(source_items_csgo).size()) + ') img');
            $(r).find(movable_wrapper).append(rouletteItemDesignCsgo({
                image: source_item_image.attr('src'),
                class: source_item_image.data('rarity-class'),
                name: source_item_image.data('product-name'),
                short_description: source_item_image.data('short-desc')
            }));
        }
    });
}

function prepareItemsPubgDrops(){
    $(drop_roulette).each(function(index, r){
        $(r).find(movable_wrapper).empty();
        for(var i = 0; i < 100; i++){
            var source_item_image = $(source_items_pubg + ':eq(' + getRandomInt(0, $(source_items_pubg).size()) + ') img');
            $(r).find(movable_wrapper).append(rouletteItemDesignPubg({
                image: source_item_image.attr('src'),
                class: source_item_image.data('rarity-class'),
                name: source_item_image.data('product-name')
            }));
        }
    });
}

$(function() {
    var case_id;
    var roulettes_quantity = 1;
    var base_offset = -88;

    if (document.getElementById('type_case') != null)
    {
        type_case = document.getElementById('type_case').value;

        if (type_case == 'csgo-case')
        {
            prepareItemsDrops();

        }
        else if (type_case=='pubg-case')
        {
            prepareItemsPubgDrops();
        }
        else
        {
            prepareItems();
        }
    }

    function request(win_position){
        $.ajax({
            url: '/roulette/play',
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                case_id: case_id,
                attempts: roulettes_quantity
            },
            timeout:30000,
            success: function (response) {
                if (response.success) {
                    if($('.header-sound').hasClass('header-sound-off')){
                        AUDIO_ROULETTE_SPINNING.volume = 0;
                        AUDIO_ROULETTE_WIN.volume = 0;
                        AUDIO_ROULETTE_SPINNING.play();
                        AUDIO_ROULETTE_WIN.play();
                    }else{
                        AUDIO_ROULETTE_SPINNING.volume = 1;
                        AUDIO_ROULETTE_WIN.volume = 1;
                        AUDIO_ROULETTE_SPINNING.play();
                        AUDIO_ROULETTE_WIN.play();
                    }

                    start(win_position);
                    //changeBalance( - response.case_price * roulettes_quantity);
                    $(roulette).each(function(index, r){
                        showHash(r, response.win_products[index+1].win.hash);
                        win(r, win_position, response.win_products[index+1].win);
                    });
                    setTimeout(function(){
                        congratulation();
                        var total_win_price = 0;
                        for(var i = 1; i <= roulettes_quantity; i++){
                            total_win_price += response.win_products[i].win.price;
                        }
                        //changeBalance( + total_win_price);
                    }, roulette_time + 1000);
                } else if (response.message) {
                    notifyError(response.message);
                }
                if (response.error) {
                    switch (response.error) {
                        case 'money_required':
                            $('.btn-payment').click();
                            notifyError(Lang.get('messages.money_required'), true);
                            break;
                        default:
                            notifyError(response.error);
                    }
                }
            },
            error: function (xhr) {
                notifyError(xhr.statusText);
            }
        });
    }

    function request_csgo(win_position){
        $.ajax({
            url: '/roulette-csgo/play',
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                case_id: case_id,
                attempts: roulettes_quantity
            },
            timeout:30000,
            success: function (response) {
                if (response.success) {
                    if(response.count==0){
                        notifyError(Lang.get('messages.unavailable'));
                    }
                    else{
                        if($('.header-sound').hasClass('header-sound-off')){
                            AUDIO_ROULETTE_SPINNING.volume = 0;
                            AUDIO_ROULETTE_WIN.volume = 0;
                            AUDIO_ROULETTE_SPINNING.play();
                            AUDIO_ROULETTE_WIN.play();
                        }else{
                            AUDIO_ROULETTE_SPINNING.volume = 1;
                            AUDIO_ROULETTE_WIN.volume = 1;
                            AUDIO_ROULETTE_SPINNING.play();
                            AUDIO_ROULETTE_WIN.play();
                        }
                        start_drop(win_position);
                        //changeBalance( - response.case_price * roulettes_quantity);
                        $(drop_roulette).each(function(index, r){
                            if(response.win_products[index+1].success){
                                winCsgo(r, win_position, response.win_products[index+1]);

                            }
                            else{
                                if(index+1==1){
                                    notifyError(Lang.get('messages.unavailable'));
                                }
                                else{
                                    $('.csgo-case-page-credits-rulette').last().remove();
                                    notifyError(Lang.get('messages.unavailable'));
                                }
                            }
                        });
                        setTimeout(function(){
                            var total_win_price = 0;

                            for(var i = 1; i <= roulettes_quantity; i++){
                                total_win_price += response.win_products[i].price;
                            }

                            total_win_price = parseFloat(total_win_price.toFixed(2));

                            if (roulettes_quantity > 1)
                            {
                                $('.win-modal-sell-block').css("display", "block");
                            }
                            else
                            {
                                $('.win-modal-sell-block').css("display", "none");
                            }

                            $('.win-modal-my-items').attr('href', '/profile/' + response.user_id);

                            $('.win-modal-sell-all-span').text('');
                            $('.win-modal-sell-all-span').append('<i class="icon-basket" aria-hidden="true"></i> Sell All for <span class="coins-sell-one-btn"></span> ' + total_win_price);

                            congratulation();
                        }, roulette_time + 1000);
                    }

                } else {
                    notifyError(Lang.get('messages.unavailable'));
                }
                if (response.error) {
                    switch (response.error) {
                        case 'money_required':
                            $('.btn-payment').click();
                            notifyError(Lang.get('messages.money_required'), true);
                            break;
                        default:
                            notifyError(response.error);
                    }
                }
            },
            error: function (xhr) {
                notifyError(xhr.statusText);
            }
        });
    }

    function request_pubg(win_position){
        $.ajax({
            url: '/roulette-pubg/play',
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                case_id: case_id,
                attempts: roulettes_quantity
            },
            timeout:30000,
            success: function (response) {
                if (response.success) {
                    if(response.count==0){
                        notifyError(Lang.get('messages.unavailable'));
                    }
                    else{
                        if($('.header-sound').hasClass('header-sound-off')){
                            AUDIO_ROULETTE_SPINNING.volume = 0;
                            AUDIO_ROULETTE_WIN.volume = 0;
                            AUDIO_ROULETTE_SPINNING.play();
                            AUDIO_ROULETTE_WIN.play();
                        }else{
                            AUDIO_ROULETTE_SPINNING.volume = 1;
                            AUDIO_ROULETTE_WIN.volume = 1;
                            AUDIO_ROULETTE_SPINNING.play();
                            AUDIO_ROULETTE_WIN.play();
                        }
                        start_drop(win_position);
                        //changeBalance( - response.case_price * roulettes_quantity);
                        $(drop_roulette).each(function(index, r){
                            if(response.win_products[index+1].success){
                                winPubg(r, win_position, response.win_products[index+1]);
                            }
                            else{
                                if(index+1==1){
                                    notifyError(Lang.get('messages.unavailable'));
                                }
                                else{
                                    $('.csgo-case-page-credits-rulette').last().remove();
                                    notifyError(Lang.get('messages.unavailable'));
                                }
                            }
                        });
                        setTimeout(function(){
                            var total_win_price = 0;

                            for(var i = 1; i <= roulettes_quantity; i++){
                                total_win_price += response.win_products[i].price;
                            }

                            total_win_price = parseFloat(total_win_price.toFixed(2));

                            if (roulettes_quantity > 1)
                            {
                                $('.win-modal-sell-block').css("display", "block");
                            }
                            else
                            {
                                $('.win-modal-sell-block').css("display", "none");
                            }

                            $('.win-modal-my-items').attr('href', '/profile/' + response.user_id);

                            $('.win-modal-sell-all-span').text('');
                            $('.win-modal-sell-all-span').append('<i class="icon-basket" aria-hidden="true"></i> Sell All for <span class="coins-sell-one-btn"></span> ' + total_win_price);

                            congratulation();
                        }, roulette_time + 1000);
                    }

                } else {
                    notifyError(Lang.get('messages.unavailable'));
                }
                if (response.error) {
                    switch (response.error) {
                        case 'money_required':
                            $('.btn-payment').click();
                            notifyError(Lang.get('messages.money_required'), true);
                            break;
                        default:
                            notifyError(response.error);
                    }
                }
            },
            error: function (xhr) {
                notifyError(xhr.statusText);
            }
        });
    }

    function start(win_position){
        // offset formula
        var main_offset = $(roulette + ' ' + items).width() * win_position;
        var roulette_offset = $(roulette).width() / 2;
        var random_correction = getRandomInt(5, ($(items).width() - 5));
        var new_offset = main_offset - roulette_offset + random_correction;

        $(roulette).each(function(index, r){
            $(r).find(movable_wrapper).animate({
                'left': '-' + new_offset
            }, roulette_time, motion_curve);
        });
    }

    function start_drop(win_position){
        // offset formula
        var main_offset = ($(drop_roulette + ' ' + items_drop).width() + 10 + 3) * win_position;
        var roulette_offset = $(drop_roulette).width() / 2;
        var random_correction = getRandomInt(5, ($(drop_roulette + ' ' + items_drop).width() - 5)); //minus padding          //51
        var new_offset = main_offset - roulette_offset + random_correction;

        $(drop_roulette).each(function(index, r){
            $(r).find(movable_wrapper).animate({
                'left': '-' + new_offset
            }, roulette_time, motion_curve);
        });
    }

    function win(r, win_position, item){
        $(r).find(items + ':eq(' + win_position + ')').replaceWith(rouletteItemDesign(item));
        $(win_wrapper).append(winItemDesign(item));
    }

    function winCsgo(r, win_position, item){
        $(r).find(items_drop + ':eq(' + win_position + ')').replaceWith(rouletteItemDesignCsgo(item));
        $(win_wrapper).append(winItemDesignCsgo(item));
    }

    function winPubg(r, win_position, item){
        $(r).find(items_drop + ':eq(' + win_position + ')').replaceWith(rouletteItemDesignPubg(item));
        $(win_wrapper).append(winItemDesignPubg(item));
    }

    function congratulation(){
        $(roulette_content).hide();
        $(win_content).fadeIn();
        $(movable_block).css('left', base_offset);
    }

    function showHash(attempt, hash){
        //$('.hashes_'+(attempt+1)).find(hash_selector).text('HASH: ' + hash);
    }

    function refreshSeeds(case_id, count, type) {
        $.ajax({
            url: '/roulette/get-seeds',
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                case_id: case_id,
                count: count,
                case_type: type

            },
            success: function (res) {
                console.log(res);
                if(res.previous_game){
                    var prev_game_wrapper = $(".roulette-prev-game");
                    prev_game_wrapper.html(provablyFairDesignPrev(res.previous_game));
                }
                if(res.next_game){
                    var next_game_wrapper = $(".roulette-next-game");
                    next_game_wrapper.html('');
                    for(var i = 1; i <=count; i++){
                        next_game_wrapper.append( provablyFairDesignNext(res.next_game[i], i) );
                    }
                }
            },
            error: function (xhr) {
                notifyError(xhr.statusText);
            }
        });
    }

    function getWinNumbers(case_id, type){
        $('.win_ranges').html('');

        $.ajax({
            url: '/roulette/get-win-numbers',
            method: 'POST',
            dataType: 'json',
            cache: false,
            data: {
                case_id: case_id,
                case_type: type
            },
            timeout: 30000,
            success: function (res) {
                if(res.success){
                    $.each(res.data, function(i, item){
                        $('.win_ranges').append(winRangeDesign(res.data[i], type));
                    });
                }
            },
            error: function (xhr) {
                notifyError(xhr.statusText);
            }
        });
    }

    function winRangeDesign(item, type){
        if (type == 'pubg')
        {
            var image_path = (item.image_source != 'url') ? '/images/pubg_images/' + item.image : item.image;
        }
        else
        {
            var image_path = (item.image_source != 'url') ? '/images/products/' + item.image : item.image;
        }

        var res = '' +
            '<tr style="color: #fff; "> ' +
            '<td><img style="background: url(/images/csgotower/case_contains_item_bg.png);" src="' + image_path + '"></td>' +
            '<td><strong>' + item.name + '</strong></td> ' +
             '<td><strong>' + item.win_range_min + ' - ' + item.win_range_max + '</strong></td> ' +
            '</tr>';

        return res;
    }

    function sellProduct(id) {
        $.ajax({
            url: '/csgo-sell-item/'+id,
            method: 'GET',
            dataType: 'json',
            cache: false,
            beforeSend: function () {

            },
            success: (function (response) {
                if (response.success) {
                    notifySuccess('Item successfully sold');
                    $(".set_sold_item_" + id).hide();
                }
                else {
                }
            }).bind(this),
            error: function (xhr) {
                console.log(xhr);
            }
        });
    }

    function sellProductPubg(id) {
        $.ajax({
            url: '/pubg-sell-item/'+id,
            method: 'GET',
            dataType: 'json',
            cache: false,
            beforeSend: function () {

            },
            success: (function (response) {
                if (response.success) {
                    notifySuccess('Item successfully sold');
                    $(".set_sold_item_" + id).hide();
                }
                else {
                }
            }).bind(this),
            error: function (xhr) {
                console.log(xhr);
            }
        });
    }


    /*** ACTIONS ***/
    $(document).on('click', btn_open, function(e){
        e.preventDefault();
        case_id = $(this).data('case_id');

        type_case = document.getElementById('type_case').value;

        if(type_case == 'csgo-case'){
            $('.credits-cases-to-open').slideUp();
            $(btn_open).slideUp();
            roulettes_quantity = $(drop_roulette).length;
            var win_position_csgo = 90;
            request_csgo(win_position_csgo);
        }
        else if(type_case == 'pubg-case'){
            $('.credits-cases-to-open').slideUp();
            $(btn_open).slideUp();
            roulettes_quantity = $(drop_roulette).length;
            var win_position_dota = 90;
            request_pubg(win_position_dota);
        }
        else{
            $(btn_open).slideUp();

            roulettes_quantity = $(roulette).length;
            var win_position = 90;
            request(win_position);
        }
    });

    $(document).on('change', roulettes_number, function(){
        var case_id = $(this).data('case_id');
        var case_price = $(this).data('case-price');

        var new_roulettes_number = $('#spins_count_id').val();

        var total_price_cases = parseFloat(case_price) * parseFloat(new_roulettes_number);

        var str = Lang.get('messages.open_for_coins') + ' <span class="coins-case-btn"></span> ' + total_price_cases.toFixed(2);

        $('.csgotower-success-btn-span').text('').append(str);

        if(new_roulettes_number < 1){
            return;
        }

        $(roulettes_wrapper).find(roulette_wrapper + ':not(:first)').remove();

        var type_case = document.getElementById('type_case').value;

        if(type_case=='csgo-case'){
            for (var i = 1; i < new_roulettes_number; i++) {
                $(roulettes_wrapper).append(rouletteWrapperCsgo());
            }
            prepareItemsDrops();
            refreshSeeds(case_id, new_roulettes_number, type_case);
        }
        else if(type_case=='pubg-case'){
            for (var i = 1; i < new_roulettes_number; i++) {
                $(roulettes_wrapper).append(rouletteWrapperCsgo());
            }
            prepareItemsPubgDrops();
            refreshSeeds(case_id, new_roulettes_number, type_case);
        }
    });

    $(document).on('click', btn_try_again, function(e){
        var type_case = document.getElementById('type_case').value;

        e.preventDefault();
        $(win_content).hide();
        $(roulette_content).fadeIn();
        $(win_wrapper).empty();

        if (type_case == 'csgo-case')
        {
            prepareItemsDrops();
        }
        else if (type_case=='pubg-case')
        {
            prepareItemsPubgDrops();
        }
        else
        {
            prepareItems();
        }

        $(btn_open).show();
        $('.credits-cases-to-open').show();

        refreshSeeds(case_id, roulettes_quantity, type_case);
    });

    $(document).on('click', '.roulette_refresh_seeds, .credits-cases-check-numbers', function(){
        refreshSeeds($(this).data('case_id'), $(roulette).length, $(this).data('case-type'));
        getWinNumbers($(this).data('case_id'), $(this).data('case-type'));
    });

    $(document).on('click', '.button_of_product_csgo', function (e) {
        var id = $(this).data('case_product_id');
        sellProduct(id);
    });

    $(document).on('click', '.button_of_product_pubg', function (e) {
        var id = $(this).data('case_product_id');
        sellProductPubg(id);
    });

    $(document).on('click', '.sell_all_csgo', function (e) {

        e.preventDefault();

        $('.button_of_product_csgo').each(function() {
            $(this).css({"display" : "none"});
            var id = $(this).data('case_product_id');
            sellProduct(id);
        });

    });

    $(document).on('click', '.sell_all_pubg', function (e) {

        e.preventDefault();

        $('.button_of_product_pubg').each(function() {
            $(this).css({"display" : "none"});
            var id = $(this).data('case_product_id');
            sellProductPubg(id);
        });

    });
    /*** ACTIONS ***/
});