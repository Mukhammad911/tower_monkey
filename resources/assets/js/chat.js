var send_message_btn = '.chat-message-btn';

var en = '.en',
    ru = '.ru',
    tr = '.tr',
    es = '.es',
    de = '.de',
    pl = '.pl';

function Chat() {

    this.type = 'en';
    this.message = '';
    this.loadMessage();
    $(en).addClass('selector-lang-li-active');

}
Chat.prototype.loadMessage = function () {
    this.requestPOST('/chat-default', 'GET', 'Getting default message...', {
        type: this.type
    }, function (response) {
        if (response.success) {
            this.addMessage(response.data);
        }
        else {
        }
    }.bind(this));
}

Chat.prototype.sendMessage = function () {
    this.message = $('.chat-message-box').val();

    if(this.message == "" || typeof this.message == undefined){
        notifyError("Please enter the text message");
        return;
    }

    if (this.message.length > 500)
    {
        notifyError('Text length must be not more then 500 symbols.');
        return;
    }

    $('.chat-message-box').val(" ");

    this.requestPOST('/chat-add-message', 'POST', 'Adding message...', {
        type: this.type,
        message: this.message,
    }, function (response) {
        $('.chat-message-box').val(" ");
        if (response.success) {

        }
        else{
            notifyError(response.message);
        }
    }.bind(this));

}

Chat.prototype.addMessage = function (data) {
    $(".chat-body").empty();

    switch (this.type) {
        case 'en':
            $(en).addClass('selector-lang-li-active');
            $(ru).removeClass('selector-lang-li-active');
            $(tr).removeClass('selector-lang-li-active');
            $(es).removeClass('selector-lang-li-active');
            $(de).removeClass('selector-lang-li-active');
            $(pl).removeClass('selector-lang-li-active');
            break;
        case 'ru':
            $(en).removeClass('selector-lang-li-active');
            $(ru).addClass('selector-lang-li-active');
            $(tr).removeClass('selector-lang-li-active');
            $(es).removeClass('selector-lang-li-active');
            $(de).removeClass('selector-lang-li-active');
            $(pl).removeClass('selector-lang-li-active');
            break;
        case 'tr':
            $(en).removeClass('selector-lang-li-active');
            $(ru).removeClass('selector-lang-li-active');
            $(tr).addClass('selector-lang-li-active');
            $(es).removeClass('selector-lang-li-active');
            $(de).removeClass('selector-lang-li-active');
            $(pl).removeClass('selector-lang-li-active');
            break;
        case 'es':
            $(en).removeClass('selector-lang-li-active');
            $(ru).removeClass('selector-lang-li-active');
            $(tr).removeClass('selector-lang-li-active');
            $(es).addClass('selector-lang-li-active');
            $(de).removeClass('selector-lang-li-active');
            $(pl).removeClass('selector-lang-li-active');
            break;
        case 'de':
            $(en).removeClass('selector-lang-li-active');
            $(ru).removeClass('selector-lang-li-active');
            $(tr).removeClass('selector-lang-li-active');
            $(es).removeClass('selector-lang-li-active');
            $(de).addClass('selector-lang-li-active');
            $(pl).removeClass('selector-lang-li-active');
            break;
        case 'pl':
            $(en).removeClass('selector-lang-li-active');
            $(ru).removeClass('selector-lang-li-active');
            $(tr).removeClass('selector-lang-li-active');
            $(es).removeClass('selector-lang-li-active');
            $(de).removeClass('selector-lang-li-active');
            $(pl).addClass('selector-lang-li-active');
            break;
    }

    for (var i = 0; i < data.length; i++) {
        $('.chat-body').append(this.userDiv({
            user_id: data[i].user_id,
            avatar: data[i].avatar,
            username: data[i].username,
            message: data[i].message,
        }));
    }

    $('.chat-body').scrollTop(1000000);
};

Chat.prototype.addMessageOnce = function(data){
    if(this.type != data.type){
        return;
    }

    $('.chat-body').append(this.userDiv({
        user_id: data.user_id,
        avatar: data.avatar,
        username: data.username,
        message: data.message,
    }));
    $('.chat-body').scrollTop(1000000);
}

Chat.prototype.userDiv = function (item) {
    return '<div class="chat-user-container">' +
        '<span class="chat-user-avatar"><img src="' + item.avatar + '"></span>' +
        '<span class="chat-username">' + item.username + '</span>' +
        '<span class="chat-user-message">' + item.message + '</span>' +
        '</div>';
};

Chat.prototype.requestPOST = function (url, method, loader_info, data, callback_success) {
    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: method,
        dataType: 'json',
        cache: false,
        data: data,
        timeout: 30000,
        beforeSend: function () {

        }.bind(this),

        success: function (response) {
            callback_success(response);
        }.bind(this),

        error: function (xhr) {
           /* $.each(xhr.responseJSON, function (key, val) {
                notifyError(val, true);
            });*/
        }.bind(this)
    });
};

$(function () {

    var chat = new Chat();

    $(document).on('click', send_message_btn, function (e) {
        e.preventDefault();

        chat.sendMessage();
    });

    $(document).keypress(function (e) {
        if(e.keyCode == 13) {
            if($('.chat-message-box').is(":focus")) {
                e.preventDefault();
                chat.sendMessage();
            }
        }
    });

    $(document).on('click', en, function (e) {
        e.preventDefault();

        chat.type = 'en';
        chat.loadMessage();
    });

    $(document).on('click', ru, function (e) {
        e.preventDefault();

        chat.type = 'ru';
        chat.loadMessage();
    });

    $(document).on('click', tr, function (e) {
        e.preventDefault();

        chat.type = 'tr';
        chat.loadMessage();
    });

    $(document).on('click', es, function (e) {
        e.preventDefault();

        chat.type = 'es';
        chat.loadMessage();
    });

    $(document).on('click', de, function (e) {
        e.preventDefault();

        chat.type = 'de';
        chat.loadMessage();
    });

    $(document).on('click', pl, function (e) {
        e.preventDefault();

        chat.type = 'pl';
        chat.loadMessage();
    });

    socket.on("chat-message", function (message) {
        var data = $.parseJSON(message.data);
        chat.addMessageOnce(data);
    });
});