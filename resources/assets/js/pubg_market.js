var beastPubg = angular.module('beastPubg', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
beastPubg.service('sharedPropertiespubg', function () {
    var cases = [];
    var items = [];

    return {
        set_case: function (new_case) {
            cases.push(new_case);
        },
        get_case: function () {
            return cases
        },
        set_item: function (new_item) {
            items.push(new_item);
        },
        get_item: function () {
            return items;
        }
    }
});
beastPubg.factory('PagerServicepubg', function () {
    var service = {};
    service.GetPager = GetPager;
    return service;
    function GetPager(totalItems, currentPage, pageSize) {
        currentPage = currentPage || 1;
        pageSize = pageSize || 33;
        var totalPages = Math.ceil(totalItems / pageSize);
        var startPage, endPage;
        if (totalPages <= 15) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        var pages = _.range(startPage, endPage + 1);
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

});
beastPubg.controller('MyCtrl5', function ($scope, sharedPropertiespubg, $http, $rootScope, $q, $filter, PagerServicepubg) {
    var all_arms = [];
    var count_profile_product = [];
    var origin_balance = '';
    var trade_url_bool = false;
    var timeout_clear = false;
    var rub_usd_sell = 0;
    var setting_checked = false;
    $scope.pubg_searchString = '';
    $scope.pubg_startNumber = 0;
    $scope.pubg_endNumber = 3000;
    $scope.pubg_sortAsc = true;
    $scope.pubg_sortDesc = false;
    $scope.pubg_curPage = 0;
    $scope.pubg_pageSize = 33;
    $scope.pubg_itemsPerPage = 5;
    $scope.pubg_weapon_types = all_arms;
    $scope.pubg_arms = '';
    $scope.pubg_propertyName = 'price';
    $scope.pubg_reverse = false;
    $scope.pubg_available_crd = 0;
    $scope.pubg_get_at_summ_price = 0;
    $scope.pubg_clear_items = [];
    $scope.pubg_arms_item = [];
    $scope.pubg_cases_id = [];
    $scope.pubg_case_price = '';
    $scope.pubg_value_of_profit = [];
    $scope.pubg_result_item = [];
    $scope.pubg_balance = 0;
    $scope.pubg_pager = {};

    $scope.init = function () {
        if (getpubgSettings()) {
        }
    };

    function clearAllpubg() {
        if (timeout_clear) {
            all_arms = [];
            count_profile_product = [];
            origin_balance = '';
            trade_url_bool = false;
            timeout_clear = false;
            rub_usd_sell = 0;
            $scope.pubg_searchString = '';
            $scope.pubg_startNumber = 0;
            $scope.pubg_endNumber = 3000;
            $scope.pubg_sortAsc = true;
            $scope.pubg_sortDesc = false;
            $scope.pubg_curPage = 0;
            $scope.pubg_pageSize = 33;
            $scope.pubg_itemsPerPage = 5;
            $scope.pubg_weapon_types = all_arms;
            $scope.pubg_arms = '';
            $scope.pubg_propertyName = 'price';
            $scope.pubg_reverse = false;
            $scope.pubg_available_crd = 0;
            $scope.pubg_get_at_summ_price = 0;
            $scope.pubg_clear_items = [];
            $scope.pubg_arms_item = [];
            $scope.pubg_cases_id = [];
            $scope.pubg_case_price = '';
            $scope.pubg_value_of_profit = [];
            $scope.pubg_result_item = [];
            $scope.pubg_balance = 0;
            $scope.pubg_pager = {};
            $scope.init();
        }
        else {
        }
    }

    function pubg_setPage(page) {
        if (page < 1 || page > $scope.pubg_pager.totalPages) {
            return;
        }
        $scope.pubg_pager = PagerServicepubg.GetPager($scope.pubg_dummyItems.length, page);
        $scope.pubg_arms = $scope.pubg_dummyItems.slice($scope.pubg_pager.startIndex, $scope.pubg_pager.endIndex + 1);
        setTimeout(function () {
            displaySelectedpubgItems()
        }, 100);
    }

    function getpubgBalance() {
        $scope.pubg_loading_balance = true;
        $http.get('/get/csgo/balance')
            .success(function (data, status, headers, config) {
                $scope.pubg_balance = parseFloat(data).toFixed(2);
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                $scope.pubg_loading_balance = false;
                origin_balance = $scope.pubg_balance;
            });
    }

    function getpubgSettings() {
        $('.market').show();
        $http.get('/get/settings')
            .success(function (data, status, headers, config) {
                rub_usd_sell = parseFloat(((parseFloat(data)) * 100).toFixed(2));
                setting_checked = true;
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                if (setting_checked) {
                    getpubgBalance();
                    getpubgProfileProduct();
                    getpubgSkins();
                    $scope.pubg_desc();
                }
            });
    }

    function getpubgSkins() {
        all_arms = [];
        $scope.pubg_loading = true;
        $http.get('/get/pubg/skins')
            .success(function (data, status, headers, config) {
                $scope.pubg_loading = false;
                for (var i = 0; i < data.length; i++) {
                    all_arms.push({
                        img_url: data[i].image_hash,
                        id: data[i].id,
                        name: data[i].name,
                        price: parseFloat((parseInt(data[i].price) / rub_usd_sell).toFixed(2))
                    });
                    sharedPropertiespubg.set_item(data[i]);
                }
            })
            .error(function (data, status, headers, config) {
                $scope.pubg_loading = false;
                $('.market').hide();
            })
            .then(function (res) {
                timeout_clear = true;
                $('.market').hide();
                setAllpubgSkins($scope.pubg_searchString, $scope.pubg_startNumber, $scope.pubg_endNumber, $scope.pubg_sortAsc, $scope.pubg_sortDesc);
            });
    }

    function setAllpubgSkins(searchString, startNumber, endNumber, sortAsc, sortDesc) {
        $scope.pubg_arms = '';
        var result_asc = [];
        var sort_value_asc = [];
        var resultItems = '';
        $scope.pubg_dummyItems = '';
        $scope.pubg_pager = {};
        for (i = 0; i < all_arms.length; i++) {
            if ((all_arms[i].name.indexOf(searchString) > -1) && (all_arms[i].price >= startNumber) && (all_arms[i].price <= endNumber)) {
                    result_asc.push(all_arms[i]);
            }
        }
        if (result_asc.length == 0) {

        }
        else {
            if (sortAsc) {
                sort_value_asc = _.sortBy(result_asc, function (num) {
                    return num.price;
                });
                resultItems = sort_value_asc;
            }
            else {
                sort_value_asc = _.sortBy(result_asc, function (num) {
                    return num.price;
                });
                resultItems = sort_value_asc.reverse();
            }
            $scope.pubg_dummyItems = resultItems;
            $scope.pubg_setPage = pubg_setPage;
            $scope.pubg_setPage(1);
        }
        $('.pagination-div').show();
    };
    $scope.pubg_searchStringClick = function () {
        setAllpubgSkins($scope.pubg_searchString, $scope.pubg_startNumber, $scope.pubg_endNumber, $scope.pubg_sortAsc, $scope.pubg_sortDesc);
    };
    $scope.pubg_select_box = function () {
        if (t == 0) {
            $('#select_box').show();
            t++;
        }
        else if (t == 1) {
            $('#select_box').hide();
            t--;
        }
    };

    $scope.pubg_asc = function () {
        $scope.pubg_sortAsc = true;
        $scope.pubg_sortDesc = false;
        $('#select_box').attr('style', 'display: none');
        $scope.pubg_text_sort = 'Price: Low -> High';
        t = 0;
        setAllpubgSkins($scope.pubg_searchString, $scope.pubg_startNumber, $scope.pubg_endNumber, $scope.pubg_sortAsc, $scope.pubg_sortDesc);
        setTimeout(function () {
            displaySelectedpubgItems()
        }, 100);
    };
    $scope.pubg_desc = function () {
        $('#select_box').attr('style', 'display: none');
        $scope.pubg_sortAsc = false;
        $scope.pubg_sortDesc = true;
        $scope.pubg_text_sort = 'Price: High -> Low';
        t = 0;
        setAllpubgSkins($scope.pubg_searchString, $scope.pubg_startNumber, $scope.pubg_endNumber, $scope.pubg_sortAsc, $scope.pubg_sortDesc);
        setTimeout(function () {
            displaySelectedpubgItems()
        }, 100);
    };
    function displaySelectedpubgItems() {
        for (k = 0; k < $scope.pubg_clear_items.length; k++) {
            $('#pubg_item_price_' + $scope.pubg_clear_items[k].id).addClass('check');
            $('#pubg_item_' + $scope.pubg_clear_items[k].id).addClass('check');
        }
    }

    $scope.$watch('pubg_selected', function () {
        if ($scope.pubg_selected == '1') {
            $scope.pubg_reverse = false;
        }
        else if ($scope.pubg_selected == '2') {
            $scope.pubg_reverse = true;
        }
    });
    $scope.pubg_select_all_view = function () {
        for (o = 0; o < $scope.pubg_arms.length; o++) {
            var int_id = parseInt($scope.pubg_arms[o].id);
            if (_.where($scope.pubg_clear_items, {
                    'id': int_id,
                    'img': $scope.pubg_arms[o].img,
                    'name': $scope.pubg_arms[o].name,
                    'item_value': 0
                }).length) {
            }
            else {
                if ($scope.pubg_clear_items.length >= 8) {
                    notifyError('You can not get more 8 items!', false, true);
                    return
                }
                else {
                    $scope.pubg_dif = 0;
                    $scope.pubg_dif = parseFloat(($scope.pubg_balance - parseFloat($scope.pubg_arms[o].price)).toFixed(2));
                    $('#pubg_item_price_' + $scope.pubg_arms[o].id).addClass('check');
                    $('#pubg_item_' + $scope.pubg_arms[o].id).addClass('check');
                    $scope.pubg_available_crd = parseInt($scope.pubg_available_crd) + 1;
                    $scope.pubg_get_at_summ_price = parseFloat((parseFloat($scope.pubg_get_at_summ_price) + parseFloat($scope.pubg_arms[o].price)).toFixed(2));
                    $scope.pubg_balance = parseFloat((parseFloat($scope.pubg_balance) - parseFloat($scope.pubg_arms[o].price)).toFixed(2));
                    $scope.pubg_clear_items.push({
                        'id': int_id,
                        'img': $scope.pubg_arms[o].image,
                        'name': $scope.pubg_arms[o].name,
                        'item_value': 0
                    });
                }
            }
        }
    };
    function getpubgProfileProduct() {
        $scope.pubg_loading_count_item = true;
        $http.get('/get/pubg/skins/profile')
            .success(function (data, status, headers, config) {
                for (n = 0; n < data.length; n++) {
                    count_profile_product.push(data[n]);
                }
            })
            .error(function (data, status, headers, config) {
            })
            .then(function (res) {
                $scope.pubg_loading_count_item = false;
                $scope.pubg_count_profile = count_profile_product.length;
            });
    }

    $scope.pubg_delete_from_icon = function(item_id, item_image, item_name, price){
        var id_item = parseInt(item_id);
        if (_.where($scope.pubg_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'item_value': 0,
                'price': price,
            }).length) {
            $("#pubg_item_price_" + item_id).removeClass('check');
            $("#pubg_item_" + item_id).removeClass('check');
            $scope.pubg_available_crd = parseInt($scope.pubg_available_crd) - 1;
            $scope.pubg_get_at_summ_price = parseFloat((parseFloat($scope.pubg_get_at_summ_price) - parseFloat(price)).toFixed(2));
            $scope.pubg_balance = parseFloat((parseFloat($scope.pubg_balance) + parseFloat(price)).toFixed(2));
            $scope.pubg_clear_items = _.without($scope.pubg_clear_items, _.findWhere($scope.pubg_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'item_value': 0,
                'price': price,
            }));
        }

    }

    $scope.pubg_getItemId = function (item_id, item_image, item_name, price) {
        var id_item = parseInt(item_id);
        if (_.where($scope.pubg_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'item_value': 0,
                'price': price
            }).length) {
            $("#pubg_item_price_" + item_id).removeClass('check');
            $("#pubg_item_" + item_id).removeClass('check');
            $scope.pubg_available_crd = parseInt($scope.pubg_available_crd) - 1;
            $scope.pubg_get_at_summ_price = parseFloat((parseFloat($scope.pubg_get_at_summ_price) - parseFloat(price)).toFixed(2));
            $scope.pubg_balance = parseFloat((parseFloat($scope.pubg_balance) + parseFloat(price)).toFixed(2));
            $scope.pubg_clear_items = _.without($scope.pubg_clear_items, _.findWhere($scope.pubg_clear_items, {
                'id': id_item,
                'img': item_image,
                'name': item_name,
                'item_value': 0,
                'price': price
            }));
        }
        else {
            if ($scope.pubg_clear_items.length >= 8) {
                notifyError('You can not get more 8 items!', false, true);
                return
            }
            else {
                $scope.pubg_dif = 0;
                $scope.pubg_dif = parseFloat(($scope.pubg_balance - parseFloat(price)).toFixed(2));
                $('#pubg_item_price_' + item_id).addClass('check');
                $('#pubg_item_' + item_id).addClass('check');
                $scope.pubg_available_crd = parseInt($scope.pubg_available_crd) + 1;
                $scope.pubg_get_at_summ_price = parseFloat((parseFloat($scope.pubg_get_at_summ_price) + parseFloat(price)).toFixed(2));
                $scope.pubg_balance = parseFloat((parseFloat($scope.pubg_balance) - parseFloat(price)).toFixed(2));
                $scope.pubg_clear_items.push({
                    'id': id_item,
                    'img': item_image,
                    'name': item_name,
                    'item_value': 0,
                    'price': price
                });
            }
        }
    };
    $scope.pubg_refresh_click = function () {
        clearAllpubg();
    };

    $scope.pubg_open_modal = function () {

        $scope.pubg_loading_count_item = true;
        var get_all_profile_product = [];

        first_process();


        function first_process() {
            var trade_checked = false;
            var items_send = [];

            for (i = 0; i < $scope.pubg_clear_items.length; i++) {
                items_send.push($scope.pubg_clear_items[i].id);
            }

            var checkBalance = function () {
                if ($scope.pubg_balance < 0) {
                    $scope.pubg_loading_count_item = false;
                    notifyError('You do not have enough SCOR!', false, true);
                    return false;
                } else
                    return true;
            };

            var check_trade = function () {
                $http.get('/trade/check')
                    .success(function (data, status, headers, config) {
                        if (data.error == 'trade_url_required') {
                            open_modal();
                            notifyError('Error. Please Enter your trade URL!', false, true);
                            $scope.pubg_loading_count_item = false;
                            trade_checked = false;
                        }
                        else if (data.check == 'success_checked') {
                            $scope.pubg_loading_count_item = false;
                            trade_checked = true;
                        }
                    })
                    .error(function (data, status, headers, config) {
                    })
                    .then(function () {
                        if (trade_checked) {
                            save_items();
                        }
                    });
            };

            var areItemExist = function () {
                if (!$scope.pubg_clear_items.length && $scope.pubg_count_profile == 0) {
                    $scope.pubg_loading_count_item = false;
                    notifyError('You must select at least 1 product!', false, true);
                    return false;
                } else
                    return true;
            };

            var save_items = function () {

                var save_item = function () {
                    var item = items_send[0];
                    if (!items_send.length) {
                        $scope.pubg_loading_count_item = false;
                        saved();
                    }
                    else {
                        items_send.splice(0, 1);
                        $http.get('/save-pubg-drop/' + item)
                            .success(function (data, status, headers, config) {
                                if (data.error == 'money_required_market') {
                                    notifyError('You do not have enough SCOR', false, true);
                                    return;
                                }
                                if (data.error == 'you_not_have_g2a_transaction') {
                                    notifyError('You haven\'t pay transaction least 2$. Please fill your balance!', false, true);
                                    return;
                                }
                                if (data.error == 'trade_url_required') {
                                    open_modal();
                                    return;
                                }
                                save_item();
                            })
                            .error(function (data, status, headers, config) {
                                notifyError(data.message, false, true);
                            })
                            .then(function (res) {
                            })
                    }
                };
                save_item();
            };

            var saved = function () {
                $scope.pubg_loading_selected_item = true;

                open_withdraw_pubg();

                $http.get('/get/pubg/skins/profile')
                    .success(function (data, status, headers, config) {
                        for (n = 0; n < data.length; n++) {
                            get_all_profile_product.push({
                                id: data[n].id,
                                status: data[n].status,
                                offer_id: data[n].offer_id
                            });

                            if (data[n].status == 'pending') {
                                $scope.pubg_result_item.push(data[n]);
                            }
                        }
                    })
                    .error(function (data, status, headers, config) {
                    })
                    .then(function (res) {
                        $scope.pubg_selected_all_items = get_all_profile_product;
                        $scope.pubg_count_profile = $scope.pubg_selected_all_items.length;
                        $scope.pubg_loading_selected_item = false;

                        $scope.pubg_send_csgo_drops();
                    })
            };

            if (checkBalance()) {
                if (areItemExist()) {
                    check_trade();
                }
            }
        }

    };

    function open_withdraw_pubg() {
        $('.popup').fadeOut();

        $('#popup-withdraw').fadeIn().css({'width': Number('531')}).prepend('<a href="#" class="close">close</a>');

        var popMargTop = ($('#popup-withdraw').height()) / 2;
        var popMargLeft = ($('#popup-withdraw').width()) / 2;

        $('#popup-withdraw').css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        $('body').append('<div id="bg-darker"></div>');
        $('#bg-darker, #popup-withdraw').fadeIn();

        $('#bg-darker, a.close').click(function () {
            $('#bg-darker, #popup-withdraw').fadeOut(function () {
                clearAllpubg();
                $('#bg-darker, a.close').remove();
            });
            return false;
        });
    }

    $scope.pubg_update_trade_url = function () {
        open_modal();
    };

    function open_modal() {
        trade_url_bool = true;

        $('.popup').fadeOut();

        $('#popup-trade-pubg').fadeIn().css({'width': Number('531')}).prepend('<a href="#" class="close">close</a>');

        var popMargTop = ($('#popup-trade-pubg').height()) / 2;
        var popMargLeft = ($('#popup-trade-pubg').width()) / 2;

        $('#popup-trade-pubg').css({
            'margin-top': -popMargTop,
            'margin-left': -popMargLeft
        });

        $('body').append('<div id="bg-darker"></div>');
        $('#bg-darker, #popup-trade-pubg').fadeIn();

        $('#bg-darker, a.close').click(function () {
            $('#bg-darker, #popup-trade-pubg').fadeOut(function () {
                $('#bg-darker, a.close').remove();
            });
            return false;
        });

        return false;
    }

    $scope.pubg_text_sort = '';
    var t = 0;
    var h = 0;

    $scope.pubg_save_trade_url = function () {
        $.ajax({
            method: 'post',
            dataType: 'json',
            url: BASE_URL + '/profile/settings',
            data: {
                trade_url: $scope.pubg_trade_url
            }
        }).success(function (ret) {
            if (ret.success) {
                $('#bg-darker, #popup-trade-pubg').fadeOut(function () {
                    $('#bg-darker, a.close').remove();
                });
            } else {
            }
        })
    };


    $scope.pubg_send_csgo_drops = function () {

        var accept_pubg_drop = function () {
            var drop = $scope.pubg_result_item[0];

            if (!$scope.pubg_result_item.length) {

            }
            else {
                $scope.pubg_result_item.splice(0, 1);

                $.ajax({
                    url: BASE_URL + '/accept-pubg-drop/' + drop.id,
                    method: 'get',
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {

                    }, success: function (response) {

                        if (response.success) {
                            accept_pubg_drop();
                        }

                        if (response.error == 'trade_url_required') {
                            open_modal();
                        }
                    }, error: function (xhr) {

                    }
                });
            }
        };
        accept_pubg_drop();
    };

    var socket_pubg = io(window.location.hostname + ':' + LIVE_DROP_PORT);

    socket_pubg.on("angular-bot-" + token + ":offered", function (message) {

        for (var i = 0; i < $scope.pubg_selected_all_items.length; i++) {
            if ($scope.pubg_selected_all_items[i].id == message.dropId) {
                $('#pubg_status_withdraw_' + message.dropId).text("Success").addClass('withdraw-success').removeClass('withdraw-send');

                $("#pubg_trade_withdraw_" + message.dropId).attr("href", "https://steamcommunity.com/tradeoffer/" + message.offerId).attr("target", "_blank").removeAttr("style");
            }
        }
    });

});