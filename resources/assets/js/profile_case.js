function Cases() {
}

Cases.prototype.sendItem = function (id, game_type, self) {
    var url = '';

    if (game_type == 'csgo')
    {
        url = '/save-csgo-case-drop';
    }
    else if (game_type == 'pubg')
    {
        url = '/save-pubg-case-drop';
    }
    else
    {
        notifyError('Wrong Game type!');
        return;
    }

    $.ajax({
        url: url + '/' + id,
        method: 'get',
        dataType: 'json',
        cache: false,
        beforeSend: function () {

        }, success: (function (response) {

            if (response.success) {
                var info = self.parent().parent();
                info.find('div').remove();

                info.append(
                    '<span class="status">' +
                    '<span class="ico-yellow"><i class="fa fa fa-clock-o"></i></span> ' +
                    'Requested' +
                    '</span>' +
                    '<span class="price">' +
                    '<span class="case-win-item-price-coins"></span> ' +
                    $(info).data('price') +
                    '</span>');
            }
            else
            {
                if (response.error == 'trade_url_required') {
                    notifyError('Please Enter your Trade-URL for getting offers!');
                    return;
                }

                notifyError('Error occured when sending item contact with us csgotowerhelp@gmail.com!');
                return;
            }
        }).bind(this),
        error: function (xhr) {
            console.log(xhr.responseJSON);

            if (xhr.responseJSON.stat = 'withdraw_lock')
            {
                notifyError(xhr.responseJSON.message);
                return;
            }
        }
    });
};

Cases.prototype.sellItem = function (id, game_type, self) {
    var url = '';

    if (game_type == 'csgo')
    {
        url = '/csgo-sell-item';
    }
    else if (game_type == 'pubg')
    {
        url = '/pubg-sell-item';
    }
    else
    {
        notifyError('Wrong Game type!');
        return;
    }

    $.ajax({
        url: url + '/' + id,
        method: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function () {

        },
        success: (function (response) {
            if (response.success) {
                notifySuccess('Item successfully sold');

                var info = self.parent().parent();
                info.find('div').remove();

                info.append(
                    '<span class="status">' +
                        '<span class="ico-green"><i class="fa fa-usd"></i></span> ' +
                        'Sold' +
                        '</span>' +
                        '<span class="price">' +
                        '<span class="case-win-item-price-coins"></span> ' +
                        $(info).data('price') +
                    '</span>');
            }
            else {
            }
        }).bind(this),
        error: function (xhr) {
            console.log(xhr);
        }
    });
};

Cases.prototype.sellAllItems = function (game_type, self) {
    var url = '';

    if (game_type == 'csgo')
    {
        url = '/csgo-sell-all-items';
    }
    else if (game_type == 'pubg')
    {
        url = '/pubg-sell-all-items';
    }
    else
    {
        notifyError('Wrong Game type!');
        return;
    }

    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'json',
        cache: false,
        beforeSend: function () {

        },
        success: (function (response) {
            if (response.success) {
                self.addClass('disabled');
                notifySuccess('All items successfully sold');

                for (var i = 0; i < response.result_ids.length; i++)
                {
                    var info = '.case-winned-item[data-id="'+ response.result_ids[i] +'"] .info';

                    $(info + ' div').remove();
                    $(info + ' span').remove();

                    $(info).append(
                        '<span class="status">' +
                            '<span class="ico-green"><i class="fa fa-usd"></i></span> ' +
                            'Sold' +
                            '</span>' +
                            '<span class="price">' +
                            '<span class="case-win-item-price-coins"></span> ' +
                            $(info).data('price') +
                        '</span>');
                }
            }
            else {
            }
        }).bind(this),
        error: function (xhr) {
            console.log(xhr);
        }
    });
};

$(function () {

    var objProfile = new Cases();

    $(document).on('click', '.send-item a', function (e) {
        var id = $(this).data('id');
        var game_type = $(this).data('drop-type');

        objProfile.sendItem(id, game_type, $(this));
    });

    $(document).on('click', '.sold-item a', function (e) {
        var id = $(this).data('id');
        var game_type = $(this).data('drop-type');

        objProfile.sellItem(id, game_type, $(this));
    });

    $(document).on('click', '.sell-all-csgo', function (e) {
        objProfile.sellAllItems('csgo', $(this));
    });

    $(document).on('click', '.sell-all-pubg', function (e) {
        objProfile.sellAllItems('pubg', $(this));
    });

    socket.on("angular-bot-" + token + ":offered", function (message) {
        var info = '.case-winned-item[data-id="'+ message.dropId +'"] .info';

        $(info + ' div').remove();
        $(info + ' span').remove();

        $(info).append(
            '<span class="status">' +
                '<span class="ico-green"><i class="fa fa-paper-plane-o"></i></span> ' +
                'Offered' +
                '</span>' +
                '<span class="price">' +
                '<span class="case-win-item-price-coins"></span> ' +
                $(info).data('price') +
            '</span>'
        );

        $('.case-winned-item[data-id="'+ message.dropId +'"] .get').css("display", "block");
        $('.case-winned-item[data-id="'+ message.dropId +'"] .get a').attr("href", "https://steamcommunity.com/tradeoffer/" + message.offerId);
    });
});