function notifySuccess(message) {
    $.jGrowl(message, {
        header: '<img src="/images/csgotower/icon-success.png"><span>' + Lang.get('messages.success') + '!</span>',
        theme: 'bg-success',
        life: 7000,
        position: 'bottom-right'
    });
}

var sound_audio = true;
var skinpay_btn = false;

function change_sound() {
        if (!sound_audio) {
            $('.header-sound').removeClass("header-sound-off");
            sound_audio = true;

        } else {
            $('.header-sound').addClass("header-sound-off");
            sound_audio = false;
        }
}

function notifyError(message, exception) {
    $.jGrowl(message, {
        header: '<img src="/images/csgotower/icon-error.png"><span>' + Lang.get('messages.error') + '<span>',
        theme: 'bg-danger',
        life: 7000,
        position: 'bottom-right'
    });

    if (exception !== true) {
        //location.reload();
    }
}

/*function changeBalance(amount) {
    amount = parseFloat(amount);
    var balance_obj = $('.user-balance');
    var current_balance = parseFloat(balance_obj.text());
    var new_balance = (current_balance + amount).toFixed(2);

    //liveCounter(balance_obj, current_balance, new_balance, true); // animated
    balance_obj.text(new_balance); // quick
}*/

function uLoginCallback(token) {
    $.post('/login', {
        token: token
    }, function (res) {
        loadFullPage(false, res);
        $('#bg-darker').click();
    });
}

/**
 * @param obj
 * @param prev_number
 * @param next_number
 * @param float
 */
function liveCounter(obj, prev_number, next_number, float){
    var exec_time = 500;
    if(float == true){
        prev_number = prev_number * 100;
        next_number = next_number * 100;
    }
    var steps = Math.abs(next_number - prev_number);
    var exec_time_step = parseInt(exec_time / steps);

    for(var i = 1; i <= steps; i++){
        var live_counter_number = (prev_number < next_number) ? (prev_number + i) : (prev_number - i);
        var timeout = i * exec_time_step;

        (function(number){
            setTimeout(function(){
                if(float == true){
                    obj.text(number / 100);
                }else{
                    obj.text(number);
                }
            }, timeout);
        }(live_counter_number))
    }
}

/**
 * @param min
 * @param max
 * @returns {*}
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

$(function () {
    /**
     * List of selectors for links, which should be loaded without page reloading
     */
    $(document).ready(function() {
    });
    var live_links = [
        '.link_top',
        '.link_trades',
        '.link_faq',
        '.link_guarantee',
        '.link_contacts',
        '.link_agreement',
        '.link_tos',
        '.link_restrictions',
        '.link_affiliates',
        '.link_profile',
        '.link_case',
        '.link_provably_fair',
        '.link_upgrade',
        '.link_tower',
        '.link_csgo_case',
        '.link_csgo_case_index',
        '.link_csgo-case',
        '.link_coin_flip',
    ];
    var page = 1;
    var page_market = 1;
    var loading_flag = false;
    var case_content = $('#case_content');
    var type = case_content.data('type');

    $("form.payment-form").validate();

    //$('input[name="amount"]').numberMask({type:'float',afterPoint:2,defaultValueInput:'',decimalMark:'.'});

    //$('[data-toggle="tooltip"], .plus').tooltip();

    if (typeof g2a_success != 'undefined') {
        g2a_success.success = g2a_success;
        notifySuccess(g2a_success);
    }
    if (typeof g2a_fail != 'undefined') {
        g2a_fail.success = g2a_fail;
        notifyError(g2a_fail);
    }

    /** FUNCTIONS **/
    function preloader() {
        return '';
    }

    function specialLiveFunctionality(live_link) {
        $('select, input[name=pay_system]').styler({
            onFormStyled: function() {
                $('.jq-selectbox__trigger').append('<i class="fa fa-angle-down" style="font-size: 19px;"></i>');
            }
        });

        switch (live_link) {
            case '.link_index':
                break;

            case '.link_case':

                type_case = document.getElementById('type_case').value;

                if (type_case == 'csgo-case')
                {
                    prepareItemsDrops();

                }
                else if (type_case == 'pubg-case')
                {
                    prepareItemsPubgDrops();
                }
                else
                {
                    prepareItems();
                }

                break;

            case '.link_upgrade':
                break;

            case '.link_tower':
                if(typeof tower !== 'undefined') tower.load();
                break;

            default:
                break;
        }
    }

    function liveLinksString() {
        var res = '';
        live_links.forEach(function (link_class) {
            res += link_class + ', ';
        });
        return res.substr(0, res.length - 2);
    }

    window.loadFullPage = function (obj, lang) {
        if (typeof(lang) === 'undefined') {
            var lang = obj.data('lang').toLowerCase();
        }
        if (lang.length !== 2) {
            console.error('lang length should be 2. ');
            lang = 'en';
        }
        Lang.setLocale(lang);

        $('.wrapper-content').html(preloader());
        $('.modal').hide();

        $.post(BASE_URL + '/language/' + lang, function (data) {
            $.get(location.href, {
                lang: 1
            }, function (data) {
                $(".wrapper-page").html(data);
                prepareItems();
                if(typeof tower !== 'undefined') tower.load();
                $('select, input[name=pay_system]').styler({
                    onFormStyled: function() {
                        $('.jq-selectbox__trigger').append('<i class="fa fa-angle-down" style="font-size: 19px;"></i>');
                    }
                });
            });
        });
    };

    function loadPage(e, url, live_link) {
        if (e) {
            e.preventDefault();
            if (loading_flag === true) {
                return;
            }
            loading_flag = true;
            history.pushState({'live_link': live_link}, null, url);
        }

        // set pre loader
        $('.wrapper-content').html(preloader());
        // hide all modal windows
        $('.modal').hide();

        $.get(url, function (data) {
            $('.wrapper-content').html(data);
            window.scrollTo(0, 0);
            setActiveMenu();
            specialLiveFunctionality(live_link);
            loading_flag = false;
        });
    }

    function setActiveMenu() {
        $('.th-menu a').removeClass('th-menu-important');
        if (typeof ACTIVE_MENU !== 'undefined') {
            switch (ACTIVE_MENU) {
                case 'partner':
                    $('.link_affiliates').addClass('th-menu-important');
                    break;

                case 'top':
                    $('.link_top').addClass('th-menu-important');
                    break;

                case 'faq':
                    $('.link_faq').addClass('th-menu-important');
                    break;

                default:
                    break;
            }

            ACTIVE_MENU = (function () {
                return;
            })();
        }
    }

    function tooltipPosition(obj) {
        var position = obj.offset();
        var left_shift = 35;
        var left_border = position.left - left_shift;
        var right_border = position.left + obj.width();

        if (left_border < 0) {
            obj.offset({
                top: position.top,
                left: (left_border * -1) + left_shift
            });
        } else if (right_border > $(window).width()) {
            obj.offset({
                top: position.top,
                left: right_border - (right_border - $(window).width()) - obj.width()
            });
        }
    }

    /**
     * @param type
     * @returns {*}
     */
    function switch_type(type) {
        if (type == 'case') {
            type = 'card';
        } else {
            type = 'case';
        }

        return type;
    }

    function windowSize() {
       /* if ($(window).width() <= 767) {
        } else {
            $('.header_link .link-menu').show();
        }

        if ($(window).width() <= 450) {
            $('.link-menu').append($('.menu'));
        } else {
            $('.header-right').prepend($('.menu'));
        }*/
    }

    /** FUNCTIONS **/


    /** ACTIONS **/
    $(window).on('load resize', windowSize);

    // live-links
    $(document).on('click', liveLinksString(), function (e) {
        var live_link = '';
        var obj = $(this);
        live_links.forEach(function (link_class) {
            if (obj.hasClass(link_class.substr(1, link_class.length))) {
                live_link = link_class;
            }
        });
        loadPage(e, obj.attr('href'), live_link);
    });
    // history moves
    window.addEventListener("popstate", function (e) {
        loadPage(false, location.pathname, e.state != null ? e.state.live_link : false);
    }, false);

    $(document).on('click', '.btn-login', function () {
        window.open('/steamlogin', '_blank', 'width=800,height=600,scrollbars=1,left=' + (window.screen.width / 2 - 250));
    });

    $(document).on('click', '.switch_toggle', function (e) {
        e.preventDefault();

        var live_drop = $('#live_drop');
        var content = $('#wrapper');
        var footer = $('#footer');
        var header = $('.header-nav');

        var hide_class = 'hide_live';
        var full_wide = 'full';

        if (live_drop.hasClass(hide_class))
        {
            live_drop.removeClass(hide_class);
            content.removeClass(full_wide);
            footer.removeClass(full_wide);
            header.removeClass(full_wide);
        }
        else
        {
            live_drop.addClass(hide_class);
            content.addClass(full_wide);
            footer.addClass(full_wide);
            header.addClass(full_wide);
        }
    });

    $(document).on('submit', '#form_sign_in', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/sign-in',
            method: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (res) {
                loadFullPage(false, res.locale);
                notifySuccess(res.message);
                $('#bg-darker').click();
            },
            error: function (res) {
                switch (res.status) {
                    case 422:
                        notifyError(res.responseText, true);
                        break;
                    default:
                        notifyError('status: ' + res.status);
                        break;
                }
            }
        });
    });

    $(document).on('submit', '#form_login', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/standard-login',
            method: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (res) {
                $('#bg-darker').click();
                loadFullPage(false, res.locale);
            },
            error: function (res) {
                switch (res.status) {
                    case 422:
                        notifyError(res.responseText, true);
                        break;
                    default:
                        notifyError('status: ' + res.status);
                        break;
                }
            }
        });
    });

    $(document).on('submit', '#form_remind_password', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/remind-password',
            method: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (res) {
                $('#bg-darker').click();
                notifySuccess(res.message);
            },
            error: function (res) {
                switch (res.status) {
                    case 422:
                        notifyError(res.responseText, true);
                        break;
                    default:
                        notifyError('status: ' + res.status);
                        break;
                }
            }
        });
    });

    $(document).on('click', '.logout', function (e) {
        e.preventDefault();

        $.get('/logout', function (data) {
            $('#bg-darker').click();
            loadFullPage(false, data);
        });
    });

    $(document).on('mouseenter', '.item-history', function (e) {
        var obj = $(this).find('.history-weapon-hover');
        tooltipPosition(obj);
    });

    $(document).on('click', '.save_trade_url', function (e) {
        var trade_url = $('.trade_url_new').val();

        $.ajax({
            url: '/profile/trade-url/update',
            async: false,
            method: 'post',
            dataType: 'json',
            data: {
                trade_url: trade_url
            },
            success: function (data) {
                notifySuccess(data.message);
                $('.payment-form').data('trade_url', trade_url);
                $('#bg-darker').click();
            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        notifyError(data.responseJSON.message, true);
                        break;
                    default:
                        notifyError('status: ' + data.status);
                        break;
                }
            }
        });
    });

    $(document).on('submit', '.payment-form', function (e) {
        e.preventDefault();

        if($(this).data('trade_url') === '' && skinpay_btn){
            $('.update-tradeurl').click();
            notifyError('Before using SkinPay please add your trade URL', true);
            return false;
        }

        $.ajax({
            url: '/pay',
            async: false,
            method: 'post',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                open_window(data.message, 1024, 768);
            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        notifyError(data.responseJSON.message, true);
                        break;
                    default:
                        notifyError('status: ' + data.status);
                        break;
                }
            }
        });
    });

    function open_window(url, width, height) {
        window.open(url, "_blank");
    }

    $('#skinpay-btn-pay').on('click', function(e){
        e.preventDefault();

        var form = $('.payment-form');

        skinpay_btn = true;

        $('.payment-form input[name=payment_system]').val('skinpay');

        form.submit();
    });

    $(document).on('click', '.objects .item-object .inside', function () {
        location = BASE_URL + '/case/' + $(this).attr('data-case-id');
    });

    $(document).on('click', '#next50caseDrops', function () {
        var n50 = document.getElementById('n50CaseDrops');

        var preload50 = document.getElementById("preload50");

        n50.style.display = "none";
        preload50.style.display = "block";

        $.ajax({
            url: BASE_URL + '/next50case/' + page + '/' + $(this).data('user-id'),
            method: 'get',
            dataType: 'html'
        }).success(function (response) {
            n50.style.display = "block";
            preload50.style.display = "none";

            if (response) {
                ++page;
                $('.case-games-drops-profile').append(response);
            } else {
                $('#next50caseDrops').remove();
            }
        });
    });

    $(document).on('click', '#next50marketDrops', function () {
        var n50 = document.getElementById('n50MarketDrops');

        var preload50 = document.getElementById("preload50market");

        n50.style.display = "none";
        preload50.style.display = "block";

        $.ajax({
            url: BASE_URL + '/next50market/' + page_market + '/' + $(this).data('user-id'),
            method: 'get',
            dataType: 'html'
        }).success(function (response) {
            n50.style.display = "block";
            preload50.style.display = "none";

            if (response) {
                ++page;
                $('.market-drops-profile').append(response);
            } else {
                $('#next50marketDrops').remove();
            }
        });
    });

    $(document).on('click', '.pull-menu', function () {
        $(".header_link .link-menu").slideToggle("normal");
    });

    $(document).on("mouseenter", ".live-drops-items li a, .item-object .inside", function (e) {
        var image = $(this).attr('data-background');

        if (image) {
            $(this).css('background-image', 'url(' + image + ')');
        }
    });

    $(document).on("mouseleave", ".live-drops-items li a, .item-object .inside", function (e) {
        $(this).css('background-image', '');
    });

    $(document).on('click', '.btn-drop-sell', function (e) {
        var $this = $(this);
        $.ajax({
            method: 'POST',
            dataType: 'json',
            url: BASE_URL + '/sell',
            data: {
                drop_id: $this.data('drop-id')
            },
            beforeSend: function () {
                $this.prop('disabled', true);
                $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'selling');
            }, complete: function () {
                $this.prop('disabled', false);
            }
        })
            .success(function (ret) {
                if (ret.success) {
                    $this.fadeOut();
                    //changeBalance(ret.sell_price);
                    $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'sold');
                }
            })
    });

    $(document).on('click', '.btn-save-profile-url', function (e) {
        e.preventDefault();

        if ($('.profile-trade-url').valid()) {
            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: BASE_URL + '/profile/settings',
                data: {
                    trade_url: $('input[name="trade_url"]').val()
                }
            }).success(function (ret) {
                if (ret.success) {
                    $('input[name="trade_url"]').addClass('success');
                } else {
                    $('input[name="trade_url"]').removeClass('success');
                    $('input[name="trade_url"]').addClass('error');
                }
            });
        } else {
            $('input[name="trade_url"]').removeClass('success');
        }
    });

    $(document).on('click', '.btn-save-profile-url-trade', function (e) {
        e.preventDefault();

        if ($('.profile-trade-url').valid()) {
            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: '/profile/settings',
                data: {
                    trade_url: $('input[name="trade_url_from_profile"]').val()
                }
            }).success(function (ret) {
                if (ret.success) {
                } else {
                    console.log($('input[name="trade_url_from_profile"]').val());
                }
            });
        } else {
        }
    });

    $(document).on('click', '.btn-resend-item', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            url: BASE_URL + '/accept-drop/' + $this.data('drop-id'),
            method: 'post',
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'requested');

            }, success: function (response) {
                if (response.error == 'trade_url_required') {
                    $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'pending');
                    modal_trade();
                } else if (response.error) {
                    $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'pending');
                    notifyError(response.error, true);
                }
            }, error: function (xhr) {
                console.log(xhr);
                notifyError(xhr.statusText);
                $('.profile-drop-item[data-drop-id=' + $this.data('drop-id') + ']').attr('data-drop-status', 'pending');
            }
        });
    });

    $(document).on('click', '#case_switcher', function () {
        type = switch_type(type);

        case_content.attr('data-type', type);

        $('#case_switcher').addClass('openlike-' + type);
        $('#case_switcher').removeClass('openlike-' + switch_type(type));

        $('#case-' + switch_type(type)).hide();
        $('#case-' + type).show();
    });

    $(document).on('click', '.select-lang a', function (e) {
        e.preventDefault();
        loadFullPage($(this));
    });

    $(document).on('click', '.btn-payment', function () {
        $("#myModalPay").fadeIn().show();
    });

    $(document).on('click', '.close-pay', function () {
        $("#myModalPay").hide();
    });

    $(document).on('click', '#myModalPay', function (e) {
        var div = $(".modal-content-pay");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $("#myModalPay").hide();
        }
    });

    $(document).on('click', '#myModalPayProfile', function (e) {
        var div = $(".modal-content-pay");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $("#myModalPay").hide();
        }
    });

    $(document).on('submit', '#form_promo_code', function (e) {
        e.preventDefault();
        //$(this).find('input[type=submit]').attr('disabled', 'disabled');

        $.ajax({
            url: '/profile/update/promo-code',
            method: 'post',
            dataType: 'json',
            cache: false,
            data: $(this).serialize(),
            success: function (data) {
                notifySuccess(data.message);
            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        notifyError(data.responseJSON.promo_code, true);
                        break;
                    default:
                        notifyError('status: ' + data.status);
                        break;
                }
            }
        });

    });

    $(document).on('click', '#copy_promo_url', function (e) {
        e.preventDefault();

        prompt('Press Ctrl + C, then Enter to copy to clipboard', BASE_URL + '/p/' + $('#promo_code').val());
    });

    $(document).on('submit', '#promocode_form', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/profile/use-promo-code/' + $(this).find('input[name=promocode]').val(),
            method: 'POST',
            dataType: 'json',
            cache: false,
            success: function (data) {
                //changeBalance(data.amount);
                notifySuccess(data.message);
            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        notifyError(data.responseJSON.message, true);
                        break;
                    default:
                        notifyError('status: ' + data.status);
                        break;
                }
            }
        });
    });

    $(document).on('submit', '#calculate_hash', function (e) {
        e.preventDefault();
        $.post('/calculate-hash', {
            form_data: $(this).serialize()
        }, function (res) {
            $('#calculated_hash').text(res);
        });
    });

    $(document).on('keyup', '.payment-form input[name=amount]', function(e){
        e.preventDefault();

        var obj_addition = $('.promo_addition'),
            obj_add_sum = $('.promo_add_sum'),
            obj_add_percent = $('.prom_add_percent'),
            obj_add_total = $('.promo_add_total'),
            add_percent = parseFloat(obj_addition.data('affiliate-percent')),
            amount = $(this).val(),
            add_sum,
            total;

        if(!add_percent){
            return false;
        }

        add_sum = (amount/100 * add_percent).toFixed(2);
        total = parseFloat(amount) + parseFloat(add_sum);

        obj_addition.show();
        obj_add_percent.text(add_percent);
        obj_add_sum.text(isNaN(add_sum) ? 0 : add_sum);
        obj_add_total.text(isNaN(total) ? 0 : total);
    });
    /** ACTIONS **/
});


/******** CLIENT  **********/

$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=_token]').attr('content')
    },
    success: function (ret) {
        if (ret.notify) {
            if (ret.success) {
                notifySuccess(ret.message);
            }
            else {
                if (!ret.message) {
                    ret.message = Lang.get('messages.contact') + ' csgotower@gmail.com !'
                }

                notifyError(ret.message);
            }
        }
    },
    error: function () {
        var message = Lang.get('messages.contact') + ' csgotowerhelp@gmail.com !';
        notifyError(message);
    }
});

var socket = io(window.location.hostname + ':' + LIVE_DROP_PORT);

socket.on("online", function (message) {
    $('.users-online').text(message.online);
    $('#online-stat').text(message.online);
    $('.live_drop_online span').text(message.online);
});

socket.on("csgotower:users", function () {
    var count_users = parseInt($('.users-count').text());
    var count_users_stat = parseInt($('#user-stat').text());

    $('.users-count').text(++count_users);
    $('#user-stat').text(++count_users_stat);
});

socket.on("refresh-balance:refresh-balance", function (message) {
    var data = $.parseJSON(message.data);
    if(parseInt(data.user_id) == user_id){
        setTimeout(function(){
            $('.user-balance').text(data.balance);
        }, data.timeout);
    }
});

socket.on("csgotower:csgo-case-roulette", function (message) {
    var data = $.parseJSON(message.data);
    var drop;

    var count_case_stat = parseInt($('#all-case-stats').text());

    drop =
        '<div class="item category_csgo csgo-rarity-' + data.class + ((data.stattrak == 1) ? 'stattrak' : '') + '" style="display: none;" data-csgo-case-drop-id="' + data.id + '">' +
            '<a class="picture link_profile" href="/profile/' + data.user_id + '">' +
                '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' + data.image_hash + '/180fx180f" class="drop-image" alt="Drop Image">' +
                '<img src="/' + data.case_image + '" class="case-image" alt="Case Image">' +
                '<div class="title">' + data.nameParsed + '<br> ' + data.shortDescriptionParsed + '</div>' +
                '<div class="hover">' +
                    '<div class="user">' +
                        '<div class="userpic" style="background-image: url(' + data.avatar + ');"></div>' +
                        '<div class="text">' +
                            '<span>' + data.username + '</span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</a>' +
        '</div>';

    $("#live_drop .drop_cont").prepend(drop);

    var prepend_el = $('#live_drop .drop_cont div[data-csgo-case-drop-id=' + data.id + ']');

    if (prepend_el.length) {
        setTimeout(function () {
            prepend_el.fadeIn(1000);
            $itemsList = $("#live_drop .drop_cont div");

            $('#all-case-stats').text(++count_case_stat);

            if ($itemsList.length > 15) {
                $itemsList.last().remove();
            }
        }.bind(this), 6000);
    }

});

socket.on("csgotower:pubg-case-roulette", function (message) {
    var data = $.parseJSON(message.data);
    var drop;

    var count_case_stat = parseInt($('#all-case-stats').text());

    drop =
        '<div class="item category_pubg pubg-rarity-' + data.product.class + '" style="display: none;" data-pubg-case-drop-id="' + data.id + '">' +
            '<a class="picture link_profile" href="/profile/' + data.user_id + '">' +
                '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' + data.image_hash + '/180fx180f" class="drop-image" alt="Drop Image">' +
                '<img src="/' + data.case_image + '" class="case-image" alt="Case Image">' +
                '<div class="title title-pubg-livedrop">' + data.market_name + '</div>' +
                '<div class="hover">' +
                    '<div class="user">' +
                        '<div class="userpic" style="background-image: url(' + data.avatar + ');"></div>' +
                        '<div class="text">' +
                            '<span>' + data.username + '</span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</a>' +
        '</div>';

    $("#live_drop .drop_cont").prepend(drop);

    var prepend_el = $('#live_drop .drop_cont div[data-pubg-case-drop-id=' + data.id + ']');

    if (prepend_el.length) {
        setTimeout(function () {
            prepend_el.fadeIn(1000);
            $itemsList = $("#live_drop .drop_cont div");

            $('#all-case-stats').text(++count_case_stat);

            if ($itemsList.length > 15) {
                $itemsList.last().remove();
            }
        }.bind(this), 6000);
    }

});

socket.on("coin-flip-join:coin-flip-join", function (message) {
    var coin_flip_stat = parseInt($('#flip-coin-stats').text());

    $('#flip-coin-stats').text(++coin_flip_stat);

    var data = $.parseJSON(message.data);
    var drop;

    drop =
        '<div class="item category_coin_flip coin-flip-live" style="display: none;" data-coin-flip-drop-id="' + data.game_id + '">' +
        '<a class="picture link_profile" href="/profile/' + data.user_id_live + '">' +
        '<img src="/images/csgotower/coin_flip_live.png" class="drop-image" alt="Drop Image">' +
        '<div class="title title-coin-flip-livedrop">' + data.bet + '</div>' +
        '<div class="hover">' +
        '<div class="user">' +
        '<div class="userpic" style="background-image: url(' + data.avatar_live + ');"></div>' +
        '<div class="text">' +
        '<span>' + data.user_name_live + '</span>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</a>' +
        '</div>';

    $("#live_drop .drop_cont").prepend(drop);

    var prepend_el = $('#live_drop .drop_cont div[data-coin-flip-drop-id=' + data.game_id + ']');

    if (prepend_el.length) {
        setTimeout(function () {
            prepend_el.fadeIn(1000);
            $itemsList = $("#live_drop .drop_cont div");

            if ($itemsList.length > 15) {
                $itemsList.last().remove();
            }
        }.bind(this), 7000);
    }
});

socket.on("bot-" + token + ":offered", function (message) {
    $('.profile-drop-item[data-drop-id=' + message.dropId + '] .btn-accept-offer').attr('href', 'https://steamcommunity.com/tradeoffer/' + message.offerId);
    $('.profile-drop-item[data-drop-id=' + message.dropId + ']').attr('data-offer-id', message.offerId);
    $('.profile-drop-item[data-drop-id=' + message.dropId + ']').attr('data-drop-status', 'offered');
    notifySuccess(Lang.get('messages.sent') + '<br>URL: <a href="https://steamcommunity.com/tradeoffer/' + message.offerId + '" target="_blank">https://steamcommunity.com/tradeoffer/' + message.offerId + '</a>');
});

socket.on("bot-" + token + ":accepted", function (message) {
    $('.profile-drop-item[data-offer-id=' + message.offerId + ']').attr('data-drop-status', 'accepted');
});

socket.on("bot-" + token + ":declined", function (message) {
    $('.profile-drop-item[data-offer-id=' + message.offerId + ']').attr('data-drop-status', 'pending');
});

socket.on("bot-" + token + ":error", function (message) {
    $('.profile-drop-item[data-drop-id=' + message.dropId + ']').attr('data-drop-status', 'pending');
    notifyError(message.message);
});

socket.on("once-free", function (message) {
    if(message.status == "success" && message.userId == user_id){
        notifySuccess(message.message);
        var balance = $('.user-balance').text();

        balance = parseFloat(balance) + 0.5;
        balance = parseFloat(balance).toFixed(2);

        $('.user-balance').text(balance);
    }
    else if(message.status == "error" && message.userId == user_id){
        notifyError(message.message);
    }

});



$(function(){
    $(document).on('click','.tower-provably',function (e) {
        e.preventDefault();
        $('.provable-tower').show();
        $('.ladder-page-verify-slide').show();
    });
    $(document).on('click','.close_provably_fair',function (e) {
        e.preventDefault();

        $('.provable-tower').hide();
        $('.ladder-page-verify-slide').hide();
    });

    $(document).on('click','.tower-logo',function (e) {
        e.preventDefault();

       window.location.href = 'http://csgotower.net'
    });

    $(document).on('click', '.once-free-confirm', function (e) {
        e.preventDefault();
        var referral = $('.once-free-value').val();

        $.ajax({
            url: '/once_free',
            method: 'post',
            dataType: 'json',
            data: {
                referral : referral
            },
            success: function (res) {
                if(!res.success){
                    notifyError('' + res.message);
                }
                else{
                    $('#once-free').modal('hide');
                }

            },
            error: function (res) {
               console.log(res);
            }
        });

    });
});

