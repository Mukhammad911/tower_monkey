
/* left bar open/close */
$(document).on('click','.left-bar-btn b',function() {
    $('.left-bar').toggleClass('left-bar-close');
    $('#wrapper').toggleClass('wrapper-move');
    $('#footer').toggleClass('footer-move');
});



/* header menu open/close */
$(document).on('touchend click', '.header-menu span', function(e) {
    $('.header-menu').toggleClass('header-menu-open');
    return false;
});
/*$('.header-menu-drop').on('touchend click', function(e) {
    e.stopPropagation();
});*/
$(document).on('touchend click', function(e) {
    $('.header-menu').removeClass('header-menu-open');
});

$(document).on('click', '.profile-page-area-right1-payment-history', function() {
    $('.profile-page-area-right1-payment-history-table').slideToggle();
    $('.profile-page-area-right1-payment-history').toggleClass('payment-history-open');
});

$(document).on('click', '.shop-page-open', function() {
    $('.shop-page-leftbar').toggleClass('shop-page-leftbar-open');
    $('.shop-page-open i').toggleClass('shop-page-open1');
});



/* header responsive */
$(function() {
    doSomethingUseful();
    $(window).resize(doSomethingUseful);

    $('.show_menu').on('click', function () {
        $('.mobile-menu').css('display','block');

        $('#live_drop').css('display','none');

        $('body').addClass('is-modal');
    });

    $('.close-menu').on('click', function () {
        $('.mobile-menu').css('display','none');

        $('#live_drop').css('display','block');

        $('body').removeClass('is-modal');
    });
});
function doSomethingUseful() {
    if(document.body.clientWidth < 1365) {
        $(init);

        function init() {
            $('.header-nav').css('display','none');
            $('.header-sound').css('display','none');
            $('.lang').css('display','none');
            $('.header-1').css('display','none');

            $('.show_menu').css('display','block');

            $('#live_drop').addClass('hide_live');
            $('#wrapper').addClass('full');
            $('#footer').addClass('full');
        }
    }
    else {
        $(init);

        function init() {
            $('.header-nav').css('display','inline-block');
            $('.header-sound').css('display','inline-block');
            $('.lang').css('display','inline-block');
            $('.header-1').css('display','inline-block');

            $('.show_menu').css('display','none');

            $('#live_drop').removeClass('hide_live');
            $('#wrapper').removeClass('full');
            $('#footer').removeClass('full');
        }
    }
}



/* popup windows */
$(document).on('touchend click', ".get-modal, .btn-sign-in, .add-balance,.modal-win-number, .btn-remind-password, .update-tradeurl", function(){
    $('.popup').fadeOut();
    var popID = $(this).attr('data-rel');
    var popURL = $(this).attr('href');

    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];

    if (popID == 'popup-win-number')
    {
        $('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close" style="padding-left: 61px;"> close</a>');
    }
    else
    {
        $('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"> close</a>');
    }

    var popMargTop = ($('#' + popID).height()) / 2;
    var popMargLeft = ($('#' + popID).width()) / 2;

    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    $('body').append('<div id="bg-darker"></div>');
    $('#bg-darker, #' + popID).fadeIn();

    return false;
});

$(document).on('click', '#bg-darker, a.close', function(){
    $('#bg-darker, .popup').fadeOut(function() {
        $('#bg-darker, a.close').remove();
    });
    return false;
});

$("li.paypal a, li.visa a, li.bitcoin a").on('touchend click', function(){
    var popID = $(this).attr('data-rel');
    var popURL = $(this).attr('href');

    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];

    $('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="pay-close">close</a>');

    var popMargTop = ($('#' + popID).height()) / 2;
    var popMargLeft = ($('#' + popID).width()) / 2;

    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    $('.popup').append('<div id="bg-pay"></div>');
    $('#bg-pay, #' + popID).fadeIn();

    $('a.pay-close').click(function(){
        $('#bg-pay, #' + popID).fadeOut(function() {
            $('#bg-pay, a.pay-close, a.close-all').remove();
        });
        return false;
    });

    $('#bg-pay, a.close-all').click(function(){
        $('#bg-pay, #' + popID ).fadeOut(function() {
            $('#bg-pay, a.pay-close').remove();
        });
        return false;
    });

    return false;

});

$("#cardnum").mask("9999  9999  9999  9999", {placeholder: "X"});

$('select, input[name=pay_system]').styler({
    onFormStyled: function() {
        $('.jq-selectbox__trigger').append('<i class="fa fa-angle-down" style="font-size: 19px;"></i>');
    }
});











