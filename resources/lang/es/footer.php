<?php

return [
	'footer_1' => 'csgotower.net - Formato único de apertura de cajas de CS:GO',
	'footer_2' => 'En nuestro sitio puedes obtener los mejores skins de CS:GO. Todos los intercambios ocurren automáticamente usando Steam.',
    'face_commun' => 'Comunidad de Facebook',
	'faq' => 'Preguntas frecuentes',
	'contacts' => 'Contactos',
	'agreement' => 'Acuerdo del usuario',
];