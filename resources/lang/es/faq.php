<?php


return [
    'question_1' => '¿Qué es csgotower?',
    'answer_1' => 'csgotower es un proyecto con varios tipos de juegos en línea tales como, por ejemplo, resolución de cajas que te permite aumentar tu capital en artículos de CS:GO o Dota. ',

    'question_2' => '¿Cómo empiezo a jugar en su sitio web?',
    'answer_2' => 'Para empezar a jugar con nosotros necesitas añadir dinero a tu cuenta usando G2A PAY de la forma que prefieras, a través de PayPal, MasterCard, Visa, PaySafeCard y más. Obtendrás acceso a tu balance luego de autorizar a csgotower a usar tu cuenta de Steam.',

    'question_3' => '¿Qué son los créditos (crd)?',
    'answer_3' => 'Es la moneda universal de csgotower. Con esta moneda puedes comprar artículos en mercados de CS:GO o Dota 2 y luego retirarlos. La tasa de cambio de moneda es 1crd = 1USD. Como puedes ver todo es muy sencillo \:)',

    'question_4' => '¿Cómo retiro los artículos comprados?',
    'answer_4' => 'Puedes hacer esto en la página de CS:GO [http://csgotower.net/csgo] o en [http://csgotower.net/dota2] para Dota 2 tomar los siguientes pasos: 1. Selecciona el objeto que quieres retirar. 2. Presiona ‘Retirar’ y luego presiona ‘Enviar’ en la nueva ventana. 3. Luego de esto, obtendrás tu artículo automáticamente en Steam dentro de los próximos 30 segundos.',

    'question_5' => '¿Por qué debería confiar en ustedes?',
    'answer_5' => 'csgotower es totalmente abierto con sus clientes. Hay un drop abierto en vivo con los ganadores más recientes que se actualiza en tiempo real. Listamos los principales jugadores y por supuesto usamos la herramienta Provably Fair que claramente verifica que no podemos incidir en el curso de tu juego y que todo es altamente transparente. Aparte siempre puedes examinar videos en YouTube y ver lo que otros jugadores piensan de nosotros para definir tu juicio final :)',

    'question_6' => 'Bien. ¿Por qué debería jugar en su sitio web?',
    'answer_6' => 'Las cajas y otros juegos de csgotower son mucho más rentables que los de Valve y otros sitios web. Aquí tienes probabilidades mucho más altas de amortizar tu inversión. ',

    'question_7' => '¿Dónde obtengo la URL de intercambio?',
    'answer_7' => '[http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url] es tu URL de intercambio para intercambios entre nuestros bots y tu cuenta.',

    'question_8' => 'He añadido el dinero a mi cuenta, pero no ha sido transferido. ¿Qué se puede hacer?',
    'answer_8' => 'Usamos G2A Pay para procesar tus pagos. Usualmente demora entre 10 y 15 minutos en efectuar tu pago. A veces puede demorarse hasta una hora. Puedes contactar a nuestro soporte al cliente o al soporte al cliente de G2A Pay para obtener más información sobre tu pago. Todas tus transacciones y sus estados están disponibles en el gabinete personal.',

    'question_9' => 'No puede iniciar sesión en mi cuenta en el sitio web.',
    'answer_9' => 'Usamos Steam Community para la autorización de cuenta. A veces hay problemas técnicos en Steam y no se pueden procesar tus solicitudes. Usualmente se requieren de 5 minutos a un par de horas para que Steam ponga sus servidores de vuelta en funcionamiento.',

    'question_10' => 'No puede obtener mi artículo en Steam.',
    'answer_10' => 'Tu problema podría estar relacionado con que Steam tenga problemas técnicos en ese momento. Intenta retirar tu artículo un poco después de que Steam arregle sus servidores. También es importante para ti no tener restricciones de Steam Guard. Los motivos están detallados aquí — https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.',

    'question_11' => '¿Tienen soporte al cliente?',
    'answer_11' => 'Sí, definitivamente. Sólo contáctanos a [support@csgotower.net|mailto:support@csgotower.net] en cualquier hora de tu conveniencia y te responderemos dentro de un plazo de 24 horas.',
];