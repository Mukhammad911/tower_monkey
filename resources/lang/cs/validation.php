<?php
return [

    /* |-------------------------------------------------------------------------- | Wersy językowe do walidacji |-------------------------------------------------------------------------- | | Następujące wersy językowe zawierają domyślne komunikaty o błędach wykorzystywane przez | instancję walidatora. Niektóre z tych zasad posiadają wiele wersji takich jak | zasady dotyczące rozmiaru. Możesz dowolnie poprawiać te komunikaty tutaj. | */

    'accepted' => ':attribute musi zostać zaakceptowany.',
    'active_url' => ':attribute nie jest poprawnym adresem URL.',
    'after' => ':attribute musi być datą późnieją niż :date.',
    'alpha' => ':attribute może zawierać wyłącznie litery.',
    'alpha_dash' => ':attribute może zawierać wyłącznie litery, cyfry i ukośniki.',
    'alpha_num' => ':attribute może zawierać wyłącznie litery i cyfry.',
    'array' => ':attribute musi być macierzą.',
    'before' => ':attribute musi być datą wcześnieją niż :date.',
    'between' => [
        'numeric' => ':attribute musi mieścić się pomiędzy :min i :max.',
        'file' => ':attribute musi mieć pomiędzy :min i :max kilobajtów.',
        'string' => ':attribute musi zawierać pomiędzy :min i :max znaków.',
        'array' => ':attribute musi zawierać pomiędzy :min i :max pozycji.',
    ],
    'boolean' => 'Pole :attribute musi być albo prawdziwe, albo fałszywe.',
    'confirmed' => ':attribute podany w polu potwierdzenia nie jest identyczny.',
    'date' => ':attribute nie jest prawidłową datą.',
    'date_format' => ':attribute nie odpowiada formatowi :format.',
    'different' => ':attribute i :other muszą się różnić.',
    'digits' => ':attribute musi składać się z cyfr :digits.',
    'digits_between' => ':attribute musi zawierać pomiędzy :min i :max cyfr.',
    'email' => ':attribute musi być prawidłowym adresem e-mail.',
    'exists' => 'Wybrany :attribute jest nieprawidłowy.',
    'filled' => 'Pole :attribute jest wymagane.',
    'image' => ':attribute musi być obrazem.',
    'in' => 'Wybrany :attribute jest nieprawidłowy.',
    'integer' => ':attribute musi być liczbą całkowitą.',
    'ip' => ':attribute musi być prawidłowym adresem IP.',
    'json' => ':attribute musi być prawidłowym ciągiem JSON.',
    'max' => [
        'numeric' => ':attribute nie może być większy niż :max.',
        'file' => ':attribute nie może być większy niż :max kilobajtów.',
        'string' => ':attribute nie może być większy niż :max znaków.',
        'array' => ':attribute nie może zawierać więcej niż :max pozycji.',
    ],
    'mimes' => 'Atrybut musi być plikiem typu :values.',
    'min' => [
        'numeric' => ':attribute musi być co najmniej :min.',
        'file' => ':attribute musi mieć co najmniej :min kilobajtów.',
        'string' => ':attribute musi zawierać co najmniej :min znaków.',
        'array' => ':attribute musi zawierać co najmniej :min pozycji.',
    ],
    'not_in' => 'Wybrany :attribute jest nieprawidłowy.',
    'numeric' => ':attribute musi być liczbą.',
    'regex' => ':attribute posiada nieprawidłowy format.',
    'required' => 'Pole :attribute jest wymagane.',
    'required_if' => 'Pole :attribute jest wymagane, jeśli :other jest :wartość.',
    'required_unless' => 'Pole :attribute jest wymagane, o ile :other nie jest w :values.',
    'required_with' => 'Pole :attribute jest wymagane, jeśli :values są obecne.',
    'required_with_all' => 'Pole :attribute jest wymagane, jeśli :values są obecne.',
    'required_without' => 'Pole :attribute jest wymagane, jeśli :values nie są obecne.',
    'required_without_all' => 'Pole :attribute jest wymagane, jeśli żadne :values nie są obecne.',
    'same' => ':attribute oraz :other muszą być ze sobą zgodne.',
    'size' => [
        'numeric' => ':attribute musi posiadać :size.',
        'file' => ':attribute musi posiadać :size kilobajtów.',
        'string' => ':attribute musi zawierać :size znaków.',
        'array' => ':attribute musi zawierać :size pozycji.',
    ],
    'string' => ':attribute musi być ciągiem.',
    'timezone' => ':attribute musi być prawidłową strefą.',
    'unique' => ':attribute został już odebrany.',
    'url' => ':attribute posiada nieprawidłowy format.',

    /* |-------------------------------------------------------------------------- | Wersy językowe dopasowanej walidacji |-------------------------------------------------------------------------- | | Tutaj możesz określić komunikaty dotyczące dopasowanej walidacji dla atrybutów, korzystając z | konwencji "attribute.rule" w celu nazwania linków. Dzięki temu możesz szybko | określić konkretny dopasowany wers językowy dla określonej zasady atrybutu. | */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'dopasowany-komunikat',
        ],
    ],

    /* |-------------------------------------------------------------------------- | Atrybuty dopasowanej walidacji |-------------------------------------------------------------------------- | | Następujące wersy językowe są wykorzystywane w celu wymiany atrybutów zastępczych | na treści bardziej przyjazne czytelnikowi, takie jak Adres e-mail zamiast | „email”. To pomaga w prosty sposób nadać komunikatom zrozumiałą formę. | */

    'attributes' => [],
];