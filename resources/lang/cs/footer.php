<?php
return [
    'footer_1' => 'csgotower.net - UNIKATOWY FORMAT ZRZUTÓW SKÓREK CS:GO',

    'footer_2' => 'NA NASZEJ STRONIE ZNAJDZIESZ NAJLEPSZE SKÓRKI CS:GO. WSZYSCY UŻYTKOWNICY POJAWIAJĄ SIĘ W TRYBIE AUTOMATYCZNYM KORZYSTAJĄCYM ZE STEAMU.',

    'face_commun' => 'Facebook Community',

    'faq' => 'FAQ - Odpowiedzi na pytania',

    'contacts' => 'Kontakt',

    'agreement' => 'Umowa z użytkownikiem',

];