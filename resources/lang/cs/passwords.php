<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasła muszą składać się z co najmniej sześciu znaków i zgadzać się z hasłem podanym w polu potwierdzenia.',
    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Wysłaliśmy do ciebie email z linkiem do zresetowania hasła!',
    'token' => 'Te dane do zresetowania hasła są nieprawidłowe.',
    'user' => 'Nie znaleźliśmy użytkownika o podanym adres e-mail.',

];
