<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Te dane uwierzytelniania nie są zgodne z danymi zapisanymi przez nas.',

    'throttle' => 'Podjęto zbyt wiele prób logowania. Spróbuj ponownie za :seconds sekundy.',

];
