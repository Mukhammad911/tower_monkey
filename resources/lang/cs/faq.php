<?php
return [
    'question_1' => 'Czym jest csgotower?',

    'answer_1' => 'csgotower jest projektem obejmującym różne typy gier online takie jak na przykład otwieranie skrzynek, które umożliwiają podniesienie kapitału w grach CS:GO lub Dota.',

    'question_2' => 'W jaki sposób mogę rozpocząć grę na tej stronie?',

    'answer_2' => 'By zacząć grać razem z nami, musisz dodać środki do swojego konta, korzystając z G2A PAY w dowolny preferowany przez ciebie sposób, przy pomocy PayPala, katd MasterCard, Visa, PaySafeCard itp. Otrzymasz dostęp do swoich środków po autoryzacji na csgotower przy pomocy twojego konta Steam.',

    'question_3' => 'Czym są punkty (pkt)?',

    'answer_3' => 'Czym są punkty (pkt)? To uniwersalna waluta csgotower. Przy pomocy tej waluty możesz nabywać przedmioty w grach CS:GO, Dota 2 lub PUBG, a następnie pobierać je. Kurs waluty: 1 pkt = 1 dolar amerykański. Jak widać, jest to bardzo proste :)',

    'question_4' => 'W jaki sposób mogę pobrać zakupione przedmioty?',

    'answer_4' => 'Możesz to zrobić na stronie CS:GO http://csgotower.net/csgo lub na stronie http://csgotower.net/dota2 w przypadku Dota 2, wykonując następujące kroki: 1. Wybierz obiekt, który chcesz pobrać. 2. Kliknij przycisk "Pobierz", a następnie kliknij "Wyślij" w oknie dialogowym. 3. Następnie twoje przedmioty automatycznie pojawią się na twoim koncie Steam w ciągu 30 sekund.',

    'question_5' => 'Dlaczego powinienem wam zaufać?',

    'answer_5' => 'csgotower jest w pełni otwarte na swoich klientów. Oferujemy aktywny live stream z ostatnimi zwycięzcami, który jest aktualizowany w czasie rzeczywistym. Tworzymy listy najlepszych graczy oraz, oczywiście, stosujemy narzędzie Provably Fair (Sprawdzanie uczciwości), które skutecznie weryfikuje fakt, że nie mamy możliwości wpływania na przebieg twojej gry oraz że wszystko jest wysoce transparentne. Poza tym możesz zawsze sprawdzić filmy w serwisie Youtube i dowiedzieć się, co sądzą o nas inni gracze, by ostatecznie wyrobić sobie zdanie :)',

    'question_6' => 'Ok. Dlaczego mam grać na waszej stronie?',

    'answer_6' => 'Skrzynki i inne gry na csgotower są znacznie bardziej opłacalne niż na stronie Valve czy w innych portalach. U nas masz znacznie wyższe szanse na zwrot zainwestowanych środków',

    'question_7' => 'Gdzie mogę zdobyć link do wymiany?',

    'answer_7' => ' http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url to twój link do wymiany .',

    'question_8' => 'Dodałem środki do mojego konta, lecz nie zostały one przelane. Co można z tym zrobić? ',

    'answer_8' => 'W celu przetworzenia płatności korzystamy z systemu G2A Pay. Zazwyczaj jej zarejestrowanie i dokończenie zajmuje 10–15 minut. Czasem może być przedłużona do 1 godziny. Możesz skontaktować się z naszą obsługą klienta lub z obsługą klienta G2A Pay w celu uzyskania informacji dotyczących twojej płatności. Wszystkie twoje transakcje oraz ich status są dostępne w gabinecie osobistym.',

    'question_9' => 'Nie mogę autoryzować mojego konta na stronie. ',

    'answer_9' => 'W celu autoryzacji kont korzystamy ze Społeczności Steam. Czasem zdarzają się problemy techniczne na stronie Steamu, przez co nie jest ona w stanie przetworzyć twoich żądań. Zazwyczaj Steam potrzebuje od około 5 minut do kilku godzin, by przywrócić właściwe działanie swoich serwerów.',

    'question_10' => 'Nie mogę pobrać mojego przedmiotu na stronie Steamu. ',
    'answer_10' => 'Prawdopodobną przyczyną są chwilowe problemy techniczne na stronie Steamu. Spróbuj pobrać swój przedmiot trochę później, kiedy Steam naprawi swoje serwery. Ważne jest także, byś nie był objęty żadnymi restrykcjami Steam Guard. Powody są podane tutaj — https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.',

    'question_11' => 'Czy posiadacie obsługę klienta?',

    'answer_11' => 'Tak, oczywiście. Skontaktuj się z nami pisząc na adres support@csgotower.net w dowolnym momencie, a odezwiemy się do ciebie w ciągu 24 godzin.',
];