<?php


return [
    'question_1' => 'Was ist csgotower?',
    'answer_1' => 'csgotower ist ein Projekt mit verschiedenen Arten von Online-Spielen, wie zum Beispiel Kisten öffnen, was es dir ermöglicht, dein Kapital in CS:GO oder Dota-Items zu erhöhen.',

    'question_2' => 'Wie beginne ich, auf eurer Website zu spielen?',
    'answer_2' => 'Um anzufangen, bei uns zu spielen, musst du mithilfe G2A PAY Geld auf dein Konto einzahlen - und zwar mit deiner bevorzugten Methode: per Paypal, MasterCard, Visa, PaySafeCard und so weiter. Du bekommst Zugriff auf dein Konto, nachdem du csgotower mit deinem Steam-Account autorisiert hast.',

    'question_3' => 'Was sind Credits (Crd)?',
    'answer_3' => 'Das ist die universelle Währung bei csgotower. Mit dieser Währung kannst du in CS:GO oder Dota 2 Märkten Items kaufen und sie dann entnehmen. Wechselkurs ist 1crd = 1 USD. Wie du siehst, ist alles ziemlich einfach \:)',

    'question_4' => 'Wie erhalte ich meine gekauften Items?',
    'answer_4' => 'Du kannst das auf der CS:GO-Seite [http://csgotower.net/csgo] oder  auf [http://csgotower.net/dota2] für Dota 2 durch die folgenden Schritte tun: 1. Wähle das Objekt aus, das du erhalten möchtest. 2. Drücke im Popup-Fenster auf "Withdraw" und dann auf "Send". 3. Danach erhältst du automatisch innerhalb der nächsten 30 Sekunden dein Item in Steam.',

    'question_5' => 'Warum sollte ich euch Vertrauen?',
    'answer_5' => 'csgotower ist vollständig offen zu seinen Kunden. Es gibt einen offenen Live Drop mit den aktuellen Gewinnern, der in Echtzeit aktualisiert wird. Wir listen die Top-Spieler auf und verwenden natürlich das Provably Fair Tool, das eindeutig bestätigt, dass wir nicht in der Lage sind, den Spielverlauf zu beeinflussen sind und dass alles sehr transparent ist. Außerdem kannst du dir immer Videos auf Youtube ansehen und erfahren, was andere Spieler von uns denken, um dein endgültiges Urteil zu fällen :)',

    'question_6' => 'Ok. Warum sollte ich auf eurer Website spielen?',
    'answer_6' => 'Die Kisten und andere Spiele auf csgotower sind viel profitabler als auf Valve und anderen Webseiten. Hier hast du eine viel höhere Chance darauf, dass sich deine Investition auszahlt.',

    'question_7' => 'Wo bekomme ich meine Tausch-URL?',
    'answer_7' => '[http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url] ist deine Url für den Austausch zwischen unseren Bots und deinem Konto.',

    'question_8' => 'Ich habe Geld auf mein Konto geladen, aber es ist nicht übertragen worden. Was kann getan werden?  ',
    'answer_8' => 'Wir verwenden G2A Pay, um deine Zahlungen zu verarbeiten. In der Regel dauert es ca. 10-15 Minuten bis deine Zahlung gebucht wurde. Manchmal kann es um ca. 1 Stunde verzögert sein. Du kannst unseren Kunden-Support oder G2A Pay Kunden-Support kontaktieren, um weitere Informationen über deine Zahlung zu erhalten. Alle deine Transaktionen und deren Status sind in deinem persönlichen Bereich einsehbar.',

    'question_9' => 'Ich kann mich nicht in mein Konto einloggen.',
    'answer_9' => 'Wir nutzen Steam Community für die Kontenberechtigung. Manchmal gibt es technische Probleme bei Steam und es ist nicht in der Lage, deine Anfragen zu verarbeiten. Normalerweise dauert es zwischen ca. 5 Minuten und ein paar Stunden für Steam, dass es seine Server wieder zum Laufen bringen.',

    'question_10' => 'Ich bin nicht in der Lage, meinen Artikel in Steam zu bekommen. ',
    'answer_10' => 'Dein Problem könnte im Zusammenhang mit aktuellen technischen Problemen bei Steam stehen. Versuche, dein Item ein bisschen später zu entnehmen, nachdem Steam seine Server-Fehler behoben hat. Es ist auch wichtig, dass du keine Steam Guard Einschränkungen hast. Die Gründe dafür sind hier aufgelistet — https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.',

    'question_11' => 'Habt ihr einen Kunden-Support?',
    'answer_11' => 'Ja, klar. Kontaktiere uns jederzeit einfach unter [support@csgotower.net|mailto:support@csgotower.net] und wir werden uns umgehend mit dir innerhalb von 24 Stunden in Verbindung setzen. ',
];