<?php

return [
	'footer_1' => 'csgotower.net - EINZIGARTIGES FORMAT FÜR CS: GO KISTEN',
	'footer_2' => 'AUF UNSERER SEITE KANNST DU DIE BESTEN CS: GO SKINS ERHALTEN. ALLE TRADES LAUFEN IM AUTOMATIK-MODUS DURCH STEAM AB.',
    'face_commun' => 'Facebook Community',
	'faq' => 'FAQ - Antworten auf Fragen',
	'contacts' => 'Kontakt',
	'agreement' => 'Nutzervereinbarungen',
];