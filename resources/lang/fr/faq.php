<?php


return [
    'question_1' => 'Qu’est-ce que csgotower ?',
    'answer_1' => 'csgotower est un projet avec différents types de jeux en ligne, comme par exemple, des énigmes à résoudre pour vous permettre d’augmenter votre capital d’articles CS:GO ou Dota',

    'question_2' => 'Comment je commence à jouer sur votre site Internet ?',
    'answer_2' => 'Afin de commencer à jouer avec nous, vous devez ajouter de l’argent à votre compte en utilisant G2A PAY ou le moyen de paiement que vous préférez parmi Paypal, Mastercard, Visa, PaySafeCard et d’autres. Vous pourrez accéder à votre solde après avoir autorisé csgotower à utiliser votre compte Steam.',

    'question_3' => 'Que sont les crédits (crd)?',
    'answer_3' => 'C’est la devise universelle de csgotower. Avec cette devise, vous pouvez acheter des articles sur les marchés de CS:GO et de Dota 2, puis les retirer. Le taux de conversion est de 1 crédit = 1 $. Comme vous pouvez le voir, c’est assez simple \:)',

    'question_4' => 'Comment retirer les articles que j’ai acheté ?',
    'answer_4' => 'Vous pouvez le faire sur la page de CS:GO [http://csgotower.net/csgo] ou sur [http://csgotower.net/dota2] pour Dota 2 en suivant les étapes suivantes : 1. Sélectionnez l’objet que vous voulez retirer. 2. Appuyez sur le bouton « Retirer », puis sur le bouton « Envoyer » dans la fenêtre instantanée. 3. Après cela, vous obtenez immédiatement votre article sur Steam dans les 30 secondes suivantes.',

    'question_5' => 'Pourquoi devrais-je vous faire confiance ?',
    'answer_5' => 'csgotower est totalement transparent pour ses clients. Il y a les drops en direct avec les derniers gagnants mises à jour en temps réel. Nous listons les meilleurs joueurs et bien évidemment, nous utilisons l’outil d’Equité Prouvée qui vérifie clairement que nous ne pouvons pas impacter le déroulement de votre jeu et que tout est totalement transparent. De plus, vous pouvez toujours consulter des vidéos sur Youtube et apprendre ce que les autres joueurs pensent de nous afin de vous faire un avis :)',

    'question_6' => 'Ok. Pourquoi devrais-je jouer sur votre site Internet ?',
    'answer_6' => 'Les boites et les autres jeux sur csgotower sont bien plus attractifs que sur Valve et sur les autres sites Internet. Vous avez une plus grande chance de rembourser votre investissement.',

    'question_7' => 'Où est-ce que j’obtiens de l’URL de Transaction ?',
    'answer_7' => '[http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url] est votre url de transaction pour les échanges entre nos robots et votre compte.',

    'question_8' => 'J’ai ajouté de l’argent à mon compte, mais il n’a pas été transféré. Qu’est-ce qui peut être fait ?',
    'answer_8' => 'Nous utilisons G2A pay pour effectuer vos paiements. Cela prend généralement environ 10 à 15 minutes pour qu’ils aient lieu. Parfois, ils peuvent être retardés d’environ 1 heure. Vous pouvez contacter notre service client ou le service client de G2A Pay afin d’obtenir plus d’informations sur votre paiement. Toutes vos transactions et leurs statuts sont disponibles dans l’espace personnel.',

    'question_9' => 'Je n’arrive pas à me connecter à mon compte sur le site Internet.',
    'answer_9' => 'Nous utilisons Steam Community pour l’autorisation du compte. Il y a parfois des problèmes techniques sur Steam et il n’est alors pas possible de répondre à vos requêtes. Cela prend généralement entre 5 minutes et plusieurs heures pour que Steam puisse remettre ses serveurs en marche. ',

    'question_10' => 'Je n’arrive pas à obtenir mon objet sur Steam.  ',
    'answer_10' => 'Votre problème pourrait être relatif au fait que Steam rencontre des problèmes techniques en ce moment. Essayez de retirer votre article un peu plus tard, après que Steam ait réparé ses serveurs. Il est également important que vous n’ayez aucune restrictions Steam Guards. Les raisons pour cela sont listées ici : 
https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.
',

    'question_11' => 'Avez-vous un service client ?',
    'answer_11' => 'Oui, tout à fait. Contactez-nous simplement sur [support@csgotower.net|mailto:support@csgotower.net] quand vous le pouvez et nous reviendrons vers vous dans les prochaines 24 heures.',
];