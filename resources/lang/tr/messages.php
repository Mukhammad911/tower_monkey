<?php

return [
    //market
    'market' => 'PAZAR',


    //top_winners
    'top_winners' => 'EN ÇOK KAZANANLAR',

    //provably_fair
    'provably_fair' => 'KANITLANABİLİR ADİLLİK',
    'faq' => 'FAQ',


    //index
    'ORIGINAL CASES' => 'ORJİNAL KASALAR',
    'credits' => 'Krediler',
    'you_may_win' => 'Şunu kazanabilirsiniz',
    'SPECIAL CASES' => 'ÖZEL KASALAR',
    'show_all' => 'HEPSİNİ GÖSTER',
    'new_way' => 'Derileri almanın kolay yolu!',

    //profile
    'personal_area' => 'KİŞİSEL ALAN',
    'activate_promocode' => 'PROMOKODUNU AKTİVE ET',
    'enter_promocode_here' => 'Promokodunuzu buraya girin',
    'activate' => 'AKTİVE ET',
    'payment' => 'ÖDEME',
    'payment_history' => 'Son ödeme: Ödeme geçmişi',
    'date' => 'Tarih',
    'details' => 'Detaylar Durumu',
    'attention_five_min_payment' => 'Dikkat! Ödemeler 5-10 dakika gecikebilir!',
    'example' => 'Örnek ',
    'calculator' => 'Hesap makinesi ',

    'your_link_exchange' => 'DEĞİŞ TOKUŞ-URL’NİZ (STEAM İNTERNET SİTESİNDEN KOPYALA)',
    'save' => 'KAYDET',
    'profile' => 'Profilim',
    'add_balance' => 'BAKİYE EKLE',
    'enter_amount' => 'Miktarı girin',
    'pay' => 'ÖDE',
    'balance' => 'Bakiye',
    'live_drop' => 'CANLI DAMLA',
    'online' => 'Online',
    'contacts' => 'Kişi ve kurum bilgileri',
    'terms' => 'Şart & koşullar',
    'cases_opened' => 'GAMES',


    'uniq_format' => 'CS:GO kasa açmanın yeni formatı',
    'uniq_format_text' => 'Sizin seçiminiz. Sizin skininiz!',
    'fast_text' => 'Hızlı',
    'auto_system' => 'Botlarımızın Steamle olan takas hızına şaşacaksınız. Skinlerin gönderimi tamamen otomatiktir ve Steam kullanımı için özel tasarlanmış yüksek hızlı botlar tarafından yapılır. Eşya listenizdeki tüm eşyalar anında size gönderilebilir. Diğer sitelerde olduğu gibi skinlerinizi gönderirken gecikme yaratmıyoruz.',
    'my_benefit' => 'Karlı',
    'my_benefit_text' => 'Normal cs:go kasalarından ve diğer kasa açma sitelerinden daha iyi takaslar yapın.',
    'new_format_cs' => 'Yeni Format',
    'new_format_cs_go' => 'CS:GO KASALARININ YENİ formati',
    'reliable_text' => 'Tüm skinler eşya listenizde güvenli olarak saklanır. Kasayı açmanızdan bir saat geçtikten sonra profilinize girebilir ve skinleri Steam hesabınıza gönderebilirsiniz. Skin bir saat içinde gönderilmezse – piyasa fiyatından otomatik olarak satılır ve bu miktar bakiyenize eklenir.',
    'respondent' => 'Destek',
    'respondent_text' => 'Destek ekibimiz her problemi çözer',
    'tutor_auth' => 'ÜCRETSİZ DEMO BİLET OYNA',
    'top_in_earnings' => 'TOP in earnings',

    //rules
    'notice_first' => 'CS:GO kasa açmanın en iyi formatını anında deneyin İdman sırasında eşya',
    'notice_second' => 'ALMAZSINIZ, ancak oyunu deneyebilirsiniz. Sadece ',
    'notice_third' => 'giriş yapın',

    'giveaway' => 'HEDİYE',
    'affiliates' => 'ORTAKLAR',
    'top_trop' => 'TOP TAKAS',
    'twitter' => 'TWITTER',
    'open_ticket' => 'BİLETİ \'A AÇ',
    'rules_first' => 'csgotower\'IN TEMEL KURALLARI:',
    'rules_second' => 'Aynı eşyadan 3 tane açın ve alın!',
    'rules_third' => '3 Deneme artı 1 ek deneme isteğe bağlı',
    'rules_fourth' => 'Herkes için garanti gazanç',
    'ticket_contains' => 'BİLET ŞUNU İÇERİR',

    'caserarity' => 'NADİR KASALAR',
    'rangcase' => 'RÜTBE KASALARI',
    'assemblies' => 'BİZİM BİRLEŞTİRMELERİMİZ',
    'сollections' => 'KOLEKSİYONLAR',
    'standart' => 'STANDART KASALAR',


    'users' => 'KULLANICILAR',
    'user_rating' => 'Kullanıcı Puanı',
    'top_24_hours' => 'SON 24 SAATTEKİ TOP KULLANICILAR',
    'tickets' => 'BİLETLER',
    'in_profit' => 'KARDA',

    //faq => privilege
    'about_project' => 'Proje ve Garantiler Hakkında',
    'privilege_title' => 'Neden bizle kasa/bilet açmalısınız?',

    'gold_title_1' => 'Tamamen otomatik sistem. ',
    'warranty_1' => 'Değişim teklifi bottan saniyeler içinde gelir – en iyi şeyleri basit ve hızlı kazanmanın sizin için önemli olduğunu düşünüyoruz.',

    'profit' => 'KARLI',
    'gold_title_2' => 'csgotower’da takas oyun içi kasa açmadan garantili daha iyidir. ',
    'warranty_2' => 'Biletten sadece en iyi şeyi kazanmak değil, aynı zamanda garantili slot skin paketi de iyi bir takas içerir! ',
    'warranty_2_link' => 'Kullanıcı yorumlarını',
    'warranty_2_2' => ' okuyun ve sıklıkla StatTrak eşyalarına rastlayacağınızdan emin olun.',

    'reliably' => 'GÜVENLİ',
    'gold_title_3' => 'Saklayacak hiçbir şeyimiz yok – tüm istatistikler göz önünde. ',
    'warranty_3' => 'Canlı-tape skinlerindeki eşyaya tıklayın ve kullanıcı- kullanıcı adı, Steam-hesabı ve takas geçmişi hakkında detaylı bilgileri görün. ',
    'warranty_3_link' => 'TOP-kullanıcılara',
    'warranty_3_2' => ' her zaman başlıktaki bağlantı yoluyla bakabilirsiniz. Ayrıca sayfanın en altında csgotower’ın genel istatistiklerini görebilirsiniz.',

    'gold_title_4' => 'Her zaman skininizi alırsınız! ',
    'warranty_4' => 'Hiçbir şey almadıysanız yada online satmaya karar verdiyseniz, eşyanın aldığınız zamandaki değerinin aynısını alırsınız. Her zaman için profilinize girip eşyayı tekrar gönderebilirsiniz.',

    //faq => instruction
    'instruction_title' => 'Skin nasıl alınır?',
    'step' => 'Aşama',
    'instruction_1' => 'Steam yoluyla giriş yapın, kullanıcı adınızın üstüne tıklayarak hesabınıza gidin',

    'instruction_2' => 'İlk idman biletinizi ',
    'instruction_2_link' => 'ana sayfadan oynayın',

    'instruction_3' => '',
    'instruction_3_link' => 'Hesap sayfasında',
    'instruction_3_2' => ', Alım-satım bağlantısına ',
    'instruction_3_link_2' => 'bu nedir?',
    'instruction_3_3' => ' girin — böylece anında skinleri alabilirsiniz',

    'instruction_4' => ' size uygun olacak şekilde',
    'instruction_4_link' => 'Hesabınızı doldurun',

    'instruction_5' => 'Bilette 9 alanın 3’ünde benzer resmi bulun ve kasadaki en iyi şeyi alın! ',
    'instruction_5_2' => 'csgotower.net’da sadece garantili skin paketli biletleri açtığınızdan her şekilde bir şey kazanırsınız.',
    'instruction_5_3' => 'csgotower’da sadece şans değil, içgüdüleriniz de işe yarar!',

    'faq_q1' => 'Steam’de kazandığım eşyayı gönderemiyorum!',
    'faq_a1' => 'Önemli! Öncelikle kasayı açmadan önce – Steam hesabınızın eşya alabildiğinden emin olun. Steam hesabınıza gidin ve eşya listenize bakın, burada hesabın dondurulduğu notu olacaktır. Steam hesabı şifre, eposta, Steam Guard, 2FA’ya bağlanmadan sonra 7 günlüğüne dondurulabilir… dondurulmuş Steam hesabı eşya alamaz, bu nedenle sitemiz size kazandığınız eşyayı gönderemeyecektir. Bu önemli bir nokta bu yüzden daha önce birkaç kez bu konuda sizleri uyardık.',

    'faq_q2' => 'Hesabımı henüz açtım ve bakiyede para yok',
    'faq_a2' => 'Ödemeler csgotower G2A Ödeme sistemi kullanılarak yapılır. Tüm ödemeler bu ödeme sistemiyle yapılır. Tüm sınırlamalar ve hatalar G2A Pay sisteminin sınırlamalarından doğmaktadır. Genellikle ödemeler 5-10 dakikada tamamlanır. Bazen ödemeler 1 saat kadar gecikebilir. Ödemeniz hakkında daha fazla bilgi için destek ekibimizle iletişime geçebilirsiniz.',

    'faq_q3' => 'Bilet açtım, hediyem nerede?',
    'faq_a3' => 'Kazançlarınız eşya listenizde saklanır, Eşya listesine hediyenin üstüne tıklayarak, yada sitenin sağ üst köşesinde bulunan eşya listesi avatarına tıklayarak gidebilirsiniz.',

    'faq_q4' => 'Kazançlarım neden satıldı?',
    'faq_a4' => 'Eşya listenizdeki skinler bir saatten sonra otomatik olarak satılır. Bu önlem botlara engel olur ve kasada her zaman mevcut skin bulundurmaya olanak verir.',

    'faq_q5' => 'Steam aracılığıyla giriş yapamıyorum yada eşya gönderemiyorum',
    'faq_a5' => 'csgotower kullanıcıları yetkilendirmek ve skin göndermek için Steam Topluluğu’nu kullanır. Bazen Steam sisteminde sorunlar oluşabilir yada taleplerimize cevap vermeyebilir.',


    'faq_attention' => 'Dikkat!',
    'faq_attention_2' => 'Bu bölümde sorunuza cevap bulamadıysanız bize ',
    'faq_attention_3' => ', adresinden mail yada ',
    'faq_attention_link' => 'Facebook’tan mesaj atabilirsiniz!',

    //profile
    'initial_settings' => 'Profil Ayarları',
    'link_exchange' => 'Takas Bağlantısı',
    'enter_below' => 'Aşağı girin.',
    'prfole_what' => 'What is it?',
    'refill_balance' => 'Bakiyeyi Doldur',
    'all_electronic_money' => 'Her türde elektronik para',
    'winer' => 'Kazan!',
    'get_best_drop' => 'En iyi Takası yapın',
    'copy_steam_link' => 'Steam sitesiyle kopyala',
    'save_profile_link' => 'KAYDET',
    'trade_restrict_link1' => 'STEAM HESABINIZIN ',
    'trade_restrict_link2' => 'TAKAS TEKLİFLERİNİ KABUL ETTİĞİNDEN EMİN OLUN!',

    //profile => info block
    'header_pointer' => 'Dikkat edin!',

    'bad_head' => 'Scamlere dikkat edin!',
    'bad_text' => 'Sizi hiçbir zaman Steam’de arkadaş olarak eklemez ve takas teklif etmeyiz – bunların hepsi yalandır! Dikkatli olun ve eşyalarınızı çaldırmayın.',

    'good_head' => 'Destek ve SSS',
    'good_text' => 'Özellikle sizin için tüm temel soruları ',
    'good_text_link' => 'SSS',
    'good_text_2' => ', hizmetimizde topladık ve daha spesifik konularınız için bize ',
    'good_text_link_2' => 'Facebook’tan mesaj yazabilirsiniz.',

    'vk_head' => 'Skin yarışları ve çekilişleri',
    'vk_text' => 'Düzenli olarak yarışma ve çekiliş düzenliyor ve ',
    'vk_text_link' => 'Facebook sayfamızda',
    'vk_text_2' => ' bakiyenizi yenilemek için promo kodları dağıtıyoruz, bize katılın!',

    'promocodes' => 'Promo kodları',
    'promocodes_text' => 'İnternet sitesinde ve ',
    'promocodes_link' => 'Twitter’da',
    'promocodes_text_2' => ' düzenli olarak promosyonel kodlar veriyoruz. Bunları ilk önce girin ve bakiyenize bonus alın!',
    'promocodes_button' => 'Tamam!',

    'items_incase_text' => 'Şu an eşya yok. ',
    'items_incase_link' => 'İlk biletinizi açın!',

    'congratulate' => 'Tebrikler!',
    'card.congratulation' => 'Tebrikler!',
    'card.thisisyour' => 'Bu sizin ',
    'card.tryanother' => 'Daha fazla dene',
    'card.sellfor' => 'Satış',
    'card.take' => 'Al',
    'card.tohome' => 'Ana Sayfaya Git',
    'balance_up' => 'Bakiyeyi doldurma',

    'cases' => 'Kasalar',
    'collections' => 'Koleksiyonlar',
    'exclusive' => 'exclusive',
    'login' => 'Steam üzerinden giriş yap',
    'login_mobile' => 'giriş yap',
    'logout' => 'çıkış Yap',
    'menu' => 'Menu',
    'guarantees' => 'Garantiler',
    'reviews' => 'Yorumlar',
    'shop_cases' => 'Drop Gabe’takinden daha iyidir',
    'lucky' => 'En Şanslılar',
    'bots_trades' => 'BOT\'S LIVE TRADES',
    'balance_fill' => 'Para yükleyiniz',
    'faq_how' => 'How does it works?',

    'faq_step' => 'Steam üzerinden oturum açınız ve hesap sayfasına geçiniz',
    'faq_step2' => 'Trade linkini giriniz ve artık kazanılan eşyaları anında alabilirsiniz',
    'faq_step3' => 'Web sitesinde her hangi uygun bir şekilde para yükleyiniz',
    'faq_step4' => 'Kasaları dünyada en uygun fiyatlarla açınız!',

    'q1' => 'I opened the case, but the object does not come, what\'s happening?!',
    'ans1' => 'Open your profile and click the link "To take" on the object you are interested in.',
    'q2' => 'When I want to take object off the Bot , I receive a mistake: " Unfortunately, our bot couldn\'t send your object..."',
    'ans2' => 'Bot can\'t send you the object in the following:
                        You have the exchange blocked ( you entered from the new appliance, changed the password or e-mail recently, changed the security settings Steam Guard, have the VAC-ban, etc. More..... in order to avoid such problems in future, always recommend to check your trade status at special page csho.tm.
                        You have inventory hidden in the privacy settings Steam
                        Check your trade link in the profile and in the settings Steam - they have to coincide
                        Bot has the problems with the Steam connection, it may happens andd very often, the problem is in the Steam, it\'s not our problem. Also bot can be banned, in this case you have to adress to technical support (support@csgotower.net)
                        If you don\'t take your object during one hour, the whole price of this object acording to it\'s price at the Steam market.',
    'q3' => 'I can\'t open the cases, it is indicated the mistake " At the moment bot doesnt have the object for the case choosen. Try to do it later."',
    'ans3' => 'For the real random, cases/categories must always contain ALL the objects in it, which are listed at the page of the case/categor. Bots don\'t have enough time to buy all necessary things in the time needed or some necessary objects for this case are absent at the market now.',
    'q4' => 'Filled the account, but money doesn\'t appear. What to do?',
    'ans4' => 'Money can appear in some time - wait an hour. If money not appeared more than in an hour write to us (support@csgotower.net),indicating the whole information of the payment.',
    'q5' => 'I have money at Steam account but they are not indicated here. Why?',
    'ans5' => 'Steam account money are not valid here at this site. To fill the balance it is needed separately.',
    'q6' => 'Can\'t enter the site through Steam, the page with the cases is not loading, there is an advertisement in the centre of the page , and in general site is not working good, what to do?',
    'ans6' => 'May be the problem is in antivirus, which can block the conection with the site, or the problem is in a browser, which shows the advertisement also can block the connection with the site.',
    'steam_profile' => 'Steam profili',
    'settings' => 'Settings',
    'transe_url_enter' => 'Trade-Url giriniz',
    'field_bottom' => 'Giriş alanı aşağıdadır',
    'add_credit' => 'Para yükleyiniz',
    'payment_methods' => 'VISA, MASTERCARD, PAYPAL ETC.',
    'win' => 'Kazanınız',
    'best_cases' => 'Kasaları uygun şartlarda açınız',
    'explore_url' => 'Explore Trade-URL',
    'your_items' => 'EŞYALARINIZ',

    'accepted' => 'Alındı',
    'sold' => 'Satıldı',
    'accept' => 'Al',
    'sell' => 'Sat',

    'warn1' => 'Dikkatli olunuz',
    'recomm1' => 'Tüm sorunlar sadece support@csgotower.net adresi üzerinden çözülür. Sosyal ağlarda size site yöneticisi adına yazanları dikkate almayınız, bunlar dolandırıcılardır.',
    'warn2' => 'Kasaları açmadan önce',
    'recomm2' => 'Steam hesabınızdaki takasları kontrol ediniz aksi takdirde botunuz eşyalarınızı size gönderemeyecektir. Demirbaşı açınız, şifre değiştirildikten sonra 7 gün içinde yasağın yokluğuna emin olunuz.',
    'warn3' => 'FAQ ve diğer sorular',
    'recomm3' => [
        'Ortaya çıkabilecek bütün sorunların yer aldığı',
        'FAQ',
        'sayfasını okuyunuz.'
    ],
    'warn4' => 'Drop’u alınız',
    'recomm4' => 'Eşyayı 30 dakika içinde almadığınız takdirde tam bedeli hesabınıza iade edilecektir.',
    'sending' => 'Gönderiyoruz',
    'items' => 'Eşyalar',
    'case_contains' => 'case contains',
    'open_case' => 'Kasayı aç',
    'open_for' => 'Open case for',
    'opening' => 'Opening',
    'unavailable' => 'Temporarily unavailable',
    'no_money' => 'There is not enough money to open case. Fill up on',
    'your_drop' => 'Your win',
    'try_again' => 'Tekrar dene',
    'sell_for' => 'Sell for',
    'congratulations' => 'Congratulations! The item can be accepted up within an hour.',
    'home' => 'Home',
    'reliable' => 'Güvenli',
    'qualitatively' => 'NİTELİKLİ',
    'fast' => 'hızlı',
    'reliable_mess' => 'In case you have not taken your items at once, you can go to the profile and resend it. For example, if you open 10 or 15 cases in a row and do not accept the items at once, the bot will send them to you again. Just go to your profile, and send the items again.',
    'qualitatively_mess' => 'We do not lie, and we can prove it. Clicking on the item in the “live-drops” feed you will see the user\'s profile, with a history of items that have fallen to him. What is it for? You may have seen a similar “live-drops” feed on other sites where different “Azimov for 50$” fall out, and then you buy cases and weapons dropped out for 0.5-1 dollars.',
    'fast_mess' => 'Everything is fully automated. Sending items via bots greatly simplifies the whole process and a special method of procurement guarantees getting of any item, whether it is “COVERT” or “MIL-SPEC” quality.',
    'server_error' => 'Server\'s error. Try again later!',
    'profile_sttings_success' => 'Trade URL successfully updated!',
    'profile_sttings_error' => 'Saving Error!',
    'success' => 'Success',
    'error' => 'Hata',
    'drops' => 'Bütün droplar',
    'loading' => 'Loading',
    'enter_your' => 'Trade-Url’unuzu giriniz',
    'pay_attention' => 'Attention! Payments could be delayed for 5-10 minutes!',
    'invalid_trade_url' => 'Invalid Trade URL format',
    'alien_trade_url' => 'That is not your trade URL',
    'second_ago' => 'second ago',
    'no_items' => 'HENÜZ EŞYALAR YOKTUR. İLK ',
    'first_case' => 'KASANI AÇ!',
    'contact' => 'please contact',
    'payment_error' => 'Payment error',
    'payment_success' => 'Thank you! Your payment is successfully done. Be aware, payments could be delayed for 5-10 minutes!',
    'receive' => 'receive',
    'received' => 'received',
    'item' => 'item',
    'bot' => 'bot',
    'user' => 'kullanıcı',
    'time' => 'time',
    'trade\'s' => 'trade',
    'send_item' => 'Send item to your steam account you can in',
    'pay_skins_message' => 'Be aware that when you deposit money using skins, your balance will receive the sum that was mentioned in the order.',

    //marks
    'sale' => 'sale',
    'popularly' => 'popular',
    'novelty' => 'new',
    'best-price' => 'best price',

    //affiliates
    'partner_head'=>'ORTAKLIK',
    'your_partner_link'=>'ORTAKLIK BAĞLANTINIZ',
    'level'=>'SEVİYE',
    'referrals'=>'REFERRALLAR',
    'to_next_level'=>'SONRAKİ SEVİYEYE',
    'percentage'=>'Percentage',
    'referral_receive'=>'REFERRAL’İN ALDIĞI',
    'referral_deposited'=>'REFERRAL’IN YATIRDIĞI',
    'earnings'=>'KAZANÇLAR',
    'best_affiliates'=>'EN İYİ ORTAKLAR',
    'on_csgotower'=>'csgotower’de referrallardan en iyi karı siz aldınız.',
    'on_start'=>'% BAŞLANGIÇTA ',
    'percent_on_market'=>'Ortaklığımızın başında pazardaki en yüksek yüzdeyi alacaksınız.',
    'sponsorship'=>'SPONSORLUK',
    'youtube_or_twitch'=>'Youtube yada Twitch kanalınız mı var? İşbirliğimizi konuşmak için bizimle media@csgotower.net adresinden iletişime geçin.',
    'partner_levels'=>'ORTAKLIK SEVİYELERİ',
    'to_deposit'=>'PARA YATIRMAYA',

    'paypal_attention' => 'Dikkat! PayPal Kullanıcıları: PayPal ödeme metodu mevcut değildir. Ama hala PayPal’i kullanarak para yatırabilirsiniz. G2A hesabı açmanız, sonrasında PayPal sistemini kullanarak bakiye eklemeniz gerekir. Bunun sonrasında G2A Cüzdanınızı kullanarak csgotower bakiyenize para ekleyebilirsiniz.',
    'hash_number'=>'Kazanan numara',
    'win_number'=>'Win number',
    'user_seed'=>'Kullanıcı seed’I',
    'server_seed'=>'Sunucu seed’i ',
    'next_ticket'=>'SONRAKİ BİLET',
    'previous_ticket'=>'ÖNCEKİ BİLET',
    'close_letter'=>'Kapat',
    'verification'=>'Doğrulama',
    'wagered'=>'Bahis',
    'games'=>'Oyunlar',
    'easy'=>'KOLAY',
    'medium'=>'ORTA',
    'hard'=>'ZOR',
    'play_demo'=>'Play_demo',
    'sort_item_by_price'=>'ÖGELERİ FİYATA GÖRE SIRALA',
    'sort_item_by_quality'=>'KALİTE',
    'price_low'=>'FİYAT: DÜŞÜK -> YÜKSEK',
    'price_high'=>'FİYAT: YÜKSEK -> DÜŞÜK',
    'enter_item_name'=>'Cilt aramak',
    'select_all_view'=>'Hepsini seç görüntüle',
    'refresh'=>'Yenile',
    'withdrawable'=>'Çekilebilir',
    'selected_items'=>'Seçilen ögeler',
    'item_value'=>'Öge değeri',
    'update_steam_trade_url'=>'Steam değiş tokus url’sini güncelle',
    'price_range'=>'ÖGE FIYAT ARALIĞI',
    'win_up_to'=>'’YA KADAR KAZANIN',
    'check_winning_number'=>'Kazanan sayıları kontrol et',
    'try_other_cases'=>'Diğer kasalara',
    'cases_to_open'=>'Açılacak kasalar',
    'credits_case'=>'KREDİ KASA',
    'win_chance'=>'KAZANMA ŞANSI',
    'multiplier'=>'ÇARPAN',
    'withdraw'=>'PARA ÇEK',

    'text_provably_fair_1'=>'The mechanism of the ‘provably fair’ game allows you to control the fair play and impartiality of the algorithms distributing the dice. In order to perform the check you may use either our own online calculator or any other php-tester, e.g.',
    'text_provably_fair_2'=>'This will help calculate the hash of the particular round.',
    'text_provably_fair_3'=>'How does the mechanism of the ‘provably fair’ game work?',
    'text_provably_fair_4'=>'Prior to every round of the game a unique seed is generated based on your data (the client seed) and the data of the server (the server seed).',
    'text_provably_fair_5'=>'After the launch of the round a hash that encodes the winning number is generated using a 256-bit encryption algorithm.',
    'text_provably_fair_6'=>'When the round is over you gain access to the winning number. Then, having all the data, you can encode the seeds and the winning number yourself to calculate the hash and compare it to the one received before the launch of the round.',


    'tower_table_rank'=>'Rütbe',
    'tower_table_player' => 'OYUNCU',
    'tower_table_bet' => 'BAHİS',
    'tower_table_profit' => 'KAR',
    'tower_table_step' => 'ADIM',
    'tower_table_level' => 'seviye',
    'tower_bet_min' => 'min',
    'tower_bet_max' => 'max',
    'tower_verify' => 'Biletin adil olup olmadığını doğrulayın',
    'top_profit' => 'kâr',
    'tower_table_wagered'  => 'BAHSE',
    'min'=>'min',
    'max'=>'max',
    'heroes'=>'KAHRAMANLAR',
    'HALL_OF_HEROES'=>'KAHRAMANLAR HALL',
    'play_demo'=>'DEMO OYNA',
    'start_game'=>'ROZPOCZNIJ GRVE',

    //market
    'selected_skins'=>'seçili kaplamalar',
    'remove_all'=>'hepsini kaldır',

    //header
   'header_tower'=> 'KULE',
   'header_market'=>'MARKET',

    //profile

    'market_stories'=>'Pazar hikayeleri'


];
