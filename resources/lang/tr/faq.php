<?php


return [
    'question_1' => 'csgotower Nedir?',
    'answer_1' => 'csgotower; CS:GO yada Dota eşyalarındaki sermayenizi arttırmanıza olanak veren kasa çözme gibi çeşitli türde online oyunları içeren bir projedir.',

    'question_2' => 'İnternet sitenizde oynamaya nasıl başlarım?',
    'answer_2' => 'Bizimle oynamaya başlayabilmek için G2A PAY kullanarak tercih ettiğiniz şekilde Paypal, MasterCard,Visa, PaySafeCard veya başka bir şekilde hesabınıza para yüklemeniz gerekir. Steam hesabınızı kullanarak BeastStkins’de onaylandıktan sonra bakiyenize erişim sağlayabilirsiniz.',

    'question_3' => 'Krediler (crd) nedir?',
    'answer_3' => 'csgotower’in genel para birimidir. Bu para birimiyle CS:GO yada Dota 2 pazarlarında eşya satın alabilir ve sonrasında onları çekebilirsiniz. 1crd = 1 USD’dir. Gördüğünüz gibi oldukça basittir \:)',

    'question_4' => 'Satın aldığım eşyaları nasıl çekerim?',
    'answer_4' => 'Bunu CS:GO sayfası [http://csgotower.net/csgo]’dan yada Dota 2 için [http://csgotower.net/dota2]’dan şu adımları izleyerek yapabilirsiniz: 1. Çekmek istediğiniz eşyayı seçin. 2. ‘Çek’e basın ve sonrasında beliren pencerede ‘Gönder’e tıklayın.',

    'question_5' => 'Size neden güvenmeliyim?',
    'answer_5' => 'csgotower kullanıcılarına tamamen açıktır. Gerçek zamanlı son kazananların güncellendiği açık canlı damla vardır. En iyi oyuncuları buraya koyuyor ve tabiki oyununuza müdahale edemeyeceğimizi açıkça göstereen Kanıtlanabilir Adil araçlar kullanıyoruz ve bunun sonucunda her şeyi şeffaf kılıyoruz. Bunun yanında her zaman Youtube’daki videoları izleyebilir ve son kararınızı vermeniz için diğer oyuncuların hakkımızda ne düşündüğünü görebilirsiniz ',

    'question_6' => 'Peki. Neden sizin internet sitenizde oynamalıyım?',
    'answer_6' => 'csgotower’teki kasalar ve diğer oyunlar Valve ve diğer internet sitelerinde olduğundan çok daha karlıdır. Burada yatırımınızın geri dönüşünü alma şansınız çok daha yüksektir.',

    'question_7' => 'Değiş Tokuş URL’sini nereden alırım?',
    'answer_7' => '[http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url] botlarımızla hesabınız arasındaki alım satımlar için değiş tokuş url’nizdir.',

    'question_8' => 'Hesabıma para yükledim, ama henüz gönderilmedi. Ne yapabilirim?',
    'answer_8' => 'Ödemelerinizi gerçekleştirmek için G2A Pay kullanıyoruz. Genellikle ödemelerinizin gerçekleşmesi 10-15 dakika sürer. Bazen 1 saate kadar gecikebilir. Ödemeniz hakkında daha fazla bilgi almak için bizim destek ekibimiz yada G2A Pay müşteri hizmetleriyle iletişime geçebilirsiniz. Tüm işlemleriniz ve durumları kişisel kabininizde mevcuttur.',

    'question_9' => 'İnternet sitesinde hesabıma giremiyorum.  ',
    'answer_9' => 'Hesap otorizasyonu için Steam Topluluğu’nu kullanıyoruz. Bazen Steam’de teknik sorunlar olabiliyor ve isteklernizi gerçekleştiremiyebiliyorlar. Genellikle Steam’in sunucularına tekrar eski haline döndürmesi 5 dakika ila bir kaç saat sürebiliyor.',

    'question_10' => 'Eşyamı Steam’de alamadım',
    'answer_10' => 'Probleminiz Steam’in o anda teknik sorunlar yaşıyor olmasıyla ilgili olabilir. Eşyanızı Steam sunucularını düzelttikten bir süre sonra çekmeyi deneyin. Ayrıca herhangi bir Steam Koruma sınırlamanız olmaması da önemlidir. Bunun sebepleri şurada listelenmiştir – 
https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.
',

    'question_11' => 'Müşteri desteğiniz var mı?',
    'answer_11' => 'Evet, kesinlikle. Tek yapmanız gereken bizimle uygun olduğunuz zamanlarda [support@csgotower.net|mailto:support@csgotower.net] iletişime geçmek. Size 24 saat içinde geri döneceğiz.',
];