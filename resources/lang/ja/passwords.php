<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードは6文字以上である必要があります。また、パスワード再確認で入力したものと一致する必要があります。',
    'reset' => 'パスワードがリセットされました。',
    'sent' => 'パスワードリセット用のリンクをメールで送信しました。',
    'token' => 'このパスワードリセット要求は無効です。',
    'user' => 'このメールアドレスのユーザーをデータベース上で発見できませんでした。',

];
