<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'このログイン情報は、当サイトのアカウントデータベースと一致しませんでした。',
    'throttle' => 'ログイン試行回数が制限を超えました。 :seconds 秒後にもう一度ログインし直してみてください。',

];
