<?php

return [
    'cases'         =>'Cases',
    'collections'   =>'collections',
    'exclusive'     =>'exclusive',
    'giveaway'      => 'LOTS',
    'top_trop'      => 'MEILLEURS DROPS',
    'twitter'       => 'TWITTER',

    'new_format_cs' => 'Le Nouveau Format',
    'top_in_earnings' => 'TOP in earnings',
    'new_format_cs_go' => 'LE NOUVEAU FORMAT DE BOITES CS:GO',
    'uniq_format' => 'Nouveau format d’ouverture de boite CS:GO',
    'uniq_format_text' => 'Votre choix, Votre skin!',
    'respondent' => 'Réactif',
    'respondent_text' => 'l’équipe d’assistance résout tous les problèmes.',
    'auto_system' => 'Vous serez agréablement surpris de la vitesse de transfert sur Steam par nos bots. Le transfert de skins est totalement automatisé et exécuté par des bots très rapides qui ont été spécifiquement optimisés pour être utilisés avec Steam. Tous les objets qui sont dans votre inventaire peuvent vous être instantanément envoyés. Il n’y a aucun délai pour l’envoi de skins sur notre site, contrairement à d’autres sites.',
    'fast_text' => 'Rapide',
    'my_benefit' => 'Profitable',
    'my_benefit_text' => 'Drops de boites cs go meilleurs que la normale et que sur les autres sites d’ouverture de boites.',
    'card_demo'     => 'Ticket Démo',
    'tutor_auth'    => 'Jouer à un Ticket Démo Gratuit',
    'tutor_auth_2'  => 'Découvrez le Meilleur Format d’Ouverture de Boites CS:GO en Action!',
    'tutor_teacher1_info' => 'Pour gagner, effacez 3 des 9 boites identiques avec un mouvement de la souris',
    'tutor_teacher1_okay' => 'Compris, je vais essayer!',
    'choose_try' => '+1 ESSAI',
    'choose_get' => 'Vous pouvez obtenir',
    'choose_info' => 'Tentez votre chance ou profitez juste d’un gain assuré?',
    'tutor_teacher2_okay' => 'Prêt à Choisir',
    'tutor_over' => 'L’entraînement est terminé!',
    'tutor_example' => 'C’est un exemple de drop:',
    'tutor_complete' => 'Terminer l’Entrainement!',
    'tutor_complete_info' => 'Il est temps de tester votre intuition et de gagner une des meilleurs skins sur csgotower.net',
    'tutor_demo_info2' => 'L’objet pour le ticket d’entrainement n’est PAS DELIVRE!',


    'caserarity'    => 'Boites rares',
    'rangcase'      => 'Boites de Niveau',
    'assemblies'    => 'Nos Assemblées',
    'сollections'   => 'Collections',
    'standart'      => 'Boites Standard',
    'partner'       => 'A VENIR',

    'tickets'       => 'TICKETS',
    'in_profit'     => 'PROFIT',
    'user_rating'   => 'Note des Utilisateurs',
    'top_24_hours'  => 'Meilleurs Utilisateurs lors des dernières 24 Heures',

    'congratulate'=>'Félicitations!',
    'card.congratulation' => 'Félicitations!',
    'card.thisisyour' => 'C’est votre',
    'card.take.1' => 'Sélectionnez un sujet pour le',
    'card.take.2' => 'profil',
    'card.take.3' => 'en une heure',
    'card.tryanother' => 'Essayer plus',
    'card.sellfor' => 'Vendre pour',
    'card.take' => 'Prendre',
    'card.tohome' => 'Retourner à l’Accueil',
    'card.auth.1' => 'Veuillez',
    'card.auth.2' => 'Vous Connecter',
    'open_ticket' => 'Ouvrir le Ticket pour ',
    'ticket_contains' => 'Contenu du Ticket',

    //rules
    'rules_first'     =>'REGLES DE BASE DE csgotower:',
    'rules_second'    =>'Ouvrez 3 fois le même objet et obtenez-le!',
    'rules_third'     =>'3 tentatives plus 1 autre en option',
    'rules_fourth'    =>'Un lot garanti pour tout le monde!',
    'notice_first'     =>'Pendant que vous vous entraînez sur un ticket',
    'notice_second'     =>'VOUS N’OBTENEZ PAS l’objet mais vous pourrez évaluer le gameplay, il vous suffit de',
    'notice_third'     =>'vous connecter',

    //faq => privilege
    'about_project' =>  'A Propos du Projet et des Garanties',
    'privilege_title' =>  'Pourquoi ouvrir des boites/des tickets avec nous?',

    'gold_title_1' =>  'Système Totalement Automatisé. ',
    'warranty_1'  =>  'L’échange offert par les bots se fait en quelques secondes – c’est important que vous soyez rapides et préparés, étant donné que les meilleures choses et les plus simples gagnent sans problème.',

    'profit'  =>  'PROFITABLE',
    'gold_title_2' =>  'Les drops sur Topscard garantissent une meilleure ouverture de boîtes in-game. ',
    'warranty_2'  =>  'Non seulement vous pouvez obtenir le meilleur de votre ticket et avec un pack de skin qui contient un bon drop! Lisez les ',
    'warranty_2_link'  =>  'avis des utilisateurs',
    'warranty_2_2'  =>  ' pour vous assurer que nous offrons souvent des objets StatTrak.',

    'reliably'  =>  'FIABLE',
    'gold_title_3' =>  'Nous n’avons rien à cacher – vous avez toutes les statistiques devant les yeux. ',
    'warranty_3'  =>  'Cliquez sur un article du bandeau et obtenez des informations détaillées sur l’utilisateur : son surnom, une référence au compte Steam et l’historique de ses drops. ',
    'warranty_3_link'  =>  'Les meilleurs utilisateurs',
    'warranty_3_2'  =>  ' sont toujours disponibles via le lien dans l’entête. En bas de la page, vous pouvez également voir les statistiques globales csgotower.',

    'gold_title_4' =>  'Vous obtenez toujours votre skin! ',
    'warranty_4'  =>  'Si vous n’avez pas pris une chose ou que vous avez décidé de le vendre, vous recevrez la valeur équivalente de l’objet. Vous pouvez toujours aller sur votre profil et revendre l’article.',

    //faq => instruction
    'instruction_title' =>  'Comment obtenir le skin?',
    'step' =>  'Etape',
    'instruction_1' =>  'Identifiez-vous sur Steam, allez sur votre compte en cliquant sur votre surnom',

    'instruction_2' =>  'Vous avez joué votre premier ticket d’entraînement sur ',
    'instruction_2_link' =>  'la page d’accueil',

    'instruction_3' =>  'Sur la ',
    'instruction_3_link' =>  'page Compte',
    'instruction_3_2' =>  ', entrez le Lien Commercial ',
    'instruction_3_link_2' =>  'Qu’est-ce que c’est?',
    'instruction_3_3' =>  ' — Vous pouvez donc obtenir instantanément les skins',

    'instruction_4' =>  ' sur le site d’une manière pratique',
    'instruction_4_link' =>  'Rechargez votre compte',

    'instruction_5' =>  'Ouvrez 3 des 9 parties identiques sur le ticket et obtenez le meilleur objet de la boite ! Dans le cas où vous auriez un problème de boite, seulement sur ',
    'instruction_5_2' =>  'csgotower.net, vous pouvez ouvrir une boite avec un pack de skins garanti.',
    'instruction_5_3' =>  'Sur csgotower.net, ce n’est pas seulement décidé sur la chance mais aussi sur votre instinct.',

    //faq answers

    'faq_q1'  =>  'Je ne peux pas envoyer un objet remporté sur Steam!',
    'faq_a1'  =>  'Important! Tout d’abord, avant d’ouvrir une boite, assurez-vous que votre compte Steam peut recevoir les objets. Allez sur votre compte Steam  et rendez-vous sur l’inventaire, il pourrait y avoir une notification signifiant que votre compte est gelé. Un compte Steam peut être gelé pendant 7 jours après un changement de mot de passe, de mail, de Steam Guard, de connexion 2FA… Les comptes Steam gelés ne peuvent pas recevoir d’objets, nous ne serons donc pas en mesure de vous envoyer l’objet que vous avez gagné. C’est un point important, donc nous vous avons prévenu plusieurs fois à ce sujet pour les prépaiements et nous avons fourni son lien commercial.',

    'faq_q2'  =>  'Je viens de recharger le compte et il n’y a pas d’argent sur mon solde',
    'faq_a2'  =>  'Pour procéder à des paiements, le système de paiement G2A Pay csgotower est utilisé. Tous les paiements sont effectués avec ce système de paiement. 99% de toutes les limites et les échecs sont associés aux restrictions de G2A Pay. Le paiement est généralement effectué en 5-10 minutes. Il peut arriver que le paiement ait un retard d’une heure. Vous pouvez contacter notre assistance pour plus d’informations sur votre paiement.',

    'faq_q3'  =>  'J’ai ouvert mon ticket, où est mon prix?',
    'faq_a3'  =>  'Vos gains sont stockés dans votre inventaire. Vous pouvez accéder à votre inventaire en cliquant sur le prix ou sur son propre avatar en haut à droite du site.',

    'faq_q4'  =>  'Pourquoi mes gains ont-ils été vendus?',
    'faq_a4'  =>  'Les skins dans votre inventaire seront automatiquement vendus au bout d’une heure. Cette mesure préventive permet de ne pas surcharger vos bots et permet de toujours garder une boite disponible pour vos skins.',

    'faq_q5'  =>  'Je ne peux pas me connecter sur Steam ou envoyer l’objet',
    'faq_a5'  =>  'La communauté  utilise Steam pour authentifier les utilisateurs et envoyer les skins. Il arrive que Steam connaissent des difficultés pour fonctionner ou qu’il ne puisse même pas répondre à nos requêtes.',

    'faq_attention'  =>  'Attention!',
    'faq_attention_2'  =>  'Si vous ne trouvez pas la réponse à vos questions dans cette section, contactez-nous par mail sur ',
    'faq_attention_3'  =>  ', ou envoyez-nous un message ',
    'faq_attention_link'  =>  'sur la communauté Facebook!',


    'login'         => '通過蒸汽連接',
    'login_mobile' => '签到',
    'login_case' => '签到',
    'logout'        =>'登出',
    'header_support' =>'支持',
    'menu'          => 'Menu',
    'guarantees'    => 'Garanties',
    'faq'           =>'常见问题',
    'reviews'       =>'Avis',
    'shop_cases'    =>'Drop meilleur que celui de Gabe',
    'lucky'         =>'Top des chanceux',
    'bots_trades'   =>'BOT\'S LIVE TRADES',
    'cases_opened'  =>'游戏',
    'users'         =>'UTILISATEURS',

    'balance_fill'  =>'添加余额',


    'faq_how'       =>'How does it works?',

    'faq_step'      =>'Autorisez-vous via Steam et accédez à votre page de compte',
    'faq_step2'     =>'Entrez le lien Trade - maintenant vous pouvez obtenir instantanément les objets gagnés',
    'faq_step3'     =>'Rechargez votre compte sur le site de toute manière commode',
    'faq_step4'     =>'Ouvrez des cases aux meilleurs prix du monde!',

    'q1'            =>'I opened the case, but the object does not come, what\'s happening?!',
    'ans1'          =>'Open your profile and click the link "To take" on the object you are interested in.',
    'q2'            =>'When I want to take object off the Bot , I receive a mistake: " Unfortunately, our bot couldn\'t send your object..."',
    'ans2'          => 'Bot can\'t send you the object in the following:
                    You have the exchange blocked ( you entered from the new appliance, changed the password or e-mail recently, changed the security settings Steam Guard, have the VAC-ban, etc. More..... in order to avoid such problems in future, always recommend to check your trade status at special page csho.tm.
                    You have inventory hidden in the privacy settings Steam
                    Check your trade link in the profile and in the settings Steam - they have to coincide
                    Bot has the problems with the Steam connection, it may happens andd very often, the problem is in the Steam, it\'s not our problem. Also bot can be banned, in this case you have to adress to technical support (support@csgotower.net)
                    If you don\'t take your object during one hour, the whole price of this object acording to it\'s price at the Steam market.',
    'q3'        =>'I can\'t open the cases, it is indicated the mistake " At the moment bot doesnt have the object for the case choosen. Try to do it later."',
    'ans3'      =>'For the real random, cases/categories must always contain ALL the objects in it, which are listed at the page of the case/categor. Bots don\'t have enough time to buy all necessary things in the time needed or some necessary objects for this case are absent at the market now.',
    'q4'        =>'Filled the account, but money doesn\'t appear. What to do?',
    'ans4'      =>'Money can appear in some time - wait an hour. If money not appeared more than in an hour write to us (support@csgotower.net),indicating the whole information of the payment.',
    'q5'        =>'I have money at Steam account but they are not indicated here. Why?',
    'ans5'      =>'Steam account money are not valid here at this site. To fill the balance it is needed separately.',
    'q6'        =>'Can\'t enter the site through Steam, the page with the cases is not loading, there is an advertisement in the centre of the page , and in general site is not working good, what to do?',
    'ans6'      =>'May be the problem is in antivirus, which can block the conection with the site, or the problem is in a browser, which shows the advertisement also can block the connection with the site.',
    'steam_profile' =>'Steam 资料',

    'settings'      =>'Settings',
    'transe_url_enter'  =>'Entrez le Trade-Url',
    'field_bottom'      =>'Le champ de saisie est ci-dessous',
    'add_credit'        =>'Rechargez le solde',
    'payment_methods'   =>'VISA, MASTERCARD, PAYPAL ETC.',
    'win'       =>'Gagnez',

    'best_cases'=>'Ouvrez des cases à des conditions favorables',

    'explore_url'   =>'Explore Trade-URL',
    'your_items'    =>'VOS OBJETS',

    'accepted'      =>'Reçu',
    'sold'          =>'Vendu',
    'accept'        =>'Obtenir',
    'sell'          =>'Vendre',

    'warn1'         =>'Soyez vigilant!',
    'recomm1'       =>'Toutes les questions ne peuvent être résolues que via support@csgotower.net
mail. Ignorez tous ceux qui vous écrivent dans les réseaux sociaux en se présentant comme l\'administrateur du site – ce sont des escrocs.',
    'warn2'     =>'Avant d\'ouvrir des cases',
    'recomm2'   =>'Assurez-vous de vérifier les échanges sur votre compte Steam, sinon notre robot ne pourra pas  vous envoyer vos objets. Ouvrez votre inventaire, assurez-vous qu\'il n\'y a pas d\'interdiction 7 jours après le changement du mot de passe.',
    'warn3'     =>'FAQ et d\'autres questions',
    'recomm3'   =>[
                    'Consultez la page',
                    'FAQ',
                    ', où tous les problèmes qui peuvent survenir sont décrits en détail.'
                ],
    'warn4'     =>'Retirez le drop',
    'recomm4'   =>'La pleine valeur de l\'objet retournera sur votre compte si vous ne l\'avez pas retiré pendant 30 minutes.',
    'sending'   =>'Envoyons',
    'items'     =>'Choses',
    'case_contains' =>'箱子包含',
    'open_case'     =>'Ouvrir un case',
    'open_for'      =>'Open case for',
    'opening'       =>'Opening',
    'unavailable'   =>'Temporarily unavailable',
    'no_money'      =>'There is not enough money to open case. Fill up on',
    'your_drop'     =>'Your win',
    'try_again'     =>'Réessayer',
    'sell_for'      =>'Sell for',
    'congratulations'=>'Congratulations! The item can be accepted up within an hour.',
    'home'      =>'家',
    'reliable'  =>'Fiable',
    'qualitatively' =>'QUALITATIF',
    'fast'          =>'RAPIDE',
    'reliable_text' => 'Tous vos skins sont stockés en toute sécurité dans votre inventaire. À tout moment dans l’heure après qu’une boite soit ouverte, vous pouvez aller sur votre profil et envoyer des skins sur votre compte Steam. Si le skin n’est pas envoyé dans l’heure, il sera vendu automatiquement au prix du marché et alimentera votre compte.',
    'reliable_mess' =>'In case you have not taken your items at once, you can go to the profile and resend it. For example, if you open 10 or 15 cases in a row and do not accept the items at once, the bot will send them to you again. Just go to your profile, and send the items again.',
    'qualitatively_mess'=>'We do not lie, and we can prove it. Clicking on the item in the “live-drops” feed you will see the user\'s profile, with a history of items that have fallen to him. What is it for? You may have seen a similar “live-drops” feed on other sites where different “Azimov for 50$” fall out, and then you buy cases and weapons dropped out for 0.5-1 dollars.',
    'fast_mess'     =>'Everything is fully automated. Sending items via bots greatly simplifies the whole process and a special method of procurement guarantees getting of any item, whether it is “COVERT” or “MIL-SPEC” quality.',
    'server_error'  =>'Server\'s error. Try again later!',
    'profile_sttings_success'   =>'Trade URL successfully updated!',
    'profile_sttings_error'     =>'Saving Error!',
    'success'       =>'成功',
    'error'         =>'錯誤',
    'drops'         =>'Tous les drops',
    'loading'       =>'Loading',
    'enter_your'    =>'Entrez votre Trade-Url',
    'pay_attention' =>'注意！ 付款可能會延遲5-10分鐘！',
    'invalid_trade_url' =>'交易網址格式無效',
    'alien_trade_url'=>'That is not your trade URL',
    'second_ago'        =>'第二天',
    'no_items'          =>'PAS D’OBJETS ENCORE. OUVREZ VOTRE',
    'first_case'        =>'PREMIER CASE!',
    'contact'=>'請聯繫',
    'payment_error'=>'付款錯誤',
    'payment_success'=>'謝謝！ 您的付款已成功完成。 請注意，付款可能會延遲5-10分鐘！',
    'receive'=>'receive',
    'received'=>'received',
    'item'=>'item',
    'bot'=>'bot',
    'user'=>'UTILISATEUR',
    'time'=>'time',
    'trade\'s'=>'trade',
    'send_item'=>'Send item to your steam account you can in',

    'pay_skins_message'=>'Be aware that when you deposit money using skins, your balance will receive the sum that was mentioned in the order.',
    
    //marks
    'sale'      =>'sale',
    'popularly' =>'popular',
    'novelty'   =>'new',
    'best-price'=>'best price',


    //profile
    'breadcrumbs_users' => '用戶',
    'balance_up' => '供應餘額',
    'profile_steam' => '蒸汽配置文件',
    'save_profile_link' => '节省',
    'initial_settings' => '檔案設定',
    'link_exchange' => '交換你的鏈接',
    'enter_below' => 'Entrez ci-dessous.',
    'prfole_what' => 'Qu’est-ce que c’est?',
    'refill_balance' => 'Recharger votre Solde',
    'all_electronic_money' => 'Toutes sortes de paiements électroniques',
    'winer' => 'Gagnez!',
    'get_best_drop' => 'Obtenez le meilleur drop',

    'copy_steam_link' => '與Steam網站複製',

    //profile => info block
    'header_pointer' => 'Faites Attention!',

    'bad_head' => 'Prenez garde aux scams!',
    'bad_text' => 'Nous ne serons jamais ajoutés à vos Amis sur Steam pour proposer des échanges – ce n’est qu’un mensonge ! Soyez prudents et vous ne vous ferez jamais voler d’objets.',

    'good_head' => 'Assistance et FAQ',
    'good_text' => 'Spécialement pour vous, nous avons collecté toutes les questions basiques sur nos services dans notre ',
    'good_text_link' => 'FAQ',
    'good_text_2' => ', et dans les cas les plus obscurs, écrivez un message sur notre ',
    'good_text_link_2' => 'Communauté Facebook.',

    'vk_head' => 'Concours et loteries de skins',
    'vk_text' => 'Nous organisons des concours et des loteries en permanence et nous distribuons des codes promos sans aucune raison particulière pour ajouter des fonds à notre ',
    'vk_text_link' => 'communauté Facebook',
    'vk_text_2' => ', rejoignez-nous!',

    'items_incase_text' => 'Il n’y a actuellement aucun objet. Ouvrez votre ',
    'items_incase_link' => 'premier ticket!',

    'promocodes' => '促销代码',
    'promocodes_text' => 'Sur le site Internet et sur ',
    'promocodes_link' => 'la Communauté Facebook',
    'promocodes_text_2' => ', des codes promotionnels apparaissent constamment. Entrez-les parmi les premiers et obtenez un bonus sur votre solde!',
    'promocodes_button' => 'Effectué!',


    //Agreements
    'terms_p1'  =>  'Aucune personne de moins de dix-huit (18) ans ne peut utiliser ce service, même en cas de consentement d’un parent ou d’un tuteur pour utiliser le Service.',
    'terms_p1_br1'  =>  'Vous avez besoin d’un navigateur Internet supporté pur accéder au Service. Vous reconnaissez et acceptez que DatDrop.com peut cesser à tout moment de supporter un navigateur Internet donné et pour continuer à utiliser le service, vous devrez téléchargé un navigateur Internet supporté. Vous convenez et acceptez également que les performances du Service dépendent des performances de votre ordinateur et de votre connexion Internet.',
    'terms_p1_br2'  =>  'Vous acceptez de vous enregistrer sur les Services via votre compte Steam fourni par Valve. Vous êtes le seul responsable de votre compte et de votre mot de passe et pour veiller à ce que votre mot de passe reste confidentiel. Vous êtes également le seul responsable pour restreindre l’accès à votre compte.',
    'terms_p1_br3'  =>  'Vous acceptez être responsable de toute activité qui a lieu sur votre compte ou suite à l’utilisation de votre mot de passe par vous-même ou d’autres personnes. Si vous pensez qu’un personne tierce a accès à votre mot de passe, utilisez l’option de réinitialisation de mot de passe du Service et obtenez un nouveau mot de passe aussitôt que possible. En toutes circonstances, vous acceptez de ne pas laisser une personne tierce utiliser le Service ou y accéder.',

    'terms_p2_ul'  =>  'Comme condition à votre utilisation du Service, vous acceptez de ne pas:',
    'terms_p2_l1'  =>  'Usurper l’identité ou prétendre faussement votre association avec une personne ou une entité;',
    'terms_p2_l2'  =>  'Accéder, falsifier ou utiliser toute zone non publique du Service ou du système informatique csgotower.net;',
    'terms_p2_l3'  =>  'Essayer de sonder, de scanner ou de tester la vulnérabilité du Service ou de tous les systèmes ou réseaux relatifs ou de toute brèche de sécurité ou mesure d’identification relatives au Service et à de tels systèmes ou réseaux;',
    'terms_p2_l4'  =>  'Essayer de déchiffrer, de décompiler, de désassembler ou d’inverser l’ingénierie ou autrement d’enquêter sur tout logiciel ou composant utilisé pour fournir le Service;',
    'terms_p2_l5'  =>  'Nuire ou menacer de nuire à d’autres utilisateurs d’une manière ou d’une autre ou perturber ou essayer de perturber l’accès de tout utilisateur, hébergeur ou réseaux, ce qui comprend sans aucune limitation, l’envoi d’un virus, la surcharge, la saturation, le spamming ou l’envoi massif de mail envers le Service;',
    'terms_p2_l6'  =>  'Fournir des informations de paiement appartenant à une personne tierce;',
    'terms_p2_l7'  =>  'Utiliser le Service de manière abusive contraire à son utilisation prévue, à la politique et aux instructions de csgotower.net et à toute loi applicable;',
    'terms_p2_l8'  =>  'Extraire systématiquement des données ou d’autres contenus du Service pour créer ou compiler, directement ou indirectement, lors de téléchargements simples ou multiples, une collection, une compilation, une base de données, un répertoire ou toute chose similaire, que ce soit manuellement ou en utilisant des bots, des robots d’indexation ou de recherche ou tout autre moyen;',
    'terms_p2_l9'  =>  'Utiliser le Service d’une manière contraire aux termes et aux conditions sous lesquelles les parties tierces fournissent l’équipement et la technologie nécessaires au fonctionnement du Service, tels que G2A ou Valve;',
    'terms_p2_l10'  =>  'Porter atteinte à la propriété intellectuelle des parties tierces en utilisant le Service, notamment mais pas seulement en rendant des objets virtuels disponibles en utilisant le Service;',
    'terms_p2_l11'  =>  'Utiliser, promouvoir, faire un lien ou donner accès à des matériaux jugés exclusivement par csgotower.net comme étant insultant ou portant atteinte à la réputation de csgotower.net, notamment, mais pas seulement, des contenus illégaux, pornographiques ou jugés insultants ou injurieux envers csgotower.net et/ou au Service (tels que des sites de Warez, des bots IRC et des sites bittorent).',
    'terms_p2_l12'  =>  'Le retrait des skins sera disponible après que vous ayez rechargé votre solde sur le site du montant minimal de 2$ par transaction',

    'terms_p3'  => 'Résiliation',

    'terms_p4'  => 'Nous pouvons résilier ou suspendre l’accès à nos services immédiatement, sans aucun préavis ou dédommagement, pour quelque raison que ce soit, notamment sans aucune limitation si vous avez enfreint les Termes.',
    'terms_p4_br1'  => 'Toutes les ressources des Termes, qui par leur nature devrait perdurer lors de la résiliation, perdureront lors de la résiliation, dont, sans aucune limitation, les ressources du propriétaire, les garanties de non responsabilités, les indemnités et les limitations de responsabilité.',
    'terms_p4_br2'  => 'Nous pouvons résilier ou suspendre votre compte immédiatement, sans aucun préavis ou dédommagement, pour quelque raison que ce soit, notamment sans aucune limitation si vous avez enfreint les Termes.',
    'terms_p4_br3'  => 'Lors de la résiliation, votre droit d’utiliser le Service cessera immédiatement. Si vous souhaitez résilier votre compte, vous devez simplement arrêter d’utiliser le Service.',
    'terms_p4_br4'  => 'Toutes les ressources des Termes, qui par leur nature devrait perdurer lors de la résiliation, perdureront lors de la résiliation, dont, sans aucune limitation, les ressources du propriétaire, les garanties de non responsabilités, les indemnités et les limitations de responsabilité.',

    'terms_p5'  => 'Liens Vers d’Autres Sites Internet',

    'terms_p6'  => 'Notre Service peut contenir des liens vers des sites Internet ou des services de parties tierces qui ne sont pas possédés ou contrôlés par csgotower.net',
    'terms_p6_br1'  => 'csgotower.net n’a aucun contrôle sur eux et n’assume aucune responsabilité sur le contenu, les politiques de confidentialité ou les pratiques de tout site Internet ou services de parties tierces.
    En outre, vous reconnaissez et acceptez que csgotower.net ne peut pas être tenu responsable, directement ou indirectement, pour tout dégâts occasionnés ou sensées être causées par ou en connexion avec l’utilisation ou dépendante de tels contenus, biens ou services disponibles sur le site ou à travers de tels sites ou services Internet.',
    'terms_p6_br2'  => 'Nous vous conseillons fortement de lire les termes et les conditions ainsi que les politiques de confidentialité de tout site Internet de parties tierces sur lesquels vous pouvez vous rendre.',

    'terms_p7'  => 'Aucune Garantie ',

    'terms_p8'  => 'Ce site Internet est fourni « en tant que » sans représentation ou garantie, explicite ou implicite. csgotower. Com ne présente aucune représentation ou garantie en relation avec le site Internet ou les informations ou les matériaux fournis sur le Site Internet. Sans aucun préjudice aux dispositions générales du paragraphe précédent, csgotower.net ne garantit pas que : ce site Internet sera constamment disponible ou disponible du tout ; ou les informations de ce site Internet sont complètes, authentiques, précises ou non trompeuses. Aucun contenu de ce site constitue ou est sensé constituer des conseils d’aucune sorte. Si vous avez besoin de conseils par rapport à tous problèmes légaux, financier ou médicaux, vous devriez consulter un professionnel approprié.',

    'terms_p9'  => 'Portefeuille csgotower.net',

    'terms_p10'  => 'En rechargeant de l’argent sur votre portefeuille csgotower.net, vous acceptez que les crédits reçus (marquées par le signe $) ne vaillent pas la somme d’argent réelle et ne peuvent pas être remboursés',

    'terms_p11'  => 'Conditions de vente automatique',

    'terms_p12'  => 'Afin d’éviter les délais excessifs pour les skins dans l’inventaire d’un simple utilisateur – les skins remportés sont vendus au bout d’une heure, la valeur complète des skins sont reversés sur le solde de l’utilisateur.',

    'terms_p13'  => 'Autres Termes et Conditions ; ALUs',

    'terms_p14'  => 'Quand vous utilisez les services G2A Pay, fournis par G2A.COM Limited (ci-après désignés sous le nom de « fournisseur de services G2A Pay ») pour faire un achat sur le site Internet, la responsabilité sur votre achat sera ensuite transférée sera d’abord transféré à G2A.COM Limited avant de vous être envoyé. Le fournisseur de services G2A Pay assume la responsabilité primaire, avec notre assistance, pour le paiement et le paiement relatif à l’assistance client. Les termes entre le fournisseur de services G2A Pay et les clients qui utilisent les services de G2A Pay sont gouvernés par des accords séparés et ils ne sont pas sujets au Termes de ce site Internet.',

    'terms_p15'  => 'Par rapport aux clients qui font des achats sur la caisse du fournisseur de services G2A Pay, (i) la Politique de Confidentialité du fournisseur de services G2A Pay s’appliquera à tous les paiements et elles doivent être révisées avant de faire le moindre achat et (ii) la Politique de Remboursement du fournisseur de services G2A Pay s’appliquera à tous les paiements sauf en cas d’avis contraire fourni à l’avance à l’acheteur par le fournisseur correspondant. De plus, l’achat de certains produits pourrait également demander aux acheteurs d’accepter un ou plusieurs Accords de Licence d’Utilisateurs (ou « ALUs ») qui peut comprendre plusieurs termes supplémentaires établis par le fournisseur de produits plutôt que par nous ou par le fournisseur de services G2A Pay. Vous serez liés par tout ALUs que vous acceptez. Nous et/ou les entités qui vendent des produits sur notre site Internet en utilisant les services G2A Pay sont principalement responsables pour la garantie, la maintenance et les services d’assistance technique et commerciale pour ces Produits.',

    'terms_p16'  => 'Nous et/ou les entités qui vendent des produits sur notre site Internet sont principalement responsables des utilisateurs en ce qui concerne les responsabilités relatives à l’execution des commandes et les ALUs conclus par l’Utilisateur Final. Les fournisseurs de services G2A Pay sont principalement responsables pour faciliter votre paiement.',

    'terms_p17'  => 'Vous êtes responsables pour tous les frais, les taxes et les autres coûts associés à l’achat et la livraison de vos objets conséquents aux charges imposés par votre relation avec les fournisseurs de services de paiement ou les droits ou les taxes imposées par les douanes de votre pays ou un autre organisme de réglementation.',

    'terms_p18'  => 'Pour les requêtes et les litiges avec le service clients, veuillez nous contacter par mail à',

    'terms_p19'  => 'Les questions relatives aux paiements faits par le fournisseur de services G2A Pay doivent être adressées à ',

    'terms_p20'  => 'Quand c’est possible, nous travaillerons avec vous et/ou tout utilisateur sur notre site Internet pour résoudre tout conflits découlant de votre achat.',

    //Trade-Modal
    'modal_trade_text'  =>  'REFRESH THE TRADE LINK',
    'trade_placeholder'  =>  'Obtenir votre Trade-URL',
    'trade_button_save'  =>  'Sauver',
    'trade_footer_text'  =>  'Don\'t know where get the trade link? Just ',
    'trade_footer_click'  =>  'click here',



    //market
    'market' => '市场',


    //top_winners
    'top_winners' => '排名靠前的赢家',

    //provably_fair
    'provably_fair' => '公平公正',


    //index
    'ORIGINAL CASES' => '原始箱',
    'credits' => '积分',
    'you_may_win' => '您可以赢',
    'SPECIAL CASES' => '特别箱',
    'show_all' => '显示所有',
    'new_way' => '简单的方法来皮肤！',

    //profile
    'personal_area' => '个人区域',
    'activate_promocode' => '激活促销代码',
    'enter_promocode_here'=>'在这里输入您的促销代码',
    'activate'=>'激活',
    'payment'=>'付款',
    'payment_history'=>'上次付款：付款历史',
    'date'=>'日期 ',
    'details'=>'详情状况',
    'attention_five_min_payment'=>'注意！付款可能会延迟5-10分钟！',
    'example'=>'举例 ',
    'calculator'=>'计算器 ',
    'your_link_exchange' => '您的 交易网址 (从STEAM网站复制)',
    'save'      =>'节省',
    'profile'=>'我的简介：',
    'add_balance'   =>'添加余额',
    'enter_amount'  =>'输入数额',
    'pay'           =>' 支付',
    'balance'       =>'余额',
    'live_drop'     =>'现时获利',
    'online'        =>'在线',
    'contacts'      =>'联系方式和公司信息 ',
    'terms'         =>'条款和细则',
    'support'=>'支持：',

    //affiliates
    'affiliates'    => '加盟',
    'partner_head'=>'合作伙伴',
    'your_partner_link'=>'您的合作链接',
    'level'=>'等级',
    'referrals'=>' 推介',
    'to_next_level'=>' 达到下一级',
    'percentage'=>' 百分比 ',
    'referral_receive'=>' 推介收到 ',
    'referral_deposited'=>'推介存入 ',
    'earnings'=>'收入',
    'best_affiliates'=>'最佳加盟',
    'csgotower'=>'在csgotower，您将通过推介获得最大力度的返现。',
    'on_start'=>'开始为 %',
    'percent_on_market'=>'在我们合作的初期，您将获得市场上最高的回报%。',
    'sponsorship'=>'赞助',
    'youtube_or_twitch'=>'您是否有Youtube或Twitch渠道？通过media@csgotower.net联系我们，讨论我们的合作。',
    'partner_levels'=>'合作等级',
    'to_deposit'=>'存入额的',

    'paypal_attention' => '注意！ PayPal用户：PayPal付款选项不可用。但您仍然可以使用PayPal进行存款。您需要注册一个G2A帐户，然后使用PayPay系统添加余额。在此之后，您可以使用您的G2A电子钱包在csgotower余额上添加资金 Pay',
    'hash_number'=>'哈希',
    'win_number'=>'中奖号码',
    'user_seed'=>'用户种子',
    'server_seed'=>'服务器种子',
    'next_ticket'=>'下一个开箱',
    'previous_ticket'=>'上一个开箱',
    'close_letter'=>'關',
    'verification'=>'验证',
    'wagered'=>'已下注',
    'games'=>'游戏',
    'easy'=>'简单',
    'medium'=>'适中',
    'hard'=>'难',
    'play_demo'=>'玩游戏演示',
    'sort_item_by_price'=>'物品价格排序',
    'sort_item_by_quality'=>'质量',
    'price_low'=>'价格:低到高',
    'price_high'=>'价格:高到低',
    'enter_item_name'=>'搜索皮肤',
    'select_all_view'=>'选择所有视图',
    'refresh'=>'刷新',
    'withdrawable'=>'可提现',
    'selected_items'=>'选择的物品',
    'item_value'=>'物品价值',
    'update_steam_trade_url'=>'更新steam trade网址',
    'price_range'=>'物品价格范围',
    'win_up_to'=>'赢取可达',
    'check_winning_number'=>'查看中奖号码',
    'try_other_cases'=>'前往其他箱子',
    'cases_to_open'=>'要开的箱子',
    'credits_case'=>'积分箱',
    'win_chance'=>'赢得机会',
    'multiplier'=>'乘数',
    'withdraw'=>'提现',
    'games_history' => '游戏历史',
    'withdraw_history' => '提现故事',

    'text_provably_fair_1'=>'“可靠公平”遊戲的機制允許您控制分配骰子的算法的公平競爭和公正性。 為了執行檢查，您可以使用我們自己的在線計算器或任何其他php測試儀，例如',
    'text_provably_fair_2'=>'這將有助於計算特定輪次的哈希值。',
    'text_provably_fair_3'=>'“可靠公平”遊戲的機制如何運作？',
    'text_provably_fair_4'=>'在每一輪遊戲之前，根據您的數據（客戶端種子）和服務器的數據（服務器種子）生成唯一的種子。',
    'text_provably_fair_5'=>'發射後，使用256位加密算法生成編碼獲勝號碼的哈希值。',
    'text_provably_fair_6'=>'回合結束時，您可以獲得獲勝號碼。 然後，擁有所有數據，您可以自己編碼種子和獲勝號碼，以計算哈希值，並將其與發布之前收到的數據進行比較。',


    'start_game'=>'開始遊戲',


    'index_make_profit' => '你可以用这种方式获利！',
    'index_span1' => '开箱并提现你真正想要的皮肤。赢得高达1百万美元的奖励！',
    'index_span2' => '可以获得所有皮肤',
    'index_start' => '开始',

    'profile_payment_status' => '状态',
    'day_top' => '昨天大奖排行榜',
    'week_top' => '上周大奖排行榜',
    'month_top' => '上月大奖排行榜',
    'calculate' => '计算',
    'link_case' => '打开',
    'modal_fair' => '公平公正',
    'modal_fair_next' => '下一个游戏',
    'modal_fair_new_seeds' => '新种子',
    'modal_fair_item' => '物品',
    'modal_fair_winrange' => '获奖号码范围',
    'case_no_money' => '不够',
    'upgrade_crd_in' => '输入数额',
    'upgrade_btn' => '升级资金',
    'upgrade_win_chance' => '获奖几率',
    'upgrade_win_multi' => '乘数',
    'upgrade_fair' => '公平公正',
    'upgrade_deposit' => '存人',
    'upgrade_withdraw' => '提出',
    'upgrade_custom_item' => '定制物品',
    'upgrade_nominal' => '选择标题',



    'modal_trade' => '交易网址',
    'modal_trade_placeholder' => '输入交易网址',
    'modal_trade_link' => '你可以在这里找到:',
    'modal_trade_save' => '保存',

    'modal_withdraw' => '提现物品',
    'modal_withdraw_wait' => '请稍候，正在发送您的交易邀请',
    'modal_withdraw_storage' => '存储',
    'modal_withdraw_open_trade' => '未完结交易',
    'modal_withdraw_sending' => '发出',
    'modal_withdraw_success' => '成功',
    'modal_withdraw_dismiss' => '解雇',

    'market_no_money' => '你的信用不足',

    'open_for_crd' => '打开',
    'roulette_fair' => '轮盘赌',
    'user_seed_fair' =>  '用户种子',
    'server_seed_fair' =>  '服务器种子',
    'prev_game_fair' => '上一场比赛',
    'win_number_fair' => '赢数',
    'hash_fair' => '哈希',
    'tower_take1' => '取',
    'tower_take2' => '学分',
    'profile_last_payment' => '最后付款',


    //tower
    'tower_table_rank'=>'秩',
    'tower_table_player' => '玩家',
    'tower_table_bet' => '投注',
    'tower_table_profit' => '获利',
    'tower_table_step' => '步骤',
    'tower_table_level' => '水平',
    'tower_bet_min' => '最小',
    'tower_bet_max' => '最大',
    'tower_verify' => '验证票的公平性',
    'top_profit' => '获利',
    'tower_table_wagered'  => '下注',
    'min'=>'我',
    'max'=>'最大',
    'heroes'=>'英雄',
    'HALL_OF_HEROES'=>'英雄殿堂',
    'play_demo'=>'玩演示',
    'start_game'=>'开始游戏',


    //market

    'selected_skins'=>'选定的皮肤',
    'remove_all'=>'移除所有',


    //header
    'header_tower'=> '塔',
    'header_market'=>'市场',

    //profile

    'market_stories'=>'市场故事'

];
