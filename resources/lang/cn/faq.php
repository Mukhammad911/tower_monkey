<?php


return [
    'question_1' => '什么是csgotower？',
    'answer_1' => 'csgotower是一个具有各种类型的在线游戏的项目，例如宝箱解决方案，使您能够在CS:GO或Dotau物品中筹集资本。',

    'question_2' => '我如何在你们的网站上开始玩？',
    'answer_2' => '要开始与我们一起玩，您需要通过PayPal、万事达卡、Visa、PaySafeCard等方式，使用G2A PAY以您喜欢的方式向您的账户添加资金。在使用您的Steam帐户授权csgotower之后，您将可以看到您的余额。',

    'question_3' => '积分是什么？',
    'answer_3' => '这是csgotower的通用货币。使用这种货币，您可以购买CS:GO或Dota 2市场中的商品，也可以提现。货币汇率为1个积分 = 1USD。正如您所看到的，一切都非常简单 \:)',

    'question_4' => '我如何能取出我购买的物品？',
    'answer_4' => '您可以在CS:GO页面[http://csgotower.net/csgo]或在[http://csgotower.net/dota2]上为Dota 2执行以下步骤：1.选择您想取出的物品。2.按“取出”，然后在弹出窗口中按“发送”。3.之后，您可以在接下来的30秒内自动将您的物品添加到Steam中。',

    'question_5' => '我如何能够信任你们？',
    'answer_5' => 'csgotower为客户全面开放。开箱后现时获利，实时更新最新获奖者。我们会列出顶级玩家，当然也会使用公平公正的工具，清楚地证明我们无法影响游戏过程，并且一切都是高度透明的。此外，您可以随时查看Youtube上的视频，并了解其他玩家对我们的看法，帮助您做出最终判断 :)',

    'question_6' => '嗯，为什么我该在你们的网站上玩？',
    'answer_6' => 'csgotower的开箱和其他游戏比Valve等网站能盈利更多。在这里，您的投资获得回报的可能性要高得多。',

    'question_7' => '我在什么地方可以获得Trade URL？',
    'answer_7' => '[http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url]是我们的机器人和您的帐户之间进行交换的交易网址。',

    'question_8' => '我已将钱添加到我的帐户，但尚未转移。我该怎么做？',
    'answer_8' => '我们使用G2A Pay来处理您的付款。通常，您的付款大概需要10-15分钟。有时可能会延迟约1小时。您可以联系我们的客户支持或G2A Pay客户支持，以获得有关您的付款的更多信息。您的所有交易及其状态均可在个人数据柜中查看。',

    'question_9' => '我无法在网站上登录我的帐户。  ',
    'answer_9' => '我们使用Steam社区进行帐号授权。有时Steam上有技术问题，无法处理您的请求。通常，Steam需要大约5分钟到几个小时不等的时间才能使服务器恢复工作。',

    'question_10' => '我无法在Steam上获得物品。  ',
    'answer_10' => '您的问题可能与Steam目前有技术问题有关。请在Steam修复其服务器后，尝试取走您的物品。还有一点很重要，请不要对Steam Guard设置限制。原因列在这里 — https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932 。',

    'question_11' => '你们有客户支持吗？',
    'answer_11' => '当然有。请通过[support@csgotower.net|mailto:support@csgotower.net]随时和我们联系，我们会在24小时内给您回复。',
];