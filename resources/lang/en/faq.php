<?php


return [
	'question_1' => 'What is csgotower?',
	'answer_1' => 'csgotower is a project with various types of online games such as, for instance, case solving that enables you to raise your capital in CS:GO and Dota or PUBG items.',

	'question_2' => 'How do I start playing on your website?',
	'answer_2' => 'In order to start playing with us you need to add money to your account using G2A PAY any way you prefer, through Paypal, MasterCard, Visa, and so on. You will gain access to your balance after authorising on csgotower using your Steam account.',

	'question_4' => 'How do I withdraw my purchased items?',
	'answer_4' => 'You can do that on the CS:GO page http://csgotower.net/csgo and on http://csgotower.net/dota2 or http://csgotower.net/pubg for Dota 2 taking the following steps: 1. Select the object you want to withdraw. 2. Press ‘Withdraw’ and then press ‘Send’ in the pop-up window. 3. After this you automatically get your item in Steam within the next 30 seconds.',

	'question_5' => 'Why should I trust you?',
	'answer_5' => 'csgotower is fully open to its clients. There is an open live stream with the latest winners that updates in real time. We list the top players. Besides, you can always check out videos  on Youtube and learn what other players think of us in order to pass your final judgment :)',

	'question_7' => 'Where do I get Trade URL?',
	'answer_7' => ' http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url is your trade url for exchanges between our bots and your account.',

	'question_8' => 'I’ve added the money to my account, but it hasn’t been transferred. What can be done? ',
	'answer_8' => 'We use G2A Pay to process your payments. Usually it takes about 10-15 minutes for your payment to go through. Sometimes it can be delayed for about 1 hour. You can contact our customer support or G2A Pay customer support in order to get more information about your payment. All your transactions and their status are available in the personal cabinet.',

	'question_9' => 'I’m unable to authorise my account on the website. ',
	'answer_9' => 'We use Steam Community for the account authorisation. Sometimes there are technical issues on Steam and it is not able to process your requests. Usually it takes from about 5 minutes to a couple of hours for Steam to put their servers back to work.',

	'question_10' => 'I am unable to get my item in Steam. ',
	'answer_10' => 'Your problem might be related to Steam having technical issues at the moment. Try to withdraw your item a bit later after Steam fixes its servers. It’s also important for you not to have any Steam Guard restrictions. The reasons for that are listed here — https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932.',

	'question_11' => 'Do you have customer support?',
	'answer_11' => 'Yes, definitely. Just contact us at csgotowerhelp@gmail.com at any convenient time and we’ll get back to you within 24 hours.',
];