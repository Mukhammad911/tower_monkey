<?php

return [
    'cases'=>'cases',
    'money_required'=>'There is not enough money!',
    'congratulate'=>'Congratulations!',
    'accept_item'=>'Accept',
    'on_home'=>'On Home Page',
    'send_item_profile'=>'Send item to your steam account you can in',
    'my_profile'=>'Profile.',
    'between_hour'=>'The item can be accepted up within an hour.',
    'inspect_game'=>'Inspect in game',
    'uniq_format' => 'New format of CS:GO case opening',
    'new_format_cs' => 'The New Format',
    'new_format_cs_go' => 'THE NEW FORMAT OF CS:GO CASES',
    'tickets' => 'TICKETS',
    'in_profit' => 'IN PROFIT',
    'user_rating' => 'User Rating',
    'top_24_hours' => 'TOP USERS IN 24 HOURS',
    'top_in_earnings' => 'TOP in earnings',
    'giveaway' => 'GIVEAWAY',
    'affiliates' => 'Affiliates',
    'top_trop' => 'TOP DROP',
    'twitter' => 'TWITTER',
    'fast_text' => 'Fast',
    'caserarity' => 'Rarity Cases',
    'error_found_page' => 'ERROR 404',
    'page_not_exist' => 'PAGE DOES NOT EXIST OR REMOVED',
    'go_home' => 'HOME PAGE',
    'rangcase' => 'Rank Cases',
    'assemblies' => 'Our Assemblies',
    'сollections' => 'Collections',
    'standart' => 'Standard Cases',
    'balance_enough' => 'You have enough money to raffle the ticket!',
    'ticket_unavailable' => 'Temporarily unavailable',
    'open_ticket' => 'OPEN TICKET FOR ',
    'ticket_contains' => 'TICKET CONTAINS',
    'card_demo' => 'DEMO TICKET',
    'next50_drops' => 'Next 50 Drops',
    'auth_warnbold' => 'Not working?',
    'auth_warnfix' => 'Solution:',
    'auth_warnp1' => '1. Clean the browser from unwanted extensions (or disable extensions that block content)',
    'auth_warnp2' => '2. Enable pop-UPS in browser',
    'auth_warnp3' => '3. Try a different browser',
    'auth_warnp4' => '4. Disable time antivirus software (can block pop-up window with login)',
    'respondent' => 'Responsive',
    'respondent_text' => 'the support team solves any problem',
    'reliable_text' => 'All your skins are securely stored in your inventory. At any time during the hour after opening case you can go to your profile and send skins to your Steam account. If within an hour skin is not sent - it will be auto-sold by market price and fill your balance',
    'my_benefit' => 'Profitable',
    'my_benefit_text' => 'Drop better than normal cs go cases, and other cases opening websites.',
    'auto_system' => 'You will be pleasantly surprised by the trade speed to Steam by our bots. Transfer of skins is fully automated and executed by high-speed bots which have been optimized specifically for use with Steam. All items that are in your inventory can be instantly sent to you. We have no delay before sending skins as on other sites.',
    'uniq_format_text' => 'Your choice. Your skin!',
    'collections'=>'collections',
    'exclusive'  =>'exclusive',
    'login' => 'sign in through steam',
    'login_mobile' => 'sign in',
    'login_case' => 'Login',
    'logout' =>'Logout',
    'header_support' =>'Support',
    'menu'  => 'Menu',
    'guarantees'=> 'Guarantees',
    'faq'   =>'Faq',
    'reviews'   =>'Reviews',
    'shop_cases'=>'DROP BETTER THAN GABEN',
    'live_drop'=>'live-drop',
    'lucky'=>'Lucky Guys',
    'bots_trades'   =>'BOT\'S LIVE TRADES',
    'cases_opened'=>'GAMES',
    'users'=>'Users',
    'online'=>'Online',
    'contacts'=>'Contacts',
    'terms'=>'TERMS & CONDITIONS',
    'balance_fill'=>'Filling up the balance',
    'add_balance'=>'Add balance',
    'pay'=>'PAY NOW',
    'faq_how'=>'How does it works?',
    'faq_step'=>'Sign in and go to profile',
    'faq_step2' =>'Enter a trade url',
    'faq_step3'=>'Add credit',
    'faq_step4'=>'Open cases for the best prices',
    'q1'=>'I opened the case, but the object does not come, what\'s happening?!',
    'ans1'=>'Open your profile and click the link "To take" on the object you are interested in.',
    'q2'=>'When I want to take object off the Bot , I receive a mistake: " Unfortunately, our bot couldn\'t send your object..."',
    'ans2'=> 'Bot can\'t send you the object in the following:
            You have the exchange blocked ( you entered from the new appliance, changed the password or e-mail recently, changed the security settings Steam Guard, have the VAC-ban, etc. More..... in order to avoid such problems in future, always recommend to check your trade status at special page csho.tm.
            You have inventory hidden in the privacy settings Steam
            Check your trade link in the profile and in the settings Steam - they have to coincide
            Bot has the problems with the Steam connection, it may happens andd very often, the problem is in the Steam, it\'s not our problem. Also bot can be banned, in this case you have to adress to technical support (support@csgotower.net)
            If you don\'t take your object during one hour, the whole price of this object acording to it\'s price at the Steam market.',
    'q3'=>'I can\'t open the cases, it is indicated the mistake " At the moment bot doesnt have the object for the case choosen. Try to do it later."',
    'ans3'=>'For the real random, cases/categories must always contain ALL the objects in it, which are listed at the page of the case/categor. Bots don\'t have enough time to buy all necessary things in the time needed or some necessary objects for this case are absent at the market now.',
    'q4'=>'Filled the account, but money doesn\'t appear. What to do?',
    'ans4'=>'Money can appear in some time - wait an hour. If money not appeared more than in an hour write to us (support@csgotower.net),indicating the whole information of the payment.',
    'q5'=>'I have money at Steam account but they are not indicated here. Why?',
    'ans5'=>'Steam account money are not valid here at this site. To fill the balance it is needed separately.',
    'q6'=>'Can\'t enter the site through Steam, the page with the cases is not loading, there is an advertisement in the centre of the page , and in general site is not working good, what to do?',
    'ans6'=>'May be the problem is in antivirus, which can block the conection with the site, or the problem is in a browser, which shows the advertisement also can block the connection with the site.',
    'steam_profile'=>'Steam Profile',
    'balance'=>'Balance',
    'settings'=>'Settings',
    'transe_url_enter'=>'ENTER A TRADE-URL',
    'field_bottom'=>'FIELD FOR ENTERING AT THE BOTTOM',
    'add_credit'=>'ADD CREDIT',
    'payment_methods'=>'VISA, MASTERCARD, PAYPAL ETC.',
    'win'=>'WIn',
    'best_cases'=>'OPEN CASES FOR THE BEST PRICES',
    'save'=>'Save',
    'explore_url'=>'Explore Trade-URL',
    'your_items'=>'Your items',
    'accepted'=>'Accepted',
    'sold'=>'Sold',
    'accept'=>'Accept',
    'sell'=>'Sell',
    'warn1'=>'Be careful!',
    'recomm1'=>'All questions can only be solved via the mail support@csgotower.net
Ignore anyone who is writting to you in social sounding networks site administrator - this is crooks.',
    'warn2'=>'Before opening Cases',
    'recomm2'=>'Be sure to check your account exchanges on Steam, otherwise our bots will not be able to send you your items. Open your inventory, make sure that there is no ban 7 days after the password had been changed.',
    'warn3'=>'FAQ and other issues',
    'recomm3'=>[
        'Check out the page',
        'FAQ',
        ', it describes in detail all issues that may arise.'
    ],
    'warn4'=>'Collect item',
    'recomm4'=>'Full value of the item will be returned on your balance if you have not taken it for an hour.',
    'sending'=>'Sending',
    'items'=>'Items',
    'case_contains'=>'case contains',
    'open_case'=>'Open case',
    'open_for'=>'Open case for',
    'opening'=>'Opening',
    'unavailable'=>'Temporarily unavailable',
    'no_money'=>'There is not enough money to open case. Fill up on',
    'your_drop'=>'Your win',
    'try_again'=>'Try again',
    'sell_for'=>'Sell for',
    'congratulations'=>'Congratulations! The item can be accepted up within an hour.',
    'home'=>'Home',
    'reliable'=>'Reliable',
    'qualitatively'=>'qualitatively',
    'fast'=>'fast',
    'reliable_mess'=>'In case you have not taken your items at once, you can go to the profile and resend it. For example, if you open 10 or 15 cases in a row and do not accept the items at once, the bot will send them to you again. Just go to your profile, and send the items again.',
    'qualitatively_mess'=>'We do not lie, and we can prove it. Clicking on the item in the “live-drops” feed you will see the user\'s profile, with a history of items that have fallen to him. What is it for? You may have seen a similar “live-drops” feed on other sites where different “Azimov for 50$” fall out, and then you buy cases and weapons dropped out for 0.5-1 dollars.',
    'fast_mess'=>'Everything is fully automated. Sending items via bots greatly simplifies the whole process and a special method of procurement guarantees getting of any item, whether it is “COVERT” or “MIL-SPEC” quality.',
    'server_error'=>'Server\'s error. Try again later!',
    'profile_sttings_success'=>'Trade URL successfully updated!',
    'profile_sttings_error'=>'Saving Error!',
    'success'=>'Success',
    'c_error'=>'Please log in on the website!',
    'error'=>'Error',
    'drops'=>'ALL drops',
    'loading'=>'Loading',
    'enter_your'=>'Enter your Trade-URL',
    'pay_attention'=>'Attention! Payments could be delayed for 5-10 minutes!',
    'enter_amount'=>'Enter the amount',
    'invalid_trade_url'=>'Invalid Trade URL format',
    'alien_trade_url'=>'That is not your trade URL',
    'second_ago'=>'second ago',
    'no_items'=>'no items, open your',
    'first_case'=>'first case',
    'contact'=>'please contact',
    'payment_error'=>'Payment error',
    'payment_success'=>'Thank you! Your payment is successfully done. Be aware, payments could be delayed for 5-10 minutes!',
    'receive'=>'receive',
    'received'=>'received',
    'pending'=>'Pending receipt',
	'offered' => 'Offered',
    'sent' => 'Trade offer has been sent.',
    'viewingame'=>'View in game',
    'card.congratulation' => 'Congratulations!',
    'card.thisisyour' => 'This is your',
    'card.tryanother' => 'Try More',
    'card.sellfor' => 'Sell for',
    'card.take' => 'Take',
    'card.tohome' => 'Go home',
    'card.share.1' => 'Share with friends and',
    'card.share.2' => 'get',
    'card.share.3' => 'to account',
    'card.share.4' => 'Tell friends',
    'card.take.1' => 'Pick a subject at',
    'card.take.2' => 'profile',
    'card.take.3' => 'in one hour',
    'card.auth.1' => 'Please',
    'card.auth.2' => 'login',
    'item'=>'item',
    'bot'=>'bot',
    'user'=>'user',
    'time'=>'time',
    'trade\'s'=>'trade',
    'send_item'=>'Send item to your steam account you can in',
    'profile'=>'Profile',
    'pay_skins_message'=>'Be aware that when you deposit money using skins, your balance will receive the sum that was mentioned in the order.',
    'tutor_auth' => 'PLAY FREE DEMO TICKET',
    'tutor_auth_2' => 'Experience the best opening format of CS:GO Cases in action!',
    'tutor_teacher1_info' => 'To win, erase the mouse movement 3 of the same slot of 9',
    'tutor_teacher1_okay' => 'Understand, I will try!',
    'tutor_teacher2_info' => 'Choose - open another slot (now for free) or just pick up your guaranteed skin?',
    'tutor_teacher2_okay' => 'Ready to choose',
    'tutor_teacher3_info' => 'If you could not guess - open your skin guaranteed!',
    'tutor_teacher3_okay' => 'Good',
    'tutor_demo_info1' => 'This is a demonstration of the work csgotower.net!',
    'tutor_demo_info2' => 'The item for the training ticket is NOT ISSUED!',
    'tutor_over' => 'Training is over!',
    'tutor_example' => 'This is an example of drop:',
    'tutor_complete' => 'Complete the training!',
    'tutor_complete_info' => 'It\'s time to test your intuition and win a best skins on csgotower.net',
    'choose_try' => '+1 Try',
    'choose_get' => 'You can get',
    'choose_info' => 'Try your luck or just take a guaranteed win?',
    'choose_free' => 'Now for free!',
    'choose_notenough' => 'Insufficient funds - replenish the balance!',

    //marks
    'sale'      =>'sale',
    'popularly' =>'popular',
    'novelty'   =>'new',
    'best-price'=>'best price',

    //rules
    'notice_first'     =>'During training in a free ticket',
    'notice_second'     =>'You DO NOT GET skin in free ticket, but will be able to evaluate the gameplay, simply',
    'notice_third'     =>'log in',

    'rules_first'     =>'BASIC RULES OF csgotower:',
    'rules_second'     =>'Open 3 of the same item and get it!',
    'rules_third'     =>'3 attempts plus 1 additional optional',
    'rules_fourth'     =>'A guaranteed win for everyone!',

    //faq => privilege
    'about_project' =>  'About the project and guarantees',
    'privilege_title' =>  'Why open cases/tickets with us?',

    'gold_title_1' =>  'Fully automated system. ',
    'warranty_1'  =>  'The exchange offer by bot comes in a few seconds - it\'s important to you quickly and prepared as the simplest and best things won without problems.',

    'profit'  =>  'Profitably',
    'gold_title_2' =>  'Drop on csgotower guaranteed better in-game opening cases. ',
    'warranty_2'  =>  'Not only that you can do to get the best thing out of the ticket, and with a guaranteed slot skin package contains a good drop! Read ',
    'warranty_2_link'  =>  'user reviews',
    'warranty_2_2'  =>  ' and make sure that we often fall StatTrak things.',

    'reliably'  =>  'Reliably',
    'gold_title_3' =>  'We have nothing to hide - all the statistics in front of your eyes. ',
    'warranty_3'  =>  'Click on the item in the live-tape skins and get detailed information about the user - his nickname, a reference to Steam-account and history of drops. ',
    'warranty_3_link'  =>  'TOP-users',
    'warranty_3_2'  =>  '  is always available via the link in the header. Also at the bottom of the page you can see the overall statistics csgotower.',

    'gold_title_4' =>  'You always get your skin! ',
    'warranty_4'  =>  'If you have not taken a thing or decide to sell it online, you will receive the equivalent value of the object at his own expense. You can always go to a profile and send the item again.',

    //faq => instruction
    'instruction_title' =>  'How to get the skin?',
    'step' =>  'Step',
    'instruction_1' =>  'Sign in through Steam, go to your account by clicking on the nick',

    'instruction_2' =>  'Play your first training ticket on ',
    'instruction_2_link' =>  'the home page',

    'instruction_3' =>  'On the ',
    'instruction_3_link' =>  'Account page',
    'instruction_3_2' =>  ', enter the Trade-link ',
    'instruction_3_link_2' =>  'what is it?',
    'instruction_3_3' =>  ' — So you can instantly get the skins',

    'instruction_4' =>  ' on the site in any convenient way',
    'instruction_4_link' =>  'Refill your account',

    'instruction_5' =>  'Open 3 of 9 identical fields in the ticket and get the best thing in the case! In this case you get in any case subject, as ',
    'instruction_5_2' =>  'csgotower.net only you can open slot with a guaranteed skin package.',
    'instruction_5_3' =>  'On csgotower it decides not only luck, but your intuition!',

    //faq => answer & question
    'faq_cat_1'  =>  'Do not come at the expense of money!',
    'faq_cat_2'  =>  'The problem with the game ticket!',
    'faq_cat_3'  =>  'I can not bring the subject!',
    'faq_cat_4'  =>  'The problem with learning a ticket!',
    'faq_cat_5'  =>  'I cheated officer csgotower!',
    'faq_cat_6'  =>  'I have another problem',

    'question_1'  =>  'I OPENED THE CASE, BUT THE OBJECT DOES NOT COME, WHAT\'S HAPPENING?!',
    'answer_1'  =>  'Open your profile and click the link "To take" on the object you are interested in.',

    'question_2'  =>  'WHEN I WANT TO TAKE OBJECT OFF THE BOT , I RECEIVE A MISTAKE: " UNFORTUNATELY, OUR BOT COULDN`T SEND YOUR OBJECT...',
    'answer_2'  =>  'Bot can\'t send you the object in the following: You have the exchange blocked ( you entered from the new appliance, changed the password or e-mail recently, changed the security settings Steam Guard, have the VAC-ban, etc. More..... in order to avoid such problems in future, always recommend to check your trade status at special page csho.tm. You have inventory hidden in the privacy settings Steam Check your trade link in the profile and in the settings Steam - they have to coincide Bot has the problems with the Steam connection, it may happens andd very often, the problem is in the Steam, it\'s not our problem. Also bot can be banned, in this case you have to adress to technical support (support@csgotower.net) If you don\'t take your object during one hour, the whole price of this object acording to it\'s price at the Steam market.',

    'question_3'  =>  'I CAN\'T OPEN THE CASES, IT IS INDICATED THE MISTAKE  AT THE MOMENT BOT DOESNT HAVE THE OBJECT FOR THE CASE CHOOSEN TRY TO DO IT LATER.',
    'answer_3'  =>  'For the real random, cases/categories must always contain ALL the objects in it, which are listed at the page of the case/categor. Bots don\'t have enough time to buy all necessary things in the time needed or some necessary objects for this case are absent at the market now.',

    'question_4'  =>  'FILLED THE ACCOUNT, BUT MONEY DOESN\'T APPEAR. WHAT TO DO?',
    'answer_4'  =>  'Money can appear in some time - wait an hour. If money not appeared more than in an hour write to us (support@csgotower.net),indicating the whole information of the payment.',

    'question_5'  =>  'I HAVE MONEY AT STEAM ACCOUNT BUT THEY ARE NOT INDICATED HERE. WHY?',
    'answer_5'  =>  'Steam account money are not valid here at this site. To fill the balance it is needed separately.',

    'question_6'  =>  'CAN\'T ENTER THE SITE THROUGH STEAM, THE PAGE WITH THE CASES IS NOT LOADING, THERE IS AN ADVERTISEMENT IN THE CENTRE OF THE PAGE , AND IN GENERAL SITE IS NOT WORKING GOOD, WHAT TO DO?',
    'answer_6'  =>  'May be the problem is in antivirus, which can block the conection with the site, or the problem is in a browser, which shows the advertisement also can block the connection with the site.',

    'question_7'  =>  'Why are some tickets available for the opening? He writes error "At the moment the bots are no items for the selected ticket. Please try again later."',
    'answer_7'  =>  'To ensure the best loot, tickets must always contain all the items that are listed on the ticket page. Bots can not keep up with all the necessary items purchased at the right time or in the marketplace lacking some of the necessary items for this ticket.',

    'question_8'  =>  'I won, but the thing does not come, what a divorce ?!',
    'answer_8'  =>  'Go to',
    'answer_link_8'  =>  'your profile',
    'answer_8_2'  =>  'and click on "COLLECT" to what you like.',

    'question_9'  =>  'When you try to bring things to a bot, I get the error: "Unfortunately our boat was not able to send your thing ...."',
    'answer_9'  =>  'Bot can not send you to the subject in the following cases:',
    'answer_9_2'  =>  'You have blocked exchange (came with the new device, recently changed the password / e-mail, changed the Steam Guard security settings have VAC-ban, etc.).',
    'answer_link_9_1'  =>  'More...',
    'answer_9_3'  =>  'To these problems was not always recommend that you check your status on the trade',
    'answer_link_9_2'  =>  'special page',
    'answer_9_4'  =>  'You have hidden in inventory',
    'answer_link_9_3'  =>  'Steam privacy settings',
    'answer_9_5'  =>  'Check your trade-in link',
    'answer_link_9_4'  =>  'profile',
    'answer_9_6'  =>  'and',
    'answer_link_9_5'  =>  'Steam settings',
    'answer_9_7'  =>  'they must be the same',
    'answer_9_8'  =>  'We bot have a problem with Steam connection, it happens quite often, the problem is in the Steam, not us. Also, the bot could be banned, in this case, contact the Help Desk',
    'answer_9_9'  =>  'If you are not taking your subject for an hour, you will return the full value of this object according to the price of',
    'answer_link_9_6'  =>  'Steam Trading Marketplace',

    'question_10'  =>  'I can not get things to the robot writes an error 11 (as well as other Steam error, or if the bot cancels trade immediately after threw exchange)',
    'answer_10'  =>  'Unfortunately, Steam is not the most optimized system and often gives failures. 99.9% of errors in the transfer of the object is the fault of the idle functional Steam. It is worth to try to deduce a thing as long as the object will not appear in your inventory.',

    'question_11'  =>  'When you try to share my displays a warning that others complained about the bot. What is the problem?',
    'answer_11'  =>  'Our detractors from Steam community complain about the bots, which can lead to such problems. Know that this warning does not vliyat bot on your work and exchange, share and easily get their items.',

    'question_12'  =>  'Why I was not given a ticket thing for training?',
    'answer_12'  =>  'No one thing is given for a training card - on it and "training" :) This ticket we show new players the principles of operation of the site.',

    'question_13'  =>  'The incentive I added a "moderator/employee" csgotower\'a, offers to buy items won. What to do?',
    'answer_13'  =>  'This crook, refer to the information in your profile! We do not add to the incentive! IT\'S ALL HYPE! YOU steal your items!',

    'question_14'  =>  'I can not enter the site through Steam, is not loaded page with the tickets, and in general the site is very much lag, what to do?',
    'answer_14'  =>  'The most common problem is either antivirus, which blocks the connection to the site, or a browser extension that show advertising, and may also block the connection to the site. Try to disable antivirus and delete any suspicious extensions in the browser. The recommended browser for viewing a site - Google Chrome.',

    'question_15'  =>  'Can I withdraw money from the site?',
    'answer_15'  =>  'No. You can sell items for real money',

    'question_16'  =>  'Can I change the link exchange to your profile and send things to another account?',
    'answer_16'  =>  'No, things are bound to the very Steam account.',

    'question_17'  =>  'I have a temporary ban on the trade account, can I pick up things later?',
    'answer_17'  =>  'If you do not pick up your item for an hour, your balance will return its full value (based on the price on the trading platform Steam). Check trades / Steam exchanges possible on',
    'answer_link_17'  =>  'special page',

    'question_18'  =>  'Window opened Error 502 Bad gateway, what to do?',
    'answer_18'  =>  'If you encounter ',
    'answer_link_18'  =>  'a similar situation, ',
    'answer_18_2'  =>  'then you will go to the working version of the site using the button at the top right (as soon as the site will be ready for operation).',

    'faq_q1'  =>  'I can not send an item won in Steam!',
    'faq_a1'  =>  'Important! First of all, before you top-up before it opening the case - make sure that your Steam account can receive the items. Go to Steam account and go to the inventory should be notification that the account is frozen. Steam account can be frozen for 7 days after the change password, e-mail, Steam Guard, connect 2FA ... frozen Steam account can not receive the items, so our boat will not be able to send your winning item. This is an important point, so we have several times warned about this for prepaid and provides its trade links.',

    'faq_q2'  =>  'Just I joined the account and there is no money on the balance',
    'faq_a2'  =>  'To process payments using csgotower G2A Pay payment system. All payments are processed by this payment system. 99% of all the limitations and failures associated with restrictions G2A Pay. Usually, the payment is processed within 5-10 minutes. Sometimes the payment may be delayed by 1 hour. You can contact our support for more information about your payment.',

    'faq_q3'  =>  ' I opened the ticket where my prize?',
    'faq_a3'  =>  'Your winnings are stored in your inventory. In the inventory you can go by clicking on the prize, or on its own avatar in the upper right corner of the site',

    'faq_q4'  =>  'Why was my winnings sold?',
    'faq_a4'  =>  'Skins in your inventory will automatically be sold after 24 hours. This preventive measure does not overwhelm your bots and keep in a case always available skins.',

    'faq_q5'  =>  'I can not login trough Steam or send the item',
    'faq_a5'  =>  'csgotower Steam Community uses for authenticating users and sending skins. Sometimes Steam is experiencing difficulties in work or can not even respond to our requests.',

    'faq_q6'  =>  'I want to turn off the sound on the site',
    'faq_a6'  =>  'You can click the icon on the Audio page of the case to enable / disable sounds',

    'faq_attention'  =>  'Attention!',
    'faq_attention_2'  =>  'If you do not find the answer to your question in this section, contact us at mail ',
    'faq_attention_3'  =>  ', or message ',
    'faq_attention_link'  =>  'in the  Facebook community!',

    //profile
    'breadcrumbs_users' => 'Users',
    'balance_up' => 'Fill up balance',
    'profile_steam' => 'Profile in Steam',
    'initial_settings' => 'Profile settings',
    'link_exchange' => 'Trade Link',
    'enter_below' => 'Enter below.',
    'prfole_what' => 'What is it?',
    'refill_balance' => 'Refill balance',
    'all_electronic_money' => 'All kinds of electronic money',
    'winer' => 'Win!',
    'get_best_drop' => 'Get the best drop',
    'your_link_exchange' => 'Your Trade-URL',
    'copy_steam_link' => 'copy with Steam website',
    'save_profile_link' => 'Save',
    'trade_restrict_link1' => 'Be sure your Steam Account ',
    'trade_restrict_link2' => 'can accept Trade Offers!',
    'trade_restrict_head' => 'TRADE RESTRICTIONS',
    'trade_restrict_b0' => 'Why my Steam account may be restricted to trade?',
    'trade_restrict_p1' => 'All reasons may be found ',
    'trade_restrict_p1_a' => 'here',
    'trade_restrict_p1_end' => ', but the most frequent reasons are:',
    'trade_restrict_b1' => '1. Steam Guard Not Enabled',
    'trade_restrict_b2' => '2. Steam Guard Only Recently Enabled',
    'trade_restrict_p2' => 'We require Steam Guard to be enabled for 15 days to help protect your items and Steam Wallet funds from being misused by someone who may have illicitly obtained your password. If you have not had Steam Guard enabled for 15 days, you will be unable to trade or use the Community Market. Accounts that currently have Steam Guard disabled will be unable to trade and use the Community Market.',
    'trade_restrict_p3' => 'If you have recently enabled Steam Guard on your account, you will be unable to use the Community Market for 15 days since Steam Guard was enabled. Deauthorizing Steam Guard or disabling then enabling Steam Guard will also trigger this restriction.',

    //profile => info block
    'header_pointer' => 'Pay attention!',

    'bad_head' => 'Beware of scams!',
    'bad_text' => 'We will never be added to your Friend on Steam and offer exchanges - it is all a lie! Be careful and you have never stolen items.',

    'good_head' => 'Support and FAQ',
    'good_text' => 'Especially for you we have collected all the basic questions about the service in our ',
    'good_text_link' => 'FAQ',
    'good_text_2' => ', and in obscure cases, write a message to our ',
    'good_text_link_2' => 'Facebook Community',

    'vk_head' => 'Contests and sweepstakes skins',
    'vk_text' => 'We constantly carry out contests, sweepstakes, and for no particular reason are distributing promo codes to replenish the balance in our ',
    'vk_text_link' => 'Facebook Community',
    'vk_text_2' => ', join us!',

    'promocodes' => 'Promo codes',
    'promocodes_text' => 'On the website and ',
    'promocodes_link' => 'in our Twitter',
    'promocodes_text_2' => ' constantly emerging promotional codes. Enter them in one of the first and get the bonus on the balance!',
    'promocodes_button' => 'Done!',

    'items_incase_text' => 'Currently there are no items. Open your ',
    'items_incase_link' => 'first ticket!',

    'partner_head'  =>  'partnership',
    'partner_text'  =>  'We are ready to thank you for what advise csgotower.net!',
    'partner_text_2'  =>  'Get the money to the account every time you have a guest user will fill your balance! If you invite many people - you can open a ticket and did not refill your account!',
    'partner_text_3'  =>  'Your link to the distribution of invitations:',
    'partner'       => 'COMING SOON',

    'registered'  =>  'Have registered',
    'percent'  =>  'Your share',
    'invited_joined'  =>  'Invited joined',
    'earnings'  =>  'Your earnings',


    //Agreements
    'terms_p1'  =>  'No individual under the age of eighteen (18) may use the Service, regardless of any consent from your parent or guardian to use the Service.',
    'terms_p1_br1'  =>  'You need a supported Web browser to access the Service. You acknowledge and agree that csgotower.net may cease to
        support a given Web browser and that your continuous use of the Service will require you to download a supported
        Web browser. You also acknowledge and agree that the performance of the Service is incumbent on the performance
        of your computer equipment and your Internet connection.',
    'terms_p1_br2'  =>  'You agree to register and sigh on the Services through your Steam account provided by the Valve Corporation. You are
        solely responsible for managing your account and password and for keeping your password confidential. You are
        also solely responsible for restricting access to your account.',
    'terms_p1_br3'  =>  'You agree that you are responsible for all activities that occur on your account or through the use of your
        password by yourself or by other persons. If you believe that a third party has access to your password, use the
        password regeneration feature of the Service to obtain a new password as soon as possible. In all circumstances,
        you agree not to permit any third party to use or access the Service.',

    'terms_p2_ul'  =>  'As a condition to your use of the Service, you agree not to:',
    'terms_p2_l1'  =>  'impersonate or misrepresent your affiliation with any person or entity;',
    'terms_p2_l2'  =>  'access, tamper with, or use any non-public areas of the Service or csgotower.net\'s computer systems;',
    'terms_p2_l3'  =>  'attempt to probe, scan, or test the vulnerability of the Service or any related system or network or breach
            any security or authentication measures used in connection with the Service and such systems and networks;',
    'terms_p2_l4'  =>  'attempt to decipher, decompile, disassemble, reverse engineer or otherwise investigate any of the software
            or components used to provide the Service;',
    'terms_p2_l5'  =>  'harm or threaten to harm other users in any way or interfere with, or attempt to interfere with, the access
            of any user, host or network, including without limitation, by sending a virus, overloading, flooding,
            spamming, or mail-bombing the Service;',
    'terms_p2_l6'  =>  'provide payment information belonging to a third party;',
    'terms_p2_l7'  =>  'use the Service in an abusive way contrary to its intended use, to csgotower.net policies and instructions and
            to any applicable law;',
    'terms_p2_l8'  =>  'systematically retrieve data or other content from the Service to create or compile, directly or indirectly,
            in single or multiple downloads, a collection, compilation, database, directory or the like, whether by
            manual methods, through the use of bots, crawlers, or spiders, or otherwise;',
    'terms_p2_l9'  =>  'make use of the Service in a manner contrary to the terms and conditions under which third parties provide
            facilities and technology necessary for the operation of the Service, such as G2A or Valve;',
    'terms_p2_l10'  =>  'infringe third party intellectual property rights when using or accessing the Service, including but not
            limited to in making available virtual items by using the Service;',
    'terms_p2_l11'  =>  'make use of, promote, link to or provide access to materials deemed by csgotower.net at its sole discretion to
            be offensive or cause harm to csgotower.net reputation, including, but not limited to, illegal content and
            pornographic content and content deemed offensive or injurious to csgotower.net and/or the Service (such as
            Warez sites, IRC bots and bittorent sites).',
    'terms_p2_l12'  =>  'The withdraw of the skins will be available after you refill your balance on the site up to the minimal
            amount $2 for one transaction',

    'terms_p3'  => 'Termination',

    'terms_p4'  => 'We may terminate or suspend access to our Service immediately, without prior notice or
        liability, for any reason whatsoever, including without limitation if you breach the Terms.',
    'terms_p4_br1'  => 'All provisions of the Terms which by their nature should survive termination shall
        survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and
        limitations of liability.',
    'terms_p4_br2'  => 'We may terminate or suspend your
        account immediately, without prior notice or liability, for any reason whatsoever, including without limitation
        if you breach the Terms.',
    'terms_p4_br3'  => 'Upon termination, your right to use the
        Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the
        Service.',
    'terms_p4_br4'  => 'All provisions of the Terms which by their nature should
        survive termination shall survive termination, including, without limitation, ownership provisions, warranty
        disclaimers, indemnity and limitations of liability.',

    'terms_p5'  => 'Links To Other Web Sites',

    'terms_p6'  => 'Our Service may contain links to third-party web sites or services that are not owned or
        controlled by csgotower.net',
    'terms_p6_br1'  => 'csgotower.net has no control over, and
        assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or
        services. You further acknowledge and agree that csgotower.net shall not be responsible or liable, directly or
        indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on
        any such content, goods or services available on or through any such web sites or services.',
    'terms_p6_br2'  => 'We strongly advise you to read the terms and conditions and privacy policies of any
        third-party web sites or services that you visit.',

    'terms_p7'  => 'No warranties ',

    'terms_p8'  => 'This website is provided “as is” without any representations or warranties, express or implied. csgotower.net
        makes no representations or warranties in relation to this website or the information and materials provided on
        this website. Without prejudice to the generality of the foregoing paragraph, csgotower.net does not warrant
        that: this website will be constantly available, or available at all; or the information on this website is
        complete, true, accurate or non-misleading. Nothing on this website constitutes, or is meant to constitute,
        advice of any kind. If you require advice in relation to any legal, financial or medical matter you should
        consult an appropriate professional.',

    'terms_p9'  => 'csgotower.net Wallet',

    'terms_p10'  => 'By topping up an amount to your csgotower.net wallet, you accept that the credits received (labeled by the $-sign)
        are not worth real money and not subject to any refunds.',

    'terms_p11'  => 'Auto-Sell conditons',

    'terms_p12'  => 'In order to avoid abuse skins delays in the inventory of single user - winning skins are sold in one hour, full
        cost of skins reverts to user\'s balance.',

    'terms_p13'  => 'Additional Terms and Conditions; EULAs',

    'terms_p14'  => 'When you use G2A Pay services provided by G2A.COM Limited (hereinafter referred to as the "G2A Pay services
        provider") to make a purchase on our website, responsibility over your purchase will first be transferred to
        G2A.COM Limited before it is delivered to you. G2A Pay services provider assumes primary responsibility, with
        our assistance, for payment and payment related customer support. The terms between G2A Pay services provider
        and customers who utilize services of G2A Pay are governed by separate agreements and are not subject to the
        Terms on this website.',

    'terms_p15'  => 'With respect to customers making purchases through G2A Pay services provider checkout, (i) the Privacy Policy of
        G2A Pay services provider shall apply to all payments and should be reviewed before making any purchase, and
        (ii) the G2A Pay services provider Refund Policy shall apply to all payments unless notice is expressly provided
        by the relevant supplier to buyers in advance. In addition the purchase of certain products may also require
        shoppers to agree to one or more End-User License Agreements (or "EULAs") that may include additional terms set
        by the product supplier rather than by Us or G2A Pay services provider. You will be bound by any EULA that you
        agree to. We and/or entities that sell products on our website by using G2A Pay services are primarily
        responsible for warranty, maintenance, technical or product support services for those Products.',

    'terms_p16'  => 'We and/or entities that sell products on our website are primarily responsible to users for any liabilities
        related to fulfillment of orders, and EULAs entered into by the End-User Customer. G2A Pay services provider is
        primarily responsible for facilitating your payment.',

    'terms_p17'  => 'You are responsible for any fees, taxes or other costs associated with the purchase and delivery of your items
        resulting from charges imposed by your relationship with payment services providers or the duties and taxes
        imposed by your local customs officials or other regulatory body.',

    'terms_p18'  => 'For customer service inquiries or disputes, You may contact us by email at',

    'terms_p19'  => 'Questions related to payments made through G2A Pay services provider payment should be
            addressed to ',

    'terms_p20'  => 'Where possible, we will work with You and/or any user selling on our website, to resolve any disputes arising
        from your purchase.',

    //Trade-Modal
    'modal_trade_text'  =>  'REFRESH THE TRADE LINK',
    'trade_placeholder'  =>  'Enter your Trade-URL',
    'trade_button_save'  =>  'Save',
    'trade_footer_text'  =>  'Don\'t know where get the trade link? Just ',
    'trade_footer_click'  =>  'click here',


    //market
    'market' => 'WITHDRAW',


    //top_winners
    'top_winners' => 'TOP WINNERS',

    //provably_fair
    'provably_fair' => 'Provably fair',


    //index
    'ORIGINAL CASES' => 'ORIGINAL CASES',
    'credits' => 'Credits',
    'you_may_win' => 'You may win',
    'SPECIAL CASES' => 'SPECIAL CASES',
    'show_all' => 'SHOW ALL',
    'new_way' => 'Easy way to get skins!',

    //profile
    'personal_area' => 'PROFILE',
    'activate_promocode' => 'ACTIVATE PROMOCODE',
    'enter_promocode_here'=>'Enter your promocode here',
    'activate'=>'Activate',
    'payment'=>'Payment',
    'payment_history'=>'Payment History',
    'date'=>'Date ',
    'details'=>'Details',
    'attention_five_min_payment'=>'Attention! Payments could be delayed for 5-10 minutes!',
    'example'=>'Example ',
    'calculator'=>'Calculator ',

    'support'=>'Support：',

    //affiliates

    'your_partner_link'=>'YOUR PARTNER LINK',
    'level'=>'Level',
    'referrals'=>'Referrals',
    'to_next_level'=>'TO NEXT LEVEL',
    'percentage'=>'free $ for user',
    'referral_receive'=>'Referral receive',
    'referral_deposited'=>'Referral deposited',

    'best_affiliates'=>'Profitable system of earning',
    'on_csgotower'=>'csgotower give you a chance to earn great money with the easiest way.',
    'on_start'=>'5% For All',
    'percent_on_market'=>'Your referrals receive always 5% to deposit on csgotower',
    'sponsorship'=>'Partnership',
    'youtube_or_twitch'=>'If you have YouTube channel with 200000+ subscribers contact us : media@csgotower.net',
    'partner_levels'=>'Partner levels',
    'to_deposit'=>'to deposit',

    'paypal_attention' => 'Attention! PayPal Users: PayPal payment option is unavailable. But you can still make a deposit by using a PayPal. You need to register a G2A account, then add balance by using a PayPay system. After that you can add funds on csgotower balance by using your G2A Wallet',
    'hash_number'=>'Hash number',
    'win_number'=>'Win number',
    'user_seed'=>'User seed',
    'server_seed'=>'Server seed',
    'next_ticket'=>'Next ticket',
    'previous_ticket'=>'Previous ticket',
    'close_letter'=>'Close',
    'verification'=>'Verification',
    'wagered'=>'Wagered',
    'games'=>'Games',
    'easy'=>'Easy',
    'medium'=>'Medium',
    'hard'=>'Hard',
    'sort_item_by_price'=>'SORT ITEMS BY PRICE',
    'sort_item_by_quality'=>'QUALITY',
    'price_low'=>'Price:Low -> High',
    'price_high'=>'Price:High -> Low',
    'enter_item_name'=>'Search for a skin...',
    'select_all_view'=>'Select all view',
    'refresh'=>'Refresh',
    'withdrawable'=>'Withdrawable',
    'selected_items'=>'Selected items',
    'item_value'=>'Item value',
    'update_steam_trade_url'=>'Update steam trade URL',
    'price_range'=>'Item price range',
    'win_up_to'=>'WIN UP to',
    'check_winning_number'=>'Provably Fair',
    'try_other_cases'=>'Back',
    'cases_to_open'=>'Cases to open',
    'credits_case'=>'CREDITS CASE',
    'win_chance'=>'Win chance',
    'multiplier'=>'Multiplier',
    'withdraw'=>'WITHDRAW',
    'games_history' => 'Games History',
    'withdraw_history' => 'Withdrawals stories',

    'text_provably_fair_1'=>'The mechanism of the ‘provably fair’ game allows you to control the fair play and impartiality of the algorithms distributing the dice. In order to perform the check you may use either our own online calculator or any other php-tester, e.g.',
    'text_provably_fair_2'=>'This will help calculate the hash of the particular round.',
    'text_provably_fair_3'=>'How does the mechanism of the ‘provably fair’ game work?',
    'text_provably_fair_4'=>'Prior to every round of the game a unique seed is generated based on your data (the client seed) and the data of the server (the server seed).',
    'text_provably_fair_5'=>'After the launch of the round a hash that encodes the winning number is generated using a 256-bit encryption algorithm.',
    'text_provably_fair_6'=>'When the round is over you gain access to the winning number. Then, having all the data, you can encode the seeds and the winning number yourself to calculate the hash and compare it to the one received before the launch of the round.',



    'index_make_profit' => 'THIS IS YOUR WAY TO MAKE PROFIT!',
    'index_span1' => 'Unbox and withdraw the skins you actually want. And win up to 1 million dollars!',
    'index_span2' => 'All skins are always available',
    'index_start' => 'Start',

    'profile_payment_status' => 'Status',
    'day_top' => 'Last day top wins',
    'week_top' => 'Last week top wins',
    'month_top' => 'Last month top wins',
    'calculate' => 'Calculate',
    'link_case' => 'Open',
    'modal_fair' => 'PROVABLY FAIR',
    'modal_fair_next' => 'NEXT GAME',
    'modal_fair_new_seeds' => 'new seeds',
    'modal_fair_item' => 'Item',
    'modal_fair_winrange' => 'Winning number range',
    'case_no_money' => 'not enough',
    'upgrade_crd_in' => 'ENTER THE AMOUNT',
    'upgrade_btn' => 'Upgrade money',
    'upgrade_win_chance' => 'WIN CHANCE',
    'upgrade_win_multi' => 'MULTIPLIER',
    'upgrade_fair' => 'Provably fair',
    'upgrade_deposit' => 'Deposit',
    'upgrade_withdraw' => 'WITHDRAW',
    'upgrade_custom_item' => 'Custom item',
    'upgrade_nominal' => 'Choose nominal',



    'modal_trade' => 'Trade URL',
    'modal_trade_placeholder' => 'Enter trade URL',
    'modal_trade_link' => 'Your link exchange:',
    'modal_trade_save' => 'Save',

    'modal_withdraw' => 'Withdraw items',
    'modal_withdraw_wait' => 'Please wait while your trade offers are being sent',
    'modal_withdraw_storage' => 'Storage',
    'modal_withdraw_open_trade' => 'open trade',
    'modal_withdraw_sending' => 'Sending',
    'modal_withdraw_success' => 'Success',
    'modal_withdraw_dismiss' => 'Dismiss',

    'market_no_money' => 'You do not have enough CRD',

    'open_for_crd' => 'Open for',
    'roulette_fair' => 'Roulette',
    'user_seed_fair' =>  'User seed',
    'server_seed_fair' =>  'Server seed',
    'prev_game_fair' => 'Previous game',
    'win_number_fair' => 'Win number',
    'hash_fair' => 'Hash',
    'tower_take1' => 'take',
    'tower_take2' => 'credits',
    'profile_last_payment' => 'Last payment',

    'open_for_coins' => 'Open Case',

    //tower
    'tower_table_rank'=>'Rank',
    'tower_table_player' => 'PLAYER',
    'tower_table_bet' => 'BET',
    'tower_table_profit' => 'PROFIT',
    'tower_table_step' => 'STEP',
    'tower_table_level' => 'LVL',
    'tower_bet_min' => 'min',
    'tower_bet_max' => 'max',
    'tower_verify' => 'Verify ticket fairness',
    'top_profit' => 'profit',
    'tower_table_wagered'  => 'WAGERED',
    'min'=>'min',
    'max'=>'max',
    'heroes'=>'HEROES',
    'chat'=>'CHAT',
    'HALL_OF_HEROES'=>'HALL OF HEROES',
    'play_demo'=>'PLAY DEMO',
    'start_game'=>'START GAME',


    //market
    'selected_skins'=>'Selected skins',
    'remove_all'=>'Remove all',

    //header
    'header_tower'=> 'TOWER',
    'header_market'=>'WITHDRAW',

    //profile

    'market_stories'=>'Market stories'

];
