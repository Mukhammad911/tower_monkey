<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Models\Drop;

class ExpiredDrops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sell skins in pending status was dropped more than 24 hours ago';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
	        DB::beginTransaction();

            $expired = Drop::whereRaw("created_at < DATE_ADD(NOW(), INTERVAL -0 HOUR)")
                ->where('status', 'pending')
	            ->whereNotActiveCardExists()
	            ->sharedLock()
                ->get();

            foreach ($expired as $drop) {
                $drop->sell('expired sell skin drop');
            }

	        DB::commit();

            $this->info('Expired drops were sold: '.count($expired));
        }
        catch(\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
