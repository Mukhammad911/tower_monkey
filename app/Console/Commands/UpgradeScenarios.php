<?php

namespace App\Console\Commands;

use App\Models\UpgradeScenario;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class UpgradeScenarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upgrade-scenarios:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $scenarios = UpgradeScenario::all();

	    try {
		    foreach ($scenarios as $scenario){
			    Redis::command('DEL',[
				    'upgrade-scenario-'.$scenario->user_id
			    ]);
		    }
		    $this->info('Counts successfully cleared');
	    } catch (\Exception $e){
		    $this->info($e->getMessage());
	    }
    }
}
