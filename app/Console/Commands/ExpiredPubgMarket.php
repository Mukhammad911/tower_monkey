<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Models\PubgDropsMarket;

class ExpiredPubgMarket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marketexpire-pubg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sell skins in Market table dota pending status was dropped more than 10 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $expired = PubgDropsMarket::whereRaw("created_at < DATE_ADD(NOW(), INTERVAL 55 MINUTE)")
                ->where('status', 'pending')
                ->sharedLock()
                ->get();

            foreach ($expired as $drop) {
                $drop->sell('expired sell market pubg skin');
            }

            DB::commit();

            $this->info('Expired dota drops from market were sold: '.count($expired));
        }
        catch(\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
