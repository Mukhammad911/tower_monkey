<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class ClearWinStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winstats:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear win stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Redis::set('stats_count', 0);
            Redis::set('stats_summ', 0);
            Redis::set('stats_profit', 0);
            Redis::set('stats_jackpot_summ', 0);
            Redis::set('stats_jackpot_target', 2000000);

            $this->info('Wins stats successfully cleared');
        }
        catch (\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
