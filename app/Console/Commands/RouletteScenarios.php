<?php

namespace App\Console\Commands;

use App\Models\RouletteScenario;
use Illuminate\Console\Command;
use App\Models\SteamCase;
use Illuminate\Support\Facades\Redis;
use Config;

class RouletteScenarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roulette-scenarios:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear promotion redis counts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $scenarios = RouletteScenario::all();

        try {
            foreach ($scenarios as $scenario){
                Redis::command('DEL',[
	                'roulette-scenario-'.$scenario->case_id.'-'.$scenario->user_id
                ]);
            }
            $this->info('Counts successfully cleared');
        } catch (\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
