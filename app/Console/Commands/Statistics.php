<?php

namespace App\Console\Commands;

use App\CustomClasses\RouletteGame;
use App\CustomClasses\TowerGame;
use App\CustomClasses\UpgradeGame;
use App\Models\RouletteSetting;
use App\Models\TowerSetting;
use App\Models\UpgradeSetting;
use Illuminate\Console\Command;
use Statistic;
use Helper;

class Statistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

	/**
	 *
	 */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $games['roulette'] = $this->rouletteData();
	    $games['upgrade'] = $this->upgradeData();
	    $games['tower'] = $this->towerData();

	    foreach ( $games as $key => $data ) {
		    $this->saveData($key, $data);
	    }
    }

	/**
	 * @param $game
	 * @param $data
	 */
	private function saveData($game, $data)
	{
		foreach ( $data as $key => $value ) {
			$Statistic = new \App\Models\Statistic();
			$Statistic->key = $key;
			$Statistic->value = $value;
			$Statistic->game = $game;
			$Statistic->save();
		}
	}

	/**
	 * @return mixed
	 */
	private function rouletteData()
	{
		$data['income'] = RouletteGame::income();
		$data['expense'] = RouletteGame::expense();
		$target_company_percent = RouletteSetting::where('key', 'profit')->first()->value;
		$data['current_profit'] = Statistic::currentCompanyProfit($data['income'], $data['expense']);
		$data['profit_deviation_percent'] = (100 - $data['current_profit'] / ($target_company_percent / 100)) * (-1);

		return $data;
	}

	/**
	 * @return mixed
	 */
	private function upgradeData()
	{
		$data['income'] = UpgradeGame::income();
		$data['expense'] = UpgradeGame::expense();
		$target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
		$data['current_profit'] = Statistic::currentCompanyProfit($data['income'], $data['expense']);
		$data['profit_deviation_percent'] = (100 - $data['current_profit'] / ($target_company_percent / 100)) * (-1);

		return $data;
	}

	/**
	 * @return mixed
	 */
	private function towerData()
	{
		$data['income'] = TowerGame::income();
		$data['expense'] = TowerGame::expense();
		$target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
		$data['current_profit'] = Statistic::currentCompanyProfit($data['income'], $data['expense']);
		$data['profit_deviation_percent'] = (100 - $data['current_profit'] / ($target_company_percent / 100)) * (-1);

		return $data;
	}
}
