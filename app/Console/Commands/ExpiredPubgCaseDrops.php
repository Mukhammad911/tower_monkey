<?php

namespace App\Console\Commands;

use App\Models\PubgCases\PubgCaseDrops;
use DB;
use Illuminate\Console\Command;

class ExpiredPubgCaseDrops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pubgcaseexpire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sell skins in Market table pending status was dropped more than 5 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $expired = PubgCaseDrops::whereRaw("created_at < DATE_ADD(NOW(), INTERVAL -1380 MINUTE)")
                ->where('status', 'pending')
                ->sharedLock()
                ->get();

            foreach ($expired as $drop) {
                $drop->sell('expired sell market pubg case skin');
            }

            DB::commit();

            $this->info('Expired drops from pubg case were sold: '.count($expired));
        }
        catch(\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
