<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Models\CsgoCaseDrops;

class ExpiredCsgoCaseDrops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csgocaseexpire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sell skins in Market table pending status was dropped more than 5 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $expired = CsgoCaseDrops::whereRaw("created_at < DATE_ADD(NOW(), INTERVAL -1380 MINUTE)")
                ->where('status', 'pending')
                ->sharedLock()
                ->get();

            foreach ($expired as $drop) {
                $drop->sell('expired sell market csgo case skin');
            }

            DB::commit();

            $this->info('Expired drops from csgo case were sold: '.count($expired));
        }
        catch(\Exception $e){
            $this->info($e->getMessage());
        }
    }
}
