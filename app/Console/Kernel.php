<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
        Commands\Inspire::class,
        Commands\RouletteScenarios::class,
		Commands\UpgradeScenarios::class,
		Commands\TowerScenarios::class,
        Commands\ClearWinStats::class,
        Commands\ExpiredDrops::class,
        Commands\ExpiredDropsMarket::class,
        Commands\ExpiredDotaMarket::class,
        Commands\ExpiredPubgMarket::class,
		Commands\Statistics::class,
		Commands\RisePercent::class,
        Commands\ExpiredCsgoCaseDrops::class,
        Commands\ExpiredPubgCaseDrops::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('statistics')->cron('0 */4 * * *');
		$schedule->command('expired')->hourly();

        $schedule->command('marketexpire')->everyMinute();
        $schedule->command('marketexpire-dota')->everyMinute();
        $schedule->command('marketexpire-pubg')->everyMinute();
        $schedule->command('csgocaseexpire')->everyMinute();
        $schedule->command('pubgcaseexpire')->everyMinute();

		$schedule->command('rise_percent')->cron('*/4 * * * *');
	}
}
