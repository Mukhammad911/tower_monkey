<?php namespace App\Models;

use DB;
use Illuminate\Support\Facades\Redis;
use Config;
use Helper;

/**
 * Class RouletteScenarioAlgorithm
 * @package App\Models
 */
class RouletteScenarioAlgorithm
{
	/**
	 * @var SteamCase
	 */
    private $case;

	/**
	 * @var
	 */
    private $stats_count;

	/**
	 * @var
	 */
    private $scenario;

	/**
	 * @var User
	 */
    private $user;

	/**
	 * @param SteamCase $case
	 * @param User $user
	 * @param $scenario
	 */
    public function __construct(SteamCase $case, User $user, $scenario)
    {
        $this->targetProfit = RouletteSetting::where('key', 'profit')->first()->value;
        $this->case = $case;
        $this->user = $user;
	    $this->scenario = $scenario;

        $this->init();
    }

	/**
	 *
	 */
    private function init()
    {
        if(is_null(Redis::get('roulette-scenario-'.$this->case->id.'-'.$this->user->id))){
            Redis::set('roulette-scenario-'.$this->case->id.'-'.$this->user->id, 0);
        }

        $this->stats_count = Redis::get('roulette-scenario-'.$this->scenario->case_id.'-'.$this->scenario->user_id);
        $this->stats_count++;
    }

	/**
	 * @return mixed
	 */
    private function getDummy()
    {
	    $weapon = Product::where('id', $this->scenario->product_id)->first();

        return $weapon;
    }

	/**
	 * @return mixed
	 */
    public function get()
    {
        $weapon = $this->getDummy();

	    $Scenario = RouletteScenario::find($this->scenario->id);
	    $Scenario->status = $weapon ? 'completed' : 'not available';
	    $Scenario->save();

        Redis::set('roulette-scenario-'.$this->scenario->case_id.'-'.$this->scenario->user_id, $this->stats_count);
        
        return $weapon;
    }
}
