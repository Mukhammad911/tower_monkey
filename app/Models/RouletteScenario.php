<?php

namespace App\Models;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RouletteScenario
 * @package App\Models
 */
class RouletteScenario extends Model
{

    public function user()
    {
	    return $this->belongsTo('App\Models\User');
    }

	public function steamCase()
	{
		return $this->belongsTo('App\Models\SteamCase');
	}

	public function product()
	{
		return $this->belongsTo('App\Models\Product');
	}
}
