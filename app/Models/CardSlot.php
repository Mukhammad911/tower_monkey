<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardSlot extends Model
{
    protected $table = 'cards_slots';

    protected $with = ['product'];

    public function card() {
        return $this->belongsTo('App\Models\Card');
    }

    public function product() {
        return $this->belongsTo('\App\Models\Product');
    }

	/**
	 * @throws \Exception
	 */
    public function erase() {
        $card = $this->card;
        $slot = $this;
        $standinProductId = $card->getStandinProductId();
        if ( ! $standinProductId) {
            throw new \Exception('Cant determine standing product_id (Card ID: '.$card->id.')');
        }

        if ( ! CardSlot::query()
            ->where('id', $slot->id)
            ->where('visible', FALSE)
            ->update([
                'product_id' => $standinProductId,
                'visible'    => true,
            ])) {
            throw new \Exception('CardSlot update to visible failed (Card ID: '.$card->id.')');
        }
        if ( ! Card::query()
            ->where('id', $card->id)
            ->where('count_slots_visible', $card->count_slots_visible)
            ->update([
                'count_slots_visible' => $card->count_slots_visible + 1,
            ])) {
            throw new \Exception('Card update data failed. (Card ID: '.$card->id.')');
        }
        if ($slot->extra) {
            $slot = CardSlot::find($slot->id);
            $card->win($slot->product_id);
            return;
        }
        $card = Card::find($card->id);
        if ($card->count_slots_visible < ($card->extended ? 4 : 3)) {
            return;
        }
        $maxEqualVisibleInfo = $card->getDefaultSlotsProductsInfo();
        if ( ! in_array($maxEqualVisibleInfo->count, [1, 2, 3])) {
            throw new \Exception('Invalid maxEqualVisibleInfo');
        }
        if ($maxEqualVisibleInfo->count == 3) {
            $card->win($maxEqualVisibleInfo->product_id);
            return;
        }
        if ( ! Card::query()
            ->where('id', $card->id)
            ->where('status', 'erasing1')
            ->update([
                'status' => (($maxEqualVisibleInfo->count == 2) && ( ! $card->extended)) ? 'choosing' : 'erasing2',
            ])) {
            throw new \Exception('Card update status failed. (Card ID: '.$card->id.')');
        }
    }

}
