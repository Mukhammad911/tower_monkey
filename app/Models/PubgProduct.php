<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class PubgProduct extends Model {

    public static $classList = [];

    public function cases()
    {
        return $this->belongsToMany('App\Models\PubgCases\PubgCase', 'pubg_case_product', 'product_id', 'case_id');
    }

    public function availableProducts() {
        return $this->hasMany('App\Models\PubgAvailableProduct');
    }

    public function pubgCaseAvailableProducts() {
        return $this->hasMany('App\Models\PubgCases\PubgCaseAvailableProduct', 'product_id', 'id');
    }
}
