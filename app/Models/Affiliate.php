<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    public function user(){
	    return $this->belongsTo('App\Models\User', 'user_id');
    }

	public function refer(){
		return $this->belongsTo('App\Models\User', 'refer_id');
	}
}
