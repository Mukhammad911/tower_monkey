<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TowerSetting
 * @package App\Models
 */
class TowerSetting extends Model
{
	/**
	 * @param $level
	 *
	 * @return float|int
	 */
	public static function getMultiplier($level)
	{
		$multiplier = 0;
		$setting = TowerSetting::where('key', $level.' level prize')->first();

		if($setting){
			$multiplier = $setting->value / 100 + 1;
		}

		return $multiplier;
	}
}
