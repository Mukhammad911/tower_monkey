<?php

namespace App\Models;

use Statistic;
use App\CustomClasses\UpgradeGame;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


/**
 * Class UpgradeAlgorithm
 * @package App\Models
 */
class UpgradeAlgorithm
{
	private $total_income;
	private $total_expense;
	private $current_company_percent;
	private $profit_deviation_percent;
	private $real_company_percent;
	private $target_company_percent;
	private $min_percent = 5;
	private $max_percent = 98;

	private $bet;
	private $profit;
	private $multiplier;
	private $chance;

	/**
	 * @param bool $bet
	 * @param bool $profit
	 */
	public function __construct($bet , $profit, $last_balance)
	{
        $this->bet = $bet;
        $this->profit = $profit;

        Log::info((float)$last_balance);
        Log::info((float)$bet);
        Log::info((float)$profit);

        if((float)$last_balance == 0){
            $this->total_income = UpgradeGame::income();
            $this->total_expense = UpgradeGame::expense();
            $this->target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();
            $this->calcChance();
            $this->calcMultiplier();
            Log::info('last_balance = 0. Upgrade: user_id: '.Auth::user()->id);
        }
        else if(((float)$bet < 1) && ((float)$last_balance>0)){
            Log::info((float)$last_balance);
            Log::info($this->profit);
            $this->total_income = 10;
            $this->total_expense = 100000;
            $this->target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();
            $this->calcChance();
            $this->calcMultiplier();
            Log::info('Promo bypass when bet < 1. Upgrade: user_id: '.Auth::user()->id);
        }
        else if(((float)$last_balance - $this->profit) <= 0){
            Log::info((float)$last_balance);
            Log::info($this->profit);
            $this->total_income = 10;
            $this->total_expense = 100000;
            $this->target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();
            $this->calcChance();
            $this->calcMultiplier();
            Log::info('Promo bypass. Upgrade: user_id: '.Auth::user()->id);
        }

        else{
            $this->total_income = UpgradeGame::income();
            $this->total_expense = UpgradeGame::expense();
            $this->target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();
            $this->calcChance();
            $this->calcMultiplier();
            Log::info('Algorithm. Upgrade: user_id: '.Auth::user()->id);
        }


	}

	/**
	 * @return bool
	 */
    public function result()
    {
	    return $this->random($this->chance);
    }

	/**
	 * @return float
	 */
	public function stabilizedCompanyPercent()
	{
		if($this->real_company_percent < 0
		   || ($this->real_company_percent < $this->target_company_percent && $this->profit_deviation_percent > 100) ){
			return $this->max_percent;
		}

		if($this->real_company_percent > $this->target_company_percent && $this->profit_deviation_percent > 100){
			return $this->min_percent;
		}

		if($this->real_company_percent > $this->target_company_percent){
			$range_step = ($this->target_company_percent - $this->min_percent) / 100;
			$correction = $range_step * $this->profit_deviation_percent;
			$corrected_company_percent = round($this->target_company_percent - $correction, 2);
		}else{
			$range_step = ($this->max_percent - $this->target_company_percent) / 100;
			$correction = $range_step * $this->profit_deviation_percent;
			$corrected_company_percent = round($this->target_company_percent + $correction, 2);
		}

		return $corrected_company_percent;
	}

	private function calcChance()
	{
		// base chance
		$chance = $this->bet / $this->profit;

		// final chance
		$this->chance = $chance  * (100 - $this->current_company_percent);
	}

	/**
	 *
	 */
	private function calcMultiplier()
	{
		$this->multiplier = $this->profit / $this->bet;
	}

	/**
	 * @param $chance
	 *
	 * @return bool
	 */
	private function random($chance)
	{
		$random = mt_rand(1, 100);

		switch(true){
			case $random <= $chance:
				$result = TRUE;
				break;

			default:
				$result = FALSE;
				break;
		}

		return $result;
	}
}
