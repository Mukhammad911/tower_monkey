<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoinFlip extends Model
{
    protected $table = 'coin_flip_games';
}
