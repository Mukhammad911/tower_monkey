<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class DotaProduct extends Model {

    public static $classList = ['common', 'uncommon', 'rare', 'mythical', 'legendary', 'immortal', 'ancient', 'arcana'];

    public function cases()
    {
        return $this->belongsToMany('App\Models\SteamCase', 'case_product', 'product_id', 'case_id');
    }

    public function availableProducts() {
        return $this->hasMany('App\Models\DotaAvailableProduct');
    }
}
