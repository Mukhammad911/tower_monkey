<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PurchasePrice extends Model {

    protected $guarded = ['id'];
    protected $fillable = ['product_id', 'quality', 'price'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
