<?php

namespace App\Models;

use App\CustomClasses\RouletteGame;
use Auth;
use DB;
use Helper;
use Illuminate\Support\Facades\Log;
use ProvablyFair;
use Statistic;
use Illuminate\Support\Facades\Session;

/**
 * Class RouletteAlgorithm
 * @package App\Models
 */
class RouletteAlgorithm
{
	private $total_income;
	private $total_expense;
	private $target_company_percent;
	private $real_company_percent;
	private $profit_deviation_percent;

	private $case;
	private $case_price;
	private $products;

	private $win_numbers_range = 100000;
	private $flag_corrected = false;

	/**
	 * @param SteamCase $case
	 */
	public function __construct(SteamCase $case,$last_balance)
	{
        $this->case = $case;
        $this->case_price = $this->case->priceDefault->price;
	    if((float)$last_balance >= $this->case_price){
            $this->total_income = 10;
            $this->total_expense = 100000;
            $this->target_company_percent = RouletteSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            Log::info('Promo bypass. Roulette: user_id: '.Auth::user()->id);
        }
        else{
            $this->total_income = RouletteGame::income();
            $this->total_expense = RouletteGame::expense();
            $this->target_company_percent = RouletteSetting::where('key', 'profit')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            Log::info('Algorithm. Roulette: user_id: '.Auth::user()->id);
        }

	}

	/**
	 * @return mixed
	 */
	public function get()
	{
		// get products
		$this->getProducts();
		if(!$this->products){
			Log::info('There are no products in case: '.$this->case->id.'; User: '.Auth::user()->id);
			return false;
		}

		// calculate random win number and get prize
		$prize = $this->getPrize(0, $this->win_numbers_range);

		// return win product
		return $prize;
	}

	/**
	 *
	 */
	private function getProducts()
	{
		$this->products = ProvablyFair::getWinNumbers($this->case->id);
	}

	/**
	 * @param $prize
	 * @param $start_rand
	 * @param $end_rand
	 *
	 * @return array|bool
	 */
	private function chanceCorrection($prize, $start_rand, $end_rand)
	{
		$this->flag_corrected = true;
		$correction = ($this->profit_deviation_percent > 99 ) ? 99 : $this->profit_deviation_percent;

		if($this->real_company_percent < $this->target_company_percent){ // deficit
			if(($prize->price / 100) < $this->case_price ){
				return false;
			}
			$end_rand = $end_rand - ( $correction * $this->win_numbers_range/100 );
		}else{ // profit
			if(($prize->price / 100) > $this->case_price ){
				return false;
			}


			$start_rand = $start_rand + ( $correction * $this->win_numbers_range/100 );
		}

		if($start_rand >= $end_rand){
			$start_rand = $end_rand / 100 * 80;
		}

		return [
			'start_random' => abs($start_rand),
			'end_random' => abs($end_rand)
		];
	}

	/**
	 * @param int $start_random
	 * @param int $end_random
	 *
	 * @return mixed
	 */
	private function getPrize($start_random = 0, $end_random)
	{
		$max_price = ($this->total_income - $this->total_expense) / 5;
		// secure from company profit drawdown
		foreach ( $this->products as $key => $product ) {
			$product_price = $product->price / 100;
			if( $product_price > $this->case_price && $product_price > $max_price ){
				if($product->win_range_min < $end_random){
					$end_random = $product->win_range_min;
				}
			}
		}
		$win_number = mt_rand($start_random, $end_random);

		foreach($this->products as $key => $product){
			if($product->win_range_min <= $win_number && $win_number <= $product->win_range_max){
				$prize = $product;
				break;
			}
		}

		if($this->flag_corrected == false && $correction = $this->chanceCorrection($prize, $start_random, $end_random)){
			return $this->getPrize($correction['start_random'], $correction['end_random']);
		}

		return $prize;
	}





}
