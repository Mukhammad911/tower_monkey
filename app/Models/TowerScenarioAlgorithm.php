<?php

namespace App\Models;

use Illuminate\Support\Facades\Redis;

/**
 * Class TowerScenarioAlgorithm
 * @package App\Models
 */
class TowerScenarioAlgorithm
{
	/**
	 * @var
	 */
	private $user;

	/**
	 * @var
	 */
	private $scenarios;

	/**
	 * @var
	 */
	private $level;

	/**
	 * @var
	 */
	private $bet;

	/**
	 * @var
	 */
	private $current_step;

	/**
	 * @var
	 */
	private $demo;

	/**
	 * @var
	 */
	private $current_attempt;

	/**
	 * @var
	 */
	private $current_attempt_any;


	/**
	 * TowerScenarioAlgorithm constructor.
	 *
	 * @param $user
	 * @param $level
	 * @param $bet
	 */
    public function __construct($user, $level, $bet)
    {
	    $this->user = $user;
	    $this->level = $level;
	    $this->bet = $bet;

    }

	/**
	 *
	 */
	public function updateRedisCounters()
	{
		// current level
		if(is_null(Redis::get('tower-scenario-'.$this->level.'-'.$this->user->id))){
			Redis::set('tower-scenario-'.$this->level.'-'.$this->user->id, 0);
		}
		$this->current_attempt = Redis::get('tower-scenario-'.$this->level.'-'.$this->user->id);

		// Any level
		if(is_null(Redis::get('tower-scenario-anyone-'.$this->user->id))){
			Redis::set('tower-scenario-anyone-'.$this->user->id, 0);
		}
		$this->current_attempt_any = Redis::get('tower-scenario-anyone-'.$this->user->id);

		if($this->bet < 1500){
			Redis::set('tower-scenario-'.$this->level.'-'.$this->user->id, $this->current_attempt + 1);
			Redis::set('tower-scenario-anyone-'.$this->user->id, $this->current_attempt_any + 1);
		}
	}

	/**
	 * @param $scenarios
	 * @param $current_step
	 * @param $demo
	 *
	 * @return string
	 */
	public function play($scenarios, $current_step, $demo)
	{
		$this->scenarios = $scenarios;
		$this->demo = $demo;
		$this->current_step = $current_step;

		if($this->bet < 1500){
			foreach ( $this->scenarios as $scenario ) {
				$this->current_attempt = Redis::get('tower-scenario-'.$this->level.'-'.$this->user->id);
				$this->current_attempt_any = Redis::get('tower-scenario-anyone-'.$this->user->id);
				if(
					($scenario->level != 'anyone' && $this->current_attempt == $scenario->attempt) ||
					($scenario->level == 'anyone' && $this->current_attempt_any == $scenario->attempt)
				) {
					if($this->current_step == $scenario->step){
						$Scenario         = TowerScenario::find( $scenario->id );
						$Scenario->status = 'completed';
						$Scenario->save();

						return 'loss';
					}

					return 'victory';
				}
			}
		}else{
			//
		}

		$TowerAlgorithm = new TowerAlgorithm($this->bet);
		return $TowerAlgorithm->play($this->level, $this->bet, $this->demo);
	}


}
