<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateLevel extends Model
{
	public function user()
	{
		return $this->hasMany('App\Models\User', 'level', 'user_id');
	}

	public function level()
	{
		return $this->belongsTo('App\Models\User');
	}
}
