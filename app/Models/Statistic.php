<?php

namespace App\Models;

use Statistic as Stat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Statistic
 * @package App\Models
 */
class Statistic extends Model
{
	/**
	 * @param $game
	 * @param $parameter
	 *
	 * @return array
	 */
    public static function getChartData($game, $parameter)
    {
	    $statistics = self::where('key', $parameter);
	    if($game != ''){
		    $statistics = $statistics->where('game', $game);
	    }
	    $statistics = $statistics->where('created_at', '>', Carbon::now()->subMonth())->get();

	    $chart_data = [];
	    $i = 0;
	    $time = 0;
	    $value = 0;
	    foreach ($statistics as $statistic) {
		    if( (strtotime($statistic->created_at) * 1000) == $time){
			    $value += floatval($statistic->value);
			    $chart_data[$i - 1] = [$time, $value];
		    }else{
			    $time = strtotime($statistic->created_at) * 1000;
			    $value = floatval($statistic->value);

			    $chart_data[$i] = [$time, $value];

			    $i++;
		    }
	    }

	    return $chart_data;
    }

	/**
	 * @param $income_data
	 * @param $expense_data
	 *
	 * @return array
	 */
	public static function getChartDataTotalCurrentProfit($income_data, $expense_data)
	{
		$chart_data = [];
		foreach($income_data as $key => $row){
			$time = $row[0];
			$value = round(Stat::currentCompanyProfit($row[1], $expense_data[$key][1]), 2);

			$chart_data[] = [$time, $value];
		}

		return $chart_data;
	}



}
