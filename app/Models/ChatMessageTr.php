<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessageTr extends Eloquent
{
    protected $table = 'messages_tr';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
