<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessageEs extends Eloquent
{
    protected $table = 'messages_es';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
