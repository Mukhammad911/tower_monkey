<?php

namespace App\Models;

use App\CustomClasses\RouletteGame;
use Auth;
use DB;
use Helper;
use Illuminate\Support\Facades\Log;
use Statistic;

/**
 * Class Win2
 * @package App\Models
 */
class Win2
{
	private $total_income;
	private $total_expense;
	private $target_company_percent;
	private $current_company_percent;
	private $profit_deviation_percent;

	private $high_quota_range;
	private $middle_quota_range;
	private $high_quota_chance;
	private $middle_quota_chance;
	private $low_quota_chance;

	private $case;
	private $case_price;
	private $high_weapon;
	private $middle_weapon;
	private $low_weapon;

	/**
	 * @param SteamCase $case
	 */
    public function __construct(SteamCase $case)
    {
	    $this->total_income = RouletteGame::income();
	    $this->total_expense = RouletteGame::expense();
	    $this->target_company_percent = RouletteSetting::where('key', 'profit')->first()->value;
	    $this->current_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
	    $this->profit_deviation_percent = abs(100 - $this->current_company_percent / ($this->target_company_percent / 100));

	    $this->high_quota_range = [
		    'min' => Helper::setting('high_quota_range_min'), // min possible percent of chance to win a high quota weapon
		    'basic' => Helper::setting('high_quota_range_basic'), // default possible percent of chance to win a high quota weapon
		    'max' => Helper::setting('high_quota_range_max') // max possible percent of chance to win a high quota weapon
	    ];
	    $this->middle_quota_range = [
		    'min' => Helper::setting('middle_quota_range_min'), // min possible percent of chance to win a middle quota weapon
		    'basic' => Helper::setting('middle_quota_range_basic'), // default possible percent of chance to win a middle quota weapon
		    'max' => Helper::setting('middle_quota_range_max') // max possible percent of chance to win a middle quota weapon
	    ];

	    $this->high_quota_chance = $this->getChance('high');
	    $this->middle_quota_chance = $this->getChance('middle');
	    $this->low_quota_chance = $this->getChance('low');

	    /*Log::info("income:                      $".$this->total_income);
	    Log::info("expense:                     $".$this->total_expense);
	    Log::info("target profit:               ".$this->target_company_percent.'%');
	    Log::info("current profit:              ".$this->current_company_percent.'%');
	    Log::info("profit deviation percent:    ".$this->profit_deviation_percent.'%');
	    Log::info("high quota chance:           ".($this->high_quota_chance/100).'%');
	    Log::info("middle quota chance:         ".($this->middle_quota_chance/100).'%');
	    Log::info("low quota chance:            ".($this->low_quota_chance/100).'%');
	    Log::info("\n -------------");*/

	    $this->case = $case;
	    $this->case_price = $this->case->priceDefault->price * 100;
	    $this->high_weapon = Helper::getWeapon(2, $this->case->id, $this->case_price);
	    $this->middle_weapon = Helper::getWeapon(1, $this->case->id, $this->case_price);
	    $this->low_weapon = Helper::getWeapon(0, $this->case->id, $this->case_price);
    }

	/**
	 * @return mixed
	 */
	public function get()
	{
		if(!$this->low_weapon){
			Log::info('There are no low quota weapons. Case: '.$this->case->id.'; User: '.Auth::user()->id);
			return false;
		}

		$quota = $this->getQuota();
		switch(true){
			case ($quota == 2):
				if($this->high_weapon){
					$weapon = $this->high_weapon;
				}elseif($this->middle_weapon){
					$weapon = $this->middle_weapon;
				}else{
					$weapon = $this->low_weapon;
				}
				break;

			case ($quota == 1):
				if($this->middle_weapon){
					$weapon = $this->middle_weapon;
				}else{
					$weapon = $this->low_weapon;
				}
				break;

			default:
				$weapon = $this->low_weapon;
				break;
		}

		return $weapon;
	}



	/**
	 * @param $quota_type
	 *
	 * @return float|int
	 */
	public function getChance($quota_type)
	{
		switch($quota_type){
			case 'high':
				$chance = $this->calculateChance(
					$this->high_quota_range['min'],
					$this->high_quota_range['basic'],
					$this->high_quota_range['max']
				);
				break;

			case 'middle':
				$chance = $this->calculateChance(
					$this->middle_quota_range['min'],
					$this->middle_quota_range['basic'],
					$this->middle_quota_range['max']
				);
				break;

			default:
				$chance = (10000 - $this->middle_quota_chance - $this->high_quota_chance) / 100;
				break;
		}

		return $chance * 100;
	}

	/**
	 * @param $range_min
	 * @param $range_basic
	 * @param $range_max
	 *
	 * @return float
	 */
	private function calculateChance($range_min, $range_basic, $range_max)
	{
		if($this->current_company_percent < 0){
			return $range_min;
		}

		if($this->current_company_percent > $this->target_company_percent){
			$quota_range_step = ($range_max - $range_basic) / 100;
			$quota_correction = $quota_range_step * $this->profit_deviation_percent;
			$chance = round($range_basic + $quota_correction, 2);
		}else{
			$quota_range_step = ($range_basic - $range_min) / 100;
			$quota_correction = $quota_range_step * $this->profit_deviation_percent;
			$chance = round($range_basic - $quota_correction, 2);
		}
		if($chance < 0){
			$chance = $range_min;
		}
		if($chance > $range_max){
			$chance = $range_max;
		}

		return $chance;
	}

	/**
	 * @return int
	 */
	private function getQuota()
	{
		$random = mt_rand(1, 10000);

		switch($random){
			// High quota
			case ($random > 0 && $random <= $this->high_quota_chance) :
				$quota = 2;
				break;
			// Middle quota
			case ($random > $this->high_quota_chance && $random <= ($this->high_quota_chance + $this->middle_quota_chance)) :
				$quota = 1;
				break;
			// Low quota
			default:
				$quota = 0;
				break;
		}

		return $quota;
	}


}
