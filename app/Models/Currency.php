<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Currency extends Model {

    protected $table = 'currency';

    public static $default = ['USD','RUB'];

    public function scopeGetDefault($query){
        return $query->where('code',Config::get('app.currency'))->first();
    }

    public function scopeGetActive($query){
        return $query->where('status',1)->get();
    }
}