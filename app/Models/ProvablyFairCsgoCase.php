<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvablyFairCsgoCase extends Model
{
    protected $table = 'provably_fair_csgo_case';
    protected $fillable = ['product_id', 'case_id', 'chance', 'start', 'end'];

    public $timestamps = false;
}
