<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveTower extends Model
{

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function tower()
	{
		return $this->belongsTo('App\Models\Tower', 'tower_id', 'id');
	}
}
