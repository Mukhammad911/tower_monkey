<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessageDe extends Eloquent
{
    protected $table = 'messages_de';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
