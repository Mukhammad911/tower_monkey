<?php

namespace App\Models\PaymentSystems;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Config;
use App\Models\Order;
use Illuminate\Support\Facades\Log;

class PayOp {
    const API_URL           = 'https://payop.com';
    const API_VERSION       = 'v1';
    const API_KEY           = 'application-84730856-fbab-41a2-a1f5-3449b20686ee';
    const API_SECRET        = 'f446c2360f77265e51cf7b0a';
    const DEFAULT_CHARSET   = 'utf-8';
    const DEFAULT_LANGUAGE  = 'en';
    const SUCCESS_URL       = 'https://csgotower.net/pay/return?status=success&invoiceId={{invoiceId}}&txid={{txid}}';
    const FAIL_URL          = 'https://csgotower.net/pay/return?status=failed&invoiceId={{invoiceId}}&txid={{txid}}';
    const MERCHANT_LOGIN    = 'dGthY2hlbmtvZGltaXRyaXlAZ21haWwuY29t';
    const MERCHANT_PASS     = 'UGF5b3AjMjIxMTc5QA==';

    private $token;

    /**
     * PayOp constructor.
     */
    public function __construct()
    {
        $this->_login();
    }

    /**
     * @param $fields
     *
     * @return bool|mixed|string
     */
    public function createInvoice($fields)
    {
        $data = array_merge($fields, [
            'publicKey' => self::API_KEY,
            'language'  => self::DEFAULT_LANGUAGE,
            'resultUrl' => self::SUCCESS_URL,
            'failPath'  => self::FAIL_URL,
            'signature' => $this->_buildSignature($fields['order']),
        ]);
        $data['order']['items'] = [[
            'id' => '1',
            'name' => 'Personal wallet balance refill',
            'price' => $data['order']['amount']
        ]];
        $data['order']['description'] = 'Skins market wallet balance refill';

        return $this->_query('invoices/create', $data, 'POST', true);
    }

    /**
     * @param $txid
     *
     * @return bool|mixed|string
     */
    public function getTransactions($txid)
    {
        return $this->_query('transactions/'. $txid, [], 'GET', true);
    }

    /**
     * @param $order
     *
     * @return string
     */
    private function _buildSignature($order)
    {
        ksort($order, SORT_STRING);
        $dataSet = array_values($order);
        $dataSet[] = self::API_SECRET;

        return hash('sha256', implode(':', $dataSet));
    }

    /**
     * @return bool
     */
    private function _login() {
        $data = [
            "email" => base64_decode(self::MERCHANT_LOGIN),
            "password"  =>  base64_decode(self::MERCHANT_PASS)
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL .'/'. self::API_VERSION .'/users/login');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json;charset='. self::DEFAULT_CHARSET,
            'Accept: application/json',
            ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $response = curl_exec($ch);
        $headers = [];

        foreach (explode("\r\n", substr($response, 0, curl_getinfo($ch, CURLINFO_HEADER_SIZE))) as $indx => $row) {
            if(0 === $indx) continue;
            if(empty($row)) continue;
            list($key, $value) = explode(': ', $row);
            $headers[$key] = $value;
        }
        curl_close($ch);
        $this->token = $headers['token'];

        return true;
    }

    /**
     * @param        $endpoint
     * @param array  $data
     * @param string $method
     * @param bool   $json
     *
     * @return bool|mixed|string
     */
    private function _query($endpoint, $data=[], $method='POST', $json=true)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL .'/'. self::API_VERSION .'/' . $endpoint);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if($json) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json;charset='. self::DEFAULT_CHARSET,
                'Accept: application/json',
                'token: '. $this->token,
            ]);
        }
        if('POST' == $method) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        $response = curl_exec($ch);
        #$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ($json) ? json_decode($response) : $response ;
    }
}
