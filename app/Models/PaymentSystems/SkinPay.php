<?php
namespace App\Models\PaymentSystems;

use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

/**
 * Class SkinPay
 * @package App\Models\PaymentSystems
 */
class SkinPay
{
	private $BASE_URL;
	private $PUBLIC_KEY;
	private $PRIVATE_KEY;

	public function __construct()
	{
		$this->BASE_URL = Config::get('skin-pay.url');
		$this->PUBLIC_KEY = Config::get('skin-pay.public_key');
		$this->PRIVATE_KEY = Config::get('skin-pay.private_key');
	}

	/**
	 * @param $order_id
	 *
	 * @return string
	 */
	public function createOrder($order_id)
	{
		$user = Auth::user();
		$steam_token = '';
		if(!empty($user->trade_url)){
			$trade_url = parse_url($user->trade_url);
			parse_str($trade_url['query'], $trade_url);
			$steam_token = $trade_url['token'];
		}

		$parameters = [
			'key' => $this->PUBLIC_KEY,
			'orderid' => $order_id,
			'userid' => $user->steamid,
			't_token' => $steam_token,
			'currency' => 'usd',
			'lang' => 'en',
		];

		$parameters['sign'] = $this->getSignature($parameters);

		return $this->BASE_URL.'/deposit?'.http_build_query($parameters);
	}

	/**
	 * @param $parameters
	 *
	 * @return string
	 */
	private function getSignature($parameters)
	{
		$paramsString = '';

		ksort($parameters);
		foreach ($parameters as $key => $value) {
			if($key == 'sign') continue;
			$paramsString .= $key .':'. $value .';';
		}

		return hash_hmac('sha1', $paramsString, $this->PRIVATE_KEY);
	}
} 