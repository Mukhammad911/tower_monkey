<?php namespace App\Models\PaymentSystems;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use App\Models\Order;
use Illuminate\Support\Facades\Log;

class G2APay {
    private $quote_url;
    private $secret;
    private $api_hash;
    private $url_ok;
    private $url_failure;
    private $item_url;

    public $redirect_url;
    public $order_id;
    public $amount;
    public $currency;
    public $sku;
    public $security_steam_id;
    public $security_registration_date;
    public $security_new_client;
    public $security_first_transaction_date;

    public function __construct()
    {
        $this->quote_url    = Config::get('g2pay.quote_url');
        $this->redirect_url = Config::get('g2pay.redirect_url');

        $this->secret       = Config::get('g2pay.secret');
        $this->api_hash     = Config::get('g2pay.api_hash');
        $this->url_ok       = Config::get('g2pay.url_ok');
        $this->url_failure  = Config::get('g2pay.url_failure');
        $this->item_url     = Config::get('g2pay.item_url');
    }

    public function getToken(){
        $hash = $this->g2pay_hash($this->order_id.$this->amount.$this->currency.$this->secret);

        $data = [
            'api_hash'      => $this->api_hash,
            'hash'          => $hash,
            'order_id'      => $this->order_id,
            'amount'        => $this->amount,
            'currency'      => $this->currency,
            'url_ok'        => $this->url_ok,
            'url_failure'   => $this->url_failure,
            'security_steam_id'                 => $this->security_steam_id,
            'security_user_logged_in'           => 1,
            'security_registration_date'        => $this->security_registration_date,
            'security_user_whitelisted'         => 0,
            'security_new_client'               => $this->security_new_client,
            'security_first_transaction_date'   => $this->security_first_transaction_date,
            'items'         => json_encode([["sku"  =>  $this->sku,
                                            'name'  =>  "$this->amount $this->currency",
                                            'amount'=>  $this->amount,
                                            'qty'   =>  1,
                                            'id'    =>  1,
                                            'price' =>  $this->amount,
                                            'url'   =>  $this->item_url]])
        ];

        $client = new Client();
        $request = $client->post($this->quote_url, ['form_params'=>$data]);
        $body = $request->getBody();

        $content = json_decode($body->getContents(), true);

        return $content;
    }

    public function validate_hash($order_id, $amount, $transactionId, $hash)
    {
        $validHash = $this->generate_hash($order_id, $amount, $transactionId);
        return $validHash === $hash;
    }

    public function generate_hash($order_id, $amount, $transaction_id){
        return $this->g2pay_hash($transaction_id . $order_id . $amount . $this->secret);
    }

    private function g2pay_hash($str){
        return hash('sha256', $str);
    }
}
