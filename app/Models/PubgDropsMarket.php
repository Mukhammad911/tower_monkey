<?php

namespace App\Models;

use App\Models\PubgAvailableProduct;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Helper;
use DB;
use App\Models\User;

class PubgDropsMarket extends Model
{
    protected $table = 'pubg_drops';

    public function user()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

    public function bot()
    {
        return $this->hasOne('App\Models\PubgBot','id', 'bot_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\PubgProduct', 'id', 'product_id');
    }

    public function sell($description = 'sell skin pubg drop')
    {
        DB::beginTransaction();

        if($this->toxic == 0) {
            $this->replaceDrop();
        }

        $this->status 		= 'sold';
        $this->assetid 		= 0;
        $this->classid 		= 0;
        $this->instanceid 	= 0;
        $this->save();

        $sell_price = Helper::exchange($this->price, 1);

        User::find($this->user_id)
            ->balance()
            ->where('currency_id', 1)
            ->where('type', 0)
            ->sharedLock()
            ->update(['summ'=>DB::raw("summ + ".$sell_price)]);

        /*$user_balance = Balance::where('user_id', $this->user_id)
            ->where('currency_id', $this->currency_id)
            ->where('type', 0)->first()->summ;*/
        //Helper::operation($sell_price, $this->currency_id, $description, $this->user_id, $user_balance);

        DB::commit();

        return $sell_price;
    }

    protected  function replaceDrop()
    {
        $av = new PubgAvailableProduct();
        $av->name               = $this->name;
        $av->stattrak           = $this->stattrak;
        $av->product_id         = $this->product_id;
        $av->price              = $this->price;
        $av->image_hash         = $this->image_hash;
        $av->market_name        = $this->market_name;
        $av->assetid            = $this->assetid;
        $av->classid            = $this->classid;
        $av->instanceid         = $this->instanceid;
        $av->bot_id             = $this->bot_id;
        $av->save();
    }
}
