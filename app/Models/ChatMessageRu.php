<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessageRu extends Eloquent
{
    protected $table = 'messages_ru';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
