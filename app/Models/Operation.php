<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Operation
 * @package App\Models
 */
class Operation extends Model
{
	public function user(){
		return $this->belongsTo('App\Models\User', 'user_id');
	}
}
