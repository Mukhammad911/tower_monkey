<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $fillable = ['payment_system', 'currency_id', 'amount', 'user_id', 'user_ip', 'status'];

    public static $statuses = ['pending', 'complete', 'rejected', 'cancelled', 'refunded', 'partial_refunded'];

    public function currency(){
        return $this->hasOne('App\Models\Currency', 'id', 'currency_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
