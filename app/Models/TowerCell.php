<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TowerCell
 * @package App\Models
 */
class TowerCell extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tower()
	{
		return $this->belongsTo('App\Models\Tower', 'tower_id');
	}

	/**
	 * @return \stdClass
	 */
	public static function getLastCell()
	{
		$last_game = Tower::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

		if(!$last_game || !count($last_game->cells)) {
			return false;
		}

		$last_cell     = new \stdClass();
		$last_cell->id = 0;
		foreach ( $last_game->cells as $cell ) {
			if ( $cell->id > $last_cell->id ) {
				$last_cell = $cell;
			}
		}

		return $last_cell;
	}
}
