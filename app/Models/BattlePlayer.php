<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BattlePlayer
 * @package App\Models
 */
class BattlePlayer extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function battle()
	{
		return $this->belongsTo('App\Models\Battle', 'battle_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\Battle', 'user_id', 'id');
	}
}
