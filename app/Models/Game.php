<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Game
 * @package App\Models
 */
class Game extends Model
{
	protected $fillable = [
		'user_id',
		'roulette_id',
		'upgrade_id',
		'tower_id',
	];

	public function user()
	{
		return $this->hasOne('App\Models\User', 'id', 'user_id');
	}

	public function roulette()
	{
		return $this->hasOne('App\Models\Drop', 'id', 'roulette_id');
	}

	public function upgrade()
	{
		return $this->hasOne('App\Models\Upgrade', 'id', 'upgrade_id');
	}

	public function tower()
	{
		return $this->hasOne('App\Models\Tower', 'id', 'tower_id');
	}
}
