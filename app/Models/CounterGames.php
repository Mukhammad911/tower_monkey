<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CounterGames extends Model
{
    protected $table = 'counter_games';
    public $timestamps = false;
}
