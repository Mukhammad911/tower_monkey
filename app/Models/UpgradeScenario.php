<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UpgradeScenario
 * @package App\Models
 */
class UpgradeScenario extends Model
{
	public function user()
	{
		return $this->hasOne('App\Models\User');
	}
}
