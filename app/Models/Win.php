<?php namespace App\Models;

use App\Models\SteamCase;
use DB;
use Helper;
use App\Models\Setting;
use App\Models\AvailableProduct;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;
use App\Models\LiveDrop;
use Illuminate\Support\Facades\Auth;


class Win{

    //Прибыль %
    private $targetProfit;

    //Резервный % прибыли для выдачи вещей дороже стоимости кейса
    private $reservProfit = 1;

    private $case;

    //Цена кейса в копейках
    private $case_price;

    private $cheapWeapon;

    private $expensiveWeapon;

    private $middleWeapon;

    private $stats;

    //Ожидаемая Средняя прибыль по всем кейсам
    private $profitAvarage;

    private $dropLevel;

    public function __construct(SteamCase $case)
    {
        $this->targetProfit = RouletteSetting::where('key', 'profit')->first()->value;
        $this->case = $case;

        $this->case_price = $this->case->priceDefault->price * 100;

        if(is_null(Redis::get('stats_count'))){
            $this->stats_init();
        }

        $this->stats = new \stdClass();
        $this->stats->count             = Redis::get('stats_count');
        $this->stats->summ              = Redis::get('stats_summ');
        $this->stats->profit            = Redis::get('stats_profit');
        $this->stats->jackpot_summ      = Redis::get('stats_jackpot_summ');
        $this->stats->jackpot_target    = Redis::get('stats_jackpot_target');

        //Реальная прибыль %
        if($this->stats->summ > 0){
            $this->profitAvarage = ($this->stats->profit / $this->stats->summ) * 100;
        }
        else{
            $this->profitAvarage = 0;
        }

        $this->init();
    }

    private function init(){
        $this->cheapWeapon  = $this->getByPrice(0);
        $this->middleWeapon = $this->getByPrice(1);
        $this->expensiveWeapon  = $this->getByPrice(2);
    }

    private function addPriceCondition(&$ret, $level){
        $price = $this->case_price * (100-$this->targetProfit)/100;

        switch($level){
            case 2:
                $ret = $ret->where('price','>=',$this->case_price * 2);
                break;
            case 1:
                $ret = $ret->where('price','>',$price);
                $ret = $ret->where('price','<=',$this->case_price * 2);
                break;
            default:
                $ret = $ret->where('price','<=',$price);
                break;
        }
    }

    private function getByPrice($level){
        $ret = DB::table('available_products')
                ->select('product_id')
                ->whereRaw('bot_id IN(SELECT id FROM bots WHERE enabled=1)')
                ->whereRaw("product_id NOT IN(SELECT id FROM products WHERE locked=1)")
                ->distinct();

        $this->addPriceCondition($ret, $level);

        $ret = $ret->whereRaw("product_id IN(SELECT product_id FROM case_product cp WHERE cp.case_id={$this->case->id})")
                ->get();

        $ret = collect($ret)->lists('product_id');

        return $ret;
    }

    private function addResultPriceCondition(&$ret, $level){
        $price = $this->case_price * (100-$this->targetProfit)/100;

        if($level === 1){
            $ret = $ret->where('price','<=',$this->case_price * 2);
        }
        elseif($level === 0){
            $ret = $ret->where('price','<=',$price);
        }
    }

    private function getRand($ids){
        $drop = mt_rand(0,count($ids)-1);

        if($this->dropLevel == 2){
            $this->stats->last_wins  = LiveDrop::all()->lists('product_id')->toArray();

            if(in_array($ids[$drop], $this->stats->last_wins)){
                $this->dropLevel = 1;
                $ids = $this->middleWeapon;
                $drop = mt_rand(0,count($ids)-1);
            }
        }

        $ret =  AvailableProduct::where('product_id',$ids[$drop])
                ->whereRaw('bot_id IN(SELECT id FROM bots WHERE enabled=1)');
                $this->addResultPriceCondition($ret, $this->dropLevel);

        $result = $ret->get()->random();
        return $result;
    }

    public function get(){

        if(!$this->cheapWeapon->count()){
            return false;
        }

        if(Auth::user()->drop_banned){
            $ids = $this->cheapWeapon;
            $this->dropLevel = 0;
        }
        else{
            switch(true){
	            case Redis::get('low_quota_'.Auth::user()->id) == true:
		            $ids = $this->cheapWeapon;
		            $this->dropLevel = 0;
		            break;

                case $this->stats->jackpot_summ >= $this->stats->jackpot_target:
                    switch(true){
                        case $this->expensiveWeapon->count():
                            $ids = $this->expensiveWeapon;
                            $this->dropLevel = 2;
                            break;
                        case $this->middleWeapon->count():
                            $ids = $this->getMiddleCollection();
                            $this->dropLevel = 1;
                            break;
                        default:
                            $ids = $this->cheapWeapon;
                            $this->dropLevel = 0;
                            break;
                    }

                    break;
                case $this->stats->count>50 && $this->profitAvarage > $this->targetProfit + $this->reservProfit:
                    if($this->middleWeapon->count()){
                        $ids = $this->getMiddleCollection();
                        $this->dropLevel = 1;
                    }
                    else{
                        $ids = $this->cheapWeapon;
                        $this->dropLevel = 0;
                    }

                    break;
                default:
                    $ids = $this->cheapWeapon;
                    $this->dropLevel = 0;
                    break;
            }
        }
        $weapon = $this->getRand($ids);
        $this->stats->count++;
        $profit = $this->case_price - $weapon->price;

        if($this->dropLevel != 2){
            if($profit > 0 && $this->stats->count%5 == 0){
                $this->stats->jackpot_summ += $profit;
            } else{
                $this->stats->profit += $profit;
            }
        } else{
            $this->stats->jackpot_summ = $this->stats->jackpot_summ - $weapon->price;
        }

        $this->stats->summ += $this->case_price;

	    if(!Redis::get('low_quota_'.Auth::user()->id)){
		    Redis::set('stats_count', $this->stats->count);
		    Redis::set('stats_summ', $this->stats->summ);
		    Redis::set('stats_profit', $this->stats->profit);
		    Redis::set('stats_jackpot_summ', $this->stats->jackpot_summ);
	    }

        return $weapon;
    }

    protected function getMiddleCollection(){
        $countCheap = $this->cheapWeapon->count();

        if($this->middleWeapon->count() > $countCheap){
            $middleResult = $this->middleWeapon->random($countCheap);
        }
        else{
            $middleResult = $this->middleWeapon;
        }

        $result = $this->cheapWeapon->merge($middleResult);

        return $result;
    }

    protected function stats_init(){
        Redis::set('stats_count',0);
        Redis::set('stats_summ',0);
        Redis::set('stats_profit',0);
        Redis::set('stats_jackpot_summ',0);
        Redis::set('stats_jackpot_target',2000000);
    }
}
