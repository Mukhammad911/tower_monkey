<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Category extends Model {

    public function cases()
    {
        return $this->hasMany('App\Models\SteamCase')->orderBy('sort');
    }

    public function pubgCases()
    {
        return $this->hasMany('App\Models\PubgCases\PubgCase')->orderBy('sort');
    }
}
