<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class DropStats
 * @package App\Models
 */
class DropStats extends Model
{
	public function user()
	{
		return $this->hasOne('App\Models\User','id', 'user_id');
	}

} 