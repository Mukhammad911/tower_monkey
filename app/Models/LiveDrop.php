<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LiveDrop
 * @package App\Models
 */
class LiveDrop extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

    public function product()
    {

        return $this->hasOne('App\Models\Product','id', 'product_id');
    }

    public function steamCase()
    {

        return $this->hasOne('App\Models\SteamCase','id', 'case_id');
    }
}
