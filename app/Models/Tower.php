<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tower
 * @package App\Models
 */
class Tower extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function cells()
	{
		return $this->hasMany('App\Models\TowerCell');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function liveTower()
	{
		return $this->hasOne('App\Models\LiveTower');
	}

	/**
	 * @return mixed
	 */
	public static function getCurrentGame()
	{
		$current_game = Tower::where('user_id', Auth::user()->id)
			->where('status', 'started')
			->lockForUpdate()
			->first();

		return $current_game;
	}
}
