<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CsgoCaseAvailableProduct extends Model
{
    protected $table = 'csgo_case_available_products';
}
