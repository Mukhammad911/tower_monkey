<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BattleCase
 * @package App\Models
 */
class BattleCase extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function battle()
	{
		return $this->belongsTo('App\Models\Battle', 'battle_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function steamCase()
	{
		return $this->belongsTo('App\Models\SteamCase', 'case_id', 'id');
	}
}
