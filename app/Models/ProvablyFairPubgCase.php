<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvablyFairPubgCase extends Model
{
    protected $table = 'provably_fair_pubg_case';
    protected $fillable = ['product_id', 'case_id', 'chance', 'start', 'end'];

    public $timestamps = false;
}
