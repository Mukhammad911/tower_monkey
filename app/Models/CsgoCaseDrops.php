<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Helper;


class CsgoCaseDrops extends Model
{
    protected $table = 'csgo_case_drops_market';


    public function user()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

    public function bot()
    {
        return $this->hasOne('App\Models\CsgoCaseBot','id', 'bot_id');
    }

    /**
     * @param string $description
     *
     * @return mixed
     */
    public function sell($description = 'csgo case sell')
    {
        if($this->toxic == 0) {
            $this->replaceDrop();
        }

        if ( ! DB::table('csgo_case_drops_market')
            ->where('id', $this->id)
            ->where('status', 'pending')
            ->update([
                'status' => 'sold',
                'assetid' => 0,
                'classid' => 0,
                'instanceid' => 0,
            ]))
        {
            throw new \Exception('Csgo Drop already sold');
        }

        $sell_price = Helper::exchange($this->price, 1);

        User::find($this->user_id)
            ->balance()
            ->where('currency_id', $this->currency_id)
            ->where('type', 0)
            ->sharedLock()
            ->update(['summ'=>DB::raw("summ + ".$sell_price)]);

        $user_balance = Balance::where('user_id', $this->user_id)
            ->where('currency_id', $this->currency_id)
            ->where('type', 0)->first()->summ;
        Helper::operation(-$sell_price, $this->currency_id, $description, $this->user_id, $user_balance);

        return $sell_price;
    }

    protected  function replaceDrop()
    {
        $av = new CsgoCaseAvailableProduct();
        $av->name               = $this->name;
        $av->short_description  = $this->short_description;
        $av->quality            = $this->quality;
        $av->stattrak           = $this->stattrak;
        $av->class              = $this->class;
        $av->product_id         = $this->product_id;
        $av->price              = $this->price;
        $av->image_hash         = $this->image_hash;
        $av->market_name        = $this->market_name;
        $av->assetid            = $this->assetid;
        $av->classid            = $this->classid;
        $av->instanceid         = $this->instanceid;
        $av->bot_id             = $this->bot_id;
        return $av->save();
    }

}
