<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TowerScenario
 * @package App\Models
 */
class TowerScenario extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user()
	{
		return $this->hasOne('App\Models\User');
	}

	/**
	 * @param $user
	 * @param $level
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public static  function getScenarios($user, $level)
	{
		return TowerScenario::query()
            ->where('user_id', $user->id)
            ->where(function($query) use ($level) {
                $query
                    ->where('level', $level)
                    ->orWhere('level', 'anyone');
            })
            ->where('status', 'enabled')
            ->get();
	}
}
