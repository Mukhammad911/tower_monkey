<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * Class Product
 * @package App\Models
 */
class Product extends Model
{

    public static $classList = ['mil-spec', 'restricted', 'classified', 'covert', 'knifes', 'common', 'uncommon', 'extraordinary', 'chip', 'key', 'case'];

    public function cases()
    {
        return $this->belongsToMany('App\Models\SteamCase', 'case_product', 'product_id', 'case_id');
    }
    
    public function availableProducts() {
        return $this->hasMany('App\Models\AvailableProduct');
    }

    public function csgoCaseAvailableProducts() {
        return $this->hasMany('App\Models\CsgoCaseAvailableProduct');
    }
}
