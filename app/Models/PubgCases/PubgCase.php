<?php

namespace App\Models\PubgCases;

use App\Models\ProvablyFairPubgCase;
use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use Session;
use Helper;
use App\Models\Currency;

class PubgCase extends Model
{
    protected $table = 'pubg_cases';

    public static $marks = ['sale', 'popularly', 'novelty', 'best-price'];

    public function category(){
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\PubgProduct', 'pubg_case_product', 'case_id' ,'product_id')
            ->withPivot('sort');
    }

    public function productsWithChance()
    {
        $case = $this;

        if(!$case){
            return [];
        }
        $products = $case->products()->orderBy('price', 'asc')->get();
        if(!$products){
            return [];
        }

        $rel = array();
        $sum_rel = 0;
        $percent = 0;
        $prev_sum = 0;
        $sum_range = 0;

        $fair_id = array();

        $case_price = $case->priceDefault->price;
        $products_fair = $case->products()->orderBy('price', 'asc')->get();

        $fair_table = ProvablyFairPubgCase::where('case_id', $case->id)->get();

        for ($j = 0; $j < sizeof($products_fair); $j++) {
            $rel[$j] = $case_price / Helper::exchange($products_fair[$j]->price,1);
            $sum_rel += $rel[$j];
        }

        for ($j = 0; $j < sizeof($fair_table); $j++) {
            $fair_id[$j] = $fair_table[$j]->product_id;
        }

        $j = 0;

        foreach ( $products as $key => $product ) {
            if (in_array($product->id, $fair_id))
            {
                $prev_sum = round($sum_range);

                $percent = ($rel[$j] * 100) / $sum_rel;

                $sum_range += ($percent * 100000) / 100;

                $fair_value = ProvablyFairPubgCase::where('case_id', $case->id)->where('product_id', $product->id)->first();

                $products[$key]->percent = $fair_value->chance;
                $products[$key]->win_range_min = $fair_value->start;
                $products[$key]->win_range_max = $fair_value->end;
            }
            else
            {
                $prev_sum = round($sum_range);

                $percent = ($rel[$j] * 100) / $sum_rel;

                $sum_range += ($percent * 100000) / 100;

                $products[$key]->percent = number_format($percent, 2);
                $products[$key]->win_range_min = $prev_sum + 1;
                $products[$key]->win_range_max = round($sum_range);
            }

            $j++;
        }

        return $products;
    }

    public function isAvailablePubgCase(){
        $targetProfit = PubgCaseSettings::where('key', 'profit')->first()->value;

        if(!$this->active){
            return false;
        }

        //Цена в копейках
        $price = $this->PubgPriceDefault->price * (100 - $targetProfit);

        return PubgCaseAvailableProduct::select(DB::raw('COUNT(id) as count'))
            ->where('price', '<=', $price)
            ->whereRaw('bot_id IN(SELECT id FROM pubg_case_bots WHERE enabled=1)')
            ->whereRaw("product_id IN(SELECT product_id FROM pubg_case_product cp WHERE cp.case_id={$this->id})")
            ->whereRaw("product_id NOT IN(SELECT id FROM pubg_products WHERE locked=1)")
            ->pluck('count');
    }

    public function getHighestPriceAttribute()
    {
        $product = $this->products()
            ->select('price')
            ->orderBy('price', 'desc')
            ->first();
        if(!$product){
            return false;
        }

        $highest_prize = $product->price / 100;

        return $highest_prize;
    }

    public function getLowestPriceAttribute()
    {
        $product = $this->products()
            ->select('price')
            ->orderBy('price', 'asc')
            ->first();
        if(!$product){
            return false;
        }

        $highest_prize = $product->price / 100;

        return $highest_prize;
    }

    public function currency()
    {
        return $this->belongsToMany('App\Models\Currency', 'pubg_case_price', 'case_id', 'currency_id');
    }

    public function getPriceObjAttribute(){
        $id = $this->id;
        $currency_id = empty(Session::get('currency_id')) ? 1 : Session::get('currency_id');

        $price = DB::table('currency as curr')
            ->join('pubg_case_price as cp','cp.currency_id','=','curr.id')
            ->where('curr.id',$currency_id)
            ->where('cp.case_id', $id)
            ->take(1)
            ->first();

        return $price;
    }

    public function getPriceDefaultAttribute(){
        $id = $this->id;

        $price = DB::table('currency as curr')
            ->join('pubg_case_price as cp','cp.currency_id','=','curr.id')
            ->where('curr.code', 'USD')
            ->where('cp.case_id', $id)
            ->take(1)
            ->first();

        return $price;
    }

    public function getPubgPriceDefaultAttribute(){
        $id = $this->id;

        $price = DB::table('currency as curr')
            ->join('pubg_case_price as cp','cp.currency_id','=','curr.id')
            ->where('curr.code', 'RUB')
            ->where('cp.case_id', $id)
            ->take(1)
            ->first();

        return $price;
    }

    public static function attachPrice($request, $case){
        foreach ($request->input('price') as $key=>$price){
            $curr_id = Currency::where('code', $key)->pluck('id');
            DB::table('pubg_case_price')->insert(['case_id'=>$case->id, 'currency_id'=>$curr_id, 'price'=>$price]);
        }
    }
}
