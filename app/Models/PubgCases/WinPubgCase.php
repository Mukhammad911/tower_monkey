<?php

namespace App\Models\PubgCases;

use App\Models\Currency;
use App\Models\Operation;
use App\Models\PubgProduct;
use Helper;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class WinPubgCase
{
    //Прибыль %
    private $targetProfit;

    //Резервный % прибыли для выдачи вещей дороже стоимости кейса
    private $reservProfit = 1;

    private $case;

    //Цена кейса в копейках
    private $case_price;

    private $cheapWeapon;

    private $expensiveWeapon;

    private $middleWeapon;

    private $stats;

    //Ожидаемая Средняя прибыль по всем кейсам
    private $profitAvarage;

    private $dropLevel;

    private $total_income;
    private $total_expense;

    private $total_balance;
    private $last_balance_own;

    /**
     * @param PubgCase $case
     */
    public function __construct(PubgCase $case, $last_balance)
    {
        $this->last_balance_own = $last_balance;
        $this->targetProfit = PubgCaseSettings::where('key', 'profit')->first()->value;
        Log::info('Target Profit: '.$this->targetProfit);
        $this->case = $case;
        Log::info('Case : '.$this->case);
        $this->case_price = $this->case->PubgPriceDefault->price * 100;
        Log::info('Case Price:'. $this->case_price );
        $this->income();
        $this->expense();
        $this->total_balance = $this->total_income - $this->total_expense;
        Log::info('Total Balance:'.  $this->total_balance );
        $this->init();
    }

    private function init()
    {
        $this->cheapWeapon  = $this->getByPrice(0);
        Log::info('cheap weapon:');
        Log::info($this->cheapWeapon);
        $this->middleWeapon = $this->getByPrice(1);
        Log::info('middle weapon:');
        Log::info($this->middleWeapon);
        $this->expensiveWeapon  = $this->getByPrice(2);
        Log::info('expensive weapon:');
        Log::info($this->expensiveWeapon);
    }

    private function income(){
        $total_income = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {

            $income_tower = Operation::where('description', 'tower bet')->sum('amount');

            $income_roulette_opening_csgo = Operation::where('description', 'csgo case opening')->sum('amount');
            $income_roulette_opening_youtuber_csgo = Operation::where('description', 'csgo case opening youtube')->sum('amount');
            $moneyback_roulette_opening_csgo = Operation::where('description', 'money back csgo case')->sum('amount');
            $moneyback_roulette_opening_youtuber_csgo = Operation::where('description', 'money back csgo case youtube')->sum('amount');

            $income_roulette_opening_pubg = Operation::where('description', 'pubg case opening')->sum('amount');
            $income_roulette_opening_youtuber_pubg = Operation::where('description', 'pubg case opening youtube')->sum('amount');
            $moneyback_roulette_opening_pubg = Operation::where('description', 'money back pubg case')->sum('amount');
            $moneyback_roulette_opening_youtuber_pubg = Operation::where('description', 'money back pubg case youtube')->sum('amount');

            $total_income = $income_tower + $income_roulette_opening_csgo + $income_roulette_opening_youtuber_csgo
                + $moneyback_roulette_opening_csgo + $moneyback_roulette_opening_youtuber_csgo
                + $income_roulette_opening_pubg + $income_roulette_opening_youtuber_pubg
                + $moneyback_roulette_opening_pubg + $moneyback_roulette_opening_youtuber_pubg;
        }

        Log::info('Total income : '.$total_income);

        $this->total_income = -$total_income;
    }

    private function expense(){
        $total_expense = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {

            $expense_tower = Operation::where('description', 'tower win')->sum('amount');

            $expense_roulette_csgo = Operation::where('description', 'csgo case win')->sum('amount');
            $expense_roulette_youtuber_csgo = Operation::where('description', 'csgo case win youtube')->sum('amount');

            $expense_roulette_pubg = Operation::where('description', 'pubg case win')->sum('amount');
            $expense_roulette_youtuber_pubg = Operation::where('description', 'pubg case win youtube')->sum('amount');

            $total_expense = $expense_tower + $expense_roulette_csgo + $expense_roulette_youtuber_csgo + $expense_roulette_pubg + $expense_roulette_youtuber_pubg;
        }

        Log::info('Total expense : '.$total_expense);
        $this->total_expense = $total_expense;
    }


    /**
     * @param $ret
     * @param $level
     */
    private function addPriceCondition(&$ret, $level){
        $price = $this->case_price * (100 - $this->targetProfit) / 100;
        Log::info('Price for get product');
        Log::info($price);

        switch($level){
            case 2:
                $ret = $ret->where('price','>=',$this->case_price * 2);
                break;
            case 1:
                $ret = $ret->where('price','>',$price);
                $ret = $ret->where('price','<=',$this->case_price * 2);
                break;
            default:
                $ret = $ret->where('price','<=',$price);
                break;
        }
    }

    /**
     * @param $level
     *
     * @return static
     */
    private function getByPrice($level){
        $ret = DB::table('pubg_case_available_products')
            ->select('product_id')
            ->whereRaw('bot_id IN(SELECT id FROM pubg_case_bots WHERE enabled=1)')
            ->whereRaw("product_id NOT IN(SELECT id FROM pubg_products WHERE locked=1)")
            ->distinct();


        $this->addPriceCondition($ret, $level);

        $ret = $ret->whereRaw("product_id IN(SELECT product_id FROM pubg_case_product cp WHERE cp.case_id={$this->case->id})")
            ->get();
        Log::info('Products in case');
        Log::info($ret);
        $ret = collect($ret)->lists('product_id');
        Log::info('Collect');
        Log::info($ret);
        return $ret;
    }

    /**
     * @param $ret
     * @param $level
     */
    private function addResultPriceCondition(&$ret, $level)
    {
        $price = $this->case_price * (100 - $this->targetProfit) / 100;

        if($level === 1){
            $ret = $ret->where('price','<=',$this->case_price * 2);
        }
        elseif($level === 0){
            $ret = $ret->where('price','<=',$price);
        }
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    private function getRand($ids)
    {
        $drop = mt_rand(0, count($ids)-1);

        if($this->dropLevel == 2){
            $last_high_quota_wins = PubgCaseDrops::where('user_id', Auth::user()->id)
                ->where('case_id', $this->case->id)
                ->take(25)
                ->orderBy('id', 'desc')
                ->lists('product_id')
                ->toArray();

            if(in_array($ids[$drop], $last_high_quota_wins) && $this->middleWeapon->count()){
                $this->dropLevel = 1;
                $ids = $this->middleWeapon;
                $drop = mt_rand(0,count($ids)-1);
            }elseif(in_array($ids[$drop], $last_high_quota_wins)){
                $this->dropLevel = 0;
                $ids = $this->cheapWeapon;
                $drop = mt_rand(0,count($ids)-1);
            }
        }

        $ret =  PubgCaseAvailableProduct::where('product_id',$ids[$drop])
            ->whereRaw('bot_id IN(SELECT id FROM pubg_case_bots WHERE enabled=1)');
        $this->addResultPriceCondition($ret, $this->dropLevel);

        $result = $ret->get()->random();

        if (!empty($result))
        {
            $product = PubgProduct::where('id', $result->product_id)->first();

            $result->class = $product->class;
        }

        return $result;
    }

    public function get()
    {
        Log::info('Vhod v get');
        if(!$this->cheapWeapon->count()){
            Log::info('Popal kogda net cheap');
            return false;
        }

        if(Auth::user()->drop_banned){
            Log::info('Popal  kogda drop banned');
            $ids = $this->cheapWeapon;
            $this->dropLevel = 0;
        }
        elseif ($this->last_balance_own > 0)
        {
            Log::info('Popal kogda promo kod pubg case');
            $ids = $this->cheapWeapon;
            $this->dropLevel = 0;
        }
        else
        {
            $company_profit = $this->currentCompanyProfit($this->total_income, $this->total_expense);

            Log::info('$company_profit');
            Log::info($company_profit);

            Log::info('Popal  v else');
            switch(true){
                case $company_profit <= $this->targetProfit:
                    Log::info('Popal v chip');
                    $ids = $this->cheapWeapon;
                    Log::info('ids cheap');
                    Log::info($ids);
                    $this->dropLevel = 0;
                    break;

                case $company_profit > $this->targetProfit:
                    switch(true){
                        case $this->expensiveWeapon->count():
                            $ids = $this->expensiveWeapon;
                            $this->dropLevel = 2;
                            break;
                        case $this->middleWeapon->count():
                            $ids = $this->getMiddleCollection();
                            $this->dropLevel = 1;
                            break;
                        default:
                            $ids = $this->cheapWeapon;
                            $this->dropLevel = 0;
                            break;
                    }
                    break;
                default:
                    $ids = $this->cheapWeapon;
                    $this->dropLevel = 0;
                    break;
            }
        }

        $weapon = $this->getRand($ids);
        Log::info('weapon get rand'.$weapon);

        $price_weapon = Helper::exchange($weapon->price, 1);

        Log::info('$price_weapon');
        Log::info($price_weapon);

        $company_pre_profit = $this->currentCompanyProfit($this->total_income, ($this->total_expense + $price_weapon));

        Log::info('$company_pre_profit');
        Log::info($company_pre_profit);

        if ($company_pre_profit <= $this->targetProfit)
        {
            Log::info('When expensive or middle more than target profit.');

            $ids = $this->cheapWeapon;
            $this->dropLevel = 0;

            $weapon = $this->getRand($ids);
            return $weapon;
        }

        $profit = $this->case_price - $weapon->price;

        Log::info('Not enter to condition of target profit. Normal situation.');

        return $weapon;
    }

    protected function getMiddleCollection(){
        $countCheap = $this->cheapWeapon->count();

        if($this->middleWeapon->count() > $countCheap){
            $middleResult = $this->middleWeapon->random($countCheap);
        } else{
            $middleResult = $this->middleWeapon;
        }

        $result = $this->cheapWeapon->merge($middleResult);

        return $result;
    }

    public function currentCompanyProfit($income, $expense)
    {
        if($expense == 0){
            return 0;
        }

        return ( ($income / $expense - 1) * 100);
    }
}
