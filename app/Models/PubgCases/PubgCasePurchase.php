<?php

namespace App\Models\PubgCases;

use Illuminate\Database\Eloquent\Model;

class PubgCasePurchase extends Model
{
    protected $table = 'pubg_case_purchases';
    protected $guarded = ['id'];
    protected $fillable = ['bot_id', 'target', 'max_price', 'low_price_limit', 'mid_price_limit', 'low_quota', 'mid_quota', 'high_quota'];

    public function bot()
    {
        return $this->belongsTo('App\Models\PubgCases\PubgCaseBot');
    }

    public function cases()
    {
        return $this->belongsToMany('App\Models\PubgCases\PubgCase', 'pubg_cases_by_purchase', 'purchase_id', 'case_id');
    }
}
