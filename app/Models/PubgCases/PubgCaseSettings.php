<?php

namespace App\Models\PubgCases;

use Illuminate\Database\Eloquent\Model;

class PubgCaseSettings extends Model
{
    protected $table = 'pubg_case_roulette_settings';
}
