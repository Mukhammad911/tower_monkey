<?php

namespace App\Models\PubgCases;

use Illuminate\Database\Eloquent\Model;

class PubgCaseAvailableProduct extends Model
{
    protected $table = 'pubg_case_available_products';
}
