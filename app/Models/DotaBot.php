<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DotaBot extends Model {

    protected $guarded = ['id', 'secret', 'revocation_code'];
    protected $fillable = ['steam_login', 'steam_password','steamid' ,'phone', 'email', 'email_password', 'api_key'];

    public function getCode() {
        $nodeBotPath = env('DOTABOT_PATH', '');
        $process = new Process('node '.$nodeBotPath.'/code.js '.$this->secret);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    public function purchases() {
        return $this->hasMany('App\Models\DotaPurchase');
    }

    public function drops() {
        return $this->hasMany('App\Models\DotaDropsMarket');
    }

    public function availableProducts() {
        return $this->hasMany('App\Models\DotaAvailableProduct');
    }
}
