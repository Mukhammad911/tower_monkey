<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CsgoCasePurchase extends Model
{
    protected $table = 'csgo_case_purchases';
    protected $guarded = ['id'];
    protected $fillable = ['bot_id', 'target', 'max_price', 'low_price_limit', 'mid_price_limit', 'low_quota', 'mid_quota', 'high_quota'];

    public function bot()
    {
        return $this->belongsTo('App\Models\CsgoCaseBot');
    }

    public function cases()
    {
        return $this->belongsToMany('App\Models\SteamCase', 'case_purchase', 'purchase_id', 'case_id');
    }

}
