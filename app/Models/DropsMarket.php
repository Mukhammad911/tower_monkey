<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Helper;
use App\Models\AvailableProduct;
use DB;
use App\Models\User;

class DropsMarket extends Model
{
    protected $table = 'drops_market';

    public function user()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

    public function bot()
    {
        return $this->hasOne('App\Models\Bot','id', 'bot_id');
    }

    public function sell($description = 'sell skin csgo drop')
    {
        DB::beginTransaction();

        if($this->toxic == 0) {
            $this->replaceDrop();
        }

        $this->status 		= 'sold';
        $this->assetid 		= 0;
        $this->classid 		= 0;
        $this->instanceid 	= 0;
        $this->save();

        $sell_price = Helper::exchange($this->price, 1);

        User::find($this->user_id)
            ->balance()
            ->where('currency_id', 1)
            ->where('type', 0)
            ->sharedLock()
            ->update(['summ'=>DB::raw("summ + ".$sell_price)]);

        /*$user_balance = Balance::where('user_id', $this->user_id)
            ->where('currency_id', $this->currency_id)
            ->where('type', 0)->first()->summ;*/
        //Helper::operation($sell_price, $this->currency_id, $description, $this->user_id, $user_balance);

        DB::commit();

        return $sell_price;
    }

    protected  function replaceDrop()
    {
        $av = new AvailableProduct();
        $av->name               = $this->name;
        $av->short_description  = $this->short_description;
        $av->quality            = $this->quality;
        $av->stattrak           = $this->stattrak;
        $av->class              = $this->class;
        $av->product_id         = $this->product_id;
        $av->price              = $this->price;
        $av->image_hash         = $this->image_hash;
        $av->market_name        = $this->market_name;
        $av->assetid            = $this->assetid;
        $av->classid            = $this->classid;
        $av->instanceid         = $this->instanceid;
        $av->bot_id             = $this->bot_id;
        $av->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    /*  public function card()
      {
          return $this->hasOne('App\Models\Card');
      }*/

    /**
     * Scope a query to only include drops without active cards.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    /* public function scopeWhereNotActiveCardExists($query)
     {
         return $query->whereDoesntHave('card', function ($query)
         {
             $query->where('status', '!=', 'winned');
         });
     }*/

    /* public function complete()
     {
         $liveDropCount = LiveDrop::count();
         if($liveDropCount > 25){
             LiveDrop::orderBy('created_at','asc')->take($liveDropCount-25)->delete();
         }

         $live = new LiveDrop();
         $live->user_id = $this->user_id;
         $live->product_id = $this->product_id;
         $live->stattrak = $this->stattrak;
         $live->save();

         $data = [
             'id'         => $live->id,
             'w_class'    => $live->product->class,
             'w_image'    => Helper::caseImageUrl($live->product),
             'w_name'     => $live->product->name,
             'w_descr'    => $live->product->short_description,
             'stattrak'   => (bool)$live->stattrak,
             'steam_id'   => $live->user->steamid,
             'username'   => $live->user->username,
             'avatar'     => $live->user->avatar
         ];

     }*/
}
