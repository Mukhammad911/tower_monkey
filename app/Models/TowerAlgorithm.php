<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Statistic;
use App\CustomClasses\TowerGame;
use DB;

/**
 * Class TowerAlgorithm
 * @package App\Models
 */
class TowerAlgorithm
{
    private $total_income;
    private $total_expense;
    private $target_company_percent;
    private $profit_deviation_percent;
    private $real_company_percent;
    private $current_company_percent;
    private $min_percent = 1;
    private $max_percent = 80;

    private $HARD_LEVEL_PRIZE;
    private $MEDIUM_LEVEL_PRIZE;
    private $EASY_LEVEL_PRIZE;

    private $hard_level_chance;
    private $medium_level_chance;
    private $easy_level_chance;

    private $level = 'easy';
    private $bet;
    private $hard_multiplier;
    private $medium_multiplier;
    private $easy_multiplier;

    /**
     *
     */
    public function __construct($bet)
    {
        $last_balance = DB::table('tower_promo')->selectRaw('sum(last_balance) as last_balance')->where('user_id','=',Auth::user()->id)->get();
        if((float)$last_balance[0]->last_balance==0){
            $this->total_income = TowerGame::income();
            $this->total_expense = TowerGame::expense();
            $this->target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();

            $this->HARD_LEVEL_PRIZE = TowerSetting::where('key', 'hard level prize')->first()->value;
            $this->MEDIUM_LEVEL_PRIZE = TowerSetting::where('key', 'medium level prize')->first()->value;
            $this->EASY_LEVEL_PRIZE = TowerSetting::where('key', 'easy level prize')->first()->value;

            $this->hard_multiplier = $this->getMultiplier($this->HARD_LEVEL_PRIZE);
            $this->medium_multiplier = $this->getMultiplier($this->MEDIUM_LEVEL_PRIZE);
            $this->easy_multiplier = $this->getMultiplier($this->EASY_LEVEL_PRIZE);

            $this->hard_level_chance = $this->getChance($this->hard_multiplier);
            $this->medium_level_chance = $this->getChance($this->medium_multiplier);
            $this->easy_level_chance = $this->getChance($this->easy_multiplier);
            Log::info('last_balance = 0. Tower: user_id: '.Auth::user()->id);
        }
        else if(((float)$bet < 1) && ((float)$last_balance[0]->last_balance >0)){
            $this->total_income = 10;
            $this->total_expense = 100000;
            $this->target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();

            $this->HARD_LEVEL_PRIZE = TowerSetting::where('key', 'hard level prize')->first()->value;
            $this->MEDIUM_LEVEL_PRIZE = TowerSetting::where('key', 'medium level prize')->first()->value;
            $this->EASY_LEVEL_PRIZE = TowerSetting::where('key', 'easy level prize')->first()->value;

            $this->hard_multiplier = $this->getMultiplier($this->HARD_LEVEL_PRIZE);
            $this->medium_multiplier = $this->getMultiplier($this->MEDIUM_LEVEL_PRIZE);
            $this->easy_multiplier = $this->getMultiplier($this->EASY_LEVEL_PRIZE);

            $this->hard_level_chance = $this->getChance($this->hard_multiplier);
            $this->medium_level_chance = $this->getChance($this->medium_multiplier);
            $this->easy_level_chance = $this->getChance($this->easy_multiplier);
            Log::info('Promo bypass when bet < 1. Tower: user_id: '.Auth::user()->id);
            DB::table('tower_promo')->where('user_id','=',Auth::user()->id)->update([
                'last_balance'=>(float)$last_balance[0]->last_balance-$bet,
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
        else if(((float)$last_balance[0]->last_balance - $bet)>=0){
            $this->total_income = 10;
            $this->total_expense = 100000;
            $this->target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();

            $this->HARD_LEVEL_PRIZE = TowerSetting::where('key', 'hard level prize')->first()->value;
            $this->MEDIUM_LEVEL_PRIZE = TowerSetting::where('key', 'medium level prize')->first()->value;
            $this->EASY_LEVEL_PRIZE = TowerSetting::where('key', 'easy level prize')->first()->value;

            $this->hard_multiplier = $this->getMultiplier($this->HARD_LEVEL_PRIZE);
            $this->medium_multiplier = $this->getMultiplier($this->MEDIUM_LEVEL_PRIZE);
            $this->easy_multiplier = $this->getMultiplier($this->EASY_LEVEL_PRIZE);

            $this->hard_level_chance = $this->getChance($this->hard_multiplier);
            $this->medium_level_chance = $this->getChance($this->medium_multiplier);
            $this->easy_level_chance = $this->getChance($this->easy_multiplier);
            Log::info('Promo bypass. Tower: user_id: '.Auth::user()->id);
            DB::table('tower_promo')->where('user_id','=',Auth::user()->id)->update([
                'last_balance'=>(float)$last_balance[0]->last_balance-$bet,
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
        else{
            $this->total_income = TowerGame::income();
            $this->total_expense = TowerGame::expense();
            $this->target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
            $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $this->total_expense);
            $this->profit_deviation_percent = abs(100 - $this->real_company_percent / ($this->target_company_percent / 100));
            $this->current_company_percent = $this->stabilizedCompanyPercent();

            $this->HARD_LEVEL_PRIZE = TowerSetting::where('key', 'hard level prize')->first()->value;
            $this->MEDIUM_LEVEL_PRIZE = TowerSetting::where('key', 'medium level prize')->first()->value;
            $this->EASY_LEVEL_PRIZE = TowerSetting::where('key', 'easy level prize')->first()->value;

            $this->hard_multiplier = $this->getMultiplier($this->HARD_LEVEL_PRIZE);
            $this->medium_multiplier = $this->getMultiplier($this->MEDIUM_LEVEL_PRIZE);
            $this->easy_multiplier = $this->getMultiplier($this->EASY_LEVEL_PRIZE);

            $this->hard_level_chance = $this->getChance($this->hard_multiplier);
            $this->medium_level_chance = $this->getChance($this->medium_multiplier);
            $this->easy_level_chance = $this->getChance($this->easy_multiplier);
            Log::info('Algorithm. Tower: user_id: '.Auth::user()->id);
        }
    }

    /**
     * @param $level
     * @param $bet
     * @param $demo
     *
     * @return string
     */

    private function calculateGame(){
        $in = $this->total_income;
        $ex = $this->total_expense;
        $victory = false;
        $current_game = Tower::getCurrentGame();
        $multiplier = TowerSetting::getMultiplier($current_game->level);
        $profit = $current_game->bet * ($multiplier ** ($current_game->step));
        $pre_profit = $this->total_expense + $profit;
        $this->real_company_percent = Statistic::currentCompanyProfit($this->total_income, $pre_profit);

        if($this->real_company_percent > ($this->target_company_percent)){
             $rand_number = mt_rand(1, 100000);

             if($rand_number >= 85000){
                 return $victory;
             }

            $victory = true;
            return $victory;
        }
        else{
            return $victory;
        }
    }
    private function calculateGameDemo(){
        $rand_number = mt_rand(1, 7);
        $current = 1;
        if($rand_number==$current){
            return false;
        }
        else{
            return true;
        }
    }
    public function play($level, $bet, $demo)
    {
        $this->level = $level;
        $this->bet = $bet;
        if($demo == 'yes'){
            if($BaseGame = $this->calculateGameDemo()){
                return 'victory';
            }
            else{
                return 'loss';
            }
        }
        else{
            $BaseGame = $this->calculateGame();
            if(!$BaseGame==TRUE)
            {
                return 'loss';
            }
            return 'victory';
        }
    }

    /**
     * @return bool
     */
    private function baseProfitDefence()
    {
        $current_multiplier = $this->level.'_multiplier';
        $profit = $this->bet * $current_multiplier;
        $balance = $this->total_income - $this->total_expense;

        if($profit * 10 > $balance){
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    /*private function calculateGame()
    {
        $rand_number = mt_rand(1, 10000);
        $victory = false;

        switch($this->level){
            case 'hard':
                $win_range = $this->hard_level_chance;
                break;

            case 'medium':
                $win_range = $this->medium_level_chance;
                break;

            case 'easy':
            default:
                $win_range = $this->easy_level_chance;
                break;
        }

        if($rand_number <= $win_range){
            $victory = true;
        }

        return $victory;
    }*/

    /**
     * @return float
     */
    public function stabilizedCompanyPercent()
    {

        if($this->real_company_percent < 0
            || ($this->real_company_percent < $this->target_company_percent && $this->profit_deviation_percent > 100) ){
            return $this->max_percent;
        }

        if($this->real_company_percent > $this->target_company_percent && $this->profit_deviation_percent > 100){
            return $this->min_percent;
        }

        /* if($this->real_company_percent > $this->target_company_percent && $this->profit_deviation_percent > 100){
            return $this->min_percent;
        }

        if($this->real_company_percent < 0
           || ($this->real_company_percent < $this->target_company_percent && $this->profit_deviation_percent > 50) ){
            return $this->max_percent;
        }*/

        if($this->real_company_percent > $this->target_company_percent){
            $range_step = ($this->target_company_percent - $this->min_percent) / 100;
            $correction = $range_step * $this->profit_deviation_percent;
            $corrected_company_percent = round($this->target_company_percent - $correction, 2);
        }else{
            $range_step = ($this->max_percent - $this->target_company_percent) / 100 * 2;
            $correction = $range_step * $this->profit_deviation_percent;
            $corrected_company_percent = round($this->target_company_percent + $correction, 2);
        }

        return $corrected_company_percent;
    }

    /**
     * @return bool
     */
    private function takeCompanyPercent()
    {
        $rand_number = mt_rand(1, 10000);
        $victory = false;

        $losing_range = $this->current_company_percent * 100;

        if($rand_number > $losing_range){
            $victory = true;
        }

        return $victory;
    }

    /**
     * @param $prize_percent
     *
     * @return float
     */
    private function getMultiplier($prize_percent)
    {
        $multiplier = $prize_percent / 100 + 1;

        return $multiplier;
    }

    /**
     * @param $multiplier
     *
     * @return float
     */
    private function getChance($multiplier)
    {
        $chance = round(10000 / $multiplier);

        return $chance;
    }
}
