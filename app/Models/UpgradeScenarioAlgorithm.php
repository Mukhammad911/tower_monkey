<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

/**
 * Class UpgradeScenarioAlgorithm
 * @package App\Models
 */
class UpgradeScenarioAlgorithm
{
	/**
	 * @var
	 */
	private $user;

	/**
	 * @var
	 */
	private $bet;

	/**
	 * @var
	 */
	private $profit;

	/**
	 * @var
	 */
	private $scenarios;

	/**
	 * @var
	 */
	private $current_attempt;

    /**
     * @var
     */
    private $last_balance;

	/**
	 * @param $user
	 * @param $bet
	 * @param $profit
	 * @param $scenarios
	 */
	public function __construct($user, $bet, $profit, $scenarios,$last_balance)
	{
        $this->last_balance = $last_balance;
		$this->user = $user;
		$this->bet = $bet;
		$this->profit = $profit;
		$this->scenarios = $scenarios;
		$this->current_attempt = Redis::get('upgrade-scenario-'.$user->id);

		Redis::set('upgrade-scenario-'.$user->id, $this->current_attempt + 1);
	}

	/**
	 * @return UpgradeAlgorithm|bool
	 */
	public function result()
	{
		$profit_percent = ( ($this->profit / $this->bet) - 1 ) * 100;

		if($profit_percent <= 400){
			foreach($this->scenarios as $scenario){
				if($this->current_attempt == ($scenario->attempt - 1)){
					$Scenario = UpgradeScenario::find($scenario->id);
					$Scenario->status = 'completed';
					$Scenario->save();

					return TRUE;
				}
			}
		}else{
			Redis::set('upgrade-scenario-'.$this->user->id, $this->current_attempt);
		}

		$UpgradeAlgorithm =  new UpgradeAlgorithm($this->bet, $this->profit,$this->last_balance);

		return $UpgradeAlgorithm->result();
	}
}
