<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Upgrade
 * @package App\Models
 */
class Upgrade extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function liveUpgrade()
	{
		return $this->hasOne('App\Models\liveUpgrade');
	}
}
