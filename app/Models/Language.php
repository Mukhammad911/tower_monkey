<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Language extends Model {
    public $timestamps = false;

    public static $default = ['en','ru','uk'];

    public function currency(){
        return $this->hasOne('App\Models\Currency', 'id', 'currency_id');
    }

    public function scopeGetActiveLanguages($query){
        return $query->where('active',1)->get();
    }

    public function scopeGetByLocale($query){
        $locale = App::getLocale();
        return $query->where('locale',$locale)->first();
    }

    //public function scopeGetCurrency($query, $iso){
    //    $ret = $this->where('iso2',$iso)->first();

    //    if(is_null($ret)){
    //        $locale = App::getLocale();
    //        $ret = $this->where('locale',$locale)->first();
    //    }

    //    return $ret->currency_id;
    //}
}