<?php

namespace App\Models;

use Helper;
use Auth;
use DB;
use Illuminate\Support\Facades\Log;

/**
 * Class WinYoutuber
 * @package App\Models
 */
class WinYoutuber
{
	private $case;
	private $case_price;

	private $low_quota_chance;
	private $middle_quota_chance;
	private $high_quota_chance;

	private $low_weapon;
	private $middle_weapon;
	private $high_weapon;

	/**
	 * @param SteamCase $case
	 */
    public function __construct(SteamCase $case)
    {
	    $this->case = $case;
	    $this->case_price = $this->case->priceDefault->price * 100;

	    $this->low_quota_chance = Helper::setting('youtuber_low_quota');
	    $this->middle_quota_chance = Helper::setting('youtuber_middle_quota');
	    $this->high_quota_chance = Helper::setting('youtuber_high_quota');

	    $this->low_weapon = Helper::getWeapon(0, $this->case->id, $this->case_price);
	    $this->middle_weapon = Helper::getWeapon(1, $this->case->id, $this->case_price);
	    $this->high_weapon = Helper::getWeapon(2, $this->case->id, $this->case_price);
    }

	/**
	 * @return mixed
	 */
	public function get()
	{
		if(!$this->low_weapon){
			Log::info('There are no low quota weapons. Case: '.$this->case->id.'; User: '.Auth::user()->id);
			return false;
		}

		$quota = $this->getQuota();
		switch(true){
			case ($quota == 2):
				if($this->high_weapon){
					$weapon = $this->high_weapon;
				}elseif($this->middle_weapon){
					$weapon = $this->middle_weapon;
				}else{
					$weapon = $this->low_weapon;
				}
				break;

			case ($quota == 1):
				if($this->middle_weapon){
					$weapon = $this->middle_weapon;
				}else{
					$weapon = $this->low_weapon;
				}
				break;

			default:
				$weapon = $this->low_weapon;
				break;
		}

		return $weapon;
	}

	/**
	 * Random mechanism
	 * @return int
	 */
	private function getQuota()
	{
		$random = mt_rand(1, 100);

		switch($random){
			// High quota
			case ($random > 0 && $random <= $this->high_quota_chance) :
				$quota = 2;
				break;

			// Middle quota
			case ($random > $this->high_quota_chance && $random <= ($this->high_quota_chance + $this->middle_quota_chance)) :
				$quota = 1;
				break;

			// Low quota
			default:
				$quota = 0;
				break;
		}

		return $quota;
	}
}
