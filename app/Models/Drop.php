<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Helper;
use App\Models\AvailableProduct;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class Drop extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }

	public function product()
	{
		return $this->hasOne('App\Models\Product', 'id', 'product_id');
	}

	public function steamCase()
	{
		return $this->hasOne('App\Models\SteamCase','id', 'case_id');
	}

    public function bot()
    {
        return $this->hasOne('App\Models\Bot','id', 'bot_id');
    }

	/**
	 * @param string $description
	 *
	 * @return mixed
	 */
    public function sell($description = 'sell skin drop')
    {
	    DB::beginTransaction();

	    if($this->toxic == 0) {
		    $this->replaceDrop();
	    }

        $this->status 		= 'sold';
        $this->assetid 		= 0;
        $this->classid 		= 0;
        $this->instanceid 	= 0;
        $this->save();

	    $sell_price = Helper::exchange($this->price, $this->currency_id);

        User::find($this->user_id)
            ->balance()
            ->where('currency_id', $this->currency_id)
	        ->where('type', 0)
	        ->sharedLock()
            ->update(['summ'=>DB::raw("summ + 0")]);

	    Helper::operation($sell_price, $this->currency_id, $description, $this->user_id);

	    DB::commit();

	    return $sell_price;
    }

	/**
	 *
	 */
    protected  function replaceDrop()
    {
        $av = new AvailableProduct();
        $av->name               = $this->name;
        $av->short_description  = $this->short_description;
        $av->quality            = $this->quality;
        $av->stattrak           = $this->stattrak;
        $av->class              = $this->class;
        $av->product_id         = $this->product_id;
        $av->price              = $this->price;
        $av->image_hash         = $this->image_hash;
        $av->market_name        = $this->market_name;
        $av->assetid            = $this->assetid;
        $av->classid            = $this->classid;
        $av->instanceid         = $this->instanceid;
        $av->bot_id             = $this->bot_id;
        $av->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function card()
    {
        return $this->hasOne('App\Models\Card');
    }

    /**
     * Scope a query to only include drops without active cards.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereNotActiveCardExists($query)
    {
        return $query->whereDoesntHave('card', function ($query)
        {
            $query->where('status', '!=', 'winned');
        });
    }

	public function complete()
	{
		$LastWinsCount = LastWin::count();
		if($LastWinsCount > 15){
			LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
		}

		$LastWin = new LastWin();
		$LastWin->game = 'roulette';
		$LastWin->game_id = $this->id;
		$LastWin->save();

		$drop = $this;
		$drop->id = 'roulette'.$LastWin->id;
		$drop->game_type = 'roulette';
		$drop->timeout = 6000;
		$drop->case_price = $this->steamCase->priceDefault->price;
		$drop->case_image = asset('images/cases/thumbnails').'/'.$this->steamCase->image;
		$drop->product_image = Helper::caseImageUrl($this->product);
		$drop->user_id = $this->user->id;
		$drop->username = $this->user->username;
		$drop->avatar = $this->user->avatar;

		event(new \App\Events\Drop($drop));
	}
}
