<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessagePl extends Eloquent
{
    protected $table = 'messages_pl';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
