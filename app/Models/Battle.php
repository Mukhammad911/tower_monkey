<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Battle
 * @package App\Models
 */
class Battle extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function host()
	{
		return $this->belongsTo('App\Models\User', 'host_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function winner()
	{
		return $this->belongsTo('App\Models\User', 'winner_id', 'id');
	}
}
