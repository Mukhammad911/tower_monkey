<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model {
    protected $table = 'balance';

    protected $fillable = ['user_id', 'currency_id', 'type', 'summ'];

    public function currency(){
        return $this->hasOne('App\Models\Currency','id','currency_id');
    }

	public function user(){
		return $this->belongsTo('App\Models\User');
	}
}
