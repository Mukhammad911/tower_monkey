<?php namespace App\Models;

use App\Http\Requests\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Symfony\Component\HttpFoundation\Session\Session;
use Validator;
use DB;
use Session as SessionObj;

/**
 * Class User
 * @package App\Models
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{

	use Authenticatable, CanResetPassword, HasRoleAndPermission;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'username', 'steamid', 'youtuber', 'network', 'identity', 'avatar', 'active', 'last_activity','profit', 'count_drops'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function balance()
	{
		return $this->hasMany('App\Models\Balance');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function operations()
	{
		return $this->hasMany('App\Models\Operation');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orders()
	{
		return $this->hasMany('App\Models\Order');
	}

	/**
	 * @return mixed
	 */
	public function drops()
	{
		return $this->hasMany('App\Models\Drop')->orderBy('created_at','desc');
	}

	/**
	 * @return mixed
	 */
	public function games()
	{
		return $this->hasMany('App\Models\Game');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function affiliateLevel()
	{
		return $this->belongsTo('App\Models\AffiliateLevel');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function partnerLevel()
	{
		return $this->hasOne('App\Models\AffiliateLevel', 'level', 'affiliate_level');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function affiliateRefer()
	{
		return $this->hasOne('App\Models\Affiliate');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function affiliateUsers()
	{
		return $this->hasMany('App\Models\Affiliate');
	}

	/**
	 * @param array $input
	 *
	 * @return mixed
	 */
	public static function loginValidator(array $input)
	{
		
		return $validator = Validator::make(
					$input,
					[
						'password' => 'required',
						'email' => 'required|email'
					]
				);
	}


	/**
	 * @return mixed
	 */
	public function isTriedTrialCard()
	{
		return Card::where('user_id', $this->id)->where('trial', true)->where('status', '=', 'winned')->exists();
	}

}
