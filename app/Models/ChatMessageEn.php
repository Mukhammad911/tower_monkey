<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ChatMessageEn extends Eloquent
{
    protected $table = 'messages_en';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
