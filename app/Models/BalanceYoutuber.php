<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceYoutuber extends Model
{
    protected $table = 'balance_youtuber';
    protected $fillable = ['profit_start','profit_end','user_id','game_count'];

}
