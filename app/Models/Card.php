<?php namespace App\Models;

use Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * Class Card
 * @package App\Models
 */
class Card extends Model
{
    protected $with = ['slotsVisible', 'product'];

    public function user() {
        return $this->hasOne('App\Models\User');
    }

    public function drop() {
        return $this->belongsTo('\App\Models\Drop');
    }

    public function steamCase() {
        return $this->belongsTo('\App\Models\SteamCase', 'case_id');
    }

    public function product() {
        return $this->belongsTo('\App\Models\Product');
    }

    public function products() {
        return $this->belongsToMany('\App\Models\Product', 'cards_products');
    }

    public function slots() {
        return $this->hasMany('\App\Models\CardSlot');
    }

    public function slotsVisible() {
        return $this->slots()->where('visible', true);
    }

	/**
	 * @param bool $max
	 *
	 * @return mixed
	 */
    public function getDefaultSlotsProductsInfo($max = true) {
        $productsInfo = \DB::table('cards_slots')
            ->where('card_id', $this->id)
            ->where('visible', true)
            ->where('extra', false)
            ->groupBy('product_id')
            ->select(['product_id', \DB::raw('COUNT(`id`) AS `count`')])
            ->orderByRaw('`count` DESC')
            ->get();
        if ($max) {
            return $productsInfo[0];
        }
        if (empty($productsInfo[1])) {
            $productsInfo[1] = (object)['product_id' => null, 'count' => 0, ];
        }
        if (empty($productsInfo[2])) {
            $productsInfo[2] = (object)['product_id' => null, 'count' => 0, ];
        }
        return $productsInfo;
    }

	/**
	 * @return null
	 */
    public function getStandinProductId() {
        if ($this->trial) {
            if ($this->count_slots_visible == 3 + ($this->extended ? 1 : 0)) {
                return $this->products()
                    ->whereNotIn('products.id', $this->slotsVisible()->lists('product_id'))
                    ->lists('products.id')
                    ->random();
            }
            return $this->products()
                ->lists('products.id')
                ->random();
        }

        $targetProfit = RouletteSetting::where('key', 'profit')->first()->value;
        $priceLimit = $this->steamCase->priceDefault->price * (100 - $targetProfit);

        $product_id = NULL;

        if ( ! $this->extended) {
            switch ($this->count_slots_visible) {
                case 0:
                    if ($this->drop->price > $priceLimit) {
                        $product_id = $this->drop->product_id;
                        break;
                    }
                    $products_id = $this->products()
                        ->where('products.id', '!=', $this->drop->product_id)
                        ->lists('products.id');
                        if($products_id->isEmpty()){
	                        Log::info('Products does not exists. Not extended, case 0.');
	                        return false;
                        }
	                    $product_id = $products_id->random();
                    break;

                case 1:
                    if ($this->slotsVisible()
                        ->where('product_id', $this->drop->product_id)
                        ->exists()) {
                        $product_id = $this->drop->product_id;
                    }
                    else {
                        $products_id = $this->products()
                            ->where('products.id', '!=', $this->drop->product_id)
                            ->lists('products.id');
                        if($products_id->isEmpty()){
	                        Log::info('Products does not exists. Not extended, case 1.');
	                        return false;
                        }
	                    $product_id = $products_id->random();
                    }
                    break;

                case 2:
                    if ($this->slotsVisible()
                        ->where('product_id', $this->drop->product_id)
                        ->exists()) {
                        $product_id = $this->drop->product_id;
                    } else {
                        $products_id = $this->products()
                            ->whereNotIn('products.id', $this->slotsVisible()->lists('product_id'))
                            ->lists('products.id');
	                    if($products_id->isEmpty()){
		                    Log::info('Products does not exists. Not extended, case 2.');
		                    return false;
	                    }
	                    $product_id = $products_id->random();
                    }
                    break;

                case 3:
                    $product_id = $this->drop->product_id;
                    break;
            }
        }

        if ($this->extended) {
            switch ($this->count_slots_visible) {
                case 3:
                    if ($this->slotsVisible()
                        ->where('product_id', $this->drop->product_id)
                        ->exists()) {
                        $product_id = $this->drop->product_id;
                    } else {
                        $products_id = $this->products()
                            ->whereNotIn('products.id', $this->slotsVisible()->lists('product_id'))
                            ->lists('products.id');
                        if($products_id->isEmpty()){
	                        Log::info('Products does not exists. Extended, case 3.');
                            return false;
                        }
	                    $product_id = $products_id->random();
                    }
                    break;
                case 4:
                    $product_id = $this->drop->product_id;
                    break;
            }
        }

        return $product_id;
    }

	/**
	 * @throws \Exception
	 */
    public function extend() {
        if ( ! Card::query()
            ->where('id', $this->id)
            ->where('status', 'choosing')
            ->update([
                'status'   => 'erasing1',
                'extended' => true,
            ])) {
            throw new \Exception('Card update data failed.');
        }
    }

	/**
	 * @param $productId
	 *
	 * @throws \Exception
	 */
    public function win($productId) {
        $productsInfo = $this->getDefaultSlotsProductsInfo(false);
        foreach ($productsInfo as &$productInfo) {
            if ($productInfo->count < 3) {
                if ( ! $productInfo->product_id) {
                    $productInfo->product_id = $this->products()
                        ->where('products.id', '!=', $productId)
                        ->whereNotIn('products.id', $this->slotsVisible()->lists('product_id'))
                        ->lists('products.id')
                        ->random();
                }
                if ( ! $productInfo->product_id) {
                    throw new \Exception('Cant determine productId for hidden slots.');
                }
                if ( ! CardSlot::query()
                    ->where('card_id', $this->id)
                    ->where('extra', false)
                    ->where('visible', false)
                    ->limit(3 - $productInfo->count)
                    ->update([
                        'product_id' => $productInfo->product_id,
                        'visible'    => true,
                    ])
                ) {
                    throw new \Exception('CardSlot update visible failed.');
                }
            }
        }
        if ( ! Card::query()
            ->where('id', $this->id)
            ->where('status', '!=', 'winned')
            ->update([
                'status'     => 'winned',
                'product_id' => $productId,
            ])) {
            throw new \Exception('Card update data failed.');
        }
        if ( ! $this->trial) {
            $this->drop->complete();
        }
    }

	/**
	 * @return float
	 */
    public function getExtendingPrice()
    {
	    $case_id = $this->drop->case_id;
        $priceObj = SteamCase::find($case_id)->priceObj;
        $currency = Currency::find(\Session::get('currency_id'));
        $k = ($currency->code == 'RUB') ? 67 : 1;

        if ($priceObj->price <= 0.39 * $k) {
            return 0.09 * $k;
        }

        if ($priceObj->price <= 2.99 * $k) {
            return 0.19 * $k;
        }

        if ($priceObj->price <= 9.99 * $k) {
            return 0.99 * $k;
        }

        if ($priceObj->price <= 49.99 * $k) {
            return 3.99 * $k;
        }

        return 19.99 * $k;
    }

	/**
	 * @param bool $demo
	 *
	 * @return bool|Model|null|static
	 */
	public function getActiveCard($demo = false){
		$CARD = false;

		if (Auth::check()) {
			$CARD = Card::query()
			            ->where('user_id', Auth::user()->id)
			            ->where('status', '!=', 'winned')
						->where('trial', '=', $demo ? 1 : 0)
			            ->first();

			if ($CARD && $CARD->status == 'choosing' && !$CARD->trial) {
				$CARD->extendingPrice = $CARD->getExtendingPrice() . Helper::get('currency')->symbol_left;
			}
		}

		return $CARD;
	}

}
