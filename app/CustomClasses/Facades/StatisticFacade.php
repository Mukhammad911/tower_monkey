<?php namespace App\CustomClasses\Facades;

use Illuminate\Support\Facades\Facade;

class StatisticFacade extends Facade
{
	protected static function getFacadeAccessor() {
		return 'statistic';
	}
}