<?php namespace App\CustomClasses\Facades;

use Illuminate\Support\Facades\Facade;

class PartnershipFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'partnership';
    }
}