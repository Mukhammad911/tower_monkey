<?php namespace App\CustomClasses\Facades;

use Illuminate\Support\Facades\Facade;

class HelperFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'helper';
    }
}