<?php namespace App\CustomClasses\Facades;

use Illuminate\Support\Facades\Facade;

class ProvablyFairFacade extends Facade
{
	protected static function getFacadeAccessor() {
		return 'provablyFair';
	}
}