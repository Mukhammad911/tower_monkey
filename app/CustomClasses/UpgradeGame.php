<?php

namespace App\CustomClasses;

use App\Models\Currency;
use App\Models\Operation;
use Auth;
use Helper as Help;

/**
 * Class UpgradeGame
 * @package App\CustomClasses
 */
class UpgradeGame
{
	/**
	 * @return float
	 */
	public static function income()
	{
		$total_income = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$income = Operation::where('description', 'upgrade bet')->where('currency_id', $currency->id)->sum('amount');
			$total_income += ($currency->id == 1) ? $income : Help::exchange($income, 1);
		}

		return -$total_income;
	}

	/**
	 * @return float
	 */
	public static function expense()
	{
		$total_expense = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$expense = Operation::where('description', 'upgrade win')->where('currency_id', $currency->id)->sum('amount');
			$total_expense += ($currency->id == 1) ? $expense : Help::exchange($expense, 1);
		}

		return $total_expense;
	}

	/**
	 * @return float
	 */
	public static function profit()
	{
		$income = self::income();
		$expense = self::expense();

		return $income - $expense;
	}

}
