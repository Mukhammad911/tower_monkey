<?php

namespace App\CustomClasses;

use App\Models\Currency;
use App\Models\Operation;
use App\Models\TowerSetting;
use Auth;
use Helper as Help;
use Statistic as Stat;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class RouletteGame
 * @package App\CustomClasses
 */
class RouletteGame
{
	/**
	 * @param bool $limit
	 * @param bool $border_time
	 *
	 * @return int
	 */
	public static function income($limit = false, $border_time = false)
	{
		$total_income = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$income = Operation::where('description', 'case opening')->where('currency_id', $currency->id);
			if($limit){
				$income = $income->take($limit);
			}elseif($border_time){
				//
			}
			$income = $income->sum('amount');
			$total_income += ($currency->id == 1) ? $income : Help::exchange($income, 1);
		}

		return -$total_income;
	}

	/**
	 * @param bool $limit
	 * @param bool $border_time
	 *
	 * @return int
	 */
	public static function expense($limit = false, $border_time = false)
	{
		$total_expense = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$expense = Operation::where('description', 'roulette win')->where('currency_id', $currency->id);
			if($limit){
				$expense = $expense->take($limit);
			}elseif($border_time){
				//
			}
			$expense = $expense->sum('amount');
			$total_expense += ($currency->id == 1) ? $expense : Help::exchange($expense, 1);
		}

		return $total_expense;
	}

	/**
	 * @return float
	 */
	public static function profit()
	{
		$income = self::income();
		$expense = self::expense();

		return $income - $expense;
	}

	/**
	 * @param $operations
	 * @param $border_time
	 *
	 * @return int
	 */
	public function deviations($operations, $border_time)
	{
		$deviations = [];
		$target_company_percent = Help::setting('profit');
		$i = 0;

		foreach ( $operations as $operation ) {
			$total_income = self::income($i);
			$total_expense = self::expense($i);
			$real_company_percent = Stat::currentCompanyProfit($total_income, $total_expense);
			$deviations[] = abs(100 - $real_company_percent / ($target_company_percent / 100));


			$i += 100;
		}

		return $deviations;
	}

}
