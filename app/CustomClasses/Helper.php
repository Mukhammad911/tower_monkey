<?php namespace App\CustomClasses;

use App;
use Auth;
use DB;
use Illuminate\Support\Facades\Redis;
use App\Models\CsgoCaseDrops;
use App\Models\PubgCases\PubgCase;
use App\Models\PubgCases\PubgCaseDrops;
use Session;
use App\Models\Balance;
use App\Models\Counter;
use App\Models\Currency;
use App\Models\Drop;
use App\Models\Language;
use App\Models\Operation;
use App\Models\User;
use App\Models\Product;
use App\Models\SteamCase;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class Helper
{
	public $data = [];

	public function __construct()
	{
        Log::info('Entered to __construct helper');
		$this->data['languages'] = Language::getActiveLanguages();

		if(Auth::check()){
			$this->user = Auth::user();

			$base_account = $this->getBalance();
			$base_account->summ = array_sum([
				$base_account->summ,
				$this->getBalance(1)->summ,
				$this->getBalance(2)->summ,
			]);

			$this->data['balance'] = $base_account;
		}

		$this->data['currency'] = Currency::find(Session::get('currency_id'));
		$this->data['lang'] = Language::getByLocale();
	}

	public function get($key)
	{
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}

	public function set($key, $val)
	{
		$this->data[$key] = $val;
	}

	public function isEmpty($key)
	{
		return empty($this->data[$key]);
	}

	public function all()
	{
		return $this->data;
	}

	/**
	 * @param $user_id
	 *
	 * @return float|int
	 */
	public function getTotalBalance($user_id)
	{
		$base_account = $this->getBalance(0, $user_id);

		return array_sum([
			$base_account->summ,
			$this->getBalance(1, $user_id)->summ,
			$this->getBalance(2, $user_id)->summ,
		]);
	}

	/**
	 * @param int $type
	 * @param int $user_id
	 *
	 * @return mixed
	 */
	public function getBalance($type = 0, $user_id = 0)
	{
		if($user_id == 0){
			$user_id = Auth::user()->id;
		}

		$balance = Balance::firstOrCreate([
			'user_id' => $user_id,
			'currency_id' => 1 /*empty(Session::get('currency_id')) ? 1 : Session::get('currency_id')*/,
			'type' => $type,
		]);

		return $balance;
	}

	/**
	 * @param $summ
	 *
	 * @return float
	 */
	public function currentPrice($summ){

		//Копейки в рубли
		$summ = $summ/100;

		if($this->data['currency']->code == 'RUB'){
			return $summ;
		}

		return round($summ / Helper::setting('rub_usd_sell'), 2);
	}

	/**
	 * @param $summ - kopecks
	 * @param $currency_id - id of currency in which it is need to convert
	 *
	 * @return float
	 */
	public function exchange($summ, $currency_id){
		$defaultCurrencyID = Currency::where('code','RUB')->first()->id;

		//Копейки в рубли
		$summ = $summ/100;

		if($defaultCurrencyID == $currency_id){
			return $summ;
		}
       // return round($summ / Setting::find(1)->rub_usd_sell, 2);
		return round($summ / Helper::setting('rub_usd_sell'), 2);
	}

	public function caseImageUrl($product){
		return ($product->image_source == 'url') ? $product->image : asset('images/products').'/'.$product->image;
	}

    public function casePubgImageUrl($product){
        return ($product->image_source == 'url') ? $product->image : asset('images/pubg_images').'/'.$product->image;
    }

	public function lastDropTime(){
		$last =  App\Models\LastWin::orderBy('created_at','desc')->first();

		Carbon::setLocale(App::getLocale());

		return (!empty($last)) ? $last->created_at->diffForHumans() : '';
	}

	public function liveDrops(){
		$last_wins = App\Models\LastWin::orderBy('created_at','desc')->get();
		foreach ( $last_wins as $key => $last_win ) {
			switch($last_win->game){
				/*case 'roulette':
					$drop = Drop::find($last_win->game_id);
					if(!$drop){
						unset($last_wins[$key]);
						continue;
					}
					$last_wins[$key]->drop = $drop;
					$last_wins[$key]->case = SteamCase::find($last_wins[$key]->drop->case_id);
					break;

				case 'upgrade':
					$upgrade = App\Models\Upgrade::find($last_win->game_id);
					if(!$upgrade){
						unset($last_wins[$key]);
						continue;
					}
					$last_wins[$key]->upgrade = $upgrade;
					break;
                */
				case 'tower':
					$tower = App\Models\Tower::find($last_win->game_id);
					if(!$tower){
						unset($last_wins[$key]);
						continue;
					}
					$last_wins[$key]->tower = $tower;
					break;
                case 'csgo-case-roulette':
                    $csgo_case_drop = CsgoCaseDrops::find($last_win->game_id);
                    if(!$csgo_case_drop){
                        unset($last_wins[$key]);
                        continue;
                    }
                    $last_wins[$key]->drop = $csgo_case_drop;
                    $last_wins[$key]->drop->avatar = User::select('avatar')->where('id',$csgo_case_drop->user_id)->first()->avatar;
                    $last_wins[$key]->drop->username = User::select('username')->where('id',$csgo_case_drop->user_id)->first()->username;
                    $last_wins[$key]->drop->case_name = SteamCase::select('name')->where('id',$csgo_case_drop->case_id)->first();
                    $last_wins[$key]->case = SteamCase::find($last_wins[$key]->drop->case_id);

                    $ex = $this->parseMarketNameCsgoLiveDrop($last_wins[$key]->drop->market_name);

                    $last_wins[$key]->nameParsed = $ex->name;
                    $last_wins[$key]->shortDescriptionParsed = $ex->short_description;
                    break;
                case 'pubg-case-roulette':
                    $pubg_case_drop = PubgCaseDrops::with('product')->find($last_win->game_id);
                    if(!$pubg_case_drop){
                        unset($last_wins[$key]);
                        continue;
                    }
                    $last_wins[$key]->drop = $pubg_case_drop;
                    $last_wins[$key]->drop->avatar = User::select('avatar')->where('id',$pubg_case_drop->user_id)->first()->avatar;
                    $last_wins[$key]->drop->username = User::select('username')->where('id',$pubg_case_drop->user_id)->first()->username;
                    $last_wins[$key]->drop->case_name = PubgCase::select('name')->where('id',$pubg_case_drop->case_id)->first();
                    $last_wins[$key]->case = PubgCase::find($last_wins[$key]->drop->case_id);
                    break;
                case 'coin-flip':
                    $coin_flip_drop = App\Models\CoinFlip::find($last_win->game_id);
                    if(!$coin_flip_drop){
                        unset($last_wins[$key]);
                        continue;
                    }

                    $last_wins[$key]->drop = $coin_flip_drop;

                    if ($coin_flip_drop->win == 'csgo')
                    {
                        $last_wins[$key]->drop->user_id = User::select('id')->where('id',$coin_flip_drop->csgo_user_id)->first()->id;
                        $last_wins[$key]->drop->avatar = User::select('avatar')->where('id',$coin_flip_drop->csgo_user_id)->first()->avatar;
                        $last_wins[$key]->drop->username = User::select('username')->where('id',$coin_flip_drop->csgo_user_id)->first()->username;
                    }
                    else
                    {
                        $last_wins[$key]->drop->user_id = User::select('id')->where('id',$coin_flip_drop->roll_user_id)->first()->id;
                        $last_wins[$key]->drop->avatar = User::select('avatar')->where('id',$coin_flip_drop->roll_user_id)->first()->avatar;
                        $last_wins[$key]->drop->username = User::select('username')->where('id',$coin_flip_drop->roll_user_id)->first()->username;
                    }

                    break;
			}
		}

		return $last_wins;

	}

    public function parseMarketNameCsgoLiveDrop($market_name){
        $str = '';
        $name = '';
        $short_description = '';

	    $str = explode('(', $market_name);

        if (sizeof($str) > 1)
        {
            $str = explode('|', $str[0]);
            $name = trim($str[0]);

            $str_new = explode('StatTrak™ ', $name);

            if (sizeof($str_new) > 1)
            {
                Log::info($str_new);

                $name = $str_new[0].$str_new[1];
            }

            $short_description = trim($str[1]);
        }

        return (object)[
            'name' => $name,
            'short_description' => $short_description
        ];
    }

    public function parseMarketNameCsgoProfile($market_name){
        $str = '';
        $name = '';
        $stattrak = 0;
        $short_description_quality = '';

        $str = explode('|', $market_name);

        if (sizeof($str) > 1)
        {
            $name = trim($str[0]);

            Log::info($name);

            $str_new = explode('StatTrak™ ', $name);

            if (sizeof($str_new) > 1)
            {
                Log::info($str_new);

                $name = $str_new[0].$str_new[1];

                $stattrak = 1;
            }

            $short_description_quality = trim($str[1]);
        }

        return (object)[
            'name' => $name,
            'stattrak' => $stattrak,
            'short_description_with_quality' => $short_description_quality
        ];
    }

	public function countUsers(){
		return User::where('steamid', '!=', '')->count();
	}

	public function onlineUsers(){
		return User::where('steamid', '!=', '')
		           ->whereRaw('last_activity >= DATE_ADD(NOW(), INTERVAL -10 MINUTE)')
		           ->count();
	}

	public function getCounter($class){
		return Counter::where('class', $class)->pluck('count');
	}

	public function getCounterAll(){
		return DB::table('counters')
		         ->select(DB::raw('SUM(count) as count'))
		         ->where('class','!=','stattrak')
		         ->pluck('count');
	}

    public function getCountDrops(){
        $pubg_games = App\Models\CounterGames::where('games', 'pubg-case')->first()->count;
        $csgo_games = App\Models\CounterGames::where('games', 'csgo-case')->first()->count;

        return ($pubg_games + $csgo_games);
    }

    public function countTower(){
        $tower_count = App\Models\Tower::where('status', 'closed')->count();

        return $tower_count;
    }

    public function countFlipCoin(){
        $flip_coin = App\Models\CounterGames::where('games', 'coin-flip')->first()->count;

        return $flip_coin;
    }


	public static function replace_unicode_escape_sequence($match) {
		return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
	}

	public static function unicode_decode($str) {
		return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'self::replace_unicode_escape_sequence', $str);
	}

    public static function marketName(CsgoCaseDrops $drop){
        $drop->market_name = str_replace($drop->quality, trans('weapon.'.$drop->quality),$drop->market_name);
        $drop->market_name = strtolower($drop->market_name);
        $drop->market_name = str_replace($drop->short_description, trans('weapon.'.$drop->short_description),$drop->market_name);

        if(Lang::has('weapon.'.$drop->name)){
            $drop->market_name = str_replace($drop->name, trans('weapon.'.$drop->name),$drop->market_name);
        }

        return $drop->market_name;
    }

    public static function pubgMarketName(PubgCaseDrops $drop){
        $drop->market_name = str_replace($drop->quality, trans('weapon.'.$drop->quality),$drop->market_name);
        $drop->market_name = strtolower($drop->market_name);
        $drop->market_name = str_replace($drop->short_description, trans('weapon.'.$drop->short_description),$drop->market_name);

        if(Lang::has('weapon.'.$drop->name)){
            $drop->market_name = str_replace($drop->name, trans('weapon.'.$drop->name),$drop->market_name);
        }

        return $drop->market_name;
    }

	/**
	 * @param $amount
	 * @param $currency_id
	 * @param $description
	 *
	 * @return bool
	 */
	public function debit($amount, $currency_id, $description = 'case opening')
	{
		$user_id = Auth::user()->id;
		$debit_remains = $amount;
		$balances = [
			Helper::getBalance(2),
			Helper::getBalance(1),
			Helper::getBalance(),
		];

		foreach ( $balances as $balance ) {
			// stop if debited
			if($debit_remains == 0){
				break;
			}
			// next account type if current is empty
			if($balance->summ == 0){
				continue;
			}
			// set up debit amount specifically for current account type
			$debit_amount = $debit_remains;
			if($balance->summ < $debit_amount){
				$debit_amount = $balance->summ;
			}
			// debit
			if(!$Balance = Balance::where('user_id', $user_id)
				->where('currency_id', $currency_id)
				->where('type', $balance->type)
				->first()
			){
				continue;
			}
			if ( ! Balance::where('id', $Balance->id)
					->where('summ', $Balance->summ)
					->update([
					  'summ' => \DB::raw('`summ` - ' . $debit_amount),
					]))
			{
				return false;
			}
			$Balance->summ -= $debit_amount;
			if(!$Balance->save()){
				$this->credit($amount - $debit_remains, $currency_id);
				return false;
			}
			Helper::operation(-$debit_amount, $currency_id, $description);

			$debit_remains -= $debit_amount;
		}

		if($debit_remains != 0){
			$this->credit($amount - $debit_remains, $currency_id);
			return false;
		}

		return true;
	}

	/**
	 * @param $amount
	 * @param $currency_id
	 * @param int $type
	 * @param string $desc
	 * @param int $user_id
	 *
	 * @return bool
	 */
	public function credit($amount, $currency_id, $type = 0, $desc = 'credit', $user_id = 0)
	{
		$Balance = Balance::firstOrNew([
			'user_id' => ($user_id == 0) ? Auth::user()->id : $user_id,
			'currency_id' => $currency_id,
			'type' => $type,
		]);

		$Balance->summ += $amount;
		if(!$Balance->save()){
			Log::error('unknown error within credit()');
			return false;
		}
		Helper::operation($amount, $currency_id, $desc, $user_id);

		return true;
	}

	/**
	 * @param $amount
	 * @param $currency_id
	 * @param $description
	 * @param int $user_id
	 *
	 * @return bool
	 */
	public function operation($amount, $currency_id, $description, $user_id = 0)
	{
		if($user_id == 0){
			$user_id = Auth::user()->id;
		}
		$user = User::where('id', $user_id)->first();
		$TotalBalance = $this->getTotalBalance($user_id);
		switch ($description){
			case 'roulette win':
				$timeout = 6000;
				break;

			case 'upgrade win':
				$timeout = 3000;
				break;

            case 'tower win':
				$timeout = 0;
				break;

            case 'tower bet':
				$timeout = 0;
				break;
            case 'flip coin':
				$timeout = 9000;
				break;

			default:
				$timeout = 0;
				break;
		}
		$event_data = [
			'user_id' => $user_id,
			'balance' => number_format($TotalBalance, 2, '.', ''),
			'timeout' => $timeout,
		];

		event(new \App\Events\RefreshBalance($event_data));

		// don't write any operations for youtubers
		/*if($user->youtuber == true){
			return true;
		}*/

		$Operation = new Operation();
		$Operation->user_id = $user_id;
		$Operation->amount = $amount;
		$Operation->balance = $TotalBalance;
		$Operation->currency_id = $currency_id;
		$Operation->description = $description;

		if(!$Operation->save()){
			return false;
		}

		return true;
	}

	/** Check imbalance for user
	 * @return bool
	 */
	public function imbalance()
	{
		$operations_total = Operation::where('user_id', '=', Auth::user()->id)->sum('amount');
		$balance_total = $this->data['balance']->summ;
		if(abs($balance_total - $operations_total) > 10){
			return true;
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getTopWinners()
	{
		return User::where('top_banned', 0)
			->orderBy('profit', 'desc')
			->where('profit', '>', 0)
			->take(6)
			->get();
	}

	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public function setting($key)
	{
		return Setting::where('key', $key)->first()->value;
	}

	/**
	 * @param $quota
	 * @param $case_id
	 * @param $case_price
	 * @param bool $all
	 *
	 * @return mixed
	 */
	public function getWeapon($quota, $case_id, $case_price, $all = false)
	{
		$price = $case_price * (100 - Helper::setting('profit')) / 100;

		$products = Product::where('class', 'chip');
		$products = $products->whereRaw("id IN(SELECT product_id FROM case_product cp WHERE cp.case_id={$case_id})");
		switch($quota){
			case 2:
				$products = $products->where('price', '>=', $case_price * 2);
				break;

			case 1:
				$products = $products->where('price', '>', $price);
				$products = $products->where('price', '<=', $case_price * 2);
				break;

			default:
				$products = $products->where('price', '<=', $price);
				break;
		}

		if($all == true){
			return $products->get();
		}

		$products = $products->orderByRaw('RAND()')->first();

		return $products;
	}
}