<?php

namespace App\CustomClasses;

use Helper as Help;
use App\Models\Currency;
use App\Models\Drop;
use App\Models\Order;
use Illuminate\Support\Facades\Log;

/**
 * Class Statistic
 * @package App\CustomClasses
 */
class Statistic
{
	/**
	 * @return int total income in dollars
	 */
	public function totalIncome()
	{
		return array_sum([
			RouletteGame::income(),
			UpgradeGame::income(),
			TowerGame::income(),
		]);
	}

	/**
	 * @return mixed total expense in dollars
	 */
	public function totalExpense()
	{
		return array_sum([
			RouletteGame::expense(),
			UpgradeGame::expense(),
			TowerGame::expense(),
		]);
	}

	/**
	 * @param $income
	 * @param $expense
	 *
	 * @return float
	 */
	public function currentCompanyProfit($income, $expense)
	{
		if($expense == 0){
			return 0;
		}

		return ( ($income / $expense - 1) * 100);
	}
} 