<?php
namespace App\CustomClasses;

use Auth;
use DB;
use Helper as Help;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Currency;
use App\Models\Balance;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


/**
 * Class Partnership
 * @package App\CustomClasses
 */
class Partnership
{
	/**
	 * @param bool $user_id
	 *
	 * @return bool
	 */
	public function getBonusPercent($user_id = false)
	{
		if(!Auth::check()){
			return false;
		}
		if(!$user_id ){
			$user_id = Auth::user()->id;
		}

		if( !$affiliate = Affiliate::where('user_id', $user_id)->first() ) {
			return false;
		}

		$refer = User::find($affiliate->refer_id);

		return $refer->partnerLevel->bonus_amount;
	}

	/**
	 * @param $user_id
	 *
	 * @return string
	 */
	public static function affiliatedAmount($user_id)
	{
		$affiliated_deposits = 0;
		$affiliates = Affiliate::where('refer_id', $user_id)
			->select(DB::raw("(SELECT sum(orders.amount) FROM orders WHERE orders.user_id = affiliates.user_id AND currency_id = 1 AND status = 'complete') as user_balance"))
			->get();

		foreach($affiliates as $affiliate){
			$affiliated_deposits += $affiliate->user_balance;
		}

		return $affiliated_deposits;
	}

	/**
	 * @param $refer_id
	 * @param $user_id
	 *
	 * @return bool
	 */
	public function newAffiliate($refer_id, $user_id)
	{
		// validation
		if(Affiliate::where('user_id', $user_id)->exists()){
			return false;
		}
		if(Affiliate::where('user_id', $user_id)->where('refer_id', $refer_id)->exists()){
			return false;
		}

		$refer = User::find($refer_id);

		// connect affiliate user to refer
		$Affiliate = new Affiliate();
		$Affiliate->user_id = $user_id;
		$Affiliate->refer_id = $refer_id;
		if(!$Affiliate->save()){
			Log::error('Error saving of a new Affiliate entity within newAffiliate()');
			return false;
		}

		// calculate new refer`s level
		if(!$this->updateLevel($refer)){
			Log::error('Error updating Level within newAffiliate()');
			return false;
		}

		return $refer->partnerLevel->bonus_amount;
	}

	/**
	 * @param $user_id
	 * @param $amount
	 * @param $currency
	 *
	 * @return bool
	 */
	public function referBonus($user_id, $amount, $currency)
	{
		if( !$affiliate = Affiliate::where('user_id', $user_id)->first() ) {
			return false;
		}
		$refer = User::find($affiliate->refer_id);
		$currency = Currency::where('code', $currency)->first();

		// add bonus to refer
		Help::credit($refer->partnerLevel->percent / 100, $currency->id, 1, 'referral bonus', $affiliate->refer_id);

		// add bonus to affiliate
		$affiliate_bonus = ($amount / 100) * $refer->partnerLevel->bonus_amount;
		Help::credit($affiliate_bonus, $currency->id, 1, 'referral bonus', $user_id);

		return true;
	}

	/**
	 * @param User $user
	 *
	 * @return bool
	 */
	public function updateLevel(User $user)
	{
		$affiliatedAmount = $this->affiliatedAmount($user->id);
		$my_level = AffiliateLevel::where('border_amount', '<=', $affiliatedAmount)->orderBy('level', 'desc')->first();
/*if($user->affiliate_level < $my_level->level){
			Help::credit(1.00, 1, 1, 'referral bonus', $user->id);
		}*/

		//$user->affiliate_level = $my_level->level;
		//$user->save();

		return true;
	}
} 