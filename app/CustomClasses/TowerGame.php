<?php

namespace App\CustomClasses;

use App\Models\Currency;
use App\Models\Operation;
use Auth;
use Helper as Help;
use Illuminate\Support\Facades\Log;

/**
 * Class TowerGame
 * @package App\CustomClasses
 */
class TowerGame
{
	/**
	 * @return float
	 */
	public static function income()
	{
		$total_income = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$income_tower = Operation::where('description', 'tower bet')->sum('amount');

            $income_roulette_opening_csgo = Operation::where('description', 'csgo case opening')->sum('amount');
            $income_roulette_opening_youtuber_csgo = Operation::where('description', 'csgo case opening youtube')->sum('amount');
            $moneyback_roulette_opening_csgo = Operation::where('description', 'money back csgo case')->sum('amount');
            $moneyback_roulette_opening_youtuber_csgo = Operation::where('description', 'money back csgo case youtube')->sum('amount');

            $income_roulette_opening_pubg = Operation::where('description', 'pubg case opening')->sum('amount');
            $income_roulette_opening_youtuber_pubg = Operation::where('description', 'pubg case opening youtube')->sum('amount');
            $moneyback_roulette_opening_pubg = Operation::where('description', 'money back pubg case')->sum('amount');
            $moneyback_roulette_opening_youtuber_pubg = Operation::where('description', 'money back pubg case youtube')->sum('amount');

            $total_income = $income_tower + $income_roulette_opening_csgo + $income_roulette_opening_youtuber_csgo + $moneyback_roulette_opening_csgo + $moneyback_roulette_opening_youtuber_csgo + $income_roulette_opening_pubg + $income_roulette_opening_youtuber_pubg + $moneyback_roulette_opening_pubg + $moneyback_roulette_opening_youtuber_pubg;

        }
        Log::info("tower ".$total_income);


		Log::info("Total income custom classes");
		Log::info($total_income);
		return -$total_income;
	}

	/**
	 * @return float
	 */
	public static function expense()
	{
		$total_expense = 0;
		foreach ( Currency::where('status', 1)->get() as $currency ) {
			$expense_tower = Operation::where('description', 'tower win')->sum('amount');

            $expense_roulette_csgo = Operation::where('description', 'csgo case win')->sum('amount');
            $expense_roulette_youtuber_csgo = Operation::where('description', 'csgo case win youtube')->sum('amount');

            $expense_roulette_pubg = Operation::where('description', 'pubg case win')->sum('amount');
            $expense_roulette_youtuber_pubg = Operation::where('description', 'pubg case win youtube')->sum('amount');

            $total_expense = $expense_tower + $expense_roulette_csgo + $expense_roulette_youtuber_csgo + $expense_roulette_pubg + $expense_roulette_youtuber_pubg;
		}
        Log::info("total_expense custom classes");
        Log::info($total_expense);
		return $total_expense;
	}

	/**
	 * @return float
	 */
	public static function profit()
	{
		$income = self::income();
		$expense = self::expense();

		return $income - $expense;
	}


}
