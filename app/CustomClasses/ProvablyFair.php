<?php

namespace App\CustomClasses;

use App\Models\Product;
use App\Models\ProvablyFairCsgoCase;
use App\Models\ProvablyFairPubgCase;
use App\Models\PubgCases\PubgCase;
use App\Models\SteamCase;
use Auth;
use Helper as Help;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class ProvablyFair
 * @package App\Models
 */
class ProvablyFair
{
	/**
	 * @param $case_id
	 *
	 * @return string
	 */
	public function getUserSeed($case_id = 0)
	{
		$user_seed = hash('sha256', $case_id.Auth::user()->id.mt_rand(999, 999999999));

		return $user_seed;
	}

	/**
	 * @return string
	 */
	public function getServerSeed()
	{
		$server_seed = hash('sha256',  mt_rand(999, 999999999).time());

		return $server_seed;
	}

	/**
	 * @param $case_id
	 *
	 * @return mixed
	 */
	public function getWinNumbers($case_id, $type)
	{
        if ($type == 'pubg' || $type == 'pubg-case')
        {
            $case = PubgCase::find($case_id);
            if(!$case){
                return [];
            }
            $products = $case->products()->orderBy('price', 'asc')->get();
            if(!$products){
                return [];
            }
        }
        else
        {
            $case = SteamCase::find($case_id);
            if(!$case){
                return [];
            }
            $products = $case->products()->orderBy('price', 'asc')->get();
            if(!$products){
                return [];
            }
        }

        if ($case->category()->first()->name == 'CSGO')
        {
            $rel = array();
            $sum_rel = 0;
            $percent = 0;
            $prev_sum = 0;
            $sum_range = 0;

            $fair_id = array();

            $case_price = $case->priceDefault->price;
            $products_fair = $case->products()->orderBy('price', 'asc')->get();

            $fair_table = ProvablyFairCsgoCase::where('case_id', $case->id)->get();

            for ($j = 0; $j < sizeof($products_fair); $j++) {
                $rel[$j] = $case_price / Help::exchange($products_fair[$j]->price,1);
                $sum_rel += $rel[$j];
            }

            for ($j = 0; $j < sizeof($fair_table); $j++) {
                $fair_id[$j] = $fair_table[$j]->product_id;
            }

            $j = 0;

            foreach ( $products as $key => $product ) {

                if (in_array($product->id, $fair_id))
                {
                    $prev_sum = round($sum_range);

                    $percent = ($rel[$j] * 100) / $sum_rel;

                    $sum_range += ($percent * 100000) / 100;

                    $fair_value = ProvablyFairCsgoCase::where('case_id', $case->id)->where('product_id', $product->id)->first();

                    $products[$key]->percent = $fair_value->chance;
                    $products[$key]->win_range_min = $fair_value->start;
                    $products[$key]->win_range_max = $fair_value->end;
                }
                else
                {
                    $prev_sum = round($sum_range);

                    $percent = ($rel[$j]*100) / $sum_rel;

                    $sum_range += ($percent * 100000) / 100;

                    $products[$key]->percent = number_format($percent, 4);
                    $products[$key]->win_range_min = $prev_sum + 1;
                    $products[$key]->win_range_max = round($sum_range);
                }

                $j++;
            }

            return $products;
        }


        if ($case->category()->first()->name == 'PUBG')
        {
            $rel = array();
            $sum_rel = 0;
            $percent = 0;
            $prev_sum = 0;
            $sum_range = 0;

            $fair_id = array();

            $case_price = $case->priceDefault->price;
            $products_fair = $case->products()->orderBy('price', 'asc')->get();

            $fair_table = ProvablyFairPubgCase::where('case_id', $case->id)->get();

            for ($j = 0; $j < sizeof($products_fair); $j++) {
                $rel[$j] = $case_price / Help::exchange($products_fair[$j]->price,1);
                $sum_rel += $rel[$j];
            }

            for ($j = 0; $j < sizeof($fair_table); $j++) {
                $fair_id[$j] = $fair_table[$j]->product_id;
            }

            $j = 0;

            foreach ( $products as $key => $product ) {
                if (in_array($product->id, $fair_id))
                {
                    $prev_sum = round($sum_range);

                    $percent = ($rel[$j] * 100) / $sum_rel;

                    $sum_range += ($percent * 100000) / 100;

                    $fair_value = ProvablyFairPubgCase::where('case_id', $case->id)->where('product_id', $product->id)->first();

                    $products[$key]->percent = $fair_value->chance;
                    $products[$key]->win_range_min = $fair_value->start;
                    $products[$key]->win_range_max = $fair_value->end;
                }
                else
                {
                    $prev_sum = round($sum_range);

                    $percent = ($rel[$j] * 100) / $sum_rel;

                    $sum_range += ($percent * 100000) / 100;

                    $products[$key]->percent = number_format($percent, 4);
                    $products[$key]->win_range_min = $prev_sum + 1;
                    $products[$key]->win_range_max = round($sum_range);
                }

                $j++;
            }

            return $products;
        }
	}

	/**
	 * @param $case_id
	 * @param $product
	 *
	 * @return bool
	 */
	public function getWinNumber($case_id, $product, $case_type)
	{
		if($case_id == 0 && $product == 0){
			$win_number = mt_rand(0, 100000);
		}else{
			$win_weapons = $this->getWinNumbers($case_id, $case_type);
			foreach($win_weapons as $win_weapon){
				if($win_weapon->id == $product->product_id){
					$win_number = mt_rand($win_weapon->win_range_min, $win_weapon->win_range_max);
					break;
				}
			}
			if(empty($win_number)){
				return false;
			}
		}

		return $win_number;
	}

	/**
	 * @param $case_id
	 * @param $attempt
	 * @param $win_number
	 *
	 * @return bool|string
	 */
	public function getHash($case_id, $attempt, $win_number)
	{
		if(!Session::has('user_seed.case_'.$case_id.'.attempt_'.$attempt) || !Session::has('server_seed.case_'.$case_id.'.attempt_'.$attempt)){
			return false;
		}
		$user_seed = Session::get('user_seed.case_'.$case_id.'.attempt_'.$attempt);
		$server_seed = Session::get('server_seed.case_'.$case_id.'.attempt_'.$attempt);
		$hash = hash('sha256', $user_seed.$server_seed.$win_number);

		return $hash;
	}
}
