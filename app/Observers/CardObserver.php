<?php

namespace App\Observers;

use App\Models\RouletteSetting;
use Helper;
use App\Models\Card;
use App\Models\CardSlot;
use App\Models\Product;
use App\Models\User;
use App\Models\Setting;

class CardObserver
{
	/**
	 * Listen to the Card creating event.
	 *
	 * @param Card $card
	 *
	 * @return bool
	 */
    public function creating(Card $card)
    {
	    if ($card->trial) {
		    if (Card::where('user_id', $card->user_id)->where('trial', 1)->exists()) {
			    return FALSE;
		    }
	    } else {
		    $card->case_id = $card->drop->case_id;
		    if (Card::where('user_id', $card->user_id)->where('status', '!=', 'winned')->where('trial', 0)->exists()) {
			    return FALSE;
		    }
	    }

	    return TRUE;
    }

	/**
	 * Listen to the Card created event.
	 *
	 * @param Card $card
	 *
	 * @return bool
	 */
    public function created(Card $card)
    {
        $slotsDefaultCount = 9;
        $slotsExtraCount = 1;

	    // create entries about card`s slots
        for ($slotNumber = 1; $slotNumber <= $slotsDefaultCount + $slotsExtraCount; $slotNumber++) {
            $cardSlot = new CardSlot();
            $cardSlot->number = $slotNumber;
            $cardSlot->extra = ! ($slotNumber <= $slotsDefaultCount);
            $card->slots()->save($cardSlot);
        }

        $productsQuery = $card->trial ? Product::query() : $card->steamCase->products();
        if ( ! $card->trial) {
	        // get some "price limit"
            $targetProfit = RouletteSetting::where('key', 'profit')->first()->value;
            $priceLimit = $card->steamCase->priceDefault->price * (100 - $targetProfit); // wtf?
	        // firstly save won product
            $card->products()->save(Product::find($card->drop->product_id));

            $productsQuery->where('products.id', '!=', $card->drop->product_id);
            $productsQuery->where('price', ($card->drop->price > $priceLimit) ? '<=' : '>', $priceLimit);
        }
        $card->products()->saveMany($productsQuery->orderByRaw(\DB::raw('RAND()'))->limit($card->trial ? 4 : 3)->get());

        $cardProductsCount = $card->products()->count();
        if ($cardProductsCount < 4) {
            $productsQuery = $card->trial ? Product::query() : $card->steamCase->products();
            $productsQuery->whereNotIn('products.id', $card->products()->lists('products.id'));
            $card->products()->saveMany($productsQuery->orderByRaw(\DB::raw('RAND()'))->limit(4 - $cardProductsCount)->get());
        }

        return TRUE;
    }

	/**
	 * Listen to the Card deleting event.
	 *
	 * @param Card $card
	 */
    public function deleting(Card $card)
    {
        //
    }
}
