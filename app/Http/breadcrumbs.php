<?php
use App\Models\SteamCase;

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url(Config::get('app.admin_prefix')));
});

Breadcrumbs::register('generic', function($breadcrumbs, $actions = [], $url)
{
    $breadcrumbs->parent('home');
    foreach ($actions as $action) {
        $breadcrumbs->push($action, url($url));
    }
});

Breadcrumbs::register('Profile', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('My Profile', url(Config::get('app.admin_prefix').'/profile'));
});

Breadcrumbs::register('Users', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Users', url(Config::get('app.admin_prefix').'/users'));
});


Breadcrumbs::register('Operations', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Operations', url(Config::get('app.admin_prefix').'/operations'));
});
Breadcrumbs::register('Operation-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Operations');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('Orders', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Orders', url(Config::get('app.admin_prefix').'/orders'));
});

Breadcrumbs::register('Affiliate-levels', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Affiliate levels', url(Config::get('app.admin_prefix').'/affiliate-levels'));
});
Breadcrumbs::register('Affiliate-level-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Affiliate-levels');
    $breadcrumbs->push('Affiliate level create');
});
Breadcrumbs::register('Affiliate-level-edit', function($breadcrumbs)
{
    $breadcrumbs->parent('Affiliate-levels');
    $breadcrumbs->push('Affiliate level edit');
});

Breadcrumbs::register('Affiliates', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Affiliates', url(Config::get('app.admin_prefix').'/affiliates'));
});
Breadcrumbs::register('Affiliate-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Affiliates');
    $breadcrumbs->push('Affiliate create');
});
Breadcrumbs::register('Affiliate-edit', function($breadcrumbs)
{
    $breadcrumbs->parent('Affiliates');
    $breadcrumbs->push('Affiliate edit');
});

Breadcrumbs::register('Scenarios', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Scenarios', url(Config::get('app.admin_prefix').'/scenarios'));
});
Breadcrumbs::register('Scenario-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Scenarios');
    $breadcrumbs->push('Create');
});
Breadcrumbs::register('Scenario-edit', function($breadcrumbs)
{
    $breadcrumbs->parent('Scenarios');
    $breadcrumbs->push('Edit');
});



Breadcrumbs::register('Categories', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Categories', url(Config::get('app.admin_prefix').'/categories'));
});

Breadcrumbs::register('Cases', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Cases', url(Config::get('app.admin_prefix').'/cases'));
});

Breadcrumbs::register('DotaCase', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Dota Cases', url(Config::get('app.admin_prefix').'/dota/cases'));
});

Breadcrumbs::register('Products', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Products', url(Config::get('app.admin_prefix').'/products'));
});

Breadcrumbs::register('Statistics', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Statistics', url(Config::get('app.admin_prefix').'/statistics'));
});

Breadcrumbs::register('News', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('News', url(Config::get('app.admin_prefix').'/news'));
});


Breadcrumbs::register('User-edit', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('Users');
    $breadcrumbs->push('Edit', url(Config::get('app.admin_prefix').'/user').'/'.$id.'/edit');
});

Breadcrumbs::register('User-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Users');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('Category-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Categories');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('Category-edit', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('Categories');
    $breadcrumbs->push('Edit', url(Config::get('app.admin_prefix').'/category').'/'.$id.'/edit');
});

Breadcrumbs::register('Case-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Cases');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('DotaCase-create', function($breadcrumbs)
{
    $breadcrumbs->parent('DotaCase');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('Case-edit', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('Cases');
    $breadcrumbs->push('Edit', url(Config::get('app.admin_prefix').'/case').'/'.$id.'/edit');
});

Breadcrumbs::register('Product-create', function($breadcrumbs)
{
    $breadcrumbs->parent('Products');
    $breadcrumbs->push('Create');
});

Breadcrumbs::register('Product-edit', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('Products');
    $breadcrumbs->push('Edit', url(Config::get('app.admin_prefix').'/product').'/'.$id.'/edit');
});

Breadcrumbs::register('bots', function($breadcrumbs, $action=null)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Bots', url(Config::get('app.admin_prefix').'/bots'));
    if(!empty($action)) {
        $breadcrumbs->push($action, url(Config::get('app.admin_prefix').'/bots'));
    }
});

Breadcrumbs::register('Settings-Localization', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Settings / Localization', url(Config::get('app.admin_prefix').'/settings/localization'));
});

Breadcrumbs::register('Settings-General', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Settings / General', url(Config::get('app.admin_prefix').'/settings/general'));
});

/** Site breadcrumbs **/

Breadcrumbs::register('site', function($breadcrumbs)
{
    $breadcrumbs->push(trans('messages.home'), url('/'));
});

Breadcrumbs::register('site-case', function($breadcrumbs, $case_name = null)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(strtoupper($case_name));
});

Breadcrumbs::register('site-contacts', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.contacts'));
});

Breadcrumbs::register('site-faq', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.faq'));
});

Breadcrumbs::register('site-guarantee', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.guarantees'));
});

Breadcrumbs::register('site-account', function($breadcrumbs, $username)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push($username);
});

Breadcrumbs::register('site-reviews', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.reviews'));
});

Breadcrumbs::register('site-top', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.lucky'));
});

Breadcrumbs::register('site-trades', function($breadcrumbs)
{
    $breadcrumbs->parent('site');
    $breadcrumbs->push(trans('messages.bots_trades'));
});
Breadcrumbs::register('Purchase', function($breadcrumbs, $action=null)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Purchases', url(Config::get('app.admin_prefix').'/purchase'));
    if(!empty($action)) {
        $breadcrumbs->push($action, url(Config::get('app.admin_prefix').'/purchase'));
    }
});

Breadcrumbs::register('Statistics-Orders', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Statistics / Orders', url(Config::get('app.admin_prefix').'/statistics/orders'));
});