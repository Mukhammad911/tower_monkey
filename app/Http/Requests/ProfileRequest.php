<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->level()>1) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' 		=> 'required|max:255',
            'email' 	=> 'required|email|unique:users,email,'.\Auth::user()->id,
            'password' 	=> 'min:8',
            'avatar'	=> 'max:2048|mimes:jpeg,gif,png,jpg'
        ];
    }

}
