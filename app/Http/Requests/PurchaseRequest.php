<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PurchaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->isAdmin()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bot_id'    => 'required|integer',
            'cases'     => 'required',
            'target'    => 'required|integer',
            'max_price' => 'required|integer'
        ];
    }
}
