<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class BotRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->isAdmin()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'steam_login' => 'required',
            'steam_password' => 'required',
            'steamid' => 'required',
            'email' => 'required|email',
            'email_password' => 'required',
        ];
    }

}
