<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CaseRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->isAdmin()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                $rules = [
                    'name' 	=> 'required|max:255',
                    'image'	=> 'required|max:2048|mimes:jpeg,png,jpg',
                    'price'	=> 'required',
                    'title'	=> 'max:255',
                ];

                if($this->input['price']){
                    foreach($this->input['price'] as $key => $val)
                    {
                        $rules['price_'.$key] = 'required|numeric|regex:/^\d{1,5}(\.\d{1,2})?$/';
                        $input['price_'.$key] = $val;
                    }
                }

                return $rules;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'name' 	=> 'required|max:255',
                    'image'	=> 'max:2048|mimes:jpeg,png,jpg',
                    'price'	=> 'required',
                    'title'	=> 'max:255',
                ];

                if($this->input['price']){
                    foreach($this->input['price'] as $key => $val)
                    {
                        $rules['price_'.$key] = 'required|numeric|regex:/^\d{1,5}(\.\d{1,2})?$/';
                        $input['price_'.$key] = $val;
                    }
                }
                return $rules;
            }
            default:
                break;
        }
    }

}
