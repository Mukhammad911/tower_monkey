<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class PubgProductRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->isAdmin()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                $rules = [
                    'name' 		=> 'required|max:255',
                    'price'		=> 'required',
                    'image'		=> 'required|max:2048|mimes:jpeg,png,jpg'
                ];

                if($this->input('image_source') == 'url'){
                    $rules['image'] = 'required|regex:/^http(s)?\:\/\//';
                }

                return $rules;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'name' 		=> 'required|max:255',
                    'price'		=> 'required',
                    'image'		=> 'max:2048|mimes:jpeg,png,jpg'
                ];

                if($this->input('image_source') == 'url'){
                    $rules['image'] = 'required|regex:/^http(s)?\:\/\//';
                }

                return $rules;
            }
            default:
                break;
        }
    }

}
