<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->isAdmin()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'name' 		=> 'required|max:255',
                    'email' 	=> 'required|email|unique:users',
                    'password' 	=> 'required|min:8',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
	                'username'  => 'required|max:255',
	                'email' 	=> 'email|unique:users,email, '.$this->input('id'),
                    'password' 	=> 'min:8',
                ];
            }
            default:
                break;
        }
    }

}
