<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();
        return ($user->level()>1) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profit' => 'integer|min:5|max:90',
            'rub_usd_sell'   => 'required|numeric|min:1|regex:/^\d{1,5}(\.\d{1,2})?$/',
            'rub_usd_buy'    => 'required|numeric|min:1|regex:/^\d{1,5}(\.\d{1,2})?$/',
        ];
    }

}
