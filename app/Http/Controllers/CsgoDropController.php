<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\DropsMarket;
use Illuminate\Support\Facades\Auth;
use Session;
use Helper;
use App;
use Lang;
use Config;
use ProvablyFair;
use Illuminate\Support\Facades\Log;

/**
 * Class BotController
 * @package App\Http\Controllers
 */
class CsgoDropController extends Controller
{
    private $user_id;
    private $status;
    private $price;
    private $product_id;
    private $name;
    private $short_description;
    private $market_name;
    private $image_hash;
    private $quality;
    private $class;
    private $stattrak;
    private $assetid;
    private $classid;
    private $instanceid;
    private $bot_id;

    public function saveAvailableProduct($id)
    {
        if(Auth::check()){
            $ret = ['success' => true, 'drop_id' => null];
            $user = Auth::user();
            $items = DB::table('available_products')->where('id', '=', $id)->get();

            foreach ($items as $item) {
                $this->product_id = $item->product_id;
                $this->price = $item->price;
                $this->name = $item->name;
                $this->short_description = $item->short_description;
                $this->market_name = $item->market_name;
                $this->image_hash=$item->image_hash;
                $this->quality = $item->quality;
                $this->class = $item->class;
                $this->stattrak = $item->stattrak;
                $this->assetid = $item->assetid;
                $this->classid = $item->classid;
                $this->instanceid = $item->instanceid;
                $this->bot_id = $item->bot_id;
            }
            $price = Helper::exchange($this->price, 1);

            try {
                DB::beginTransaction();

                $drop = new DropsMarket();
                $drop->user_id = $user->id;
                $drop->status = 'pending';
                $drop->product_id = $this->product_id;
                $drop->price = $this->price;
                $drop->name = $this->name;
                $drop->short_description = $this->short_description;
                $drop->market_name = $this->market_name;
                $drop->image_hash = $this->image_hash;
                $drop->quality = $this->quality;
                $drop->class = $this->class;
                $drop->stattrak = $this->stattrak;
                $drop->assetid = $this->assetid;
                $drop->classid = $this->classid;
                $drop->instanceid = $this->instanceid;
                $drop->bot_id = $this->bot_id;
                $drop->currency_id = 1;

                if (!$drop->save()) {
                    return response()->json(['success' => false, 'error' => 'server_error']);
                }
                DB::commit();


                $user->balance()
                    ->where('currency_id',1)
                    ->where('type', 0)
                    ->update(['summ'=>DB::raw("summ - $price")]);


                $ret['drop_id'] = $drop->id;
                DB::table('available_products')->where('id', '=', $id)->delete();

            } catch (\Exception $e) {
                Log::error($e->getMessage());
                DB::rollback();

                $this->moneyBack($user, $price);

                $ret['notify']  = true;
                $ret['success'] = false;
                return response()->json($ret);
            }
            $ret['win'] = $drop;
            return response()->json($ret);
        }
    }
    private function moneyBack($user, $amount)
    {
        $user->balance()
            ->where('currency_id',1)
            ->where('type', 0)
            ->update(['summ'=>DB::raw("summ + $amount")]);
    }
}
