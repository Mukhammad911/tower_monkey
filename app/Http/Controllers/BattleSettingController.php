<?php

namespace App\Http\Controllers;

use App\Models\BattleSetting;
use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class BattleSettingController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$settings = DB::table('battle_settings')
		              ->select('*');

		$res = Datatables::of($settings)
		                 ->editColumn('value', function($setting){
			                 return '<span class="label label-warning">'.$setting->value.' '.$setting->symbol.'</span>';
		                 })
		                 ->editColumn('updated_at', function($setting){
			                 return '<span class="label label-default">'.$setting->updated_at.'</span>';
		                 })
		                 ->addColumn('action', function ($setting) {
			                 return '<ul class="icons-list">
                                <li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/battle-setting/$setting->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
                                <li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteBAttleSetting('.$setting->id.')" data-original-title="Delete"><i class="icon-bin"></i> Delete</a></li>
                            </ul>';
		                 })
		                 ->make(true);
		return $res;
	}

	public function getIndex()
	{
		return view('admin.battle_settings.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.battle_settings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'key' => 'required|max:255',
			'value' => 'required|max:255',
			'symbol' => 'max:10',
			'description' => 'max:255',
		]);

		$TowerSetting = new BattleSetting();
		$TowerSetting->key = $request->input('key');
		$TowerSetting->value = $request->input('value');
		$TowerSetting->symbol = $request->input('symbol');
		$TowerSetting->description = $request->input('description');
		$TowerSetting->save();

		return redirect(Config::get('app.admin_prefix').'/battle-settings')->with('success', 'New Battle setting has been successfully created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param BattleSetting $BattleSetting
	 *
	 * @return $this
	 */
	public function edit(BattleSetting $BattleSetting)
	{
		return view('admin.tower_settings.edit')->with('setting', $BattleSetting);
	}

	/**
	 * @param BattleSetting $BattleSetting
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(BattleSetting $BattleSetting, Request $request)
	{
		$this->validate($request, [
			'key' => 'required|max:255',
			'value' => 'required|max:255',
			'symbol' => 'max:10',
			'description' => 'max:255',
		]);

		$BattleSetting->key = $request->input('key');
		$BattleSetting->value = $request->input('value');
		$BattleSetting->symbol = $request->input('symbol');
		$BattleSetting->description = $request->input('description');
		$BattleSetting->save();

		return redirect(Config::get('app.admin_prefix').'/battle-settings')->with('success', 'Battle setting has been successfully updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		BattleSetting::destroy($id);
		return response()->json(true);
	}
}
