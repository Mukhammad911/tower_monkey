<?php

namespace App\Http\Controllers;

use App\Models\SteamCase;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Yajra\Datatables\Datatables;
use App\Models\Purchase;
use App\Models\Product;
use App\Models\Bot;
use App\Models\DropsMarket;
use App\Http\Requests\PurchaseRequest;
use Lang;
use Config;

class CsgoPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.csgo_purchases.index');
    }

    public function getPurchases()
    {
        $data = DB::table('csgo_purchases')->get();
        return view('admin.csgo_purchases.index', ['data' => $data]);
    }

    public function deletePurchases($id)
    {
        $purchase_id = $id;
        DB::table('csgo_purchases')->where('id', '=', $purchase_id)->delete();
        DB::table('csgo_search_items')->where('purchase_id', '=', $purchase_id)->delete();

        return redirect('spidercontrolpanel/csgo-purchase');
    }

    public function createFormPurchases()
    {
        return view('admin.csgo_purchases.create');
    }

    public function createPurchases()
    {
        $dota_bots = DB::table('bots')->select('id', 'steam_login')->get();
        return response()->json($dota_bots);
    }

    public function getProductPurchases()
    {
        $csgo_products = DB::table('products')->select('id', 'name', 'short_description', 'image', 'price','type')->where('short_description','!=','chip')->orderBy('name','asc')->groupBy('name','short_description')->get();
        return response()->json($csgo_products);
    }

    public function savePurchases(Request $request)
    {
        $error = ['var_error', 'bot_error', 'not_inserted'];
        $check_exist_bot_id = false;
        $bot_id = $request->input('bot_id');
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $quality = $request->input('quality');
        $rarity = $request->input('rarity');
        $purchase_id = '';
        if ($bot_id == '' || $min_price == '' || $max_price == '' || $quality == '' || $rarity == '') {
            return response()->json($error[0]);
        } else {
            $db_bot_id = DB::table('csgo_purchases')->select('bot_id')->get();
            foreach ($db_bot_id as $bot_id_id) {
                if ($bot_id == $bot_id_id->bot_id) {
                    $check_exist_bot_id = true;
                }
            };
            if ($check_exist_bot_id) {
                return response()->json($error[1]);
            } else {
                $inserted_purchase = DB::table('csgo_purchases')->insert([
                    'bot_id' => $bot_id,
                    'min_price' => $min_price,
                    'max_price' => $max_price,
                    'quality' => $quality,
                    'rarity' => $rarity
                ]);
                if ($inserted_purchase) {
                    $purchases_id = DB::table('csgo_purchases')->select('id')->where('bot_id', '=', $bot_id)->get();
                    foreach ($purchases_id as $id) {
                        $purchase_id = $id->id;
                    }
                    $json_response_true = ['message' => 'success', 'purchase_id' => $purchase_id];
                    return response()->json($json_response_true);
                } else {
                    return response()->json($error[2]);
                }
            }
        }
    }

    public function saveProductPurchases(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $product_name = $request->input('name');
        $product_quota = $request->input('quota');
        $product_price = $request->input('price');
        $check =DB::table('csgo_search_items')->insert([
            'purchase_id' => $purchase_id,
            'name' => $product_name,
            'quota' => $product_quota,
            'price'=>$product_price
        ]);
        if($check){

        }
        if(!$check){
            Log::info("Error insert product: ".$product_name);
        }
    }

    public function saveProductPurchasesComplete(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $bot_id = $request->input('bot_id');

        Redis::publish(Config::get('app.redis_prefix') . 'csgo-purchase-change-' . $bot_id, json_encode(['purchaseId' => $purchase_id]));

        $json_response_true = ['message' => 'success', 'bot_id' => $bot_id, 'purchase_id' => $purchase_id];
        return response()->json($json_response_true);
    }
}
