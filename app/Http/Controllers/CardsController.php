<?php namespace App\Http\Controllers;

use App;
use App\Http\Requests;
use App\Models\Card;
use App\Models\Counter;
use App\Models\Drop;
use Config;
use DB;
use Helper;
use Illuminate\Support\Facades\Auth;
use Lang;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/**
 * Class CardsController
 * @package App\Http\Controllers
 */
class CardsController extends Controller
{

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function postTrial()
    {
        $ret = ['success'=>true,];

        try {
            $card = DB::transaction(function () use (&$ret) {
                $card = new Card();
                $card->user_id = Auth::user()->id;
                $card->trial = 1;

                if ( ! $card->save()) {
                    $ret['message'] = trans('messages.activecard');
                    throw new \Exception('Already exists active drop (or trial has been tried)');
                }

                return $card;
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            $ret['notify']  = true;
            $ret['success'] = false;
            return response()->json($ret);
        }

        $card = Card::find($card->id);

        Redis::publish(config('app.redis_prefix') . 'cards:' . $card->status, json_encode(['userId' => Auth::user()->id, 'card' => $card, ]));

        $ret['card'] = $card;

        return response()->json($ret);
    }

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function postErase(Request $request)
    {
        $card = Card::where('user_id', Auth::user()->id)->find($request->input('card_id'));
        if ( ! $card) {
            return response()->json(['message' => 'no card'], 422);
        }

        $slot = $card->slots()->where('number', $request->input('number'))->first();
        if ( ! $slot) {
            return response()->json(['message' => 'no slot'], 422);
        }
	    if($slot->card->status == 'winned'){
		    return response()->json(['message' => 'You are already won this card! <br> Check out your profile page.'], 422);
	    }
	    if($slot->visible){
		    return response()->json(['message' => 'This field has been erased already.'], 422);
	    }
	    // for broken drops
	    /*if($card->products()->count() < 4 && $card->extended == 1 && $card->count_slots_visible == 3 ){
		    $card->status = 'winned';
		    $card->save();
	    }*/

        try {
            DB::transaction(function () use ($slot) {
                $slot->erase();
            });
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['message' => 'Some error in erasing. Please, contact us support@csgotower.net'], 422);
        }

        $card = Card::find($card->id);

        if ( ! $card->trial) {
	        switch($card->status){
		        case 'choosing':
			        $card->extendingPrice = $card->getExtendingPrice() . Helper::get('currency')->symbol_left;
			        break;

		        case 'winned':
			        $card->dropBotSteamid = Drop::find($card->drop_id)->bot->steamid;
			        $card->dropAssetid = Drop::find($card->drop_id)->assetid;
			        $card->dropPrice = Helper::currentPrice(Drop::find($card->drop_id)->price) . Helper::get('currency')->symbol_left;
			        $card->dropImageHash = Drop::find($card->drop_id)->image_hash;

					$this->updateCounters($card->drop);
			        break;
	        }
        }

        Redis::publish( config('app.redis_prefix') . 'cards:' . $card->status,
	        json_encode([
		        'userId' => Auth::user()->id,
		        'card' => $card,
	        ]));

        return response()->json(['card' => $card], 200);
    }

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
    public function postExtend(Request $request)
    {
        $card = Card::where('user_id', Auth::user()->id)->find($request->input('card_id'));
        if ( ! $card) {
            return response()->json(['success' => false], 404);
        }
	    $extended_price = 0;

        if (($card->status == 'choosing') && ( ! $card->trial)) {
	        $extended_price = $card->getExtendingPrice();
            if (( ! Helper::get('balance')) || (Helper::get('balance')->summ < $extended_price)) {
                return response()->json(['success' => false,'reason' => 'notenough',]);
            }

            if (!Helper::debit($card->getExtendingPrice(), Session::get('currency_id'), 'extra erase card') ) {
                return response()->json(['success'=>false,'reason' => 'server_error',]);
            }
        }

        try {
            DB::transaction(function () use ($card) {
                $card->extend();
            });
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['success' => false]);
        }

        $card = Card::find($card->id);

        Redis::publish(config('app.redis_prefix') . 'cards:' . $card->status, json_encode(['userId' => Auth::user()->id, 'card' => $card, ]));

        return response()->json([
	        'success' => true,
	        'card' => $card,
	        'extended_price' => $extended_price,
        ]);
    }

	/**
	 * @param $weapon
	 */
	private function updateCounters($weapon){
		if(!in_array($weapon->class,['common','uncommon'])){
			$counter = Counter::where('class',$weapon->class)->first();
			$counter->count++;
			$counter->save();
		}

		if((bool)$weapon->stattrak){
			$counter = Counter::where('class','stattrak')->first();
			$counter->count++;
			$counter->save();
		}
	}

}
