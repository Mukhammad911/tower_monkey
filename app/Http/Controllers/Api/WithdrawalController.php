<?php

namespace App\Http\Controllers\Api;

use Auth;
use Helper;
use App\Models\Withdrawal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

/**
 * Class WithdrawalController
 * @package App\Http\Controllers\Api
 */
class WithdrawalController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function paypal(Request $request)
	{
		$this->validate($request, [
			'amount' => 'required|numeric',
			'email' => 'required|email'
		]);
		if($request->input('amount') > Helper::get('balance')->summ){
			return response()->json('Not enough money.', 422);
		}

		$Withdrawal = new Withdrawal();
		$Withdrawal->user_id = Auth::user()->id;
		$Withdrawal->amount = $request->input('amount');
		$Withdrawal->type = 'paypal';
		$Withdrawal->details = serialize([
			'email' => $request->input('email'),
		]);
		if(!$w_id = $Withdrawal->save()){
			return response()->json('Something went wrong. Please, try again later.', 422);
		}

		if(!Helper::debit($request->input('amount'), 1, 'withdrawal')){
			$Withdrawal = Withdrawal::find($w_id);
			$Withdrawal->status = 'rejected';
			return response()->json('Not enough money.', 422);
		}

		return response()->json([
			'message' => 'Your request has been successfully saved!',
			'amount' => $request->input('amount'),
		], 200);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function visa(Request $request)
	{
		$this->validate($request, [
			'amount' => 'required|numeric',
			'card_number' => 'required|numeric',
			'holder_name' => 'required|string'
		]);
		if($request->input('amount') > Helper::get('balance')){
			return response()->json('Not enough money.', 422);
		}

		$Withdrawal = new Withdrawal();
		$Withdrawal->user_id = Auth::user()->id;
		$Withdrawal->amount = $request->input('amount');
		$Withdrawal->type = 'visa';
		$Withdrawal->details = serialize([
			'card_number' => $request->input('card_number'),
			'holder_name' => $request->input('holder_name'),
		]);
		if(!$w_id = $Withdrawal->save()){
			return response()->json('Something went wrong. Please, try again later.', 422);
		}

		if(!Helper::debit($request->input('amount'), 1, 'withdrawal')){
			$Withdrawal = Withdrawal::find($w_id);
			$Withdrawal->status = 'rejected';
			return response()->json('Not enough money.', 422);
		}

		Helper::debit($request->input('amount'), 1, 'withdrawal');

		return response()->json([
			'message' => 'Your request has been successfully saved!',
			'amount' => $request->input('amount'),
		], 200);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function bitcoin(Request $request)
	{
		$this->validate($request, [
			'amount' => 'required|numeric',
			'account' => 'required|numeric'
		]);
		if($request->input('amount') > Helper::get('balance')){
			return response()->json('Not enough money.', 422);
		}

		$Withdrawal = new Withdrawal();
		$Withdrawal->user_id = Auth::user()->id;
		$Withdrawal->amount = $request->input('amount');
		$Withdrawal->type = 'bitcoin';
		$Withdrawal->details = serialize([
			'account' => $request->input('account'),
		]);
		if(!$w_id = $Withdrawal->save()){
			return response()->json(['message' => 'Something went wrong. Please, try again later.'], 422);
		}

		if(!Helper::debit($request->input('amount'), 1, 'withdrawal')){
			$Withdrawal = Withdrawal::find($w_id);
			$Withdrawal->status = 'rejected';
			return response()->json('Not enough money.', 422);
		}

		Helper::debit($request->input('amount'), 1, 'withdrawal');

		return response()->json([
			'message' => 'Your request has been successfully saved!',
			'amount' => $request->input('amount'),
		], 200);
	}
}
