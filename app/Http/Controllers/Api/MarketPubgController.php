<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveTower;
use App\Models\Operation;
use App\Models\PubgProduct;
use App\Models\Tower;
use App\Models\TowerSetting;
use App\Models\Upgrade;
use App\Models\UpgradeSetting;
use Partnership;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Balance;
use App\Models\Card;
use App\Models\User;
use App\Models\Category;
use App\Models\SteamCase;
use App\Models\DropsMarket;
use App\Models\PubgDropsMarket;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Lang;
use App\Models\Drop;
use App\Models\Setting;
use App\Models\Currency;
use Helper;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\PubgAvailableProduct;
use JavaScript;
use Config;
use ProvablyFair;

class MarketPubgController extends Controller
{
    public function getPubgskins(Request $request)
    {
        if((Auth::check()) && ($request->wantsJson())) {
            $cases = DB::select("SELECT * FROM `pubg_available_products`");
            return $cases;
        }
    }

    public function getPubgProfileModal()
    {
        $user_id = Auth::user()->id;
        $count = DB::table('pubg_drops')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->count();

        return response()->json($count);
    }



    public function getPubgProfile()
    {
        $user_id = Auth::user()->id;
        $profile_product = DB::table('pubg_drops')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->get();
        return response()->json($profile_product);
    }
}
