<?php

namespace App\Http\Controllers\Api;
use App;
use Helper;
use App\Models\Currency;
use App\Models\Operation;
use App\Models\Game;
use App\Models\LastWin;
use App\Models\LiveTower;
use App\Models\Tower;
use App\Models\TowerAlgorithm;
use App\Models\TowerCell;
use App\Models\TowerScenario;
use App\Models\TowerScenarioAlgorithm;
use App\Models\TowerSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;
use Config;
use DB;
use Illuminate\Support\Facades\Redis;
use ProvablyFair;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Models\Balance;
use Illuminate\Support\Facades\Session;
use Statistic;

/**
 * Class TowerController
 * @package App\Http\Controllers\Api
 */
class TowerController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private $bonus_balance;
    public function start(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'bet' => 'required|numeric|between:0.1,5000',
            'level' => 'required',
        ]);

        DB::beginTransaction();
        $Tower = new Tower();

        $current_game = $Tower->getCurrentGame();
        if($current_game){
            return response()->json([
                'success' => false,
                'message' => 'You already have an opened game!',
            ]);
        }

        if($request->input('demo') == 'yes'){
            return response()->json([
                'success' => true,
                'message' => 'Demo game started!',
            ]);
        }

        $sum_locale = Balance::where('user_id', Auth::user()->id)
            ->where('type', 2)
            ->where('currency_id', 1)
            ->first();

        $this->bonus_balance = $sum_locale->summ;
        $data = DB::table('tower_promo')->where('user_id','=',Auth::user()->id)->get();

        if(!sizeof($data)){
            DB::table('tower_promo')->insert([
                'user_id'=>Auth::user()->id,
                'last_balance'=>$this->bonus_balance,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
        else{
            DB::table('tower_promo')->where('user_id','=',Auth::user()->id)->update([
                'last_balance'=>$this->bonus_balance,
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        if (Auth::user()->youtuber) {
            if (!Helper::debit($request->input('bet'), 1, 'tower bet youtube')) {
                return response()->json([
                    'success' => false,
                    'message' => 'Not enough money!',
                ]);
            }

        }
        elseif ($sum_locale->summ > 0){
            if (!Helper::debit($request->input('bet'), 1, 'tower bet from promo')) {
                return response()->json([
                    'success' => false,
                    'message' => 'Not enough money!',
                ]);
            }
        }
        else {
            if (!Helper::debit($request->input('bet'), 1, 'tower bet')) {
                return response()->json([
                    'success' => false,
                    'message' => 'Not enough money!',
                ]);
            }
        }


        $Tower->user_id = $user->id;
        $Tower->bet = $request->input('bet');
        $Tower->profit = - $request->input('bet');
        $Tower->level = $request->input('level');
        $Tower->step = 1;
        $Tower->status = 'started';
        $Tower->save();

        DB::commit();

        $scenarios = TowerScenario::getScenarios($user, $Tower->level);
        if(Config::get('app.promotion_mode') && $scenarios) {
            $TowerScenarioAlgorithm = new TowerScenarioAlgorithm( $user, $Tower->level, $Tower->bet);
            $TowerScenarioAlgorithm->updateRedisCounters();
        }

        return response()->json([
            'success' => true,
            'message' => 'Game started!',
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOpenedGame()
    {
        $current_game = Tower::getCurrentGame();
        if(!$current_game){
            return response()->json([
                'success' => false,
            ]);
        }

        $current_game->multiplier = TowerSetting::getMultiplier($current_game->level);
        $current_game->cells;

        return response()->json([
            'success' => true,
            'game' => $current_game,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function openCell(Request $request)
    {


        $this->validate($request, [
            'bet' => 'required|numeric|between:0.1,5000',
            'level' => 'required',
            'cell_number' => 'required',
        ]);

        $TowerCell = new TowerCell();

        if($request->input('demo') == 'yes'){
            $game_result = $this->getGameResult(
                $request->input('level'),
                $request->input('bet'),
                0,
                $request->input('demo')

            );

            return response()->json([
                'success' => true,
                'status' => $game_result,
            ]);
        }

        $current_game = Tower::getCurrentGame();
        if(!$current_game) {
            return response()->json( [
                'success' => false,
                'message' => 'Please, start the game first!',
            ] );
        }

        $game_result = $this->getGameResult(
            $current_game->level,
            $request->input('bet'),
            $current_game->step,
            $request->input('demo')

        );

        $current_game->step += 1;
        $current_game->status = ($game_result == 'victory') ? 'started' : 'closed';
        $current_game->save();

        $TowerCell->tower_id = $current_game->id;
        $TowerCell->step = $current_game->step - 1;
        $TowerCell->cell = $request->input('cell_number');
        $TowerCell->win = ($game_result == 'victory') ? true : false;

        $TowerCell->save();

        if($game_result == 'loss'){
            $this->live($current_game->id);
            $this->liveLeft($current_game->id);

            $user = Auth::user();

            $user->profit = $user->profit + number_format($current_game->profit, 2);
            $user->count_drops++;
            $user->save();

            if($current_game->profit > 0){
                Game::create([
                    'user_id' => $current_game->user_id,
                    'tower_id' => $current_game->id,
                    'profit' => number_format($current_game->profit, 2),
                ]);
            }
        }

        return response()->json([
            'success' => true,
            'status' => $game_result,
        ]);
    }

    /**
     * @param $level
     * @param $bet
     * @param $current_step
     * @param $demo
     *
     * @return string
     */
    private function getGameResult($level, $bet, $current_step, $demo)
    {
        Log::info('level');
        Log::info($level);
        $user = Auth::user();
        $scenarios = TowerScenario::getScenarios($user, $level);

        if(Auth::user()->youtuber && $demo =='yes'){
            $TowerAlgorithm = new TowerAlgorithm($bet);
            $result = $TowerAlgorithm->play($level, $bet, $demo);
            return $result;
        }
        else if(Auth::user()->youtuber && $demo =='no'){
            $easy_random = Helper::setting('easy_level_random');
            $medium_random = Helper::setting('medium_level_random');
            $hard_random = Helper::setting('hard_level_random');

            Log::info("Tower youtuber ".Auth::user()->id." easy_random = ".$easy_random." medium_random = ".$medium_random." hard_random = ".$hard_random);

            if($level=='easy'){
                $rand = mt_rand(1,1000000);
                if($rand<=$easy_random){
                    return 'victory';
                }
                else{
                    return 'loss';
                }
            } else if($level=='medium'){
                $rand = mt_rand(1,1000000);
                if($rand<=$medium_random){
                    return 'victory';
                }
                else{
                    return 'loss';
                }
            } if($level=='hard'){
                $rand = mt_rand(1,1000000);
                if($rand<=$hard_random){
                    return 'victory';
                }
                else{
                    return 'loss';
                }
            }
            else{
                return 'loss';
            }
            /*
               $current_game = Tower::getCurrentGame();
               $multiplier = TowerSetting::getMultiplier($current_game->level);
               $profit = $current_game->bet * ($multiplier ** ($current_game->step));
               $rand = mt_rand(1,1000000);
               $win = $bet/$profit*1000000;
               Log::info('Tower User_id: '.Auth::user()->id);
               Log::info('Win Tower value: '.$win);
               Log::info('Win Tower rand: '.$rand);
               if($rand<=$win){
                   return 'victory';
               }
               else{
                   return 'loss';
               }*/
        }
        else{
            $TowerAlgorithm = new TowerAlgorithm($bet);
            $result = $TowerAlgorithm->play($level, $bet, $demo);
            return $result;

        }

    }
    private function income_youtube(){
        $total_income = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $income_roulette = Operation::where('description', 'case opening youtube')->where('user_id','=',Auth::user()->id);
            $income_upgrade = Operation::where('description', 'upgrade bet youtube')->where('user_id','=',Auth::user()->id);
            $income_tower = Operation::where('description', 'tower bet youtube')->where('user_id','=',Auth::user()->id);
            $total_income_roulette = $income_roulette->sum('amount');
            $total_income_upgrade = $income_upgrade->sum('amount');
            $total_income_tower = $income_tower->sum('amount');
            $total_income = (float)$total_income_roulette+(float)$total_income_upgrade+(float)$total_income_tower;
        }
        Log::info($total_income);
        return -$total_income;
    }
    private function expense_youtube(){
        $total_expense = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $expense_roulette = Operation::where('description', 'roulette win youtube')->where('user_id','=',Auth::user()->id);
            $expense_upgrade = Operation::where('description', 'upgrade win youtube')->where('user_id','=',Auth::user()->id);
            $expense_tower = Operation::where('description', 'tower win youtube')->where('user_id','=',Auth::user()->id);
            $total_expense_roulette = $expense_roulette->sum('amount');
            $total_expense_upgrade = $expense_upgrade->sum('amount');
            $total_expense_tower = $expense_tower->sum('amount');
            $total_expense = (float)$total_expense_roulette+(float)$total_expense_upgrade+(float)$total_expense_tower;
        }
        Log::info($total_expense);
        return $total_expense;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function takeProfit()
    {
        DB::beginTransaction();
        $current_game = Tower::getCurrentGame();
        if(!$current_game){
            return response()->json([
                'success' => false,
                'message' => 'You are have been taken a profit already!',
            ]);
        }

        $multiplier = TowerSetting::getMultiplier($current_game->level);
        $profit = $current_game->bet * ($multiplier ** ($current_game->step - 1));

        $current_game->profit = $profit - $current_game->bet;
        $current_game->status = 'closed';
        if(!$current_game->save()){
            return response()->json([
                'success' => false,
                'message' => 'Something went wrong, please, try again later!',
            ]);
        }
        if($current_game->profit > 0){
            Game::create([
                'user_id' => $current_game->user_id,
                'tower_id' => $current_game->id,
                'profit' => number_format($current_game->profit, 2),
            ]);

            $user = Auth::user();

            $user->profit = $user->profit + number_format($current_game->profit, 2);
            $user->count_drops++;
            $user->save();
        }

        $last_balance = 0;

        $last_bal = DB::table('tower_promo')->selectRaw('sum(last_balance) as last_balance')->where('user_id','=',Auth::user()->id)->get();

        foreach ($last_bal as $bal)
        {
            $last_balance = $bal->last_balance;
        }

        if ($last_balance > 0 && !Auth::user()->youtuber )
        {
            Helper::credit($profit, 1, 2, 'tower win from promo');
        }
        else if(Auth::user()->youtuber)
        {
            Helper::credit($profit, 1, 0, 'tower win youtube');
            $total_income = $this->income_youtube();
            $total_expense =$this->expense_youtube();
            $total_balance = $total_income - $total_expense;
            $real_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
           /* BalanceYoutuber::where('user_id',Auth::user()->id)->update([
                'income'=>(float)$total_income,
                'expense'=>(float)$total_expense,
                'balance'=>(float)$total_balance,
                'current_profit'=>(float)$real_company_percent,
            ]);*/
        }
        else{
            Helper::credit($profit, 1, 0, 'tower win');
        }
        $total_income = $this->income_youtube();
        $total_expense =$this->expense_youtube();
        $total_balance = $total_income - $total_expense;
        $real_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
      /*  BalanceYoutuber::where('user_id',Auth::user()->id)->update([
            'income'=>(float)$total_income,
            'expense'=>(float)$total_expense,
            'balance'=>(float)$total_balance,
            'current_profit'=>(float)$real_company_percent,
        ]);*/

        DB::commit();

        $this->live($current_game->id);
        $this->liveLeft($current_game->id);

        return response()->json([
            'success' => true,
            'message' => 'Profit is taken! ',
            'profit' => $profit
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMultiplier(Request $request)
    {
        $multiplier = TowerSetting::getMultiplier($request->input('level'));

        return response()->json([
            'success' => true,
            'multiplier' => $multiplier
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSeeds()
    {
        $prev_game = [];

        if($last_cell = TowerCell::getLastCell()){
            $prev_game = [
                'user_seed' => $last_cell->user_seed,
                'server_seed' => $last_cell->server_seed,
                'win_number' => $last_cell->win_number,
                'hash' => hash('sha256', $last_cell->user_seed.$last_cell->server_seed.$last_cell->win_number),
            ];
        }

        $next_user_seed = ProvablyFair::getUserSeed();
        $next_server_seed = ProvablyFair::getServerSeed();
        Session::put('user_seed.case_0.attempt_tower', $next_user_seed);
        Session::put('server_seed.case_0.attempt_tower', $next_server_seed);

        return response()->json([
            'success' => true,
            'prev_game' => $prev_game,
            'next_game' => [
                'user_seed' => $next_user_seed,
                'server_seed' => $next_server_seed,
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSeeds()
    {
        $win_number = mt_rand(1, 99999);
        if(!ProvablyFair::getHash(0, 'tower', $win_number)){
            return response()->json([
                'success' => false,
            ]);
        }

        if(!$last_cell = TowerCell::getLastCell()){
            return response()->json([
                'success' => false,
            ]);
        }

        $last_cell = TowerCell::find($last_cell->id);
        $last_cell->user_seed = Session::get('user_seed.case_0.attempt_tower');
        $last_cell->server_seed = Session::get('server_seed.case_0.attempt_tower');
        $last_cell->win_number = $win_number;
        $last_cell->save();

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * @param $tower_id
     */
    private function live($tower_id)
    {
        $liveTowerCount = LiveTower::count();
        if($liveTowerCount > 15){
            LiveTower::orderBy('created_at','asc')->take($liveTowerCount-15)->delete();
        }

        $LiveTower = new LiveTower();
        $LiveTower->tower_id = $tower_id;
        $LiveTower->save();

        $tower = Tower::find($tower_id);
        $tower->games = Tower::count();
        $tower->bets = Tower::sum('bet');
        $tower->avatar = $tower->user->avatar;
        $tower->username = $tower->user->username;
        $tower->user_id = $tower->user->id;

        event(new \App\Events\TowerGame($tower));
    }

    /**
     * @param $tower_id
     */
    private function liveLeft($tower_id)
    {
        $Tower = Tower::find($tower_id);
        $Tower->games = Tower::count();
        $Tower->bets = Tower::sum('bet');
        $Tower->avatar = $Tower->user->avatar;
        $Tower->username = $Tower->user->username;
        $Tower->game_type = 'tower';

        if($Tower->profit <= 0){
            return false;
        }

        $LastWinsCount = LastWin::count();
        if($LastWinsCount > 15){
            LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
        }

        $LastWin = new LastWin();
        $LastWin->game = 'tower';
        $LastWin->game_id = $tower_id;
        $LastWin->save();

        event(new \App\Events\Drop($Tower));
    }


}
