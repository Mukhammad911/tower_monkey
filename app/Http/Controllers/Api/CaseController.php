<?php

namespace App\Http\Controllers\Api;

use App\Models\Drop;
use App\Models\SteamCase;
use Auth;
use Illuminate\Support\Facades\Log;
use Lang;
use ProvablyFair;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

/**
 * Class CaseController
 * @package App\Http\Controllers\Api
 */
class CaseController extends Controller
{
	/**
	 * @param $status
	 * @param $message
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	private static function response($status, $message)
	{
		$res = [
			'message'=>Lang::has('messages.'.$message) ? trans('messages.'.$message) : $message,
		];

		return response()->json($res, $status);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function calculateHash(Request $request)
	{
		parse_str($request->input('form_data'), $data);
		return response()->json(hash('sha256', $data['user_seed'] .''. $data['server_seed'].''. + $data['win_number']), 200);
	}

	/**
	 * @param $case_id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getProducts($case_id)
	{
		$SteamCase = SteamCase::find($case_id);

		return response()->json($SteamCase->products, 200);
	}
}
