<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveTower;
use App\Models\Operation;
use App\Models\Product;
use App\Models\Tower;
use App\Models\TowerSetting;
use App\Models\Upgrade;
use App\Models\UpgradeSetting;
use Partnership;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Balance;
use App\Models\Card;
use App\Models\User;
use App\Models\Category;
use App\Models\SteamCase;
use App\Models\DropsMarket;
use App\Models\DotaDropsMarket;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Lang;
use App\Models\Drop;
use App\Models\Setting;
use App\Models\Currency;
use Helper;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\AvailableProduct;
use App\Models\DotaAvailableProduct;
use JavaScript;
use Config;
use ProvablyFair;

class MarketDotaController extends Controller
{
    public function getDota2skins(Request $request)
    {
        if((Auth::check()) && ($request->wantsJson())) {
            $cases = DB::select("SELECT * FROM `dota_available_products` where short_description !='chip'");
            return $cases;
        }
    }

    public function getDotaProfileModal()
    {
        $user_id = Auth::user()->id;
        $count = DB::table('dota_drops_market')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->count();

        return response()->json($count);
    }



    public function getDotaProfile()
    {
        $user_id = Auth::user()->id;
        $profile_product = DB::table('dota_drops_market')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->get();
        return response()->json($profile_product);
    }
    function getDota2heroes(){
        if(Auth::check()){
            $heroes = DB::table('dota_available_products')->select('hero')->groupBy('hero')->get();
            return response()->json($heroes);
        }
    }
}
