<?php

namespace App\Http\Controllers\Api;

use App\Models\CounterGames;
use App\Models\PubgCases\PubgCase;
use App\Models\PubgCases\PubgCaseAvailableProduct;
use App\Models\PubgCases\PubgCaseDrops;
use App\Models\PubgCases\PubgCaseSettings;
use App\Models\PubgCases\WinPubgCase;
use App\Models\PubgProduct;
use App\Models\LastWin;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use JavaScript;
use DB;
use ProvablyFair;
use Helper;

class PubgCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $token = Auth::check() ? Redis::get('token_' . Auth::user()->steamid) : '';
        $user_id = Auth::check() ? Auth::user()->id : 0;
        JavaScript::put([
            'BASE_URL' => url('/'),
            'LIVE_DROP_PORT' => Config::get('broadcasting.live_drop_port'),
            'token' => $token,
            'user_id' => $user_id,

        ]);

        self::$lang = $request->input('lang');
    }

    public function index()
    {

    }

    public function getPubgCase(Request $request, $id)
    {

        if ($request->is('pubg-crate/*')) {
            $active_menu = 'pubg-crate';
        }

        $case = PubgCase::find($id);
        if(!$case){
            abort(404);
        }
        $weapon = [];

        $products = $case->products()
            ->orderBy(DB::raw('RAND()'))
            ->take(25)
            ->get();
        if(!sizeof($products)){
            Log:info('True epth keas');
            //return view(self::getView('site.csgo-case-unavailable'));
            return view(self::getView('site.pubg_case_index'), [
                'page_case' => true,
                'case' => null,
                'win_numbers' => '',
                'user_seed' => '',
                'server_seed' =>'',
                'previous_game' => '',
                'highest_prize' => ''
            ]);
        }
        $highest_prize = $case->products()
                ->select('price')
                ->orderBy('price', 'desc')
                ->first()->price / 100;

        foreach ($products as $w) {
            $name = (Lang::has('weapon.' . $w['name'])) ? trans('weapon.' . $w['name']) : $w['name'];
            $description = (Lang::has('weapon.' . $w['short_description'])) ? trans('weapon.' . $w['short_description']) : $w['short_description'];
            $weapon[] = [
                'name' => $name,
                'short_description' => $description,
                'class' => $w['class'],
                'image' => ($w['image_source'] == 'url') ? $w['image'] : asset('images/pubg_images/') . "/" . $w['image']
            ];
        }

        if (Auth::check()) {
            $user_seed = ProvablyFair::getUserSeed($case->id);
            $server_seed = ProvablyFair::getServerSeed();
            Session::put('user_seed.case_' . $case->id . '.attempt_1', $user_seed);
            Session::put('server_seed.case_' . $case->id . '.attempt_1', $server_seed);

            $previous_game = PubgCaseDrops::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
            if($previous_game){
                $previous_game->hash = hash('sha256', $previous_game->user_seed.$previous_game->server_seed.$previous_game->win_number);
            }
        }

        $win_numbers = ProvablyFair::getWinNumbers($id, 'pubg');

        return view(self::getView('site.pubg_case_index'), [
            'page_case' => true,
            'active_menu' => $active_menu,
            'case' => $case,
            'win_numbers' => $win_numbers,
            'user_seed' => @$user_seed,
            'server_seed' => @$server_seed,
            'previous_game' => @$previous_game,
            'highest_prize' => $highest_prize
        ]);
    }

    public function play(Request $request)
    {
        $case_id = $request->input('case_id');
        $last_balance = $request->input('last_balance');
        $case = PubgCase::find($case_id);
        $user_id = Auth::user()->id;

        $res = [
            'success'=>true,
            'win_products' => [],
            'user_id' => $user_id,
            'case_price' => $case->priceObj->price,
            'message' => '',
            'count'=>0
        ];

        for($i = 1; $i <= $request->input('attempts'); $i++){
            $res['win_products'][$i] = $this->game($request, $case, $i,$last_balance);

            if($res['win_products'][$i]['success']==false){

            }
            else{
                $res['count']++;
            }

        }

        return response()->json($res);

    }

    private function game($request, $case, $attempt, $last_balance)
    {
        $ret = [
            'success'=>true,
            'win'=>null,
        ];

        $user = Auth::user();

        $product = $this->getWinProduct($case, $user,$last_balance);

        if(!$product){
            $this->moneyBack($user, $case->priceObj->price);

            $ret['notify']  = true;
            $ret['success'] = false;
            $ret['message'] = trans('messages.unavailable');
            return $ret;
        }

        $win_number = ProvablyFair::getWinNumber($case->id, $product, 'pubg');
        $hash = ProvablyFair::getHash($case->id, $attempt, $win_number);

        try{
            DB::beginTransaction();

            $drop = new PubgCaseDrops();
            $drop->user_id              = $user->id;
            $drop->case_id              = $case->id;
            $drop->product_id           = $product->product_id;
            $drop->price                = $product->price;

            $drop->name                 = $product->name;
            $drop->short_description    = $product->short_description;
            $drop->market_name          = $product->market_name;
            $drop->image_hash           = $product->image_hash;
            $drop->case_image           = (!empty($case->class)) ? 'images/case/'.$case->class.'.png' : 'images/cases/thumbnails/'.$case->image;
            $drop->quality              = $product->quality;
            $drop->class                = $product->class;
            $drop->stattrak             = $product->stattrak;
            $drop->assetid              = $product->assetid;
            $drop->classid              = $product->classid;
            $drop->instanceid           = $product->instanceid;
            $drop->bot_id               = $product->bot_id;
            $drop->user_seed            = empty( Session::get('user_seed.case_'.$case->id.'.attempt_'.$attempt) ) ? '' : Session::get('user_seed.case_'.$case->id.'.attempt_'.$attempt);
            $drop->server_seed          = empty( Session::get('server_seed.case_'.$case->id.'.attempt_'.$attempt) ) ? '' : Session::get('server_seed.case_'.$case->id.'.attempt_'.$attempt);
            $drop->win_number           = $win_number;
            $drop->currency_id          = Session::get('currency_id');

            if(!$drop->save()){
                return response()->json(['success'=>false,'error'=>'server_error']);
            }

            $ret['drop_id'] = $drop->id;

            //Profit в копейках
            $user->profit = $user->profit + ($drop->price/100/60 - $case->PubgPriceDefault->price/60);
            $user->count_drops++;
            $user->save();
            $product->delete();
            $this->complete($drop);

            if ($user->youtuber)
            {
                Helper::operation($drop->price/100/round(Helper::setting('rub_usd_sell'), 2), 1, 'pubg case win youtube', Auth::user()->id);
            }
            else
            {
                Helper::operation($drop->price/100/round(Helper::setting('rub_usd_sell'), 2), 1, 'pubg case win',Auth::user()->id);
            }

            DB::commit();
        } catch(\Exception $e){
            Log::error($e->getMessage());
            DB::rollback();

            $this->moneyBack($user, $case->priceObj->price);

            $ret['notify']  = true;
            $ret['success'] = false;
            return response()->json($ret);
        }


        $drop->price = Helper::currentPrice($drop->price);
        $product     = PubgProduct::find($product->product_id);
        $drop->image = Helper::casePubgImageUrl($product);

        $drop->win_number = $win_number;
        $drop->hash = $hash;

        if(Lang::has('weapon.'.$drop->name)){
            $drop->name = trans('weapon.'.$drop->name);
        }
        if(Lang::has('weapon.'.$drop->short_description)){
            $drop->short_description = trans('weapon.'.$drop->short_description);
        }
        $drop['notify']  = true;
        $drop['success'] = true;
        return $drop;

    }

    private function getWinProduct($case, $user, $last_balance)
    {
        $win = new WinPubgCase($case, $last_balance);
        $product = $win->get();

        Log::info('Product winned');
        Log::info($product);

        return $product;
    }


    /**
     * @param $user
     * @param $amount
     */
    private function moneyBack($user, $amount)
    {
        $user->balance()
            ->where('currency_id', 1)
            ->where('type', 0)
            ->update([
                'summ' => DB::raw("summ + $amount")
            ]);

        if ($user->youtuber)
        {
            Helper::operation($amount, 1, 'money back pubg case youtube');
        }
        else
        {
            Helper::operation($amount, 1, 'money back pubg case');
        }
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    /* public function getWinNumbers(Request $request)
     {
         $win_numbers = ProvablyFair::getWinNumbers($request->input('case_id'));

         return response()->json([
             'success' => true,
             'data' => $win_numbers
         ], 200);
     }*/

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSeeds(Request $request)
    {
        $res = [
            'next_game',
            'previous_game',
        ];
        for ($i = 1; $i <= $request->input('count');  $i++ ) {
            $res['next_game'][$i]['user_seed'] = ProvablyFair::getUserSeed($request->input('case_id'));
            $res['next_game'][$i]['server_seed'] = ProvablyFair::getServerSeed();

            Session::put('user_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['user_seed']);
            Session::put('server_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['server_seed']);
        }

        $res['previous_game'] = Drop::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        if($res['previous_game']){
            $res['previous_game']->hash = hash('sha256', $res['previous_game']->user_seed + $res['previous_game']->server_seed + $res['previous_game']->win_number);
        }

        return response()->json($res, 200);
    }

    public function sellItem($id)
    {
        if(Auth::check()){
            $ret = ['success' => true, 'notify' => true];
            try {
                $drop = PubgCaseDrops::find($id);

                if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                    $ret['success'] = false;

                    return response()->json($ret);
                }

                $drop->sell();

                $ret['message'] = trans('messages.sold');

            } catch (\Exception $e) {
                DB::rollback();
                $ret['success'] = false;
            }

            return response()->json($ret);
        }

        return response('Unauthorized.', 401);
    }

    public function sellAllItems()
    {
        $res_array = array();

        if(Auth::check()){
            $ret = ['success' => true, 'notify' => true, 'result_ids' => array()];
            try {
                $drops = PubgCaseDrops::where('status', 'pending')->where('user_id', Auth::user()->id)->get();

                foreach ($drops as $drop)
                {
                    if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                        $ret['success'] = false;

                        return response()->json($ret);
                    }

                    array_push($res_array, $drop->id);
                    $ret['result_ids'] = $res_array;

                    $drop->sell();
                }

                if (!sizeof($res_array))
                {
                    $ret['success'] = false;

                    return response()->json($ret);
                }

                $ret['message'] = trans('messages.sold');
            } catch (\Exception $e) {
                DB::rollback();
                $ret['success'] = false;
            }

            return response()->json($ret);
        }

        return response('Unauthorized.', 401);
    }

    public function myPubgCaseDrops()
    {
        if(Auth::check())
        {
            $user_id = Auth::user()->id;
            $profile_product = DB::table('pubg_case_drops_market')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold" and  status !="pending"')->get();
            return response()->json($profile_product);
        }

        return response('Unauthorized.', 401);
    }

    private function complete(PubgCaseDrops $drop){
        $LastWinsCount = LastWin::count();
        if($LastWinsCount > 15){
            LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
        }

        $this->updateCounters('pubg-case');

        $LastWin = new LastWin();
        $LastWin->game = 'pubg-case-roulette';
        $LastWin->game_id = $drop->id;
        $LastWin->save();
        $drop->avatar = Auth::user()->avatar;
        $drop->username = Auth::user()->username;
        $drop->case_name =  PubgCase::select('name')->where('id',$drop->case_id)->first();
        $drop->ex_price =  Helper::exchange($drop->price,1);
        $drop->load('product');

        /*  $drop = $this;
          $drop->id = 'csgo-case-roulette'.$LastWin->id;
          $drop->game_type = 'csgo-case-roulette';
          $drop->timeout = 6000;
          $drop->case_price = $this->steamCase->priceDefault->price;
          $drop->case_image = asset('images/cases/thumbnails').'/'.$this->steamCase->image;
          $drop->product_image = Helper::caseImageUrl($this->product);
          $drop->user_id = $this->user->id;
          $drop->username = $this->user->username;
          $drop->avatar = $this->user->avatar;*/

        event(new \App\Events\PubgCaseDrop($drop));

        /*$liveDropCount = LiveDrop::count();
        if($liveDropCount > 25){
            LiveDrop::orderBy('created_at','asc')->take($liveDropCount-25)->delete();
        }

        $live = new LiveDrop();
        $live->user_id = $drop->user_id;
        $live->case_id = $drop->case_id;
        $live->product_id = $drop->product_id;
        $live->stattrak = $drop->stattrak;
        $live->save();

        $data = [
            'id'         => $live->id,
            'case_class' => $live->steamCase->class,
            'case_image' => asset('images/cases/thumbnails').'/'.$live->steamCase->image,
            'w_class'    => $live->product->class,
            'w_image'    => Helper::caseImageUrl($live->product),
            'w_name'     => $live->product->name,
            'w_descr'    => $live->product->short_description,
            'stattrak'   => (bool)$live->stattrak,
            'steam_id'   => $live->user->steamid,
            'username'   => $live->user->username,
            'avatar'     => $live->user->avatar
        ];

        event(new EventDrop($data));*/
        /*$job = (new App\Jobs\LiveDrop($data))->delay(4);
        $this->dispatch($job);*/
    }

    private function updateCounters($game_type){
        $counter = CounterGames::where('games', $game_type)->first();
        $counter->count++;
        $counter->save();
    }
}
