<?php

namespace App\Http\Controllers\Api;

use App\Models\Balance;
use Auth;
use DB;
use Partnership;
use Helper;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;

class UserController extends Controller
{
	/**
	 * @param $status
	 * @param $message
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	private static function response($status, $message)
	{
		$res = [
			'message'=>Lang::has('messages.'.$message) ? trans('messages.'.$message) : $message,
		];

		return response()->json($res, $status);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateTradeUrl(Request $request)
	{
		$this->validate($request, [
			'trade_url' => 'required|unique:users'
		]);

		$user = User::find($this->user->id);
		$user->trade_url = $request->input('trade_url');
		if(!$user->save()){
			return self::response(422, 'Updating error. Please, try again later.');
		}

		return self::response(200, 'Your trade URL has been successfully saved!');
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updatePromoCode(Request $request)
	{
		$this->validate($request, [
			'promo_code' => 'required|alpha|max:20|unique:users'
		]);

		$user = User::find($this->user->id);
		$user->promo_code = $request->input('promo_code');
		if(!$user->save()){
			return self::response(422, 'Updating error. Please, try again later.');
		}

		return self::response(200, 'Your promo-code has been successfully saved.');
	}

	/**
	 * @param $promo_code
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function usePromoCode(Request $request, $promo_code)
	{
	    if ($request->ajax())
	    {
	        $user = Auth::user();
            if ($user->bonus_used == 1) {
                return self::response(422, 'You are have used a promo code already.');
            }
            if (!$refer = User::where('id', $promo_code)
                ->orWhere('promo_code', $promo_code)
                ->first()
            ) {
                return self::response(422, 'Wrong promo code');
            }
            if ($refer->id == $user->id) {
                return self::response(422, 'You can not use your own code!');
            }

            DB::beginTransaction();

            $user->bonus_used = 1;
            if (!$user->save()) {
                DB::rollback();
                return self::response(422, 'Server error with saving promo code.');
            }

            // give a promo bonus
            $Balance = Balance::where('user_id', $user->id)
                ->where('currency_id', 1)
                ->where('type', 2)
                ->sharedLock()
                ->first();
            $Balance->summ += $refer->partnerLevel->bonus_amount;
            Helper::operation($refer->partnerLevel->bonus_amount, 1, 'promo bonus');
            if (!$Balance->save()) {
                DB::rollback();
                Log::error('Error saving of a new Balance entity within newAffiliate()');
                return self::response(422, 'Server error with saving promo code.');
            }

            DB::commit();
            return response()->json([
                'message' => 'Promo-code has been successfully activated. You have received <strong>$' . $refer->partnerLevel->bonus_amount . '</strong> bonus.',
                'amount' => $refer->partnerLevel->bonus_amount,
            ], 200);
        }
	}


}
