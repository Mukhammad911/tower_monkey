<?php

namespace App\Http\Controllers\Api;

use App;
use Lang;
use Config;
use ProvablyFair;
use DB;
use Session;
use Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SteamCase;
use App\Models\Drop;
use App\Models\RouletteScenarioAlgorithm;
use App\Models\RouletteAlgorithm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/**
 * Class RouletteController
 * @package App\Http\Controllers\Api
 */
class RouletteController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function play(Request $request)
	{
		$case_id = $request->input('case_id');
		$last_balance = $request->input('last_balance');
		$case = SteamCase::find($case_id);

		$res = [
			'success'=>true,
			'win_products' => [],
			'case_price' => $case->priceObj->price
		];

		for($i = 1; $i <= $request->input('attempts'); $i++){
			$res['win_products'][$i] = $this->game($request, $case, $i,$last_balance);
		}

		return response()->json($res);
	}

	/**
	 * @param $request
	 * @param $case
	 * @param $attempt
	 *
	 * @return array
	 */
	private function game($request, $case, $attempt,$last_balance)
	{
		$ret = [
			'success'=>true,
			'win'=>null,
		];

		$user = Auth::user();

		$product = $this->getWinProduct($case, $user,$last_balance);
		if(!$product){
			$this->moneyBack($user, $case->priceObj->price);

			$ret['notify']  = true;
			$ret['success'] = false;
			$ret['message'] = trans('messages.unavailable');
			return $ret;
		}

		$win_number = ProvablyFair::getWinNumber($case->id, $product);
		$hash = ProvablyFair::getHash($case->id, $attempt, $win_number);

		try{
			DB::beginTransaction();

			$drop = new Drop();
			$drop->user_id              = $user->id;
			$drop->case_id              = $case->id;
			$drop->product_id           = $product->id;
			$drop->price                = ($product->price / 100);
			$drop->stattrak             = 0;
			$drop->name                 = $product->name;
			$drop->short_description    = $product->short_description;
			$drop->case_image           = (!empty($case->class)) ? 'images/case/'.$case->class.'.png' : 'images/cases/thumbnails/'.$case->image;
			$drop->class                = $product->class;
			$drop->currency_id          = empty( Session::get('currency_id') ) ? 1 : Session::get('currency_id');
			$drop->user_seed            = empty( Session::get('user_seed.case_'.$case->id.'.attempt_'.$attempt) ) ? '' : Session::get('user_seed.case_'.$case->id.'.attempt_'.$attempt);
			$drop->server_seed          = empty( Session::get('server_seed.case_'.$case->id.'.attempt_'.$attempt) ) ? '' : Session::get('server_seed.case_'.$case->id.'.attempt_'.$attempt);
			$drop->win_number           = $win_number;
			if(!$drop->save()){
				return ['success'=>false, 'error'=>'server_error'];
			}

			App\Models\Game::create([
				'user_id' => $user->id,
				'roulette_id' => $drop->id,
			]);

			//Profit в копейках
			$user->profit = $user->profit + ($drop->price - $case->priceDefault->price*100);
			$user->count_drops++;
			$user->save();

			$drop->complete();

			DB::commit();
		} catch(\Exception $e){
			Log::error($e->getMessage());
			DB::rollback();

			$this->moneyBack($user, $case->priceObj->price);

			$ret['notify']  = true;
			$ret['success'] = false;
			return $ret;
		}

		//$drop->price = Helper::currentPrice($drop->price);
		$drop->image = Helper::caseImageUrl($product);
		$drop->win_number = $win_number;
		$drop->hash = $hash;

		if(Lang::has('weapon.'.$drop->name)){
			$drop->name = trans('weapon.'.$drop->name);
		}
		$drop->short_description = trans('weapon.'.$drop->short_description);

		if ($last_balance > 0)
        {
            Helper::credit($drop->price, 1, 2, 'roulette win from promo');
        }
        else
        {
            Helper::credit($drop->price, 1, 0, 'roulette win');
        }



		$ret['win'] = $drop;

		return $ret;
	}

	/**
	 * @param $case
	 * @param $user
	 *
	 * @return mixed
	 */
	private function getWinProduct($case, $user,$last_balance)
	{
		$scenario = App\Models\RouletteScenario::query()
		                    ->where('user_id', $user->id)
		                    ->where('case_id', $case->id)
		                    ->where('status', 'enabled')
		                    ->first();

		if(Config::get('app.promotion_mode') && $scenario){
			if(is_null(Redis::get('roulette-scenario-'.$case->id.'-'.$user->id))){
				Redis::set('roulette-scenario-'.$case->id.'-'.$user->id, 0);
			}
			$current_attempt = Redis::get('roulette-scenario-'.$case->id.'-'.$user->id);

			if($scenario->attempt == $current_attempt + 1){
				$win = new RouletteScenarioAlgorithm($case, $user, $scenario);
			} else{
				Redis::set('roulette-scenario-'.$case->id.'-'.$user->id, $current_attempt + 1 );
				$win = new RouletteAlgorithm($case,$last_balance);
			}
		} else{
			$win = new RouletteAlgorithm($case,$last_balance);
		}

		return $win->get();
	}

	/**
	 * @param $user
	 * @param $amount
	 */
	private function moneyBack($user, $amount)
	{
		$user->balance()
		     ->where('currency_id', 1)
		     ->where('type', 0)
		     ->update([
			     'summ' => DB::raw("summ + $amount")
		     ]);

		Helper::operation($amount, Session::get('currency_id'), 'money back');
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
    public function getWinNumbers(Request $request)
    {
        $win_numbers = ProvablyFair::getWinNumbers($request->input('case_id'), $request->input('case_type'));

        return response()->json([
            'success' => true,
            'data' => $win_numbers
        ], 200);
    }

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSeeds(Request $request)
	{
		$res = [
			'next_game',
			'previous_game',
		];

        $case_type = $request->input('case_type');

        if ($case_type == 'csgo-case')
        {
            for ($i = 1; $i <= $request->input('count');  $i++ ) {
                $res['next_game'][$i]['user_seed'] = ProvablyFair::getUserSeed($request->input('case_id'));
                $res['next_game'][$i]['server_seed'] = ProvablyFair::getServerSeed();

                Session::put('user_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['user_seed']);
                Session::put('server_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['server_seed']);
            }

            $res['previous_game'] = App\Models\CsgoCaseDrops::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
            if($res['previous_game']){
                $res['previous_game']->hash = hash('sha256', $res['previous_game']->user_seed.$res['previous_game']->server_seed.$res['previous_game']->win_number);
            }
        }
		elseif ($case_type == 'pubg-case')
        {
            for ($i = 1; $i <= $request->input('count');  $i++ ) {
                $res['next_game'][$i]['user_seed'] = ProvablyFair::getUserSeed($request->input('case_id'));
                $res['next_game'][$i]['server_seed'] = ProvablyFair::getServerSeed();

                Session::put('user_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['user_seed']);
                Session::put('server_seed.case_'.$request->input('case_id').'.attempt_'.$i, $res['next_game'][$i]['server_seed']);
            }

            $res['previous_game'] = App\Models\PubgCases\PubgCaseDrops::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
            if($res['previous_game']){
                $res['previous_game']->hash = hash('sha256', $res['previous_game']->user_seed.$res['previous_game']->server_seed.$res['previous_game']->win_number);
            }
        }

		return response()->json($res, 200);
	}
}
