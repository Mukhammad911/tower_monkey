<?php

namespace App\Http\Controllers\Api;

use App\Models\Game;
use App\Models\LastWin;
use App\Models\LiveUpgrade;
use App\Models\Product;
use App\Models\Upgrade;
use App\Models\UpgradeAlgorithm;
use App\Models\UpgradeScenario;
use App\Models\UpgradeScenarioAlgorithm;
use App\Models\UpgradeSetting;
use Illuminate\Http\Request;

use Auth;
use Helper;
use Config;
use Illuminate\Support\Facades\Redis;
use ProvablyFair;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use App\Models\Balance;
use Illuminate\Support\Facades\Session;

/**
 * Class UpgradeController
 * @package App\Http\Controllers\Api
 */
class UpgradeController extends Controller
{
	/**
	 * @param $status
	 * @param $data
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	private static function response($status, $data)
	{
		if(is_array($data)){
			$res = [];
			foreach($data as $key => $value){
				$res[$key] = $value;
			}
		}else{
			$res = [
				'message' => Lang::has('messages.'.$data) ? trans('messages.'.$data) : $data,
			];
		}

		return response()->json($res, $status);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function upgrade(Request $request)
	{
		$this->validate($request, [
			'bet' => 'required|numeric|min:0.5',
			'profit' => 'required|numeric|min:0.5',
		]);
		$bet = $request->input('bet');
		$profit = $request->input('profit');

		$user = Auth::user();
		$user_balance = Helper::get('balance')->summ;

		if($bet > $user_balance){
			return $this->response(422, 'Not enough money');
		}

		if($bet > $profit){
			return $this->response(422, 'Profit should be more that your bet!');
		}

        $bonus_balance = Balance::where('user_id', Auth::user()->id)
            ->where('type', 2)
            ->where('currency_id', 1)
            ->first();

		if(! Helper::debit($bet, 1, 'upgrade bet') ){
			return $this->response(422, 'Not enough money');
		}

		$multiplier = $profit / $bet;
		$win_number = ProvablyFair::getWinNumber(0, 0);
		$hash = ProvablyFair::getHash(0, 1, 0);
		$win = $this->getGameResult($user, $bet, $profit,$bonus_balance->summ);

		$Upgrade = new Upgrade();
		$Upgrade->user_id = $user->id;
		$Upgrade->bet = $bet;
		$Upgrade->profit = $profit;
		$Upgrade->multiplier = $multiplier;
		$Upgrade->win = $win;
		$Upgrade->demo = 0;
		$Upgrade->user_seed = empty( Session::get('user_seed.case_0.attempt_upgrade') ) ? '' : Session::get('user_seed.case_0.attempt_upgrade');
		$Upgrade->server_seed = empty( Session::get('server_seed.case_0.attempt_upgrade') ) ? '' : Session::get('server_seed.case_0.attempt_upgrade');
		$Upgrade->win_number = mt_rand(1, 99999);
		$Upgrade->save();


		if($win == TRUE){
			$this->live($Upgrade->id);
			Game::create([
				'user_id' => $user->id,
				'upgrade_id' => $Upgrade->id,
			]);
		}

		if($win == TRUE){
			$win_amount = ($bet * $multiplier);

			if ($bonus_balance->summ > 0)
            {
                Helper::credit($win_amount, 1, 2, 'upgrade win from promo');
            }
            else
            {
                Helper::credit($win_amount, 1, 0, 'upgrade win');
            }



			return $this->response(200, [
				'message' => 'You won!',
				'win' => true,
				'hash' => $hash,
				'win_number' => $win_number,
			]);
		}else{
			return $this->response(200, [
				'message' => 'You lose...',
				'win' => false,
				'hash' => $hash,
				'win_number' => $win_number,
			]);
		}
	}

	/**
	 * @param $user
	 * @param $bet
	 * @param $profit
	 *
	 * @return bool
	 */
	private function getGameResult($user, $bet, $profit,$last_balance)
	{
		$scenarios = UpgradeScenario::query()
			->where('user_id', $user->id)
			->where('status', 'enabled')
			->get();

		if(Config::get('app.promotion_mode') && $scenarios){
			if(is_null(Redis::get('upgrade-scenario-'.$user->id))){
				Redis::set('upgrade-scenario-'.$user->id, 0);
			}
			$win = new UpgradeScenarioAlgorithm($user, $bet, $profit, $scenarios,$last_balance);
			$result = $win->result();
		} else{
			$win = new UpgradeAlgorithm($bet, $profit,$last_balance);
			$result = $win->result();
		}

		return $result;
	}

	/**
	 * @param Request $request
	 *
	 * @return float
	 */
	public function getCustomChance(Request $request)
	{
		$this->validate($request, [
			'bet' => 'required|numeric',
			'profit' => 'required|numeric',
		]);

		$company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
		$multiplier = $request->input('profit') / $request->input('bet');

		// base chance
		$chance = $request->input('bet') / $request->input('profit');

		// final chance
		$chance = $chance  * (100 - $company_percent);

		return $this->response(200, [
			'success' => true,
			'chance' => $chance
		]);
	}

	/**
	 * @return mixed
	 */
	public function getHash()
	{
		$hash = ProvablyFair::getHash(0, 1, 0);

		return $hash;
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSeeds()
	{
		$prev_game = [];

		if($prev_game = Upgrade::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first()){
			$prev_game->hash = hash('sha256', $prev_game->user_seed + $prev_game->server_seed + $prev_game->win_number);
		}

		$next_user_seed = ProvablyFair::getUserSeed();
		$next_server_seed = ProvablyFair::getServerSeed();
		Session::put('user_seed.case_0.attempt_upgrade', $next_user_seed);
		Session::put('server_seed.case_0.attempt_upgrade', $next_server_seed);

		return response()->json([
			'success' => true,
			'prev_game' => $prev_game,
			'next_game' => [
				'user_seed' => $next_user_seed,
				'server_seed' => $next_server_seed,
			],
		]);
	}

	/**
	 * @param $upgrade_id
	 */
	private function live($upgrade_id)
	{
		$LastWinsCount = LastWin::count();
		if($LastWinsCount > 15){
			LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
		}

		$LastWin = new LastWin();
		$LastWin->game = 'upgrade';
		$LastWin->game_id = $upgrade_id;
		$LastWin->save();

		$Upgrade = Upgrade::find($upgrade_id);
		$Upgrade->game_type = 'upgrade';
		$Upgrade->timeout = 3000;
		$Upgrade->user_id = $Upgrade->user->id;
		$Upgrade->avatar = $Upgrade->user->avatar;
		$Upgrade->username = $Upgrade->user->username;

		event(new \App\Events\Drop($Upgrade));
	}
}
