<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveTower;
use App\Models\Operation;
use App\Models\Product;
use App\Models\Tower;
use App\Models\TowerSetting;
use App\Models\Upgrade;
use App\Models\UpgradeSetting;
use Partnership;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Balance;
use App\Models\Card;
use App\Models\User;
use App\Models\Category;
use App\Models\SteamCase;
use App\Models\DropsMarket;
use App\Models\DotaDropsMarket;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Lang;
use App\Models\Drop;
use App\Models\Setting;
use App\Models\Currency;
use Helper;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\AvailableProduct;
use App\Models\DotaAvailableProduct;
use JavaScript;
use Config;
use ProvablyFair;

class MarketCsgoController extends Controller
{
    public function getCsgoskins(Request $request)
    {
        if ((Auth::check()) && ($request->wantsJson())) {
            $cases = DB::select("SELECT * FROM `available_products` where short_description !='chip'");
            return $cases;
        }
    }

    function getCsgoAllClasses()
    {
        if (Auth::check()) {
            $classes = DB::table('available_products')->where('short_description', '!=', 'chip')->select('class')->groupBy('class')->get();
            return response()->json($classes);
        }
    }

    function getCsgoAllQuality()
    {
        if (Auth::check()) {
            $qualities = DB::table('available_products')->where('short_description', '!=', 'chip')->select('quality')->groupBy('quality')->get();
            return response()->json($qualities);
        }
    }

    public function getBalance()
    {
        $cases = DB::select("SELECT * FROM `available_products` where short_description !='chip'");
        return $cases;
    }

    public function getCsgoProfileModal()
    {
        $user_id = Auth::user()->id;
        $count = DB::table('drops_market')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->count();

        return response()->json($count);
    }

    public function getCsgoProfile()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $profile_product = DB::table('drops_market')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->get();
            return response()->json($profile_product);
        }
    }

    public function getBotId()
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $bot_id = DB::table('drops_market')->selectRaw('distinct(bot_id) as bot_id, offer_id,status')->where('user_id', '=', $user_id)->whereRaw('status != "accepted" and  status !="sold"')->get();
            return response()->json($bot_id);
        }
    }

    public function getBalanceWithdrawable()
    {

        if (Auth::check()) {
            $balance = Helper::get('balance')->summ;
            return response()->json($balance);
        }

    }

    public function getOfferId()
    {
        $ret = ['success' => true];
        if (Auth::check()) {
            $data = DropsMarket::selectRaw('distinct(offer_id) as offer_id')->where('status', '=', 'offered')->where('user_id', '=', Auth::user()->id)->get();
            return response()->json($data);

        } else {
            $ret['success'] = false;
            return response()->json($ret);
        }
    }

    public function getActiveTradeOffer()
    {
        if (Auth::check()) {
            $data = DropsMarket::selectRaw('count(offer_id) as offer_id')->where('status', '=', 'offered')->where('user_id', '=', Auth::user()->id)->get();
            return response()->json($data);

        } else {
        }
    }

    public function getAdminMarket()
    {
        $csgo_market = '';
        $dota_market = '';
        $pubg_market = '';
        $data = DB::table('markets_access')->get();
        foreach ($data as $d) {
            if ($d->name == 'CSGO') {
                $csgo_market = $d->status;
            }
            if ($d->name == 'DOTA2') {
                $dota_market = $d->status;
            }
            if ($d->name == 'PUBG') {
                $pubg_market = $d->status;
            }
        }
        return view('admin.markets.index', ['csgo_market' => $csgo_market, 'dota_market' => $dota_market, 'pubg_market' => $pubg_market]);
    }

    public function setDisableStatusMarket(Request $request)
    {
        if (Auth::check()) {
            $result = ['success' => false];
            $market = $request->input('market');
            $status = $request->input('status');
            if ($market == 'csgo') {
                DB::table('markets_access')->where('name', '=', 'CSGO')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }
            else if ($market == 'dota') {
                DB::table('markets_access')->where('name', '=', 'DOTA2')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }
            else if ($market == 'pubg') {
                DB::table('markets_access')->where('name', '=', 'PUBG')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }

        }
    }
    public function setEnableStatusMarket(Request $request)
    {
        if (Auth::check()) {
            $result = ['success' => false];
            $market = $request->input('market');
            $status = $request->input('status');
            if ($market == 'csgo') {
                DB::table('markets_access')->where('name', '=', 'CSGO')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }
            else if ($market == 'dota') {
                DB::table('markets_access')->where('name', '=', 'DOTA2')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }
            else if ($market == 'pubg') {
                DB::table('markets_access')->where('name', '=', 'PUBG')->update([
                    'status' => (int)$status
                ]);
                $result = ['success' => true];
                return response()->json($result);
            }
        }
    }

}
