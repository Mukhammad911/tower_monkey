<?php

namespace App\Http\Controllers\Api;

use App\Models\CounterGames;
use App\Models\LastWin;
use App\Models\User;
use DaveJamesMiller\Breadcrumbs\Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Helper;
use App\Models\Balance;
use App\Models\CoinFlip;
use DB;
use Illuminate\Support\Facades\Log;

class CoinFlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkAuth(Request $request)
    {
        if (!$request->ajax()) {
            return response('Forbidden', 403);
        }
        if (Auth::check()) {
            $response = ['success' => true, 'message' => null];
            return response()->json($response);
        } else {
            $response = ['success' => false, 'message' => 'Please sign in to start playing'];
            return response()->json($response);
        }
    }

    public function checkBalance(Request $request)
    {
        if (!$request->ajax()) {
            return response('Forbidden', 403);
        }

        if (Auth::check()) {
            $balance = Balance::
            where('user_id', Auth::user()->id)
                ->where('type', 0)
                ->where('currency_id', 1)
                ->first();
            if ($balance->summ > 0) {
                $response = ['success' => true, 'message' => null, 'data' => $balance->summ];
                return response()->json($response);
            } else {
                $response = ['success' => false, 'message' => 'Not enough money'];
                return response()->json($response);
            }
        } else {
            $response = ['success' => false, 'message' => 'Error'];
            return response()->json($response);
        }
    }

    public function startGame(Request $request)
    {
        if (!$request->ajax()) {
            return response('Forbidden', 403);
        }

        $this->validate($request, [
            'bet' => 'required|numeric|between:0.1,5000',
            'mode' => 'required',
        ]);

        if (Auth::check()) {
            $mode = $request->input('mode');
            $bet = $request->input('bet');
            $balance = Balance::
            where('user_id', Auth::user()->id)
                ->where('type', 0)
                ->where('currency_id', 1)
                ->first();

            if ($bet <= 0) {
                $response = ['success' => false, 'message' => 'Bet must be more 0'];
                return response()->json($response);
            }

            if ($balance->summ < $bet) {
                $response = ['success' => false, 'message' => 'Not enough money'];
                return response()->json($response);
            }

            if (($balance->summ - $bet) < 0) {
                $response = ['success' => false, 'message' => 'Not enough money'];
                return response()->json($response);
            }

            if (!Helper::debit($bet, 1)) {
                $response = ['success' => false, 'message' => 'Not enough money'];
                return response()->json($response);
            }

            DB::beginTransaction();
            try {
                if ($mode == 'roll') {
                    $newGame = new CoinFlip();
                    $newGame->roll_user_id = (int)Auth::user()->id;
                    $newGame->bet = (float)$bet;
                    $newGame->created_at = \Carbon\Carbon::now();
                    $newGame->updated_at = \Carbon\Carbon::now();
                    $newGame->save();
                    $event_data = [
                        'game_id' => $newGame->id,
                        'user_id' => Auth::user()->id,
                        'avatar' => Auth::user()->avatar,
                        'user_name' => Auth::user()->username,
                        'bet' => $bet,
                        'mode' => $mode,
                    ];

                } else {
                    $newGame = new CoinFlip();
                    $newGame->csgo_user_id = (int)Auth::user()->id;
                    $newGame->bet = (float)$bet;
                    $newGame->created_at = \Carbon\Carbon::now();
                    $newGame->updated_at = \Carbon\Carbon::now();
                    $newGame->save();
                    $event_data = [
                        'game_id' => $newGame->id,
                        'user_id' => Auth::user()->id,
                        'avatar' => Auth::user()->avatar,
                        'user_name' => Auth::user()->username,
                        'bet' => $bet,
                        'mode' => $mode,
                    ];
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
            }
            event(new \App\Events\CoinFlip($event_data));
        }
    }

    public function joinGame(Request $request)
    {
        if (!$request->ajax()) {
            return response('Forbidden', 403);
        }

        $game_id = $request->input('game_id');

        if (Auth::check())
        {
            if ($game_id != '')
            {
                DB::beginTransaction();
                try {
                    $win = mt_rand(1,1000);

                    Log::info("Random");
                    Log::info($win);

                    if($win<=500){
                        Log::info($win.' <= 500');

                        $data = CoinFlip::where('id', $game_id)->first();

                        if ($data->csgo_user_id != Auth::user()->id)
                        {
                            $balance = Balance::where('user_id', Auth::user()->id)
                                ->where('type', 0)
                                ->where('currency_id', 1)
                                ->first();

                            if ($data->bet <= 0) {
                                $response = ['success' => false, 'message' => 'Bet must be more 0'];
                                return response()->json($response);
                            }

                            if ($balance->summ < $data->bet) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            if (($balance->summ - $data->bet) < 0) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            if (!Helper::debit($data->bet, 1)) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            $csgo_game = DB::table('coin_flip_games')
                                                ->where('id', $game_id)
                                                ->where('roll_user_id','<>',Auth::user()->id)
                                                ->whereRaw('win is null and csgo_user_id is null')
                                                ->update([
                                                    'csgo_user_id' => Auth::user()->id,
                                                    'win' => 'csgo',
                                                ]);

                            Log::info('$csgo_game');
                            Log::info($csgo_game);

                            if($csgo_game){
                                if (!Helper::credit($data->bet * 2, 1, 0, 'flip coin', $data->csgo_user_id)) {
                                    Log::info('ERROR. Balance has not been completely filled');
                                }

                                $this->updateCounters('coin-flip');

                                $event_data = [
                                    'game_id' => $data->id,
                                    'user_id' => Auth::user()->id,
                                    'avatar' => Auth::user()->avatar,
                                    'user_name' => Auth::user()->username,
                                    'user_id_live' => $data->csgo_user_id,
                                    'avatar_live' => User::select('avatar')->where('id',$data->csgo_user_id)->first()->avatar,
                                    'user_name_live' => User::select('username')->where('id',$data->csgo_user_id)->first()->username,
                                    'win' => 'csgo',
                                    'bet' => $data->bet,
                                ];

                                $LastWinsCount = LastWin::count();
                                if($LastWinsCount > 15){
                                    LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
                                }

                                $LastWin = new LastWin();
                                $LastWin->game = 'coin-flip';
                                $LastWin->game_id = $data->id;
                                $LastWin->save();

                                event(new \App\Events\CoinFlipJoin($event_data));
                            }
                            else{
                                Log::info('else csgogame <= 500');
                                $data = CoinFlip::where('id', $game_id)->first();

                                if ($data->roll_user_id != Auth::user()->id)
                                {
                                    $roll_game = DB::table('coin_flip_games')
                                                        ->where('id', $game_id)
                                                        ->where('csgo_user_id','<>',Auth::user()->id)
                                                        ->whereRaw('win is null and roll_user_id is null')
                                                        ->update([
                                                            'roll_user_id' => Auth::user()->id,
                                                            'win' => 'csgo',
                                                        ]);


                                    Log::info($roll_game);


                                    if($roll_game){
                                        if (!Helper::credit($data->bet * 2, 1, 0, 'flip coin', $data->csgo_user_id)) {
                                            Log::info('ERROR. Balance has not been completely filled');
                                        }

                                        $this->updateCounters('coin-flip');

                                        $event_data = [
                                            'game_id' => $data->id,
                                            'user_id' => Auth::user()->id,
                                            'avatar' => Auth::user()->avatar,
                                            'user_name' => Auth::user()->username,
                                            'user_id_live' => $data->csgo_user_id,
                                            'avatar_live' => User::select('avatar')->where('id',$data->csgo_user_id)->first()->avatar,
                                            'user_name_live' => User::select('username')->where('id',$data->csgo_user_id)->first()->username,
                                            'win' => 'csgo',
                                            'bet' => $data->bet,
                                        ];

                                        $LastWinsCount = LastWin::count();
                                        if($LastWinsCount > 15){
                                            LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
                                        }

                                        $LastWin = new LastWin();
                                        $LastWin->game = 'coin-flip';
                                        $LastWin->game_id = $data->id;
                                        $LastWin->save();

                                        event(new \App\Events\CoinFlipJoin($event_data));
                                    }
                                    else
                                    {
                                        if (!Helper::credit($data->bet, 1, 0, 'flip coin moneyback', Auth::user()->id)) {
                                            Log::info('ERROR. Balance has not been completely filled');
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Helper::credit($data->bet, 1, 0, 'flip coin moneyback', Auth::user()->id)) {
                                        Log::info('ERROR. Balance has not been completely filled');
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $data = CoinFlip::where('id', $game_id)->first();

                        if ($data->csgo_user_id != Auth::user()->id)
                        {
                            $balance = Balance::where('user_id', Auth::user()->id)
                                ->where('type', 0)
                                ->where('currency_id', 1)
                                ->first();

                            if ($data->bet <= 0) {
                                $response = ['success' => false, 'message' => 'Bet must be more 0'];
                                return response()->json($response);
                            }

                            if ($balance->summ < $data->bet) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            if (($balance->summ - $data->bet) < 0) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            if (!Helper::debit($data->bet, 1)) {
                                $response = ['success' => false, 'message' => 'Not enough money'];
                                return response()->json($response);
                            }

                            $csgo_game = DB::table('coin_flip_games')
                                                ->where('id', $game_id)
                                                ->where('roll_user_id','<>',Auth::user()->id)
                                                ->whereRaw('win is null and csgo_user_id is null')
                                                ->update([
                                                    'csgo_user_id' => Auth::user()->id,
                                                    'win' => 'roll',
                                                ]);

                            if($csgo_game){
                                if (!Helper::credit($data->bet * 2, 1, 0, 'flip coin', $data->roll_user_id)) {
                                    Log::info('ERROR. Balance has not been completely filled');
                                }

                                $this->updateCounters('coin-flip');

                                $event_data = [
                                    'game_id' => $data->id,
                                    'user_id' => Auth::user()->id,
                                    'avatar' => Auth::user()->avatar,
                                    'user_name' => Auth::user()->username,
                                    'user_id_live' => $data->roll_user_id,
                                    'avatar_live' => User::select('avatar')->where('id',$data->roll_user_id)->first()->avatar,
                                    'user_name_live' => User::select('username')->where('id',$data->roll_user_id)->first()->username,
                                    'win' => 'roll',
                                    'bet' => $data->bet,
                                ];

                                $LastWinsCount = LastWin::count();
                                if($LastWinsCount > 15){
                                    LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
                                }

                                $LastWin = new LastWin();
                                $LastWin->game = 'coin-flip';
                                $LastWin->game_id = $data->id;
                                $LastWin->save();

                                event(new \App\Events\CoinFlipJoin($event_data));
                            }
                            else{
                                $data = CoinFlip::where('id', $game_id)->first();

                                if ($data->csgo_user_id != Auth::user()->id)
                                {
                                    $roll_game = DB::table('coin_flip_games')
                                                        ->where('id', $game_id)
                                                        ->where('csgo_user_id','<>',Auth::user()->id)
                                                        ->whereRaw('win is null and roll_user_id is null')
                                                        ->update([
                                                            'roll_user_id' => Auth::user()->id,
                                                            'win' => 'roll',
                                                        ]);

                                    Log::info('Prefix roll '.$roll_game);

                                    if($roll_game){
                                        if (!Helper::credit($data->bet * 2, 1, 0, 'flip coin', $data->roll_user_id)) {
                                            Log::info('ERROR. Balance has not been completely filled');
                                        }

                                        $this->updateCounters('coin-flip');

                                        $event_data = [
                                            'game_id' => $data->id,
                                            'user_id' => Auth::user()->id,
                                            'avatar' => Auth::user()->avatar,
                                            'user_name' => Auth::user()->username,
                                            'user_id_live' => $data->roll_user_id,
                                            'avatar_live' => User::select('avatar')->where('id',$data->roll_user_id)->first()->avatar,
                                            'user_name_live' => User::select('username')->where('id',$data->roll_user_id)->first()->username,
                                            'win' => 'roll',
                                            'bet' => $data->bet,
                                        ];

                                        $LastWinsCount = LastWin::count();
                                        if($LastWinsCount > 15){
                                            LastWin::orderBy('created_at', 'asc')->take($LastWinsCount - 15)->delete();
                                        }

                                        $LastWin = new LastWin();
                                        $LastWin->game = 'coin-flip';
                                        $LastWin->game_id = $data->id;
                                        $LastWin->save();

                                        event(new \App\Events\CoinFlipJoin($event_data));
                                    }
                                    else{
                                        if (!Helper::credit($data->bet, 1, 0, 'flip coin moneyback', Auth::user()->id)) {
                                            Log::info('ERROR. Balance has not been completely filled');
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Helper::credit($data->bet, 1, 0, 'flip coin moneyback', Auth::user()->id)) {
                                        Log::info('ERROR. Balance has not been completely filled');
                                    }
                                }
                            }
                        }

                    }

                    DB::commit();
                }
                catch (\Exception $e) {

                    $data = CoinFlip::where('id', $game_id)->first();

                    if (!Helper::credit($data->bet, 1, 0, 'flip coin moneyback', Auth::user()->id)) {
                        Log::info('ERROR. Balance has not been completely filled');
                    }

                    DB::rollback();
                }
            }
        }

    }

    public function loadGame(Request $request)
    {
        if (!$request->ajax()) {
            return response('Forbidden', 403);
        }
        $response = [];

        $roll_games_query = "select cf.id as game_id, u.id as user_id, u.avatar as avatar, u.username as user_name, cf.bet as bet, 'roll' as mode  from coin_flip_games cf, users u where cf.roll_user_id = u.id and cf.roll_user_id is not null and cf.csgo_user_id is null;";
        $roll_games_products = DB::select($roll_games_query);
        $roll_games = collect($roll_games_products);

        if (sizeof($roll_games)) {
            foreach ($roll_games as $roll_game) {
                array_push($response, $roll_game);
            }
        }

        $csgo_games_query = "select cf.id as game_id, u.id as user_id, u.avatar as avatar, u.username as user_name, cf.bet as bet, 'csgo' as mode  from coin_flip_games cf, users u where cf.csgo_user_id = u.id and cf.csgo_user_id is not null and cf.roll_user_id is null;";
        $csgo_games_products = DB::select($csgo_games_query);
        $csgo_games = collect($csgo_games_products);

        if (sizeof($csgo_games)) {
            foreach ($csgo_games as $csgo_game) {
                array_push($response, $csgo_game);
            }
        }
        // array_push($response,$csgo_games);

        return response()->json($response);

        /*   $games_csgo = CoinFlip::where('csgo_user_id',null)->get();
           $games_roll = CoinFlip::where('roll_user_id',null)->get();

           if(sizeof($games_csgo)){

           }*/

    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function updateCounters($game_type){
        $counter = CounterGames::where('games', $game_type)->first();
        $counter->count++;
        $counter->save();
    }
}
