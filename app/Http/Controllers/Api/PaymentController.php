<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Affiliate;
use App\Models\Currency;
use App\Models\PaymentSystems\SkinPay;
use Illuminate\Http\Request;
use App\Models\PaymentSystems\G2APay;
use App\Models\PaymentSystems\PayOp;
use Helper;
use Illuminate\Support\Facades\Redirect;
use Partnership;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\G2payIpnRequest;
use App\Models\Category;
use App\Models\Setting;
use App\Models\User;
use App\Models\Balance;
use Illuminate\Support\Facades\Log;
use Config;
use DB;
use JavaScript;

/**
 * Class PaymentController
 * @package App\Http\Controllers\Api
 */
class PaymentController extends Controller
{
	private $activeCurrency;

	public function __construct(){
		parent::__construct();

		JavaScript::put([
			'BASE_URL' => url('/'),
			'LIVE_DROP_PORT'=> Config::get('broadcasting.live_drop_port')
		]);
		$this->activeCurrency = Currency::getActive();
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function getSuccess(Request $request)
	{
		$order = $this->user->orders()->orderBy('id', 'desc')->first();
		$categories = Category::orderBy('sort')->get();
		$params = ['categories'=>$categories, 'payment' => 'success'];

		if(!empty($order)) {
			$params['amount'] = $order->amount;
			$params['currency'] = ($order->currency_id == 7) ? "RUB" : "USD" ;
		}

		return redirect('/')->with('g2a_status', 'success');
	}

	/**
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function getFail()
	{
		return redirect('/')->with('g2a_status', 'fail');
		//return view('site.payment-fail');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getPayments()
	{
		return view('site.payments',[
			'payments' => Order::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get(),
		]);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postMake(Request $request){
        switch($request->input('payment_system')){
            case '':
                #$answer = $this->g2aPayStart($request);
                $answer = $this->payPayOpStart($request);
                break;

            case 'skinpay':
                $answer = $this->skinPayStart($request);
                break;

            default:
                $answer = [
                    'message' => 'Unknown error. Please, try again later.',
                    'status' => 422,
                ];
                break;
        }

		return response()->json($answer, $answer['status']);
	}

	/**
	 * @param $request
	 *
	 * @return array
	 */
	private function g2aPayStart($request)
	{
		$currency = Currency::find(1);
		$amount = (float)$request->input('amount');

		if($amount < 1){
			return [
				'message' => trans('messages.payment_error').', '.trans('messages.contact').' csgotowerhelp@gmail.com',
				'status' => 422,
			];
		}

		$order = Order::create([
			'payment_system'=>'g2pay',
			'amount'=> $amount,
			'user_id'=> $this->user->id,
			'currency_id'=> $currency->id
		]);

		$g2aPay = new G2APay();
		$g2aPay->amount                     = $amount;
		$g2aPay->currency                   = $currency->code;
		$g2aPay->order_id                   = $order->id;
		$g2aPay->sku                        = $currency->id;
		$g2aPay->security_steam_id          = $this->user->steamid;
		$g2aPay->security_registration_date = $this->user->created_at->format('Y-m-d');
		$info  = DB::table('orders')
		           ->select(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as created_at"),
			           DB::raw('DATEDIFF(NOW(),created_at) as days_left'))
		           ->where('user_id',$this->user->id)
		           ->where('status', 'complete')
                   ->where('payment_system', 'g2pay')
		           ->take(1)
		           ->first();

		if($info){
			$g2aPay->security_new_client                = ($info->days_left<7) ? 1 : 0;
			$g2aPay->security_first_transaction_date    = $info->created_at;
		}

		$content = $g2aPay->getToken();

		if(isset($content['status']) && $content['status'] == 'ok'){
			Session::put('order_id', $order->id);
			return [
				'message' => $g2aPay->redirect_url.'?token='.$content['token'],
				'status' => 200,
			];
		}
	}

	/**
	 * @param $request
	 *
	 * @return array
	 */
	private function skinPayStart($request)
	{
		$currency = Currency::find(1);

		$order = Order::create([
			'payment_system'=>'skinpay',
			'user_id'=> $this->user->id,
			'currency_id'=> $currency->id
		]);

		$SkinPay = new SkinPay();
		return [
			'message' => $SkinPay->createOrder($order->id),
			'status' => 200,
		];

	}

	/**
	 * @param Request $request
	 * //
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function postG2payIpn(Request $request)
	{
		$this->validate($request, [
			'userOrderId' => 'required',
			'status' => 'required',
			'transactionId' => 'required',
			'amount' => 'required',
			'currency' => 'required',
			'hash' => 'required',
		]);

		$order_id       = $request->input('userOrderId');
		$status         = $request->input('status');
		$transactionId  = $request->input('transactionId');
		$amount         = (float)$request->input('amount');
		$currency       = $request->input('currency');
		$valid_hash     = $request->input('hash');

		$g2pay = new G2APay();
		$validate_ipn_hash = $g2pay->validate_hash($order_id, $amount, $transactionId, $valid_hash);

		if($validate_ipn_hash){
			if(!$order = Order::where('id', $order_id)->where('status', '!=', 'complete')->first()){
				return response('Error', 422);
			}
			$order->status = $status;
			$order->transaction_id = $transactionId;

			if($order->save() && $order->status == 'complete'){
				$currency_obj = Currency::where('code', $currency)->first();
				if( !Helper::credit($amount, $currency_obj->id, 0, 'g2a transaction', $order->user_id) ) {
					Log::info('postG2payIpn ERROR. Balance has not been completely filled');
				}

				Partnership::referBonus($order->user_id, $amount, $currency);
			}
		}

		return response('Ok', 200);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function skinPayAnswer(Request $request)
	{
		$this->validate($request, [
			'orderid' => 'required',
			'transaction_id' => 'required',
			'status' => 'required',
			'amount_rur' => 'required',
			'amount_currency_rate' => 'required',
			'amount_currency' => 'required',
		]);

		if(!$order = Order::where('id', $request->input('orderid'))->where('status', '!=', 'complete')->first()){
			return response('Error, order not found or completed', 422);
		}

		switch($request->input('status')){
			case 'success':
				$status = 'complete';
				break;

			default:
				$status = 'rejected';
				break;
		}

		$amount = $request->input('amount_rur') / $request->input('amount_currency_rate');
		$order->transaction_id = $request->input('transaction_id');
		$order->currency_id = 1;
		$order->amount = $amount / 100;
		$order->status = $status;

		if($order->save() && $status == 'complete'){
			//$currency_obj = Currency::where('code', strtoupper($request->input('amount_currency')) )->first();
			if( !Helper::credit($amount/ 100, 1, 0, 'skin-pay transaction', $order->user_id) ) {
				Log::info('skinPayAnswer ERROR. Balance has not been completely filled');
			}

			Partnership::referBonus($order->user_id, $amount/ 100, strtoupper($request->input('amount_currency')));
		}

		return response('OK', 200);
	}

    /**
     * @param Request $request
     *
     * @return array
     */
	public function payPayOpStart(Request $request)
    {
        $currency = Currency::find(1);
        $amount = (float)$request->input('amount');

        if($amount < 1){
            return [
                'message' => trans('messages.payment_error').', '.trans('messages.contact').' csgotowerhelp@gmail.com',
                'status' => 422,
            ];
        }

        $order = Order::create([
            'payment_system'=>'payop',
            'amount'=> $amount,
            'user_id'=> $this->user->id,
            'currency_id'=> $currency->id,
            'user_ip'=> $request->ip(),
        ]);

        $payop = new PayOp();

        $invoice = $payop->createInvoice([
            'order' => [
                'id'          => (string)$order->id,
                'amount'      => (string)$amount,
                'currency'    => $currency->code,
            ],
            'payer' => [
                'email' => (empty($this->user->email)) ? $this->user->id .'@csgotower.net' : $this->user->email,
                'name'  => $this->user->username,
                'phone' => (string)$this->user->steamid
            ]
        ]);

        if(!$invoice->status) {
            return [
                'message' => trans('messages.payment_error').', '.trans('messages.contact').' csgotowerhelp@gmail.com',
                'status' => 422,
            ];
        }

        $order->invoice_id = $invoice->data;
        $order->save();
        Session::put('order_id', $order->id);

        return [
            'message' => 'https://payop.com/en/payment/invoice-preprocessing/'. $invoice->data,
            'status' => 200,
        ];
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function postPayOpIpn(Request $request) {

	    $input = json_decode($request->getContent(), true);

        Log::info('postOpPayIpn');
        Log::info(print_r($input, true));

        $finalStatuses = [
            '1' => 'new',
            '2' => 'complete',
            '3' => 'failed',
            '4' => 'pending',
            '5' => 'failed'
        ];

        $payop = new PayOp();
        $transactions = (array)$payop->getTransactions($input['transaction']['id']);

        if(1 === (int)$transactions['status']) {

            $transactions = $transactions['data'];

            if(!$order = Order::where('id', $transactions->orderId)->where('status', '!=', 'success')->first()){
                return response('Error', 422);
            }
            $order->status = $finalStatuses[$transactions->state];
            $order->transaction_id = $transactions->identifier;
            $order->payer_info = json_encode($transactions->payerInformation);
            $order->geo_info = json_encode($transactions->geoInformation);

            if($order->save() && $order->status == 'success') {

                $currency = Currency::where('code', $transactions->currency)->first();

                if( !Helper::credit($transactions->amount, $currency->id, 0, 'PayOp transaction', $order->user_id) ) {
                    Log::info('postPayOpIpn ERROR. Balance has not been completely filled');
                }
                Partnership::referBonus($order->user_id, $transactions->amount, $transactions->currency);
            }
        }

        return response('Ok', 200);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function postPayOpReturn(Request $request) {

        Log::info('postOpPayReturn');
        Log::info(print_r($request->all(), true));

        $status    = $request->input('status');
        $txid      = $request->input('txid');

        $payop = new PayOp();
        $transactions = (array)$payop->getTransactions($txid);

        if(1 === (int)$transactions['status']) {

            $transactions = $transactions['data'];

            if(!$order = Order::where('id', $transactions->orderId)->where('status', '!=', 'success')->first()){
                return response('Error', 422);
            }
            $order->status = $status;
            $order->transaction_id = $transactions->identifier;
            $order->payer_info = json_encode($transactions->payerInformation);
            $order->geo_info = json_encode($transactions->geoInformation);

            if($order->save() && $order->status == 'success') {

                $currency = Currency::where('code', $transactions->currency)->first();

                if( !Helper::credit($transactions->amount, $currency->id, 0, 'PayOp transaction', $order->user_id) ) {
                    Log::info('postOpPayReturn ERROR. Balance has not been completely filled');
                }
                Partnership::referBonus($order->user_id, $transactions->amount, $transactions->currency);
            }
        }

        return redirect(route('csgomarket.homepage.index'));
    }
}
