<?php namespace App\Http\Controllers\Auth;

use Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * @param Guard $auth
	 * @param PasswordBroker $passwords
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest');
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function remindPassword(\Illuminate\Http\Request $request)
	{
		/*$this->validate($request, [
			'email' => 'required|exists:users',
		]);

		$user = User::where('email', $request->input('email'))->first();
		$new_random_password = str_random(8);
		$user->password = Hash::make($new_random_password);
		$user->save();

		$to      = 'uzbxodgiev@gmail.com';
		$subject = 'the subject';
		$message = 'hello';
		$headers = 'From: csgotower <support@csgotower.net>' . "\r\n" .
		           'X-Mailer: PHP/' . phpversion();

		$ans = mail($to, $subject, $message, $headers);

		/*Mail::send('emails.new-password', [ 'new_password' => $new_random_password], function($m) use($user){
			$m->from('support@csgotower.com', 'csgotower');
			$m->to($user->email);
			$m->subject('New password!');
		});

		return response()->json([
			'message' => 'The new password has been successfully sent to your email address!: '.$ans
		]);*/
	}

}
