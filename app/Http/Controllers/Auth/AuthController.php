<?php namespace App\Http\Controllers\Auth;

use App\Events\NewUser;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Models\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Invisnik\LaravelSteamAuth\SteamAuth;
use Auth;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use Partnership;
use Validator;

class AuthController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * AuthController constructor.
	 * @param SteamAuth $steam
	 */
	public function __construct(SteamAuth $steam)
	{
		$this->steam 		= $steam;
		if(Auth::check() && Auth::user()->level()>1){
			Auth::logout();
		}
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function getSteamLogin(Request $request)
	{
		if ($this->steam->validate()) {
			$info = $this->steam->getUserInfo();

			if (! is_null($info)) {
				$user = User::where('steamid', $info->steamID64)->first();
				if (! is_null($user)) {
					Auth::login($user, true);
				}else{
					$userRole = Role::find(1);
					$user = User::create([
						'username' => htmlspecialchars($info->personaname),
						'avatar'   => $info->avatarfull,
						'steamid'  => $info->steamID64
					]);
					$user->attachRole($userRole);
					Auth::login($user, true);

					event(new \App\Events\NewUser());
				}

				if($refer_id = $request->cookie('refer_id')){
					Partnership::newAffiliate($refer_id, Auth::user()->id);
				}

				echo "<script>
					opener.loadFullPage(false, '". Session::get('locale')  ."');
					window.close();
				</script>";
			}
		} else {
			return $this->steam->redirect(); // redirect to Steam login page
		}
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	/*public function signIn(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email|unique:users|max:255',
			'password' => 'required|min:8|max:32|alpha_dash',
			'password2' => 'required|same:password',
		]);

		// registration
		if(!$user = User::create([
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('password')),
		])){
			return response()->json('Something goes wrong! Please, try again later.', 422);
		}

		// authorization
		$user->attachRole(Role::find(1));
		Auth::login($user, true);
		event(new NewUser());
		if($refer_id = $request->cookie('refer_id')){
			Partnership::newAffiliate($refer_id, Auth::user()->id);
		}


		return response()->json([
			'message' => 'You are successfully signed in!',
			'locale' => Session::get('locale'),
		], 200);
	}*/

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function standardLogin(Request $request)
	{
		if(!$user = User::where('email', $request->input('email'))->first()){
			return response()->json('Wrong email or password!', 422);
		}
		if(!Hash::check($request->input('password'), $user->password)){
			return response()->json('Wrong email or password!', 422);
		}

		Auth::login($user, true);

		return response()->json([
			'locale' => Session::get('locale'),
		], 200);
	}

	/**
	 * @param Request $request
	 */
	/*public function login(Request $request)
	{
		$s = file_get_contents('http://ulogin.ru/token.php?token=' . $request->input('token') . '&host=' . $_SERVER['HTTP_HOST']);
		$data = json_decode($s, true);

		if($user = User::where('network', $data['network'])->where('identity', $data['identity'])->first()){
			Auth::login($user, true);
		} else {
			$user = User::create([
				'username' => empty($data['nickname']) ? $data['first_name'] : $data['nickname'],
				'avatar'   => $data['photo_big'],
				'network'  => $data['network'],
				'identity'  => $data['identity'],
			]);

			$user->attachRole(Role::find(1));

			Auth::login($user, true);

			event(new NewUser());
		}

		if($refer_id = $request->cookie('refer_id')){
			Partnership::newAffiliate($refer_id, Auth::user()->id);
		}

		return Session::get('locale');

		echo "<script>
					opener.loadFullPage(false, '". Session::get('locale')  ."');
					window.close();
				</script>";
	}*/

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function postLogin(Request $request)
	{
		$validator = User::loginValidator($request->all());

		if ($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors());
		}

		$email 		= $request->input('email');
		$password 	= $request->input('password');
		$remember 	= $request->input('remember_token');

		if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1], $remember)) {
			return redirect()->intended(Config::get('app.admin_prefix'));
		} else {
			return redirect()->back()->withErrors(['incorrect' => 'Incorrect Email or Password']);
		}
	}

	public function getLogin()
	{
		return view('admin.users.login');
	}

	public function getLogout(){
		if(Auth::check()){
			Auth::logout();

		}
		return redirect('/');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		]);
	}

	protected function create(array $data)
	{
		return User::create([
			'name'		=> $data['name'],
			'username'	=> $data['username'],
			'email'		=> $data['email'],
			'steamid'	=> $data['steamid'],
			'avatar'	=> $data['avatar'],
			'active'	=> 1,
			'password'	=> bcrypt($data['password'])
		]);
	}
}
