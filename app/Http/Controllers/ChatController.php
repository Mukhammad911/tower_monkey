<?php

namespace App\Http\Controllers;

use App\Models\ChatMessageDe;
use App\Models\ChatMessageEn;
use App\Models\ChatMessageEs;
use App\Models\ChatMessagePl;
use App\Models\ChatMessageRu;
use App\Models\ChatMessageTr;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Snipe\BanBuilder\CensorWords;

class ChatController extends Controller
{

    public function getDefault(Request $request)
    {

        $this->validate($request, [
            'type' => 'in:en,ru,tr,es,de,pl'
        ]);

        $type = $request->input('type');

        switch ($type) {
            case 'en':
                $chat = ChatMessageEn::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
            case 'ru':
                $chat = ChatMessageRu::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
            case 'tr':
                $chat = ChatMessageTr::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
            case 'es':
                $chat = ChatMessageEs::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
            case 'de':
                $chat = ChatMessageDe::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
            case 'pl':
                $chat = ChatMessagePl::take(20)->get();
                if (!empty($chat) || !is_null($chat)) {

                    $data = [];
                    foreach($chat as $key => $user){
                        $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
                        $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;
                        $data[$key]['user_id'] = $user->user_id;
                        $data[$key]['message'] = $user->message_clean;
                    }

                    return response()->json([
                        'success' => true,
                        'data' => $data,
                    ]);

                }

                $response = ['success' => false, 'data' => $chat];
                return response()->json($response);
                break;
        }

    }

    public function addMessage(Request $request)
    {
        if(!Auth::check())
        {
            $response = ['success' => false, 'message' => 'Login to chat...'];
            return response()->json($response);
        }

        $this->validate($request, [
            'type' => 'in:en,ru,tr,es,de,pl'
        ]);
        $type = $request->input('type');
        $msg_event = $msg = $request->input('message');

        if (strlen($msg) > 500) {
            $response = ['success' => false, 'message' => 'Text length must be not more then 500 symbols.'];
            return response()->json($response);
        }

        switch ($type)
        {
            case 'en':
                $last_message_unix = ChatMessageEn::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessageEn::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessageEn();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'en',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
            case 'ru':
                $last_message_unix = ChatMessageRu::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessageRu::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessageRu();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'ru',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
            case 'tr':
                $last_message_unix = ChatMessageTr::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessageTr::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessageTr();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'tr',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
            case 'es':
                $last_message_unix = ChatMessageEs::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessageEs::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessageEs();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'es',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
            case 'de':
                $last_message_unix = ChatMessageDe::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessageDe::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessageDe();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'de',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
            case 'pl':
                $last_message_unix = ChatMessagePl::where('user_id', Auth::user()->id)->max('created_unix_utc');

                if ((\Carbon\Carbon::now()->timestamp - $last_message_unix) <= 5) {
                    $response = ['success' => false, 'message' => 'Sending message less then 5 sec period is impossible.'];
                    return response()->json($response);
                }

                $last_message = ChatMessagePl::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();

                if (isset($last_message->created_unix_utc)) {
                    if ((\Carbon\Carbon::now()->timestamp - $last_message->created_unix_utc) <= 60) {
                        if ($last_message->message == $msg) {
                            $response = ['success' => false, 'message' => 'Sending similar message less then 1 minute period is impossible.'];
                            return response()->json($response);
                        }
                    }
                }

                // The Regular Expression filter
                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                // Check if there is a url in the text
                if (preg_match($reg_exUrl, $msg, $url)) {
                    // make the urls hyper links
                    $msg_event = preg_replace($reg_exUrl, " ", $msg);
                }

                $censor = new CensorWords;

                $badwords = $censor->setDictionary(storage_path('dictionary.php'));
                $string = $censor->censorString($msg_event);

                $msg_event = $string['clean'];

                $newMsg = new ChatMessagePl();
                $newMsg->user_id = (int)Auth::user()->id;
                $newMsg->message = $msg;
                $newMsg->message_clean = $msg_event;
                $newMsg->created_unix_utc = \Carbon\Carbon::now()->timestamp;
                $newMsg->created_at = \Carbon\Carbon::now();
                $newMsg->updated_at = \Carbon\Carbon::now();
                $newMsg->save();

                $event_data = [
                    'msg_id' => $newMsg->id,
                    'user_id' => Auth::user()->id,
                    'avatar' => Auth::user()->avatar,
                    'username' => Auth::user()->username,
                    'message' => $msg_event,
                    'type' => 'pl',
                ];

                event(new \App\Events\ChatMessageEvent($event_data));
                break;
        }

    }
}
