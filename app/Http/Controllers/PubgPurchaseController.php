<?php

namespace App\Http\Controllers;

use App\Models\SteamCase;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Yajra\Datatables\Datatables;
use App\Models\PubgPurchase;
use App\Models\PubgProduct;
use App\Models\PubgBot;
use App\Models\PubgDropsMarket;
use App\Http\Requests\PurchaseRequest;
use Lang;
use Config;

class PubgPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pubg_purchases.index');
    }

    public function getPurchases()
    {
        $data = DB::table('pubg_purchases')->get();
        return view('admin.pubg_purchases.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function create(PubgPurchase $purchase)
    {
        $bots = PubgBot::all();
        $cases = SteamCase::all();
        return view('admin.pubg_purchases.create', ['bots' => $bots, 'cases' => $cases, 'caseIds' => []])->with('purchase', $purchase);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase = PubgPurchase::create($fields);


        $purchase->cases()->attach($request->input('cases'));
        Redis::publish(Config::get('app.redis_prefix') . 'pubg-purchase-change-' . $purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));

        return redirect(Config::get('app.admin_prefix') . '/pubg-purchase')->with('success', 'Successfully created purchase!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(PubgPurchase $purchase)
    {
        $bots = PubgBot::all();
        $cases = SteamCase::all();
        $purchasedCases = $purchase->cases()->get(array('case_id'))->toArray();

        $func = function ($item) {
            return $item['case_id'];
        };

        return view('admin.pubg_purchases.edit', ['bots' => $bots, 'cases' => $cases, 'caseIds' => array_map($func, $purchasedCases)])->with('purchase', $purchase);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Purchase $purchase
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(PubgPurchase $purchase, PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase->update($fields);

        $purchase->cases()->sync($request->input('cases'));

        Redis::publish(Config::get('app.redis_prefix') . 'pubg-purchase-change-' . $purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));
        return redirect(Config::get('app.admin_prefix') . '/pubg-purchase')->with('success', 'Successfully purchase updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(PubgPurchase $purchase)
    {
        $ret = $purchase->delete();
        Redis::publish(Config::get('app.redis_prefix') . 'pubg-purchase-change-' . $purchase->bot_id, json_encode(['purchaseId' => null]));
        return response()->json($ret);
    }

    private function prepareFields($request)
    {
        $fields = $request->all();
        $fields['max_price'] = $fields['max_price'] * 100;
        $fields['target'] = ($fields['target'] <= 900) ? $fields['target'] : 900;

        return $fields;
    }

    public function deletePurchases($id)
    {
        $purchase_id = $id;
        DB::table('pubg_purchases')->where('id', '=', $purchase_id)->delete();
        DB::table('pubg_search_items')->where('purchase_id', '=', $purchase_id)->delete();


        return redirect('spidercontrolpanel/pubg-purchase');
    }

    public function createFormPurchases()
    {
        return view('admin.pubg_purchases.create');
    }

    public function createPurchases()
    {
        $dota_bots = DB::table('pubg_bots')->select('id', 'steam_login')->get();
        return response()->json($dota_bots);
    }

    public function getProductPurchases()
    {
        $dota_products = DB::table('pubg_products')->select('id', 'name', 'image', 'price')->orderBy('name','asc')->groupBy('name')->get();
        return response()->json($dota_products);
    }

    public function savePurchases(Request $request)
    {
        $error = ['var_error', 'bot_error', 'not_inserted'];
        $check_exist_bot_id = false;
        $bot_id = $request->input('bot_id');
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $quality = 'all';
        $rarity = 'all';
        $purchase_id = '';
        if ($bot_id == '' || $min_price == '' || $max_price == '' || $quality == '' || $rarity == '') {
            return response()->json($error[0]);
        } else {
            $db_bot_id = DB::table('pubg_purchases')->select('bot_id')->get();
            foreach ($db_bot_id as $bot_id_id) {
                if ($bot_id == $bot_id_id->bot_id) {
                    $check_exist_bot_id = true;
                }
            };
            if ($check_exist_bot_id) {
                return response()->json($error[1]);
            } else {
                $inserted_purchase = DB::table('pubg_purchases')->insert([
                    'bot_id' => $bot_id,
                    'min_price' => $min_price,
                    'max_price' => $max_price,
                    'quality' => $quality,
                    'rarity' => $rarity
                ]);
                if ($inserted_purchase) {
                    $purchases_id = DB::table('pubg_purchases')->select('id')->where('bot_id', '=', $bot_id)->get();
                    foreach ($purchases_id as $id) {
                        $purchase_id = $id->id;
                    }
                    $json_response_true = ['message' => 'success', 'purchase_id' => $purchase_id];
                    return response()->json($json_response_true);
                } else {
                    return response()->json($error[2]);
                }
            }
        }
    }

    public function saveProductPurchases(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $product_name = $request->input('name');
        $product_quota = $request->input('quota');
        $product_price = $request->input('price');
        $check =DB::table('pubg_search_items')->insert([
            'purchase_id' => $purchase_id,
            'name' => $product_name,
            'quota' => $product_quota,
            'price'=>$product_price
        ]);
        if(!$check){
            Log::info("Error insert product: ".$product_name);
        }
    }
    public function saveProductPurchasesComplete(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $bot_id = $request->input('bot_id');

        Redis::publish(Config::get('app.redis_prefix') . 'pubg-purchase-change-' . $bot_id, json_encode(['purchaseId' => $purchase_id]));

        $json_response_true = ['message' => 'success', 'bot_id' => $bot_id, 'purchase_id' => $purchase_id];
        return response()->json($json_response_true);
    }
}
