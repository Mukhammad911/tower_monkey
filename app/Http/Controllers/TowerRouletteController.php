<?php

namespace App\Http\Controllers;

use App\CustomClasses\Helper;
use App\Models\RouletteTower;
use App\Models\RouletteTowerGames;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;

class TowerRouletteController extends Controller
{
    public function getBalance(){

        if(Auth::check()){
            $helper = new Helper();
            $balance = number_format($helper->get('balance')->summ, 2, '.', '');

            return response()->json(['success' => true, 'balance' => $balance]);
        }
        return response()->json(['success' => false]);


    }

    public function checkUserAuth(){

        if(Auth::check()){
            return response()->json(['success'=>true]);
        }

        return response()->json(['success'=>false]);

    }

    public function rouletteBet(Request $request){
        $helper = new Helper();

        $response = ['success'=>true, 'message'=>''];

        if(!Auth::check())
        {
            $response['success'] = false;
            $response['message'] = 'You must login to bet!';

            return response()->json($response);
        }

        $user = Auth::user();

        $this->validate($request, [
            'bet' => 'required|numeric|between:0.01,5000',
            'type' => 'in:2x,3x,5x,50x'
        ]);

        $roulette_game = RouletteTower::where('status', 'opened')->first();

        if(empty($roulette_game) || is_null($roulette_game))
        {
            $response['success'] = false;
            $response['message'] = 'The game is not found';
            return response()->json($response);
        }

        $type = $request->input('type');

        $exist_game = RouletteTowerGames::where('user_id', $user->id)->where('type', $type)->where('roulette_tower',$roulette_game->id)->first();

        if(!empty($exist_game) || !is_null($exist_game))
        {
            $response['success'] = false;
            $response['message'] = 'You can not make a double bet';
            return response()->json($response);
        }

        if (!$helper->debit($request->input('bet'), 1, 'tower bet')) {
            return response()->json([
                'success' => false,
                'message' => 'Not enough money!',
            ]);
        }

        $rouletteTower = new RouletteTowerGames();
        $rouletteTower->bet = $request->input('bet');
        $rouletteTower->roulette_tower = $roulette_game->id;
        $rouletteTower->user_id = $user->id;
        $rouletteTower->type = $type;
        $rouletteTower->win = 0;

        $rouletteTower->save();

        switch ($type){
            case '2x':
                $old_value = Redis::get('2x');
                $new_value = (float)$old_value + (float)$request->input('bet');
                Redis::set('2x', $new_value);

                $old_jackpot_add = Redis::get('jackpot-add');
                $old_jackpot_add = (float)$old_jackpot_add;
                $result_jackpot_add = (float)$request->input('bet') + $old_jackpot_add;

                Redis::set('jackpot-add', $result_jackpot_add);

                break;
            case '3x':
                $old_value = Redis::get('3x');
                $new_value = (float)$old_value + (float)$request->input('bet');
                Redis::set('3x', $new_value);

                $old_jackpot_add = Redis::get('jackpot-add');
                $old_jackpot_add = (float)$old_jackpot_add;
                $result_jackpot_add = (float)$request->input('bet') + $old_jackpot_add;

                Redis::set('jackpot-add', $result_jackpot_add);

                break;
            case '5x':
                $old_value = Redis::get('5x');
                $new_value = (float)$old_value + (float)$request->input('bet');
                Redis::set('5x', $new_value);

                $old_jackpot_add = Redis::get('jackpot-add');
                $old_jackpot_add = (float)$old_jackpot_add;
                $result_jackpot_add = (float)$request->input('bet') + $old_jackpot_add;

                Redis::set('jackpot-add', $result_jackpot_add);

                break;
            case '50x':
                $old_value = Redis::get('50x');
                $new_value = (float)$old_value + (float)$request->input('bet');
                Redis::set('50x', $new_value);

                $old_jackpot_add = Redis::get('jackpot-add');
                $old_jackpot_add = (float)$old_jackpot_add;
                $result_jackpot_add = (float)$request->input('bet') + $old_jackpot_add;

                Redis::set('jackpot-add', $result_jackpot_add);

                break;
        }

        $jackpot_add = Redis::get('jackpot-add');

        $rouletteTower['avatar'] = $user->avatar;
        $rouletteTower['username'] = $user->username;
        $rouletteTower['jackpot_add'] = $jackpot_add;

        Redis::publish(Config::get('app.redis_prefix') . 'roulette-tower-bet-add', json_encode([
            'data' => $rouletteTower
        ]));

        return response()->json([
            'success' => true,
            'message' => 'The data successfully added',
        ]);
    }

    public function loadUsers(){

        $roulette_game = RouletteTower::where('status', 'opened')->first();

        if(empty($roulette_game) || is_null($roulette_game))
        {
            $response['success'] = false;
            $response['message'] = 'The game is not found';
            return response()->json($response);
        }

        $users = RouletteTowerGames::where('roulette_tower', $roulette_game->id)->get();

        if(empty($users) || is_null($users))
        {
            $response['success'] = false;
            $response['message'] = 'Data not found';
            return response()->json($response);
        }

        $data = [];
        foreach($users as $key => $user){
            $data[$key]['avatar'] = User::where('id', $user->user_id)->first()->avatar;
            $data[$key]['username'] = User::where('id', $user->user_id)->first()->username;

            $data[$key]['bet'] = $user->bet;
            $data[$key]['type'] =$user->type;
            $data[$key]['user_id'] = $user->user_id;
            $data[$key]['win'] = $user->win;
        }

        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }

    public function getJackpot(){
        $jackpot = Redis::get('jackpot-add');

        return response()->json(['success'=>true, 'data'=>$jackpot]);

    }

    public function getHistory()
    {
        $history = RouletteTower::where('status', 'closed')
        ->orderBy('id', 'DESC')
        ->take(10)
        ->get();

        return response()->json($history);
    }
}
