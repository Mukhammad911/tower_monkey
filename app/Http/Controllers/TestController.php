<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\PaymentController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class TestController
 * @package App\Http\Controllers
 */
class TestController extends Controller
{
	// TODO: remove it
	public function testPay(Request $request)
	{
		$PaymentController = new PaymentController();

		$PaymentController->postG2payIpn($request);
	}
}
