<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SteamCase;
use DB;
use App\Models\DotaDropsMarket;
use Illuminate\Support\Facades\Auth;
use Session;
use Helper;
use App;
use Lang;
use Config;
use ProvablyFair;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/**
 * Class BotController
 * @package App\Http\Controllers
 */
class DotaDropController extends Controller
{
    private $user_id;
    private $status;
    private $price;
    private $product_id;
    private $name;
    private $short_description;
    private $market_name;
    private $image_hash;
    private $quality;
    private $class;
    private $stattrak;
    private $assetid;
    private $classid;
    private $instanceid;
    private $bot_id;

    public function saveAvailableProduct($id)
    {
        $ret = ['success' => true, 'drop_id' => null];

        $user = Auth::user();
        //$case_id = $request->input('case_id');
        //$attempt = $request->input('attempt');
        //$case = SteamCase::find($case_id);
        //$ret['case_price'] = $case->priceObj->price;

        /*$scenario = Scenario::query()
            ->where('user_id', $user->id)
            ->where('case_id', $case_id)
            ->where('status', 'enabled')
            ->first();*/

        /*if($user->youtuber){
            $win = new WinYoutuber($case);
        } else if(Config::get('app.promotion_mode') && $scenario){
            if(is_null(Redis::get('stats-count-'.$case_id.'-'.$user->id))){
                Redis::set('stats-count-'.$case_id.'-'.$user->id, 0);
            }
            $current_attempt = Redis::get('stats-count-'.$case_id.'-'.$user->id);

            if($scenario->attempt == $current_attempt + 1){
                $win = new PromotionScript($case, $user, $scenario);
            } else{
                Redis::set('stats-count-'.$case_id.'-'.$user->id, $current_attempt + 1 );
                $win = new Win2($case);
            }
        } else{
            $win = new Win2($case);
        }*/
        $items = DB::table('dota_available_products')->where('id', '=', $id)->get();

        foreach ($items as $item) {
            $this->product_id = $item->product_id;
            $this->price = $item->price;
            $this->name = $item->name;
            $this->short_description = $item->short_description;
            $this->market_name = $item->market_name;
            $this->image_hash=$item->image_hash;
            $this->quality = $item->quality;
            $this->class = $item->class;
            $this->stattrak = $item->stattrak;
            $this->assetid = $item->assetid;
            $this->classid = $item->classid;
            $this->instanceid = $item->instanceid;
            $this->bot_id = $item->bot_id;
        }

        /*if(!$item){
            $this->moneyBack($user, $case->priceObj->price);

            $ret['notify']  = true;
            $ret['success'] = false;
            $ret['message'] = trans('messages.unavailable');
            return response()->json($ret);
        }*/

        // $win_number = ProvablyFair::getWinNumber($case_id, $weapon);
        //$hash = ProvablyFair::getHash($case_id, $attempt+1, $win_number);

        $price = Helper::exchange($this->price, 1);

        try {
            DB::beginTransaction();

            $drop = new DotaDropsMarket();
            $drop->user_id = $user->id;
            $drop->status = 'pending';
            $drop->product_id = $this->product_id;
            $drop->price = $this->price;
            $drop->name = $this->name;
            $drop->short_description = $this->short_description;
            $drop->market_name = $this->market_name;
            $drop->image_hash = $this->image_hash;
            $drop->quality = $this->quality;
            $drop->class = $this->class;
            $drop->stattrak = $this->stattrak;
            $drop->assetid = $this->assetid;
            $drop->classid = $this->classid;
            $drop->instanceid = $this->instanceid;
            $drop->bot_id = $this->bot_id;
            $drop->currency_id = 1;

            if (!$drop->save()) {
                return response()->json(['success' => false, 'error' => 'server_error']);
            }
            DB::commit();


            $user->balance()
                ->where('currency_id',1)
                ->where('type', 0)
                ->update(['summ'=>DB::raw("summ - $price")]);


            $ret['drop_id'] = $drop->id;

            //Profit в копейках
            //$user->profit = $user->profit + ($drop->price - $case->priceDefault->price*100);
            // $user->count_drops++;
            //$user->save();

            //$this->updateCounters($weapon);
            // $drop->complete();
            DB::table('dota_available_products')->where('id', '=', $id)->delete();

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();

            $this->moneyBack($user, $price);

            $ret['notify']  = true;
            $ret['success'] = false;
            return response()->json($ret);
        }

        //$drop->price = Helper::currentPrice($drop->price);
        // $product     = Product::find($item->product_id);
        //$drop->image = Helper::caseImageUrl($product);
        //$drop->win_number = $win_number;
        //$drop->hash = $hash;

        /* if(Lang::has('weapon.'.$drop->name)){
             $drop->name = trans('weapon.'.$drop->name);
         }*/
        //$drop->short_description = trans('weapon.'.$drop->short_description);

        $ret['win'] = $drop;

        Log::info(serialize($ret));
        return response()->json($ret);
    }

    /*  private function updateCounters($weapon)
      {
          if(!in_array($weapon->class,['common','uncommon'])){
              $counter = Counter::where('class',$weapon->class)->first();
              $counter->count++;
              $counter->save();
          }

          if((bool)$weapon->stattrak){
              $counter = Counter::where('class','stattrak')->first();
              $counter->count++;
              $counter->save();
          }
      }*/

    private function moneyBack($user, $amount)
    {
        $user->balance()
            ->where('currency_id',1)
            ->where('type', 0)
            ->update(['summ'=>DB::raw("summ + $amount")]);

        //Helper::operation($amount, Session::get('currency_id'), 'money back');
    }
}
