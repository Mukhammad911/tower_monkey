<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class DotaAvailableProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('dota_available_products')
            ->select(
                'id',
                'stattrak',
                'class',
                'price',
                'market_name',
                'image_hash',
                'bot_id'
            );

        return Datatables::of($products)
            ->editColumn('market_name', function($product){
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/'.$product->image_hash.'/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$product->market_name.'</strong';
            })
            ->editColumn('stattrak', function ($scenario){
                if($scenario->stattrak){
                    return '<span style="color: #ff6000; font-weight: bold;">StatTrak™</span>';
                }
                return '-';
            })
            ->editColumn('bot_id', function($product){
                return '<a href="'.url(Config::get('app.admin_prefix')."/dota_bots/$product->bot_id").'">'.$product->bot_id.'</a>';
            })
            ->editColumn('price', function($product){
                return '<span class="label label-success"><i class="icon-price-tags"></i> '.($product->price/100).' RUB</span>';
            })
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.dota_available_products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getDotaIndex()
    {
        return view('admin.edit_price_products.dota');
    }
    public function getDotaAllProduct()
    {
        $products = DB::select("SELECT * FROM `dota_available_products`");
        return $products;
    }

    public function saveDotaAllProduct(Request $request)
    {

        $product_price = $request->input('price');
        $product_id = $request->input('id');

        DB::table('dota_available_products')
            ->where('id', $product_id)
            ->update(['price' => $product_price]);


    }
    public function getTrades(){
        return view('admin.trades.dota');
    }

    public function getTradesProducts(){
        $query = "select dm.id as id,u.username as username,u.steamid as steamid,dm.market_name as market_name,dm.image_hash as image_hash,dm.price as price,dm.class as class,dm.created_at as created_at,dm.updated_at as updated_at,dm.bot_id as bot_id from dota_drops_market dm, users u where dm.user_id = u.id and dm.status='accepted'";
        $products = DB::select($query);
        $pr = collect($products);
        return Datatables::of($pr)
            ->editColumn('market_name', function ($product) {
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' . $product->image_hash . '/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $product->market_name . '</strong';
            })
            ->editColumn('bot_id', function ($product) {
                return '<a href="' . url(Config::get('app.admin_prefix') . "/bots/$product->bot_id") . '">' . $product->bot_id . '</a>';
            })
            ->editColumn('price', function ($product) {
                return '<span class="label label-success"><i class="icon-price-tags"></i> ' . ($product->price / 100) . ' RUB</span>';
            })
            ->make(true);
    }
}
