<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use Illuminate\Http\Request;

use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $orders = DB::table('orders as order')
            ->select(
	            'order.id',
	            'order.user_id',
	            'order.transaction_id',
	            'order.currency_id',
	            'order.amount',
	            'order.status',
	            'order.updated_at',

	            'user.username as username',
	            'user.avatar as avatar',
	            'user.steamid as steamid'
            )
		    ->leftJoin('users as user', 'user.id', '=', 'order.user_id');
	    $res = Datatables::of($orders)
		    ->editColumn('username', function ($order) {
			    return "<a href='/profile/{$order->steamid}' target='_blank'>
						<img src='{$order->avatar}' style='height:40px; border-radius:40px;'> {$order->username}
						</a>";
		    })
		    ->editColumn('amount', function($order) {
			    return '<span class="label label-success"><i class="fa '.Currency::find($order->currency_id)->class.'"></i> ' . $order->amount.'</span>';
		    })
		    ->editColumn('status', function($order){
			    switch($order->status){
				    case 'pending':
					    return '<span class="label label-default">'.$order->status.'</span>';
					    break;

				    case 'complete':
					    return '<span class="label label-success">'.$order->status.'</span>';
					    break;

				    case 'refunded':
					case 'partial_refunded':
					    return '<span class="label label-danger">'.$order->status.'</span>';
					    break;

				    default:
					    return '<span class="label label-info">'.$order->status.'</span>';
					    break;
			    }
		    })
			->make(true);
	    return $res;
    }

	public function getOrders()
	{
		return view('admin.orders.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
