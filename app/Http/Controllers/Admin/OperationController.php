<?php

namespace App\Http\Controllers\Admin;

use App\Models\Balance;
use App\Models\Currency;
use App\Models\Operation;
use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Yajra\Datatables\Facades\Datatables;

class OperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $operations = DB::table('operations as operation')
            ->select(
	            'operation.*',
	            'user.username',
	            'user.avatar',
	            'user.id as user_id'
            )
            ->leftJoin('users as user', 'user.id', '=', 'operation.user_id');
	    $res = Datatables::of($operations)
		    ->editColumn('username', function ($operation) {
			    return "<a href='/profile/{$operation->user_id}' target='_blank'>
						<img src='{$operation->avatar}' style='height:40px; border-radius:40px;'> {$operation->username}
						</a>";
		    })
		    ->editColumn('balance', function ($operation) {
			    return '<i class="fa '.Currency::find($operation->currency_id)->class.'"></i> '.number_format($operation->balance, 2);
		    })
		    ->editColumn('amount', function ($operation) {
			    $status = 'success';
			    if($operation->amount <= 0){
				    $status = 'danger';
			    }
			    return '<span class="label label-'.$status.'"><i class="fa '.Currency::find($operation->currency_id)->class.'"></i> '.number_format($operation->amount, 2).'</span>';
		    })
		    ->editColumn('description', function ($operation) {
			    return '<span class="label label-default">'.$operation->description.'</span>';
		    })
            ->make(true);
	    return $res;
    }

	public function getIndex()
	{
		return view('admin.operations.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.operations.create', [
		    'currencies' => Currency::where('status', 1)->get(),
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    DB::beginTransaction();

	    $Balance = Balance::firstOrNew([
		    'user_id' => $request->input('user_id'),
		    'currency_id' => $request->input('currency_id'),
		    'type' => 0,
	    ]);
	    $Balance->summ += $request->input('amount');

	    $total_balance = Balance::where('user_id', $request->input('user_id'))
	                            ->where('currency_id', $request->input('currency_id'))
	                            ->sum('summ') + $request->input('amount');

        $Operation = new Operation();
	    $Operation->user_id = $request->input('user_id');
	    $Operation->amount = $request->input('amount');
	    $Operation->balance = $total_balance;
	    $Operation->currency_id = $request->input('currency_id');
	    $Operation->description = 'created by moderator';

	    if(!$Operation->save()){
		    DB::rollBack();
		    return redirect(Config::get('app.admin_prefix').'/operations')->with('warning', 'Error in saving new operation.');
	    }
	    if(!$Balance->save()){
		    DB::rollBack();
		    return redirect(Config::get('app.admin_prefix').'/operations')->with('warning', 'Error in updating balance.');
	    }

	    DB::commit();

	    $event_data = [
		    'user_id' => $request->input('user_id'),
		    'balance' => number_format($total_balance, 2, '.', ''),
	    ];
	    event(new \App\Events\RefreshBalance($event_data));

	    return redirect(Config::get('app.admin_prefix').'/operations')->with('success', 'New operation has been successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
