<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

	/**
	 *
	 */
    public function __construct()
    {
        //$this->middleware('level:2');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function getIndex()
    {
        return view('admin/home');
    }

	public function loginLikeUser($user_id)
	{
		if(Auth::check() && Auth::user()->level() == 2) {
			Auth::logout();
			$user = User::where( 'id', $user_id )->first();
			Auth::login( $user, true );
		}

		return redirect('/');
	}
}
