<?php

namespace App\Http\Controllers\Admin\PubgCases;

use App\Http\Requests\CaseRequest;
use App\Models\Category;
use App\Models\Currency;
use App\Models\PubgCases\PubgCase;
use App\Models\PubgCases\PubgCaseSettings;
use App\Models\PubgProduct;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Intervention\Image\Facades\Image;
use Datatables;
use Helper;
use Config;
use DB;

class PubgCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cases = DB::table('pubg_cases as cs')
            ->select('cs.id',
                'cs.name',
                'cs.image',
                'cs.class',
                DB::raw("c.name as category"),
                'cs.sort',
                'cs.active')
            ->leftJoin('categories as c','c.id','=','cs.category_id');
        //->orderBy('cs.sort');

        return Datatables::of($cases)
            ->addColumn('action', function ($case) {
                $btns = '<ul class="icons-list">
								<li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/pubg_case/$case->id/products").'" data-original-title="Products"><i class="icon-list"></i></a></li>
								<li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/pubg_case/$case->id/purchases").'" data-original-title="Products"><i class="icon-basket position-right"></i></a></li>
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortUp('.$case->id.')" data-original-title="Up"><i class="icon-arrow-up16 position-right"></i></a></li>
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortDown('.$case->id.')" data-original-title="Down"><i class="icon-arrow-down16 position-right"></i></a></li>
                                <li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/pubg_case/$case->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>';
                if(!$case->class){
                    $btns.='<li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteCase('.$case->id.')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a></li>';
                }

                $btns.='</ul>';

                return $btns;
            })
            ->editColumn('image', function($case){
                return '<img style="max-height:40px" src="'.asset("images/cases/$case->image").'">';
            })
            ->editColumn('price', function($case){
                $prices = DB::table('currency as cur')
                    ->select('cur.code','cur.symbol_left', 'cp.price as item')
                    ->join('pubg_case_price as cp', 'cur.id','=','cp.currency_id')
                    ->where('cp.case_id', $case->id)
                    ->get();
                $html = '';
                foreach ($prices as $price){
                    $html.='<label class="label label-info">'."$price->code-$price->item".'</label>';
                }
                return $html;

            })
            ->editColumn('active', function($case){
                if($case->active == 1){
                    $data = 'yes';
                    return '<label class="label label-success">'.$data.'</label>';
                }
                $data = 'no';
                return '<label class="label label-danger">'.$data.'</label>';

            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(PubgCase $pubg_case)
    {
        $categories = Category::all();
        $currency = Currency::where('status',1)->get();
        return view('admin.pubg_cases.create',['categories'=>$categories, 'currency'=>$currency])->with('case',$pubg_case);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PubgCase $pubg_case, CaseRequest $request)
    {
        $pubg_case->name = $request->input('name');
        $pubg_case->description = $request->input('description');
        $pubg_case->category_id = $request->input('category_id');
        $pubg_case->active 	= ($request->input('active') == 'on') ? 1 : 0;
        $pubg_case->mark 	= $request->input('mark');
        $pubg_case->title 	= $request->input('title');

        if($request->hasFile('image')){
            $file 		= $request->file('image');
            $image 	= $file->getClientOriginalName();
            $file->move(Config::get('app.upload_path').'cases', $image);

            Image::make(public_path('images/cases/'.$image))->resize(null, 150,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/cases/thumbnails/'.$image));

            $pubg_case->image = $image;
        }

        $lastSort = PubgCase::orderBy('sort','desc')
            ->take(1)
            ->pluck('sort');
        $pubg_case->sort = ++$lastSort;

        $pubg_case->save();

        PubgCase::attachPrice($request, $pubg_case);

        return redirect(Config::get('app.admin_prefix').'/pubg/cases')->with('success', 'Successfully created case!');
    }

    public function postAttachProducts(Request $request)
    {
        $case = PubgCase::find($request->input('case_id'));

        if($request->input('attach')){
            $lastSort = $case->products()
                ->orderBy('sort','desc')
                ->take(1)
                ->pluck('sort');

            $case->products()->attach($request->input('product_id'),['sort' => ++$lastSort]);
        }
        else{
            $case->products()->detach($request->input('product_id'));
        }

        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param PubgCase $case
     *
     * @return $this
     */
    public function edit(PubgCase $pubg_case)
    {
        $categories = Category::all();
        $currency = Currency::where('status',1)->get();
        $case_price = DB::table('currency as cur')
            ->select('cur.code','cur.symbol_left', 'cp.price as price')
            ->join('pubg_case_price as cp', 'cur.id','=','cp.currency_id')
            ->where('cp.case_id', $pubg_case->id)
            ->get();
        $prices = [];

        foreach ($case_price as $item){
            $prices[$item->code] = $item->price;
        }

        return view('admin.pubg_cases.edit',[
            'categories'=>$categories,
            'currency'=>$currency,
            'prices'=>$prices,
            'weapons' => PubgProduct::select('name')->groupBy('name')->get(),
        ])->with('case',$pubg_case);
    }

    /**
     * @param SteamCase $case
     * @param CaseRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PubgCase $pubg_case, CaseRequest $request)
    {
        $pubg_case->name = $request->input('name');
        $pubg_case->description = $request->input('description');
        $pubg_case->category_id = $request->input('category_id');
        $pubg_case->active 	= ($request->input('active') == 'on') ? 1 : 0;
        $pubg_case->mark 	= $request->input('mark');
        $pubg_case->title 	= $request->input('title');

        if($request->hasFile('image')){
            $file 		= $request->file('image');
            $image 	= $file->getClientOriginalName();
            $file->move(Config::get('app.upload_path').'cases', $image);

            Image::make(public_path('images/cases/'.$image))->resize(null, 150,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('images/cases/thumbnails/'.$image));

            $pubg_case->image = $image;
        }

        $pubg_case->save();

        $pubg_case->currency()->detach();

        PubgCase::attachPrice($request, $pubg_case);

        return redirect(Config::get('app.admin_prefix').'/pubg/cases')->with('success', 'Successfully updated case!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $case = PubgCase::find($id);
        PubgCase::find($case->id)->products()->detach();
        $ret = $case->delete();
        return response()->json($ret);
    }

    public function getCases()
    {
        return view('admin.pubg_cases.index');
    }


    public function getSortUp($id)
    {
        $case = PubgCase::find($id);

        $previous = PubgCase::orderBy('sort','desc')
            ->where('sort','<',$case->sort)
            ->take(1)
            ->first();

        if(!is_null($previous)){
            $prevCase = PubgCase::find($previous->id);
            $prevCase->sort = $case->sort;
            $prevCase->save();

            $case->sort = $previous->sort;
            $case->save();
        }

        return response()->json(true);
    }

    public function getSortDown($id)
    {
        $case = PubgCase::find($id);

        $next = PubgCase::orderBy('sort')
            ->where('sort','>',$case->sort)
            ->take(1)
            ->first();

        if(!is_null($next)){
            $nextCase = PubgCase::find($next->id);
            $nextCase->sort = $case->sort;
            $nextCase->save();

            $case->sort = $next->sort;
            $case->save();
        }

        return response()->json(true);
    }

    public function getCaseAutocomplete(Request $request)
    {
        $cases = DB::table('pubg_cases');
        $term = $request->input('term');
        $exept = $request->input('exept');

        if($term) {
            $cases = $cases->where('name', 'LIKE', "%$term%");
        }
        if($exept){
            $cases = $cases->where('id','!=', $exept);
        }
        $cases = $cases->take(10)->get();

        return response()->json($cases);
    }

    public function postChangePositions(Request $request)
    {
        $case1 = PubgCase::find($request->input('first'));
        $case2 = PubgCase::find($request->input('second'));

        $case1Clone = clone $case1;
        $case2Clone = clone $case2;

        $case1->sort = $case2Clone->sort;
        $case2->sort = $case1Clone->sort;

        return response()->json($case1->save() && $case2->save());
    }

    public function getLoadProductsList($case_id = null)
    {
        $case_product = [];

        $products = PubgProduct::orderBy('class')
            ->get();

        if(!is_null($case_id)){
            $case_product = PubgCase::find($case_id)->products->lists('id')->toArray();
        }

        return view('admin.products._partials.product-list',['products'=>$products, 'case_product'=>$case_product]);
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return mixed
     */
    public function getProductsList($id, Request $request)
    {
        $case_products = PubgCase::find($id)->products->lists('id')->toArray();
        $products = DB::table('pubg_products as p')->select('p.*')->orderBy('p.name','asc');

        return Datatables::of($products)
            ->addColumn('check', function ($product) use ($case_products) {
                $checked = (in_array($product->id, $case_products)) ? 'checked' : '';

                return "<input data-popup=\"tooltip\" data-original-title=\"Add/Remove\" class=\"styled weapon-item\" $checked type=\"checkbox\" value=\"$product->id\">";
            })
            ->editColumn('image', function($product){
                $source = ($product->image_source == 'url') ? $product->image : asset("images/pubg_images/$product->image");
                return '<img style="max-height:40px" src="'.$source.'">';
            })
            ->editColumn('name', function($product){
                $name = (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name;

                return "<label class=\"text-bold\">$name</label>";
            })
            ->setRowClass(function () {
                return 'pubg_admin_row_product_case';
            })
            ->filter(function ($query) use ($request) {
                if ($request->has('product_name')) {
                    $term = $request->input('product_name');

                    $query->where('p.name','like',"$term%");
                }
                if ($request->has('product_description')) {
                    $term = $request->input('product_description');

                    $query->where('p.short_description','like',"$term%");
                }
            })
            ->make(true);
    }

    public function getProducts($id)
    {
        $case = PubgCase::find($id);
        return view('admin.pubg_cases.case-products', [
            'case' => $case,

        ]);
    }

    public function getCaseProducts(Request $request)
    {
        $case_id = $request->input('case_id');
        $products = PubgCase::find($case_id)->products()->orderBy('pubg_case_product.sort')->get();

        return Datatables::of($products)
            ->addColumn('action', function ($product) use ($case_id) {
                return '<ul class="icons-list">
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortUp('.$product->id.')" data-original-title="Up"><i class="icon-arrow-up16 position-right"></i> Up</a></li>
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortDown('.$product->id.')" data-original-title="Down"><i class="icon-arrow-down16 position-right"></i> Down</a></li>
                                <li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteFromCase('.$product->id.','.$case_id.')" data-original-title="Delete"><i class="icon-basket"></i> Delete</a></li>
                            </ul>';
            })
            ->editColumn('image', function($product){
                $source = ($product->image_source == 'url') ? $product->image : asset("images/pubg_images/$product->image");
                return '<img style="max-height:40px" src="'.$source.'">';
            })
            ->editColumn('name', function($product){
                $name = (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name;
                return '<label class="text-bold">'.$name.'</label>';
            })
            ->editColumn('short_description', function($product){
                $descr = (Lang::has('weapon.'.$product->short_description)) ? trans('weapon.'.$product->short_description) : $product->short_description;
                return '<label class="text-bold">'.$descr.'</label>';
            })
            ->editColumn('price', function($product){
                return '<label class="label label-danger">'.$product->price.'</label>';
            })
            ->editColumn('class', function($product){
                return '<label class="label '.$product->class.'">'.$product->class.'</label>';
            })
            ->make(true);
    }

    public function getDeleteProductCase(Request $request){
        PubgCase::find($request->input('case_id'))->products()->detach($request->input('id'));
        return response()->json(true);
    }


    public function getAvailableProducts(PubgCase $pubg_case)
    {
        $targetProfit = PubgCaseSettings::where('key', 'profit')->first()->value;
        $products = $pubg_case->products;
        $price = $pubg_case->getPubgPriceDefaultAttribute()->price;
        $caseLowPrice = $price*(100-$targetProfit);
        $caseMidPrice = $price*200;
        $totalLow = 0;
        $totalMid = 0;
        $totalHigh = 0;

        foreach ($products as $product) {
            if($product->price < $caseLowPrice) {
                $totalLow += count($product->pubgCaseAvailableProducts);
            } else if($product->price < $caseMidPrice) {
                $totalMid += count($product->pubgCaseAvailableProducts);
            } else {
                $totalHigh += count($product->pubgCaseAvailableProducts);
            }
        }



        return view('admin.pubg_cases.available-products', [
            'case' => $pubg_case,
            'caseLowPrice'=>$caseLowPrice,
            'caseMidPrice'=>$caseMidPrice,
            'totalLow' => $totalLow,
            'totalMid' => $totalMid,
            'totalHigh' => $totalHigh,
            'products' => $products]);
    }
}
