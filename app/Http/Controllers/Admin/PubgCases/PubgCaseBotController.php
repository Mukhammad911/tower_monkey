<?php

namespace App\Http\Controllers\Admin\PubgCases;

use App\Http\Requests\BotRequest;
use App\Models\PubgCases\PubgCaseBot;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class PubgCaseBotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $bots = PubgCaseBot::all();
        return view('admin.pubg_case_bots.index')->with('bots', $bots);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pubg_case_bots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BotRequest $request
     * @return Response
     */
    public function store(BotRequest $request)
    {
        PubgCaseBot::create($request->all());
        return redirect(Config::get('app.admin_prefix').'/pubg_case_bots')->with('success', 'Successfully added new bot!');
    }

    /**
     * Display the specified resource.
     *
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function show(PubgCaseBot $bot)
    {
        return view('admin.pubg_case_bots.show')->with('bot', $bot);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function edit(PubgCaseBot $bot)
    {
        return view('admin.pubg_case_bots.edit')->with('bot', $bot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BotRequest $request
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function update(BotRequest $request, PubgCaseBot $bot)
    {
        $bot->update($request->except(['steam_login']));
        return redirect(Config::get('app.admin_prefix').'/pubg_case_bots')->with('success', 'Successfully update bot '.$bot->steam_login.'!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bot $bot
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(PubgCaseBot $bot)
    {
        if($bot->enabled) Redis::publish(Config::get('app.redis_prefix').'pubg-case-disable-bot', json_encode(['botId' => $bot->id]));
        $bot->delete();

        return redirect(Config::get('app.admin_prefix').'/pubg_case_bots')->with('success', 'Successfully deleted bot!');
    }

    /**
     * @param Bot $bot
     * @return string
     */
    public function getAuthCode(PubgCaseBot $bot) {
        return $bot->getCode();
    }

    /**
     * Enable bot
     * @param Bot $bot
     * @return string
     */
    public function getEnable(Request $request, PubgCaseBot $bot) {
        if($bot->secret) {
            $bot->enabled = 1;
            $bot->save();
            Log::info('enabe');
            Redis::publish(Config::get('app.redis_prefix').'pubg-case-enable-bot', json_encode(['botId' => $bot->id]));
            return ($request->ajax()) ? json_encode(['status' => 'success']) : "success";
        }

        return ($request->ajax()) ? json_encode(['status' => 'failed', 'message' => 'Account wasn\'t activated.']) : "failed";
    }

    /**
     * Disable bot
     * @param Bot $bot
     * @return string
     */
    public function getDisable(PubgCaseBot $bot) {
        $bot->enabled = 0;
        $bot->save();

        Redis::publish(Config::get('app.redis_prefix').'disable-pubg-case-bot', json_encode(['botId' => $bot->id]));

        return "success";
    }
}
