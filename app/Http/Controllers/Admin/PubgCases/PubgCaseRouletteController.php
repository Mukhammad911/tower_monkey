<?php

namespace App\Http\Controllers\Admin\PubgCases;

use Statistic;
use App\Models\Currency;
use App\Models\PubgCases\PubgCaseSettings;
use App\Models\Operation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;

class PubgCaseRouletteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = DB::table('pubg_case_drops_market')
            ->select('*');

        $res = Datatables::of($entities)
            ->make(true);
        return $res;
    }

    public function getIndex()
    {
        return view('admin.pubg-case-roulettes.index');
    }

    public function statistics()
    {

        $total_income = $this->income();
        $total_expense = $this->expense();
        $target_company_percent = PubgCaseSettings::where('key', 'profit')->first()->value;
        $current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
        $profit_deviation_percent = abs(100 - $current_company_percent / ($target_company_percent / 100));

        return view('admin.pubg-case-roulettes.statistics',[
            'total_income' => $total_income,
            'total_expense' => $total_expense,
            'target_company_percent' => $target_company_percent,
            'current_company_percent' => $current_company_percent,
            'profit_deviation_percent' => $profit_deviation_percent,

            'income_data' => \App\Models\Statistic::getChartData('roulette', 'income'),
            'expense_data' => \App\Models\Statistic::getChartData('roulette', 'expense'),
            'profit_deviation_percent_data' => \App\Models\Statistic::getChartData('roulette', 'profit_deviation_percent'),
            'current_profit_percent_data' => \App\Models\Statistic::getChartData('roulette', 'current_profit'),
        ]);
    }
    public function resetStatistics(){
        Operation::where('description', '=', 'pubg case opening')->delete();

        Operation::where('description', '=', 'pubg case win')->delete();

        Operation::where('description', '=', 'pubg case sell')->delete();

        Operation::where('description', '=', 'money back pubg case')->delete();

        Operation::where('description', '=', 'pubg case opening from promo')->delete();

        return redirect('spidercontrolpanel/pubg-case-roulette-statistics');
    }

    public function resetStatisticsYoutubers()
    {
        Operation::where('description', '=', 'pubg case opening youtube')->delete();

        Operation::where('description', '=', 'pubg case win youtube')->delete();

        Operation::where('description', '=', 'pubg case sell')->delete();

        Operation::where('description', '=', 'money back pubg case youtube')->delete();

        return redirect('spidercontrolpanel/pubg-case-roulette-statistics');
    }

    private function income(){
        $total_income = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $income_roulette_opening = Operation::where('description', 'pubg case opening');
            $total_income_opening = $income_roulette_opening->sum('amount');

            $income_roulette_opening_promo = Operation::where('description', 'pubg case opening from promo');
            $total_income_opening_promo = $income_roulette_opening_promo->sum('amount');

            $income_roulette_opening_youtuber = Operation::where('description', 'pubg case opening youtube');
            $total_income_opening_youtuber = $income_roulette_opening_youtuber->sum('amount');

            $moneyback_roulette_opening = Operation::where('description', 'money back pubg case');
            $total_moneyback_opening = $moneyback_roulette_opening->sum('amount');

            $moneyback_roulette_opening_youtuber = Operation::where('description', 'money back pubg case youtube');
            $total_moneyback_opening_youtuber = $moneyback_roulette_opening_youtuber->sum('amount');

            $total_income = $total_income_opening + $total_income_opening_promo + $total_income_opening_youtuber + $total_moneyback_opening + $total_moneyback_opening_youtuber;
        }

        Log::info('Total income : '.$total_income);

        return -$total_income;
    }

    private function expense(){
        $total_expense = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $expense_roulette = Operation::where('description', 'pubg case win');
            $expense_roulette_win = $expense_roulette->sum('amount');

            $expense_roulette_youtuber = Operation::where('description', 'pubg case win youtube');
            $expense_roulette_win_youtuber = $expense_roulette_youtuber->sum('amount');

            $total_expense = $expense_roulette_win + $expense_roulette_win_youtuber;
        }

        Log::info('Total expense : '.$total_expense);
        return $total_expense;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pubg-case-roulettes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.pubg-case-roulettes.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
