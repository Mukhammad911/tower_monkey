<?php

namespace App\Http\Controllers\Admin;

use App\Models\Affiliate;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class AffiliateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $affiliates = DB::table('affiliates as affiliate')
            ->select(
	            'affiliate.id as affiliate_id',
                'affiliate.user_id',
                'affiliate.refer_id',

                'user.username as user_username',
                'user.steamid as user_steamid',
                'user.avatar as user_avatar',

                'refer.username as refer_username',
                'refer.steamid as refer_steamid',
                'refer.avatar as refer_avatar'
            )
		    ->leftJoin('users as user', 'user.id', '=', 'affiliate.user_id')
		    ->leftJoin('users as refer', 'refer.id', '=', 'affiliate.refer_id');

	    return Datatables::of($affiliates)
		    ->editColumn('user_username', function ($affiliates) {
			    return "<a href='/profile/{$affiliates->user_steamid}' target='_blank'><img src='{$affiliates->user_avatar}' style='height:40px; border-radius:40px;'> {$affiliates->user_username}</a>";
		    })
		    ->editColumn('refer_username', function ($affiliates) {
			    return "<a href='/profile/{$affiliates->refer_steamid}' target='_blank'><img src='{$affiliates->refer_avatar}' style='height:40px; border-radius:40px;'> {$affiliates->refer_username}</a>";
		    })
             ->addColumn('action', function ($affiliate) {
                 return '<ul class="icons-list">
							<li>
								<a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/affiliate/$affiliate->affiliate_id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a>
							</li>
						    <li>
						    <a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteAffiliate('.$affiliate->affiliate_id.')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a>
						    </li>
						</ul>';
             })
             ->make(true);
    }

	/**
	 *
	 */
	public function getIndex()
	{
		return view('admin.affiliates.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.affiliates.create');
    }

	/**
	 * @param Affiliate $affiliate
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function store(Affiliate $affiliate, Request $request)
    {
	    $user_id = $request->input('user_id');
	    $refer_id = $request->input('refer_id');
        if(Affiliate::where('user_id', $user_id)
            ->where('refer_id', $refer_id)
            ->exists()){
	        return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Affiliate already exists.');
        }
	    if(!User::find($user_id)){
		    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Affiliate doesn\'t exists.');
	    }
	    if(!User::find($refer_id)){
		    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Refer doesn\'t exists.');
	    }

	    $affiliate->user_id = $user_id;
	    $affiliate->refer_id = $refer_id;
	    $affiliate->save();

	    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('success', 'Affiliate successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * @param Affiliate $affiliate
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function edit(Affiliate $affiliate)
    {
	    return view('admin.affiliates.edit', [
		    'affiliate' => $affiliate,
		    'user' => User::find($affiliate->user_id),
		    'refer' => User::find($affiliate->refer_id),
	    ]);
    }

	/**
	 * @param Affiliate $affiliate
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(Affiliate $affiliate, Request $request)
    {
	    $user_id = $request->input('user_id');
	    $refer_id = $request->input('refer_id');
	    if(Affiliate::where('user_id', $user_id)
	                ->where('refer_id', $refer_id)
	                ->exists()){
		    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Affiliate already exists.');
	    }
	    if(!User::find($user_id)){
		    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Affiliate doesn\'t exists.');
	    }
	    if(!User::find($refer_id)){
		    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('warning', 'Refer doesn\'t exists.');
	    }

	    $affiliate->user_id = $user_id;
	    $affiliate->refer_id = $refer_id;
	    $affiliate->save();

	    return redirect(Config::get('app.admin_prefix').'/affiliates')->with('success', 'Affiliate successfully created.');
    }

	/**
	 * @param Affiliate $affiliate
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
    public function destroy(Affiliate $affiliate)
    {
	    $res = $affiliate->delete();

	    return response()->json($res);
    }

	/**
	 * @param $promo_code
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function promoCode($promo_code)
	{
		if($refer = User::where('id', $promo_code)
			->orWhere('promo_code', $promo_code)
			->first()){

			return redirect('/')->withCookie('refer_id', $refer->id, 10080);
		}

		return redirect('/');
	}
}
