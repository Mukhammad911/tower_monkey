<?php

namespace App\Http\Controllers\Admin;

use App\CustomClasses\TowerGame;
use App\Models\Operation;
use App\Models\TowerAlgorithm;
use App\Models\TowerSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;

use DB;
use Config;
use Helper;
use Statistic;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

/**
 * Class TowerController
 * @package App\Http\Controllers
 */
class TowerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$settings = DB::table('towers')
		              ->select('*');

		$res = Datatables::of($settings)
		                 ->make(true);
		return $res;
	}

	public function getIndex()
	{
		return view('admin.towers.index');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function statistics()
	{
		$total_income = TowerGame::income();
		$total_expense = TowerGame::expense();
		$target_company_percent = TowerSetting::where('key', 'company percent')->first()->value;
		$current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
		$profit_deviation_percent = abs(100 - $current_company_percent / ($target_company_percent / 100));

		return view('admin.towers.statistics',[
			'total_income' => $total_income,
			'total_expense' => $total_expense,
			'target_company_percent' => $target_company_percent,
			'current_company_percent' => $current_company_percent,
			'profit_deviation_percent' => $profit_deviation_percent,

			'income_data' => \App\Models\Statistic::getChartData('upgrade', 'income'),
			'expense_data' => \App\Models\Statistic::getChartData('upgrade', 'expense'),
			'profit_deviation_percent_data' => \App\Models\Statistic::getChartData('upgrade', 'profit_deviation_percent'),
			'current_profit_percent_data' => \App\Models\Statistic::getChartData('upgrade', 'current_profit'),
		]);
	}

    public function resetStatistics(){
        Operation::where('description', 'tower bet')->delete();

        Operation::where('description', 'csgo case opening')->delete();
        Operation::where('description', 'csgo case opening youtube')->delete();
        Operation::where('description', 'money back csgo case')->delete();
        Operation::where('description', 'money back csgo case youtube')->delete();

        Operation::where('description', 'pubg case opening')->delete();
        Operation::where('description', 'pubg case opening youtube')->delete();
        Operation::where('description', 'money back pubg case')->delete();
        Operation::where('description', 'money back pubg case youtube')->delete();


        Operation::where('description', 'tower win')->delete();

        Operation::where('description', 'csgo case win')->delete();
        Operation::where('description', 'csgo case win youtube')->delete();

        Operation::where('description', 'pubg case win')->delete();
        Operation::where('description', 'pubg case win youtube')->delete();

        return redirect('controlbeast/tower-statistics');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param TowerSetting $TowerSetting
	 *
	 * @return $this
	 */
	public function edit(TowerSetting $TowerSetting)
	{

	}

	/**
	 * @param TowerSetting $TowerSetting
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(TowerSetting $TowerSetting, Request $request)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
}
