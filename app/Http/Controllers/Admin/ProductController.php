<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\SteamCase;
use Config;
use Datatables;
use App\Http\Requests\ProductRequest;
use Lang;

class ProductController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function index(Request $request)
	{
		$products = DB::table('products')->select('products.*')->orderBy('products.name','asc');

		return Datatables::of($products)
		                 ->editColumn('image', function($product){
			                 return ($product->image_source == 'url') ? $product->image : asset("images/products/$product->image");
		                 })
		                 ->editColumn('name', function($product){
			                 return (Lang::has('weapon.'.$product->name)) ? trans('weapon.'.$product->name) : $product->name;
		                 })
		                 ->editColumn('short_description', function($product){
			                 return (Lang::has('weapon.'.$product->short_description)) ? trans('weapon.'.$product->short_description) : $product->short_description;
		                 })
		                 ->filter(function ($query) use ($request) {
			                 if ($request->has('case_id')) {
				                 $query->leftJoin('case_product', 'case_product.product_id', '=', 'products.id');
				                 $query->where('case_product.case_id', $request->input('case_id'));
			                 }

			                 if($request->has('class')){
				                 $query->where('products.class', $request->input('class'));
			                 }

			                 if ($request->has('id')) {
				                 $query->where('products.id', $request->input('id'));
			                 }

			                 if ($request->has('min')) {
				                 $query->where('products.price', '>=' , $request->input('min')*100);
			                 }

			                 if ($request->has('max')) {
				                 $query->where('products.price', '<=' , $request->input('max')*100);
			                 }
		                 })
		                 ->make(true);
	}

	/**
	 * @param Product $product
	 *
	 * @return $this
	 */
	public function create(Product $product)
	{
		return view('admin.products.create',[
			'classList'=>Product::$classList
		])->with('product',$product);
	}

	/**
	 * @param Product $product
	 * @param ProductRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Product $product,ProductRequest $request)
	{
		$product->name = $request->input('name');
		$product->short_description = $request->input('short_description');
		$product->class = $request->input('class');
		$product->image = $request->input('image');
		$product->price = $request->input('price')*100;
		$product->image_source = $request->input('image_source');

		if($request->hasFile('image')){
			$file 		= $request->file('image');
			$image 	= $file->getClientOriginalName();
			$file->move(Config::get('app.upload_path').'products', $image);
			$product->image = $image;
		}

		$product->save();

		return redirect(Config::get('app.admin_prefix').'/products')->with('success', 'Successfully created product!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param Product $product
	 *
	 * @return $this
	 */
	public function edit(Product $product)
	{
		return view('admin.products.edit',[
			'weapons' => Product::select('name', 'short_description as desc')->get(),
			'classList' => Product::$classList
		])->with('product',$product);
	}

	/**
	 * @param Product $product
	 * @param ProductRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Product $product, ProductRequest $request)
	{
		$product->name = $request->input('name');
		$product->short_description = $request->input('short_description');
		$product->class = $request->input('class');
		$product->locked = ($request->input('locked') == 'on') ? 1 : 0;

		if($request->input('image')){
			$product->image = $request->input('image');
		}
		$product->price = $request->input('price')*100;

		if($request->input('image_source') == 'url'){
			$product->image_source = 'url';
		}

		if($request->hasFile('image')){
			$file 		= $request->file('image');
			$image 	= $file->getClientOriginalName();
			$file->move(Config::get('app.upload_path').'products', $image);
			$product->image = $image;
			$product->image_source = 'file';
		}

		$product->save();

		return redirect(Config::get('app.admin_prefix').'/products')->with('success', 'Successfully updated product!');
	}

	/**
	 * @param Product $product
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function destroy(Product $product)
	{
		Product::find($product->id)->cases()->detach();
		$ret = $product->delete();
		return response()->json($ret);
	}

	public function getProducts(){

		return view('admin.products.index',['classList'=>Product::$classList]);
	}

	public function getSortUp($id, $case_id){
		$product = SteamCase::find($case_id)->products()->find($id);

		$previous = SteamCase::find($case_id)->products()
		                     ->where('sort','<',$product->pivot->sort)
		                     ->orderBy('sort','desc')
		                     ->take(1)
		                     ->first();

		if(!is_null($previous->pivot)){
			$sortPrev = $previous->pivot->sort;
			$currSort = $product->pivot->sort;

			SteamCase::find($case_id)->products()->updateExistingPivot($id, ['sort'=>$sortPrev]);
			SteamCase::find($case_id)->products()->updateExistingPivot($previous->id, ['sort'=>$currSort]);
		}

		return response()->json(true);
	}

	public function getSortDown($id, $case_id){
		$product = SteamCase::find($case_id)->products()->find($id);

		$next = SteamCase::find($case_id)->products()
		                 ->where('sort','>',$product->pivot->sort)
		                 ->take(1)
		                 ->first();

		if(!is_null($next->pivot)){
			$sortNext = $next->pivot->sort;
			$currSort = $product->pivot->sort;

			SteamCase::find($case_id)->products()->updateExistingPivot($id, ['sort'=>$sortNext]);
			SteamCase::find($case_id)->products()->updateExistingPivot($next->id, ['sort'=>$currSort]);
		}

		return response()->json(true);
	}

	public function getCaseAutocomplete(){
		$cases = SteamCase::take(10)->get();

		return response()->json($cases);
	}

	public function getProductAutocomplete(Request $request){
		$term = $request->input('term');

		$products = Product::all();

		$products = $products->filter(function ($item) use ($term){
			$name = (Lang::has('weapon.'.$item['name'])) ? trans('weapon.'.$item['name']) : $item['name'];
			return stripos($name, $term)!==false ||
			       stripos(trans('weapon.'.$item['short_description']), $term)!==false;
		});

		return response()->json($products);
	}
}
