<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

class ImbalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $users = DB::table('users as user')
            ->select(
	            'user.id',
                'user.steamid',
                'user.username',
                'user.avatar',
	            DB::raw('(SELECT sum(operations.amount) FROM operations WHERE operations.user_id = user.id) as operations_total'),
	            DB::raw('(SELECT sum(balance.summ) FROM balance WHERE balance.user_id = user.id AND balance.currency_id = 1) as balance_total'),
	            DB::raw('((SELECT sum(balance.summ) FROM balance WHERE balance.user_id = user.id AND balance.currency_id = 1) - (SELECT sum(operations.amount) FROM operations WHERE operations.user_id = user.id)) as imbalance')
            )
		    ->where('user.created_at', '>', '2017-03-26')
		    ->where(DB::raw('(SELECT sum(balance.summ) FROM balance WHERE balance.user_id = user.id AND balance.currency_id = 1)'), '!=', DB::raw('(SELECT sum(operations.amount) FROM operations WHERE operations.user_id = user.id)'))
	        ->groupBy('user.id');
	    $res = Datatables::of($users)
             ->editColumn('username', function ($user) {
                 return "<a href='/profile/{$user->steamid}' target='_blank'>
			<img src='{$user->avatar}' style='height:40px; border-radius:40px;'> {$user->username}
			</a>";
             })
		    ->editColumn('operations_total', function($user){
			    return "<span class='label label-default'>$ $user->balance_total</span> <span class='label label-danger'>≠</span> <span class='label label-default'>$ $user->operations_total</span>";
		    })
		    ->editColumn('imbalance', function($user){
			    return "<span class='label label-danger'>$ ".number_format($user->balance_total - $user->operations_total, 2)."</span>";
		    })
            ->make(true);
	    return $res;
    }

	public function getIndex()
	{
		return view('admin.imbalances.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
