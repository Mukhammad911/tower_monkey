<?php

namespace App\Http\Controllers\Admin;

use App\Models\TowerSetting;
use Illuminate\Http\Request;

use DB;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class TowerCellController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$settings = DB::table('tower_cells')
		              ->select('*');

		$res = Datatables::of($settings)
		                 ->make(true);
		return $res;
	}

	public function getIndex()
	{
		return view('admin.tower_cells.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param TowerSetting $TowerSetting
	 *
	 * @return $this
	 */
	public function edit(TowerSetting $TowerSetting)
	{

	}

	/**
	 * @param TowerSetting $TowerSetting
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(TowerSetting $TowerSetting, Request $request)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
}
