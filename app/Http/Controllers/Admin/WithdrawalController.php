<?php

namespace App\Http\Controllers\Admin;

use App\Models\Balance;
use App\Models\Withdrawal;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;

use DB;
use Datatables;
use Config;
use Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operations = DB::table('withdrawals as w')
	                    ->select(
		                    'w.*',
		                    'user.username',
		                    'user.avatar'
	                    )
	                    ->leftJoin('users as user', 'user.id', '=', 'w.user_id');
	    $res = Datatables::of($operations)
        ->editColumn('username', function ($withdrawal) {
             return "<a href='/profile/{$withdrawal->user_id}' target='_blank'>
			<img src='{$withdrawal->avatar}' style='height:40px; border-radius:40px;'> {$withdrawal->username}
			</a>";
        })
        ->editColumn('amount', function ($withdrawal) {
            $status = 'success';
            if($withdrawal->amount <= 0){
                $status = 'danger';
            }
            return '<span class="label label-'.$status.'">$'.number_format($withdrawal->amount, 2).'</span>';
        })
        ->editColumn('status', function ($withdrawal) {
	        switch($withdrawal->status){
		        case 'rejected':
			        $class = 'danger';
			        break;
		        case 'completed':
			        $class = 'success';
			        break;
		        default:
					$class = 'default';
			        break;
	        }
	        return '<span class="label label-'.$class.'">'.$withdrawal->status.'</span>';
        })
	    ->editColumn('details', function ($withdrawal) {
		    $res = '';
		    $details = unserialize($withdrawal->details);
		    foreach ( $details as $key => $detail ) {
			    $res .= '<strong>'.$key.'</strong>: '.$detail. "<br>";
		    }

		    return $res;
	    })
	    ->addColumn('actions', function($withdrawal){
		    return '<ul class="icons-list">
                    <li>
                        <a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/withdrawal/$withdrawal->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a>
                    </li>
                </ul>';
        })
        ->make(true);

		return $res;
    }

	public function getIndex()
	{
		return view('admin.withdrawals.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * @param Withdrawal $withdrawal
	 *
	 * @return $this
	 */
    public function edit(Withdrawal $withdrawal)
    {
        return view('admin.withdrawals.edit')->with('withdrawal', $withdrawal);
    }

	/**
	 * @param Request $request
	 * @param Withdrawal $withdrawal
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(Request $request, Withdrawal $withdrawal)
    {
	    $Withdrawal = Withdrawal::find($withdrawal->id);
	    $Withdrawal->status = $request->input('status');
	    if($Withdrawal->save() && $request->input('status') == 'rejected'){
		    $Balance = Balance::where('user_id', $withdrawal->user_id)
			    ->where('currency_id', 1)
			    ->where('type', 0)
			    ->first();
		    $Balance->summ += $withdrawal->amount;
		    $Balance->save();
		    Helper::operation(-$withdrawal->amount, 1, 'created by moderator', $Withdrawal->user_id);
	    }elseif($Withdrawal->save() && $request->input('status') == 'completed' && $withdrawal->status == 'rejected'){
		    $Balance = Balance::where('user_id', $withdrawal->user_id)
		                      ->where('currency_id', 1)
		                      ->where('type', 0)
		                      ->first();
		    $Balance->summ -= $withdrawal->amount;
		    $Balance->save();
		    Helper::operation($withdrawal->amount, 1, 'created by moderator', $Withdrawal->user_id);
	    }

	    return redirect(Config::get('app.admin_prefix').'/withdrawals')->with('success', 'Withdrawal has been successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
