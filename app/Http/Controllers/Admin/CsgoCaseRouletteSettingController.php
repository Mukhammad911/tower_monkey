<?php

namespace App\Http\Controllers\Admin;

use App\Models\CsgoCaseSettings;
use Illuminate\Http\Request;

use DB;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CsgoCaseRouletteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = DB::table('csgo_case_roulette_settings')
            ->select('*');

        $res = Datatables::of($settings)
            ->editColumn('value', function($setting){
                return '<span class="label label-warning">'.$setting->value.' '.$setting->symbol.'</span>';
            })
            ->editColumn('updated_at', function($setting){
                return '<span class="label label-default">'.$setting->updated_at.'</span>';
            })
            ->addColumn('action', function ($setting) {
                return '<ul class="icons-list">
                                <li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/csgo-case-roulette-setting/$setting->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
                                <li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteRouletteSetting('.$setting->id.')" data-original-title="Delete"><i class="icon-bin"></i> Delete</a></li>
                            </ul>';
            })
            ->make(true);
        return $res;
    }

    public function getIndex()
    {
        return view('admin.csgo_case_roulette_settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.csgo_case_roulette_settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|max:255',
            'value' => 'required|max:255',
            'symbol' => 'max:10',
            'description' => 'max:255',
        ]);

        $TowerSetting = new CsgoCaseSettings();
        $TowerSetting->key = $request->input('key');
        $TowerSetting->value = $request->input('value');
        $TowerSetting->symbol = $request->input('symbol');
        $TowerSetting->description = $request->input('description');
        $TowerSetting->save();

        return redirect(Config::get('app.admin_prefix').'/csgo-case-roulette-settings')->with('success', 'New upgrade setting has been successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param RouletteSetting $RouletteSetting
     *
     * @return $this
     */
    public function edit(CsgoCaseSettings $RouletteSetting)
    {
        return view('admin.csgo_case_roulette_settings.edit')->with('setting', $RouletteSetting);
    }

    /**
     * @param RouletteSetting $RouletteSetting
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CsgoCaseSettings $RouletteSetting, Request $request)
    {
        $this->validate($request, [
            'key' => 'required|max:255',
            'value' => 'required|max:255',
            'symbol' => 'max:10',
            'description' => 'max:255',
        ]);

        $RouletteSetting->key = $request->input('key');
        $RouletteSetting->value = $request->input('value');
        $RouletteSetting->symbol = $request->input('symbol');
        $RouletteSetting->description = $request->input('description');
        $RouletteSetting->save();

        return redirect(Config::get('app.admin_prefix').'/csgo-case-roulette-settings')->with('success', 'Roulette setting has been successfully updated!');
    }

    /**
     * @param RouletteSetting $RouletteSetting
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CsgoCaseSettings $RouletteSetting)
    {
        CsgoCaseSettings::destroy($RouletteSetting->id);
        return response()->json(true);
    }
}
