<?php

namespace App\Http\Controllers\Admin;

use App\CustomClasses\RouletteGame;
use App\Models\Currency;
use App\Models\Operation;
use App\Models\CsgoCaseSettings;
use App\Models\SteamCase;
use App\Models\Win2;
use Carbon\Carbon;
use Illuminate\Http\Request;

use DB;
use Helper;
use Statistic;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Auth;


/**
 * Class RouletteController
 * @package App\Http\Controllers
 */
class CsgoCaseRouletteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = DB::table('csgo_case_drops_market')
            ->select('*');

        $res = Datatables::of($entities)
            ->make(true);
        return $res;
    }

    public function getIndex()
    {
        return view('admin.csgo-case-roulettes.index');
    }



    public function statistics()
    {
        $total_income = $this->income();
        $total_expense = $this->expense();

        $target_company_percent = CsgoCaseSettings::where('key', 'profit')->first()->value;
        $current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
        $profit_deviation_percent = abs(100 - $current_company_percent / ($target_company_percent / 100));

        return view('admin.csgo-case-roulettes.statistics',[
            'total_income' => $total_income,
            'total_expense' => $total_expense,
            'target_company_percent' => $target_company_percent,
            'current_company_percent' => $current_company_percent,
            'profit_deviation_percent' => $profit_deviation_percent,

            'income_data' => \App\Models\Statistic::getChartData('roulette', 'income'),
            'expense_data' => \App\Models\Statistic::getChartData('roulette', 'expense'),
            'profit_deviation_percent_data' => \App\Models\Statistic::getChartData('roulette', 'profit_deviation_percent'),
            'current_profit_percent_data' => \App\Models\Statistic::getChartData('roulette', 'current_profit'),
        ]);
    }

    public function resetStatistics()
    {
        Operation::where('description', '=', 'csgo case opening')->delete();

        Operation::where('description', '=', 'csgo case win')->delete();

        Operation::where('description', '=', 'csgo case sell')->delete();

        Operation::where('description', '=', 'money back csgo case')->delete();

        return redirect('spidercontrolpanel/csgo-case-roulette-statistics');
    }

    public function resetStatisticsYoutubers()
    {
        Operation::where('description', '=', 'csgo case opening youtube')->delete();

        Operation::where('description', '=', 'csgo case win youtube')->delete();

        Operation::where('description', '=', 'csgo case sell')->delete();

        Operation::where('description', '=', 'money back csgo case youtube')->delete();

        return redirect('spidercontrolpanel/csgo-case-roulette-statistics');
    }

    private function income(){
        $total_income = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $income_roulette_opening = Operation::where('description', 'csgo case opening');
            $total_income_opening = $income_roulette_opening->sum('amount');

            $income_roulette_opening_youtuber = Operation::where('description', 'csgo case opening youtube');
            $total_income_opening_youtuber = $income_roulette_opening_youtuber->sum('amount');

            $moneyback_roulette_opening = Operation::where('description', 'money back csgo case');
            $total_moneyback_opening = $moneyback_roulette_opening->sum('amount');

            $moneyback_roulette_opening_youtuber = Operation::where('description', 'money back csgo case youtube');
            $total_moneyback_opening_youtuber = $moneyback_roulette_opening_youtuber->sum('amount');

            $total_income = $total_income_opening + $total_income_opening_youtuber + $total_moneyback_opening + $total_moneyback_opening_youtuber;
        }
        Log::info('Total income : '.$total_income);

        return -$total_income;
    }

    private function expense(){
        $total_expense = 0;
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $expense_roulette = Operation::where('description', 'csgo case win');
            $expense_roulette_win = $expense_roulette->sum('amount');

            $expense_roulette_youtuber = Operation::where('description', 'csgo case win youtube');
            $expense_roulette_win_youtuber = $expense_roulette_youtuber->sum('amount');

            $total_expense = $expense_roulette_win + $expense_roulette_win_youtuber;
        }

        Log::info('Total expense : '.$total_expense);
        return $total_expense;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.csgo-case-roulettes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.csgo-case-roulettes.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
