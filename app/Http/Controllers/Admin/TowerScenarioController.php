<?php

namespace App\Http\Controllers\Admin;

use App\Models\TowerScenario;
use App\Models\User;
use DB;
use Config;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Yajra\Datatables\Datatables;

class TowerScenarioController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$scenarios = DB::table('tower_scenarios as scenario')
		               ->select(
			               'scenario.id',
			               'scenario.user_id',
			               'scenario.attempt',
			               'scenario.level',
			               'scenario.step',
			               'scenario.status',

			               'user.username',
			               'user.steamid',
			               'user.avatar'
		               )
		               ->leftJoin('users as user', 'user.id', '=', 'scenario.user_id');

		return Datatables::of($scenarios)
		                 ->editColumn('attempt', function($scenario){
			                 if(is_null(Redis::get('tower-scenario-'.$scenario->level.'-'.$scenario->user_id))){
				                 Redis::set('tower-scenario-'.$scenario->level.'-'.$scenario->user_id, 0);
			                 }
			                 $current_attempt = Redis::get('tower-scenario-'.$scenario->level.'-'.$scenario->user_id);
			                 if($current_attempt >= $scenario->attempt && $scenario->status == 'enabled'){
				                 $current_attempt = '<strong class="text-danger" title="need to drop counters">('.$current_attempt.')</strong>';
			                 }else{
				                 $current_attempt = '('.$current_attempt.')';
			                 }
			                 return $scenario->attempt.' '.$current_attempt;
		                 })
		                 ->editColumn('username', function ($scenario) {
			                 return "<a href='/profile/{$scenario->steamid}' target='_blank'><img src='{$scenario->avatar}' style='height:40px; border-radius:40px;'> {$scenario->username}</a>";
		                 })
		                 ->editColumn('status', function ($scenario) {
			                 switch($scenario->status){
				                 case 'not available':
					                 $status = 'danger';
					                 break;
				                 case 'enabled':
					                 $status = 'primary';
					                 break;
				                 case 'completed':
					                 $status = 'success';
					                 break;
				                 default:
					                 $status = 'default';
					                 break;
			                 }
			                 return "<span class='label label-{$status}'>$scenario->status</span>";
		                 })
		                 ->addColumn('action', function ($scenario) {
			                 $btns = '
				<ul class="icons-list">
                    <li>
                    <a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/tower-scenario/$scenario->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a>
                    </li>
				    <li>
				    <a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteScenario('.$scenario->id.')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a>
				    </li>
				    <li>
				    	<input class="checked_scenario" type="checkbox" value="'. $scenario->id .'">
					</li>
			    </ul>';

			                 return $btns;
		                 })
		                 ->make(true);
	}

	public function getScenarios()
	{
		return view('admin.tower_scenarios.index');
	}

	/**
	 * @param TowerScenario $scenario
	 *
	 * @return $this
	 */
	public function create(TowerScenario $scenario)
	{
		return view('admin.tower_scenarios.create', [

		])->with('scenario', $scenario);
	}

	/**
	 * @param TowerScenario $scenario
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(TowerScenario $scenario, Request $request)
	{
		// validation
		if(TowerScenario::query()
			->where('user_id', $request->input('user_id'))
			->where('attempt', $request->input('attempt'))
			->where('level', $request->input('level'))
			->where('step', $request->input('step'))
			->first()){
			return redirect(Config::get('app.admin_prefix').'/tower-scenarios')->with('warning', 'This scenario already exists.');
		}

		$scenario->user_id = $request->input('user_id');
		$scenario->attempt = $request->input('attempt');
		$scenario->level = $request->input('level');
		$scenario->step = $request->input('step');
		$scenario->status = $request->input('status') == 'on' ? 'enabled' : 'disabled';
		$scenario->save();

		return redirect(Config::get('app.admin_prefix').'/tower-scenarios')->with('success', 'Scenario successfully created.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param TowerScenario $scenario
	 *
	 * @return $this
	 */
	public function edit(TowerScenario $scenario)
	{
		return view('admin.tower_scenarios.edit', [
			'user' => User::where('id', $scenario->user_id)->first(),
		])->with('scenario', $scenario);
	}

	/**
	 * @param Request $request
	 * @param TowerScenario $scenario
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Request $request, TowerScenario $scenario)
	{
		$scenario->user_id = $request->input('user_id');
		$scenario->attempt = $request->input('attempt');
		$scenario->level = $request->input('level');
		$scenario->step = $request->input('step');
		$scenario->status = $request->input('status') == 'on' ? 'enabled' : 'disabled';
		$scenario->save();

		return redirect(Config::get('app.admin_prefix').'/tower-scenarios')->with('success', 'Scenario successfully updated');
	}

	/**
	 * @param TowerScenario $scenario
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function destroy(TowerScenario $scenario)
	{
		$ret = $scenario->delete();
		return response()->json($ret);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteSelected(Request $request)
	{
		TowerScenario::destroy($request->input('ids'));

		return response()->json([
			'message' => 'Ok'
		], 200);
	}

	/**
	 * Drop all current attempts to 0. After that, all enabled scenarios might be played again
	 */
	public function dropCounters()
	{
		Artisan::call('tower-scenarios:clear');

		return redirect(Config::get('app.admin_prefix').'/tower-scenarios')->with('success', 'Scenario counters has been successfully dropped.');
	}
}
