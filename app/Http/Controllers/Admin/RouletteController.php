<?php

namespace App\Http\Controllers\Admin;

use App\CustomClasses\RouletteGame;
use App\Models\Operation;
use App\Models\RouletteSetting;
use App\Models\SteamCase;
use App\Models\Win2;
use Carbon\Carbon;
use Illuminate\Http\Request;

use DB;
use Helper;
use Statistic;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class RouletteController
 * @package App\Http\Controllers
 */
class RouletteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $entities = DB::table('drops')
	                  ->select('*');

	    $res = Datatables::of($entities)
	                     ->make(true);
	    return $res;
    }

	public function getIndex()
	{
		return view('admin.roulettes.index');
	}


	public function statistics()
	{
		$total_income = RouletteGame::income();
		$total_expense = RouletteGame::expense();
		$target_company_percent = RouletteSetting::where('key', 'profit')->first()->value;
		$current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
		$profit_deviation_percent = abs(100 - $current_company_percent / ($target_company_percent / 100));

		return view('admin.roulettes.statistics',[
			'total_income' => $total_income,
			'total_expense' => $total_expense,
			'target_company_percent' => $target_company_percent,
			'current_company_percent' => $current_company_percent,
			'profit_deviation_percent' => $profit_deviation_percent,

			'income_data' => \App\Models\Statistic::getChartData('roulette', 'income'),
			'expense_data' => \App\Models\Statistic::getChartData('roulette', 'expense'),
			'profit_deviation_percent_data' => \App\Models\Statistic::getChartData('roulette', 'profit_deviation_percent'),
			'current_profit_percent_data' => \App\Models\Statistic::getChartData('roulette', 'current_profit'),
		]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.roulettes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    return view('admin.roulettes.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
