<?php

namespace App\Http\Controllers\Admin;

use App\Models\AffiliateLevel;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class AffiliateLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $levels = DB::table('affiliate_levels as level')
		->select(
			'level.id',
			'level.level',
            'level.border_amount',
            'level.bonus_amount',
            'level.percent'
		);

		return Datatables::of($levels)
			->editColumn('border_amount', function($level){
				return '<span class="label label-success">$ '.number_format($level->border_amount, 2).'</span>';
			})
			->editColumn('bonus_amount', function($level){
				return '<span class="label label-warning">'.number_format($level->bonus_amount, 2).'%</span>';
			})
			->editColumn('percent', function($level){
				return '<span class="label label-warning">'.number_format($level->percent, 2).' %</span>';
			})
			->addColumn('users', function ($level) {
				return '<span class="label label-primary">'.User::where('affiliate_level', $level->level)->count().'</span>';
			})
			->addColumn('action', function ($level) {
				return '<ul class="icons-list">
					<li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/affiliate-level/$level->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
					</ul>';
			})
			->make(true);
    }

	public function getIndex()
	{
		return view('admin.affiliate_levels.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin.affiliate_levels.create');
    }

	/**
	 * @param AffiliateLevel $affiliateLevel
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function store(AffiliateLevel $affiliateLevel, Request $request)
    {
	    if(AffiliateLevel::where('level', $request->input('level'))->first()){
		    return redirect(Config::get('app.admin_prefix').'/affiliate-levels')->with('warning', 'Affiliate level already exists.');
	    }
	    $affiliateLevel->level = $request->input('level');
	    $affiliateLevel->border_amount = $request->input('border_amount');
	    $affiliateLevel->bonus_amount = $request->input('bonus_amount');
	    $affiliateLevel->percent = $request->input('percent');

	    $affiliateLevel->save();

	    return redirect(Config::get('app.admin_prefix').'/affiliate-levels')->with('success', 'Affiliate level successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * @param AffiliateLevel $affiliate_level
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function edit(AffiliateLevel $affiliate_level)
    {
	    return view('admin.affiliate_levels.edit', [
		    'affiliate_level' => $affiliate_level
	    ]);
    }

	/**
	 * @param AffiliateLevel $affiliateLevel
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(AffiliateLevel $affiliateLevel, Request $request)
    {
	    $affiliateLevel->level = $request->input('level');
	    $affiliateLevel->border_amount = $request->input('border_amount');
	    $affiliateLevel->bonus_amount = $request->input('bonus_amount');
	    $affiliateLevel->percent = $request->input('percent');

	    $affiliateLevel->save();

	    return redirect(Config::get('app.admin_prefix').'/affiliate-levels')->with('success', 'Affiliate level successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
