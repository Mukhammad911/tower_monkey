<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProvablyFairCsgoCase;
use Illuminate\Http\Request;
use DB;
use App\Models\SteamCase;
use Config;
use Datatables;
use Helper;
use Illuminate\Support\Facades\Log;

class FairCsgoCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cases = DB::table('cases as cs')
            ->select('cs.id',
                'cs.name',
                'cs.image',
                'cs.class',
                DB::raw("c.name as category"),
                'cs.sort',
                'cs.active')
            ->leftJoin('categories as c','c.id','=','cs.category_id')->where('c.name','=','CSGO');
        //->orderBy('cs.sort');

        return Datatables::of($cases)
            ->addColumn('action', function ($case) {
                $btns = '<ul class="icons-list">
								<li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/fair-csgo/".$case->id."/products").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>';

                $btns.='</ul>';

                return $btns;
            })
            ->editColumn('image', function($case){
                return '<img style="max-height:40px" src="'.asset("images/cases/$case->image").'">';
            })
            ->editColumn('price', function($case){
                $prices = DB::table('currency as cur')
                    ->select('cur.code','cur.symbol_left', 'cp.price as item')
                    ->join('case_price as cp', 'cur.id','=','cp.currency_id')
                    ->where('cp.case_id', $case->id)
                    ->get();
                $html = '';
                foreach ($prices as $price){
                    $html.='<label class="label label-info">'."$price->code-$price->item".'</label>';
                }
                return $html;

            })
            ->make(true);
    }

    public function getCases()
    {
        return view('admin.csgo-case-provably-fair.index');
    }

    public function getProducts($id)
    {
        $case = SteamCase::find($id);
        return view('admin.csgo-case-provably-fair.case-products', [
            'case' => $case,

        ]);
    }

    public function getCaseProducts($case_id)
    {
        $products = SteamCase::find($case_id)->products()->orderBy('price', 'asc')->get();

        return Datatables::of($products)
            ->editColumn('image', function($product){
                $source = ($product->image_source == 'url') ? $product->image : asset("images/products/$product->image");
                return '<img style="max-height:40px" src="'.$source.'">';
            })
            ->editColumn('name', function($product){
                $name = $product->name;
                return '<label class="text-bold">'.$name.'</label>';
            })
            ->editColumn('short_description', function($product){
                $descr = $product->short_description;
                return '<label class="text-bold">'.$descr.'</label>';
            })
            ->editColumn('price', function($product){
                return '<label class="label label-danger">'.Helper::exchange($product->price, 7).' ₽ - '.Helper::exchange($product->price, 1).'$</label>';
            })
            ->editColumn('chance', function($product) use ($case_id) {
                $fair = ProvablyFairCsgoCase::where('case_id', $case_id)
                                        ->where('product_id', $product->id)
                                        ->first();
                $var_send = empty($fair) ? 0 : $fair->chance;

                return '<input type="number" data-case-id="'.$case_id.'" data-price="'.Helper::exchange($product->price, 1).' "data-id="'.$product->id.'" class="rates-class chance-'.$product->id.'" value="'.$var_send.'" />';
            })
            ->editColumn('start', function($product) use ($case_id){
                $fair = ProvablyFairCsgoCase::where('case_id', $case_id)
                    ->where('product_id', $product->id)
                    ->first();

                $var_send = empty($fair) ? 0 : $fair->start;

                return '<input type="number" class="start-'.$product->id.'" value="'.$var_send.'" disabled />';
            })
            ->editColumn('end', function($product) use ($case_id){
                $fair = ProvablyFairCsgoCase::where('case_id', $case_id)
                    ->where('product_id', $product->id)
                    ->first();

                $var_send = empty($fair) ? 0 : $fair->end;

                return '<input type="number" class="end-'.$product->id.'" value="'.$var_send.'" disabled />';
            })
            ->make(true);
    }

    public function saveChanceProduct(Request $request)
    {
        $data = $request->input('data');

        foreach ($data as $d)
        {
            $product_id = $d['id'];
            $case_id = $d['case_id'];

            $chance = $d['chance'];
            $start = $d['start'];
            $end = $d['end'];

            $fair_csgo = ProvablyFairCsgoCase::updateOrCreate(
                ['product_id' => $product_id, 'case_id' => $case_id],
                ['chance' => $chance, 'start' => $start, 'end' => $end]
            );
        }

        return response()->json(array('success' => true));
    }
}
