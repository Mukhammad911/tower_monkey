<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\RouletteScenario;
use App\Models\SteamCase;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Yajra\Datatables\Facades\Datatables;

class RouletteScenarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scenarios = DB::table('roulette_scenarios as scenario')
            ->select(
	            'scenario.id',
	            'scenario.user_id',
	            'scenario.status',
	            'scenario.attempt',
	            'scenario.case_id',

	            'user.username',
	            'user.steamid',
	            'user.avatar',

	            'case.name as case_name',
	            'case.image as case_image',

	            'product.name as product_name',
	            'product.short_description as product_desc',
	            'product.image as product_image'
	            )
            ->leftJoin('users as user', 'user.id', '=', 'scenario.user_id')
		    ->leftJoin('cases as case', 'case.id', '=', 'scenario.case_id')
	        ->leftJoin('products as product', 'product.id', '=', 'scenario.product_id');

	    return Datatables::of($scenarios)
		    ->editColumn('attempt', function($scenario){
			    if(is_null(Redis::get('roulette-scenario-'.$scenario->case_id.'-'.$scenario->user_id))){
				    Redis::set('roulette-scenario-'.$scenario->case_id.'-'.$scenario->user_id, 0);
			    }
			    $current_attempt = Redis::get('roulette-scenario-'.$scenario->case_id.'-'.$scenario->user_id);
			    if($current_attempt >= $scenario->attempt && $scenario->status == 'enabled'){
				    $current_attempt = '<strong class="text-danger" title="need to drop counters">('.$current_attempt.')</strong>';
			    }else{
				    $current_attempt = '('.$current_attempt.')';
			    }
			    return $scenario->attempt.' '.$current_attempt;
		    })
		    ->editColumn('username', function ($scenario) {
			    return "<a href='/profile/{$scenario->steamid}' target='_blank'><img src='{$scenario->avatar}' style='height:40px; border-radius:40px;'> {$scenario->username}</a>";
		    })
		    ->editColumn('case_name', function ($scenario) {
			    return "<a href='/case/{$scenario->case_id}' target='_blank'><img src='/images/cases/{$scenario->case_image}' style='height:40px;'> {$scenario->case_name}</a>";
		    })
		    ->editColumn('product_name', function ($scenario) {
			    return "<img src='/images/products/{$scenario->product_image}' style='height:40px;'> {$scenario->product_name} | {$scenario->product_desc}";
		    })
		    ->editColumn('status', function ($scenario) {
			    switch($scenario->status){
				    case 'not available':
					    $status = 'danger';
					    break;
				    case 'enabled':
					    $status = 'primary';
					    break;
				    case 'completed':
					    $status = 'success';
					    break;
				    default:
						$status = 'default';
					    break;
			    }
			    return "<span class='label label-{$status}'>$scenario->status</span>";
		    })
		    ->addColumn('action', function ($scenario) {
			    $btns = '
				<ul class="icons-list">
                    <li>
                    <a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/roulette-scenario/$scenario->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a>
                    </li>
				    <li>
				    <a title="" data-popup="tooltip" href="javascript:void(0)" onclick="deleteScenario('.$scenario->id.')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a>
				    </li>
				    <li>
				    	<input class="checked_scenario" type="checkbox" value="'. $scenario->id .'">
					</li>
			    </ul>';

			    return $btns;
		    })
		    ->make(true);
    }

	public function getScenarios()
	{
		return view('admin.roulette_scenarios.index');
	}

	/**
	 * @param RouletteScenario $scenario
	 *
	 * @return $this
	 */
    public function create(RouletteScenario $scenario)
    {
	    return view('admin.roulette_scenarios.create', [
		    //'users' => User::orderBy('name')->get(),
		    'cases' => SteamCase::orderBy('name', 'asc')->get(),
		    'products' => Product::orderBy('name', 'asc')->where('short_description','=','chip')->get(),
	    ])->with('scenario', $scenario);
    }

	/**
	 * @param RouletteScenario $scenario
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function store(RouletteScenario $scenario, Request $request)
    {
	    // validation
	    if(RouletteScenario::query()
		    ->where('user_id', $request->input('user_id'))
		    ->where('case_id', $request->input('case_id'))
		    ->where('attempt', $request->input('attempt'))
		    ->first()){
		    return redirect(Config::get('app.admin_prefix').'/roulette-scenarios')->with('warning', 'This scenario already exists.');
	    }

	    $scenario->user_id = $request->input('user_id');
	    $scenario->case_id = $request->input('case_id');
	    $scenario->attempt = $request->input('attempt');
	    $scenario->product_id = $request->input('product_id');
	    $scenario->status = $request->input('status') == 'on' ? 'enabled' : 'disabled';

	    $scenario->save();

	    return redirect(Config::get('app.admin_prefix').'/roulette-scenarios')->with('success', 'Scenario successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * @param RouletteScenario $scenario
	 *
	 * @return $this
	 */
    public function edit(RouletteScenario $scenario)
    {
	    return view('admin.roulette_scenarios.edit', [
		    'user' => User::where('id', $scenario->user_id)->first(),
		    'cases' => SteamCase::orderBy('name', 'asc')->get(),
		    'products' => Product::orderBy('name', 'asc')->where('short_description','=','chip')->get(),
	    ])->with('scenario', $scenario);
    }

	/**
	 * @param Request $request
	 * @param RouletteScenario $scenario
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(Request $request, RouletteScenario $scenario)
    {
	    $scenario->user_id = $request->input('user_id');
	    $scenario->case_id = $request->input('case_id');
	    $scenario->attempt = $request->input('attempt');
	    $scenario->product_id = $request->input('product_id');
	    $scenario->stattrak = $request->input('stattrak') == 'on' ? 1 : 0;
	    $scenario->quality = $request->input('quality');
	    $scenario->status = $request->input('status') == 'on' ? 'enabled' : 'disabled';

	    $scenario->save();

	    return redirect(Config::get('app.admin_prefix').'/roulette-scenarios')->with('success', 'Scenario successfully updated');
    }

	/**
	 * @param RouletteScenario $scenario
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
    public function destroy(RouletteScenario $scenario)
    {
	    $ret = $scenario->delete();
	    return response()->json($ret);
    }

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function deleteSelected(Request $request)
	{
		RouletteScenario::destroy($request->input('ids'));

		return response()->json([
			'message' => 'Ok'
		], 200);
	}

	/**
	 * Drop all current attempts to 0. After that, all enabled scenarios can be played again
	 */
	public function dropCounters()
	{
		Artisan::call('roulette-scenarios:clear');

		return redirect(Config::get('app.admin_prefix').'/roulette-scenarios')->with('success', 'Scenario counters has been successfully dropped.');
	}
}
