<?php

namespace App\Http\Controllers\Admin;

use App\CustomClasses\UpgradeGame;
use App\Models\SteamCase;
use App\Models\UpgradeAlgorithm;
use App\Models\UpgradeSetting;
use App\Models\Win2;
use Carbon\Carbon;
use Illuminate\Http\Request;

use DB;
use Helper;
use Statistic;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class UpgradeController
 * @package App\Http\Controllers
 */
class UpgradeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$entities = DB::table('upgrades')
		               ->select('*');

		$res = Datatables::of($entities)
		                 ->make(true);
		return $res;
	}

	public function getIndex()
	{
		return view('admin.upgrades.index');
	}

	public function statistics()
	{
		$total_income = UpgradeGame::income();
		$total_expense = UpgradeGame::expense();
		$target_company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
		$current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);
		$profit_deviation_percent = abs(100 - $current_company_percent / ($target_company_percent / 100));

		return view('admin.upgrades.statistics',[
			'total_income' => $total_income,
			'total_expense' => $total_expense,
			'target_company_percent' => $target_company_percent,
			'current_company_percent' => $current_company_percent,
			'profit_deviation_percent' => $profit_deviation_percent,

			'income_data' => \App\Models\Statistic::getChartData('upgrade', 'income'),
			'expense_data' => \App\Models\Statistic::getChartData('upgrade', 'expense'),
			'profit_deviation_percent_data' => \App\Models\Statistic::getChartData('upgrade', 'profit_deviation_percent'),
			'current_profit_percent_data' => \App\Models\Statistic::getChartData('upgrade', 'current_profit'),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.upgrades.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('admin.upgrades.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
