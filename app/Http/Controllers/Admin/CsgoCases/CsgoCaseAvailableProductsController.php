<?php

namespace App\Http\Controllers\Admin\CsgoCases;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Facades\Datatables;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CsgoCaseAvailableProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('csgo_case_available_products')
            ->where('short_description','!=','chip')
            ->select(
                'id',
                'stattrak',
                'class',
                'price',
                'market_name',
                'image_hash',
                'bot_id'
            );

        return Datatables::of($products)
            ->editColumn('market_name', function ($product) {
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' . $product->image_hash . '/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $product->market_name . '</strong';
            })
            ->editColumn('stattrak', function ($scenario) {
                if ($scenario->stattrak) {
                    return '<span style="color: #ff6000; font-weight: bold;">StatTrak™</span>';
                }
                return '-';
            })
            ->editColumn('bot_id', function ($product) {
                return '<a href="' . url(Config::get('app.admin_prefix') . "/bots/$product->bot_id") . '">' . $product->bot_id . '</a>';
            })
            ->editColumn('price', function ($product) {
                return '<span class="label label-success"><i class="icon-price-tags"></i> ' . ($product->price / 100) . ' RUB</span>';
            })
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.csgo_case_available_products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTrades(){
        return view('admin.case_trades.csgo');
    }

    public function getTradesProducts(){
        $query = "select dm.id as id,u.username as username,u.steamid as steamid,dm.market_name as market_name,dm.stattrak as stattrak,dm.image_hash as image_hash,dm.price as price,dm.class as class,dm.created_at as created_at,dm.updated_at as updated_at,dm.bot_id as bot_id from csgo_case_drops_market dm, users u where dm.user_id = u.id and dm.status='accepted'";
        $products = DB::select($query);
        $pr = collect($products);
        return Datatables::of($pr)
            ->editColumn('market_name', function ($product) {
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' . $product->image_hash . '/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $product->market_name . '</strong';
            })
            ->editColumn('stattrak', function ($scenario) {
                if ($scenario->stattrak) {
                    return '<span style="color: #ff6000; font-weight: bold;">StatTrak™</span>';
                }
                return '-';
            })
            ->editColumn('bot_id', function ($product) {
                return '<a href="' . url(Config::get('app.admin_prefix') . "/bots/$product->bot_id") . '">' . $product->bot_id . '</a>';
            })
            ->editColumn('price', function ($product) {
                return '<span class="label label-success"><i class="icon-price-tags"></i> ' . ($product->price / 100) . ' RUB</span>';
            })
            ->make(true);
    }

    public function getPurchaseHistory(){
        return view('admin.case_purchase_history.csgo');
    }

    public function getPurchaseHistoryProducts(){
        $query = "select dm.id as id, cc.market_name as market_name,cc.image_hash as image_hash, cc.class as class, dm.price as price, dm.opskins_price as opskins_price,dm.created_at as created_at,dm.updated_at as updated_at,dm.bot_id as bot_id from csgo_case_csgotm_items dm, csgo_case_available_products cc where cc.assetid = dm.assetid and dm.status='received'";
        $products = DB::select($query);
        $pr = collect($products);
        return Datatables::of($pr)
            ->editColumn('market_name', function ($product) {
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' . $product->image_hash . '/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $product->market_name . '</strong';
            })
            ->editColumn('bot_id', function ($product) {
                return '<a href="' . url(Config::get('app.admin_prefix') . "/bots/$product->bot_id") . '">' . $product->bot_id . '</a>';
            })
            ->editColumn('price', function ($product) {
                return '<span class="label label-success"><i class="icon-price-tags"></i> ' . ($product->price) . ' RUB</span>';
            })
            ->make(true);
    }

}
