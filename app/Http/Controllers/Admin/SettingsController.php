<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Balance;
use App\Models\Drop;
use App\Models\Game;
use App\Models\LastWin;
use App\Models\LiveTower;
use App\Models\Operation;
use App\Models\Order;
use App\Models\Statistic;
use App\Models\Tower;
use App\Models\TowerCell;
use App\Models\Upgrade;
use Helper;
use App\Models\Language;
use App\Models\Currency;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Models\Setting;
use App\Http\Requests\SettingRequest;
use Illuminate\Support\Facades\Config;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class SettingsController extends Controller
{

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getLocalization()
	{
		$defaulLocales = Language::$default;
		$defaulCurr = Currency::$default;

		return view('admin.settings.localization',['defaultCurrency'=>json_encode($defaulCurr),
		                                           'defaultLocales'=>json_encode($defaulLocales)]);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getLanguages()
	{
		$langs = DB::table('languages as langs')
		           ->select('langs.*',
			           'curr.title as currency', 'curr.id as currency_id')
		           ->leftJoin('currency as curr', 'langs.currency_id','=','curr.id')
		           ->orderBy('active','desc')
		           ->orderBY('sort')
		           ->orderBy('name')
		           ->get();

		$currency_list = Currency::where('status',1)->get()->toArray();

		foreach ($langs as &$lang){
			$lang->currency_list = $currency_list;
		}

		return response()->json(['data'=>$langs]);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSetLangActive(Request $request)
	{
		$lang = Language::find($request->input('id'));

		$lang->active = $request->input('active');
		$ret = $lang->save();

		return response()->json($ret);
	}

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getCurrency()
	{
		$curr = Currency::orderBy('status','desc')
		                ->orderBy('title')
		                ->get();

		return response()->json(['data'=>$curr]);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postSetCurrencyActive(Request $request)
	{
		$curr = Currency::find($request->input('id'));
		$curr->status = $request->input('active');
		$ret = $curr->save();

		if(!$curr->status){
			Language::where('currency_id',$curr->id)->update(['currency_id'=>0]);
		}

		return response()->json($ret);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function postLanguageSetCurrency(Request $request)
	{
		$lang = Language::find($request->input('id'));
		$lang->currency_id = $request->input('currency_id');
		$ret = $lang->save();

		return response()->json($ret);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getGeneral()
	{
		return view('admin.settings.general', [
			'settings' => Setting::all(),
		]);
	}

	/**
	 * @param SettingRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postGeneral(SettingRequest $request)
	{
		foreach($request->input() as $key => $value){
			Setting::where('key', $key)->update(['value' => $value]);
		}

		return redirect(Config::get('app.admin_prefix'))->with('success', 'Settings successfully updated!');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getClearProject()
	{
		return view('admin.settings.clear_project');
	}

	/**
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postClearProject()
	{
		if($_ENV['APP_ENV'] != 'development'){
			return redirect(Config::get('app.admin_prefix').'/settings/clear-project')->with('danger', 'The project in production mode. This operation is strictly prohibited.');
		}

		DB::statement("SET foreign_key_checks = 0");
		Balance::query()->truncate();
		Affiliate::query()->truncate();
		Balance::query()->truncate();
		Drop::query()->truncate();
		LastWin::query()->truncate();
		LiveTower::query()->truncate();
		Operation::query()->truncate();
		Order::query()->truncate();
		Statistic::query()->truncate();
		TowerCell::query()->truncate();
		Tower::query()->truncate();
		Upgrade::query()->truncate();
		Game::query()->truncate();
		DB::statement("SET foreign_key_checks = 1");

		return redirect(Config::get('app.admin_prefix').'/settings/clear-project')->with('success', 'The project has been successfully cleared!');
	}
}
