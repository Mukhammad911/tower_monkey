<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Operation;
use App\Models\RouletteSetting;
use App\Models\SteamCase;
use App\Models\Win2;
use Illuminate\Support\Facades\Log;
use Statistic;
use Helper;
use App\Models\Order;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Models\Currency;

class StatisticsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */

    public function getOrders(){
        $currency = Currency::getActive();

        return view('admin.statistics.orders',['currency'=>$currency]);
    }

    public function getOrdersList(Request $request){
        $orders = Order::all();

        return Datatables::of($orders)
            ->editColumn('currency', function($order){
                return $order->currency->code;
            })
            ->editColumn('user', function($order){
                return $order->user->username;
            })
            ->editColumn('created_at', function($order){
                return Carbon::parse($order->created_at)->format('d-M-Y H:i');
            })
            ->filter(function ($instance) use ($request) {
                if ($request->has('status')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return ($row['status'] == $request->input('status'));
                    });
                }
                if ($request->has('date_from')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return strtotime(Carbon::parse($row['created_at'])->format('d-M-Y')) >= strtotime($request->input('date_from'));
                    });
                }
                if ($request->has('date_to')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return strtotime(Carbon::parse($row['created_at'])->format('d-M-Y')) <= strtotime($request->input('date_to'));
                    });
                }
                if ($request->has('currency_id')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return ($row['currency_id'] == $request->input('currency_id'));
                    });
                }
            })
            ->make(true);
    }

	public function getFinances()
	{
		$total_income = Statistic::totalIncome();
		$total_expense = Statistic::totalExpense();
		$current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);

		$income_data = \App\Models\Statistic::getChartData('', 'income');
		$expense_data = \App\Models\Statistic::getChartData('', 'expense');
		$current_profit_percent_data = \App\Models\Statistic::getChartDataTotalCurrentProfit($income_data, $expense_data);

		return view('admin.statistics.finances',[
			'total_income' => $total_income,
			'total_expense' => $total_expense,
			'current_company_percent' => $current_company_percent,

			'income_data' => $income_data,
			'expense_data' => $expense_data,
			'current_profit_percent_data' => $current_profit_percent_data,
		]);
	}

    public function getGames()
    {
        //$total_income = Statistic::totalIncome();
        //$total_expense = Statistic::totalExpense();
        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $income_tower = Operation::where('description', 'tower bet')->where('currency_id', $currency->id)->sum('amount');

            Log::info("income tower");
            Log::info($income_tower);

            $income_roulette_opening_csgo = Operation::where('description', 'csgo case opening')->sum('amount');
            $income_roulette_opening_youtuber_csgo = Operation::where('description', 'csgo case opening youtube')->sum('amount');
            $moneyback_roulette_opening_csgo = Operation::where('description', 'money back csgo case')->sum('amount');
            $moneyback_roulette_opening_youtuber_csgo = Operation::where('description', 'money back csgo case youtube')->sum('amount');

            Log::info(" income_roulette_opening_youtuber_csgo");
            Log::info($income_roulette_opening_youtuber_csgo);

            $income_roulette_opening_pubg = Operation::where('description', 'pubg case opening')->sum('amount');
            $income_roulette_opening_youtuber_pubg = Operation::where('description', 'pubg case opening youtube')->sum('amount');
            $moneyback_roulette_opening_pubg = Operation::where('description', 'money back pubg case')->sum('amount');
            $moneyback_roulette_opening_youtuber_pubg = Operation::where('description', 'money back pubg case youtube')->sum('amount');

            $total_income_all = $income_tower + $income_roulette_opening_csgo + $income_roulette_opening_youtuber_csgo
                + $moneyback_roulette_opening_csgo + $moneyback_roulette_opening_youtuber_csgo
                + $income_roulette_opening_pubg + $income_roulette_opening_youtuber_pubg
                + $moneyback_roulette_opening_pubg + $moneyback_roulette_opening_youtuber_pubg;

        }

        $total_income = -$total_income_all;

        Log::info("TTTTTTTTotal income");
        Log::info($total_income);

        foreach ( Currency::where('status', 1)->get() as $currency ) {
            $expense_tower = Operation::where('description', 'tower win')->where('currency_id', $currency->id)->sum('amount');

            $expense_roulette_csgo = Operation::where('description', 'csgo case win')->sum('amount');
            $expense_roulette_youtuber_csgo = Operation::where('description', 'csgo case win youtube')->sum('amount');

            $expense_roulette_pubg = Operation::where('description', 'pubg case win')->sum('amount');
            $expense_roulette_youtuber_pubg = Operation::where('description', 'pubg case win youtube')->sum('amount');

            $total_expense = $expense_tower + $expense_roulette_csgo + $expense_roulette_youtuber_csgo + $expense_roulette_pubg + $expense_roulette_youtuber_pubg;
        }


        $current_company_percent = Statistic::currentCompanyProfit($total_income, $total_expense);

        $income_data = \App\Models\Statistic::getChartData('', 'income');
        $expense_data = \App\Models\Statistic::getChartData('', 'expense');
        $current_profit_percent_data = \App\Models\Statistic::getChartDataTotalCurrentProfit($income_data, $expense_data);

        return view('admin.statistics.games',[
            'total_income' => $total_income,
            'total_expense' => $total_expense,
            'current_company_percent' => $current_company_percent,

            'income_data' => $income_data,
            'expense_data' => $expense_data,
            'current_profit_percent_data' => $current_profit_percent_data,
        ]);
    }
}
