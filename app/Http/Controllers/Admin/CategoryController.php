<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\SteamCase;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Config;
use Validator;
use DB;
use Datatables;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Category $category)
	{
		$categories = $category->select('id', 'name', 'sort')
								->orderBy('sort');
		
		return Datatables::of($categories)
			->addColumn('action', function ($category) {
				return '<ul class="icons-list">
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortUp('.$category->id.')" data-original-title="Up"><i class="icon-arrow-up16 position-right"></i> Up</a></li>
								<li><a title="" data-popup="tooltip" href="javascript:void(0)" onclick="sortDown('.$category->id.')" data-original-title="Down"><i class="icon-arrow-down16 position-right"></i> Down</a></li>
                                <li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/category/$category->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
                                <li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteCategory('.$category->id.')" data-original-title="Delete"><i class="icon-basket"></i> Delete</a></li>
                            </ul>';
			})
			->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Category $category)
	{
		return view('admin.categories.create')->with('category', $category);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Category $category, CategoryRequest $request)
	{
		$category->name = $request->input('name');
		$category->description = $request->input('description');

		$lastSort = Category::orderBy('sort','desc')
							->take(1)
							->pluck('sort');
		$category->sort = ++$lastSort;

		$category->save();

		return redirect(Config::get('app.admin_prefix').'/categories')->with('success', 'Successfully created new category!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param Category $category
	 *
	 * @return $this
	 */
	public function edit(Category $category)
	{
		return view('admin.categories.edit')->with('category', $category);
	}

	/**
	 * @param Category $category
	 * @param CategoryRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(Category $category, CategoryRequest $request)
	{
		$category->name = $request->input('name');
		$category->description = $request->input('description');

		$category->save();

		return redirect(Config::get('app.admin_prefix').'/categories')->with('success', 'Successfully updated category!');
	}

	/**
	 * @param Category $category
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function destroy(Category $category)
	{
		$category->delete();
		SteamCase::where('category_id',$category->id)
				->update(['category_id'=>0]);

		return response()->json(true);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getCategories()
	{
		return view('admin.categories.index');
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSortUp($id)
	{
		$category = Category::find($id);

		$previous = Category::orderBy('sort','desc')
							->where('sort','<',$category->sort)
							->take(1)
							->first();

		if(!is_null($previous)){
			$prevCategory = Category::find($previous->id);
			$prevCategory->sort = $category->sort;
			$prevCategory->save();

			$category->sort = $previous->sort;
			$category->save();
		}

		return response()->json(true);
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSortDown($id)
	{
		$category = Category::find($id);

		$next = Category::orderBy('sort')
						->where('sort','>',$category->sort)
						->take(1)
						->first();
		
		if(!is_null($next)){
			$nextCategory = Category::find($next->id);
			$nextCategory->sort = $category->sort;
			$nextCategory->save();

			$category->sort = $next->sort;
			$category->save();
		}

		return response()->json(true);
	}
}
