<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Balance;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;
use Config;
use DB;
use Helper;
use Bican\Roles\Models\Role;
use Datatables;
use App\Http\Requests\UserRequest;
use App\Http\Requests\ProfileRequest;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = DB::table('users as user')
            ->select(
	            'user.*',
	            DB::raw('(SELECT balance.summ FROM balance WHERE balance.currency_id = 1 AND balance.type = 0 AND user_id = user.id LIMIT 1) as balance')
            )
            ->where('user.id', '!=', Auth::user()->id);
		$res = Datatables::of($users)
             ->addColumn('action', function ($user) {
                 return '<ul class="icons-list">
                    <li>
                        <a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/user/$user->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a>
                    </li>

                    <li>
                        <a href="javascript:void(0)"><button class="btn btn-primary login_like_user" data-user_id="'.$user->id.'">Login</button></a>
                    </li>

                    <li>
                        <a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteUser('.$user->id.')" data-original-title="Delete"><i class="icon-basket"></i> Delete</a>
                        </li>
                </ul>';
             })
			->editColumn('balance', function($user){
				$res = '';
				foreach ( Currency::where('status', 1)->get() as $currency ) {
					$amount = DB::table('balance')
					         ->where('user_id', $user->id)
					         ->where('currency_id', $currency->id)
					         ->sum('summ');
					if($amount){
						$res .= '<span class="label label-success"><i class="fa '.$currency->class.'"></i>'.$amount.'</span>';
					}
				}

				return $res;
			})
			->addColumn('profit', function($user) {
				$amount = Helper::exchange($user->profit, 1);
				$class = $amount >=0 ? 'success' : 'danger';
				return '<strong class="text-'.$class.'">$'.$amount.'</strong>';
			})
			->editColumn('youtuber', function($user) {
				return ($user->youtuber == 1) ? '<span class="label label-danger"><i class="icon-youtube"></i> YouTube</span>' : '-';
			})
			->addColumn('top_banned', function($user) {
				return ($user->top_banned == 1) ? '<span class="label label-danger">top ban</span>' : '-';
			})
			->addColumn('drop_banned', function($user) {
				return ($user->drop_banned == 1) ? '<span class="label label-danger">drop ban</span>' : '-';
			})
			->addColumn('withdraw_banned', function($user) {
				return ($user->withdraw_banned == 1) ? '<span class="label label-danger">withdraw ban</span>' : '-';
			})
			->make(true);
		return $res;
	}

	/**
	 * @param User $user
	 *
	 * @return $this
	 */
	public function create(User $user)
	{
		return view('admin.users.create')->with('user', $user);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRequest $request)
	{
		$this->createUser($request);

		return redirect(Config::get('app.admin_prefix').'/users')->with('success', 'Successfully created new user!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * @param User $user
	 *
	 * @return $this
	 */
	public function edit(User $user)
	{
		return view('admin.users.edit')->with('user', $user);
	}

	/**
	 * @param User $user
	 * @param UserRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(User $user, UserRequest $request)
	{
		$this->updateUser($request, $user->id);

		return redirect(Config::get('app.admin_prefix').'/users')->with('success', 'Successfully updated user!');
	}

	/**
	 * @param User $user
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 */
	public function destroy(User $user)
	{
		return response()->json($user->delete());
	}

	public function getProfile(){
		return view('admin.users.profile',['user'=>Auth::user()]);
	}

	public function postUpdateProfile(ProfileRequest $request){

		$this->updateUser($request, Auth::user()->id);

		return redirect(Config::get('app.admin_prefix'));
	}

	public function getUsers(){
		return view('admin.users.index');
	}

	private function updateUser($request, $id)
	{
		//return redirect(Config::get('app.admin_prefix').'/users')->with('danger', 'This promo code already has another user!');

		$password 	= $request->input('password');
		$user = User::find($id);
		$user->email = $request->input('email');
		$user->username = $request->input('username');

		if($request->input('promo_code') == '' || User::where('promo_code', $request->input('promo_code'))->count()){
		    $user->promo_code = null;
		}else{
			$user->promo_code = $request->input('promo_code');
		}
		$user->active = ($request->input('active') == 'on') ? 1 : 0;
		$user->youtuber = ($request->input('youtuber') == 'on') ? 1 : 0;
		$user->top_banned = ($request->input('top_banned') == 'on') ? 1 : 0;
		$user->drop_banned = ($request->input('drop_banned') == 'on') ? 1 : 0;
		$user->withdraw_banned = ($request->input('withdraw_banned') == 'on') ? 1 : 0;

		if(!empty($password)){
			$user->password = bcrypt($password);
		}

		if($request->hasFile('avatar')){
			$file 		= $request->file('avatar');
			$avatar 	= $file->getClientOriginalName();
			$file->move(Config::get('app.upload_path').'avatars', $avatar);
			$user->avatar = $avatar;
		}

		return $user->save();
	}

	private function createUser($request){
		$name		= $request->input('name');
		$email 		= $request->input('email');
		$promo_code = $request->input('promo_code');
		$youtuber	= $request->input('youtuber');
		$password 	= bcrypt($request->input('password'));
		$active 	= ($request->input('active') == 'on') ? 1 : 0;
		$avatar 	='';

		if($request->hasFile('avatar')){
			$file 		= $request->file('avatar');
			$avatar 	= $file->getClientOriginalName();
			$file->move(Config::get('app.upload_path').'avatars', $avatar);
		}

		$user = User::create([
			'name'		=> $name,
			'email'		=> $email,
			'promo_code'=> $promo_code,
			'youtuber'  => $youtuber,
			'password'	=> $password,
			'active'	=> $active,
			'avatar'	=> $avatar
		]);

		$userRole = Role::find(3);

		return $user->attachRole($userRole);
	}

	public function getAutocomplete(Request $request){
		$users = DB::table('users');
		$term = $request->input('term');

		if($term) {
			$users = $users
				->where('name', 'LIKE', "%$term%")
				->orWhere('username', 'LIKE', "%$term%")
				->orWhere('email', 'LIKE', "%$term%");
		}
		$users = $users->take(10)->get();

		return response()->json($users);
	}
}
