<?php

namespace App\Http\Controllers\Admin;

use App\Models\UpgradeSetting;
use Illuminate\Http\Request;

use DB;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

/**
 * Class UpgradeSettingController
 * @package App\Http\Controllers
 */
class UpgradeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $settings = DB::table('upgrade_settings')
	                  ->select('*');

	    $res = Datatables::of($settings)
	                     ->editColumn('value', function($setting){
		                     return '<span class="label label-warning">'.$setting->value.' '.$setting->symbol.'</span>';
	                     })
	                     ->editColumn('updated_at', function($setting){
		                     return '<span class="label label-default">'.$setting->updated_at.'</span>';
	                     })
	                     ->addColumn('action', function ($setting) {
		                     return '<ul class="icons-list">
                                <li><a title="" data-popup="tooltip" href="'.url(Config::get('app.admin_prefix')."/upgrade-setting/$setting->id/edit").'" data-original-title="Edit"><i class="icon-pencil7 position-right"></i> Edit</a></li>
                                <li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteUpgradeSetting('.$setting->id.')" data-original-title="Delete"><i class="icon-bin"></i> Delete</a></li>
                            </ul>';
	                     })
	                     ->make(true);
	    return $res;
    }

	public function getIndex()
	{
		return view('admin.upgrade_settings.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.upgrade_settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $this->validate($request, [
		    'key' => 'required|max:255',
		    'value' => 'required|max:255',
		    'symbol' => 'max:10',
		    'description' => 'max:255',
	    ]);

	    $TowerSetting = new UpgradeSetting();
	    $TowerSetting->key = $request->input('key');
	    $TowerSetting->value = $request->input('value');
	    $TowerSetting->symbol = $request->input('symbol');
	    $TowerSetting->description = $request->input('description');
	    $TowerSetting->save();

	    return redirect(Config::get('app.admin_prefix').'/upgrade-settings')->with('success', 'New upgrade setting has been successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

	/**
	 * @param UpgradeSetting $UpgradeSetting
	 *
	 * @return $this
	 */
    public function edit(UpgradeSetting $UpgradeSetting)
    {
	    return view('admin.upgrade_settings.edit')->with('setting', $UpgradeSetting);
    }

	/**
	 * @param UpgradeSetting $UpgradeSetting
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function update(UpgradeSetting $UpgradeSetting, Request $request)
    {
	    $this->validate($request, [
		    'key' => 'required|max:255',
		    'value' => 'required|max:255',
		    'symbol' => 'max:10',
		    'description' => 'max:255',
	    ]);

	    $UpgradeSetting->key = $request->input('key');
	    $UpgradeSetting->value = $request->input('value');
	    $UpgradeSetting->symbol = $request->input('symbol');
	    $UpgradeSetting->description = $request->input('description');
	    $UpgradeSetting->save();

	    return redirect(Config::get('app.admin_prefix').'/upgrade-settings')->with('success', 'Upgrade setting has been successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UpgradeSetting::destroy($id);
	    return response()->json(true);
    }
}
