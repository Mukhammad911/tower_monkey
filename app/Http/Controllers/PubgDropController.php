<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SteamCase;
use DB;
use App\Models\PubgDropsMarket;
use Illuminate\Support\Facades\Auth;
use Session;
use Helper;
use App;
use Lang;
use Config;
use ProvablyFair;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Log;

/**
 * Class BotController
 * @package App\Http\Controllers
 */
class PubgDropController extends Controller
{
    private $user_id;
    private $status;
    private $price;
    private $product_id;
    private $name;
    private $market_name;
    private $image_hash;
    private $stattrak;
    private $assetid;
    private $classid;
    private $instanceid;
    private $bot_id;

    public function saveAvailableProduct($id)
    {
        $ret = ['success' => true, 'drop_id' => null];
        $user = Auth::user();
        $items = DB::table('pubg_available_products')->where('id', '=', $id)->get();

        foreach ($items as $item) {
            $this->product_id = $item->product_id;
            $this->price = $item->price;
            $this->name = $item->name;
            $this->market_name = $item->market_name;
            $this->image_hash=$item->image_hash;
            $this->stattrak = $item->stattrak;
            $this->assetid = $item->assetid;
            $this->classid = $item->classid;
            $this->instanceid = $item->instanceid;
            $this->bot_id = $item->bot_id;
        }

        $price = Helper::exchange($this->price, 1);

        try {
            DB::beginTransaction();

            $drop = new PubgDropsMarket();
            $drop->user_id = $user->id;
            $drop->status = 'pending';
            $drop->product_id = $this->product_id;
            $drop->price = $this->price;
            $drop->name = $this->name;
            $drop->market_name = $this->market_name;
            $drop->image_hash = $this->image_hash;
            $drop->stattrak = $this->stattrak;
            $drop->assetid = $this->assetid;
            $drop->classid = $this->classid;
            $drop->instanceid = $this->instanceid;
            $drop->bot_id = $this->bot_id;
            $drop->currency_id =1;

            if (!$drop->save()) {
                return response()->json(['success' => false, 'error' => 'server_error']);
            }
            DB::commit();
            $user->balance()
                ->where('currency_id',1)
                ->where('type', 0)
                ->update(['summ'=>DB::raw("summ - $price")]);


            $ret['drop_id'] = $drop->id;

            DB::table('pubg_available_products')->where('id', '=', $id)->delete();

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();

            $this->moneyBack($user, $price);

            $ret['notify']  = true;
            $ret['success'] = false;
            return response()->json($ret);
        }
        $ret['win'] = $drop;
        Log::info(serialize($ret));
        return response()->json($ret);
    }

    private function moneyBack($user, $amount)
    {
        $user->balance()
            ->where('currency_id',1)
            ->where('type', 0)
            ->update(['summ'=>DB::raw("summ + $amount")]);
    }
}
