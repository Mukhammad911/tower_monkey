<?php

namespace App\Http\Controllers;

use App\Models\SteamCase;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Yajra\Datatables\Datatables;
use App\Models\CsgoCasePurchase;
use App\Models\Product;
use App\Models\CsgoCaseBot;
use App\Models\CsgoCaseDrops;
use App\Http\Requests\PurchaseRequest;
use Lang;
use Config;

class CsgoCasePurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.csgo_case_purchases.index');
    }

    public function getPurchases(){
        return Datatables::of(CsgoCasePurchase::with('bot')->select('csgo_case_purchases.*'))
            ->editColumn('available', function($purchase){
                $botId = $purchase->bot->id;
                $dropsCount = DB::table('csgo_case_available_products')
                    ->where('bot_id', '=', $botId)
                    ->count();
                return $dropsCount;
            })
            ->editColumn('drops', function($purchase){
                $botId = $purchase->bot->id;
                $dropsCount = DB::table('csgo_case_drops_market')
                    ->where('bot_id', '=', $botId)
                    ->where('status', '=', 'pending')
                    ->count();
                return $dropsCount;
            })

                /*if($request->has('available')){
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if($request->input('available') == 'yes'){
                            return ($row['available'] > 0);
                        }
                        else{
                            return ($row['available'] == 0);
                        }
                    });

                }

                if ($request->has('bot_id')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return ($row['bot_id'] == $request->input('bot_id'));
                    });
                }*/
            ->make(true);

      /*  return Datatables::of(CsgoCasePurchase::with('bot')->select('csgo_case_purchases.id'))
            ->editColumn('id', function($purchase) {
                return $purchase->id;
            })

           /* ->editColumn('available', function($purchase){
                return $purchase->bot->availableProducts->count();
            })
            ->editColumn('drops', function($purchase){
                $botId = $purchase->bot->id;
                $dropsCount = DB::table('csgo_case_drops_market')
                    ->where('bot_id', '=', $botId)
                    ->where('status', '=', 'pending')
                    ->count();
                return $dropsCount;
            })
            ->filter(function ($instance) use ($request) {*/

                /*if($request->has('available')){
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if($request->input('available') == 'yes'){
                            return ($row['available'] > 0);
                        }
                        else{
                            return ($row['available'] == 0);
                        }
                    });

                }

                if ($request->has('bot_id')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return ($row['bot_id'] == $request->input('bot_id'));
                    });
                }*/

            /*->make(true);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function create(CsgoCasePurchase $purchase)
    {
        $bots = CsgoCaseBot::all();
        $cases = SteamCase::all();
        return view('admin.csgo_case_purchases.create', ['bots' => $bots, 'cases' => $cases, 'caseIds' => []])->with('purchase',$purchase);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase = CsgoCasePurchase::create($fields);


        $purchase->cases()->attach($request->input('cases'));
        Redis::publish(Config::get('app.redis_prefix').'csgo-case-purchase-change-'.$purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));

        return redirect('/spidercontrolpanel/csgo-case-purchases')->with('success', 'Successfully created purchase!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(CsgoCasePurchase $purchase)
    {
        $bots = CsgoCaseBot::all();
        $cases = SteamCase::all();
        $purchasedCases = $purchase->cases()->get(array('case_id'))->toArray();

        $func = function($item) {
            return $item['case_id'];
        };

        return view('admin.csgo_case_purchases.edit', ['bots'=>$bots, 'cases' => $cases, 'caseIds' => array_map($func, $purchasedCases)])->with('purchase',$purchase);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Purchase $purchase
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(CsgoCasePurchase $purchase, PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase->update($fields);

        $purchase->cases()->sync($request->input('cases'));

        Redis::publish(Config::get('app.redis_prefix').'csgo-case-purchase-change-'.$purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));
        return redirect('/spidercontrolpanel/csgo-case-purchases')->with('success', 'Successfully purchase updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(CsgoCasePurchase $purchase)
    {
        $ret = $purchase->delete();
        Redis::publish(Config::get('app.redis_prefix').'csgo-case-purchase-change-'.$purchase->bot_id, json_encode(['purchaseId' => null]));
        return response()->json($ret);
    }

    private function prepareFields($request) {
        $fields = $request->all();
        $fields['max_price'] = $fields['max_price']*100;
        $fields['target'] = ($fields['target'] <= 900) ? $fields['target'] : 900;

        return $fields;
    }
}
