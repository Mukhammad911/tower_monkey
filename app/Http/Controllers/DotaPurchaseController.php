<?php

namespace App\Http\Controllers;

use App\Models\SteamCase;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Psy\Util\Json;
use Yajra\Datatables\Datatables;
use App\Models\DotaPurchase;
use App\Models\DotaProduct;
use App\Models\DotaBot;
use App\Models\DotaDropsMarket;
use App\Http\Requests\PurchaseRequest;
use Lang;
use Config;

class DotaPurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dota_purchases.index');
    }

    public function getPurchases()
    {
        $data = DB::table('dota_purchases')->get();
        return view('admin.dota_purchases.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function create(DotaPurchase $purchase)
    {
        $bots = DotaBot::all();
        $cases = SteamCase::all();
        return view('admin.dota_purchases.create', ['bots' => $bots, 'cases' => $cases, 'caseIds' => []])->with('purchase', $purchase);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase = DotaPurchase::create($fields);


        $purchase->cases()->attach($request->input('cases'));
        Redis::publish(Config::get('app.redis_prefix') . 'purchase-dota-change-' . $purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));

        return redirect(Config::get('app.admin_prefix') . '/dota-purchase')->with('success', 'Successfully created purchase!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(DotaPurchase $purchase)
    {
        $bots = DotaBot::all();
        $cases = SteamCase::all();
        $purchasedCases = $purchase->cases()->get(array('case_id'))->toArray();

        $func = function ($item) {
            return $item['case_id'];
        };

        return view('admin.dota_purchases.edit', ['bots' => $bots, 'cases' => $cases, 'caseIds' => array_map($func, $purchasedCases)])->with('purchase', $purchase);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Purchase $purchase
     * @param PurchaseRequest|Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(DotaPurchase $purchase, PurchaseRequest $request)
    {
        $fields = $this->prepareFields($request);
        $purchase->update($fields);

        $purchase->cases()->sync($request->input('cases'));

        Redis::publish(Config::get('app.redis_prefix') . 'dota-purchase-change-' . $purchase->bot_id, json_encode(['purchaseId' => $purchase->id]));
        return redirect(Config::get('app.admin_prefix') . '/dota-purchase')->with('success', 'Successfully purchase updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Purchase $purchase
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(DotaPurchase $purchase)
    {
        $ret = $purchase->delete();
        Redis::publish(Config::get('app.redis_prefix') . 'purchase-dota-change-' . $purchase->bot_id, json_encode(['purchaseId' => null]));
        return response()->json($ret);
    }

    private function prepareFields($request)
    {
        $fields = $request->all();
        $fields['max_price'] = $fields['max_price'] * 100;
        $fields['target'] = ($fields['target'] <= 900) ? $fields['target'] : 900;

        return $fields;
    }

    public function deletePurchases($id)
    {
        $purchase_id = $id;
        DB::table('dota_purchases')->where('id', '=', $purchase_id)->delete();
        DB::table('dota_search_items')->where('purchase_id', '=', $purchase_id)->delete();


        return redirect('spidercontrolpanel/dota-purchase');
    }

    public function createFormPurchases()
    {
        return view('admin.dota_purchases.create');
    }

    public function createPurchases()
    {
        $dota_bots = DB::table('dota_bots')->select('id', 'steam_login')->get();
        return response()->json($dota_bots);
    }

    public function getProductPurchases()
    {
        $dota_products = DB::table('dota_products')->select('id', 'name', 'short_description', 'image', 'price')->orderBy('name','asc')->groupBy('name')->get();
        return response()->json($dota_products);
    }

    public function savePurchases(Request $request)
    {
        $error = ['var_error', 'bot_error', 'not_inserted'];
        $check_exist_bot_id = false;
        $bot_id = $request->input('bot_id');
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');
        $quality = $request->input('quality');
        $rarity = $request->input('rarity');
        $purchase_id = '';
        if ($bot_id == '' || $min_price == '' || $max_price == '' || $quality == '' || $rarity == '') {
            return response()->json($error[0]);
        } else {
            $db_bot_id = DB::table('dota_purchases')->select('bot_id')->get();
            foreach ($db_bot_id as $bot_id_id) {
                if ($bot_id == $bot_id_id->bot_id) {
                    $check_exist_bot_id = true;
                }
            };
            if ($check_exist_bot_id) {
                return response()->json($error[1]);
            } else {
                $inserted_purchase = DB::table('dota_purchases')->insert([
                    'bot_id' => $bot_id,
                    'min_price' => $min_price,
                    'max_price' => $max_price,
                    'quality' => $quality,
                    'rarity' => $rarity
                ]);
                if ($inserted_purchase) {
                    $purchases_id = DB::table('dota_purchases')->select('id')->where('bot_id', '=', $bot_id)->get();
                    foreach ($purchases_id as $id) {
                        $purchase_id = $id->id;
                    }
                    $json_response_true = ['message' => 'success', 'purchase_id' => $purchase_id];
                    return response()->json($json_response_true);
                } else {
                    return response()->json($error[2]);
                }
            }
        }
    }

    public function saveProductPurchases(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $product_name = $request->input('name');
        $product_quota = $request->input('quota');
        $check =DB::table('dota_search_items')->insert([
            'purchase_id' => $purchase_id,
            'name' => $product_name,
            'quota' => $product_quota
        ]);
        if(!$check){
            Log::info("Error insert product: ".$product_name);
        }
    }
    public function saveProductPurchasesComplete(Request $request)
    {
        $purchase_id = $request->input('purchase_id');
        $bot_id = $request->input('bot_id');

        Redis::publish(Config::get('app.redis_prefix') . 'dota-purchase-change-' . $bot_id, json_encode(['purchaseId' => $purchase_id]));

        $json_response_true = ['message' => 'success', 'bot_id' => $bot_id, 'purchase_id' => $purchase_id];
        return response()->json($json_response_true);
    }
}
