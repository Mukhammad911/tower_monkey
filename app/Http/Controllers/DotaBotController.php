<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\BotRequest;
use App\Models\DotaBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Config;


/**
 * Class BotController
 * @package App\Http\Controllers
 */
class DotaBotController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $bots = DotaBot::all();
        return view('admin.dota_bots.index')->with('bots', $bots);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.dota_bots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BotRequest $request
     * @return Response
     */
    public function store(BotRequest $request)
    {
        DotaBot::create($request->all());
        return redirect(Config::get('app.admin_prefix').'/dota_bots')->with('success', 'Successfully added new bot!');
    }

    /**
     * Display the specified resource.
     *
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function show(DotaBot $bot)
    {
        return view('admin.dota_bots.show')->with('bot', $bot);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function edit(DotaBot $bot)
    {
        return view('admin.dota_bots.edit')->with('bot', $bot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BotRequest $request
     * @param Bot $bot
     * @return Response
     * @internal param int $id
     */
    public function update(BotRequest $request, DotaBot $bot)
    {
        $bot->update($request->except(['steam_login']));
        return redirect(Config::get('app.admin_prefix').'/dota_bots')->with('success', 'Successfully update bot '.$bot->steam_login.'!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bot $bot
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(DotaBot $bot)
    {
        if($bot->enabled) Redis::publish(Config::get('app.redis_prefix').'disable-dota-bot', json_encode(['botId' => $bot->id]));
        $bot->delete();

        return redirect(Config::get('app.admin_prefix').'/dota_bots')->with('success', 'Successfully deleted bot!');
    }

    /**
     * @param Bot $bot
     * @return string
     */
    public function getAuthCode(DotaBot $bot) {
        return $bot->getCode();
        //return $bot->steam_login;
    }

    /**
     * Enable bot
     * @param Bot $bot
     * @return string
     */
    public function getEnable(Request $request, DotaBot $bot) {
        if($bot->secret) {
            $bot->enabled = 1;
            $bot->save();

            Redis::publish(Config::get('app.redis_prefix').'enable-dota-bot', json_encode(['botId' => $bot->id]));
            return ($request->ajax()) ? json_encode(['status' => 'success']) : "success";
        }

        return ($request->ajax()) ? json_encode(['status' => 'failed', 'message' => 'Account wasn\'t activated.']) : "failed";
    }

    /**
     * Disable bot
     * @param Bot $bot
     * @return string
     */
    public function getDisable(DotaBot $bot) {
        $bot->enabled = 0;
        $bot->save();

        Redis::publish(Config::get('app.redis_prefix').'disable-dota-bot', json_encode(['botId' => $bot->id]));

        return "success";
    }

}
