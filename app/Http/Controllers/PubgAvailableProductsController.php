<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Log;

class PubgAvailableProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('pubg_available_products')
            ->select(
                'id',
                'stattrak',
                'price',
                'market_name',
                'image_hash',
                'bot_id'
            );

        return Datatables::of($products)
            ->editColumn('market_name', function($product){
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/'.$product->image_hash.'/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$product->market_name.'</strong';
            })
            ->editColumn('bot_id', function($product){
                return '<a href="'.url(Config::get('app.admin_prefix')."/pubg_bots/$product->bot_id").'">'.$product->bot_id.'</a>';
            })
            ->editColumn('price', function($product){
                return '<span class="label label-success"><i class="icon-price-tags"></i> '.($product->price/100).' RUB</span>';
            })
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('admin.pubg_available_products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getTrades(){
        return view('admin.trades.pubg');
    }
    public function getTradesProducts(){
        $query = "select dm.id as id,u.username as username,u.steamid as steamid,dm.market_name as market_name,dm.image_hash as image_hash,dm.price as price,dm.created_at as created_at,dm.updated_at as updated_at,dm.bot_id as bot_id from pubg_drops dm, users u where dm.user_id = u.id and dm.status='accepted'";
        $products = DB::select($query);
        $pr = collect($products);
        return Datatables::of($pr)
            ->editColumn('market_name', function ($product) {
                return '<img src="https://steamcommunity-a.akamaihd.net/economy/image/' . $product->image_hash . '/100fx80f/image.png" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>' . $product->market_name . '</strong';
            })
            ->editColumn('bot_id', function ($product) {
                return '<a href="' . url(Config::get('app.admin_prefix') . "/bots/$product->bot_id") . '">' . $product->bot_id . '</a>';
            })
            ->editColumn('price', function ($product) {
                return '<span class="label label-success"><i class="icon-price-tags"></i> ' . ($product->price / 100) . ' RUB</span>';
            })
            ->make(true);
    }
    public function getPubgIndex()
    {
        return view('admin.edit_price_products.pubg');
    }
    public function getPubgAllProduct()
    {
        $products = DB::select("SELECT * FROM `pubg_available_products`");
        return $products;
    }
    public function savePubgAllProduct(Request $request)
    {

        $product_price = $request->input('price');
        $product_id = $request->input('id');
        $assetid = DB::table('pubg_available_products')->select('assetid')->where('id','=',$product_id)->get();
        $csgotm_assetid = DB::table('pubgtm_items')->select('assetid')->where('assetid','=',$assetid[0]->assetid)->get();
        if(count($csgotm_assetid)>0){
            DB::table('pubg_available_products')
                ->where('id', $product_id)
                ->update(['price' => $product_price]);
            DB::table('pubgtm_items')
                ->where('assetid', $csgotm_assetid[0]->assetid)
                ->update(['price' => $product_price/100]);
        }
        else{
            DB::table('pubg_available_products')
                ->where('id', $product_id)
                ->update(['price' => $product_price]);
            $data = DB::table('pubg_available_products')->where('id','=',$product_id)->get();
            DB::table('pubgtm_items')->insert([
                'bot_id' =>$data[0]->bot_id,
                'status' =>'received',
                'classId' =>$data[0]->classid,
                'instanceId' =>$data[0]->instanceid,
                'price' =>$data[0]->price/100,
                'csgotm_purchase_id' =>1,
                'csgotm_bot_id' =>1,
                'tradeofferid' =>1111111111,
                'tradeid' =>1111111111111111111111,
                'assetid' =>$data[0]->assetid,
            ]);


        };
    }

}
