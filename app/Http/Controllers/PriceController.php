<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\BotRequest;
use App\Models\PurchasePrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Datatables;
use Lang;

/**
 * Class BotController
 * @package App\Http\Controllers
 */
class PriceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.prices.index');
    }

    public function getData()
    {
        return Datatables::of(PurchasePrice::with('product'))->editColumn('product.name', function($price){
            $name = (Lang::has('weapon.'.$price->product->name)) ? trans('weapon.'.$price->product->name) : $price->product->name;
            if(!empty($price->product->short_description)) {
                $description = (Lang::has('weapon.'.$price->product->short_description)) ? trans('weapon.'.$price->product->short_description) : $price->product->short_description;
                return $name.' | '.$description;
            }
            return $name;
        })->make(true);
    }

}
