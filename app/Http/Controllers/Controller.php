<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public $user;

	/**
	 * @var
	 */
	protected static $lang;

	public function __construct() {
		if(Auth::check()){
			$this->user = Auth::user();
		}
	}

	/**
	 * @param $template
	 *
	 * @return string
	 */
	protected static function getView($template){
		if(Request::ajax()) {
			if(empty(self::$lang)){
				return $template.'_inc';
			}else{
				return $template.'_ext';
			}
		}

		return $template;
	}
}
