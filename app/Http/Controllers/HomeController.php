<?php namespace App\Http\Controllers;

use App\Models\Bot;
use App\Models\Game;
use App\Models\LiveTower;
use App\Models\Operation;
use App\Models\Product;
use App\Models\Tower;
use App\Models\TowerSetting;
use App\Models\Upgrade;
use App\Models\UpgradeSetting;
use Partnership;
use App\Models\Affiliate;
use App\Models\AffiliateLevel;
use App\Models\Balance;
use App\Models\Card;
use App\Models\User;
use App\Models\Category;
use App\Models\SteamCase;
use App\Models\DropsMarket;
use App\Models\DotaDropsMarket;
use App\Models\PubgDropsMarket;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Lang;
use App\Models\Drop;
use App\Models\Setting;
use App\Models\Currency;
use Helper;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\AvailableProduct;
use App\Models\DotaAvailableProduct;
use JavaScript;
use Config;
use ProvablyFair;
use App\Models\PubgCases\PubgCaseDrops;
use App\Models\CsgoCaseDrops;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        #Auth::login(User::find(31));
        $token = Auth::check() ? Redis::get('token_' . Auth::user()->steamid) : '';
        $user_id = Auth::check() ? Auth::user()->id : 0;
        JavaScript::put([
            'BASE_URL' => url('/'),
            'LIVE_DROP_PORT' => Config::get('broadcasting.live_drop_port'),
            'token' => $token,
            'user_id' => $user_id,
        ]);

        self::$lang = $request->input('lang');
    }

    public function getIndex()
    {
       return redirect('tower');

        //$Card = new Card();
        $categories = Category::orderBy('sort')->get();
        $lang = Helper::get('lang')->locale;
        //$CARD = $Card->getActiveCard(true);

        if (Session::get('g2a_status')) {
            switch (Session::get('g2a_status')) {
                case 'success':
                    JavaScript::put(['g2a_success' => trans('messages.payment_success')]);
                    break;

                case 'fail':
                    JavaScript::put(['g2a_fail' => trans('messages.payment_error') . ', ' . trans('messages.contact') . ' support@csgotower.net']);
                    break;
            }
        }

        return view(self::getView('site.index'), [
            'categories' => $categories,
            'custom_lang' => $lang,
            //'CARD' => $CARD,
        ]);
    }

    public function getFaq(Request $request)
    {
        $active_menu = $request->path();
        $lang = Helper::get('lang')->locale;

        return view(self::getView('site.faq'), [
            'active_menu' => $active_menu,
            'custom_lang' => $lang
        ]);
    }

    public function getGuarantee()
    {
        return view(self::getView('site.guarantee'));
    }

    public function getReviews()
    {
	    return view(self::getView('site.reviews'));
    }

	public function getTos()
	{
		return view(self::getView('site.tos'));
	}

    public function getContest()
    {
	    return view(self::getView('site.contest'));
    }

    public function getAffiliates(Request $request)
    {
	    if (!Auth::check()) {
		    return $this->getIndex();
	    }
        $active_menu = $request->path();

        $user = Auth::user();
        Partnership::updateLevel($user);

        $bonus_balance = Balance::
        where('user_id', $user->id)
            ->where('type', 1)
            ->where('currency_id', Session::get('currency_id'))
            ->first();

        if (!$next_level = AffiliateLevel::where('level', $user->affiliate_level + 1)->first()) {
            $next_level = AffiliateLevel::where('level', $user->affiliate_level)->first();
        }
        $next_border_amount = $next_level->border_amount;

        return view(self::getView('site.affiliates'), [
            'active_menu' => $active_menu,
            'user' => $user,
            'bonus_balance' => $bonus_balance,
            'levels' => AffiliateLevel::all(),
            'next_border_amount' => $next_border_amount,
            'referrals_amount' => Affiliate::where('refer_id', $user->id)->count(),
        ]);

    }

    public function getAgreement()
    {
        return view(self::getView('site.agreement'));
    }

    public function getTradeRestrict()
    {
        return view(self::getView('site.restrictions'));
    }

    public function getContacts()
    {
        return view(self::getView('site.contacts'));
    }

    public function getTrades()
    {
        $trades = Drop::where('status', 'accepted')
            ->orderBy('updated_at', 'desc')
            ->take(20)
            ->get();

        return view(self::getView('site.trades'), [
            'trades' => $trades
        ]);
    }

    public function getProfile($id)
    {
        if (!$user = User::where('id', $id)->first()) {
            abort(404);
        }

        if (Auth::check() && Auth::user()->id == $id) {
            $user = Auth::user();
            $orders = Order::where('user_id', $user->id)->orderBy('id', 'desc')->get();
            $last_payment = Order::where('user_id', $user->id)->where('status', 'complete')->orderBy('id', 'desc')->first();
            JavaScript::put([
                'token' => Redis::get('token_' . $user->steamid)
            ]);

	        $games = Game::where('user_id', Auth::user()->id)->take(100)->get();
            $bet=Tower::where('user_id',$id)->get();

            $count = Tower::where('user_id', $user->id)->where('status', 'closed')->count();

            $count_csgo = CsgoCaseDrops::where('user_id',$id)->count();
            $count_pubg = PubgCaseDrops::where('user_id',$id)->count();

            $count = $count + $count_csgo + $count_pubg;
            $drops_count = $count_csgo + $count_pubg;

            $count_csgo_market = DropsMarket::where('user_id',$id)->count();
            $count_dota_market = DotaDropsMarket::where('user_id',$id)->count();
            $count_pubg_market = PubgDropsMarket::where('user_id',$id)->count();

            $drops_count_market = $count_csgo_market + $count_dota_market + $count_pubg_market;

	        $received_items_csgo = collect(DropsMarket::where('user_id',Auth::user()->id)->where('status','=','accepted')->get());
	        $received_items_dota = collect(DotaDropsMarket::where('user_id',Auth::user()->id)->where('status','=','accepted')->get());
	        $received_items_pubg = collect(PubgDropsMarket::with('product')->where('user_id',Auth::user()->id)->where('status','=','accepted')->get());

            $received_items_csgo->map(function ($csgo) {
                $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

                $csgo['nameParsed'] = $ex->name;
                $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

                $csgo['profile_drop_type'] = 'csgo';
                return $csgo;
            });

            $received_items_dota->map(function ($dota) {
                $dota['profile_drop_type'] = 'dota';
                return $dota;
            });

            $received_items_pubg->map(function ($pubg) {
                $pubg['profile_drop_type'] = 'pubg';
                return $pubg;
            });

            $profile_drops_market = $received_items_csgo->merge($received_items_dota);
            $profile_drops_market = $profile_drops_market->merge($received_items_pubg)->sortByDesc('created_at')->forPage(1, 50);

	        $profile_items_csgo = collect(CsgoCaseDrops::where('user_id',Auth::user()->id)->orderBy('id', 'desc')->get());
            $profile_items_pubg = collect(PubgCaseDrops::with('product')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->get());

            $pending_items_csgo_count = CsgoCaseDrops::where('user_id',Auth::user()->id)->where('status','=','pending')->count();
            $pending_items_pubg_count = PubgCaseDrops::where('user_id',Auth::user()->id)->where('status','=','pending')->count();

            $profile_items_csgo->map(function ($csgo) {
                $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

                $csgo['nameParsed'] = $ex->name;
                $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

                $csgo['profile_drop_type'] = 'csgo';
                return $csgo;
            });

            $profile_items_pubg->map(function ($pubg) {
                $pubg['profile_drop_type'] = 'pubg';
                return $pubg;
            });

            $profile_drops = $profile_items_csgo->merge($profile_items_pubg)->sortByDesc('created_at')->forPage(1, 50);

            return view(self::getView('site.profile'), [
                'own'                   => true,
                'user'                  => $user,
                'games'                 => $games,
                'count'                 => $count,
                'drops_count'           => $drops_count,
                'drops_count_market'    => $drops_count_market,
                'orders'                => $orders,
                'last_payment'          => $last_payment,
                'profile_drops'         => $profile_drops,
                'profile_drops_market'  => $profile_drops_market,
                'pending_csgo_count'    => $pending_items_csgo_count,
                'pending_pubg_count'    => $pending_items_pubg_count,
            ]);
        }

        $games = Game::where('user_id', $user->id)->take(100)->get();

        $count = Tower::where('user_id', $user->id)->where('status', 'closed')->count();

        $count_csgo = CsgoCaseDrops::where('user_id',$id)->count();
        $count_pubg = PubgCaseDrops::where('user_id',$id)->count();

        $count = $count + $count_csgo + $count_pubg;
        $drops_count = $count_csgo + $count_pubg;

        $count_csgo_market = DropsMarket::where('user_id',$id)->count();
        $count_dota_market = DotaDropsMarket::where('user_id',$id)->count();
        $count_pubg_market = PubgDropsMarket::where('user_id',$id)->count();

        $drops_count_market = $count_csgo_market + $count_dota_market + $count_pubg_market;

        $received_items_csgo = collect(DropsMarket::where('user_id',$id)->where('status','=','accepted')->get());
        $received_items_dota = collect(DotaDropsMarket::where('user_id',$id)->where('status','=','accepted')->get());
        $received_items_pubg = collect(PubgDropsMarket::with('product')->where('user_id',$id)->where('status','=','accepted')->get());

        $received_items_csgo->map(function ($csgo) {
            $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

            $csgo['nameParsed'] = $ex->name;
            $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

            $csgo['profile_drop_type'] = 'csgo';
            return $csgo;
        });

        $received_items_dota->map(function ($dota) {
            $dota['profile_drop_type'] = 'dota';
            return $dota;
        });

        $received_items_pubg->map(function ($pubg) {
            $pubg['profile_drop_type'] = 'pubg';
            return $pubg;
        });

        $profile_drops_market = $received_items_csgo->merge($received_items_dota);
        $profile_drops_market = $profile_drops_market->merge($received_items_pubg)->sortByDesc('created_at')->forPage(1, 50);

        $profile_items_csgo = collect(CsgoCaseDrops::where('user_id',$id)->orderBy('id', 'desc')->get());
        $profile_items_pubg = collect(PubgCaseDrops::with('product')->where('user_id',$id)->orderBy('id', 'desc')->get());

        $profile_items_csgo->map(function ($csgo) {
            $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

            $csgo['nameParsed'] = $ex->name;
            $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

            $csgo['profile_drop_type'] = 'csgo';
            return $csgo;
        });

        $profile_items_pubg->map(function ($pubg) {
            $pubg['profile_drop_type'] = 'pubg';
            return $pubg;
        });

        $profile_drops = $profile_items_csgo->merge($profile_items_pubg)->sortByDesc('created_at')->forPage(1, 50);

        return view(self::getView('site.profile'), [
            'own'                   => false,
            'user'                  => $user,
            'count'                 => $count,
            'drops_count'           => $drops_count,
            'drops_count_market'    => $drops_count_market,
            'games'                 => $games,
            'profile_drops'         => $profile_drops,
            'profile_drops_market'  => $profile_drops_market,
            'pending_csgo_count'    => null,
            'pending_pubg_count'    => null,
        ]);
    }

    public function getNext50Case($page, $user_id)
    {
        $page = $page + 1;
        $profile_items_csgo = collect(CsgoCaseDrops::where('user_id',$user_id)->orderBy('id', 'desc')->get());
        $profile_items_pubg = collect(PubgCaseDrops::with('product')->where('user_id',$user_id)->orderBy('id', 'desc')->get());

        $profile_items_csgo->map(function ($csgo) {
            $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

            $csgo['nameParsed'] = $ex->name;
            $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

            $csgo['profile_drop_type'] = 'csgo';
            return $csgo;
        });

        $profile_items_pubg->map(function ($pubg) {
            $pubg['profile_drop_type'] = 'pubg';
            return $pubg;
        });

        $drops = $profile_items_csgo->merge($profile_items_pubg)->sortByDesc('created_at')->forPage($page, 50);

        return view('site.partials.profile-guest-drops', [
            'own' => Auth::check() && Auth::user()->id == $user_id,
            'drops' => $drops,
        ]);
    }

    public function getNext50Market($page, $user_id)
    {
        $page = $page + 1;

        $received_items_csgo = collect(DropsMarket::where('user_id',$user_id)->where('status','=','accepted')->get());
        $received_items_dota = collect(DotaDropsMarket::where('user_id',$user_id)->where('status','=','accepted')->get());
        $received_items_pubg = collect(PubgDropsMarket::with('product')->where('user_id',$user_id)->where('status','=','accepted')->get());

        $received_items_csgo->map(function ($csgo) {
            $ex = Helper::parseMarketNameCsgoProfile($csgo->market_name);

            $csgo['nameParsed'] = $ex->name;
            $csgo['shortDescriptionParsed'] = $ex->short_description_with_quality;

            $csgo['profile_drop_type'] = 'csgo';
            return $csgo;
        });

        $received_items_dota->map(function ($dota) {
            $dota['profile_drop_type'] = 'dota';
            return $dota;
        });

        $received_items_pubg->map(function ($pubg) {
            $pubg['profile_drop_type'] = 'pubg';
            return $pubg;
        });

        $drops = $received_items_csgo->merge($received_items_dota);
        $drops = $drops->merge($received_items_pubg)->sortByDesc('created_at')->forPage($page, 50);

        return view('site.partials.profile-guest-drops', [
            'own' => Auth::check() && Auth::user()->id == $user_id,
            'drops' => $drops,
        ]);
    }

    public function urlStringToArr($url)
    {
        $parsed_url = parse_url($url);

        if (empty($parsed_url['query'])) {
            return false;
        }
        $str = $parsed_url['query'];
        $res_arr = [];
        $arr = explode('&', $str);

        foreach ($arr as $item) {
            list($key, $value) = explode('=', $item);
            $res_arr[$key] = $value;
        }

        return $res_arr;
    }

    public function postSettings(Request $request)
    {
        $ret = ['success' => true, 'notify' => true, 'message' => trans('messages.profile_sttings_success')];

        $params = $this->urlStringToArr($request->input('trade_url'));

        if (empty($params['partner']) || empty($params['token'])) {
            $ret['message'] = trans('messages.invalid_trade_url');
            $ret['success'] = false;

            return response()->json($ret);
        }

        /*if ($params['partner'] != (\Auth::user()->steamid - 76561197960265728)) {
            $ret['message'] = trans('messages.alien_trade_url');
            $ret['success'] = false;

            return response()->json($ret);
        }*/

        $user = User::find(Auth::user()->id);
        $user->trade_url = $request->input('trade_url');
        $user->partner_id = $params['partner'];
        $user->token = $params['token'];
        $ret['success'] = $user->save();

        if (!$ret['success']) {
            $ret['message'] = trans('messages.profile_sttings_error');
        }

        return response()->json($ret);
    }

    public function postSettingsModal(Request $request)
    {
        $ret = ['success' => true, 'notify' => true, 'message' => trans('messages.profile_sttings_success')];

        $params = $this->urlStringToArr($request->input('trade_url1'));

        if (empty($params['partner']) || empty($params['token'])) {
            $ret['message'] = trans('messages.invalid_trade_url');
            $ret['success'] = false;

            return response()->json($ret);
        }

        /*if ($params['partner'] != (\Auth::user()->steamid - 76561197960265728))
        {
            $ret['message'] = trans('messages.alien_trade_url');
            $ret['success'] = false;

            return response()->json($ret);
        }*/

        $user = User::find(Auth::user()->id);
        $user->trade_url = $request->input('trade_url1');
        $user->partner_id = $params['partner'];
        $user->token = $params['token'];
        $ret['success'] = $user->save();

        if (!$ret['success']) {
            $ret['message'] = trans('messages.profile_sttings_error');
        }

        return response()->json($ret);
    }

    public function getCase($id)
    {
        $case = SteamCase::find($id);
	    if(!$case){
		    abort(404);
	    }
        $weapon = [];

        $products = $case->products()
            ->orderBy(DB::raw('RAND()'))
            ->take(25)
            ->get();

	    $highest_prize = $case->products()
		    ->select('price')
		    ->orderBy('price', 'desc')
		    ->first()->price / 100;

        foreach ($products as $w) {
            $name = (Lang::has('weapon.' . $w['name'])) ? trans('weapon.' . $w['name']) : $w['name'];
	        $description = (Lang::has('weapon.' . $w['short_description'])) ? trans('weapon.' . $w['short_description']) : $w['short_description'];
            $weapon[] = [
                'name' => $name,
                'short_description' => $description,
                'class' => $w['class'],
                'image' => ($w['image_source'] == 'url') ? $w['image'] : asset('images/products') . "/" . $w['image']
            ];
        }

        JavaScript::put([
            'WEAPON' => json_encode($weapon),
            'case_id' => $case->id
        ]);

        if (Auth::check()) {
            $user_seed = ProvablyFair::getUserSeed($case->id);
            $server_seed = ProvablyFair::getServerSeed();
            Session::put('user_seed.case_' . $case->id . '.attempt_1', $user_seed);
            Session::put('server_seed.case_' . $case->id . '.attempt_1', $server_seed);

	        $previous_game = Drop::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
	        if($previous_game){
		        $previous_game->hash = hash('sha256', $previous_game->user_seed + $previous_game->server_seed + $previous_game->win_number);
	        }
        }
        $win_numbers = ProvablyFair::getWinNumbers($id, 'simple');


        return view(self::getView('site.case'), [
            'page_case' => true,
            'case' => $case,
            'win_numbers' => $win_numbers,
            'user_seed' => @$user_seed,
            'server_seed' => @$server_seed,
	        'previous_game' => @$previous_game,
	        'highest_prize' => $highest_prize
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTop()
    {

        $top_general = User::where('profit', '>', 0)
            ->where('top_banned', 0)
            ->orderBy('profit', 'desc')
            ->take(3)
            ->get();

        $top_day = Game::where('created_at', '>', Carbon::now()->subDay())
            ->whereIn('user_id' , function($f){
                $f
                    ->select('id')
                    ->from('users')
                    ->where('top_banned', 0);
            })
            ->orderBy('profit', 'desc')
            ->take(2)
            ->get();


        $top_week = Game::where('created_at', '>', Carbon::now()->subWeek())
            ->whereIn('user_id' , function($f){
                $f
                    ->select('id')
                    ->from('users')
                    ->where('top_banned', 0);
            })
            ->orderBy('profit', 'desc')
            ->take(5)
            ->get();

        $top_month = Game::where('created_at', '>', Carbon::now()->subMonth())
            ->whereIn('user_id' , function($f){
                $f
                    ->select('id')
                    ->from('users')
                    ->where('top_banned', 0);
            })
            ->orderBy('profit', 'desc')
            ->take(5)
            ->get();

        return view(self::getView('site.top'), [
            'top_general' => $top_general,
            'top_day' => $top_day,
            'top_week' => $top_week,
            'top_month' => $top_month,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSell(Request $request)
    {
        $ret = ['success' => true, 'notify' => true];

        try {
            $drop = Drop::find($request->input('drop_id'));

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $sell_price = $drop->sell();

            $ret['message'] = trans('messages.sold');

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['success' => false]);
        }

        $ret['sell_price'] = $sell_price;

        return response()->json($ret);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSellProfile($id)
    {
        $ret = ['success' => true, 'notify' => true];

        try {
            $drop = Drop::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $drop->sell();

            $ret['message'] = trans('messages.sold');

        } catch (\Exception $e) {
            DB::rollback();
            $ret['success'] = false;
        }

        return response()->json($ret);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function acceptDrop($id)
    {
        $ret = ['success' => true];

        try {
            $drop = Drop::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $drop->status = 'requested';
            $drop->save();

            $this->sendPrize($drop, Auth::user());
        } catch (\Exception $e) {
            $ret['success'] = false;
        }

        return response()->json($ret);
    }

    private function sendPrize(Drop $drop, User $user)
    {
        Redis::publish(Config::get('app.redis_prefix') . 'send-drop-' . $drop->bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }


    public function csgoDrop(Request $request)
    {
        $drop_id_arr = $request->drop_ids;
        $bot_id_node = '';
        $arr_drop = [];

        $user_id = Auth::user()->id;
        $ret = ['success' => true];
        foreach ($drop_id_arr as $d){
            try {
                $drop = DropsMarket::find($d);

                $bot_id_node = $drop->bot_id;

                if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                    $ret['success'] = false;

                    return response()->json($ret);
                }
                $drop->status = 'requested';
                $drop->save();
                array_push($arr_drop,$drop);
            } catch (\Exception $e) {
                $ret['success'] = false;
            }
        }
        Log::info($bot_id_node);
        Log::info($arr_drop);
        $this->sendWidthdraw($bot_id_node, $arr_drop, Auth::user());
        return response()->json($ret);
    }

    public function dotaDrop($id)
    {
        $user_id = Auth::user()->id;
        $ret = ['success' => true];

        try {
            /*$drops = DB::table('drops_market')->where('user_id','=',$user_id)->get();
            foreach ($drops as $drop){

            };*/
            $drop = DotaDropsMarket::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $drop->status = 'requested';
            $drop->save();

            $this->sendDotaWidthdraw($drop, Auth::user());
        } catch (\Exception $e) {
            //dd($e);
            $ret['success'] = false;
        }

        return response()->json($ret);
    }

    public function pubgDrop($id)
    {
        $user_id = Auth::user()->id;
        $ret = ['success' => true];
        try {
            $drop = PubgDropsMarket::find($id);
            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;
                return response()->json($ret);
            }
            $drop->status = 'requested';
            $drop->save();
            $this->sendPubgWidthdraw($drop, Auth::user());
        } catch (\Exception $e) {
            $ret['success'] = false;
        }
        return response()->json($ret);
    }

    public function csgoCaseDrop($id)
    {
        $user_id = Auth::user()->id;
        $ret = ['success' => true, 'bot_id' => 0, 'status' => 'pending', 'offer_id' => 0];
        try {
            $drop = CsgoCaseDrops::find($id);
            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;
                return response()->json($ret);
            }
            $drop->status = 'requested';
            $drop->save();

            $ret['bot_id'] = $drop->bot_id;
            $ret['status'] = $drop->status;
            $ret['offer_id'] = $drop->offer_id;

            $this->sendCsgoCaseWidthdraw($drop, Auth::user());
        } catch (\Exception $e) {
            $ret['success'] = false;
        }
        return response()->json($ret);
    }

    public function pubgCaseDrop($id)
    {
        $user_id = Auth::user()->id;
        $ret = ['success' => true, 'bot_id' => 0, 'status' => 'pending', 'offer_id' => 0];

        DB::beginTransaction();
        try {
            $drop = PubgCaseDrops::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;
                return response()->json($ret);
            }

            $drop->status = 'requested';
            $drop->save();

            $ret['bot_id'] = $drop->bot_id;
            $ret['status'] = $drop->status;
            $ret['offer_id'] = $drop->offer_id;

            $this->sendPubgCaseWidthdraw($drop, Auth::user());

            DB::commit();
        } catch (\Exception $e) {
            $ret['success'] = false;
            DB::rollback();
        }
        return response()->json($ret);
    }

    private function sendWidthdraw($bot_id, $drop, User $user)
    {
        Redis::publish(Config::get('app.redis_prefix') . 'widthdraw-csgo-'.$bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }

    private function sendDotaWidthdraw(DotaDropsMarket $drop, User $user)
    {

        Redis::publish(Config::get('app.redis_prefix') . 'widthdraw-dota-'.$drop->bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }

    private function sendPubgWidthdraw(PubgDropsMarket $drop, User $user)
    {

        Redis::publish(Config::get('app.redis_prefix') . 'pubg-widthdraw-'.$drop->bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }

    private function sendCsgoCaseWidthdraw(CsgoCaseDrops $drop, User $user)
    {
        Redis::publish(Config::get('app.redis_prefix') . 'csgo-case-widthdraw-'.$drop->bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }

    private function sendPubgCaseWidthdraw(PubgCaseDrops $drop, User $user)
    {
        Redis::publish(Config::get('app.redis_prefix') . 'pubg-case-widthdraw-'.$drop->bot_id, json_encode(['accountId' => $user->steamid, 'token' => $user->token, 'drop' => $drop]));
    }

    /**
     * @param $promo_code
     */
    public function promoCode($promo_code)
    {
        // Validation
        if (!Auth::check()) {
            //
        }
        $user_id = Auth::user()->id;
        if (!$refer = User::where('id', $promo_code)
            ->orWhere('promo_code', $promo_code)
            ->exists()
        ) {
            //
        }
        if ($refer->id == $user_id) {
            //
        }
        if (Affiliate::where('user_id', $user_id)
            ->where('refer_id', $refer->id)
            ->exists()
        ) {
            //
        }

        try {
            $Affiliate = new Affiliate();
            $Affiliate->user_id = $user_id;
            $Affiliate->refer_id = $refer->id;
            $Affiliate->save();


        } catch (\Exception $e) {
            Log::error($e->getMessage());
            //
        }


    }

    public function getProvablyFair()
    {
        return view(self::getView('site.provably-fair'), [

        ]);
    }

    public function getCsgomarket()
    {
        $active_menu = 'market';
        return view(self::getView('site.csgo',['active_menu' => $active_menu]));
    }

    public function getdota2market()
    {
        $active_menu = 'market';

        return view(self::getView('site.dota2',['active_menu' => $active_menu]));

    }

    public function getpubgmarket()
    {
        $active_menu = 'market';

        return view(self::getView('site.pubg',['active_menu' => $active_menu]));

    }

    public function getCoinFlip()
    {
        $active_menu = 'coin-flip';

        return view(self::getView('site.coin_flip', ['active_menu' => $active_menu]));
    }

    public function getTowerRoulette(Request $request)
    {
        $active_menu = 'tower-roulette';
        $top_general = User::where('profit', '>', 0)
            ->where('top_banned', 0)
            ->orderBy('profit', 'desc')
            ->take(10)
            ->get();

        return view('site.tower_roulette', [
            'active_menu' => $active_menu,
            'steps' => TowerSetting::where('key', 'steps')->first()->value,
            'live_games' => LiveTower::orderBy('id', 'desc')->get(),
            'total_games' => Tower::count(),
            'total_bets' => Tower::sum('bet'),
            'top' => $top_general,
        ]);
    }
    public function getSpiderJackpot()
    {

        return view(self::getView('site.spider-jackpot.spider_jackpot'));

    }

    public function csgoTradeCheck()
    {
        return response()->json(['check'=>'success_checked']);
    }


    public function postSellDropsMarket($id)
    {
        $ret = ['success' => true, 'notify' => true];

        try {
            $drop = DropsMarket::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $sell_price = $drop->sell();

            $ret['message'] = trans('messages.sold');

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['success' => false]);
        }

        $ret['sell_price'] = $sell_price;

        return response()->json($ret);
    }

    public function postSellDotaDropsMarket($id)
    {
        $ret = ['success' => true, 'notify' => true];

        try {
            $drop = DotaDropsMarket::find($id);

            if ($drop->status != 'pending' || $drop->user_id != Auth::user()->id) {
                $ret['success'] = false;

                return response()->json($ret);
            }

            $sell_price = $drop->sell();

            $ret['message'] = trans('messages.sold');

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['success' => false]);
        }

        $ret['sell_price'] = $sell_price;

        return response()->json($ret);
    }

    public function getSettings()
    {
        //$rub_usd_sell = (float)Setting::find(1)->rub_usd_sell;
        $rub_usd_sell = (float)Helper::setting('rub_usd_sell');
        //test
        return response()->json($rub_usd_sell);
    }

	public function getUpgrade()
	{
		if(!Auth::check()){
			return view(self::getView('site.upgrade'));
		}
		$products = Product::where('class', 'chip')->orderBy('price')->get();

		$company_percent = UpgradeSetting::where('key', 'profit')->first()->value;
		$multipliers = [1.5, 2, 5, 10];
		$settings = [];

		for($i = 0; $i < count($multipliers); $i++){
			$settings[$i]['multiplier'] = $multipliers[$i];
			$base_chance = 1 / $multipliers[$i];
			$settings[$i]['chance'] = round($base_chance * (100 - $company_percent));
		}

		if($prev_game = Upgrade::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first()){
			$prev_game->hash = hash('sha256', $prev_game->user_seed + $prev_game->server_seed + $prev_game->win_number);
		}

		$user_seed = ProvablyFair::getUserSeed();
		$server_seed = ProvablyFair::getServerSeed();
		Session::put('user_seed.case_0.attempt_upgrade', $user_seed);
		Session::put('server_seed.case_0.attempt_upgrade', $server_seed);

		return view(self::getView('site.upgrade'), [
			'settings' => $settings,
			'products' => $products,
			'prev_game' => @$prev_game,
			'next_game' => [
				'user_seed' => $user_seed,
				'server_seed' => $server_seed
			],
		]);
	}

	public function getTower(Request $request)
	{
	    $active_menu = $request->path();

        $top_general = User::where('profit', '>', 0)
            ->where('top_banned', 0)
            ->orderBy('profit', 'desc')
            ->take(10)
            ->get();

		return view(self::getView('site.tower'), [
		    'active_menu' => $active_menu,
			'steps' => TowerSetting::where('key', 'steps')->first()->value,
			'live_games' => LiveTower::orderBy('id', 'desc')->get(),
			'total_games' => Tower::count(),
			'total_bets' => Tower::sum('bet'),
			'top' => $top_general,
		]);
	}

	public function battleCreate()
	{
		return view(self::getView('site.battle-create'), [
			'cases' => SteamCase::where('active', 1)->get(),
		]);
	}

    public function getLastTime(){
        $time = Helper::lastDropTime();
        Log::info($time);
        return response()->json($time);
    }

    public function getCsgoCase(Request $request){
        $active_menu = $request->path();

        $categories = Category::where('name','=','CSGO')->orderBy('sort')->get();
        $lang = Helper::get('lang')->locale;

        if (Session::get('g2a_status')) {
            switch (Session::get('g2a_status')) {
                case 'success':
                    JavaScript::put(['g2a_success' => trans('messages.payment_success')]);
                    break;

                case 'fail':
                    JavaScript::put(['g2a_fail' => trans('messages.payment_error') . ', ' . trans('messages.contact') . ' csgotowershelp@gmail.com']);
                    break;
            }
        }

        return view(self::getView('site.csgo-case'), [
            'active_menu' => $active_menu,
            'categories' => $categories,
            'custom_lang' => $lang,
        ]);
    }

    public function getPubgCase(Request $request){
        $active_menu = 'pubg-crate'; //$request->path();

        $categories = Category::where('name','=','PUBG')->orderBy('sort')->get();
        $lang = Helper::get('lang')->locale;

        if (Session::get('g2a_status')) {
            switch (Session::get('g2a_status')) {
                case 'success':
                    JavaScript::put(['g2a_success' => trans('messages.payment_success')]);
                    break;

                case 'fail':
                    JavaScript::put(['g2a_fail' => trans('messages.payment_error') . ', ' . trans('messages.contact') . ' csgotowerhelp@gmail.com']);
                    break;
            }
        }

        return view(self::getView('site.pubg-case'), [
            'active_menu' => $active_menu,
            'categories' => $categories,
            'custom_lang' => $lang,
        ]);
    }

    public function setOnceFree(Request $request)
    {

        $response = ['success' => false, 'message' => ''];

        if(!Auth::check())
        {
            $response['message'] = 'Sign in through Steam to claim your free 0.50 coins to get started!';
            return response()->json($response);
        }

        $referral = $request->input('referral');

        if($referral != 'CSGOTOWER')
        {
            $response['message'] = 'The referral item is incorrect!';
            return response()->json($response);
        }

        $user = Auth::user();

        if($user->once_free)
        {
            $response['message'] = 'You already activated referral!';
            return response()->json($response);
        }

        $bot = Bot::where('enabled', 1)->where('status_code', 1)->first();


        Redis::publish(Config::get('app.redis_prefix') . 'once-free-' . $bot->id, json_encode(['steam_id' => $user->steamid, 'user_id' => $user->id]));

        $response['success'] = true;
        $response['message'] = 'The request has been successfully send';

        return response()->json($response);

    }

}
