<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' 			=> \App\Http\Middleware\Authenticate::class,
		'auth.basic' 	=> \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
		'guest' 		=> \App\Http\Middleware\RedirectIfAuthenticated::class,
		'dashboard' 	=> \App\Http\Middleware\Dashboard::class,
		'admin' 		=> \App\Http\Middleware\Admin::class,
		'language'      => \App\Http\Middleware\Language::class,
		'rouletteStart' => \App\Http\Middleware\RouletteStart::class,
		'balance'       => \App\Http\Middleware\Balance::class,
		'steamUser'     => \App\Http\Middleware\Steam::class,
		'userActivity'	=> \App\Http\Middleware\UserActivity::class,
		'xss'			=> \App\Http\Middleware\XSSProtection::class,
		'dropTimedOut'  => \App\Http\Middleware\DropTimedOut::class,
		'tradeUrl'  	=> \App\Http\Middleware\TradeUrl::class,
		'withdraw'      => \App\Http\Middleware\Withdraw::class,
	];

}
