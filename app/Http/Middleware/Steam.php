<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Illuminate\Support\Facades\Log;
use JavaScript;
use Config;
use Bican\Roles\Models\Role;
use Illuminate\Support\Facades\Redis;

class Steam {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

	/**
	 * @param Guard $auth
	 */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->auth->check() && $this->auth->user()->level()==1){
            return $next($request);
        }

	    if($request->segment(1) == 'get-seeds' || $request->segment(1) == 'tower'){
		    return response()->json();
	    }

        return redirect()->guest('/');
    }

}
