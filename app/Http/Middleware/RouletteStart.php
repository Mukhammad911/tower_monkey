<?php

namespace App\Http\Middleware;

use Helper;
use App\Models\SteamCase;
use App\Models\PubgCases\PubgCase;
use Closure;
use App\Models\Balance;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

/**
 * Class RouletteStart
 * @package App\Http\Middleware
 */
class RouletteStart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !$request->ajax() ) {
            return response('Forbidden', 403);
        }
        if ($request->is('roulette/play')) {
            $case = SteamCase::find($request->input('case_id'));
            if($request->input('attempts')==1){
                $total_price = $case->priceObj->price * 1;
            }
            else if($request->input('attempts')==2){
                $total_price = $case->priceObj->price * 2;
            }
            else if($request->input('attempts')==3){
                $total_price = $case->priceObj->price * 3;
            }
            else if($request->input('attempts')==4){
                $total_price = $case->priceObj->price * 4;
            }
            else if($request->input('attempts')==5){
                $total_price = $case->priceObj->price * 5;
            }
            else{
                Log::info('Hack Ajax call attemts user_id: ', Auth::user()->id);
                return response()->json(['success'=>false, 'error'=>'Please, check you attempts']);
            }
            if( !$case->active ){
                return response()->json(['success'=>false, 'error'=>'case disabled']);
            }

            if ( Helper::get('balance')->summ < $total_price ) {
                return response()->json(['success'=>false, 'error'=>'money_required']);
            }
            $bonus_balance = Balance::where('user_id', Auth::user()->id)
                ->where('type', 2)
                ->where('currency_id', 1)
                ->first();

            // debit money
            if(Auth::user()->youtuber){
                if ( !Helper::debit($total_price, 1,'case opening youtube') ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            elseif ($bonus_balance->summ > 0){
                if ( !Helper::debit($total_price, 1, 'case opening from promo') ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            else
            {
                if ( !Helper::debit($total_price, 1) ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            $request->merge(array("last_balance" => $bonus_balance->summ));

            return $next($request);
        }
        else if ($request->is('roulette-csgo/play'))
        {
            $case = SteamCase::find($request->input('case_id'));
            if($request->input('attempts')==1){
                $total_price = $case->priceObj->price * 1;
            }
            else if($request->input('attempts')==2){
                $total_price = $case->priceObj->price * 2;
            }
            else if($request->input('attempts')==3){
                $total_price = $case->priceObj->price * 3;
            }
            else if($request->input('attempts')==4){
                $total_price = $case->priceObj->price * 4;
            }
            else if($request->input('attempts')==5){
                $total_price = $case->priceObj->price * 5;
            }
            else{
                Log::info('Hack Ajax call attemts user_id: ', Auth::user()->id);
                return response()->json(['success'=>false, 'error'=>'Please, check you attempts']);
            }
            if( !$case->active ){
                return response()->json(['success'=>false, 'error'=>'case disabled']);
            }

            if ( Helper::get('balance')->summ < $total_price ) {
                return response()->json(['success'=>false, 'error'=>'money_required']);
            }
            $bonus_balance = Balance::where('user_id', Auth::user()->id)
                ->where('type', 2)
                ->where('currency_id', 1)
                ->first();

            // debit money
            if(Auth::user()->youtuber){
                if ( !Helper::debit($total_price, 1,'csgo case opening youtube') ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            elseif ($bonus_balance->summ > 0){
                if ( !Helper::debit($total_price, 1, 'csgo case opening from promo') ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            else{
                if ( !Helper::debit($total_price, 1,'csgo case opening') ) {
                    return response()->json(['success'=>false, 'error'=>'money_required']);
                }
            }
            $request->merge(array("last_balance" => $bonus_balance->summ));

            return $next($request);
        }
        else if ($request->is('roulette-pubg/play'))
        {
            $case = PubgCase::find($request->input('case_id'));

            if ($request->input('attempts') == 1)
            {
                $total_price = $case->priceObj->price * 1;
            }
            else if ($request->input('attempts') == 2)
            {
                $total_price = $case->priceObj->price * 2;
            }
            else if($request->input('attempts') == 3)
            {
                $total_price = $case->priceObj->price * 3;
            }
            else if($request->input('attempts') == 4)
            {
                $total_price = $case->priceObj->price * 4;
            }
            else if($request->input('attempts') == 5)
            {
                $total_price = $case->priceObj->price * 5;
            }
            else
            {
                Log::info('Hack Ajax call attemts user_id: ', Auth::user()->id);

                return response()->json([
                    'success'=>false,
                    'error'=>'Please, check you attempts'
                ]);
            }

            if (!$case->active)
            {
                return response()->json([
                    'success'=>false,
                    'error'=>'case disabled'
                ]);
            }

            if (Helper::get('balance')->summ < $total_price)
            {
                return response()->json([
                    'success'=>false,
                    'error'=>'money_required'
                ]);
            }

            $bonus_balance = Balance::where('user_id', Auth::user()->id)
                ->where('type', 2)
                ->where('currency_id', 1)
                ->first();

            // debit money
            if(Auth::user()->youtuber)
            {
                if (!Helper::debit($total_price, 1, 'pubg case opening youtube'))
                {
                    return response()->json([
                        'success'=>false,
                        'error'=>'money_required'
                    ]);
                }
            }
            else if ($bonus_balance->summ > 0)
            {
                if (!Helper::debit($total_price, 1, 'pubg case opening from promo'))
                {
                    return response()->json([
                        'success'=>false,
                        'error'=>'money_required'
                    ]);
                }
            }
            else
            {
                if (!Helper::debit($total_price, 1, 'pubg case opening'))
                {
                    return response()->json([
                        'success'=>false,
                        'error'=>'money_required'
                    ]);
                }
            }

            $request->merge(array("last_balance" => $bonus_balance->summ));

            return $next($request);
        }

    }
}
