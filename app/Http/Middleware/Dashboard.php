<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Bican\Roles\Models\Role;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Config;

class Dashboard {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Application $app, Guard $auth)
    {
        $this->app = $app;
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->app->setLocale('ru');

        if ($this->auth->check())
        {
            if($this->auth->user()->level()>1){
                return $next($request);
            }
            else{
                return redirect('/');
            }
        }

        return redirect()->guest(Config::get('app.admin_prefix').'/login');
    }

}
