<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'pay/ipn',
	    'payment/skin-pay/push-back',
	    'login',
        'profile/settings',
        'tower/get-opened-game',
        'tower/multiplier',
        'tower/start',
        'tower/open-cell',
        'tower/get-seeds',
        'tower/set-seeds',
        'tower-roulette-bet',
        'tower-roulette-load-users',
        'chat-add-message',
        'tower/take-profit',

    ];
}
