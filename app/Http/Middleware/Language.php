<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Session;
use App\Models\Language as AcceptedLang;
use App\Models\Currency;
use Auth;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use Config;

class Language{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public $rusLocales = ['ru','uk'];

	public function __construct(Application $app, Redirector $redirector, Request $request) {
		$this->app = $app;
		$this->redirector = $redirector;
		$this->request = $request;
	}

	public function handle($request, Closure $next)
	{
		if(!empty(Session::get('locale'))){
			$this->app->setLocale(Session::get('locale'));
		}
		else{
			$locale = (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) ? $this->getBrowserLocale($request) : strtolower($this->app->getLocale());

			Session::put('locale', $locale);

			$this->app->setLocale($locale);
		}

		//$currency_id = AcceptedLang::getByLocale()->currency_id;
		$currency_id = 1;

		Session::put('currency_id', $currency_id);

		return $next($request);
	}

	protected function getBrowserLocale($request)
	{
		$websiteLanguages = AcceptedLang::getActiveLanguages()->lists('locale')->toArray();

		if(empty($websiteLanguages)){
			return strtolower($this->app->getLocale());
		}
		// Credit: https://gist.github.com/Xeoncross/dc2ebf017676ae946082
		// Parse the Accept-Language according to:
		// http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.4
		preg_match_all(
			'/([a-z]{1,8})' .       // M1 - First part of language e.g en
			'(-[a-z]{1,8})*\s*' .   // M2 -other parts of language e.g -us
			// Optional quality factor M3 ;q=, M4 - Quality Factor
			'(;\s*q\s*=\s*((1(\.0{0,3}))|(0(\.[0-9]{0,3}))))?/i',
			$request->server('HTTP_ACCEPT_LANGUAGE'),
			$langParse);

		$langs = $langParse[1]; // M1 - First part of language
		$quals = $langParse[4]; // M4 - Quality Factor

		$numLanguages = count($langs);
		$langArr = array();

		for ($num = 0; $num < $numLanguages; $num++)
		{
			$newLang = $langs[$num];
			$newQual = isset($quals[$num]) ?
				(empty($quals[$num]) ? 1.0 : floatval($quals[$num])) : 0.0;

			// Choose whether to upgrade or set the quality factor for the
			// primary language.
			$langArr[$newLang] = (isset($langArr[$newLang])) ?
				max($langArr[$newLang], $newQual) : $newQual;
		}

		// sort list based on value
		// langArr will now be an array like: array('EN' => 1, 'ES' => 0.5)
		arsort($langArr, SORT_NUMERIC);

		// The languages the client accepts in order of preference.
		$acceptedLanguages = array_keys($langArr);
		
		// Set the most preferred language that we have a translation for.
		foreach ($acceptedLanguages as $preferredLanguage)
		{
			//dd(is_array($websiteLanguages));
			if (in_array($preferredLanguage, $websiteLanguages))
			{
				return (in_array($preferredLanguage, $this->rusLocales)) ? 'ru' : strtolower($preferredLanguage);
			}

		}

		return strtolower($this->app->getLocale());
	}

}
