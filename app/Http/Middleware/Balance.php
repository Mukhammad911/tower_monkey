<?php namespace App\Http\Middleware;

use App\Models\PubgAvailableProduct;
use App\Models\AvailableProduct;
use App\Models\DotaAvailableProduct;
use App\Models\Order;
use App\Models\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Helper;
use Auth;
use App\Models\SteamCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Balance {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->is('save-csgo-drop/*')) {
            $exist_balance = Order::selectRaw('sum(amount) as amount')->where('user_id','=',Auth::user()->id)->where('status','=','complete')->get();
            foreach ($exist_balance as $balance){
                if((float)$balance->amount<2){
                    return response()->json(['success'=>false, 'error'=>'you_not_have_g2a_transaction']);
                }
            }
            $product = AvailableProduct::find($request->route('id'));

            $p = Helper::exchange($product->price, 1);

            if (Helper::get('balance')->summ < $p) {
                return response()->json(['success'=>false, 'error'=>'money_required_market']);
            }
        }
        else if ($request->is('save-dota-drop/*')) {
            $exist_balance = Order::selectRaw('sum(amount) as amount')->where('user_id','=',Auth::user()->id)->where('status','=','complete')->get();
            foreach ($exist_balance as $balance){
                if((float)$balance->amount<2){
                    return response()->json(['success'=>false, 'error'=>'you_not_have_g2a_transaction']);
                }
            }
            $product = DotaAvailableProduct::find($request->route('id'));

            $p = Helper::exchange($product->price, 1);

            if (Helper::get('balance')->summ < $p) {
                return response()->json(['success'=>false, 'error'=>'money_required_market']);
            }
        }
        else if  ($request->is('save-pubg-drop/*')) {
            $exist_balance = Order::selectRaw('sum(amount) as amount')->where('user_id','=',Auth::user()->id)->where('status','=','complete')->get();
            foreach ($exist_balance as $balance){
                if((float)$balance->amount<2){
                    return response()->json(['success'=>false, 'error'=>'you_not_have_g2a_transaction']);
                }
            }
            $product = PubgAvailableProduct::find($request->route('id'));

            $p = Helper::exchange($product->price, 1);

            if (Helper::get('balance')->summ < $p) {
                return response()->json(['success'=>false, 'error'=>'money_required_market']);
            }
        }
        else
        {
            if  (!$request->ajax()) {
                return response('Forbidden', 403);
            }

            $case = SteamCase::find($request->input('case_id'));
            $priceObj = $case->priceObj;

            if(!$case->active){
                return response()->json(['success'=>false, 'error'=>'case disabled']);
            }

            if (Helper::get('balance')->summ < $priceObj->price) {
                return response()->json(['success'=>false, 'error'=>'money_required']);
            }

            // debit money
            if (!Helper::debit($case->priceObj->price, $case->priceObj->currency_id)) {
                return response()->json(['success'=>false, 'error'=>'money_required']);
            }
        }

        return $next($request);
    }

}
