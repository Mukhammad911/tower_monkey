<?php

namespace App\Http\Middleware;

use App\Models\DotaDropsMarket;
use App\Models\DropsMarket;
use App\Models\PubgDropsMarket;
use Auth;
use Closure;
use DB;
use Helper;

/**
 * Class Withdraw
 * @package App\Http\Middleware
 */
class Withdraw
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->withdraw_banned ){
            if (Auth::user()->youtuber){
                return response()->json([
                    'success' => false,
                    'stat' => 'withdraw_lock',
                    'message' => 'Withdraw Locked for Content Maker by Administrator',
                ], 422);
            }
            else{
                return response()->json([
                    'success' => false,
                    'stat' => 'withdraw_lock',
                    'message' => 'Withdraw Locked for Content Maker by Administrator',
                ], 422);
            }
        }
        if ($request->is('save-csgo-drop/*')) {

            $csgo_drops = DropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $dota_drops = DotaDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $pubg_drops = PubgDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $csgo_drops_sum = round(($csgo_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $dota_drops_sum = round(($dota_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $pubg_drops_sum = round(($pubg_drops / 100) / Helper::setting('rub_usd_sell'), 2);

            $drops_sum = $csgo_drops_sum + $dota_drops_sum + $pubg_drops_sum;


            if ($drops_sum >= 1000)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'You can not withdraw skins within 24 hours',
                ], 422);
            }

            $check = DB::table('markets_access')->where('name','=','CSGO')->get();
            foreach ($check as $ch){
                if($ch->status==1){
                    return response()->json([
                        'success' => false,
                        'message' => 'MARKET CSGO IS TEMPORARY UNAVAILABLE',
                    ], 422);
                }
            }
        }
        if ($request->is('save-dota-drop/*')) {

            $csgo_drops = DropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $dota_drops = DotaDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $pubg_drops = PubgDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $csgo_drops_sum = round(($csgo_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $dota_drops_sum = round(($dota_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $pubg_drops_sum = round(($pubg_drops / 100) / Helper::setting('rub_usd_sell'), 2);

            $drops_sum = $csgo_drops_sum + $dota_drops_sum + $pubg_drops_sum;


            if ($drops_sum >= 1000)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'You can not withdraw skins within 24 hours',
                ], 422);
            }

            $check = DB::table('markets_access')->where('name','=','DOTA2')->get();
            foreach ($check as $ch){
                if($ch->status==1){
                    return response()->json([
                        'success' => false,
                        'message' => 'MARKET DOTA2 IS TEMPORARY UNAVAILABLE',
                    ], 422);
                }
            }
        }
        if ($request->is('save-pubg-drop/*')) {

            $csgo_drops = DropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $dota_drops = DotaDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $pubg_drops = PubgDropsMarket::where('user_id', Auth::user()->id)
                ->whereRaw("created_at >= DATE_ADD(NOW(), INTERVAL -24 HOUR)")
                ->where(function($q) {
                    $q->where('status', 'requested')
                        ->orWhere('status', 'offered')
                        ->orWhere('status', 'accepted');
                })
                ->sum('price');

            $csgo_drops_sum = round(($csgo_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $dota_drops_sum = round(($dota_drops / 100) / Helper::setting('rub_usd_sell'), 2);
            $pubg_drops_sum = round(($pubg_drops / 100) / Helper::setting('rub_usd_sell'), 2);

            $drops_sum = $csgo_drops_sum + $dota_drops_sum + $pubg_drops_sum;


            if ($drops_sum >= 1000)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'You can not withdraw skins within 24 hours',
                ], 422);
            }

            $check = DB::table('markets_access')->where('name','=','PUBG')->get();
            foreach ($check as $ch){
                if($ch->status==1){
                    return response()->json([
                        'success' => false,
                        'message' => 'MARKET PUBG IS TEMPORARY UNAVAILABLE',
                    ], 422);
                }
            }
        }

        return $next($request);
    }
}
