<?php namespace App\Http\Middleware;

use App\Models\Order;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Helper;
use Session;

class TradeUrl {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

	/**
	 * @param Guard $auth
	 */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->trade_url)
        {
            return response()->json(['success'=>false,'error'=>'trade_url_required']);
        }

	    $orders_balance = Order::where('user_id', Auth::user()->id)
		    ->where('currency_id', Session::get('currency_id'))
		    ->where('status', 'complete')
		    ->sum('amount');

	    if(Auth::user()->bonus_used == 1 && $orders_balance < 1){
		    return response()->json(['success'=>false, 'error'=>'Fund your account with at least $1.00']);
	    }

        return $next($request);
    }

}
