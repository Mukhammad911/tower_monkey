<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Models\AvailableProduct;
use Helper;
use Illuminate\Support\Facades\Redis;

class DropTimedOut
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->check()){

            //Перемещение дропов в available_products которые не забрали в течение часа

            $this->sellTimedOut();

            $this->requestedTimedOut();

            if(is_null(Redis::get('token_'.$this->auth->user()->steamid))){
                $token = $this->auth->user()->id.md5(uniqid(mt_rand(), true));

                Redis::set('token_'.$this->auth->user()->steamid, $token);
            }
        }
        return $next($request);
    }

    private function requestedTimedOut(){
        $this->auth->user()
                ->drops()
                ->whereNotActiveCardExists()
                ->whereRaw("updated_at < DATE_ADD(NOW(), INTERVAL -10 MINUTE)")
                ->where('status', 'requested')
                ->update(['status'=>'pending']);
    }

    private function sellTimedOut(){
        $drops = $this->auth->user()
            ->drops()
            ->whereNotActiveCardExists()
            ->whereRaw("created_at < DATE_ADD(NOW(), INTERVAL -1 HOUR)")
            ->where('status', 'pending')
            ->get();

        foreach ($drops as $drop) {
            $drop->sell();
        }
    }
}
