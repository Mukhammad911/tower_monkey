<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//PayOp route
Route::post('pay/ipn', 	['middleware'=>'xss', 'uses' => 'Api\PaymentController@postPayOpIpn']);
Route::get('pay/return', 'Api\PaymentController@postPayOpReturn');
//G2a pay ipn route
#Route::post('pay/ipn', 	['middleware'=>'xss', 'uses' => 'Api\PaymentController@postG2payIpn']);
//Route::get('pay/ipn/test', 	['uses' => 'TestController@testPay']);
//SkinPay push back
Route::post('payment/skin-pay/push-back', 	['middleware'=>'xss', 'uses' => 'Api\PaymentController@skinPayAnswer']);

Route::get('refresh-token', function(){
	return csrf_token();
});
Route::group(['middleware' => ['xss','language']], function() {
	Route::any('language/{lang}', ['as' => 'language', 'uses' => function($language){
		Session::set('locale', $language);

		return redirect()->back();
	}]);
	Route::get('testtest', function(){
		$pass = bcrypt("Mega33333");
		echo($pass);
	});

	Route::post('once_free', 'HomeController@setOnceFree');

	/*Route::get('/', 'HomeController@getIndex');*/
	Route::get('faq', 'HomeController@getFaq');
	Route::get('agreement', 'HomeController@getAgreement');
	Route::get('tos', 'HomeController@getTos');
    Route::get('trade-restrictions', 'HomeController@getTradeRestrict');
	Route::get('guarantee', 'HomeController@getGuarantee');
	Route::get('reviews', 'HomeController@getReviews');
	Route::get('top-winners', 'HomeController@getTop');
	Route::get('contacts', 'HomeController@getContacts');
	Route::get('trades', 'HomeController@getTrades');
	Route::get('contest', 'HomeController@getContest');
	Route::get('affiliates', 'HomeController@getAffiliates');
	Route::get('p/{promo_code}', 'Admin\AffiliateController@promoCode');
	Route::get('provably-fair', 'HomeController@getProvablyFair');
	//Route::get('upgrade', 'HomeController@getUpgrade');
	Route::get('tower', 'HomeController@getTower');
	Route::get('csgo', 'HomeController@getCsgomarket')->name('csgomarket.homepage.index');
	Route::get('dota2', 'HomeController@getdota2market');
	Route::get('pubg', 'HomeController@getpubgmarket');
	Route::get('battle-create', 'HomeController@battleCreate');

    Route::get('csgo-crate', 'HomeController@getCsgoCase');
   // Route::get('/', 'HomeController@getPubgCase');

    //Coin Flip
   // Route::get('coin-flip', 'HomeController@getCoinFlip');
   // Route::post('coin-flip/check-auth', 'Api\CoinFlipController@checkAuth');
   // Route::post('coin-flip/check-balance', 'Api\CoinFlipController@checkBalance');
    //Route::post('coin-flip/start-game', 'Api\CoinFlipController@startGame');
   // Route::post('coin-flip/start-join', 'Api\CoinFlipController@joinGame');
   // Route::post('coin-flip/load-games', 'Api\CoinFlipController@loadGame');

    //TowerRoulette
//    Route::get('/', 'HomeController@getTowerRoulette');
Route::get('/', function(){
	return redirect('csgo');
	    });
    Route::get('tower-roulette-get-balance', 'TowerRouletteController@getBalance');
    Route::get('tower-roulette-check-auth', 'TowerRouletteController@checkUserAuth');
    Route::post('tower-roulette-bet', 'TowerRouletteController@rouletteBet');
    Route::get('tower-roulette-load-users', 'TowerRouletteController@loadUsers');
	Route::get('tower-roulette-get-jackpot', 'TowerRouletteController@getJackpot');
	Route::get('tower-roulette-get-history', 'TowerRouletteController@getHistory');

    //ChatController
    Route::get('chat-default', 'ChatController@getDefault');
    Route::post('chat-add-message', 'ChatController@addMessage');

    //Spider-Jackpot
    Route::get('spider-jackpot', 'HomeController@getSpiderJackpot');

    Route::get('/get/csgo/skins', 'Api\MarketCsgoController@getCsgoskins');
    Route::get('/get/dota2/skins', 'Api\MarketDotaController@getDota2skins');
    Route::get('/get/pubg/skins', 'Api\MarketPubgController@getPubgskins');
    Route::get('/get/dota2/heroes', 'Api\MarketDotaController@getDota2heroes');
    Route::get('/get/csgo/classes', 'Api\MarketCsgoController@getCsgoAllClasses');
    Route::get('/get/csgo/qualities', 'Api\MarketCsgoController@getCsgoAllQuality');
    Route::get('/get/active/trade/offer/csgo', 'Api\MarketCsgoController@getActiveTradeOffer');
    Route::get('/get/csgo/skins/balance', 'Api\MarketCsgoController@getBalance');
    Route::get('/get/offerid', 'Api\MarketCsgoController@getOfferId');
    Route::get('/get/csgo/bot_id', 'Api\MarketCsgoController@getBotId');
    Route::get('/get/csgo/skins/profile', 'Api\MarketCsgoController@getCsgoProfile');
    Route::get('/get/dota/skins/profile', 'Api\MarketDotaController@getDotaProfile');
    Route::get('/get/pubg/skins/profile', 'Api\MarketPubgController@getPubgProfile');
    Route::get('/get/csgo/skins/profile/count', 'Api\MarketCsgoController@getCsgoProfileModal');
    Route::get('/get/dota/skins/profile/count', 'Api\MarketDotaController@getDotaProfileModal');
    Route::get('/get/csgo/balance', 'Api\MarketCsgoController@getBalanceWithdrawable');
    Route::get('/get/settings', 'HomeController@getSettings');

	Route::get('case/{id}/{switch_type?}', 'HomeController@getCase');

    Route::get('csgo-crate/{id}/{switch_type?}', 'Api\CsgoCaseController@getCsgoCase');
    Route::get('pubg-crate/{id}/{switch_type?}', 'Api\PubgCaseController@getPubgCase');

	Route::get('profile/{id}', ['middleware' => 'dropTimedOut', 'uses'=>'HomeController@getProfile']);
	//Route::get('profile/{id}/{steamid}/{profit}', ['middleware' => 'dropTimedOut', 'uses'=>'HomeController@getProfile']);
	Route::get('next50case/{page}/{user_id}', ['uses'=>'HomeController@getNext50Case']);
	Route::get('next50market/{page}/{user_id}', ['uses'=>'HomeController@getNext50Market']);

	Route::group(['middleware' => 'steamUser'], function(){
		/*Route::post('cards/trial', 'CardsController@postTrial');
		Route::post('cards/open', ['as' => 'cards.open', 'middleware' => ['balance'], 'uses'=>'DropController@postWin']);
		Route::post('cards/erase', 'CardsController@postErase');
		Route::post('cards/extend', 'CardsController@postExtend');*/

		Route::post('profile/settings', ['uses' => 'HomeController@postSettings']);
        Route::post('profile/settingsmodal', ['uses' => 'HomeController@postSettingsModal']);

		Route::post('sell','HomeController@postSell');
        Route::post('sell_item/{id}','HomeController@postSellProfile');
		Route::post('accept-drop/{id}', ['middleware' => 'tradeUrl', 'uses'=>'HomeController@acceptDrop']);

        Route::get('accept-csgo-drop', ['middleware' => ['tradeUrl'], 'uses'=>'HomeController@csgoDrop']);
        Route::get('accept-dota-drop/{id}', ['middleware' => ['tradeUrl'], 'uses'=>'HomeController@dotaDrop']);
        Route::get('accept-pubg-drop/{id}', ['middleware' => ['tradeUrl'], 'uses'=>'HomeController@pubgDrop']);

        Route::get('save-csgo-drop/{id}', ['middleware' => ['withdraw','balance'], 'uses' => 'CsgoDropController@saveAvailableProduct']);
        Route::get('save-dota-drop/{id}', ['middleware' => ['withdraw','balance'], 'uses' => 'DotaDropController@saveAvailableProduct']);
        Route::get('save-pubg-drop/{id}', ['middleware' => ['withdraw','balance'], 'uses' => 'PubgDropController@saveAvailableProduct']);
        Route::get('trade/check', ['middleware' => 'tradeUrl', 'uses'=>'HomeController@csgoTradeCheck']);

		// API
		Route::post('pay', ['uses'=>'Api\PaymentController@postMake']);
        Route::get('success', 'Api\PaymentController@getSuccess');
		Route::get('fail', 'Api\PaymentController@getFail');

		Route::post('profile/trade-url/update', 'Api\UserController@updateTradeUrl');
		Route::post('profile/update/promo-code', 'Api\UserController@updatePromoCode');
		Route::post('profile/use-promo-code/{promo_code}', 'Api\UserController@usePromoCode');
		Route::post('calculate-hash', 'Api\CaseController@CalculateHash');

		/*Route::post('cashout/paypal', 'Api\WithdrawalController@paypal');
		Route::post('cashout/visa', 'Api\WithdrawalController@visa');
		Route::post('cashout/bitcoin', 'Api\WithdrawalController@bitcoin');*/

		Route::post('roulette/get-win-numbers', 'Api\RouletteController@getWinNumbers');
		Route::post('roulette/get-seeds', 'Api\RouletteController@getSeeds');
		Route::post('roulette/play', ['middleware' => ['rouletteStart'], 'uses' => 'Api\RouletteController@play']);

		Route::post('tower/start', 'Api\TowerController@start');
		Route::post('tower/get-opened-game', 'Api\TowerController@getOpenedGame');
		Route::post('tower/get-seeds', 'Api\TowerController@getSeeds');
		Route::post('tower/set-seeds', 'Api\TowerController@setSeeds');
		Route::post('tower/open-cell', 'Api\TowerController@openCell');
		Route::post('tower/take-profit', 'Api\TowerController@takeProfit');
		Route::post('tower/multiplier', 'Api\TowerController@getMultiplier');

		Route::post('upgrade', 'Api\UpgradeController@upgrade');
		Route::post('upgrade/get-custom-chance', 'Api\UpgradeController@getCustomChance');
		Route::post('upgrade/get-seeds', 'Api\UpgradeController@getSeeds');

        //csgo-case
        Route::post('roulette-csgo/play', ['middleware' => ['rouletteStart'], 'uses' => 'Api\CsgoCaseController@play']);
        Route::get('save-csgo-case-drop/{id}', ['middleware' => ['tradeUrl','withdraw'], 'uses' => 'HomeController@csgoCaseDrop']);
        Route::get('csgo-sell-item/{id}', 'Api\CsgoCaseController@sellItem');
        Route::get('csgo-sell-all-items', 'Api\CsgoCaseController@sellAllItems');
        Route::get('my-csgo-case-drop', 'Api\CsgoCaseController@myCsgoCaseDrops');

        //pubg-case
        Route::post('roulette-pubg/play', ['middleware' => ['rouletteStart'], 'uses' => 'Api\PubgCaseController@play']);
        Route::get('save-pubg-case-drop/{id}', ['middleware' => ['tradeUrl','withdraw'], 'uses' => 'HomeController@pubgCaseDrop']);
        Route::get('pubg-sell-item/{id}', 'Api\PubgCaseController@sellItem');
        Route::get('pubg-sell-all-items', 'Api\PubgCaseController@sellAllItems');
        Route::get('my-pubg-case-drop', 'Api\PubgCaseController@myPubgCaseDrops');
	});
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get(Config::get('app.admin_prefix').'/login','Auth\AuthController@getLogin');
Route::post(Config::get('app.admin_prefix').'/login','Auth\AuthController@postLogin');
Route::get('steamlogin', 'Auth\AuthController@getSteamLogin');

Route::get('logout','Auth\AuthController@getLogout');

// ADMIN
Route::group(['prefix'=>Config::get('app.admin_prefix'), 'middleware' => 'dashboard'], function()
{
    Route::get('/','Admin\AdminController@getIndex');
	Route::get('profile', 'Admin\UserController@getProfile');
	Route::post('profile/update', 'Admin\UserController@postUpdateProfile');

	Route::group(['middleware'=>'admin'], function(){
		Route::get('users', 'Admin\UserController@getUsers');
		Route::get('user/autocomplete', 'Admin\UserController@getAutocomplete');
		Route::model('user', '\App\Models\User');
		Route::resource('user', 'Admin\UserController');
		Route::get('login-like-user/{user_id}', 'Admin\AdminController@loginLikeUser');

		Route::get('operations', 'Admin\OperationController@getIndex');
		Route::model('operation', '\App\Models\Operation');
		Route::resource('operation', 'Admin\OperationController');

		Route::get('orders', 'Admin\OrderController@getOrders');
		Route::model('order', '\App\Models\Order');
		Route::resource('order', 'Admin\OrderController');

		Route::get('withdrawals', 'Admin\WithdrawalController@getIndex');
		Route::model('withdrawal', '\App\Models\Withdrawal');
		Route::resource('withdrawal', 'Admin\WithdrawalController');

		Route::get('imbalances', 'Admin\ImbalanceController@getIndex');
		Route::model('imbalance', '\App\Models\Imbalance');
		Route::resource('imbalance', 'Admin\ImbalanceController');

		Route::get('categories', 'Admin\CategoryController@getCategories');
		Route::get('category/sortup/{id}', 'Admin\CategoryController@getSortUp');
		Route::get('category/sortdown/{id}', 'Admin\CategoryController@getSortDown');
		Route::model('category', '\App\Models\Category');
		Route::resource('category', 'Admin\CategoryController');

		Route::get('cases', 'Admin\CaseController@getCases');
		Route::get('case/{id}/products', 'Admin\CaseController@getProducts');
		Route::get('case-products', 'Admin\CaseController@getCaseProducts');
		Route::get('delete/product-case', 'Admin\CaseController@getDeleteProductCase');
        Route::get('deletecase/{id}', 'Admin\CaseController@destroy');

		// Cases
		Route::get('case/get/products/{case_id}', 'Api\CaseController@getProducts');
		Route::get('case/sortup/{id}', 'Admin\CaseController@getSortUp');
		Route::get('case/sortdown/{id}', 'Admin\CaseController@getSortDown');
		Route::get('case/autocomplete', 'Admin\CaseController@getCaseAutocomplete');
		Route::post('cases/change-positions', 'Admin\CaseController@postChangePositions');
		Route::get('case/{id}/product-list', 'Admin\CaseController@getProductsList');
		Route::post('case/attach-product', 'Admin\CaseController@postAttachProducts');
		Route::model('case', '\App\Models\SteamCase');
		Route::resource('case', 'Admin\CaseController');
		Route::get('case/{case}/purchases', 'Admin\CaseController@getAvailableProducts');

        // Pubg Cases
        Route::get('pubg/case/get/products/{case_id}', 'Api\PubgCaseController@getProducts');
        Route::get('pubg/case/sortup/{id}', 'Admin\PubgCases\PubgCaseController@getSortUp');
        Route::get('pubg/case/sortdown/{id}', 'Admin\PubgCases\PubgCaseController@getSortDown');
        Route::get('pubg/case/autocomplete', 'Admin\PubgCases\PubgCaseController@getCaseAutocomplete');
        Route::post('pubg/cases/change-positions', 'Admin\PubgCases\PubgCaseController@postChangePositions');
        Route::get('pubg/case/{id}/product-list', 'Admin\PubgCases\PubgCaseController@getProductsList');
        Route::post('pubg/case/attach-product', 'Admin\PubgCases\PubgCaseController@postAttachProducts');
        Route::model('pubg_case', '\App\Models\PubgCases\PubgCase');
        Route::resource('pubg_case', 'Admin\PubgCases\PubgCaseController');
        Route::get('pubg_case/{pubg_case}/purchases', 'Admin\PubgCases\PubgCaseController@getAvailableProducts');

        Route::get('pubg/cases', 'Admin\PubgCases\PubgCaseController@getCases');
        Route::get('pubg_case/{id}/products', 'Admin\PubgCases\PubgCaseController@getProducts');
        Route::get('pubg/case-products', 'Admin\PubgCases\PubgCaseController@getCaseProducts');
        Route::get('pubg/delete/product-case', 'Admin\PubgCases\PubgCaseController@getDeleteProductCase');
        Route::get('pubg/deletecase/{id}', 'Admin\PubgCases\PubgCaseController@destroy');

        // Provably Fair CSGO Cases
        Route::resource('fair-csgo', 'Admin\FairCsgoCaseController');
        Route::get('fair-csgo/{id}/products', 'Admin\FairCsgoCaseController@getProducts');
        Route::get('fair-csgo-case/case-products/{case_id}', 'Admin\FairCsgoCaseController@getCaseProducts');
        Route::get('fair-csgo-case/save-chance', 'Admin\FairCsgoCaseController@saveChanceProduct');

        Route::get('fair-csgo-case', 'Admin\FairCsgoCaseController@getCases');

        // Provably Fair PUBG Cases
        Route::resource('fair-pubg', 'Admin\FairPubgCaseController');
        Route::get('fair-pubg/{id}/products', 'Admin\FairPubgCaseController@getProducts');
        Route::get('fair-pubg-case/case-products/{case_id}', 'Admin\FairPubgCaseController@getCaseProducts');
        Route::get('fair-pubg-case/save-chance', 'Admin\FairPubgCaseController@saveChanceProduct');

        Route::get('fair-pubg-case', 'Admin\FairPubgCaseController@getCases');

		// Products
		Route::get('products', 'Admin\ProductController@getProducts');
		Route::get('product/sortup/{id}/{case_id}', 'Admin\ProductController@getSortUp');
		Route::get('product/sortdown/{id}/{case_id}', 'Admin\ProductController@getSortDown');
		Route::get('product-case/autocomplete', 'Admin\ProductController@getCaseAutocomplete');
		Route::get('product/autocomplete', 'Admin\ProductController@getProductAutocomplete');
		Route::model('product', '\App\Models\Product');
		Route::resource('product', 'Admin\ProductController');

		// Dota2
		Route::get('dota-products', 'DotaProductController@getProducts');
		Route::get('dota-product/sortup/{id}/{case_id}', 'DotaProductController@getSortUp');
		Route::get('dota-product/sortdown/{id}/{case_id}', 'DotaProductController@getSortDown');
		Route::get('dota-product-case/autocomplete', 'DotaProductController@getCaseAutocomplete');
		Route::get('dota-product/autocomplete', 'DotaProductController@getProductAutocomplete');
		Route::model('dota-product', '\App\Models\DotaProduct');
		Route::resource('dota-product', 'DotaProductController');

		// Pubg
		Route::get('pubg-products', 'PubgProductController@getProducts');
		Route::get('pubg-product/sortup/{id}/{case_id}', 'PubgProductController@getSortUp');
		Route::get('pubg-product/sortdown/{id}/{case_id}', 'PubgProductController@getSortDown');
		Route::get('pubg-product-case/autocomplete', 'PubgProductController@getCaseAutocomplete');
		Route::get('pubg-product/autocomplete', 'PubgProductController@getProductAutocomplete');
		Route::model('pubg-product', '\App\Models\PubgProduct');
		Route::resource('pubg-product', 'PubgProductController');

        //Csgo, Dota2, Pubg Case trades
        Route::get('/csgo-case-trades', 'Admin\CsgoCases\CsgoCaseAvailableProductsController@getTrades');
        Route::get('/get-csgo-case-products-trades', 'Admin\CsgoCases\CsgoCaseAvailableProductsController@getTradesProducts');

        Route::get('/dota-case-trades', 'Admin\DotaCases\DotaCaseAvailableProductsController@getTrades');
        Route::get('/get-dota-case-products-trades', 'Admin\DotaCases\DotaCaseAvailableProductsController@getTradesProducts');

        Route::get('/pubg-case-trades', 'Admin\PubgCases\PubgCaseAvailableProductsController@getTrades');
        Route::get('/get-pubg-case-products-trades', 'Admin\PubgCases\PubgCaseAvailableProductsController@getTradesProducts');


        Route::get('available-products', 'Admin\AvailableProductsController@getIndex');
        Route::resource('available-product', 'Admin\AvailableProductsController');

        Route::get('csgo-case-available-products', 'Admin\CsgoCases\CsgoCaseAvailableProductsController@getIndex');
        Route::resource('csgo-case-available-product', 'Admin\CsgoCases\CsgoCaseAvailableProductsController');

        Route::get('dota-case-available-products', 'Admin\DotaCases\DotaCaseAvailableProductsController@getIndex');
        Route::resource('dota-case-available-product', 'Admin\DotaCases\DotaCaseAvailableProductsController');

        Route::get('dota-available-products', 'DotaAvailableProductsController@getIndex');
        Route::resource('dota-available-product', 'DotaAvailableProductsController');

        Route::get('pubg-case-available-products', 'Admin\PubgCases\PubgCaseAvailableProductsController@getIndex');
        Route::resource('pubg-case-available-product', 'Admin\PubgCases\PubgCaseAvailableProductsController');

        Route::get('pubg-available-products', 'PubgAvailableProductsController@getIndex');
        Route::resource('pubg-available-product', 'PubgAvailableProductsController');

        //Csgo Edit Available Products
        Route::get('edit-csgo-available-products', 'Admin\AvailableProductsController@getCsgoIndex');
        Route::get('get-edit-csgo-available-products', 'Admin\AvailableProductsController@getCsgoAllProduct');
        Route::post('save-edit-csgo-available-products', 'Admin\AvailableProductsController@saveCsgoAllProduct');

        //Dota2 Edit Available Products
        Route::get('edit-dota-available-products', 'DotaAvailableProductsController@getDotaIndex');
        Route::get('get-edit-dota-available-products', 'DotaAvailableProductsController@getDotaAllProduct');
        Route::post('save-edit-dota-available-products', 'DotaAvailableProductsController@saveDotaAllProduct');

        //PUBG Edit Available Products
        Route::get('edit-pubg-available-products', 'PubgAvailableProductsController@getPubgIndex');
        Route::get('get-edit-pubg-available-products', 'PubgAvailableProductsController@getPubgAllProduct');
        Route::post('save-edit-pubg-available-products', 'PubgAvailableProductsController@savePubgAllProduct');

        //Csgo, Dota2, Pubg trades
        Route::get('/csgo-trades', 'Admin\AvailableProductsController@getTrades');
        Route::get('/get-csgo-products-trades', 'Admin\AvailableProductsController@getTradesProducts');
        Route::get('/dota-trades', 'DotaAvailableProductsController@getTrades');
        Route::get('/get-dota-products-trades', 'DotaAvailableProductsController@getTradesProducts');
        Route::get('/pubg-trades', 'PubgAvailableProductsController@getTrades');
        Route::get('/get-pubg-products-trades', 'PubgAvailableProductsController@getTradesProducts');


        // Statistics
		Route::get('statistics/orders', 'Admin\StatisticsController@getOrders');
		Route::get('statistics/orders-list', 'Admin\StatisticsController@getOrdersList');
		Route::get('statistics/finances', 'Admin\StatisticsController@getFinances');
		Route::get('statistics/games', 'Admin\StatisticsController@getGames');

		// Bots
		Route::model('bots', '\App\Models\Bot');
		Route::resource('bots', 'BotController');
        Route::group(['prefix' => 'bots'], function() {
            Route::get('{bots}/getCode', 'BotController@getAuthCode');
			Route::get('{bots}/enable', 'BotController@getEnable');
			Route::get('{bots}/disable', 'BotController@getDisable');
			Route::get('{bots}/purchases', 'PurchaseController@index');
        });

        // Dota_Bots
		Route::model('dota_bots', '\App\Models\DotaBot');
		Route::resource('dota_bots', 'DotaBotController');
        Route::group(['prefix' => 'dota_bots'], function() {
            Route::get('{dota_bots}/getCode', 'DotaBotController@getAuthCode');
			Route::get('{dota_bots}/enable', 'DotaBotController@getEnable');
			Route::get('{dota_bots}/disable', 'DotaBotController@getDisable');
			Route::get('{dota_bots}/purchases', 'DotaPurchaseController@index');
        });

        // PUBG_Bots
		Route::model('pubg_bots', '\App\Models\PubgBot');
		Route::resource('pubg_bots', 'PubgBotController');
        Route::group(['prefix' => 'pubg_bots'], function() {
            Route::get('{pubg_bots}/getCode', 'PubgBotController@getAuthCode');
			Route::get('{pubg_bots}/enable', 'PubgBotController@getEnable');
			Route::get('{pubg_bots}/disable', 'PubgBotController@getDisable');
			Route::get('{pubg_bots}/purchases', 'PubgPurchaseController@index');
        });

        // Csgo Case Bots
        Route::model('csgo_case_bots', '\App\Models\CsgoCaseBot');
        Route::resource('csgo_case_bots', 'CsgoCaseBotController');
        Route::group(['prefix' => 'csgo_case_bots'], function() {
            Route::get('{csgo_case_bots}/getCode', 'CsgoCaseBotController@getAuthCode');
            Route::get('{csgo_case_bots}/enable', 'CsgoCaseBotController@getEnable');
            Route::get('{csgo_case_bots}/disable', 'CsgoCaseBotController@getDisable');
            Route::get('{csgo_case_bots}/purchases', 'CsgoCaseBotController@index');
        });

        // Pubg Case Bots
        Route::model('pubg_case_bots', '\App\Models\PubgCases\PubgCaseBot');
        Route::resource('pubg_case_bots', 'Admin\PubgCases\PubgCaseBotController');
        Route::group(['prefix' => 'pubg_case_bots'], function() {
            Route::get('{pubg_case_bots}/getCode', 'Admin\PubgCases\PubgCaseBotController@getAuthCode');
            Route::get('{pubg_case_bots}/enable', 'Admin\PubgCases\PubgCaseBotController@getEnable');
            Route::get('{pubg_case_bots}/disable', 'Admin\PubgCases\PubgCaseBotController@getDisable');
            Route::get('{pubg_case_bots}/purchases', 'Admin\PubgCases\PubgCaseBotController@index');
        });

		// Purchases
		Route::get('purchases', 'PurchaseController@getPurchases');
		Route::get('purchase/product-autocomplete', 'PurchaseController@getProductAutocomplete');
		Route::model('purchase', '\App\Models\Purchase');
		Route::resource('purchase', 'PurchaseController');

		// Dota2  Purchases
		Route::get('dota-purchase', 'DotaPurchaseController@getPurchases');
		Route::get('dota-purchase/create', 'DotaPurchaseController@createPurchases');
		Route::post('dota-purchase/save', 'DotaPurchaseController@savePurchases');
		Route::post('dota-purchase/save/product', 'DotaPurchaseController@saveProductPurchases');
		Route::get('dota-purchase/get_product', 'DotaPurchaseController@getProductPurchases');
        Route::post('dota-purchase/save/complete', 'DotaPurchaseController@saveProductPurchasesComplete');
        Route::get('dota-purchase/create_form', 'DotaPurchaseController@createFormPurchases');
		Route::post('dota_delete/{id}', 'DotaPurchaseController@deletePurchases');

		// Pubg  Purchases
		Route::get('pubg-purchase', 'PubgPurchaseController@getPurchases');
		Route::get('pubg-purchase/create', 'PubgPurchaseController@createPurchases');
		Route::post('pubg-purchase/save', 'PubgPurchaseController@savePurchases');
		Route::post('pubg-purchase/save/product', 'PubgPurchaseController@saveProductPurchases');
		Route::get('pubg-purchase/get_product', 'PubgPurchaseController@getProductPurchases');
        Route::post('pubg-purchase/save/complete', 'PubgPurchaseController@saveProductPurchasesComplete');
        Route::get('pubg-purchase/create_form', 'PubgPurchaseController@createFormPurchases');
		Route::post('pubg_delete/{id}', 'PubgPurchaseController@deletePurchases');

		// CSGO  Purchases
		Route::get('csgo-purchase', 'CsgoPurchaseController@getPurchases');
		Route::get('csgo-purchase/create', 'CsgoPurchaseController@createPurchases');
		Route::post('csgo-purchase/save', 'CsgoPurchaseController@savePurchases');
		Route::post('csgo-purchase/save/product', 'CsgoPurchaseController@saveProductPurchases');
        Route::post('csgo-purchase/save/complete', 'CsgoPurchaseController@saveProductPurchasesComplete');
		Route::get('csgo-purchase/get_product', 'CsgoPurchaseController@getProductPurchases');
		Route::get('csgo-purchase/create_form', 'CsgoPurchaseController@createFormPurchases');
		Route::post('csgo_delete/{id}', 'CsgoPurchaseController@deletePurchases');

        /*Csgo Purchases Controller */
        Route::get('csgo-case-purchases', 'CsgoCasePurchaseController@getPurchases');
        Route::get('csgo-case-purchases/get/data', 'CsgoCasePurchaseController@getPurchases');
        Route::get('csgo-case-purchases/product-autocomplete', 'CsgoCasePurchaseController@getProductAutocomplete');
        Route::model('csgo-case-purchases', '\App\Models\CsgoCasePurchase');
        Route::resource('csgo-case-purchases', 'CsgoCasePurchaseController');

        /*PUBG Case Purchases Controller */
        Route::get('pubg-case-purchases', 'Admin\PubgCases\PubgCasePurchaseController@getPurchases');
        Route::get('pubg-case-purchases/get/data', 'Admin\PubgCases\PubgCasePurchaseController@getPurchases');
        Route::get('pubg-case-purchases/product-autocomplete', 'Admin\PubgCases\PubgCasePurchaseController@getProductAutocomplete');
        Route::model('pubg-case-purchases', '\App\Models\PubgCases\PubgCasePurchase');
        Route::resource('pubg-case-purchases', 'Admin\PubgCases\PubgCasePurchaseController');

		//Route::model('bots', '\App\Models\Bot');
		Route::group(['prefix' => 'prices'], function() {
			Route::get('/', 'PriceController@index');
			Route::get('/data', ['as' => 'prices.data', 'uses' => 'PriceController@getData']);
			//Route::get('{bots}/getCode', 'BotController@getAuthCode');
		});


		// Settings
		Route::get('settings/general', 'Admin\SettingsController@getGeneral');
		Route::post('settings', 'Admin\SettingsController@postGeneral');
		Route::get('settings/localization', 'Admin\SettingsController@getLocalization');
		Route::get('settings/languages', 'Admin\SettingsController@getLanguages');
		Route::get('settings/currency', 'Admin\SettingsController@getCurrency');
		Route::post('lang-active', 'Admin\SettingsController@postSetLangActive');
		Route::post('currency-active', 'Admin\SettingsController@postSetCurrencyActive');
		Route::post('language/set-currency', 'Admin\SettingsController@postLanguageSetCurrency');
		Route::get('settings/clear-project', 'Admin\SettingsController@getClearProject');
		Route::post('settings/clear-project', 'Admin\SettingsController@postClearProject');

		// Affiliates
		//Route::get('affiliate-levels', 'Admin\AffiliateLevelController@getIndex');
		//Route::model('affiliate-level', '\App\Models\AffiliateLevel');
		//Route::resource('affiliate-level', 'Admin\AffiliateLevelController');

		//Route::get('affiliates', 'Admin\AffiliateController@getIndex');
		//Route::model('affiliate', '\App\Models\Affiliate');
		//Route::resource('affiliate', 'Admin\AffiliateController');

		// Roulette Game
		Route::get('roulettes', 'Admin\RouletteController@getIndex');
		Route::model('roulette', '\App\Models\Drop');
		Route::resource('roulette', 'Admin\RouletteController');
		Route::get('roulette-statistics', 'Admin\RouletteController@statistics');

		Route::get('roulette-scenarios', 'Admin\RouletteScenarioController@getScenarios');
		Route::get('roulette-scenarios/drop-counters', 'Admin\RouletteScenarioController@dropCounters');
		Route::post('roulette-scenarios/delete', 'Admin\RouletteScenarioController@deleteSelected');
		Route::model('roulette-scenario', '\App\Models\RouletteScenario');
		Route::resource('roulette-scenario', 'Admin\RouletteScenarioController');

		Route::get('roulette-settings', 'Admin\RouletteSettingController@getIndex');
		Route::model('roulette-setting', '\App\Models\RouletteSetting');
		Route::resource('roulette-setting', 'Admin\RouletteSettingController');

        //Roulette CSGO CASE
        Route::get('csgo-case-roulettes', 'Admin\CsgoCaseRouletteController@getIndex');
        Route::model('csgo-case-roulette', '\App\Models\CsgoCaseDrops');
        Route::resource('csgo-case-roulette', 'Admin\CsgoCaseRouletteController');
        Route::get('csgo-case-roulette-statistics', 'Admin\CsgoCaseRouletteController@statistics');
        Route::get('csgo-case-roulette-reset', 'Admin\CsgoCaseRouletteController@resetStatistics');
        Route::get('csgo-case-roulette-reset-youtubers', 'Admin\CsgoCaseRouletteController@resetStatisticsYoutubers');

        Route::get('csgo-case-roulette-settings', 'Admin\CsgoCaseRouletteSettingController@getIndex');
        Route::model('csgo-case-roulette-setting', '\App\Models\CsgoCaseSettings');
        Route::resource('csgo-case-roulette-setting', 'Admin\CsgoCaseRouletteSettingController');

        //Roulette PUBG CASE
        Route::get('pubg-case-roulettes', 'Admin\PubgCases\PubgCaseRouletteController@getIndex');
        Route::model('pubg-case-roulette', '\App\Models\PubgCases\PubgCaseDrops');
        Route::resource('pubg-case-roulette', 'Admin\PubgCases\PubgCaseRouletteController');
        Route::get('pubg-case-roulette-statistics', 'Admin\PubgCases\PubgCaseRouletteController@statistics');
        Route::get('pubg-case-roulette-reset', 'Admin\PubgCases\PubgCaseRouletteController@resetStatistics');
        Route::get('pubg-case-roulette-reset-youtubers', 'Admin\PubgCases\PubgCaseRouletteController@resetStatisticsYoutubers');

        Route::get('pubg-case-roulette-settings', 'Admin\PubgCases\PubgCaseRouletteSettingController@getIndex');
        Route::model('pubg-case-roulette-setting', '\App\Models\PubgCases\PubgCaseSettings');
        Route::resource('pubg-case-roulette-setting', 'Admin\PubgCases\PubgCaseRouletteSettingController');

		// Upgrade Game
		Route::get('upgrades', 'Admin\UpgradeController@getIndex');
		Route::model('upgrade', '\App\Models\Upgrade');
		Route::resource('upgrade', 'Admin\UpgradeController');
		Route::get('upgrade-statistics', 'Admin\UpgradeController@statistics');

		Route::get('upgrade-scenarios', 'Admin\UpgradeScenarioController@getScenarios');
		Route::get('upgrade-scenarios/drop-counters', 'Admin\UpgradeScenarioController@dropCounters');
		Route::post('upgrade-scenarios/delete', 'Admin\UpgradeScenarioController@deleteSelected');
		Route::model('upgrade-scenario', '\App\Models\UpgradeScenario');
		Route::resource('upgrade-scenario', 'Admin\UpgradeScenarioController');

		Route::get('upgrade-settings', 'Admin\UpgradeSettingController@getIndex');
		Route::model('upgrade-setting', '\App\Models\UpgradeSetting');
		Route::resource('upgrade-setting', 'Admin\UpgradeSettingController');

		// Tower Game
		Route::get('towers', 'Admin\TowerController@getIndex');
		Route::model('tower', '\App\Models\Tower');
		Route::resource('tower', 'Admin\TowerController');
		Route::get('tower-statistics', 'Admin\TowerController@statistics');
        Route::get('tower-reset', 'Admin\TowerController@resetStatistics');

		Route::get('tower-cells', 'Admin\TowerCellController@getIndex');
		Route::model('tower-cell', '\App\Models\TowerCell');
		Route::resource('tower-cell', 'Admin\TowerCellController');

		Route::get('tower-scenarios', 'Admin\TowerScenarioController@getScenarios');
		Route::get('tower-scenarios/drop-counters', 'Admin\TowerScenarioController@dropCounters');
		Route::post('tower-scenarios/delete', 'Admin\TowerScenarioController@deleteSelected');
		Route::model('tower-scenario', '\App\Models\TowerScenario');
		Route::resource('tower-scenario', 'Admin\TowerScenarioController');

		Route::get('tower-settings', 'Admin\TowerSettingController@getIndex');
		Route::model('tower-setting', '\App\Models\TowerSetting');
		Route::resource('tower-setting', 'Admin\TowerSettingController');

		// Battle Game
		Route::get('battles', 'BattleController@getIndex');
		Route::model('battle', '\App\Models\Battle');
		Route::resource('battle', 'BattleController');
		Route::get('battle-statistics', 'BattleController@statistics');

		Route::get('battle-settings', 'BattleSettingController@getIndex');
		Route::model('battle-setting', '\App\Models\BattleSetting');
		Route::resource('battle-setting', 'BattleSettingController');

		//Markets
        Route::get('markets','Api\MarketCsgoController@getAdminMarket');
        Route::post('market-change/disable','Api\MarketCsgoController@setDisableStatusMarket');
        Route::post('market-change/enable','Api\MarketCsgoController@setEnableStatusMarket');
	});
});


