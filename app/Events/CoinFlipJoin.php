<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Config;
use Illuminate\Support\Facades\Log;

class CoinFlipJoin extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        Log::info('Data');
        Log::info($data);
        $this->data = json_encode($data);
    }

    public function broadcastAs()
    {
        return 'coin-flip-join';
    }

    public function broadcastOn()
    {
        return [Config::get('app.redis_prefix').'coin-flip-join'];
    }
}
