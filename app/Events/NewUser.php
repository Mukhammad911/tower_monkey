<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Config;

class NewUser extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public  $data = [];

    public function __construct()
    {
        $this->data = json_encode($this->data);
    }

    public function broadcastAs()
    {
        return 'users';
    }

    public function broadcastOn()
    {
        return [Config::get('app.redis_prefix').'csgotower'];
    }
}
