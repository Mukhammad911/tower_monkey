<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Config;
use Illuminate\Support\Facades\Log;

class ChatMessageEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {

        $this->data = json_encode($data);
    }

    public function broadcastAs()
    {
        return 'chat-message';
    }

    public function broadcastOn()
    {
        return [Config::get('app.redis_prefix').'chat-message'];
    }
}
