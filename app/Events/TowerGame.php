<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Config;

/**
 * Class TowerGame
 * @package App\Events
 */
class TowerGame extends Event implements ShouldBroadcast
{
	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public $data;

	public function __construct($data)
	{
		$this->data = json_encode($data);
	}

	public function broadcastAs()
	{
		return 'tower';
	}

	public function broadcastOn()
	{
		return [Config::get('app.redis_prefix').'tower'];
	}
}
