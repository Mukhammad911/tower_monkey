<?php

namespace App\Events;

use Config;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

/**
 * Class RefreshBalance
 * @package App\Events
 */
class RefreshBalance extends Event implements ShouldBroadcast
{
    use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public $data;

	/**
	 * RefreshBalance constructor.
	 *
	 * @param $data
	 */
    public function __construct($data)
    {
	    $this->data = json_encode($data);
    }

    public function broadcastAs()
    {
    	return 'refresh-balance';
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [Config::get('app.redis_prefix').'refresh-balance'];
    }
}
