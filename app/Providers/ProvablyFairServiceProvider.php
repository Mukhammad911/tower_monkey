<?php

namespace App\Providers;

use App\CustomClasses\ProvablyFair;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class ProvablyFairServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    App::bind('provablyFair', function() {
		    return new ProvablyFair();
	    });
    }
}
