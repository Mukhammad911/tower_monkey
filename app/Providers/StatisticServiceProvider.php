<?php

namespace App\Providers;

use App\CustomClasses\Statistic;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class StatisticServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    App::bind('statistic', function() {
		    return new Statistic();
	    });
    }
}
