<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Card;
use App\Observers\CardObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Card::observe(CardObserver::class);

        if (env('APP_SECURE'))
        {
            \URL::forceSchema('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
