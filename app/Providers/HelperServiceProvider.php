<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use App\CustomClasses\Helper;

class HelperServiceProvider extends ServiceProvider
{
    public function register() {
        App::bind('helper', function() {
            return new Helper;
        });
    }
}