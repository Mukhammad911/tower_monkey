<?php

namespace App\Providers;

use App\CustomClasses\Partnership;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class PartnershipServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    App::bind('partnership', function() {
		    return new Partnership;
	    });
    }
}
