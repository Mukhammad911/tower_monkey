var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'once-free.css',
        'libs/bootstrap/bootstrap.css',
        'libs/jquery.formstyler.css',
        'libs/notifications/jquery.jgrowl.min.css',
        'styles.css',
        'styles-upgrade.css',
        'tower.css',
        //'coin_flip.css',
        //'csgotower-jackpot.css',
        'ladder.css',
        'roulette-tower.css',
        'chat.css',
        'new-header.css',
    ],'public/assets/css');

    mix.scripts([
        'libs/jquery/jquery-2.0.0.min.js',
        'libs/jquery/jquery.maskedinput.min.js',
        'libs/jquery/jquery.formstyler.min.js',
        'libs/jquery/jquery.easing.1.3.js',
        //'libs/respond/respond.min.js',
        'libs/notifications/jquery.jgrowl.min.js',
        'libs/jquery/jquery.animateNumber.min.js',
        'libs/forms/validate.min.js',
        //'libs/forms/jquery.numberMask.min.js',
        'libs/bootstrap/bootstrap.min.js',
        'libs/socket.io/socket.io-1.4.5.js',
        'csgotower.js',
        'roulette.js',
        'tower.js',
      //'upgrade.js',
        'underscore.js',
        'profile_case.js',
        'common.js',
       // 'coin_flip.js',
        'chat.js',
        'roulette_tower.js'
    ],'public/assets/js');

    mix.version([
        'public/assets/js/all.js',
        'public/assets/css/all.css'
    ]);
});
