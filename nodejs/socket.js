var io = require('socket.io')();
var Redis = require('ioredis');
var Publish = require('./publish');
var redisDb = new Redis();
var redis = new Redis();

var publish = new Publish();
var config = require('dotenv').config({path: '../.env'});

var IoUsersConnectionsManager = require('./src/IoUsersConnectionsManager.js');
var ioUsersConnectionsManager = new IoUsersConnectionsManager(io);
var RedisCardsSubscriber = require('./src/RedisCardsSubscriber.js');
var redisCardsSubscriber = new RedisCardsSubscriber(redis, ioUsersConnectionsManager);


redis.subscribe(
    config.REDIS_PREFIX + 'csgotower',
    config.REDIS_PREFIX + 'bot',
    config.REDIS_PREFIX + 'live-trades',
    config.REDIS_PREFIX + 'upgrade',
    config.REDIS_PREFIX + 'tower',
    config.REDIS_PREFIX + 'refresh-balance',
    config.REDIS_PREFIX + 'coin-flip',
    config.REDIS_PREFIX + 'coin-flip-join',
    config.REDIS_PREFIX + 'roulette-tower-bet-add',
    config.REDIS_PREFIX + 'chat-message',
    config.REDIS_PREFIX +'roulette-tower',
    config.REDIS_PREFIX +'roulette-tower-node',
    config.REDIS_PREFIX +'once-free',
    function (err, count) {
    });

io.sockets.on('connection', function () {
    io.emit('online', {'online': io.engine.clientsCount});

}.bind(this));

var cc = 18;
var runTimer = function () {
    var myInterval = setInterval(function () {
        if (cc <= 1) {
            clearInterval(myInterval);
            cc = 18;
            publish.publishRedis();

            return;
        }
        console.log(cc * 100);
            io.emit('roulette-tower-timer', {'timer': cc * 100});
        cc--;
    }, 1000);
};


redis.on('message', function (channel, message) {

    var channelClear = (config.REDIS_PREFIX != '') ? channel.replace(config.REDIS_PREFIX, '') : channel;

    message = JSON.parse(message);

    switch (channel) {
        case config.REDIS_PREFIX + 'bot':
            redisDb.get('token_' + message.userId, function (err, value) {
                if (message.status == 'offered') {
                    setTimeout(function () {
                        io.emit(channelClear + '-' + value + ':' + message.status, message);
                        io.emit('angular-' + channelClear + '-' + value + ':' + message.status, message);
                    }, 0);
                } else {
                    io.emit(channelClear + '-' + value + ':' + message.status, message);
                }

            });
            if (message.status == 'accepted') {
                io.emit('live-trades', message);
            }
            break;

        case config.REDIS_PREFIX + 'upgrade':
            io.emit(channelClear + ':' + message.event, message.data);
            break;

        case config.REDIS_PREFIX + 'tower':
            io.emit(channelClear + ':' + message.event, message.data);
            break;
        case config.REDIS_PREFIX + 'refresh-balance':
            io.emit(channelClear + ':' + message.event, message.data);
            break;
        case config.REDIS_PREFIX + 'coin-flip':
            io.emit(channelClear + ':' + message.event, message.data);
            break;
        case config.REDIS_PREFIX + 'coin-flip-join':
            io.emit(channelClear + ':' + message.event, message.data);
            break;
        case config.REDIS_PREFIX + 'roulette-tower-bet-add':
            io.emit('roulette-tower-bet-add', message.data);
            break;
        case config.REDIS_PREFIX + 'chat-message':
            io.emit('chat-message', message.data);
            break;
        case config.REDIS_PREFIX + 'once-free':
            io.emit('once-free', message);
            break;
        case config.REDIS_PREFIX + 'csgotower':
        case config.REDIS_PREFIX + 'live-trades':
            io.emit(channelClear + ':' + message.event, message.data);
            break;
        case 'roulette-tower':
            switch (message.operation) {
                case 'run-timer':
                    runTimer();
                    break;
                case 'game-pre-closed':
                    io.emit('roulette-tower-game-pre-closed', {data: true});
                    break;
                case 'winner':
                    var randomData = 0;

                    switch (message.winner) {
                        case '2x':
                            randomData = [2010, 2012, 2020, 2021, 2022,2023,2024,2025,2026, 2186, 2126, 2140, 2143];
                            randomData = randomData[Math.floor(Math.random() * randomData.length)];
                            io.emit('roulette-tower-start-roulette', {data: randomData});
                            setTimeout(function(){
                                io.emit('roulette-tower-game-active', {data: true, winner: '2x'});
                            },6000);

                            setTimeout(function(){
                                io.emit('roulette-tower-clear-all', {data: true, winner: '2x'});
                                publish.restartGameRedis();
                            },10000);
                            break;
                        case '3x':
                            randomData = [2000, 2027, 2028,2029,2030,2031,2032,2033,2166, 2176, 2136, 2347];
                            randomData = randomData[Math.floor(Math.random() * randomData.length)];
                            io.emit('roulette-tower-start-roulette', {data: randomData});

                            setTimeout(function(){
                                io.emit('roulette-tower-game-active', {data: true, winner: '3x'});
                            },6000);

                            setTimeout(function(){
                                io.emit('roulette-tower-clear-all', {data: true, winner: '3x'});
                                publish.restartGameRedis();
                            },10000);
                            break;
                        case '5x':
                            randomData = [2014, 2015, 2016, 2017, 2018, 2019, 2156, 2343];
                            randomData = randomData[Math.floor(Math.random() * randomData.length)];
                            io.emit('roulette-tower-start-roulette', {data: randomData});

                            setTimeout(function(){
                                io.emit('roulette-tower-game-active', {data: true, winner: '5x'});
                            },6000);

                            setTimeout(function(){
                                io.emit('roulette-tower-clear-all', {data: true, winner: '5x'});
                                publish.restartGameRedis();
                            },10000);

                            break;
                        case '50x':
                            randomData = [2148, 2149, 2150,2151, 2152, 2153];
                            randomData = randomData[Math.floor(Math.random() * randomData.length)];
                            io.emit('roulette-tower-start-roulette', {data: randomData});
                            
                            setTimeout(function(){
                                io.emit('roulette-tower-game-active', {data: true, winner: '50x'});
                            },6000);

                            setTimeout(function(){
                                io.emit('roulette-tower-clear-all', {data: true, winner: '50x'});
                                publish.restartGameRedis();
                            },10000);
                            break;
                    }
                        break;

            }
    }
});

io.listen(config.LIVE_DROP_PORT);