var NodeLaravelSession = require('node-laravel-session');
var Cookie = require('cookie');

var io = undefined;
var usersConnections = {};

var IoUsersConnectionsManager = function (_io) {
    io = _io;

    var self = this;

    NodeLaravelSession.getAppKey('../.env').then(function (appKey) {
        io.sockets.on('connection', function (socket) {
            var laravelSession = Cookie.parse(String(socket.request.headers.cookie)).laravel_session;
            if (laravelSession) {
                try {
                    var laravelSessionKey = NodeLaravelSession.getSessionKey(laravelSession, appKey, (String(appKey).length > 16) ? 32 : 16);
                } catch (e) {
                    console.warn(e);
                    return;
                }
                NodeLaravelSession.getSessionFromFile(laravelSessionKey, '../storage/framework/sessions').then(function (session) {
                    var userId = NodeLaravelSession.getUserIdFromSession(session);
                    if (userId) {
                        self.add(userId, socket);
                        socket.on('disconnect', function () {
                            self.remove(userId, socket);
                        });
                    }
                });
            }
        });
    });
};

IoUsersConnectionsManager.prototype.has = function (userId, connection) {
    if ( ! usersConnections[userId]) {
        return false;
    }
    for (var connectionIndex in usersConnections[userId]) {
        if (usersConnections[userId][connectionIndex] == connection) {
            return true;
        }
    }
    return false;
};

IoUsersConnectionsManager.prototype.add = function (userId, connection) {
    if ( ! usersConnections[userId]) {
        usersConnections[userId] = [];
    }
    if ( ! this.has(userId, connection)) {
        usersConnections[userId].push(connection);
    }
};

IoUsersConnectionsManager.prototype.remove = function (userId, connection) {
    if ( ! usersConnections[userId]) {
        return;
    }
    for (var connectionIndex in usersConnections[userId]) {
        if (usersConnections[userId][connectionIndex] == connection) {
            usersConnections[userId].splice(connectionIndex, 1);
            if (usersConnections[userId].length === 0) {
                delete usersConnections[userId];
            }
            return;
        }
    }
};

IoUsersConnectionsManager.prototype.emit = function(userId, data1, data2) {
    for (var connectionIndex in usersConnections[userId]) {
        try {
            usersConnections[userId][connectionIndex].emit(data1, data2);
        } catch (e) {
            console.warn(e);
        }
    }
};

module.exports = IoUsersConnectionsManager;
