var redis = undefined;
var ioUsersConnectionsManager = undefined;

var RedisCardsSubscriber = function (_redis, _ioUsersConnectionsManager)
{
    redis = _redis;
    ioUsersConnectionsManager = _ioUsersConnectionsManager;

    redis.subscribe(
        process.env.REDIS_PREFIX+'cards:erasing1',
        process.env.REDIS_PREFIX+'cards:erasing2',
        process.env.REDIS_PREFIX+'cards:choosing',
        process.env.REDIS_PREFIX+'cards:winned');

    redis.on('message', function(channel, message)
    {
        channel = (process.env.REDIS_PREFIX != '') ? channel.replace(process.env.REDIS_PREFIX, '') : channel;
        message = JSON.parse(message);

        switch (channel) {
            case 'cards:erasing1':
            case 'cards:erasing2':
            case 'cards:choosing':
            case 'cards:winned':
                ioUsersConnectionsManager.emit(message.userId, channel, message.card);
                break;
        }
    });
};

module.exports = RedisCardsSubscriber;
