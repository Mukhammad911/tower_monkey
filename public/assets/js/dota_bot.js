$(function() {
    $('.getCodeDotaButton').click(function(event){
        event.preventDefault();
        getDotaBotAuthCode(this);
    });


    $(".dota_switchery").each(function () {
        var switchery = new Switchery(this);
        this.onchange = function(event) {
            var botId = this.dataset.id;
            if(this.checked) {
                $.get('/'+ ADMIN_PREFIX +'/dota_bots/' + botId + '/enable');
            } else {
                $.get('/'+ ADMIN_PREFIX +'/dota_bots/' + botId + '/disable');
            }
        }
    });

});

function getDotaBotAuthCode(elem) {
    $.get(elem.href, function(data){
        //$('#botCode').html(data);
        //$('#codeModal').modal('show');
        bootbox.alert("Mobile Authenticator code: " + data);
    });
}