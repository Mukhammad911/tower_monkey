var beastCreatePurchase = angular.module('beastCreatePurchase', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

beastCreatePurchase.controller('Ctrl4', function ($scope, $http) {
    var dota_bots = [];
    var dota_products = [];
    $scope.bots = [];
    $scope.products = [];
    $scope.selected_items = [];
    $scope.selected_items_rare = [];
    $scope.selected_product_purchase = [];
    $scope.selected_product_purchase_result = [];
    $scope.selected_bot_purchase = '';
    $scope.max_price = '';
    $scope.min_price = '';
    $scope.resultItem = [];
    $scope.purchase_id = '';
    var t = 0;
    var p = 0;
    var quotas = [];
    var check_all_item_or_not = false;

    $scope.searchString = '';

    $scope.searchStringClick = function () {
        setAllSkins($scope.searchString);
    };

    function setAllSkins(searchString) {
        $scope.products = [];

        for (i = 0; i < dota_products.length; i++) {

            if ((dota_products[i].name.indexOf(searchString) > -1)) {
                $scope.products.push(dota_products[i]);
            }
        }

        $('.pagination-div').show();
    };



    $http.get('/spidercontrolpanel/dota-purchase/create')
        .success(function (data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                dota_bots.push({
                    id: data[i].id,
                    name: data[i].steam_login
                });
            }
        })
        .error(function (data, status, headers, config) {
            console.log('err');
            console.log(data);
        })
        .then(function (res) {
            $scope.bots = dota_bots;

        });
    $http.get('/spidercontrolpanel/dota-purchase/get_product')
        .success(function (data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                dota_products.push({
                    id: data[i].id,
                    name: data[i].name,
                    image: data[i].image,
                    price: parseFloat((data[i].price/100).toFixed(2))
                });
            }
        })
        .error(function (data, status, headers, config) {
            console.log('err');
            console.log(data);
        })
        .then(function (res) {
            $scope.products = dota_products;


        });
    $scope.selectItem = function () {
        if (t == 0) {
            $('#listOfItems').show();
            t++;
        }
        else if (t == 1) {
            $('#listOfItems').hide();
            t = 0;
        }
    };
    $scope.selectItems = function (quality) {
        $('#listOfItems').hide();
        t = 0;
        var exist = _.contains($scope.selected_items, quality);
        var all_text = _.contains($scope.selected_items, 'all');
        if (exist) {
        }
        else {
            if (quality == 'all') {
                if ($scope.selected_items.length == 0) {
                    $scope.selected_items.push(quality);
                }
            }
            else if (all_text) {
            }
            else {
                $scope.selected_items.push(quality);

            }

        }
    };
    $scope.deleteItems = function (quality) {
        var index = _.indexOf($scope.selected_items, quality);
        $scope.selected_items.splice(index, 1);
    };

    $scope.selectItemRare = function () {
        if (p == 0) {
            $('#listOfItemsRare').show();
            p++;
        }
        else if (p == 1) {
            $('#listOfItemsRare').hide();
            p = 0;
        }
    };
    $scope.selectItemsRare = function (quality) {
        $('#listOfItemsRare').hide();
        p = 0;
        var exist = _.contains($scope.selected_items_rare, quality);
        var all_text = _.contains($scope.selected_items_rare, 'all');
        if (exist) {
        }
        else {
            if (quality == 'all') {
                if ($scope.selected_items_rare.length == 0) {
                    $scope.selected_items_rare.push(quality);
                }
            }
            else if (all_text) {
            }
            else {
                $scope.selected_items_rare.push(quality);

            }

        }
    };
    $scope.deleteItemsRare = function (quality) {
        var index = _.indexOf($scope.selected_items_rare, quality);
        $scope.selected_items_rare.splice(index, 1);
    };
    $scope.currentPage = 0;
    $scope.pageSize = 50;
    $scope.data = $scope.products;
    $scope.q = '';
    $scope.numberOfPages = function () {
        return Math.ceil($scope.getData().length / $scope.pageSize);
    };

    for (var i = 0; i < $scope.products.length; i++) {
        $scope.data.push("Item " + i);
    }
    ;

    $scope.select_item_purchase = function (id, name, price, image) {
        var my_id = parseInt(id);
        var my_price = parseInt(price);
        if (_.where($scope.selected_product_purchase, {
                'id': my_id,
                'image': image,
                'name': name,
                'price': my_price
            }).length) {
        }
        else {
            $scope.selected_product_purchase.push({'id': my_id, 'image': image, 'name': name, 'price': my_price})
        }
    };

    $scope.delete_purchase_selected_item = function (id) {
        var my_id = parseInt(id);
        $scope.selected_product_purchase = _.without($scope.selected_product_purchase, _.findWhere($scope.selected_product_purchase, {
            id: my_id
        }));
    };
    $scope.savePurchase = function () {
        checkAll();
    };
    function checkAll() {
        if ($scope.selected_bot_purchase == '' || $scope.max_price == '' || $scope.min_price == '' || $scope.selected_items == '' || $scope.selected_items_rare == '' || $scope.selected_product_purchase == '') {
        }
        else {
            startProcess();
        }
    }

    function firstProcess(callback) {
        var quality = $scope.selected_items.join(", ");
        var rarity = $scope.selected_items_rare.join(", ");
        $scope.resultItem.push({
            bot_id: $scope.selected_bot_purchase,
            min_price: $scope.min_price,
            max_price: $scope.max_price,
            quality: quality,
            rarity: rarity
        });
        callback();
    }

    function secondProcess() {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/spidercontrolpanel/dota-purchase/save',
            method: 'post',
            dataType: 'json',
            data: $scope.resultItem[0],
            cache: false,
            beforeSend: function () {
                $('#loader_purchase').show();
            }, success: function (response) {
                $('#loader_purchase').hide();
                $scope.resultItem = [];
                console.log(response);
                if (response.message == 'success') {
                    $scope.purchase_id = response.purchase_id;
                    checkSelectedItemQuota(function () {
                        setItemsChecked(function () {
                            $('#loader_purchase').show();
                            thirdProcess();

                        })
                    });
                }
                else if (response == 'var_error') {
                    alert('Есть пустои поле');
                }
                else if (response == 'bot_error') {
                    alert('Прошу выбрать другой бот')
                }
                else {
                    console.log(response);
                }
            }, error: function (xhr) {
                $scope.resultItem = [];
                $('#loader_purchase').hide();
            }
        });
    }

    function theEnd() {
        $('#loader_purchase').hide();
        var data_complete = [];
        data_complete.push({
            'purchase_id': $scope.purchase_id,
            'bot_id': $scope.selected_bot_purchase
        });
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/spidercontrolpanel/dota-purchase/save/complete',
            method: 'post',
            dataType: 'json',
            data: data_complete[0],
            cache: false,
            beforeSend: function () {

            }, success: function (response) {

            }, error: function (xhr) {

            }
        });
        alert('Данные успешно сохранены');
        location.reload();
    }

    function thirdProcess() {
        var runSave = function () {
            var item = $scope.selected_product_purchase_result[0];
            if(!$scope.selected_product_purchase_result.length){
                $('#loader_purchase').hide();
                theEnd();
            }
            else{
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/spidercontrolpanel/dota-purchase/save/product',
                    method: 'post',
                    dataType: 'json',
                    data: item,
                    cache: false,
                    beforeSend: function () {

                    }, success: function (response) {
                        runSave()
                    }, error: function (xhr) {
                        runSave()
                    }
                });
                $scope.selected_product_purchase_result.splice(0,1);
            }
        };
        runSave();
    }

    function checkSelectedItemQuota(callback) {
        if(!check_all_item_or_not){
            for (var i = 0; i < $scope.selected_product_purchase.length; i++) {
                var quota = document.getElementById('value_id_of_' + $scope.selected_product_purchase[i].id).value;
                quotas.push(quota);
            }
        }
        callback();
    }

    function setItemsChecked(callback) {
        for (var i = 0; i < $scope.selected_product_purchase.length; i++) {
            $scope.selected_product_purchase_result.push({
                'name': $scope.selected_product_purchase[i].name,
                'quota': quotas[i],
                'purchase_id': $scope.purchase_id
            });
        }

        callback()
    }

    function startProcess() {
        firstProcess(function () {
            secondProcess();

        })
    }

    $scope.select_all_item = function () {
        check_all_item_or_not = true;
        $scope.selected_product_purchase = [];
        console.log($scope.selected_product_purchase);
        if ($scope.max_price == '' || $scope.max_price == '') {
            alert('Max Price or Min Price is empty')
        }
        else {
            for (var i = 0; i < dota_products.length; i++) {
                if (dota_products[i].price >= $scope.min_price && dota_products[i].price <= $scope.max_price) {
                    $scope.selected_product_purchase.push({'id': dota_products[i].id, 'image': dota_products[i].image, 'name': dota_products[i].name, 'price': dota_products[i].price})
                }
            }
            for(var j =0;j<$scope.selected_product_purchase.length; j++){
                quotas.push(30);
            }
          //  $scope.selected_product_purchase =$scope.all_items_select
        /*    $scope.savePurchase();*/
        }
    }
});
beastCreatePurchase.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
