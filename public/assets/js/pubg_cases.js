var selInit = function(sel){
    return {
        ajax:{
            type: "GET",
            url: casesAutocomplete,
        delay: 250,
        dataType: 'json',
        data:function(params){
        return {term: params.term}
    },
    processResults: function (data, page) {
        var selected = sel.find('option:selected').val();
        var results = [];

        $.each(data, function (i, v) {
            var o = {};
            if(v.id!=selected){
                o.id = v.id;
                o.name = v.name;
                results.push(o);
            }
        });

        return {
            results: results
        };
    },
    cache: true
},
    escapeMarkup: function (markup) { return markup;},
    templateResult: function (steamCase) {
        if (steamCase.loading)
            return 'Loading...';
        var markup = '<option value="' + steamCase.id + '">' + steamCase.name + '</option>';

        return markup;
    },
    templateSelection: function (steamCase) {
        return steamCase.name;
    }
}
};

function deleteCase(id){
    swal({
        title: "Are you sure?",
        text: "Selected Case will be deleted",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        html: false
    }, function() {
        $.ajax({
                type: "get",
                dataType: "json",
                url: 'deletecase'+'/'+id
    })
        .success(function(success){
            if(success){
                $('#cases').DataTable().ajax.reload();
                swal(
                    "Deleted!",
                    "Case has been deleted.",
                    "success"
                );
            }
        });
    });
}

function sortUp(id){
    $.ajax({
            type: "GET",
            dataType: "json",
            url: up+id
})
.success(function(success){
        if(success){
            $('#cases').DataTable().ajax.reload();
        }
    });
}

function sortDown(id){
    $.ajax({
            type: "GET",
            dataType: "json",
            url: down+id
})
.success(function(success){
        if(success){
            $('#cases').DataTable().ajax.reload();
        }
    });
}

$(function(){
    var table = $('#cases').DataTable({
        ajax: casesData,
        searching: true,
        ordering: false,
        processing: true,
        serverSide: true,

        autoWidth: true,
        lengthMenu: [ [10, 20, 50, -1], [10, 20, 50, "All"] ],
        columns: [
            { data: "id", name: 'cs.id'},
            { data: "name", name: 'cs.name'},
            { data: "image", name: 'cs.image'},
            { data: "price", width:200, searchable: false},
            { data: "category", name: 'c.name'},
            { data: "active", searchable: false},
            { data: 'action', name: 'action', orderable: false, searchable: false, width:400}
        ],
        dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
        }
});

    $('.datatable-header #cases_filter').after('<div class="pull-right form-group"><button id="change_position_btn" data-toggle="modal" data-target="#change_cases" class="btn bg-orange-800">Change Position</button></div>');

    $('#case1').select2(selInit($('#case2')));
    $('#case2').select2(selInit($('#case1')));

    $( "#change-position-form").validate({
        rules: {
            case1: {
                required: true
            },
            case2: {
                required: true
            }
        }
    });

    $('#confirm_change_position').click(function(e){
        e.preventDefault();
        if($("#change-position-form").valid()){
            $.ajax({
                    url: changePositions,
                data: {
                first:$('#case1').val(),
                    second:$('#case2').val()
            },
            type: "POST",
                dataType: "json"

        })
        .success(function(success){
                if(success){
                    table.ajax.reload();
                    $('#change_cases').modal('hide');
                }
            });
        }
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});