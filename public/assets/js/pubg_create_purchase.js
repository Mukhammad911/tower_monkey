var beastCreatePurchaseCsgo = angular.module('beastCreatePurchaseCsgo', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

beastCreatePurchaseCsgo.controller('Ctrl6', function ($scope, $http) {
    var dota_bots = [];
    var dota_products = [];
    $scope.csgo_bots = [];
    $scope.csgo_products = [];
    $scope.csgo_selected_items = [];
    $scope.csgo_selected_items_rare = [];
    $scope.csgo_selected_product_purchase = [];
    $scope.csgo_selected_product_purchase_result = [];
    $scope.csgo_selected_bot_purchase = '';
    $scope.csgo_max_price = '';
    $scope.csgo_min_price = '';
    $scope.csgo_resultItem = [];
    $scope.csgo_purchase_id = '';
    var check_all_item_or_not = false;
    var t = 0;
    var p = 0;
    var quotas = [];

    $scope.searchString = '';

    $scope.searchStringClick = function () {
        setAllSkins($scope.searchString);
    };

    function setAllSkins(searchString) {
        $scope.csgo_products = [];

        for (i = 0; i < dota_products.length; i++) {

            if ((dota_products[i].name.indexOf(searchString) > -1)) {
                $scope.csgo_products.push(dota_products[i]);
            }
        }

        $('.pagination-div').show();
    };


    $http.get('/spidercontrolpanel/pubg-purchase/create')
        .success(function (data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                dota_bots.push({
                    id: data[i].id,
                    name: data[i].steam_login
                });
            }
        })
        .error(function (data, status, headers, config) {
            console.log('err');
            console.log(data);
        })
        .then(function (res) {
            $scope.csgo_bots = dota_bots;

        });
    $http.get('/spidercontrolpanel/pubg-purchase/get_product')
        .success(function (data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                if(data[i].image.includes('http')){
                    dota_products.push({
                        id: data[i].id,
                        name: data[i].name,
                        image: data[i].image,
                        price: parseFloat((data[i].price/100).toFixed(2))
                    });
                }else{
                    dota_products.push({
                        id: data[i].id,
                        name: data[i].name,
                        image: '/images/products/'+data[i].image,
                        price: parseFloat((data[i].price/100).toFixed(2))
                    });
                }
            }
        })
        .error(function (data, status, headers, config) {
            console.log('err');
            console.log(data);
        })
        .then(function (res) {
            $scope.csgo_products = dota_products;


        });
    $scope.csgo_selectItem = function () {
        if (t == 0) {
            $('#listOfItems').show();
            t++;
        }
        else if (t == 1) {
            $('#listOfItems').hide();
            t = 0;
        }
    };
    $scope.csgo_selectItems = function (quality) {
        $('#listOfItems').hide();
        t = 0;
        var exist = _.contains($scope.csgo_selected_items, quality);
        var all_text = _.contains($scope.csgo_selected_items, 'all');
        if (exist) {
        }
        else {
            if (quality == 'all') {
                if ($scope.csgo_selected_items.length == 0) {
                    $scope.csgo_selected_items.push(quality);
                }
            }
            else if (all_text) {
            }
            else {
                $scope.csgo_selected_items.push(quality);

            }

        }
    };
    $scope.csgo_deleteItems = function (quality) {
        var index = _.indexOf($scope.csgo_selected_items, quality);
        $scope.csgo_selected_items.splice(index, 1);
    };

    $scope.csgo_selectItemRare = function () {
        if (p == 0) {
            $('#listOfItemsRare').show();
            p++;
        }
        else if (p == 1) {
            $('#listOfItemsRare').hide();
            p = 0;
        }
    };
    $scope.csgo_selectItemsRare = function (quality) {
        $('#listOfItemsRare').hide();
        p = 0;
        var exist = _.contains($scope.csgo_selected_items_rare, quality);
        var all_text = _.contains($scope.csgo_selected_items_rare, 'all');
        if (exist) {
        }
        else {
            if (quality == 'all') {
                if ($scope.csgo_selected_items_rare.length == 0) {
                    $scope.csgo_selected_items_rare.push(quality);
                }
            }
            else if (all_text) {
            }
            else {
                $scope.csgo_selected_items_rare.push(quality);

            }

        }
    };
    $scope.csgo_deleteItemsRare = function (quality) {
        var index = _.indexOf($scope.csgo_selected_items_rare, quality);
        $scope.csgo_selected_items_rare.splice(index, 1);
    };
    $scope.csgo_currentPage = 0;
    $scope.csgo_pageSize = 50;
    $scope.csgo_data = $scope.csgo_products;
    $scope.csgo_q = '';
    $scope.csgo_numberOfPages = function () {
        return Math.ceil($scope.csgo_getData().length / $scope.csgo_pageSize);
    };

    for (var i = 0; i < $scope.csgo_products.length; i++) {
        $scope.csgo_data.push("Item " + i);
    }
    ;

    $scope.csgo_select_item_purchase = function (id, name, price, image) {
        var my_id = parseInt(id);
        var my_price = parseFloat(price);
        if (_.where($scope.csgo_selected_product_purchase, {
                'id': my_id,
                'image': image,
                'name': name,
                'price': my_price,
            }).length) {
        }
        else {
            $scope.csgo_selected_product_purchase.push({'id': my_id, 'image': image, 'name': name, 'price': my_price})
        console.log('log pushed price');
        console.log($scope.csgo_selected_product_purchase);
        }

    };

    $scope.csgo_delete_purchase_selected_item = function (id) {
        var my_id = parseInt(id);
        $scope.csgo_selected_product_purchase = _.without($scope.csgo_selected_product_purchase, _.findWhere($scope.csgo_selected_product_purchase, {
            id: my_id
        }));
    };
    $scope.csgo_savePurchase = function () {
        checkAll();
    };
    function checkAll() {
        if ($scope.csgo_selected_bot_purchase == '' || $scope.csgo_max_price == '' || $scope.csgo_min_price == '' || $scope.csgo_selected_product_purchase == '') {
            console.log($scope.csgo_selected_bot_purchase);
            console.log($scope.csgo_selected_items);
            console.log($scope.csgo_selected_product_purchase);
        }
        else {
            startProcess();
        }
    }
    function firstProcess(callback) {
        var quality = $scope.csgo_selected_items.join(", ");
        var rarity = $scope.csgo_selected_items_rare.join(", ");
        $scope.csgo_resultItem.push({
            bot_id: $scope.csgo_selected_bot_purchase,
            min_price: parseFloat($scope.csgo_min_price),
            max_price: parseFloat($scope.csgo_max_price),
            quality: 'All',
            rarity: 'All'
        });
        callback();
    }
    function secondProcess() {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/spidercontrolpanel/pubg-purchase/save',
            method: 'post',
            dataType: 'json',
            data: $scope.csgo_resultItem[0],
            cache: false,
            beforeSend: function () {
                $('#loader_purchase').show();
            }, success: function (response) {
                $('#loader_purchase').hide();
                $scope.csgo_resultItem = [];
                console.log(response);
                if (response.message == 'success') {
                    $scope.csgo_purchase_id = response.purchase_id;
                    checkSelectedItemQuota(function () {
                        setItemsChecked(function () {
                            $('#loader_purchase').show();
                            thirdProcess();
                        })
                    });
                }
                else if (response == 'var_error') {
                    alert('Есть пустои поле');
                }
                else if (response == 'bot_error') {
                    alert('Прошу выбрать другой бот')
                }
                else {
                    console.log(response);
                }
            }, error: function (xhr) {
                $scope.csgo_resultItem = [];
                $('#loader_purchase').hide();
            }
        });
    }

    function theEnd() {
        $('#loader_purchase').hide();

        var data_complete = [];

        data_complete.push({
            'purchase_id': $scope.csgo_purchase_id,
            'bot_id': $scope.csgo_selected_bot_purchase
        });

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/spidercontrolpanel/pubg-purchase/save/complete',
            method: 'post',
            dataType: 'json',
            data: data_complete[0],
            cache: false,
            beforeSend: function () {

            }, success: function (response) {

            }, error: function (xhr) {

            }
        });

        alert('Данные успешно сохранены');
        location.reload();
    }
    function thirdProcess() {
        var runSave = function () {
            var item = $scope.csgo_selected_product_purchase_result[0];
            if(!$scope.csgo_selected_product_purchase_result.length){
                $('#loader_purchase').hide();
                theEnd();
            }
            else{
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/spidercontrolpanel/pubg-purchase/save/product',
                    method: 'post',
                    dataType: 'json',
                    data: item,
                    cache: false,
                    beforeSend: function () {

                    }, success: function (response) {
                        runSave()
                    }, error: function (xhr) {
                        runSave()
                    }
                });
                $scope.csgo_selected_product_purchase_result.splice(0,1);
            }
        };
        runSave();
    }


    function checkSelectedItemQuota(callback) {
        if(!check_all_item_or_not){
            for (var i = 0; i < $scope.csgo_selected_product_purchase.length; i++) {
                var quota = document.getElementById('value_id_of_' + $scope.csgo_selected_product_purchase[i].id).value;
                quotas.push(quota);
            }
            callback();
        }else{
            callback();
        }
    }

    function setItemsChecked(callback) {
        console.log('Price Product Custom');
        console.log($scope.csgo_selected_product_purchase);
        for (var i = 0; i < $scope.csgo_selected_product_purchase.length; i++) {

            $scope.csgo_selected_product_purchase_result.push({
                'name': $scope.csgo_selected_product_purchase[i].name,
                'quota': quotas[i],
                'purchase_id': $scope.csgo_purchase_id,
                'price':$scope.csgo_selected_product_purchase[i].price

            });
        }
        callback()
    }

    function startProcess() {
        firstProcess(function () {
            secondProcess();

        })
    }
    $scope.csgo_select_all_item = function () {
        check_all_item_or_not = true;
        $scope.csgo_selected_product_purchase = [];
        console.log($scope.csgo_selected_product_purchase);
        if ($scope.csgo_max_price == '' || $scope.csgo_max_price == '') {
            alert('Max Price or Min Price is empty')
        }
        else {
            for (var i = 0; i < dota_products.length; i++) {
                if (parseFloat(dota_products[i].price) >= parseFloat($scope.csgo_min_price) && parseFloat(dota_products[i].price) <= parseFloat($scope.csgo_max_price)) {
                    $scope.csgo_selected_product_purchase.push({'id': dota_products[i].id, 'image': dota_products[i].image, 'name': dota_products[i].name, 'price': parseFloat(dota_products[i].price)})
                }
            }
            for(var j =0;j<$scope.csgo_selected_product_purchase.length; j++){
                quotas.push(30);
            }
        }
    }
});
beastCreatePurchaseCsgo.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
