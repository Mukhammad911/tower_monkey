

$(function(){
    var table = $('#cases').DataTable({
        ajax: casesData,
        searching: true,
        ordering: false,
        processing: true,
        serverSide: true,

        autoWidth: true,
        lengthMenu: [ [10, 20, 50, -1], [10, 20, 50, "All"] ],
        columns: [
            { data: "id", name: 'cs.id'},
            { data: "name", name: 'cs.name'},
            { data: "image", name: 'cs.image'},
            { data: "price", width:200, searchable: false},
            { data: "category", name: 'c.name'},
            { data: 'action', name: 'action', orderable: false, searchable: false, width:400}
        ],
        dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
        }
});

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});