function deleteProduct(id){
    swal({
        title: "Are you sure?",
        text: "Selected product will be deleted",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        html: false
    }, function() {
        $.ajax({
                type: "DELETE",
                dataType: "json",
                url: productDelete + '/'+id
    })
        .success(function(success){
            if(success){
                $('#products').DataTable().ajax.reload();
                swal(
                    "Deleted!",
                    "Product has been deleted.",
                    "success"
                );
            }
        });
    });
}

$(function(){
    var table = $('#products').DataTable({
            ajax:{
                url: productsData,
            data: function (request) {
                request.case_id = $('#case').find('option:selected').val();
                request.class = $('#class').find('option:selected').val();
                request.id = $('#id').find('option:selected').val();
                request.min = $('#min').val();
                request.max = $('#max').val();
            }
        },
        searching: false,
        processing: true,
        serverSide: true,
        lengthMenu: [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
        columns: [
        { data: "id"},
        {
            data: "name",
            render: function (data) {
                return '<strong>'+data+'</strong>';
            }
        },
        { data: "short_description"},
        {
            data: "price",
            render: function (data) {
                return '<span class="badge badge-success">'+Math.ceil(data/100)+'</span>';
            }
        },
        { 
            data: "image",
            render: function (data) {
                return '<img style="max-height:40px" src="'+data+'">';
            }
        },
        { 
            data: "class",
            render: function (data) {
                return '<label class="label '+data+'">'+data+'</label>';
            }
        
        },
        { 
            data: 'id',
            name: 'action',
            orderable: false,
            searchable: false,
            width:200,
            render: function (data) {
                return '<ul class="icons-list">' +
                            '<li><a title="" data-popup="tooltip" href="/'+ ADMIN_PREFIX +'/product/'+data+'/edit" data-original-title="Edit"><i class="icon-pencil7"></i> Edit</a></li>'+
                            '<li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deleteProduct('+data+')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a></li>'+
                       '</ul>';
            }
        }
    ]
});


    $('#search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });

    $("#case").select2({
        ajax:{
            type: "GET",
            url: caseSearch+'/',
            delay: 250,
            dataType: 'json',
            data:function(params){
                return {term: params.term}
            },
            processResults: function (data) {
                var results = [];

                $.each(data, function (i, v) {
                    var o = {};
                    o.id = v.id;
                    o.name = v.name;
                    results.push(o);
                });

                return {
                    results: results
                };
            },
            cache: true
        },
    escapeMarkup: function (markup) { return markup;},
    templateResult: function (steamCase) {
        if (steamCase.loading)
            return 'Loading...';
        var markup = '<option value="' + steamCase.id + '">' + steamCase.name + '</option>';

        return markup;
    },
    templateSelection: function (steamCase) {
        return steamCase.name;
    }
});

    $('#class').select2({
        minimumResultsForSearch: -1
    });

    $('#id').select2({
            allowClear: true,
            placeholder: "Select Product",
            minimumInputLength: 1,
            ajax:{
                type: "GET",
                url: productSearch+'/',
                delay: 250,
                dataType: 'json',
                data:function(params){
                    return {term: params.term}
                },
                processResults: function (data) {
                    var results = [];

                    $.each(data, function (index, value) {
                        var o = {};
                        o.id = value.id;
                        o.image = value.image;
                        o.image_source = value.image_source;
                        var name = (Lang.has('weapon.'+value.name)) ? Lang.get('weapon.'+value.name) : value.name;
                        o.name = name+' | '+ Lang.get('weapon.'+value.short_description);
                        results.push(o);
                    });

                    return {
                        results: results
                    };
                },
                cache: true
            },
        escapeMarkup: function (markup) { return markup;},
    templateResult: function (product) {
        if (product.loading)
            return 'Loading...';

        var image = (product.image_source == 'file')
            ? '<img src="'+assetPath+'/'+product.image+'">'
    : '<img src="'+product.image+'">';

        var markup = image + '<option value="' + product.id + '">'+ product.name + '</option>';

        return markup;
    },
    templateSelection: function (product) {
        return product.name;
    }
});

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});