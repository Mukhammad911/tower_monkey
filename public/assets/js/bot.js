$(function() {
    $('.getCodeButton').click(function(event){
        event.preventDefault();
        getBotAuthCode(this);
    });


    $(".switchery").each(function () {
        var switchery = new Switchery(this);
        this.onchange = function(event) {
            var botId = this.dataset.id;
            if(this.checked) {
                $.get('/'+ ADMIN_PREFIX +'/bots/' + botId + '/enable');
            } else {
                $.get('/'+ ADMIN_PREFIX +'/bots/' + botId + '/disable');
            }
        }
    });

});

function getBotAuthCode(elem) {
    $.get(elem.href, function(data){
        //$('#botCode').html(data);
        //$('#codeModal').modal('show');
        bootbox.alert("Mobile Authenticator code: " + data);
    });
}