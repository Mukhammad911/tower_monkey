var editPriceProductPubg = angular.module('editPriceProductPubg', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

editPriceProductPubg.controller('editDotaCtrl', function ($scope, $http) {
    var dota_products = [];
    $scope.csgo_bots = [];
    $scope.csgo_products = [];
    $scope.csgo_selected_items = [];
    $scope.csgo_selected_items_rare = [];
    $scope.csgo_selected_product_purchase = [];
    $scope.csgo_selected_product_purchase_result = [];
    $scope.csgo_selected_bot_purchase = '';
    $scope.csgo_max_price = '';
    $scope.csgo_min_price = '';
    $scope.csgo_resultItem = [];
    $scope.csgo_purchase_id = '';
    var check_all_item_or_not = false;
    var t = 0;
    var p = 0;
    var quotas = [];
    $scope.searchString = '';

    $scope.searchStringClick = function () {
        setAllSkins($scope.searchString);
    };

    function setAllSkins(searchString) {
        $scope.csgo_products = [];

        for (i = 0; i < dota_products.length; i++) {

            if ((dota_products[i].name.indexOf(searchString) > -1)) {
                $scope.csgo_products.push(dota_products[i]);
            }
        }

        $('.pagination-div').show();
    };

    $http.get('get-edit-pubg-available-products')
        .success(function (data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                if (parseInt(data[i].stattrak) == 0) {
                    dota_products.push({
                        id: data[i].id,
                        name: data[i].name,
                        short_description: data[i].short_description,
                        image: data[i].image_hash,
                        price: parseFloat((data[i].price / 100).toFixed(2)),
                        quality: data[i].quality,
                        stattrak: ''
                    });
                } else {
                    dota_products.push({
                        id: data[i].id,
                        name: data[i].name,
                        short_description: data[i].short_description,
                        image: data[i].image_hash,
                        price: parseFloat((data[i].price / 100).toFixed(2)),
                        quality: data[i].quality,
                        stattrak: ''
                    });
                }


            }
        })
        .error(function (data, status, headers, config) {
            console.log('err');
            console.log(data);
        })
        .then(function (res) {
            $scope.csgo_products = dota_products;


        });
    $scope.csgo_selectItem = function () {
        if (t == 0) {
            $('#listOfItems').show();
            t++;
        }
        else if (t == 1) {
            $('#listOfItems').hide();
            t = 0;
        }
    };
    $scope.csgo_selectItems = function (quality) {
        $('#listOfItems').hide();
        t = 0;
        var exist = _.contains($scope.csgo_selected_items, quality);
        var all_text = _.contains($scope.csgo_selected_items, 'all');
        if (exist) {
        }
        else {
            if (quality == 'all') {
                if ($scope.csgo_selected_items.length == 0) {
                    $scope.csgo_selected_items.push(quality);
                }
            }
            else if (all_text) {
            }
            else {
                $scope.csgo_selected_items.push(quality);

            }

        }
    };
    $scope.csgo_deleteItems = function (quality) {
        var index = _.indexOf($scope.csgo_selected_items, quality);
        $scope.csgo_selected_items.splice(index, 1);
    };
    $scope.csgo_currentPage = 0;
    $scope.csgo_pageSize = 50;
    $scope.csgo_data = $scope.csgo_products;
    $scope.csgo_q = '';
    $scope.csgo_numberOfPages = function () {
        return Math.ceil($scope.csgo_getData().length / $scope.csgo_pageSize);
    };

    for (var i = 0; i < $scope.csgo_products.length; i++) {
        $scope.csgo_data.push("Item " + i);
    }
    ;

    $scope.csgo_select_item_purchase = function (id, name, price, short_description, image, quality) {
        var my_id = parseInt(id);
        var my_price = parseFloat(price).toFixed(2);
        if (_.where($scope.csgo_selected_product_purchase, {
                'id': my_id,
                'image': image,
                'name': name,
                'price': my_price,
                'short_description': short_description,
                'quality': quality,
            }).length) {
        }
        else {
            $scope.csgo_selected_product_purchase.push({
                'id': my_id,
                'image': image,
                'name': name,
                'price': my_price,
                'short_description': short_description,
                'quality': quality
            })
        }
    };

    $scope.csgo_delete_purchase_selected_item = function (id) {
        var my_id = parseInt(id);
        $scope.csgo_selected_product_purchase = _.without($scope.csgo_selected_product_purchase, _.findWhere($scope.csgo_selected_product_purchase, {
            id: my_id
        }));
    };
    $scope.csgo_savePurchase = function () {
        checkAll();
    };
    function checkAll() {
        if ($scope.csgo_selected_product_purchase == '') {
            alert('Прошу выбрать продукт для сохранение')
        }
        else {
            startProcess();
        }
    }

    function firstProcess(callback) {
        callback();
    }

    function secondProcess() {
        checkSelectedItemQuota(function () {
            setItemsChecked(function () {
                $('#loader_purchase').show();
                thirdProcess();
            })
        });

    }

    function theEnd() {
        $('#loader_purchase').hide();

        alert('Данные успешно сохранены');
        location.reload();
    }

    function thirdProcess() {
        var runSave = function () {
            var item = $scope.csgo_selected_product_purchase_result[0];
            if (!$scope.csgo_selected_product_purchase_result.length) {
                $('#loader_purchase').hide();
                theEnd();
            }
            else {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '/spidercontrolpanel/save-edit-pubg-available-products',
                    method: 'post',
                    dataType: 'json',
                    data: item,
                    cache: false,
                    beforeSend: function () {

                    }, success: function (response) {
                        runSave()
                    }, error: function (xhr) {
                        runSave()
                    }
                });
                $scope.csgo_selected_product_purchase_result.splice(0, 1);
            }
        };
        runSave();
    }

    function checkSelectedItemQuota(callback) {
        if (!check_all_item_or_not) {
            for (var i = 0; i < $scope.csgo_selected_product_purchase.length; i++) {
                var product_edit_price = document.getElementById('value_id_of_' + $scope.csgo_selected_product_purchase[i].id).value;
                quotas.push(product_edit_price);
            }
            callback();
        } else {
            callback();
        }
    }

    function setItemsChecked(callback) {
        for (var i = 0; i < $scope.csgo_selected_product_purchase.length; i++) {
            $scope.csgo_selected_product_purchase_result.push({
                'id': $scope.csgo_selected_product_purchase[i].id,
                'price': parseInt(quotas[i] * 100),

            });
        }
        callback()
    }

    function startProcess() {
        firstProcess(function () {
            secondProcess();
        })
    }

    $scope.csgo_select_all_item = function () {
        check_all_item_or_not = true;
        $scope.csgo_selected_product_purchase = [];
        console.log($scope.csgo_selected_product_purchase);
        if ($scope.csgo_max_price == '' || $scope.csgo_max_price == '') {
            alert('Max Price or Min Price is empty')
        }
        else {
            for (var i = 0; i < dota_products.length; i++) {
                if (parseInt(dota_products[i].price) >= parseInt($scope.csgo_min_price) && parseInt(dota_products[i].price) <= parseInt($scope.csgo_max_price)) {
                    $scope.csgo_selected_product_purchase.push({
                        'id': dota_products[i].id,
                        'image': dota_products[i].image,
                        'name': dota_products[i].name,
                        'price': parseInt(dota_products[i].price),
                        'short_description': dota_products[i].short_description
                    })
                }
            }
            for (var j = 0; j < $scope.csgo_selected_product_purchase.length; j++) {
                quotas.push(30);
            }
        }
    }
});
editPriceProductPubg.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
