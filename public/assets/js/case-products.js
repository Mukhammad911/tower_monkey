function deleteFromCase(id, case_id){
    swal({
        title: "Are you sure?",
        text: "Selected product will be deleted from {{ $case->name }}",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        html: false
    }, function() {
        $.ajax({
                type: "GET",
                dataType: "json",
                url: removeProduct,
            data:{
            id:id,
                case_id:case_id
        }
    })
        .success(function(success){
            if(success){
                $('#products').DataTable().ajax.reload();
                swal(
                    "Deleted!",
                    "Product has been deleted.",
                    "success"
                );
            }
        });
    });
}

function sortUp(id){
    var url = up.replace('id',id);

    $.ajax({
            type: "GET",
            dataType: "json",
            url:url
})
.success(function(success){
        if(success){
            $('#products').DataTable().ajax.reload();
        }
    });
}

function sortDown(id){
    var url = down.replace('id',id);

    $.ajax({
            type: "GET",
            dataType: "json",
            url: url
})
.success(function(success){
        if(success){
            $('#products').DataTable().ajax.reload();
        }
    });
}

$(function(){
    var table = $('#products').DataTable({
            ajax:{
                url: productsData,
            data:{
                case_id:caseId
            }
        },
        autoWidth: true,
        sorting:false,
        searching:false,
        processing: true,
        serverSide: true,
        ordering:false,
        lengthMenu: [ [25, 50, 100, -1], [25, 50, 100, "All"] ],
        columns: [
            { data: "id"},
            { data: "name"},
            { data: "short_description"},
            { data: "price"},
            { data: "image"},
            { data: "class"},
            { data: 'action', name: 'action', orderable: false, searchable: false, width:400}
        ]
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});