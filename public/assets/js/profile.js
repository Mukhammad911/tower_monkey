function soldContent(){
    return '<p class="sold"><i class="fa '+currency_class+'" aria-hidden="true"></i> <span>'+Lang.get('messages.sold')+'</span></p>';
}

function receivedContent(){
    return '<p class="received"><i class="icon icon-arrow-circle-b"></i> <span>'+Lang.get('messages.received')+'</span></p>';
}

function receiveContent(href){
    return '<a target="_blank" href="'+href+'" class="btn-green receive"><i class="icon icon-arrow-right"></i><span>'+Lang.get('messages.receive')+'</span></a>';
}

function sendingContent() {
    return '<p class="sending">'+Lang.get('messages.sending')+'...</p>';
}

function acceptContent(drop_id) {
    var html='<a href="javascript:void(0)" id="'+drop_id+'" class="btn-green send"><i class="icon icon-arrow-right"></i> <span>'+Lang.get('messages.accept')+'</span></a>';
    return html;
}

function pendingContent() {
    var html='<div class="btn-profile-process">';
        html+='<a href="javascript:void(0)"  class="btn-green send"><i class="icon icon-arrow-right"></i> <span>'+Lang.get('messages.accept')+'</span></a>';
        html+='<a href="javascript:void(0)"  class="btn-blue sell"><i class="fa fa-usd" aria-hidden="true"></i> <span>'+Lang.get('messages.sell')+'</span></a>';
        html+= '</div>';
    return html;
}

$(function(){

    var socket = io(window.location.hostname+':'+LIVE_DROP_PORT);

    socket.on("bot-"+token+":offered",function(message){
        var href = 'https://steamcommunity.com/tradeoffer/'+message.offerId,
            el = $('.info#'+message.dropId);

        el.attr('data-offer-id',message.offerId);
        el.html(receiveContent(href));
    });

    socket.on("bot-"+token+":accepted",function(message){
        var el = $('.info[data-offer-id='+message.offerId+']');

        el.html(receivedContent());
    });

    socket.on("bot-"+token+":declined",function(message){
        var el = $('.info[data-offer-id='+message.offerId+']'),
            dropId = el.attr('id');

        el.html(acceptContent(dropId));
    });

    socket.on("bot-"+token+":error",function(message){
        var el = $('.info#'+message.dropId);

        el.html(pendingContent());

        notifyError(message.message, true);
    });


    $('.btn-save-profile-url').click(function(e){
        e.preventDefault();

        if($('.profile-trade-url').valid()){
            $.ajax({
                    method:'POST',
                    dataType:'json',
                    url:BASE_URL+'/profile/settings',
                data:{
                trade_url:$('input[name="trade_url"]').val()
            }
        })
        .success(function(ret){
                if(ret.success){
                    $('input[name="trade_url"]').addClass('success');
                }
                else{
                    $('input[name="trade_url"]').removeClass('success');
                    $('input[name="trade_url"]').addClass('error');
                }
            })
        }
        else{
            $('input[name="trade_url"]').removeClass('success');
        }
    });

    $('.objects .item-object .inside').click(function(){
        location = BASE_URL + '/case/'+$(this).attr('data-case-id');
    });

    $(document).on('click','.send',function(){
        var parentInfo = $(this).parents('.info');
        var drop_id = parentInfo.attr('id');

        $.ajax({
            url:BASE_URL + '/accept-drop/'+drop_id,
            method:'post',
            dataType:'json',
            cache:false,
            beforeSend:function(){
                parentInfo.html(sendingContent());
            }
        })
    })
});
//# sourceMappingURL=profile.js.map
