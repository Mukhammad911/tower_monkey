function deletePurchase(id){
    swal({
        title: "Are you sure?",
        text: "Selected purchase will be deleted",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        html: false
    }, function() {
        $.ajax({
                type: "DELETE",
                dataType: "json",
                url: deleteURL+ '/'+id,
    })
        .success(function(success){
            if(success){
                $('#purchases').DataTable().ajax.reload();
                swal(
                    "Deleted!",
                    "Purchase has been deleted.",
                    "success"
                );
            }
        });
    });
}

$(function(){
    var table = $('#purchases').DataTable({
            ajax:{
                url: dataURL,
            data: function (d) {
                //d.product_id = $('[name=product_id]').find('option:selected').val();
                //d.available = $('[name=available]').find('option:selected').val();
            }
        },
        searching:false,
        processing: true,
        serverSide: true,
        columns: [
            { data: "id"},
            { data: "bot.steam_login"},
            {
                data: "available",
                render: function(data) {
                    return '<span class="badge badge-success">'+data+'</span>';
                }
            },
            {
                data: "drops",
                render: function(data) {
                    return '<span class="badge badge-warning">'+data+'</span>';
                }
            },
            {
                data: "target",
                render: function(data) {
                    return '<span class="badge badge-flat border-primary text-primary">'+data+'</span>';
                }
            },
            {
                data: "max_price",
                render: function (data) {
                    return data/100;
                }
            },
            {
                data: 'id',
                name: 'action',
                orderable: false,
                searchable: false,
                width:200,
                render: function (data) {
                    return '<ul class="icons-list">' +
                            '<li><a title="" data-popup="tooltip" href="/'+ ADMIN_PREFIX +'/dota-purchase/'+data+'/edit" data-original-title="Edit"><i class="icon-pencil7"></i> Edit</a></li>'+
                            '<li><a title="" data-popup="tooltip" href="javascript:void(0)"  onclick="deletePurchase('+data+')" data-original-title="Delete"><i class="icon-cancel-square position-right"></i> Delete</a></li>'+
                           '</ul>';
                }
            }
        ],
        lengthMenu: [ [25, 50, 100, -1], [25, 50, 100, "All"] ]
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});