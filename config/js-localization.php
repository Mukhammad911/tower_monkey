<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Define the languages you want exported messages for
    |--------------------------------------------------------------------------
    */

    'locales' => ['en', 'ru', 'de', 'es', 'fr', 'it', 'pl', 'pt', 'ro', 'tr','cn', 'cs', 'sv', 'ja'],

    /*
    |--------------------------------------------------------------------------
    | Define the messages to export
    |--------------------------------------------------------------------------
    |
    | An array containing the keys of the messages you wish to make accessible
    | for the Javascript code.
    | Remember that the number of messages sent to the browser influences the
    | time the website needs to load. So you are encouraged to limit these
    | messages to the minimum you really need.
    |
    | Supports nesting:
    |   [ 'mynamespace' => ['test1', 'test2'] ]
    | for instance will be internally resolved to:
    |   ['mynamespace.test1', 'mynamespace.test2']
    |
    */

    'messages' => ['weapon',
        'messages.second_ago',
        'messages.sending',
        'messages.success',
        'messages.error',
        'messages.server_error',
        'messages.contact',
        'messages.sent',
        'messages.received',
        'messages.receive',
        'messages.sending',
        'messages.accept',
        'messages.sold',
        'messages.sell',
        'messages.c_error',
        'messages.money_required',
        'messages.unavailable',
        'messages.activecard',
        'messages.open_for_coins'
    ],

    /*
    |--------------------------------------------------------------------------
    | Set the keys of config properties you want to use in javascript.
    | Caution: Do not expose any configuration values that should be kept privately!
    |--------------------------------------------------------------------------
    */
    'config' => [
        /*'app.debug'  // example*/
    ],

    /*
    |--------------------------------------------------------------------------
    | Disables the config cache if set to true, so you don't have to
    | run `php artisan js-localization:refresh` each time you change configuration files.
    | Attention: Should not be used in production mode due to decreased performance.
    |--------------------------------------------------------------------------
    */
    'disable_config_cache' => false,

];
