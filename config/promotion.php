<?php

return [
    /**
    * EXAMPLE
    *
      user_id => [
	       case_id => [
	           attempt => ['id'=>id_product, 'stattrak'=>0|1, 'quality'=>quality],
           ],
	  ],
     *
     * QUALITIES
     *
     * 'Battle-Scarred'=>'Закаленное в боях',
     * 'Well-Worn'     =>'Поношенное',
     * 'Field-Tested'  =>'После полевых испытаний',
     * 'Minimal Wear'  =>'Немного поношенное',
     * 'Factory New'   =>'Прямо с завода',
     */



	/**
	 * http://topcscard.com/profile/76561198144234094 - 200$
	knife ticket - 1й bayonet tiger tooth factory new
	 */
	28 => [
		6 => [
			1 => ['id'=>431, 'stattrak'=>0, 'quality'=>'Factory New'],
		],
	],

	/**
	 * http://topcscard.com/profile/76561198109888507 - 300$
	1й covert ticket - lightning strike factory new
	 */
	20 => [
		5 => [
			1 => ['id'=>353, 'stattrak'=>0, 'quality'=>'Factory New'],
		],
	],



];