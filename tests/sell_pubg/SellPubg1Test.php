<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class TowerGame1Test
 */
class SellPubg1Test extends TestCase
{
    //use WithoutMiddleware;

    public function testSell()
    {
        $start = microtime(true);
        $user = \App\Models\User::find(98);
        $this->be($user);

        //for($i = 0; $i < 10; $i ++){
        $drop_id = 238;

        $response = $this->call('GET', '/pubg-sell-item/'.$drop_id);


        $time = microtime(true) - $start;
        \Illuminate\Support\Facades\Log::info("\n time: $time ");

        $this->assertEquals(200, $response->status());
        //}
    }

    /*public function testGetChance()
    {
        $model = new \App\CustomClasses\TowerGame();
        $chance = $model->getChance(1.4);

        \Illuminate\Support\Facades\Log::info("\n Chance: $chance ");

        $this->assertTrue(true);
    }

    public function testGetMultiplier()
    {
        $model = new \App\CustomClasses\TowerGame();
        $chance = $model->getMultiplier(50);

        \Illuminate\Support\Facades\Log::info("\n Multiplier: $chance ");

        $this->assertTrue(true);
    }

    public function testCalculateGame()
    {
        $model = new \App\CustomClasses\TowerGame();

        $result = [0, 0];
        for($i = 0; $i < 10000; $i++){
            if(!$model->calculateGame()){
                $result[0]++;
            }else{
                $result[1]++;
            }
        }

        \Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

        $this->assertTrue(true);
    }

    public function testTakeCompanyPercent()
    {
        $model = new \App\CustomClasses\TowerGame();

        $result = [0, 0];
        for($i = 0; $i < 10000; $i++){
            if(!$model->takeCompanyPercent()){
                $result[0]++;
            }else{
                $result[1]++;
            }
        }

        \Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

        $this->assertTrue(true);
    }*/
}
