<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class RouletteGame2Test
 */
class RouletteGame2Test extends TestCase
{
	//use WithoutMiddleware;

	public function testPlay()
	{
		$start = microtime(true);
		$user = \App\Models\User::find(19);
		$this->be($user);

		//for($i = 0; $i < 10; $i ++){
			$case_id = 80;

			$response = $this->call('POST', '/roulette/play', [
				'case_id' => $case_id,
				'attempts' => 1,
			]);


			$time = microtime(true) - $start;
			\Illuminate\Support\Facades\Log::info("\n time: $time ");

			$this->assertEquals(200, $response->status());
		//}
	}
}
