<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class TowerGame1Test
 */
class FlipGame1Test extends TestCase
{
	//use WithoutMiddleware;

	public function testPlay()
	{
        $start = microtime(true);
        $user = \App\Models\User::find(59);
        $this->be($user);

        //for($i = 0; $i < 10; $i ++){
        $case_id = 80;

        $response = $this->call('POST', '/coin-flip/start-join', [
            'game_id' => 31,
        ]);


        $time = microtime(true) - $start;
        \Illuminate\Support\Facades\Log::info("\n time: $time ");

        $this->assertEquals(200, $response->status());
        //}
	}

	/*public function testGetChance()
	{
		$model = new \App\CustomClasses\TowerGame();
		$chance = $model->getChance(1.4);

		\Illuminate\Support\Facades\Log::info("\n Chance: $chance ");

		$this->assertTrue(true);
	}

	public function testGetMultiplier()
	{
		$model = new \App\CustomClasses\TowerGame();
		$chance = $model->getMultiplier(50);

		\Illuminate\Support\Facades\Log::info("\n Multiplier: $chance ");

		$this->assertTrue(true);
	}

	public function testCalculateGame()
	{
		$model = new \App\CustomClasses\TowerGame();

		$result = [0, 0];
		for($i = 0; $i < 10000; $i++){
			if(!$model->calculateGame()){
				$result[0]++;
			}else{
				$result[1]++;
			}
		}

		\Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

		$this->assertTrue(true);
	}

	public function testTakeCompanyPercent()
	{
		$model = new \App\CustomClasses\TowerGame();

		$result = [0, 0];
		for($i = 0; $i < 10000; $i++){
			if(!$model->takeCompanyPercent()){
				$result[0]++;
			}else{
				$result[1]++;
			}
		}

		\Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

		$this->assertTrue(true);
	}*/
}
