<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Log;

/**
 * Class Payment1Test
 */
class Payment1Test extends TestCase
{
	use WithoutMiddleware;

    public function testPostG2payIpn()
    {
	    $user = \App\Models\User::find(19);
	    $this->be($user);

	    $response = $this->call('POST', '/pay/ipn', [
		    'userOrderId' => 1,
		    'status' => 'complete',
		    'transactionId' => 'qweasdzxc',
		    'amount' => 5,
		    'currency' => 'USD',
		    'hash' => 'bla-bla',
	    ]);

	    /*$order = \App\Models\Order::find(1);
	    $order->status = 'pending';
	    $order->save();*/

	    $this->assertEquals(200, $response->status());
    }
}
