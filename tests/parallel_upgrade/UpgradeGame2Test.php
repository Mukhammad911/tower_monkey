<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class UpgradeGame2Test
 */
class UpgradeGame2Test extends TestCase
{
	//use WithoutMiddleware;

	public function testPlay()
	{
		$user = \App\Models\User::find(19);
		$this->be($user);

		$bet = 5;
		$profit = 10;

		$response = $this->call('POST', '/upgrade', [
			'bet' => $bet,
			'profit' => $profit,
		]);

		$this->assertEquals(200, $response->status());
	}
}
