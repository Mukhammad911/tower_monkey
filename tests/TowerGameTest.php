<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class TowerGameTest
 */
class TowerGameTest extends TestCase
{
	use WithoutMiddleware;

	/*public function testPlay()
	{
		$user = \App\Models\User::find(19);
		$this->be($user);

		for($i = 0; $i < 1000; $i ++){
			$bet = mt_rand(10, 200);
			$cell = mt_rand(1, 3);
			$level = mt_rand(1, 3);
			switch($level){
				case 3: $level = 'hard'; break;
				case 2: $level = 'medium'; break;
				case 1: $level = 'easy'; break;
			}

			$this->call('POST', '/tower/start', [
				'bet' => $bet,
				'level' => $level,
				'demo' => 'no',
			]);

			$open_cell = $this->call('POST', '/tower/open-cell', [
				'bet' => $bet,
				'level' => $level,
				'cell_number' => $cell,
				'demo' => 'no',
			]);

			$open_cell_res = json_decode($open_cell->content());
			if($open_cell_res->status == 'victory'){
				$this->call('POST', '/tower/take-profit', [
				]);
			}

			$this->assertEquals(200, $open_cell->status());
		}
	}*/

	/*public function testGetChance()
	{
		$model = new \App\CustomClasses\TowerGame();
		$chance = $model->getChance(1.4);

		\Illuminate\Support\Facades\Log::info("\n Chance: $chance ");

		$this->assertTrue(true);
	}

	public function testGetMultiplier()
	{
		$model = new \App\CustomClasses\TowerGame();
		$chance = $model->getMultiplier(50);

		\Illuminate\Support\Facades\Log::info("\n Multiplier: $chance ");

		$this->assertTrue(true);
	}

	public function testCalculateGame()
	{
		$model = new \App\CustomClasses\TowerGame();

		$result = [0, 0];
		for($i = 0; $i < 10000; $i++){
			if(!$model->calculateGame()){
				$result[0]++;
			}else{
				$result[1]++;
			}
		}

		\Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

		$this->assertTrue(true);
	}

	public function testTakeCompanyPercent()
	{
		$model = new \App\CustomClasses\TowerGame();

		$result = [0, 0];
		for($i = 0; $i < 10000; $i++){
			if(!$model->takeCompanyPercent()){
				$result[0]++;
			}else{
				$result[1]++;
			}
		}

		\Illuminate\Support\Facades\Log::info("\n Victories: $result[1] \n Losses: $result[0]");

		$this->assertTrue(true);
	}*/
}
