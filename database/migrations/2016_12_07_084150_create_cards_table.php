<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('status',['erasing1', 'erasing2', 'choosing', 'winned'])->default('erasing1');
            $table->boolean('trial')->default(FALSE);
            $table->integer('drop_id')->unsigned()->nullable();
            $table->integer('case_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->boolean('extended')->default(FALSE);
            $table->integer('count_slots_visible')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards');
    }
}
