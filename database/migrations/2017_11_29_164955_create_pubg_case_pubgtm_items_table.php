<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgCasePubgtmItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_case_pubgtm_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bot_id')->unsigned();
            $table->enum('status', [ 'buyed', 'stored', 'received' ]);
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->decimal('price', 10, 2);
            $table->bigInteger('pubgtm_purchase_id')->unsigned();
            $table->bigInteger('pubgtm_bot_id')->unsigned()->nullable();
            $table->bigInteger('tradeofferid')->unsigned()->nullable();
            $table->bigInteger('tradeid')->unsigned()->nullable();
            $table->bigInteger('assetid')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('bot_id')->references('id')->on('pubg_case_bots')->onDelete('cascade');
            $table->index(['bot_id', 'status', 'pubgtm_purchase_id'], 'pubgtm_items_buyed_conditions_index');
            $table->index(['bot_id', 'status', 'classid', 'instanceid', 'pubgtm_bot_id'], 'pubgtm_items_stored_conditions_index');
            $table->index('assetid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_case_pubgtm_items');
    }
}
