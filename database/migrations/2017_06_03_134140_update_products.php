<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE products CHANGE COLUMN class class ENUM(
			'mil-spec',
			'restricted',
			'classified',
			'covert',
			'knifes',
			'common',
			'uncommon',
			'extraordinary',
			'chip'
		)");
	    DB::statement("ALTER TABLE drops CHANGE COLUMN class class ENUM(
			'mil-spec',
			'restricted',
			'classified',
			'covert',
			'knifes',
			'common',
			'uncommon',
			'extraordinary',
			'chip'
		)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
