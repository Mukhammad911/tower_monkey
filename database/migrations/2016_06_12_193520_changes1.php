<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Changes1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('life_drops', 'live_drops');

        Schema::table('case_product', function ($table) {
            $table->index('case_id');
            $table->index('product_id');
        });

        Schema::table('available_products', function ($table) {
            $table->dropColumn('case_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('live_drops', 'life_drops');

        Schema::table('case_product', function ($table) {
            $table->dropIndex('case_product_case_id_index');
            $table->dropIndex('case_product_product_id_index');
        });

        Schema::table('available_products', function ($table) {
            $table->integer('case_id');
        });
    }
}
