<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgCaseProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_case_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->tinyInteger('sort');

            $table->foreign('case_id')
                ->references('id')->on('pubg_cases')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('pubg_products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_case_product');
    }
}
