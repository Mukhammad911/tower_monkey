<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewStatusesAndOfferIdToDropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drops', function(Blueprint $table)
        {
            $table->bigInteger('offer_id')->unsigned();
        });

        DB::statement("ALTER TABLE drops CHANGE COLUMN status status ENUM('pending', 'accepted', 'sold', 'offered', 'requested') NOT NULL DEFAULT 'pending'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
