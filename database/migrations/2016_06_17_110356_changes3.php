<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Changes3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE products CHANGE COLUMN class class ENUM('mil-spec', 'restricted','classified', 'covert', 'knifes', 'common', 'uncommon')");
        DB::statement("ALTER TABLE available_products CHANGE COLUMN class class ENUM('mil-spec', 'restricted','classified', 'covert', 'knifes', 'common', 'uncommon')");

        DB::statement("ALTER TABLE available_products CHANGE COLUMN stattrack stattrak INT NOT NULL");
        DB::statement("ALTER TABLE drops CHANGE COLUMN stattrack stattrak INT NOT NULL");
        DB::statement("ALTER TABLE live_drops CHANGE COLUMN stattrack stattrak INT NOT NULL");

        Schema::table('available_products', function(Blueprint $table)
        {
            $table->string('market_name')->after('product_id');
            $table->string('image_hash')->after('market_name');
        });

        Schema::table('drops', function(Blueprint $table)
        {
            $table->string('name')->after('youtube_id');
            $table->string('short_description')->after('name');
            $table->string('market_name')->after('short_description');
            $table->string('image_hash')->after('market_name');
            $table->string('case_image')->after('image_hash');
            $table->enum('quality',['Battle-Scarred','Well-Worn','Field-Tested','Minimal Wear','Factory New'])->after('case_image');
            $table->enum('class',['mil-spec','restricted','classified','covert','knifes','common','uncommon'])->after('quality');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
