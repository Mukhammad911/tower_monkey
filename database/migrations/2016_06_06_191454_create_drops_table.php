<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drops', function ($table) {
            $table->increments('id')->unsigned();
            $table->enum('status',['pending','accepted', 'sold'])->default('pending');
            $table->decimal('price', 10, 2);
            $table->integer('case_id');
            $table->integer('product_id');
            $table->tinyInteger('stattrack');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drops');
    }
}
