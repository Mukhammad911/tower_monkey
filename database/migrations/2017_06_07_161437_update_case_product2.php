<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCaseProduct2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE `case_product` MODIFY `case_id` INT(10) UNSIGNED NOT NULL");
	    DB::statement("ALTER TABLE `case_product` MODIFY `product_id` INT(10) UNSIGNED NOT NULL");

	    DB::statement("
			ALTER TABLE `case_product`
			ADD CONSTRAINT `case_product_case_id_foreign`
			FOREIGN KEY (`case_id`)
			REFERENCES `cases` (`id`)
			ON DELETE CASCADE
		");
	    DB::statement("
			ALTER TABLE `case_product`
			ADD CONSTRAINT `case_product_product_id_foreign`
			FOREIGN KEY (`product_id`)
			REFERENCES `products` (`id`)
			ON DELETE CASCADE
		");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
