<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCounterGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counter_games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('games', 255);
            $table->integer('count')->unsigned();
        });

        DB::table('counter_games')->insert(
            array(
                ['games' => 'csgo-case', 'count' => 0],
                ['games' => 'pubg-case', 'count' => 0],
                ['games' => 'coin-flip', 'count' => 0]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('counter_games');
    }
}
