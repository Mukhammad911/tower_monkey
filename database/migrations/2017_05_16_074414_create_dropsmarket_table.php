<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropsmarketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drops_market', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->enum('status',['pending','accepted', 'sold', 'offered', 'requested'])->default('pending');
            $table->decimal('price', 10, 2);
            $table->integer('product_id');
            $table->tinyInteger('stattrak');
            $table->integer('user_id');
            $table->integer('youtube_id');
            $table->string('name', 255);
            $table->string('short_description', 255);
            $table->string('market_name', 255);
            $table->string('image_hash', 255);
            $table->enum('quality',['Battle-Scarred','Well-Worn','Field-Tested','Minimal Wear','Factory New']);
            $table->enum('class',['mil-spec','restricted','classified','covert','knifes','common','uncommon','extraordinary'])->default(NULL)->nullable();
            $table->timestamps();
            $table->bigInteger('assetid')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->integer('bot_id')->unsigned();
            $table->bigInteger('offer_id')->unsigned();
            $table->tinyInteger('toxic')->default('0');
            $table->integer('currency_id')->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drops_market');
    }
}
