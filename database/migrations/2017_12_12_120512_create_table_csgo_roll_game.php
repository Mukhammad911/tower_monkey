<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCsgoRollGame extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_flip_games', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('roll_user_id')->nullable();
            $table->integer('csgo_user_id')->nullable();
            $table->decimal('bet', 10, 2);
            $table->string('win')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coin_flip_games');
    }
}
