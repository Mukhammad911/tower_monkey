<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateLevels extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('affiliate_levels', function(Blueprint $table){
			$table->increments('id');
			$table->tinyInteger('level')->unsigned();

			$table->integer('border_amount')->unsigned();
			$table->float('bonus_amount', 4, 2)->unsigned();
			$table->float('percent', 4, 2)->unsigned();

			$table->timestamps();

			$table->unique('level');
		});

		DB::statement("INSERT INTO affiliate_levels (`level`) VALUES (1)");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('affiliate_levels');
	}
}
