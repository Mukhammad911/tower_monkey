<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoryIdsToDropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drops', function (Blueprint $table) {
            $table->bigInteger('assetid')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->integer('bot_id')->unsigned();

            $table->index('bot_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drops', function (Blueprint $table) {
            $table->dropIndex('drops_bot_id_index');
            $table->dropColumn('assetid');
            $table->dropColumn('classid');
            $table->dropColumn('instanceid');
            $table->dropColumn('bot_id');
        });
    }
}
