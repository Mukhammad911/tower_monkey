<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgCaseBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_case_bots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('steam_login');
            $table->string('steam_password');
            $table->string('steamid');
            $table->string('phone')->nullable();
            $table->string('secret')->nullable();
            $table->string('identity_secret')->nullable();
            $table->string('device_id')->nullable();
            $table->string('revocation_code')->nullable();
            $table->string('email')->nullable();
            $table->string('email_password')->nullable();
            $table->timestamps();
            $table->boolean('enabled')->default(0);
            $table->integer('status_code')->unsigned()->default(0);
            $table->string('status_msg')->nullable();
            $table->string('api_key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_case_bots');
    }
}
