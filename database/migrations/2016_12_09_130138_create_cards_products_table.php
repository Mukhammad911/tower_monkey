<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_products', function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('card_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards_products');
    }
}
