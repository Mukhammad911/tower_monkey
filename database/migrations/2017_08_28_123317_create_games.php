<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function(Blueprint $table){
	        $table->increments('id');
	        $table->integer('user_id')->unsigned()->nullable();
	        $table->integer('roulette_id')->unsigned()->nullable();
	        $table->integer('upgrade_id')->unsigned()->nullable();
	        $table->integer('tower_id')->unsigned()->nullable();
	        $table->timestamps();


	        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
	        $table->foreign('roulette_id')->references('id')->on('drops')->onDelete('cascade');
	        $table->foreign('upgrade_id')->references('id')->on('upgrades')->onDelete('cascade');
	        $table->foreign('tower_id')->references('id')->on('towers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('games');
    }
}
