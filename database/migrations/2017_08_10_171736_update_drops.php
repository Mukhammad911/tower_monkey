<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDrops2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('drops', function(Blueprint $table){
		    $table->dropColumn('win_number');
	    });
	    Schema::table('drops', function(Blueprint $table){
		    $table->mediumInteger('win_number');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
