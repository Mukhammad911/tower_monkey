<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyCaseIdToCasePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE case_price CHANGE COLUMN case_id case_id INT UNSIGNED NOT NULL");

        Schema::table('case_price', function (Blueprint $table) {
            $table->foreign('case_id')
                ->references('id')->on('cases')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('case_price', function (Blueprint $table) {
            $table->dropForeign('case_price_case_id_foreign');
        });
    }
}
