<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropForeign('purchases_product_id_foreign');

            $table->dropColumn('product_id');
            $table->dropColumn('available');
            $table->integer('max_price')->unsigned()->default(0);
            $table->integer('low_price_limit')->unsigned()->default(0);
            $table->integer('mid_price_limit')->unsigned()->default(0);
            $table->integer('low_quota')->unsigned()->default(0);
            $table->integer('mid_quota')->unsigned()->default(0);
            $table->integer('high_quota')->unsigned()->default(0);

            DB::table('purchases')->truncate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            //
        });
    }
}
