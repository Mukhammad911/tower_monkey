<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bot_id');
            $table->integer('max_price');
            $table->integer('min_price');
            $table->string('quality');
            $table->string('rarity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_purchases');
    }
}
