<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDotaPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dota_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bot_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('target')->unsigned()->default(1);
            $table->integer('available')->unsigned()->default(0);
            $table->timestamps();

            /*$table->foreign('bot_id')
                ->references('id')->on('dota_bots')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('dota_products')
                ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dota_purchases');
    }
}



