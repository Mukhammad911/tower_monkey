<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE `cases` MODIFY `category_id` INT(10) UNSIGNED NOT NULL");

	    DB::statement("
			ALTER TABLE `cases`
			ADD CONSTRAINT `cases_category_id_foreign`
			FOREIGN KEY (`category_id`)
			REFERENCES `categories` (`id`)
			ON DELETE CASCADE
		");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
