<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateScenariosStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE `scenarios` CHANGE COLUMN `status` `status` ENUM(
			'disabled',
			'enabled',
			'completed',
			'not available'
		)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE `scenarios` CHANGE COLUMN `status` `status` ENUM(
			'disabled',
			'enabled',
			'completed'
		)");
    }
}
