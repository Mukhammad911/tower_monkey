<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drop_stats', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('case_id');
            $table->integer('count');
            $table->decimal('summ', 10, 2);
            $table->decimal('profit', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('drop_stats');
    }
}
