<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_products', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('short_description');
            $table->enum('image_source',['url','file'])->default('url');
            $table->text('image');
            $table->string('class');
            $table->decimal('purchase_price', 5, 2);
            $table->decimal('price', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_products');
    }
}
