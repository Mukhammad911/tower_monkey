<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumPriceOpskinsPubgCaseTm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pubg_case_pubgtm_items', function ($table) {
            $table->decimal('opskins_price', 10, 2)->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pubg_case_pubgtm_items', function (Blueprint $table)
        {
            $table->dropColumn('opskins_price');
        });
    }
}
