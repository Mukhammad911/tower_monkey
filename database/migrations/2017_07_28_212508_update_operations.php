<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOperations4 extends Migration {
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
		DB::statement( "ALTER TABLE `operations` CHANGE COLUMN `description` `description` ENUM(
			'created by moderator',
		    'extra erase card',
		    'credit',
		    'promo bonus',
		    'money back',
		    'g2a transaction',
		    'referral bonus',
		    'sell skin',
		    'expired sell skin',
		    'withdrawal',
		    'upgrade bet',
		    'upgrade win',
		    'tower bet',
		    'tower win',
		    'case opening',
		    'roulette win'
		)" );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		DB::statement( "ALTER TABLE `operations` CHANGE COLUMN `description` `description` ENUM(
			'created by moderator',
		    'case opening',
		    'extra erase card',
		    'credit',
		    'promo bonus',
		    'money back',
		    'g2a transaction',
		    'referral bonus',
		    'sell skin',
		    'expired sell skin',
		    'withdrawal',
		    'upgrade',
		    'tower bet',
		    'tower win',
		    'roulette win'
		)" );
	}
}
