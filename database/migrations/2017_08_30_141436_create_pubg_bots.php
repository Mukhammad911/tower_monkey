<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgBots extends Migration
{
    public function up()
    {
        Schema::create('pubg_bots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('steam_login');
            $table->string('steam_password');
            $table->string('phone');
            $table->string('secret');
            $table->string('revocation_code');
            $table->string('email');
            $table->string('email_password');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_bots');
    }
}
