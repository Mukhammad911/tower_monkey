<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOperationsDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE `operations` CHANGE COLUMN `description` `description` ENUM(
			'created by moderator',
		    'case opening',
		    'extra erase card',
		    'credit',
		    'promo bonus',
		    'money back',
		    'g2a transaction',
		    'referral bonus',
		    'sell skin',
		    'expired sell skin'
		)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::statement("ALTER TABLE `operations` CHANGE COLUMN `description` `description` ENUM(
			'created by moderator',
		    'case opening',
		    'extra erase card',
		    'credit',
		    'promo bonus',
		    'money back',
		    'g2a transaction',
		    'referral bonus',
		    'sell skin'
		)");
    }
}
