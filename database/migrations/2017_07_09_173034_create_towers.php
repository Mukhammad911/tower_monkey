<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('towers', function(Blueprint $table){
		    $table->increments('id');

		    $table->integer('user_id')->unsigned();
		    $table->float('bet');
		    $table->float('profit');
		    $table->enum('level', [
			    'easy',
			    'medium',
			    'hard',
		    ]);
		    $table->tinyInteger('step')->unsigned();
		    $table->enum('status', [
				'started',
			    'closed'
		    ]);
		    $table->boolean('demo');

		    $table->timestamps();

		    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('towers');
    }
}
