<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpgrades extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('upgrades', function(Blueprint $table){
			$table->increments('id');

			$table->integer('user_id')->unsigned();
			$table->float('bet');
			$table->float('profit');
			$table->float('multiplier');
			$table->boolean('win');
			$table->boolean('demo');

			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('upgrades');
	}
}
