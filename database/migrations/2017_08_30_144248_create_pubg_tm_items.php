<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgTmItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubgtm_items', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('bot_id');
            $table->enum('status', [ 'buyed', 'stored', 'received', ]);
            $table->bigInteger('classid');
            $table->bigInteger('instanceid');
            $table->decimal('price', 10, 2);
            $table->bigInteger('csgotm_purchase_id');
            $table->bigInteger('csgotm_bot_id');
            $table->bigInteger('tradeofferid');
            $table->bigInteger('tradeid');
            $table->bigInteger('assetid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubgtm_items');
    }
}
