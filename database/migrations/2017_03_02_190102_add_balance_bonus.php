<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBalanceBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('balance', function (Blueprint $table)
		{
			$table->tinyInteger('type')->unsigned()->after('summ');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('balance', function (Blueprint $table)
		{
			$table->dropColumn('type');
		});
	}
}
