<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_slots', function (Blueprint $table)
        {
            $table->increments('id');

            $table->integer('card_id')->unsigned();
            $table->integer('number')->unsigned();
            $table->boolean('extra');
            $table->integer('product_id')->unsigned()->nullable();
            $table->boolean('visible')->default(FALSE);

            $table->timestamps();

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');

            $table->unique(['card_id', 'number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards_slots');
    }
}
