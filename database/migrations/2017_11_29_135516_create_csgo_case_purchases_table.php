<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsgoCasePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csgo_case_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bot_id')->unsigned();
            $table->integer('target')->unsigned()->default(1);
            $table->integer('max_price')->unsigned()->default(0);
            $table->integer('low_price_limit')->unsigned()->default(0);
            $table->integer('mid_price_limit')->unsigned()->default(0);
            $table->integer('low_quota')->unsigned()->default(0);
            $table->integer('mid_quota')->unsigned()->default(0);
            $table->integer('high_quota')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('bot_id')
                ->references('id')->on('csgo_case_bots')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('csgo_case_purchases');
    }
}
