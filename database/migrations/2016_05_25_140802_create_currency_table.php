<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currency', function ($table) {
			$table->increments('id')->unsigned();
			$table->string('title', 255);
			$table->string('symbol_left', 12);
			$table->string('symbol_right', 12);
			$table->string('code', 3);
			$table->string('class',255);
			$table->integer('decimal_place');
			$table->string('decimal_point', 3);
			$table->string('thousand_point', 3);
			$table->integer('status');
			$table->timestamps();
		});

		$currencies = [
			[
				'title' => 'U.S. Dollar',
				'symbol_left' => '$',
				'symbol_right' => '',
				'code' => 'USD',
				'class'=>'fa-usd',
				'decimal_place' => 2,
				'decimal_point' => '.',
				'thousand_point' => ',',
				'status' => 1,
			],
			[
				'title' => 'Euro',
				'symbol_left' => '€',
				'symbol_right' => '',
				'code' => 'EUR',
				'class'=>'fa-eur',
				'decimal_place' => 2,
				'decimal_point' => '.',
				'thousand_point' => ',',
				'status' => 0,
			],
			[
				'title' => 'Russian ruble',
				'symbol_left' => '₽',
				'symbol_right' => '',
				'code' => 'RUB',
				'class'=>'fa-rub',
				'decimal_place' => 2,
				'decimal_point' => '.',
				'thousand_point' => ',',
				'status' => 1,
			],
		];

		DB::table('currency')->insert($currencies);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currency');
	}

}
