<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLastWinsTableSafe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `last_wins` CHANGE `game` `game` ENUM(
            'tower',
            'pubg-case-roulette',
            'csgo-case-roulette',
            'coin-flip'
        ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `last_wins` CHANGE `game` `game` ENUM(
            'tower',
            'pubg-case-roulette',
            'csgo-case-roulette',
            'coin-flip'
        ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
    }
}
