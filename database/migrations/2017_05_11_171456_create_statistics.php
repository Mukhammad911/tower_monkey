<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatistics extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('statistics', function(Blueprint $table){
			$table->increments('id');
			$table->enum('key', [
				'income',
				'expense',
				'current_profit',
				'profit_deviation_percent'
			]);
			$table->float('value', 12, 4);

			$table->timestamps();
			$table->index('key');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('statistics');
	}
}
