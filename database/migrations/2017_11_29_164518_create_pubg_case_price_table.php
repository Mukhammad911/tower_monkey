<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgCasePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_case_price', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('case_id')->unsigned();
            $table->integer('currency_id');
            $table->decimal('price', 10, 2);

            $table->foreign('case_id')
                ->references('id')->on('pubg_cases')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_case_price');
    }
}
