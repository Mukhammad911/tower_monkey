<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOperationsWithoutCrash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `operations` CHANGE `description` `description` ENUM(
            'created by moderator',
            'extra erase card',
            'credit',
            'promo bonus',
            'money back',
            'g2a transaction',
            'skin-pay transaction',
            'referral bonus',
            'sell skin',
            'expired sell skin',
            'withdrawal',
            'upgrade bet',
            'upgrade win',
            'tower bet',
            'tower win',
            'case opening',
            'roulette win',
            'upgrade bet youtube',
            'upgrade win youtube',
            'tower bet youtube',
            'tower win youtube',
            'case opening youtube',
            'roulette win youtube',
            'tower win from promo',
            'upgrade win from promo',
            'roulette win from promo',
            'csgo case opening',
            'csgo case win',
            'csgo case sell',
            'case opening from promo',
            'csgo case opening from promo',
            'upgrade bet from promo',
            'tower bet from promo',
            'dota case sell',
            'dota case opening',
            'dota case win',
            'dota case opening from promo',
            'money back csgo case',
            'money back dota case',
            'pubg case sell',
            'pubg case opening',
            'pubg case win',
            'pubg case opening from promo',
            'money back pubg case',
            'money back csgo case youtube',
            'money back dota case youtube',
            'money back pubg case youtube',
            'csgo case win youtube',
            'dota case win youtube',
            'pubg case win youtube',
            'csgo case opening youtube',
            'dota case opening youtube',
            'pubg case opening youtube'
        ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `operations` CHANGE `description` `description` ENUM(
            'created by moderator',
            'extra erase card',
            'credit',
            'promo bonus',
            'money back',
            'g2a transaction',
            'skin-pay transaction',
            'referral bonus',
            'sell skin',
            'expired sell skin',
            'withdrawal',
            'upgrade bet',
            'upgrade win',
            'tower bet',
            'tower win',
            'case opening',
            'roulette win',
            'upgrade bet youtube',
            'upgrade win youtube',
            'tower bet youtube',
            'tower win youtube',
            'case opening youtube',
            'roulette win youtube',
            'tower win from promo',
            'upgrade win from promo',
            'roulette win from promo',
            'csgo case opening',
            'csgo case win',
            'csgo case sell',
            'case opening from promo',
            'csgo case opening from promo',
            'upgrade bet from promo',
            'tower bet from promo',
            'dota case sell',
            'dota case opening',
            'dota case win',
            'dota case opening from promo',
            'money back csgo case',
            'money back dota case',
            'pubg case sell',
            'pubg case opening',
            'pubg case win',
            'pubg case opening from promo',
            'money back pubg case',
            'money back csgo case youtube',
            'money back dota case youtube',
            'money back pubg case youtube',
            'csgo case win youtube',
            'dota case win youtube',
            'pubg case win youtube',
            'csgo case opening youtube',
            'dota case opening youtube',
            'pubg case opening youtube'
        ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
    }
}
