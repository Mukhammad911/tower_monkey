<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Changes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('users', function ($table) {
            $table->index('steamid');
        });

        Schema::table('cases', function ($table) {
            $table->string('class')->after('name');
        });

        DB::statement("ALTER TABLE products CHANGE COLUMN class class ENUM('mil-spec', 'destricted','classified', 'covert', 'knifes', 'common', 'uncommon')");
        DB::statement("ALTER TABLE products CHANGE COLUMN price price decimal(10,2)");

        Schema::table('products', function ($table) {
            $table->dropColumn('purchase_price');
        });

        Schema::table('case_product', function ($table) {
            $table->tinyInteger('sort');
        });

        Schema::table('currency', function ($table) {
            $table->decimal('ruble_course', 5, 3)->after('status');
        });

        Schema::table('drops', function ($table) {
            $table->integer('youtube_id')->after('user_id');
            $table->index('case_id');
            $table->index('product_id');
            $table->index('user_id');
            $table->index('youtube_id');
        });

        Schema::table('balance', function ($table) {
            $table->index('user_id');
            $table->index('currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drops', function ($table) {
            $table->dropIndex('drops_case_id_index');
            $table->dropIndex('drops_product_id_index');
            $table->dropIndex('drops_user_id_index');
            $table->dropIndex('drops_youtube_id_index');
        });

        Schema::table('balance', function ($table) {
            $table->dropIndex('balance_currency_id_index');
            $table->dropIndex('balance_user_id_index');
        });

        Schema::table('users', function ($table) {
            $table->dropIndex('users_steamid_index');
        });
    }
}
