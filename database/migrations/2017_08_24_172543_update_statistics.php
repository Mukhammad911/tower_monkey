<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('statistics', function(Blueprint $table){
		    $table->enum('game', [
			    'roulette',
			    'upgrade',
			    'tower',
		    ])->after('value');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('statistics', function(Blueprint $table){
		    $table->dropColumn('game');
	    });
    }
}
