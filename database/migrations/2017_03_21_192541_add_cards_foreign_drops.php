<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardsForeignDrops extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cards', function (Blueprint $table)
		{
			$table->foreign('drop_id')->references('id')->on('drops')->onDelete('cascade');
			$table->foreign('case_id')->references('id')->on('cases')->onDelete('cascade');
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cards', function (Blueprint $table)
		{
			$table->dropForeign([
				'drop_id',
				'case_id',
				'product_id',
			]);
		});
	}
}
