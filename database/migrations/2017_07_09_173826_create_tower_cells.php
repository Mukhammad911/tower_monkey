<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTowerCells extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('tower_cells', function(Blueprint $table){
		    $table->increments('id');

		    $table->integer('tower_id')->unsigned();
		    $table->tinyInteger('step')->unsigned();
		    $table->tinyInteger('cell')->unsigned();
		    $table->boolean('win');
		    $table->string('user_seed', 64);
		    $table->string('server_seed', 64);
		    $table->smallInteger('win_number')->unsigned();

		    $table->timestamps();

		    $table->foreign('tower_id')->references('id')->on('towers')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('tower_cells');
    }
}
