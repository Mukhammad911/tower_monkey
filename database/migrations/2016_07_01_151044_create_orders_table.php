<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payment_logs');

        Schema::create('orders', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('payment_system')->default('g2pay');
            $table->string('transaction_id');
            $table->integer('currency_id');
            $table->decimal('amount',10,2);
            $table->integer('user_id')->unsigned();
            $table->enum('status',['pending','complete','rejected','cancelled','refunded','partial_refunded'])->default('pending');
            $table->timestamps();

            $table->index('currency_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

        Schema::create('payment_logs', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('payment_system')->default('g2pay');
            $table->integer('currency_id');
            $table->decimal('amount',10,2);
            $table->integer('user_id')->unsigned();
            $table->enum('status',['pending','complete','rejected','cancelled','refunded'])->default('pending');
            $table->timestamps();

            $table->index('currency_id');
            $table->index('user_id');
        });
    }
}
