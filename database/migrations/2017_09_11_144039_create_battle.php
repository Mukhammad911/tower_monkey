<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function(Blueprint $table){
	        $table->increments('id');
	        $table->integer('host_id')->unsigned();
	        $table->integer('winner_id')->unsigned();
	        $table->tinyInteger('players')->unsigned();
	        $table->tinyInteger('rounds')->unsigned();
	        $table->tinyInteger('current_round')->unsigned();
	        $table->tinyInteger('price')->unsigned();
	        $table->enum('status', [
		        'created',
		        'playing',
		        'closed',
	        ]);
	        $table->timestamps();


	        $table->foreign('host_id')->references('id')->on('users')->onDelete('cascade');
	        $table->foreign('winner_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('battles');
    }
}
