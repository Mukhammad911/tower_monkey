<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('withdrawals', function(Blueprint $table){
		    $table->dropColumn('status');
	    });

	    Schema::table('withdrawals', function(Blueprint $table){
		    $table->enum('status', [
			    'pending',
			    'rejected',
			    'completed'
		    ])->default('pending')->after('details');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
