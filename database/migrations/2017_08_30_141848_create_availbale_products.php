<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailbaleProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_available_products', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('short_description');
            $table->string('quality');
            $table->tinyInteger('stattrack');
            $table->text('image');
            $table->enum('class', ['mil-spec', 'destricted','classified', 'covert', 'knifes', 'common', 'uncommon']);
            $table->decimal('price', 10, 2);
            $table->integer('case_id');
            $table->integer('product_id');
            $table->timestamps();
            $table->index('case_id');
            $table->index('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_available_products');
    }
}
