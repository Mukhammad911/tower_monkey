<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('operations', function(Blueprint $table){
		    $table->increments('id');
		    $table->integer('user_id')->unsigned();
		    $table->float('amount');
		    $table->tinyInteger('currency_id')->unsigned();
		    $table->enum('description', [
			    'created by moderator',
			    'case opening',
			    'extra erase card',
			    'credit',
				'promo bonus',
			    'money back',
			    'g2a transaction',
			    'referral bonus',
			    'sell skin',
		    ]);

		    $table->timestamps();

		    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('operations');
    }
}
