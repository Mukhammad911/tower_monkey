<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStattrakFieldType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE available_products CHANGE COLUMN stattrak stattrak BOOLEAN NOT NULL");
        DB::statement("ALTER TABLE drops CHANGE COLUMN stattrak stattrak BOOLEAN NOT NULL");
        DB::statement("ALTER TABLE live_drops CHANGE COLUMN stattrak stattrak BOOLEAN NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
