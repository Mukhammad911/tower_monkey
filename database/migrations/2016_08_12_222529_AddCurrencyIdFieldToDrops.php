<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyIdFieldToDrops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drops', function ($table) {
            $table->integer('currency_id')->unsigned()->default(1);
            $table->index('currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drops', function ($table) {
            $table->dropIndex('drops_currency_id_index');
            $table->dropColumn('currency_id');
        });
    }
}
