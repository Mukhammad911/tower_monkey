<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveTowers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_towers', function(Blueprint $table){
	        $table->increments('id');
	        $table->integer('tower_id')->unsigned();
	        $table->timestamps();

	        $table->foreign('tower_id')->references('id')->on('towers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('live_towers');
    }
}
