<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsgoCaseDropsMarketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csgo_case_drops_market', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->enum('status',['pending', 'accepted', 'sold', 'offered', 'requested'])->default('pending');
            $table->decimal('price', 10, 2);
            $table->integer('case_id');
            $table->integer('product_id');
            $table->tinyInteger('stattrak');
            $table->integer('user_id');
            $table->integer('youtube_id');
            $table->string('name', 255);
            $table->string('short_description', 255)->nullable();
            $table->string('market_name', 255);
            $table->string('image_hash', 255);
            $table->string('case_image', 255);
            $table->enum('quality',['Battle-Scarred','Well-Worn','Field-Tested','Minimal Wear','Factory New'])->default(NULL)->nullable();
            $table->enum('class',['mil-spec','restricted','classified','covert','knifes','common','uncommon','extraordinary'])->default(NULL)->nullable();
            $table->bigInteger('assetid')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->integer('bot_id')->unsigned();
            $table->bigInteger('offer_id')->unsigned();
            $table->boolean('toxic')->default(0);
            $table->integer('currency_id')->unsigned()->default(1);
            $table->string('user_seed', 64);
            $table->string('server_seed', 64);
            $table->mediumInteger('win_number');
            $table->timestamps();

            $table->index('price');
            $table->index('case_id');
            $table->index('product_id');
            $table->index('user_id');
            $table->index('youtube_id');
            $table->index('bot_id');
            $table->index('currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('csgo_case_drops_market');
    }
}
