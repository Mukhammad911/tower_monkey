<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTowerSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('tower_settings', function(Blueprint $table){
		    $table->string('symbol', 10)->after('value');
		    $table->string('description')->after('symbol');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('tower_settings', function(Blueprint $table){
		    $table->dropColumn([
			    'symbol',
			    'description'
		    ]);
	    });
    }
}
