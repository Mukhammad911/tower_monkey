<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraordinary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::statement("ALTER TABLE products CHANGE COLUMN class class ENUM('mil-spec', 'restricted','classified', 'covert', 'knifes', 'common', 'uncommon', 'extraordinary')");
	    DB::statement("ALTER TABLE available_products CHANGE COLUMN class class ENUM('mil-spec', 'restricted','classified', 'covert', 'knifes', 'common', 'uncommon', 'extraordinary')");
	    DB::statement("ALTER TABLE drops CHANGE COLUMN class class ENUM('mil-spec', 'restricted','classified', 'covert', 'knifes', 'common', 'uncommon', 'extraordinary')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
