<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('operations', function(Blueprint $table){
		    $table->dropColumn('description');
	    });

	    Schema::table('operations', function(Blueprint $table){
		    $table->enum('description', [
			    'created by moderator',
			    'case opening',
			    'extra erase card',
			    'credit',
			    'promo bonus',
			    'money back',
			    'g2a transaction',
			    'referral bonus',
			    'sell skin',
			    'expired sell skin',
			    'withdrawal',
		    ])->after('currency_id');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
