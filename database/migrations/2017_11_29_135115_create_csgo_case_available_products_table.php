<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsgoCaseAvailableProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csgo_case_available_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('short_description');
            $table->string('quality');
            $table->tinyInteger('stattrak');
            $table->enum('class', ['mil-spec','restricted','classified','covert','knifes','common','uncommon','extraordinary'])->nullable();
            $table->integer('product_id')->unsigned();
            $table->decimal('price', 10, 2);
            $table->string('image_hash');
            $table->string('market_name');
            $table->bigInteger('assetid')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->integer('bot_id')->unsigned();
            $table->timestamps();

            $table->index('product_id');

            $table->foreign('bot_id')
                ->references('id')->on('csgo_case_bots')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('csgo_case_available_products');
    }
}
