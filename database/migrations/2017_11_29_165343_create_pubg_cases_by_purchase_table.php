<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubgCasesByPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pubg_cases_by_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id')->unsigned();
            $table->integer('purchase_id')->unsigned();

            $table->foreign('case_id')
                ->references('id')->on('pubg_cases')
                ->onDelete('cascade');

            $table->foreign('purchase_id')
                ->references('id')->on('pubg_case_purchases')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pubg_cases_by_purchase');
    }
}
