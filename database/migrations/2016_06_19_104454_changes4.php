<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Changes4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE case_product CHANGE COLUMN sort sort INT NOT NULL");
        DB::statement("ALTER TABLE products CHANGE COLUMN price price INT NOT NULL");
        DB::statement("ALTER TABLE available_products CHANGE COLUMN price price INT NOT NULL");
        DB::statement("ALTER TABLE drops CHANGE COLUMN price price INT NOT NULL");
        DB::statement("ALTER TABLE drop_stats CHANGE COLUMN summ summ INT NOT NULL");
        DB::statement("ALTER TABLE drop_stats CHANGE COLUMN profit profit INT NOT NULL");

        Schema::table('settings', function ($table) {
            $table->decimal('rub_usd_sell', 10, 2);
            $table->decimal('rub_usd_buy', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
