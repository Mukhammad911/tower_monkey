<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUpgradeSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('upgrade_settings', function(Blueprint $table){
		    $table->dropColumn([
			    'multiplier',
			    'chance'
		    ]);

		    $table->string('key')->after('id');
		    $table->string('value')->after('key');
		    $table->string('symbol')->after('value');
		    $table->string('description')->after('symbol');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
