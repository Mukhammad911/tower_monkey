<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Changes5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('available_products', function ($table) {
            $table->dropColumn('image');
        });

        DB::statement("ALTER TABLE available_products CHANGE COLUMN stattrak stattrak BIT NOT NULL");
        DB::statement("ALTER TABLE drops CHANGE COLUMN stattrak stattrak BIT NOT NULL");
        DB::statement("ALTER TABLE live_drops CHANGE COLUMN stattrak stattrak BIT NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
