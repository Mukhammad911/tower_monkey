<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattleCases extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('battle_cases', function(Blueprint $table){
			$table->increments('id');
			$table->integer('battle_id')->unsigned();
			$table->integer('case_id')->unsigned();
			$table->timestamps();

			$table->foreign('battle_id')->references('id')->on('battles')->onDelete('cascade');
			$table->foreign('case_id')->references('id')->on('cases')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('battle_cases');
	}
}
