<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsgoCaseByPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('case_purchase');

        Schema::create('case_purchase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('case_id')->unsigned();
            $table->integer('purchase_id')->unsigned();

            $table->foreign('case_id')
                ->references('id')->on('cases')
                ->onDelete('cascade');

            $table->foreign('purchase_id')
                ->references('id')->on('csgo_case_purchases')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('case_purchase');
    }
}
