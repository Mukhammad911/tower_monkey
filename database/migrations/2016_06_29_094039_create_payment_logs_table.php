<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_logs', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->string('payment_system')->default('g2pay');
            $table->integer('currency_id');
            $table->decimal('amount',10,2);
            $table->integer('user_id')->unsigned();
            $table->enum('status',['pending','complete','rejected','cancelled','refunded'])->default('pending');
            $table->timestamps();

            $table->index('currency_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_logs');
    }
}
