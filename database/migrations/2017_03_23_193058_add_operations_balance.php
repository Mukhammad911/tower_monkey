<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOperationsBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('operations', function (Blueprint $table)
	    {
		    $table->float('balance')->after('amount');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('operations', function (Blueprint $table)
	    {
		    $table->dropColumn('balance');
	    });
    }
}
