<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CsgoPricesFieldTestedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csgo_prices_field_tested', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id');
            $table->decimal('price', 10, 2)->nullable();;
            $table->integer('opskins')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('csgo_prices_field_tested');
    }
}
