<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->integer('sort');
			$table->timestamps();
		});

		$categories = [
			['name'=>'weapon',
			'sort'=>1],
			['name'=>'cases','sort'=>2],
			['name'=>'collections','sort'=>3],
		];

		DB::table('categories')->insert($categories);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
