<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastWins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('last_wins', function(Blueprint $table){
		    $table->increments('id');
		    $table->enum('game', [
			    'roulette',
			    'upgrade',
			    'tower',
		    ]);
		    $table->integer('game_id')->unsigned();

		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('last_wins');
    }
}
