<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('invoice_id', 255)->nullable()->after('payment_system');
            $table->string('user_ip', 255)->nullable()->after('status');
            $table->text('payer_info')->nullable()->after('user_ip');
            $table->text('geo_info')->nullable()->after('payer_info');
            $table->enum('status',[
                'success',
                'failed',
                'pending',
                'complete',
                'rejected',
                'cancelled',
                'refunded',
                'partial_refunded'
            ])->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('invoice_id', 255)->nullable()->after('payment_system');
            $table->string('user_ip', 255)->nullable()->after('status');
            $table->text('payer_info')->nullable()->after('user_ip');
            $table->text('geo_info')->nullable()->after('payer_info');
            $table->enum('status',[
                'pending',
                'complete',
                'rejected',
                'cancelled',
                'refunded',
                'partial_refunded'
            ])->default('pending');
        });
    }
}
