<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('languages', function ($table) {
			$table->increments('id')->unsigned();
			$table->string('name', 255);
			$table->char('locale', 3);
			$table->char('code', 3)->nullable();
			$table->smallInteger('sort');
			$table->integer('currency_id')->default(0);
			$table->tinyInteger('active');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('languages');
	}

}
