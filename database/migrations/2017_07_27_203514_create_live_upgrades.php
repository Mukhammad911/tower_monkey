<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveUpgrades extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('live_upgrades', function(Blueprint $table){
			$table->increments('id');
			$table->integer('upgrade_id')->unsigned();
			$table->timestamps();

			$table->foreign('upgrade_id')->references('id')->on('upgrades')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('live_upgrades');
	}
}
