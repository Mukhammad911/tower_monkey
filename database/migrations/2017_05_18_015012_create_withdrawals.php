<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawals', function(Blueprint $table){
	        $table->increments('id');
	        $table->integer('user_id')->unsigned();
	        $table->float('amount');
	        $table->enum('type', [
		        'paypal',
		        'visa',
		        'bitcoin',
		        'cs_go',
		        'dota_2'
	        ]);
	        $table->string('details', 1000);
	        $table->enum('status', [
		        'pending',
		        'rejected',
		        'completed'
	        ]);

	        $table->timestamps();
	        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('withdrawals');
    }
}
