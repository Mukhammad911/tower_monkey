<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInventoryIdsToAvailableProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('available_products', function (Blueprint $table) {
            $table->bigInteger('assetid')->unsigned();
            $table->bigInteger('classid')->unsigned();
            $table->bigInteger('instanceid')->unsigned();
            $table->integer('bot_id')->unsigned();

            $table->foreign('bot_id')
                ->references('id')->on('bots')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('available_products', function (Blueprint $table) {
            $table->dropForeign('available_products_bot_id_foreign');

            $table->dropColumn('assetid');
            $table->dropColumn('classid');
            $table->dropColumn('instanceid');
            $table->dropColumn('bot_id');
        });
    }
}
