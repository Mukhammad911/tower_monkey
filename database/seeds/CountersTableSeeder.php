<?php

use App\Models\Counter;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CountersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $classes = [
            'mil-spec',
            'covert',
            'restricted',
            'classified',
            'knifes',
            'stattrak',
            'uncommon',
            'common',
            'extraordinary',
        ];

        foreach($classes as $class) {
            Counter::create(['class'=>$class]);
        }
    }
}
