<?php

use App\Models\Bot;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BotTableSeeder extends Seeder {
    public function run() {

        Model::unguard();

        $bots = [
            ['dmenning','RFfaG7OA','dmenning@mail.ru','RFfaG7OA'],
            ['ronald_keysi','Fx2CbzHK','ronald.keysi@mail.ru','Fx2CbzHK'],
            ['pmakdonald1','hM8aYH6P','pmakdonald@bk.ru','hM8aYH6P'],
            ['ubriggs','wjMA1XkO','ubriggs@mail.ua','wjMA1XkO'],
            ['charlz_rouz','fnDcr192','charlz.rouz@mail.ru','fnDcr192'],
            ['dkhenkok','wzEInRiCCKZl','dkhenkok@mail.ua','wzEInRiCCKZl'],
            ['derek_armstrong','OXWI3d73rZ9d','derek-armstrong@mail.ru','OXWI3d73rZ9d'],
            ['stiven_rouz','vGPRnEVutwH6','stiven.rouz@mail.ru','vGPRnEVutwH6'],
            ['donald_stoun','fspbJ4ruoagc','donald.stoun@mail.ru','fspbJ4ruoagc'],
            ['okhemilton','67Q6RzYcPsk1','okhemilton@mail.ru','67Q6RzYcPsk1'],
            ['rleng3','XotXGDc1pqsY','rleng@mail.ua','XotXGDc1pqsY'],
            ['khabbard','QakLqYAjlv3p','khabbard@mail.ua','QakLqYAjlv3p'],
            ['pbarnett2','INfJnug9iobY','pbarnett@mail.ua','INfJnug9iobY'],
            ['bfergyuson','jbEZYCavcyRn','bfergyuson@mail.ru','jbEZYCavcyRn'],
        ];

        $bots = array_map([$this, 'mapKeys'], $bots);

        foreach($bots as $bot) {
            Bot::create($bot);
        }
    }

    private function mapKeys ($values) {
        $botKeys = ['steam_login', 'steam_password', 'email', 'email_password', 'phone', 'secret', 'revocation_code'];

        $keysCount = count($botKeys);
        $valuesCount = count($values);
        if($keysCount > $valuesCount) {
            $stabArray = array_fill($valuesCount, $keysCount-$valuesCount, null);
            $values = $values+$stabArray;
        }

        return array_combine($botKeys, $values);
    }
}