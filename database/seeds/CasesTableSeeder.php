<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\SteamCase;
use App\Models\DropStats;

class CasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $cases = [
            [1, 'mil-spec', 'mil-spec', 'mil-spec.png', 1, 1],
            [1, 'restricted', 'restricted', 'restricted.png', 1, 2],
            [1, 'classified', 'classified', 'classified.png', 1, 3],
            [1, 'covert', 'covert', 'covert.png', 1, 4],
            [1, 'knifes', 'knifes', 'knifes.png', 1, 5]
        ];

        $cases = array_map([$this, 'mapKeys'], $cases);

        foreach($cases as $case) {
            $case = SteamCase::create($case);
            DropStats::create(['case_id'=>$case->id]);
        }
    }

    private function mapKeys ($values) {
        $botKeys = ['category_id', 'name', 'class', 'image', 'active', 'sort'];

        $keysCount = count($botKeys);
        $valuesCount = count($values);
        if($keysCount > $valuesCount) {
            $stabArray = array_fill($valuesCount, $keysCount-$valuesCount, null);
            $values = $values+$stabArray;
        }

        return array_combine($botKeys, $values);
    }
}
