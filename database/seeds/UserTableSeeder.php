<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Bican\Roles\Models\Role;

class UserTableSeeder extends Seeder {
    public function run() {
        $userRole = Role::find(2);

        $user = User::create([
                    'name'		=> 'Admin',
                    'email'		=> 'admin@gmail.com',
                    'password'	=> bcrypt('admin'),
                    'active'	=> 1
                ]);

        $user->attachRole($userRole);

        $userRole = Role::find(3);

        $user = User::create([
            'name'		=> 'Youtuber',
            'email'		=> 'youtube@gmail.com',
            'password'	=> bcrypt('youtube'),
            'active'	=> 1
        ]);

        $user->attachRole($userRole);
    }
}