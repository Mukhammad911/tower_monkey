<?php

use App\Models\Bot;
use App\Models\Product;
use App\Models\Purchase;
use Illuminate\Database\Seeder;

class PurchasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bot = Bot::find(1);
        $product = Product::where('name', 'galil ar')->where('short_description', 'sage spray')->first();

        $purchase = Purchase::create([
            'bot_id' => $bot->id,
            'product_id' => $product->id,
            'target' => 5
        ]);
        $purchase->save();
    }
}
