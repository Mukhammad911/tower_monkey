<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role as Role;
//use DB;

class RoleTableSeeder extends Seeder {

    public function run() {

        Role::create([
            'name' => 'User',
            'slug' => 'user',
            'description' => 'Steam User', // optional
            'level' => 1, // optional, set to 1 by default
        ]);

        Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Access all in admin panel', // optional
            'level' => 2, // optional, set to 1 by default
        ]);

        Role::create([
            'name' => 'Youtuber',
            'slug' => 'youtuber',
            'description' => 'Youtuber admin panel', // optional
            'level' => 2, // optional, set to 1 by default
        ]);
    }
}